//A test to check that the action stack clears the contents of the redo stack after a new action is pushed.

function start(){

    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();
}

function performAction(){
    _terrain.beginMouseAction("height");
    _terrain.heightBrush(true, 0.5, 0.5, 100, 100);
    _terrain.endMouseAction();
}

function update(){

    for(local i = 0; i < 4; i++){ //Create four actions.
        performAction();
    }

    _test.assertEqual(_actionStack.getUndoStackCount(), 4);
    _test.assertEqual(_actionStack.getRedoStackCount(), 0);

    _actionStack.undo();
    _actionStack.undo();

    _test.assertEqual(_actionStack.getUndoStackCount(), 2);
    _test.assertEqual(_actionStack.getRedoStackCount(), 2);

    performAction(); //Push a new action, and the redo stack should be cleared.

    _test.assertEqual(_actionStack.getUndoStackCount(), 3);
    _test.assertEqual(_actionStack.getRedoStackCount(), 0);

    _test.endTest();

}
