//A test to check that the action stack removes actions after it has reached its action limit.

function start(){

    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    ::maxActions <- _actionStack.getMaxActions();
}

function update(){

    for(local i = 0; i < maxActions + 30; i++){ //A few over what the max is, just to check it's actually being limited.
        _terrain.beginMouseAction("height");
        _terrain.heightBrush(true, 0.5, 0.5, 100, 100);
        _terrain.endMouseAction();

        local l = i + 1;
        if(l > maxActions) l = maxActions;
        _test.assertEqual(_actionStack.getUndoStackCount(), l);
        print(_actionStack.getUndoStackCount());
    }

    _test.endTest();

}
