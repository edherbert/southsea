//A test which inserts a new item into both an empty node and a node with children.

function start(){
    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    _scene.setSceneTree([
        ["first", 2],
        ["second", 2],
        ["", 1],
            ["item", 2],
        ["", 0],
    ]);
}

function update(){
    local firstId = _scene.getIdByName("first");
    local secondId = _scene.getIdByName("second");

    {
        _scene.performSceneInsertAction(firstId, 0);

        local expectedTree = [
            ["first", firstId],
            ["", 0],
                ["empty", -1],
            ["", 0],
            ["second", secondId],
            ["", 0],
                ["item", -1],
            ["", 0],
        ]

        _test.assertTrue(_scene.compareCurrentTree(expectedTree));

        _actionStack.undo();

        _test.assertTrue(_scene.compareCurrentTree([
            ["first", firstId],
            ["second", secondId],
            ["", 0],
                ["item", -1],
            ["", 0],
        ]));

        _actionStack.redo();

        _test.assertTrue(_scene.compareCurrentTree(expectedTree));

        _actionStack.undo();
    }

    {
        _scene.performSceneInsertAction(secondId, 0);

        local expectedTree = [
            ["first", firstId],
            ["second", secondId],
            ["", 0],
                ["item", -1],
                ["empty", -1],
            ["", 0],
        ]

        _test.assertTrue(_scene.compareCurrentTree(expectedTree));

        _actionStack.undo();

        _test.assertTrue(_scene.compareCurrentTree([
            ["first", firstId],
            ["second", secondId],
            ["", 0],
                ["item", -1],
            ["", 0],
        ]));

        _actionStack.redo();

        _test.assertTrue(_scene.compareCurrentTree(expectedTree));
    }

    _test.endTest();
}
