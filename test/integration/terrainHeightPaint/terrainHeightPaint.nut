//A test to check that height paint actions effect the height of the terrain.

function start(){
    ::stage <- 0;
    ::stageCount <- 0.0;

    ::previousHeight <- 0;

    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();
}

function update(){
    if(stage == 0){
        previousHeight = _terrain.getHeightAt(0.5, 0.5); //Populate the height at the beginning.

        _terrain.beginMouseAction("height");

        stage++;
    }
    if(stage == 1){
        _terrain.heightBrush(true, 0.5, 0.5, 100, 100);

        local currentHeight = _terrain.getHeightAt(0.5, 0.5);
        print(currentHeight);

        _test.assertTrue(currentHeight > previousHeight);

        if(stageCount >= 10){
            stage++;
        }
        stageCount++;
    }
    if(stage == 2){
        _terrain.endMouseAction();
        stage++;
    }
    if(stage == 3){
        _test.endTest();
    }
}
