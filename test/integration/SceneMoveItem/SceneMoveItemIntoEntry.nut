//A test which moves inserts an item into another, expecting it to appear as a child of that entry.

function start(){
    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    _scene.setSceneTree([ ["first", 2], ["second", 2] ]);
}

function update(){
    local targetId = _scene.getIdByName("second");
    local destinationId = _scene.getIdByName("first");

    //1 - into
    //2 - above
    //3 - below
    _scene.moveSceneTreeEntry(targetId, destinationId, 1);

    local expectedTree = [
        ["first", destinationId],
        ["", 0],
            ["second", targetId],
        ["", 0]
    ]

    _test.assertTrue(_scene.compareCurrentTree(expectedTree));

    _actionStack.undo();

    _test.assertTrue(_scene.compareCurrentTree([ ["first", destinationId], ["second", targetId] ]));

    _actionStack.redo();

    _test.assertTrue(_scene.compareCurrentTree(expectedTree));


    _test.endTest();
}
