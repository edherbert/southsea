//A test to check that renaming of items in the scene tree conforms to the undo and redo procedure.

function start(){
    ::stage <- 0;
    ::stageCount <- 0;

    ::firstName <- "firstName";
    ::changeName <- "newName";

    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    _scene.setSceneTree([[firstName, 2]]);
}

function update(){
        local targetId = _scene.getIdByName(firstName);
        print(targetId);
        _test.assertNotEqual(targetId, 0);

        _scene.performSceneRenameAction(targetId, changeName);

        local result = _scene.getSceneNameOfEntry(targetId);
        _test.assertEqual(result, changeName);

        for(local i = 0; i < 10; i++){
            local checkString = firstName;
            if(i % 2 == 0){
                _actionStack.undo();
                print("undo");
            }else{
                _actionStack.redo();
                print("redo")

                checkString = changeName;
            }

            local result = _scene.getSceneNameOfEntry(targetId);
            _test.assertEqual(result, checkString);
        }


        _test.endTest();

}
