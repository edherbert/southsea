//A test to check that the action stack is cleared when a chunk switch occurs.
//Both the undo and redo action stacks should be emptied when a switch happens, so this test generates some content for them and checks it's gone after a switch.

function start(){
    ::stage <- 0;

    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    _chunk.switchChunk(0, 0); //Make sure we start on 0, 0
}

function update(){

    _test.assertEqual(_actionStack.getUndoStackCount(), 0);
    _test.assertEqual(_actionStack.getRedoStackCount(), 0);

    for(local i = 0; i < 2; i++){
        _terrain.beginMouseAction("height");
        _terrain.heightBrush(true, 0.5, 0.5, 100, 100);
        _terrain.endMouseAction();
    }

    _actionStack.undo();
    //Now there should be one action in both stacks.

    _test.assertEqual(_actionStack.getUndoStackCount(), 1);
    _test.assertEqual(_actionStack.getRedoStackCount(), 1);

    _chunk.switchChunk(1, 0);

    _test.assertEqual(_actionStack.getUndoStackCount(), 0);
    _test.assertEqual(_actionStack.getRedoStackCount(), 0);

    _test.endTest();
}
