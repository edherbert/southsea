//A test which deletes items from a tree and then undoes it.

function start(){
    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    _scene.setSceneTree([
        ["first", 2],
        ["second", 2],
        ["", 1],
            ["item", 2],
            ["itemSecond", 2],
            ["", 1],
                ["itemInside", 2],
            ["", 0],
        ["", 0],
        ["third", 2]
    ]);

    ::first <- true;
}

function update(){
    if(first){
        first = false;
    }else return;

    local targetId = _scene.getIdByName("second");
    local destinationId = _scene.getIdByName("first");

    _scene.performSceneDeleteAction(targetId);

    local expectedTree = [
        ["first", -1],
        ["third", -1]
    ]

    _test.assertTrue(_scene.compareCurrentTree(expectedTree));

    _actionStack.undo();

    _test.assertTrue(_scene.compareCurrentTree([
        ["first", destinationId],
        ["second", targetId],
        ["", 0],
            ["item", -1],
            ["itemSecond", -1],
            ["", 0],
                ["itemInside", -1],
            ["", 0],
        ["", 0],
        ["third", -1]
    ]));

    _actionStack.redo();

    _test.assertTrue(_scene.compareCurrentTree(expectedTree));


    _test.endTest();
}
