//A test which deletes an only child, expecting the terinators to be destroyed.

function start(){
    local fileDir = _test.getScriptFileDirectory();
    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    _scene.setSceneTree([
        ["second", 2],
        ["", 1],
            ["item", 2],
        ["", 0]
    ]);

    ::first <- true;
}

function update(){
    if(first){
        first = false;
    }else return;

    local targetId = _scene.getIdByName("item");
    local secondId = _scene.getIdByName("second");

    _scene.performSceneDeleteAction(targetId);

    local expectedTree = [
        ["second", secondId]
    ]

    _test.assertTrue(_scene.compareCurrentTree(expectedTree));

    _actionStack.undo();

    _test.assertTrue(_scene.compareCurrentTree([
        ["second", secondId],
        ["", 0],
            ["item", targetId],
        ["", 0]
    ]));

    _actionStack.redo();

    _test.assertTrue(_scene.compareCurrentTree(expectedTree));


    _test.endTest();
}
