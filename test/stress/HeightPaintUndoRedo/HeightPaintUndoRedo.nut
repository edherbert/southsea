//A stress test to draw height data on the terrain, and check that the undo and redo functionality works correctly.

function start(){
    ::count <- 0;
    ::editorStarted <- false;

    local fileDir = _test.getScriptFileDirectory();

    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir");
    _project.loadSpecifiedProject();

    ::drawX <- 0.0;
    ::drawY <- 0.0;
    ::animCount <- 0.0;
    ::frameCount <- 0; //Int containing number of frames.
    ::actionCount <- 0; //Int of how many actions have occured
    ::maxActionCount <- 4;
    ::actionStage <- 0;

    enum STATES{
        draw,
        undo
    };

    ::currentState <- STATES.draw;

    ::beforeAfterDraw <- [];

    _terrain.beginMouseAction("height");

    _camera.setPosition(0, 0, -100);
    _camera.lookAt(0, -50, 0);
}

function updateDraw(){
    animCount += 0.003;
    drawX = sin((animCount * 20)) / 4 + 0.5;
    drawY = animCount;

    local oldHeight = _terrain.getHeightAt(drawX, drawY);
    _terrain.heightBrush(true, drawX, drawY, 100, 100);

    if(frameCount == 50){
        //Half way through the draw cycle make a note of what the height was both before and after the action takes place.

        local newHeight = _terrain.getHeightAt(drawX, drawY);
        beforeAfterDraw.push({
            "x":drawX,
            "y":drawY,
            "old":oldHeight,
            "new":newHeight
        });
    }

    if(frameCount >= 100){
        _terrain.endMouseAction();
        actionCount++;

        if(actionCount >= maxActionCount - 1){
            animCount = 0;
            currentState = STATES.undo;
        }else{
            _terrain.beginMouseAction("height");
        }

        frameCount = 0;
    }
}

function updateUndo(){
    if(frameCount % 20 != 0) return;

    if(actionStage == 0){

        if(actionCount > 0){
            _actionStack.undo();

            //local vals = beforeAfterDraw[actionCount - 1];

            //local newHeight = _terrain.getHeightAt(vals.x, vals.y);

            actionCount--;
        }else{
            //Start the redo stage
            actionCount = maxActionCount - 1;
            actionStage++;

        }

    }
    if(actionStage == 1){

        if(actionCount > 0){
            _actionStack.redo();

            actionCount--;
        }else{
            //Start the redo stage
            actionCount = maxActionCount - 1;
            actionStage++;
        }

    }
    if(actionStage == 2){
        if(actionCount > 0){
            _actionStack.undo();

            actionCount--;
        }else{
            //Return to drawing.
            currentState = STATES.draw;
            frameCount = 0;
            actionCount = 0;
            actionStage = 0;

            beforeAfterDraw.clear();

            _terrain.beginMouseAction("height");
        }

    }
}

function update(){
    frameCount++;

    if(currentState == STATES.draw){
        updateDraw();
    }
    else if(currentState == STATES.undo){
        updateUndo();
    }

}
