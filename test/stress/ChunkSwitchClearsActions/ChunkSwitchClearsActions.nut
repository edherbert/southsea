//A stress test to generate actions, while switching the chunk back and forth.
//There should be no remaining actions after the switch, but more than anything this stress test is used to check there are no memory leaks resulting from doing this.

function start(){
    ::count <- 0;
    ::targetChunk <- false;
    ::actionsPerformed <- 0;
    ::actionsRequired <- 10;

    local fileDir = _test.getScriptFileDirectory();

    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir/TerrainMapFilled");
    _project.loadSpecifiedProject();
}

function update(){
    count++;


    if(count % 10 == 0){ //Perform an action

        {
            _terrain.beginMouseAction("height");
            _terrain.heightBrush(true, 0.5, 0.5, 100, 100);
            _terrain.endMouseAction();
        }
        actionsPerformed++;

        _test.assertEqual(_actionStack.getUndoStackCount(), actionsPerformed);
        _test.assertEqual(_actionStack.getRedoStackCount(), 0);

        if(actionsPerformed > actionsRequired){

            for(local i = 0; i < 5; i++){
                _actionStack.undo(); //Undo some actions so there is also something in the redo stack.
            }

            targetChunk = !targetChunk;
            _chunk.switchChunk(targetChunk ? 0 : 1, 0);

            actionsPerformed = 0;
        }
    }
}
