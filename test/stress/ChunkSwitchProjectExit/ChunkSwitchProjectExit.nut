//A stress test to switch the current chunk of the editor back and forth, and then exit the project every few switches.

function start(){
    ::count <- 0;
    ::chunkX <- 0;
    ::chunkY <- 0;

    ::maxChunk <- 2;


    enum EDITOR_STATES{
        started,
        stopped
    };

    ::currentState <- EDITOR_STATES.stopped;

}

function switchStates(){
    if(currentState == EDITOR_STATES.started){
        _project.closeProject();

        currentState = EDITOR_STATES.stopped;
    }else{
        local fileDir = _test.getScriptFileDirectory();

        _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
        _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir/TerrainMapFilled");
        _project.loadSpecifiedProject();

        currentState = EDITOR_STATES.started;
    }

    count = 0;
}

function update(){
    count++;
    if(currentState == EDITOR_STATES.started){
        updateStarted();
    }else{
        updateStopped();
    }
}

function updateStarted(){

    if(chunkX > maxChunk){
        chunkX = 0;
        chunkY++;
    }
    if(chunkY > maxChunk){
        chunkY = 0;
        chunkX = 0;
        switchStates();
    }

    if(count % 100 == 0){
        _chunk.switchChunk(chunkX, chunkY);
        chunkX++;
    }
}

function updateStopped(){
    if(count >= 100){
        switchStates();
    }
}
