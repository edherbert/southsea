//A stress test to switch the current chunk of the editor back and forth.

function start(){
    ::count <- 0;
    ::chunkX <- 0;
    ::chunkY <- 0;

    ::maxChunk <- 2;

    local fileDir = _test.getScriptFileDirectory();

    _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
    _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir/TerrainMapFilled");
    _project.loadSpecifiedProject();
}

function update(){
    count++;

    if(chunkX > maxChunk){
        chunkX = 0;
        chunkY++;
    }
    if(chunkY > maxChunk){
        chunkY = 0;
        chunkX = 0;
    }

    if(count % 100 == 0){
        _chunk.switchChunk(chunkX, chunkY);
        chunkX++;
    }
}
