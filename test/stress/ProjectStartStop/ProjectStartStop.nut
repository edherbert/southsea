//A stress test to start and stop the editor to check for things like memory leaks.

function start(){
    ::count <- 0;
    ::editorStarted <- false;
}

function switchEditor(started){

    if(!started){
        local fileDir = _test.getScriptFileDirectory();

        print(fileDir + "/../../common/simpleProject");
        _project.specifyProjectPath(fileDir + "/../../common/simpleProject");
        _project.specifyMapPath(fileDir + "/../../common/simpleProject/mapsDir/first");
        //Load the specified project settings.
        _project.loadSpecifiedProject();
    }else{
        _project.closeProject();
    }
}

function update(){

    count++;
    if(count >= 100){
        switchEditor(editorStarted);
        editorStarted = !editorStarted;
        count = 0;
    }
}
