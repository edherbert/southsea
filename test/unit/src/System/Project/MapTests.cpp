#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../../FileSystemHelper.h"

#define private public

#include "System/Project/Map.h"
#include "System/Project/AVSetupFile.h"

#include <fstream>
#include <iostream>

class MapTests : public ::testing::Test {
    protected:

    MapTests() {
    }

    virtual ~MapTests() {
    }

};

TEST_F(MapTests, parseDirectoryEntryPassesRegex){

    std::vector<std::string> stringsToAttempt = {
        "00000000",
        "-00000000",
        "-10001000",
        "-10001000",
        "-1000-1000",
        "-9999-9999",
        "99999999"
    };

    for(const std::string& s : stringsToAttempt){
        Southsea::Map m;

        m._parseDirectoryEntry(s);
        ASSERT_EQ(m.mParsedChunks.size(), 1); //If the parsed chunks goes up then it was accepted.
    }

}

TEST_F(MapTests, parseDirectoryEntryFailsRegex){

    std::vector<std::string> stringsToAttempt = {
        "--00000000",
        "00",
        "123123",
        "42069",
        "AA-D80073",
        "abcdefghijk"
    };

    for(const std::string& s : stringsToAttempt){
        Southsea::Map m;

        m._parseDirectoryEntry(s);
        ASSERT_EQ(m.mParsedChunks.size(), 0); //If the parsed chunks goes up then it was accepted.
    }

}
