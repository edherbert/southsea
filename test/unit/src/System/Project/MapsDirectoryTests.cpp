#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../../FileSystemHelper.h"

#define private public

#include "System/Project/MapsDirectory.h"

class MapsDirectoryTests : public ::testing::Test {
    protected:

    MapsDirectoryTests() {
    }

    virtual ~MapsDirectoryTests() {
    }

};

TEST_F(MapsDirectoryTests, FindAvailableMapsFindsDirectories){
    const std::string mapPath = FileSystemHelper::getDummyDirectoryPath();
    const filesystem::path targetDirectory(mapPath);

    static const int targetCount = 10;
    for(int i = 0; i < targetCount; i++){
        filesystem::create_directory(targetDirectory / filesystem::path( std::to_string(i) ));
    }

    Southsea::MapsDirectory mapsDir(mapPath);
    std::vector<std::string> mapsResult;

    ASSERT_TRUE(mapsDir.findAvailableMaps(mapsResult));

    ASSERT_EQ(targetCount, mapsResult.size());
}

TEST_F(MapsDirectoryTests, FindAvailableMapsReturnsFalseIfBadPath){
    const std::string path = FileSystemHelper::getNonExistantDirectoryPath();

    Southsea::MapsDirectory mapsDir(path);
    std::vector<std::string> mapsResult = {
        "first", "second", "third" //The function clears the list if the directory is invalid, so these should be cleared.
    };

    ASSERT_FALSE(mapsDir.findAvailableMaps(mapsResult));

    ASSERT_EQ(0, mapsResult.size());
}
