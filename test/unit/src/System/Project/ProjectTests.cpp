#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../../FileSystemHelper.h"

#define private public

#include "System/Project/Project.h"
#include "System/Project/AVSetupFile.h"

#include <fstream>
#include <iostream>

class ProjectTests : public ::testing::Test {
    protected:

    ProjectTests() {
    }

    virtual ~ProjectTests() {
    }

};

TEST_F(ProjectTests, DetermineProjectPathValidityReturnsTrueOnDirectoryWithFile){
    const std::string mapPath = FileSystemHelper::getDummyDirectoryPath();
    const filesystem::path targetDirectory(mapPath);

    { //Create the avSetup.cfg file in this directory.
        std::ofstream outfile;
        outfile.open( ( targetDirectory / filesystem::path(Southsea::AVSetupFile::AV_FILE_NAME) ).str().c_str());

        outfile << "Test String!" << std::endl;

        outfile.close();
    }

    Southsea::Project proj(mapPath); //This one assumes the file is named avSetup.cfg, and if it finds it in the given directory returns true.

    ASSERT_TRUE(proj.isValid());
}

TEST_F(ProjectTests, DetermineProjectPathValidityReturnsFalseOnEmptyDirectory){
    const std::string mapPath = FileSystemHelper::getDummyDirectoryPath();
    const filesystem::path targetDirectory(mapPath);

    Southsea::Project proj(mapPath);

    ASSERT_FALSE(proj.isValid());
}
