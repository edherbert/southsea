#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <OgreConfigFile.h>
#include "../../FileSystemHelper.h"

#define private public

#include "System/Project/AVSetupFile.h"

class AVSetupFileTests : public ::testing::Test {
    protected:

    AVSetupFileTests() {
    }

    virtual ~AVSetupFileTests() {
    }

};

TEST_F(AVSetupFileTests, DetermineFSObjectReturnsFalseIfFileNotFound){
    const std::string path = FileSystemHelper::getNonExistantFilePath();
    Southsea::AVSetupFile file(path);

    filesystem::path outPath;
    ASSERT_FALSE(file._determineFSObjectWithPath("/", path, outPath));
}

TEST_F(AVSetupFileTests, DetermineFSObjectReturnsTrueIfFileExists){
    const std::string path = FileSystemHelper::getDummyFilePath();
    Southsea::AVSetupFile file(path);

    filesystem::path outPath;
    ASSERT_TRUE(file._determineFSObjectWithPath("/", path, outPath));
}
/*
TEST_F(AVSetupFileTests, determineDirectoryWithPathReturnsAbsolutePath){
    //If given an absolute path it should just return that.
    Southsea::AVSetupFile file(""); //Doens't matter if the file has a good path or not.
    file.parse();

    const std::string path = FileSystemHelper::getDummyDirectoryPath();

    std::string output;
    //First variable is just an empty string which would otherwise be the data direcotry path.
    ASSERT_TRUE( file._determineItemWithPath("", path, &output, Southsea::AVSetupFile::PathType::Directory) );

    ASSERT_EQ(output, path);
}

TEST_F(AVSetupFileTests, determineDirectoryWithPathResolvesPathInCurrentDirectory){
    Southsea::AVSetupFile file("");
    file.parse();

    filesystem::path dirPath(FileSystemHelper::getDummyDirectoryPath());

    std::string output;
    ASSERT_TRUE( file._determineItemWithPath(dirPath.str(), ".", &output, Southsea::AVSetupFile::PathType::Directory) );
    //The . means this current directory, so the path should just be exactly the same.
    ASSERT_EQ(output, dirPath.str());
}

TEST_F(AVSetupFileTests, determineDirectoryWithPathResolvesDirectoryDown){
    Southsea::AVSetupFile file("");
    file.parse();

    const std::string customPath("extra/");

    const filesystem::path dirPath(FileSystemHelper::getDummyDirectoryPath());

    filesystem::path otherPath = dirPath / filesystem::path(customPath);
    filesystem::create_directory(otherPath);

    std::string output;
    ASSERT_TRUE( file._determineItemWithPath(dirPath.str(), customPath, &output, Southsea::AVSetupFile::PathType::Directory) );

    ASSERT_EQ(output, otherPath.str());
}

TEST_F(AVSetupFileTests, determineDirectoryWithPathResolvesDirectoryUp){
    Southsea::AVSetupFile file("");
    file.parse();

    //The dummy path is where we expect to end up.
    const filesystem::path dummyPath(FileSystemHelper::getDummyDirectoryPath());

    //The data directory is dummy/extra. This is the data directory.
    filesystem::path dataPath = dummyPath / filesystem::path("extra");
    filesystem::create_directory(dataPath);

    //Going one up ( .. ) means back to the dummy directory.
    std::string output;
    ASSERT_TRUE( file._determineItemWithPath(dataPath.str(), "..", &output, Southsea::AVSetupFile::PathType::Directory) );

    ASSERT_EQ(output, dummyPath.str());
}

TEST_F(AVSetupFileTests, getDataDirectoryReturnsAbsolutePath){
    const std::string parentDir = FileSystemHelper::getDummyDirectoryPath();
    const std::string setupFilePath = (filesystem::path(parentDir) / filesystem::path("avSetup.cfg")).str();
    const std::string targetDirectory = (filesystem::path(parentDir) / filesystem::path("otherDirectory")).str();
    filesystem::create_directory(targetDirectory);

    FileSystemHelper::cfgList cfgs = {
        {"DataDirectory", targetDirectory} //This should be an absolute directory.
    };
    FileSystemHelper::writeCfgFile(setupFilePath, cfgs);

    Southsea::AVSetupFile file(setupFilePath);
    file.parse();
    const std::string determinedDataDirectory = file.getDataDirectory();

    ASSERT_EQ(determinedDataDirectory, targetDirectory);
}

TEST_F(AVSetupFileTests, getDataDirectoryFindsRelativePath){
    const std::string parentDir = FileSystemHelper::getDummyDirectoryPath();
    const std::string setupFilePath = (filesystem::path(parentDir) / filesystem::path("avSetup.cfg")).str();
    const std::string targetDirectory = (filesystem::path(parentDir) / filesystem::path("otherDirectory")).str();
    filesystem::create_directory(targetDirectory);

    FileSystemHelper::cfgList cfgs = {
        {"DataDirectory", "otherDirectory"} //Now it's a relative directory.
    };
    FileSystemHelper::writeCfgFile(setupFilePath, cfgs);

    Southsea::AVSetupFile file(setupFilePath);
    file.parse();
    const std::string determinedDataDirectory = file.getDataDirectory();

    ASSERT_EQ(determinedDataDirectory, targetDirectory);
}

TEST_F(AVSetupFileTests, getMapsDirectoryFindsAbsolutePath){
    const std::string parentDir = FileSystemHelper::getDummyDirectoryPath();
    const std::string setupFilePath = (filesystem::path(parentDir) / filesystem::path("avSetup.cfg")).str();
    const std::string targetDirectory = (filesystem::path(parentDir) / filesystem::path("mapsDirectory")).str();
    filesystem::create_directory(targetDirectory);

    FileSystemHelper::cfgList cfgs = {
        {"DataDirectory", "."},
        {"MapsDirectory", targetDirectory}
    };
    FileSystemHelper::writeCfgFile(setupFilePath, cfgs);

    Southsea::AVSetupFile file(setupFilePath);
    file.parse();
    const std::string determinedMapsDirectory = file.getMapsDirectory();

    ASSERT_EQ(determinedMapsDirectory, targetDirectory);
}

TEST_F(AVSetupFileTests, getMapsDirectoryFindsRelativePath){
    const std::string parentDir = FileSystemHelper::getDummyDirectoryPath();
    const std::string setupFilePath = (filesystem::path(parentDir) / filesystem::path("avSetup.cfg")).str();
    const std::string targetDirectory = (filesystem::path(parentDir) / filesystem::path("mapsDirectory")).str();
    filesystem::create_directory(targetDirectory);

    FileSystemHelper::cfgList cfgs = {
        {"DataDirectory", "."},
        {"MapsDirectory", "mapsDirectory"}
    };
    FileSystemHelper::writeCfgFile(setupFilePath, cfgs);

    Southsea::AVSetupFile file(setupFilePath);
    file.parse();
    const std::string determinedMapsDirectory = file.getMapsDirectory();

    ASSERT_EQ(determinedMapsDirectory, targetDirectory);
}

TEST_F(AVSetupFileTests, fileNotValidIfDirectory){
    const std::string dir = FileSystemHelper::getDummyDirectoryPath();
    Southsea::AVSetupFile file(dir);
    file.parse();

    ASSERT_FALSE(file.valid());
}

TEST_F(AVSetupFileTests, fileNotValidIfDoesntExist){
    const std::string dir = FileSystemHelper::getNonExistantFilePath();
    Southsea::AVSetupFile file(dir);
    file.parse();

    ASSERT_FALSE(file.valid());
}

TEST_F(AVSetupFileTests, getOgreResourcesFileFindsRelativePath){
    const std::string parentDir = FileSystemHelper::getDummyDirectoryPath();
    const std::string setupFilePath = (filesystem::path(parentDir) / filesystem::path("avSetup.cfg")).str();
    const std::string filePath = (filesystem::path(parentDir) / filesystem::path("OgreResources.cfg")).str();

    FileSystemHelper::cfgList ogreCfgs;
    FileSystemHelper::writeCfgFile(filePath, ogreCfgs);

    FileSystemHelper::cfgList cfgs = {
        {"DataDirectory", "."},
        {"OgreResourcesFile", "OgreResources.cfg"}
    };
    FileSystemHelper::writeCfgFile(setupFilePath, cfgs);

    Southsea::AVSetupFile file(setupFilePath);
    file.parse();
    const std::string determinedFilePath = file.getOgreResourcesFile();

    ASSERT_EQ(determinedFilePath, filePath);
}
*/
