#include "gtest/gtest.h"
#include "gmock/gmock.h"

#define private public

#include "Editor/Scene/SceneTreeManager.h"

class SceneTreeManagerTests : public ::testing::Test {
protected:

    Southsea::SceneTreeManager* mSceneTree;

    SceneTreeManagerTests(){
        mSceneTree = new Southsea::SceneTreeManager();
    }

    virtual ~SceneTreeManagerTests(){
        delete mSceneTree;
    }

    bool compareLists(const std::vector<Southsea::SceneEntry>& f, const std::vector<Southsea::SceneEntry> s){
        if(f.size() != s.size()) return false;

        for(int i = 0; i < f.size(); i++){
            if(f[i].id != s[i].id) return false;
        }

        return true;
    }

};

TEST_F(SceneTreeManagerTests, moveAbove){
    using namespace Southsea;

    EntryId first = mSceneTree->_getEntryId();
    EntryId second = mSceneTree->_getEntryId();
    mSceneTree->mSceneTree = {
        SceneTreeManager::CHILD,
        {first, "", SceneEntryType::empty, false},
        {second, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    };

    mSceneTree->moveEntry(second, first, ObjectInsertionType::above);

    ASSERT_TRUE(compareLists(mSceneTree->mSceneTree, {
        SceneTreeManager::CHILD,
        {second, "", SceneEntryType::empty, false},
        {first, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    }));
}

TEST_F(SceneTreeManagerTests, insertInto){
    using namespace Southsea;

    EntryId first = mSceneTree->_getEntryId();
    EntryId second = mSceneTree->_getEntryId();
    mSceneTree->mSceneTree = {
        SceneTreeManager::CHILD,
        {first, "", SceneEntryType::empty, false},
        {second, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    };

    mSceneTree->moveEntry(second, first, ObjectInsertionType::into);

    ASSERT_TRUE(compareLists(mSceneTree->mSceneTree, {
        SceneTreeManager::CHILD,
        {first, "", SceneEntryType::empty, false},
            SceneTreeManager::CHILD,
            {second, "", SceneEntryType::empty, false},
            SceneTreeManager::TERM,
        SceneTreeManager::TERM
    }));
}

TEST_F(SceneTreeManagerTests, moveBelow){
    using namespace Southsea;

    EntryId first = mSceneTree->_getEntryId();
    EntryId second = mSceneTree->_getEntryId();
    mSceneTree->mSceneTree = {
        SceneTreeManager::CHILD,
        {first, "", SceneEntryType::empty, false},
        {second, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    };

    mSceneTree->moveEntry(first, second, ObjectInsertionType::below);

    ASSERT_TRUE(compareLists(mSceneTree->mSceneTree, {
        SceneTreeManager::CHILD,
        {second, "", SceneEntryType::empty, false},
        {first, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    }));
}

TEST_F(SceneTreeManagerTests, moveChildItemsIntoItem){
    using namespace Southsea;

    EntryId first = mSceneTree->_getEntryId();
    EntryId second = mSceneTree->_getEntryId();
    EntryId third = mSceneTree->_getEntryId();
    mSceneTree->mSceneTree = {
        SceneTreeManager::CHILD,
        {first, "", SceneEntryType::empty, false},
            SceneTreeManager::CHILD,
                {third, "", SceneEntryType::empty, false},
            SceneTreeManager::TERM,
        {second, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    };

    mSceneTree->moveEntry(first, second, ObjectInsertionType::into);

    ASSERT_TRUE(compareLists(mSceneTree->mSceneTree, {
        SceneTreeManager::CHILD,
        {second, "", SceneEntryType::empty, false},
        SceneTreeManager::CHILD,
            {first, "", SceneEntryType::empty, false},
            SceneTreeManager::CHILD,
                {third, "", SceneEntryType::empty, false},
            SceneTreeManager::TERM,
        SceneTreeManager::TERM,
        SceneTreeManager::TERM
    }));
}

TEST_F(SceneTreeManagerTests, movingAllChildrenFromParentRemovesTerminators){
    using namespace Southsea;

    EntryId first = mSceneTree->_getEntryId();
    EntryId second = mSceneTree->_getEntryId();
    mSceneTree->mSceneTree = {
        SceneTreeManager::CHILD,
        {first, "", SceneEntryType::empty, false},
            SceneTreeManager::CHILD,
                {second, "", SceneEntryType::empty, false},
            SceneTreeManager::TERM,
        SceneTreeManager::TERM
    };

    mSceneTree->moveEntry(second, first, ObjectInsertionType::above);

    ASSERT_TRUE(compareLists(mSceneTree->mSceneTree, {
        SceneTreeManager::CHILD,
        {second, "", SceneEntryType::empty, false},
        {first, "", SceneEntryType::empty, false},
        SceneTreeManager::TERM
    }));
}
