#include "gtest/gtest.h"
#include "gmock/gmock.h"

#define private public

#include "Editor/Chunk/NavMesh/NavMeshScanner.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/SceneTypes.h"
#include <memory>
#include <set>

class NavMeshScannerTests : public ::testing::Test {
protected:

    static size_t mTotalChecks;
    static std::set<Southsea::EntryId> mExpectedIds;
    Southsea::NavMeshScanner* mNavMeshScanner;
    NavMeshScannerTests(){
        mNavMeshScanner = new Southsea::NavMeshScanner();
        mExpectedIds.clear();
        mTotalChecks = 0;
    }

    virtual ~NavMeshScannerTests() {
        delete mNavMeshScanner;
    }

    static bool checkObjectCounter(const Southsea::NavMarkerRules* parentRules, const Southsea::SceneEntry& e, Southsea::NavMeshScanner::Tree data, Southsea::NavMeshScanner::OgreTree ogreData){
        auto it = mExpectedIds.find(e.id);
        if(it != mExpectedIds.end()){
            mExpectedIds.erase(e.id);
        }
        mTotalChecks++;

        return false;
    }

};

std::set<Southsea::EntryId> NavMeshScannerTests::mExpectedIds;
size_t NavMeshScannerTests::mTotalChecks;

TEST_F(NavMeshScannerTests, scanTreeForSingleMarker){
    Southsea::SceneEntry rootEntry = Southsea::SceneTreeManager::CHILD;
    rootEntry.id = Southsea::ROOT_NODE;
    const std::vector<Southsea::SceneEntry> sceneTree = {
        rootEntry,
        {2, "first", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {3, "second", Southsea::SceneEntryType::empty, false, true},
                Southsea::SceneTreeManager::CHILD,
                {4, "inside", Southsea::SceneEntryType::navMarker, false, true},
                {5, "moreInside", Southsea::SceneEntryType::empty, false, true},
                Southsea::SceneTreeManager::TERM,
            {6, "third", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        {7, "fourth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {8, "fifth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        Southsea::SceneTreeManager::TERM
    };
    Southsea::NavMarkerObjectData* createdData = new Southsea::NavMarkerObjectData();
    std::unique_ptr<Southsea::NavMarkerObjectData> d(createdData);
    Southsea::TreeObjectManager::TreeData objectData = {
        {4, createdData}
    };
    Southsea::OgreTreeManager::TreeData ogreData;

    mExpectedIds = {5};
    mNavMeshScanner->scanTreeForMarker(sceneTree, objectData, ogreData, &NavMeshScannerTests::checkObjectCounter);
    //All the expected values should have been found.
    ASSERT_EQ(mExpectedIds.size(), 0);
    ASSERT_EQ(mTotalChecks, 1);
}

TEST_F(NavMeshScannerTests, scanTreeForMultipleMarkers){
    Southsea::SceneEntry rootEntry = Southsea::SceneTreeManager::CHILD;
    rootEntry.id = Southsea::ROOT_NODE;
    const std::vector<Southsea::SceneEntry> sceneTree = {
        rootEntry,
        {2, "first", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {3, "second", Southsea::SceneEntryType::navMarker, false, true},
            {4, "third", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        {5, "fourth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {6, "fifth", Southsea::SceneEntryType::navMarker, false, true},
            {7, "fifth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        Southsea::SceneTreeManager::TERM
    };
    Southsea::NavMarkerObjectData* createdData = new Southsea::NavMarkerObjectData();
    std::unique_ptr<Southsea::NavMarkerObjectData> d(createdData);
    Southsea::TreeObjectManager::TreeData objectData = {
        {3, createdData},
        {6, createdData},
    };
    Southsea::OgreTreeManager::TreeData ogreData;

    mExpectedIds = {4, 7};
    mNavMeshScanner->scanTreeForMarker(sceneTree, objectData, ogreData, &NavMeshScannerTests::checkObjectCounter);
    ASSERT_EQ(mExpectedIds.size(), 0);
    ASSERT_EQ(mTotalChecks, 2);
}

TEST_F(NavMeshScannerTests, scanTreeForOverlappingMarker){
    //Even though there are two nav markers overlapping where each other would select, no items should be submitted twice.
    Southsea::SceneEntry rootEntry = Southsea::SceneTreeManager::CHILD;
    rootEntry.id = Southsea::ROOT_NODE;
    const std::vector<Southsea::SceneEntry> sceneTree = {
        rootEntry,
        {2, "marker", Southsea::SceneEntryType::navMarker, false, true},
        {3, "first", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {4, "second", Southsea::SceneEntryType::navMarker, false, true},
                Southsea::SceneTreeManager::CHILD,
                {5, "inside", Southsea::SceneEntryType::empty, false, true},
                {6, "moreInside", Southsea::SceneEntryType::empty, false, true},
                Southsea::SceneTreeManager::TERM,
            {7, "third", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        {8, "fourth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {9, "fifth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        Southsea::SceneTreeManager::TERM
    };
    Southsea::NavMarkerObjectData* createdData = new Southsea::NavMarkerObjectData();
    std::unique_ptr<Southsea::NavMarkerObjectData> d(createdData);
    Southsea::TreeObjectManager::TreeData objectData = {
        {2, createdData},
        {4, createdData},
    };
    Southsea::OgreTreeManager::TreeData ogreData;

    mExpectedIds = {3, 5, 6, 7, 8, 9};
    mNavMeshScanner->scanTreeForMarker(sceneTree, objectData, ogreData, &NavMeshScannerTests::checkObjectCounter);
    //All the expected values should have been found.
    ASSERT_EQ(mExpectedIds.size(), 0);
    ASSERT_EQ(mTotalChecks, 6);
}

TEST_F(NavMeshScannerTests, resolveFinalRulesForParent){
    Southsea::SceneEntry rootEntry = Southsea::SceneTreeManager::CHILD;
    rootEntry.id = Southsea::ROOT_NODE;
    const std::vector<Southsea::SceneEntry> sceneTree = {
        rootEntry,
        {2, "first", Southsea::SceneEntryType::empty, false, true},
        {3, "first", Southsea::SceneEntryType::navMarker, false, true},
        {4, "first", Southsea::SceneEntryType::empty, false, true},
        {5, "first", Southsea::SceneEntryType::navMarker, false, true},
        Southsea::SceneTreeManager::TERM
    };
    Southsea::NavMarkerObjectData* createdData = new Southsea::NavMarkerObjectData();
    std::unique_ptr<Southsea::NavMarkerObjectData> d(createdData);
    Southsea::TreeObjectManager::TreeData objectData = {
        {3, createdData},
        {5, createdData},
    };
    Southsea::OgreTreeManager::TreeData ogreData;

    Southsea::NavMarkerRules rules;
    Southsea::NavMarkerRules outRules;
    int foundMarkers = mNavMeshScanner->resolveFinalRulesForParent(sceneTree, objectData, ogreData, 0, &rules, &outRules);

    //It should find two markers.
    ASSERT_EQ(foundMarkers, 2);
}

TEST_F(NavMeshScannerTests, resolveFinalRulesForParentSkipChildren){
    Southsea::SceneEntry rootEntry = Southsea::SceneTreeManager::CHILD;
    rootEntry.id = Southsea::ROOT_NODE;
    const std::vector<Southsea::SceneEntry> sceneTree = {
        rootEntry,
        {3, "first", Southsea::SceneEntryType::navMarker, false, true},
            Southsea::SceneTreeManager::CHILD,
            //It should not find this one, because the child tag should be skipped.
            {4, "first", Southsea::SceneEntryType::navMarker, false, true},
            Southsea::SceneTreeManager::TERM,
        {2, "first", Southsea::SceneEntryType::empty, false, true},
        {5, "first", Southsea::SceneEntryType::empty, false, true},
        Southsea::SceneTreeManager::TERM
    };
    Southsea::NavMarkerObjectData* createdData = new Southsea::NavMarkerObjectData();
    std::unique_ptr<Southsea::NavMarkerObjectData> d(createdData);
    Southsea::TreeObjectManager::TreeData objectData = {
        {3, createdData},
        {4, createdData},
    };
    Southsea::OgreTreeManager::TreeData ogreData;

    Southsea::NavMarkerRules rules;
    Southsea::NavMarkerRules outRules;
    int foundMarkers = mNavMeshScanner->resolveFinalRulesForParent(sceneTree, objectData, ogreData, 0, &rules, &outRules);

    //This scan should only find the one marker.
    ASSERT_EQ(foundMarkers, 1);
}

TEST_F(NavMeshScannerTests, scanTreeForOverlappingMarkerAfterChildren){
    //TODO this test is failing due to a bug in the scanner.
    //If the encompassing marker is after the child markers then it will try and rescan that entire depth, which can lead to values being called twice.
    //I need to keep track of which markers have been scanned. If a scan runs into a found marker then skip that entire depth.
    Southsea::SceneEntry rootEntry = Southsea::SceneTreeManager::CHILD;
    rootEntry.id = Southsea::ROOT_NODE;
    const std::vector<Southsea::SceneEntry> sceneTree = {
        rootEntry,
        {3, "first", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {4, "second", Southsea::SceneEntryType::navMarker, false, true},
                Southsea::SceneTreeManager::CHILD,
                {5, "inside", Southsea::SceneEntryType::empty, false, true},
                {6, "moreInside", Southsea::SceneEntryType::empty, false, true},
                Southsea::SceneTreeManager::TERM,
            {7, "third", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        {8, "fourth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::CHILD,
            {9, "fifth", Southsea::SceneEntryType::empty, false, true},
            Southsea::SceneTreeManager::TERM,
        {2, "marker", Southsea::SceneEntryType::navMarker, false, true},
        Southsea::SceneTreeManager::TERM
    };
    Southsea::NavMarkerObjectData* createdData = new Southsea::NavMarkerObjectData();
    std::unique_ptr<Southsea::NavMarkerObjectData> d(createdData);
    Southsea::TreeObjectManager::TreeData objectData = {
        {2, createdData},
        {4, createdData},
    };
    Southsea::OgreTreeManager::TreeData ogreData;

    mExpectedIds = {3, 5, 6, 7, 8, 9};
    mNavMeshScanner->scanTreeForMarker(sceneTree, objectData, ogreData, &NavMeshScannerTests::checkObjectCounter);
    //All the expected values should have been found.
    ASSERT_EQ(mExpectedIds.size(), 0);
    ASSERT_EQ(mTotalChecks, 6);
}
