#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <OgreConfigFile.h>
#include "../../FileSystemHelper.h"

#define private public

#include "Editor/Compositor/EditorCompositorManager.h"

class EditorCompositorManagerTests : public ::testing::Test {
protected:

    const Ogre::Vector2 minVal = Ogre::Vector2(2000, 2000);

    EditorCompositorManagerTests() {
    }

    virtual ~EditorCompositorManagerTests() {
    }

};

TEST_F(EditorCompositorManagerTests, getBestSizeReturnsMinVal){
    Southsea::EditorCompositorManager compMan(0, 0);

    ASSERT_EQ(compMan._getBestSizeForSize(10, 10), minVal);

    ASSERT_EQ(compMan._getBestSizeForSize(2000, 2000), minVal);

    ASSERT_EQ(compMan._getBestSizeForSize(1000, 1000), minVal);
}

TEST_F(EditorCompositorManagerTests, getBestSizeReturnsVaryingIncrease){
    Southsea::EditorCompositorManager compMan(0, 0);

    ASSERT_EQ(compMan._getBestSizeForSize(2001, 2001), Ogre::Vector2(4000, 4000));
    ASSERT_EQ(compMan._getBestSizeForSize(3000, 3000), Ogre::Vector2(4000, 4000));
    ASSERT_EQ(compMan._getBestSizeForSize(4000, 4000), Ogre::Vector2(4000, 4000));

    ASSERT_EQ(compMan._getBestSizeForSize(6000, 6000), Ogre::Vector2(8000, 8000));
    ASSERT_EQ(compMan._getBestSizeForSize(7000, 7000), Ogre::Vector2(8000, 8000));
    ASSERT_EQ(compMan._getBestSizeForSize(8000, 8000), Ogre::Vector2(8000, 8000));

    ASSERT_EQ(compMan._getBestSizeForSize(14000, 14000), Ogre::Vector2(16000, 16000));
    ASSERT_EQ(compMan._getBestSizeForSize(10000, 10000), Ogre::Vector2(16000, 16000));
    ASSERT_EQ(compMan._getBestSizeForSize(8001, 8001), Ogre::Vector2(16000, 16000));

}

TEST_F(EditorCompositorManagerTests, getBestSizeReturnsAlteringValues){
    Southsea::EditorCompositorManager compMan(0, 0);

    ASSERT_EQ(compMan._getBestSizeForSize(3000, 100), Ogre::Vector2(4000, 2000));
    ASSERT_EQ(compMan._getBestSizeForSize(1999, 100), Ogre::Vector2(2000, 2000));
    ASSERT_EQ(compMan._getBestSizeForSize(4000, 100), Ogre::Vector2(4000, 2000));
    ASSERT_EQ(compMan._getBestSizeForSize(6000, 16000), Ogre::Vector2(8000, 16000));
}
