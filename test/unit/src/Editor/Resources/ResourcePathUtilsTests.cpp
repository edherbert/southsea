#include "gtest/gtest.h"
#include "gmock/gmock.h"

#define private public

#include "Editor/Resources/ResourcePathUtils.h"

class ResourcePathUtilsTests : public ::testing::Test {
protected:

    std::string mDataDirectory;
    static const std::string UNTOUCHED_STRING;

    ResourcePathUtilsTests()
        : mDataDirectory("/home/someone/something") {

        Southsea::ResourcePathUtils::_setDataDirectory(mDataDirectory);
    }

    virtual ~ResourcePathUtilsTests() {
        Southsea::ResourcePathUtils::_setDataDirectory("");
    }

};

const std::string ResourcePathUtilsTests::UNTOUCHED_STRING = "untouched";

TEST_F(ResourcePathUtilsTests, lexicallyRelativeReturnsCorrectPath){
    ASSERT_EQ(Southsea::ExposedPath("/a/d").lexicallyRelative( Southsea::ExposedPath("/a/b/c")), "../../d");
    ASSERT_EQ(Southsea::ExposedPath("/a/b/c").lexicallyRelative( Southsea::ExposedPath("/a/d")), "../b/c");
    ASSERT_EQ(Southsea::ExposedPath("a/b/c").lexicallyRelative( Southsea::ExposedPath("a")), "b/c");
    ASSERT_EQ(Southsea::ExposedPath("a/b/c").lexicallyRelative( Southsea::ExposedPath("a/b/c/x/y")), "../..");
    ASSERT_EQ(Southsea::ExposedPath("a/b/c").lexicallyRelative( Southsea::ExposedPath("a/b/c")), ".");
    ASSERT_EQ(Southsea::ExposedPath("a/b").lexicallyRelative( Southsea::ExposedPath("c/d")), "../../a/b");
}

TEST_F(ResourcePathUtilsTests, formatPathReturnsDefaultWithBadDataDirectory){
    Southsea::ResourcePathUtils::_setDataDirectory("fjklfjsld");
    std::string result = UNTOUCHED_STRING;

    Southsea::ResourcePathUtils::formatPath(mDataDirectory, result);
    ASSERT_EQ(result, Southsea::ResourcePathUtils::ResHeader);

    Southsea::ResourcePathUtils::formatPath("/SomeNonsense", result);
    ASSERT_EQ(result, Southsea::ResourcePathUtils::ResHeader);
}

TEST_F(ResourcePathUtilsTests, parsePathFailsWithoutHeader){
    std::string result = UNTOUCHED_STRING;
    ASSERT_FALSE(Southsea::ResourcePathUtils::parsePath("somethingWithoutAHeader", result));
    ASSERT_EQ(result, UNTOUCHED_STRING);

    //Off by one character
    std::string targetStr = Southsea::ResourcePathUtils::ResHeader.substr(0, Southsea::ResourcePathUtils::ResHeader.size()-1);
    ASSERT_FALSE(Southsea::ResourcePathUtils::parsePath(targetStr + "somethingWithoutAHeader", result));
    ASSERT_EQ(result, UNTOUCHED_STRING);
}

TEST_F(ResourcePathUtilsTests, parsePathConvertsString){
    std::string result = UNTOUCHED_STRING;
    ASSERT_TRUE(Southsea::ResourcePathUtils::parsePath(Southsea::ResourcePathUtils::ResHeader + "s", result));

    //Check the data directory string is now contained within this path.
    ASSERT_TRUE(result.rfind(mDataDirectory) == 0);
}

TEST_F(ResourcePathUtilsTests, removeHeader){
    std::string result = UNTOUCHED_STRING;
    ASSERT_FALSE(Southsea::ResourcePathUtils::removeHeader("noHeader", result));
    ASSERT_EQ(result, UNTOUCHED_STRING);

    static const std::string prevString = "aString";
    ASSERT_TRUE(Southsea::ResourcePathUtils::removeHeader(Southsea::ResourcePathUtils::ResHeader + prevString, result));
    ASSERT_EQ(result, prevString);
}
