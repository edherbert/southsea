#pragma once

#include "filesystem/path.h"
#include <string>
#include <vector>

/**
Provides a cross platform way to perform file system operations.
*/
class FileSystemHelper{
public:

    typedef std::vector<std::pair<std::string, std::string>> cfgList;

    //Get a path to a file which is guaranteed to not exist.
    static std::string getNonExistantFilePath();
    //Get a path to a directory which is guaranteed to not exist.
    static std::string getNonExistantDirectoryPath();

    //Get the absolute path to an empty file.
    static std::string getDummyFilePath();
    //Get the absolute path to an empty directory.
    static std::string getDummyDirectoryPath();

    static bool writeCfgFile(const std::string& path, const cfgList& list);

    static int remove_directory(const char *path);

private:
    //Get the absolute path to a directory where files can be created or operated upon.
    //This directory is platform specific, but will most likely be /tmp on unix like systems.
    static filesystem::path _getWorkspaceDirectory();
};
