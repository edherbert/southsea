#include "FileSystemHelper.h"

#include <fstream>
#include <cassert>

#include <sys/types.h>
#include <dirent.h>

    int FileSystemHelper::remove_directory(const char *path){ //When I move to the c++17 filesystem this can be replaced.
        DIR *d = opendir(path);
        size_t path_len = strlen(path);
        int r = -1;

        if (d){
            struct dirent *p;
            r = 0;
            while (!r && (p=readdir(d))){
                int r2 = -1;
                char *buf;
                size_t len;

                /* Skip the names "." and ".." as we don't want to recurse on them. */
                if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")){
                    continue;
                }

                len = path_len + strlen(p->d_name) + 2;
                buf = (char*)malloc(len);
                if(buf){
                    struct stat statbuf;

                    snprintf(buf, len, "%s/%s", path, p->d_name);
                    if (!stat(buf, &statbuf)){
                        if (S_ISDIR(statbuf.st_mode)){
                            r2 = remove_directory(buf);
                        }
                        else{
                            r2 = unlink(buf);
                        }
                    }

                    free(buf);
                }

                r = r2;
            }

            closedir(d);
        }
        if (!r){
            r = rmdir(path);
        }
        return r;
    }

    std::string FileSystemHelper::getDummyFilePath(){
        filesystem::path workspace = _getWorkspaceDirectory();

        filesystem::path file = workspace / filesystem::path("dummyFile");
        if(file.exists()){
            file.remove_file(); //Even though we want to create a file that exists after this function call, remove one if it does exist incase it's something like a directory.
        }

        std::ofstream ofs(file.str());
        ofs << "dummy file\n";
        ofs.close();

        assert(file.exists() && file.is_file()); //Should definitely exist at this point.

        return file.str();
    }

    std::string FileSystemHelper::getDummyDirectoryPath(){
        filesystem::path workspace = _getWorkspaceDirectory();

        filesystem::path dir = workspace / filesystem::path("dummyDirectory");
        if(dir.exists()){
            remove_directory(dir.str().c_str());
        }

        filesystem::create_directory(dir);

        assert(dir.exists() && dir.is_directory());

        return dir.str();
    }

    std::string FileSystemHelper::getNonExistantFilePath(){
        filesystem::path workspace = _getWorkspaceDirectory();

        filesystem::path nonExistant = workspace / filesystem::path("fileWhichShouldNotExist");
        if(nonExistant.exists()){
            nonExistant.remove_file(); //If for whatever reason.
        }

        return nonExistant.str();
    }

    std::string FileSystemHelper::getNonExistantDirectoryPath(){
        filesystem::path workspace = _getWorkspaceDirectory();

        filesystem::path nonExistant = workspace / filesystem::path("directoryWhichShouldNotExist");
        if(nonExistant.exists()){
            remove_directory(nonExistant.str().c_str());
        }

        return nonExistant.str();
    }

    filesystem::path FileSystemHelper::_getWorkspaceDirectory(){
        filesystem::path topPath("/tmp");

        assert(topPath.exists() && topPath.is_directory()); //Should always exist.

        filesystem::path workSpacePath = topPath / filesystem::path("workspaceDir");
        if(workSpacePath.exists()){ //If it exists from a previous run, get rid of it.
            workSpacePath.remove_file();
        }

        filesystem::create_directory(workSpacePath);

        assert(workSpacePath.is_absolute());

        return workSpacePath;
    }

    bool FileSystemHelper::writeCfgFile(const std::string& path, const cfgList& list){
        filesystem::path filePath(path);

        if(!filePath.parent_path().exists()){
            return false; //The parent path does not exist which means that file can't be created.
        }
        if(filePath.exists()){
            filePath.remove_file();
        }

        std::ofstream ofs(filePath.str());
        for(const auto& i : list){
            //Write out the members of that list to the cfg file.
            ofs << i.first << "\t" << i.second << std::endl;
        }
        ofs.close();

        return true;
    }
