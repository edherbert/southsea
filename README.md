# Southsea

### A level editor for the avEngine.

![Screenshot](/info/screenshot.png "Screenshot")

Southsea was written to provide a bespoke level editor for the [avEngine](http://gitlab.com/edherbert/avEngine).
It implements a number of commonly used features for such a 3D application, including:

 * Real time terrain manipulation, including height and blend map manipulation.
 * A hierarchical scene tree of objects.
 * Full support for undo and redo.
 * Easy access to view the current scene in the engine.
 * A variety of tools and features which make editing easier.

Southsea is intended for use with the avEngine, and thus provides a very integrated experience.
It is able to read in engine projects, rather than having to create its own file format.
It provides native support for reading and writing the appropriate engine data files.
It helps to abstract all the various nuances of development in the engine into a single, streamlined workflow.

As well as this, it provides a very flexible and functional user interface, with many of the expected properties of a 3D content creation application.

Currently Southsea supports Windows and Linux. In future MacOS support should also be possible.

## Sculpting on the terrain

![terrainSculpting](/info/terrainSculpting.gif "terrainSculpting")

## Moving objects about the scene

![objectPlacement](/info/objectPlacement.gif "objectPlacement")
