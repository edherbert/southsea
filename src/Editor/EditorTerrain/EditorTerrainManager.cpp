#include "EditorTerrainManager.h"

#include "Editor/Editor.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "Editor/Action/Actions/Terrain/TerrainMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainBlendMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainSetHeightMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainSmoothMouseAction.h"

namespace Southsea{
    EditorTerrainManager::EditorTerrainManager(Editor* editor)
        : mEditor(editor) {

    }

    EditorTerrainManager::~EditorTerrainManager(){

    }

    void EditorTerrainManager::checkRayIntersect(Ogre::Ray ray, RayIntersectResult& outResult) const{
        std::pair<bool, Ogre::Vector3> result = mEditor->mChunk->getTerrainManager()->checkRayIntersect(ray);
        if(result.first){
            outResult.found = result.first;
            outResult.pos = result.second;
            outResult.terrainId = -1;
            return;
        }

        for(int i = 0; i < mEditor->NUM_OUTER_CHUNKS; i++){
            result = mEditor->mOutsideChunks[i]->getTerrainManager()->checkRayIntersect(ray);
            if(result.first){
                outResult.found = result.first;
                outResult.pos = result.second;
                outResult.terrainId = i;
                return;
            }
        }
    }

    uint16_t EditorTerrainManager::_getEffectedChunks(const RayIntersectResult& rayResult, int boxSize){
        uint16_t result = 0u;
        float size = float(boxSize) / 2;

        Ogre::Vector3 positions[5] = {
            //One for the middle coordinate.
            {0, 0, 0},
            {-size, 0, -size},
            {-size, 0, size},
            {size, 0, -size},
            {size, 0, size}
        };
        for(int i = 0; i < 5; i++){
            Ogre::Vector3 vec = (rayResult.pos + positions[i]) / mEditor->getSlotSize();
            //+1 to remove the negatives, and +1 again so the counting starts at 1 for the bitmask.
            int chunkcoordX = int(std::floor(vec.x)) + 1;
            int chunkcoordY = int(std::floor(vec.z)) + 1;
            int total = chunkcoordX + chunkcoordY*3;
            // //The centre chunk.
            // if(total == 4){
            //     continue;
            // }
            // //Increase the count so the start chunk becomes index 1.
            // //The hole left by the centre chunk has to be filled, so only increase if less than that.
            // if(total < 4)
            //     total += 1;

            // assert(total >= 1 && total < 9);
            // uint16_t foundId = 1u << total-1;
            // result |= foundId;
            result |= 1u << total;
        }

        return result;
    }

    int _getChunkId(int id){
        //TODO temporary until the coordinate system can be fixed.
        switch(id){
            case 1: return 0; break;
            case 3: return 1; break;
            case 4: return -1; break;
            case 5: return 2; break;
            case 7: return 3; break;
            default: return -2; break;
        }
    }

    void EditorTerrainManager::applyHeightDiff(const RayIntersectResult& rayResult, const std::vector<float>& data, int boxSize, float value, TerrainMouseAction* action){
        uint16_t effectedChunks = _getEffectedChunks(rayResult, boxSize);
        action->addEffectedTerrains(effectedChunks);
        for(int i = 0; i < 9; i++){
            if(effectedChunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applyHeightDiff(rayResult.pos, data, boxSize, value, action);
            }
        }
    }

    void EditorTerrainManager::applyBlendDiff(const RayIntersectResult& rayResult, const std::vector<float>& data, int boxSize, float value, int blendLayer, TerrainMouseAction* action){
        uint16_t effectedChunks = _getEffectedChunks(rayResult, boxSize);
        action->addEffectedTerrains(effectedChunks);
        for(int i = 0; i < 9; i++){
            if(effectedChunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applyBlendDiff(rayResult.pos, data, boxSize, value, blendLayer, action);
            }
        }
    }

    void EditorTerrainManager::applySmoothDiff(const RayIntersectResult& rayResult, const std::vector<float>& data, int boxSize, float strength, TerrainMouseAction* action){
        uint16_t effectedChunks = _getEffectedChunks(rayResult, boxSize);
        action->addEffectedTerrains(effectedChunks);
        for(int i = 0; i < 9; i++){
            if(effectedChunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applySmoothDiff(rayResult.pos, data, boxSize, strength, action);
            }
        }
    }

    void EditorTerrainManager::applySetHeight(const RayIntersectResult& rayResult, float height, int boxSize, TerrainMouseAction* action){
        uint16_t effectedChunks = _getEffectedChunks(rayResult, boxSize);
        action->addEffectedTerrains(effectedChunks);
        for(int i = 0; i < 9; i++){
            if(effectedChunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applySetHeight(rayResult.pos, height, boxSize, action);
            }
        }
    }

    void EditorTerrainManager::applyHeightMouseAction(TerrainHeightMouseAction* action, bool performAction){
        for(int i = 0; i < 9; i++){
            uint16_t chunks = action->getEffectedTerrains();
            if(chunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applyHeightMouseAction(action, performAction);
            }
        }
    }

    void EditorTerrainManager::applyBlendMouseAction(TerrainBlendMouseAction* action, bool performAction){
        for(int i = 0; i < 9; i++){
            uint16_t chunks = action->getEffectedTerrains();
            if(chunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applyBlendMouseAction(action, performAction);
            }
        }
    }

    void EditorTerrainManager::applySmoothMouseAction(TerrainSmoothMouseAction* action, bool performAction){
        for(int i = 0; i < 9; i++){
            uint16_t chunks = action->getEffectedTerrains();
            if(chunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applyHeightMouseAction((TerrainHeightMouseAction*)action, performAction);
            }
        }
    }

    void EditorTerrainManager::applySetHeightMouseAction(TerrainSetHeightMouseAction* action, bool performAction){
        for(int i = 0; i < 9; i++){
            uint16_t chunks = action->getEffectedTerrains();
            if(chunks & 1u << i){
                int chunkId = _getChunkId(i);
                if(i == -2) continue;
                mEditor->getChunkById(chunkId)->getTerrainManager()->applyHeightMouseAction((TerrainHeightMouseAction*)action, performAction);
            }
        }
    }
}
