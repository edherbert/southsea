#pragma once

#include "OgreVector3.h"
#include "OgreRay.h"

namespace Southsea{
    class Editor;
    class TerrainMouseAction;
    class TerrainHeightMouseAction;
    class TerrainBlendMouseAction;
    class TerrainSmoothMouseAction;
    class TerrainSetHeightMouseAction;

    struct RayIntersectResult{
        bool found;
        Ogre::Vector3 pos;
        int terrainId;
    };

    /**
    Keeps track of terrain functions of all chunks within the editor.
    */
    class EditorTerrainManager{
    public:
        EditorTerrainManager(Editor* editor);
        ~EditorTerrainManager();

        void checkRayIntersect(Ogre::Ray ray, RayIntersectResult& outResult) const;

        void applyHeightDiff(const RayIntersectResult& rayResult, const std::vector<float>& data, int boxSize, float value, TerrainMouseAction* action);
        void applyBlendDiff(const RayIntersectResult& rayResult, const std::vector<float>& data, int boxSize, float value, int blendLayer, TerrainMouseAction* action);
        void applySmoothDiff(const RayIntersectResult& rayResult, const std::vector<float>& data, int boxSize, float strength, TerrainMouseAction* action);
        void applySetHeight(const RayIntersectResult& rayResult, float height, int boxSize, TerrainMouseAction* action);

        void applyHeightMouseAction(TerrainHeightMouseAction* action, bool performAction);
        void applyBlendMouseAction(TerrainBlendMouseAction* action, bool performAction);
        void applySmoothMouseAction(TerrainSmoothMouseAction* action, bool performAction);
        void applySetHeightMouseAction(TerrainSetHeightMouseAction* action, bool performAction);

    private:
        Editor* mEditor;

        uint16_t _getEffectedChunks(const RayIntersectResult& rayResult, int boxSize);
    };
}
