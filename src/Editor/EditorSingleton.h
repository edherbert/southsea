#pragma once

//This include is here for convenience. It's assumed that anything that wants the singleton wants the editor.
#include "Editor/Editor.h"

namespace Southsea{

    class EditorSingleton{
    public:
        EditorSingleton() = delete;
        ~EditorSingleton() = delete;

        static bool startEditor(const Project& proj, const Map& map);
        static bool stopEditor();

        static bool isEditing() { return mCurrentlyEditing; }

        static Editor* getEditor() { return mEditor; }

    private:
        static Editor* mEditor;
        static bool mCurrentlyEditing;
    };
}
