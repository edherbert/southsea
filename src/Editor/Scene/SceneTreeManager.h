#pragma once

#include "SceneTypes.h"

#include "Editor/Action/Actions/Scene/ObjectInsertionType.h"

#include <vector>
#include <stack>
#include <set>

namespace Ogre{
    class Vector3;
}

namespace Southsea{
    class OgreTreeManager;
    class TreeRearrangeAction;
    class SceneNamespace;
    class SceneSerialiser;

    struct Resource;
    struct ResourceFSEntry;

    /**
    A class to manage the structure of the scene tree.
    This class creates an abstraction of the tree in the form of a vector. This vector is iterated by imgui to draw the actual content for the gui.
    In this sense the tree stored here is just a representation of what the actual tree looks like.
    */
    class SceneTreeManager{
        friend SceneNamespace; //Used for testing.
        friend SceneSerialiser; //Contains a function to populate the scene tree, so needs complete access.
    public:
        SceneTreeManager();
        ~SceneTreeManager();

        void setupTestSceneTree();
        const std::vector<SceneEntry>& getSceneTree() const { return mSceneTree; }

        void renameTreeItem(EntryId id, const std::string& newName);

        struct ObjectInsertData{
            std::string itemName;
            SceneEntryType type;
        };
        typedef std::pair<EntryId, int> InsertResult; //Id of created object, index of created object.
        InsertResult insertObjectIntoTree(EntryId target, ObjectInsertionType type, const ObjectInsertData& data);
        /**
        Delete or recursively delete an object from the tree.
        Populate result index with a positive value to avoid the fuction manually determining where to delete from.

        @returns
        A boolean indicating whether or not the deleted object was an only child.
        If it was this function will also delete the terminator values that would have wrapped around the child.
        */
        bool deleteObjectFromTree(EntryId resultId, int resultIndex = -1);

        std::string getSceneTreeItemName(EntryId id) const;

        void _debugPrint() const;

        void alterTreeItemExpansion(EntryId item, int entryPos, bool expand, bool recursiveExpand);

        /**
        Clear the current information in the tree manager.
        */
        void clearCurrentContent();

        static const SceneEntry TERM;
        static const SceneEntry CHILD;

        /**
        A utility function to check whether an item has children.
        */
        bool itemHasChildren(const std::vector<SceneEntry>& e, int index) const;

        void clearAllSelection();

        bool isEntrySelected(EntryId id) const{
            return mSelectedIds.find(id) != mSelectedIds.end();
        }

        /**
        Set the entry with id to be the only selection.
        */
        void setSingleSelection(EntryId item);
        void notifySelection(EntryId item, int entryIndex, bool ctrlModifier, bool shiftModifier);
        void notifyTerrainSelected();

        /**
        Inserted a selected id into the list.
        This should only be called as part of setup, for instance if reading selected object data from disk.
        */
        void _setSelectedId(EntryId item);

        /**
        Move an entry in the scene tree.

        @param customDestinationIndex
        Used to specify what the user thinks the destination index is.
        This is used in the undo-redo procedure. Otherwise the function will determine this automatically.
        */
        int moveEntry(EntryId target, EntryId destination, ObjectInsertionType type, int customDestinationIndex = -1);

        /**
        Populate a scene re-arrange event with the information it requires.

        @returns
        True or false depending on whether the event is valid based on the input provided.
        */
        bool populateReArrangeEvent(TreeRearrangeAction& action, EntryId destination, ObjectInsertionType type);

        /**
        The actual id of a parent node will be different depending on the insertion type.
        For instance, in the case of the above or below insertion types, the actual parent destination will be whatever the parent of the target is.
        This function will resolve the actual parent of a node given the insertion type.
        */
        EntryId getTrueDestination(EntryId target, ObjectInsertionType type) const;

        /**
        Returns the id of the parent of the item at idx
        */
        EntryId getIdOfParent(int idx) const;

        /**
        Populate a vector with the contents of a scene node.
        If that node has children, the vector will be populated with those entries, as well as their terminator values.

        @returns
        The index of the id in the tree list.
        */
        int populateVectorWithNode(EntryId id, std::vector<SceneEntry>& target) const;

        /**
        Insert the contents of a vector into the scene tree. The contents between the start and end of the list will be inserted at the index idx.
        */
        void insertVectorItems(std::vector<SceneEntry>& vec, int idx, int start, int end, bool recreateTerminators, bool replaceIds = false);

        /**
        Insert a list of vector items as children of the provided item.
        @returns True or false depending on whether terminators were created as part of this insertion.
        */
        bool insertVectorItemsIntoItem(std::vector<SceneEntry>& vec, EntryId item, int start, int end);

        /**
        Change the visibility of an item in the tree.
        This will traverse upwards in the tree to determine if any of the parents are invisible.
        If they are the visibility variable will still be changed, although the function will return a different bool.

        @returns
        The value that the visibility should be changed to.
        If one of this items parents is invisible the returned value will be false.
        Otherwise the value of visible will be returned.
        */
        bool setItemVisibility(EntryId id, bool visibile, OgreTreeManager* ogreMan);

        /**
        Set the current selection to be just the provided id.
        This will perform a search of the tree.
        */
        void setSelectionById(EntryId item);

        const std::string& getNameForEntry(EntryId id);

        EntryId _getEntryId();
        void _returnEntryId(EntryId id);

        const std::set<EntryId>& getSelectedIds() const { return mSelectedIds; }

        /**
        Reduce the selected items down for resource dragging.
        For instance, if an item is selected, and it has children which are also selected, the children will be removed from the list.
        The selection of the parent encompasses the child, and this function reduces the list down.
        */
        std::set<EntryId> getReducedSelection() const;

        bool isTerrainSelected() const { return mTerrainSelected; }
        /**
        Set the terrain to be selected
        This function should only be called at setup, for instance as part of a meta file load.
        */
        void _setTerrainSelected(bool selected);

        /**
        Insert an object into the tree using an object insertion action.
        This function chooses which node to insert into based on the current selection.
        */
        void insertObjectIntoTreeFromSelection(const Resource& res, const ResourceFSEntry& resFS, const Ogre::Vector3& pos) const;
        void insertSceneTreeFileIntoTreeFromSelection(const std::string& file, const Ogre::Vector3& pos) const;

        size_t getSelectedCount() const { return mSelectedIds.size(); }

        /**
        Find and return the EntryId of the first selected object in the list.
        If none are selected an invalid id will be returned.
        */
        EntryId getFirstSelection() const;

        /**
        Loop through all the currently selected items and check if they exist in the tree.
        */
        void removeInvalidSelections();

        /**
        Generate an object deletion event for the current selection and push it to the action stack.
        */
        void deleteCurrentSelection();

        /**
        Returns true if the provided object is an only child.
        */
        bool isObjectOnlyChild(EntryId targetObject) const;

        void selectAll();

        void saveCurrentSelection();

    private:
        std::vector<SceneEntry> mSceneTree;

        std::set<EntryId> mSelectedIds;

        static const EntryId _baseId = 2;
        EntryId mUsedIds = _baseId; //0 is reserved for the child and terminator entries. 1 is reserved for the root node.
        std::stack<EntryId> mRecycledIds;

        EntryId mMostRecentSelection = 0;
        bool mTerrainSelected = false;

        int _recursiveDeleteInTree(int index);

        void _selectedItemsChanged();
        void _clearAllSelection();

        //Return the index of the parent for the provided entry.
        int _getParentIndexOfEntry(int entry) const;

        int _findEntryIdIndexInTree(EntryId item) const;

        bool _isParentSelected(int id) const;

    public:
        static int utilFindEntryIdIndexInTree(EntryId item, const std::vector<SceneEntry>& list);
        static int utilGetParentIndexOfEntry(int entry, const std::vector<SceneEntry>& list);
        static int utilGetTerminatorForChild(int start, const std::vector<SceneEntry>& list);
    };
}
