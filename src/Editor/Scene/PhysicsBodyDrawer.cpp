#include "PhysicsBodyDrawer.h"

#include "OgreSceneManager.h"
#include "OgreRoot.h"
#include "OgreHlmsUnlit.h"
#include "OgreHlmsUnlitDatablock.h"
#include "OgreItem.h"

#include "Editor/Scene/SceneTypeMasks.h"

namespace Southsea{

    //Some of these are there for the future case.
    const char* PhysicsBodyDrawer::mDbNames[NUM_DATABLOCKS] = {
        "Southsea/PhysicsRegularVis",
        "Southsea/PhysicsFadedVis",
        "Southsea/PhysicsThirdVis"
    };

    PhysicsBodyDrawer::PhysicsBodyDrawer(){
        for(int i = 0; i < NUM_DATABLOCKS; i++)
            mDatablocks[i] = 0;
    }

    PhysicsBodyDrawer::~PhysicsBodyDrawer(){
        _destroyDatablocks();
    }

    void PhysicsBodyDrawer::initialise(Ogre::SceneManager* sceneManager){
        mSceneManager = sceneManager;

        _setupDatablocks();
    }

    void PhysicsBodyDrawer::_destroyDatablocks(){
        Ogre::Hlms* hlms = Ogre::Root::getSingletonPtr()->getHlmsManager()->getHlms(Ogre::HLMS_UNLIT);
        for(int i = 0; i < sizeof(mDbNames) / sizeof(const char*); i++){
            assert(mDatablocks[i]);
            hlms->destroyDatablock(mDbNames[i]);
        }
    }

    void PhysicsBodyDrawer::_setupDatablocks(){
        Ogre::Hlms* hlms = Ogre::Root::getSingletonPtr()->getHlmsManager()->getHlms(Ogre::HLMS_UNLIT);
        Ogre::HlmsUnlit* unlit = dynamic_cast<Ogre::HlmsUnlit*>(hlms);


        // Ogre::HlmsMacroblock mm;
        // mm.mDepthCheck = false;

        for(int i = 0; i < NUM_DATABLOCKS; i++){
            Ogre::HlmsDatablock* block = unlit->createDatablock(mDbNames[i], mDbNames[i], Ogre::HlmsMacroblock(), Ogre::HlmsBlendblock(), Ogre::HlmsParamVec(), false);
            Ogre::HlmsUnlitDatablock* unlitBlock = dynamic_cast<Ogre::HlmsUnlitDatablock*>(block);
            unlitBlock->setUseColour(true);
            unlitBlock->setColour(Ogre::ColourValue(0.4, 0.4, 0.9));

            mDatablocks[i] = block;
        }
    }

    void PhysicsBodyDrawer::createShapeForNode(Ogre::SceneNode* node, PhysicsShapeType shapeType){
        assert(mShapeNodes.find(node) == mShapeNodes.end());

        const char* targetName = _getShapeName(shapeType);
        Ogre::Item *item = mSceneManager->createItem(targetName, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
        node->attachObject((Ogre::MovableObject*)item);

        item->setDatablock(mDatablocks[0]);
        item->setVisibilityFlags(PHYSICS_OBJECT_MASK);

        mShapeNodes.insert(node);
    }

    void PhysicsBodyDrawer::destroyShapeForNode(Ogre::SceneNode* node){
        auto shapeNodesIt = mShapeNodes.find(node);
        assert(shapeNodesIt != mShapeNodes.end());

        //When it comes time to destroy a mesh (movable object) a bit more work needs to be done, as you have to check it's actually there.
        Ogre::SceneNode::ObjectIterator it = node->getAttachedObjectIterator();
        if(it.hasMoreElements()){
            Ogre::MovableObject* obj = it.getNext();
            mSceneManager->destroyMovableObject(obj);
        }
        //At the moment a mesh object should only have the one mesh attached to it, so by this point there should be no more.
        assert(!it.hasMoreElements());

        mShapeNodes.erase(shapeNodesIt);
    }

    const char* PhysicsBodyDrawer::_getShapeName(PhysicsShapeType type){
        switch(type){
            case PhysicsShapeType::box: return "FullLineBox";
            case PhysicsShapeType::sphere: return "LineSphere";
            case PhysicsShapeType::capsule: return "FullLineBox";
            default: assert(false); break;
        }
    }
}
