#pragma once

#include <iostream>
#include "OgreSceneManager.h"

#include "Editor/Scene/SceneTypes.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/SceneCopyData.h"
#include "Editor/Scene/OgreTreeManager.h"

//Taken from the avEngine
#include "World/Slot/Recipe/AvScene/AvSceneFileParser.h"

namespace Southsea{
    //Interface to export to the scene tree.
    class SceneFileSceneTreeParserInterface : public AV::AVSceneFileParserInterface{
    private:
        const std::shared_ptr<SceneTreeManager> mSceneMan;
        const std::shared_ptr<OgreTreeManager> mOgreMan;
        const std::shared_ptr<TreeObjectManager> mTreeMan;

    public:
        std::vector<SceneEntry> mCopiedSceneEntries;
        std::vector<CopiedSelectionInfo> mEntrySectionList;
        std::vector<ObjectData*> mObjectData;
        std::vector<OgreTreeManager::TreeNodeCreationData> mTreeCreationData;

        std::stack<int> mPrevParent;
        int mPrevNode;

        SceneFileSceneTreeParserInterface(const std::shared_ptr<SceneTreeManager> sceneMan, const std::shared_ptr<OgreTreeManager> ogreMan, const std::shared_ptr<TreeObjectManager> treeMan)
            : mSceneMan(sceneMan),
            mOgreMan(ogreMan),
            mTreeMan(treeMan),
            mPrevNode(-1) {

            mPrevParent.push(-1);
        }

        void reachedEndForParent(int parent){
            if(parent == mPrevParent.top()){
                mCopiedSceneEntries.push_back(SceneTreeManager::TERM);
                mPrevParent.pop();
            }
        }

        void logError(const char* message){
            std::cerr << message << std::endl;
        }
        void log(const char* message){
            std::cout << message << std::endl;
        }

        int createEmpty(int parent, const Ogre::Vector3& pos, const Ogre::Vector3& scale, const Ogre::Quaternion& orientation){
            if(parent != mPrevParent.top()){
                mCopiedSceneEntries.push_back(SceneTreeManager::CHILD);
                mPrevParent.push(parent);
            }

            EntryId newId = mSceneMan->_getEntryId();
            mCopiedSceneEntries.push_back({newId, "name", SceneEntryType::empty, false, true});
            mObjectData.push_back(new ObjectData());
            mTreeCreationData.push_back({pos, scale, orientation});

            mPrevNode = newId;

            //NOTE unsigned to signed conversion.
            return newId;
        }
        int createMesh(int parent, const char* name, const char* mesh, const Ogre::Vector3& pos, const Ogre::Vector3& scale, const Ogre::Quaternion& orientation){
            EntryId newId = mSceneMan->_getEntryId();
            mCopiedSceneEntries.push_back({newId, name, SceneEntryType::mesh, false, true});
            MeshObjectData* meshData = new MeshObjectData();
            meshData->meshName = mesh;
            mObjectData.push_back(meshData);
            mTreeCreationData.push_back({pos, scale, orientation});

            return 0;
        }
    };



};

