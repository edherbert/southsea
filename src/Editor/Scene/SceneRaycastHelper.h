#pragma once

#include <vector>
#include <string>
#include "Editor/Scene/SceneTypes.h"

namespace Ogre{
    class SceneManager;
    class Camera;
    class RaySceneQuery;
}

namespace Southsea{

    struct RaycastListEntry{
        EntryId i;
        std::string s;
    };

    class SceneRaycastHelper{
    public:
        SceneRaycastHelper() = delete;
        ~SceneRaycastHelper() = delete;

        static void populateSceneManager(Ogre::SceneManager* sceneManager, Ogre::Camera* camera);

        static bool castRayInScene(float mouseX, float mouseY);
        static bool castRayToList(float mouseX, float mouseY, std::vector<RaycastListEntry>& outList);

    private:
        static Ogre::SceneManager* mSceneManager;
        static Ogre::Camera* mCamera;
        static Ogre::RaySceneQuery* mSceneQuery;
    };


}
