#include "CopyStateManager.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Action/Actions/Scene/ScenePasteAction.h"

namespace Southsea{
    CopyStateManager::CopyStateManager(){

    }

    CopyStateManager::~CopyStateManager(){

    }

    void CopyStateManager::copyCurrentSelection(){
        _clearAllState();
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();

        std::set<EntryId> selected = sceneMan->getReducedSelection();
        if(selected.empty()) return;
        for(EntryId i : selected){
            int startIdx = mCopiedSceneEntries.size();
            int idx = sceneMan->populateVectorWithNode(i, mCopiedSceneEntries);
            int finalSize = mCopiedSceneEntries.size();

            int objStart = mObjectData.size();
            ogreMan->populateVectorWithNodeData(i, mObjectData, mTreeCreationData, treeMan.get());
            int objEnd = mObjectData.size();
            EntryId parentEntry = ogreMan->getParentOfEntry(i);

            mEntrySectionList.push_back({idx, startIdx, finalSize, objStart, objEnd, parentEntry});
        }
    }

    void CopyStateManager::populatePasteActionForDuplicate(ScenePasteAction* a){
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();

        //Just take the first selected object and populate according to that.
        EntryId selected = sceneMan->getFirstSelection();
        assert(selected != INVALID_ID);

        int startIdx = a->mCopiedSceneEntries.size();
        int idx = sceneMan->populateVectorWithNode(selected, a->mCopiedSceneEntries);
        int finalSize = a->mCopiedSceneEntries.size();

        int objStart = a->mObjectData.size();
        ogreMan->populateVectorWithNodeData(selected, a->mObjectData, a->mTreeCreationData, treeMan.get());
        int objEnd = a->mObjectData.size();
        EntryId parentEntry = ogreMan->getParentOfEntry(selected);

        a->mEntrySectionList.push_back({idx, startIdx, finalSize, objStart, objEnd, parentEntry});

        a->mDestination = parentEntry;
        a->mPopulated = true;
    }

    void CopyStateManager::populatePasteAction(ScenePasteAction* a){
        EntryId targetId = EditorSingleton::getEditor()->getSceneTreeManager()->getFirstSelection();
        if(targetId == INVALID_ID){
            targetId = ROOT_NODE;
        }
        a->populate(targetId, mCopiedSceneEntries, mEntrySectionList, mObjectData, mTreeCreationData);
    }

    void CopyStateManager::_clearAllState(){
        mCopiedSceneEntries.clear();
        for(ObjectData* data : mObjectData){
            delete data;
        }
        mObjectData.clear();
        mEntrySectionList.clear();
        mTreeCreationData.clear();
    }
}
