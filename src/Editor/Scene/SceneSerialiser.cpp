#include "SceneSerialiser.h"

#include "SceneTypes.h"
#include "SceneTreeManager.h"
#include "OgreTreeManager.h"
#include "TreeObjectManager.h"
#include <regex>
#include <iostream>
//Parser from the avEngine.
#include "World/Slot/Recipe/CollisionObjectSceneParser.h"
#include "World/Physics/Worlds/CollisionWorldUtils.h"

#include "OgreStringConverter.h"

#include "OgreSceneNode.h"

#include "filesystem/path.h"

namespace Southsea{
    SceneSerialiser::SceneSerialiser(){

    }

    SceneSerialiser::~SceneSerialiser(){

    }

    inline void _outVec3(const Ogre::Vector3& vec, std::ofstream& file){
        file << vec.x << " " << vec.y << " " << vec.z << std::endl;
    }

    inline void _outQuat(const Ogre::Quaternion& quat, std::ofstream& file){
        file << quat.w << " " <<    quat.x << " " << quat.y << " " << quat.z << std::endl;
    }

    bool SceneSerialiser::serialise(const SceneTreeManager& sceneMan, const OgreTreeManager& ogreMan, const TreeObjectManager& treeMan, const std::string& outDirectory){
        const filesystem::path outDirPath(outDirectory);
        const std::vector<SceneEntry>& sceneTree = sceneMan.getSceneTree();

        {
            const filesystem::path sceneTreeFilePath = outDirPath / filesystem::path("sceneTree.txt");

            std::ofstream myfile;
            myfile.open(sceneTreeFilePath.str());
            if(!myfile.is_open()) return false;

            for(auto it = sceneTree.begin() + 1; it < sceneTree.end() - 1; it++){
                const SceneEntry& e = *it;
                if(e.type == SceneEntryType::treeChild || e.type == SceneEntryType::treeTerminator) {
                    char c = e.type == SceneEntryType::treeChild ? '1' : '2';
                    myfile << c << " 0 0 0" << std::endl;
                    continue;
                }
                EntryId id = e.id;

                Ogre::SceneNode* node = ogreMan.getNodeById(id);

                bool hasPosition = node->getPosition() != Ogre::Vector3::ZERO;
                bool hasScale = node->getScale() != Ogre::Vector3(1, 1, 1);
                bool hasOrientation = node->getOrientation() != Ogre::Quaternion::IDENTITY;

                char typeChar = objectTypeToChar(e.type);
                //assert(typeChar != '0');
                //Write header info.
                //0 means this is not a terminator.
                myfile << typeChar << " " << hasPosition << " " << hasScale << " " << hasOrientation << std::endl;

                if(e.type == SceneEntryType::physicsShape){
                    const ObjectData* data = treeMan.getDataObject(e.id);
                    const PhysicsShapeObjectData* objectShapeData = static_cast<const PhysicsShapeObjectData*>(data);
                    myfile << _physicsShapeToChar(objectShapeData->shapeType) << "\n";
                }

                if(hasPosition){
                    _outVec3(node->getPosition(), myfile);
                }
                if(hasScale){
                    _outVec3(node->getScale(), myfile);
                }
                if(hasOrientation){
                    _outQuat(node->getOrientation(), myfile);
                }
            }

            myfile.close();
        }

        {
            const filesystem::path sceneDataFilePath = outDirPath / filesystem::path("southseaSceneData.txt");

            std::ofstream file;
            file.open(sceneDataFilePath.str());
            if(!file.is_open()) return false;

            for(auto it = sceneTree.begin() + 1; it < sceneTree.end() - 1; it++){
                const SceneEntry& e = *it;
                if(e.type == SceneEntryType::treeChild || e.type == SceneEntryType::treeTerminator) continue;

                file << e.name << std::endl;
                file << e.expanded << " " << e.visible << std::endl;
            }

            file.close();
        }

        {
            //Write out meshes.
            filesystem::path staticMeshFilePath = outDirPath / filesystem::path("staticMeshes.txt");

            std::ofstream file;
            file.open(staticMeshFilePath.str());
            if(!file.is_open()) return false;

            bool meshFound = false;
            for(auto it = sceneTree.begin() + 1; it < sceneTree.end() - 1; it++){
                const SceneEntry& e = *it;
                if(e.type != SceneEntryType::mesh) continue;
                const ObjectData* data = treeMan.getDataObject(e.id);
                assert(data->type() == ObjectType::mesh);
                const MeshObjectData* meshData = static_cast<const MeshObjectData*>(data);

                file << meshData->meshName << std::endl;
                file << meshData->meshMaterial << std::endl;

                meshFound = true;
            }

            file.close();
            if(!meshFound){
                //In this case we don't have anything to write, so the file can just be removed entirely.
                staticMeshFilePath.remove_file();
            }
        }

        {
            //Write out static physics shapes.
            filesystem::path staticMeshFilePath = outDirPath / filesystem::path("bodies.txt");

            std::ofstream file;
            file.open(staticMeshFilePath.str());
            if(!file.is_open()) return false;

            /**
            The file type requires you to maintain a list of shapes, which are written to the top.
            Later shape entries are able to specify which shapes they use, and this means sharing is possible.
            So here I have to check the scale of each shape and determine whether they share anything.
            To be honest, this isn't all that efficient, but I plan to re-design this whole system at a later date.
            */

            std::vector<ShapeEntryData> shapeData;
            std::vector<PhysicsShapeData> entryData;

            bool anyShapeFound = false;
            for(auto it = sceneTree.begin() + 1; it < sceneTree.end() - 1; it++){
                const SceneEntry& e = *it;
                if(e.type != SceneEntryType::physicsShape) continue;
                const ObjectData* data = treeMan.getDataObject(e.id);
                assert(data->type() == ObjectType::physicsShape);

                //NOTE this isn't very efficient, as I call this above. In future have just a single loop over the tree and call this once.
                Ogre::SceneNode* node = ogreMan.getNodeById(e.id);

                const PhysicsShapeObjectData* objectShapeData = static_cast<const PhysicsShapeObjectData*>(data);
                ShapeTypeInt shapeType = (ShapeTypeInt)objectShapeData->shapeType;

                FoundShapeInfo info;
                _findAndProcessShape(&shapeData, shapeType, node, &info);
                entryData.push_back({info.pos, info.orientation, info.shapeIndex});

                anyShapeFound = true;
            }

            if(anyShapeFound){
                for(const ShapeEntryData& e : shapeData){
                    file << e.shapeType << "\n";
                    _outVec3(e.scale, file);
                }
                file << "==\n";

                for(const PhysicsShapeData& e : entryData){
                    file << e.shapeId << "\n";
                    _outVec3(e.position, file);
                    _outQuat(e.orientation, file);
                }
            }else{
                assert(shapeData.empty());
                assert(entryData.empty());
            }

            file.close();
            if(!anyShapeFound){
                //No shapes were found, so get rid of the file.
                staticMeshFilePath.remove_file();
            }
        }

        //Write out collision objects.
        {
            filesystem::path collisionObjectsFilePath = outDirPath / filesystem::path("collisionObjects.txt");

            std::ofstream file;
            file.open(collisionObjectsFilePath.str());
            if(!file.is_open()) return false;

            std::vector<ShapeEntryData> shapeData;
            std::vector<CollisionObjectPositionData> entryData;
            std::vector<ScriptEntryData> scriptData;
            std::vector<ScriptEntryData> closureData;
            std::vector<ScriptAndClosureId> scriptAndClosureEntries;
            std::vector<CollisionObjectData> collisionObjectData;

            bool anyShapeFound = false;
            for(auto it = sceneTree.begin() + 1; it < sceneTree.end() - 1; it++){
                const SceneEntry& e = *it;
                if(e.type != SceneEntryType::collisionSender) continue;
                const ObjectData* data = treeMan.getDataObject(e.id);
                assert(data->type() == ObjectType::collisionSender);

                const CollisionSenderObjectData* objectData = static_cast<const CollisionSenderObjectData*>(data);
                Ogre::SceneNode* node = ogreMan.getNodeById(e.id);

                ShapeTypeInt shapeType = (ShapeTypeInt)objectData->shapeType;

                FoundShapeInfo info;
                _findAndProcessShape(&shapeData, shapeType, node, &info);

                //Find and populate the scripts.
                VecIdx targetScript = _findAndProcessScript(&scriptData, objectData->script.name);
                //Find and populate the closures.
                VecIdx targetClosure = _findAndProcessScript(&closureData, objectData->closureName);

                //Find and populate the script and closure ids.
                VecIdx targetScriptAndClosure = _findAndProcessScriptAndClosureId(&scriptAndClosureEntries, {targetScript, targetClosure});

                VecIdx targetCollisionData = _findAndProcessCollisionData(&collisionObjectData, objectData);

                entryData.push_back({info.pos, info.shapeIndex, targetScriptAndClosure, targetCollisionData});

                anyShapeFound = true;
            }

            if(anyShapeFound){
                for(const ShapeEntryData& e : shapeData){
                    file << e.shapeType << "\n";
                    _outVec3(e.scale, file);
                }
                file << "==\n";

                for(const ScriptEntryData& e : scriptData){
                    file << e.path << "\n";
                }
                file << "==\n";
                for(const ScriptEntryData& e : closureData){
                    file << e.path << "\n";
                }

                file << "==\n";
                for(const ScriptAndClosureId& e : scriptAndClosureEntries){
                    file << e.scriptId << " " << e.closureId << "\n";
                }

                file << "==\n";
                for(const CollisionObjectData& e : collisionObjectData){
                    //TODO right now I don't have a value for the world.
                    file << 0 << "\n";
                    for(int i = 0; i < 7; i++)
                        file << e.targetValues[i];
                    file << "\n";
                    for(int i = 0; i < 3; i++)
                        file << e.eventValues[i];
                    file << "\n";
                    file << e.id << "\n";
                }
                file << "==\n";

                for(const CollisionObjectPositionData& e : entryData){
                    file << e.shapeId << "\n";
                    file << e.scriptId << "\n";
                    file << e.dataId << "\n";
                    _outVec3(e.position, file);
                    _outQuat(Ogre::Quaternion::IDENTITY, file);
                }
            }else{
                assert( shapeData.empty() && entryData.empty() && scriptData.empty() && closureData.empty() && scriptAndClosureEntries.empty() && collisionObjectData.empty() );
            }

            file.close();
            if(!anyShapeFound){
                collisionObjectsFilePath.remove_file();
            }
        }

        //Write out data points
        {
            filesystem::path dataPointsFilePath = outDirPath / filesystem::path("dataPoints.txt");

            std::ofstream file;
            file.open(dataPointsFilePath.str());
            if(!file.is_open()) return false;

            bool anyPointFound = false;
            for(auto it = sceneTree.begin() + 1; it < sceneTree.end() - 1; it++){
                const SceneEntry& e = *it;
                if(e.type != SceneEntryType::dataPoint) continue;
                const ObjectData* data = treeMan.getDataObject(e.id);
                assert(data->type() == ObjectType::dataPoint);
                const DataPointObjectData* pointData = dynamic_cast<const DataPointObjectData*>(data);

                Ogre::SceneNode* node = ogreMan.getNodeById(e.id);
                const Ogre::Vector3& np = node->_getDerivedPositionUpdated();

                file << np.x << " " << np.y << " " << np.z << std::endl;
                file << static_cast<int>(pointData->dataType) << " ";
                file << static_cast<int>(pointData->subDataType) << " ";
                file << pointData->userData << std::endl;

                anyPointFound = true;
            }

            file.close();
            if(!anyPointFound){
                dataPointsFilePath.remove_file();
            }
        }

        return true;
    }

    inline bool _compareArray(const bool* a, const bool* b, size_t arraySize){
        for(size_t i = 0; i < arraySize; i++){
            if( *(a+i) != *(b+i) ) return false;
        }
        return true;
    }

    SceneSerialiser::VecIdx SceneSerialiser::_findAndProcessCollisionData(std::vector<CollisionObjectData>* scriptData, const CollisionSenderObjectData* data) const{
        for(VecIdx i = 0; i < scriptData->size(); i++){
            const CollisionObjectData& sData = (*scriptData)[i];
            if(!_compareArray(&(data->targetValues[0]), &(sData.targetValues[0]), 7)) continue;
            if(!_compareArray(&(data->eventValues[0]), &(sData.eventValues[0]), 3)) continue;
            if(data->id != sData.id) continue;

            //At this point a match was found.
            return i;
        }

        //Otherwise, insert a new value.
        VecIdx retVal = scriptData->size();
        CollisionObjectData newData;
        //{data->targetValues, data->eventValues, data->id}
        memcpy(newData.targetValues, data->targetValues, sizeof(bool)*7);
        memcpy(newData.eventValues, data->eventValues, sizeof(bool)*3);
        newData.id = data->id;
        scriptData->push_back(newData);

        return retVal;
    }

    SceneSerialiser::VecIdx SceneSerialiser::_findAndProcessScriptAndClosureId(std::vector<ScriptAndClosureId>* scriptData, const ScriptAndClosureId& targetId) const{
        SceneSerialiser::VecIdx retVal = 0;
        for(const ScriptAndClosureId& i : *scriptData){
            if(i.scriptId == targetId.scriptId && i.closureId == targetId.closureId){
                //A match was found, meaning this combination of closure and script already exists.
                return retVal;
            }
            retVal++;
        }

        //No match was found, so create a new one
        scriptData->push_back(targetId);
        //retVal is set to the correct value in the loop.
        return retVal;
    }

    SceneSerialiser::VecIdx SceneSerialiser::_findAndProcessScript(std::vector<ScriptEntryData> *scriptData, const std::string& scriptPath) const{
        const Ogre::IdString id(scriptPath);
        for(VecIdx i = 0; i < scriptData->size(); i++){
            //Check for a match.
            if( (*scriptData)[i].mHash == id) return i;
        }

        VecIdx retVal = scriptData->size();
        scriptData->push_back({id, scriptPath});

        return retVal;
    }

    void SceneSerialiser::_findAndProcessShape(std::vector<ShapeEntryData> *shapeData, ShapeTypeInt shapeType, Ogre::SceneNode* node, FoundShapeInfo* outInfo) const{
        const Ogre::Vector3& nodeScale = node->_getDerivedScaleUpdated();
        const Ogre::Vector3& nodePosition = node->_getDerivedPositionUpdated();
        const Ogre::Quaternion& nodeOrientation = node->_getDerivedOrientationUpdated();

        VecIdx targetShape = 0;
        bool shapeFound = false;
        for(; targetShape < shapeData->size(); targetShape++){
            const ShapeEntryData& d = (*shapeData)[targetShape];
            //See if that shape exists.
            if(d.shapeType == shapeType && d.scale == nodeScale){
                shapeFound = true;
                break;
            }
        }

        if(!shapeFound){
            //The shape does not exist, insert it now.
            targetShape = shapeData->size();
            shapeData->push_back({nodeScale, shapeType});
        }
        *outInfo = {targetShape, nodePosition, nodeOrientation};
    }

    bool SceneSerialiser::deserialise(SceneTreeManager& treeMan, OgreTreeManager& ogreMan, TreeObjectManager& objectMan, const std::string& targetDirectory){
        AV::CollisionWorldChunkData collisionChunkData;
        bool result = _deserialise(treeMan, ogreMan, objectMan, targetDirectory, collisionChunkData);
        //Delete the memory allocated by the collision object parser.
        AV::CollisionObjectSceneParser::_clearRecipeData(collisionChunkData);
        if(!result){
            _clearSceneTree(treeMan);
            std::cout << "Warning! Scene Tree deserialisation failed, so the scene tree will be presented as empty." << '\n';
            std::cout << mSceneDeserialisationErrorReason << std::endl;
            return false;
        }

        return true;
    }

    bool SceneSerialiser::_deserialise(SceneTreeManager& treeMan, OgreTreeManager& ogreMan, TreeObjectManager& objectMan, const std::string& targetDirectory, AV::CollisionWorldChunkData& collisionChunkData){
        const filesystem::path targetDirectoryPath(targetDirectory);
        mCurrentReadCollisionObject = 0;

        SceneEntry rootEntry = SceneTreeManager::CHILD;
        rootEntry.id = ROOT_NODE;
        treeMan.mSceneTree.push_back(rootEntry);

        const filesystem::path sceneTreeFilePath = targetDirectoryPath / filesystem::path("sceneTree.txt");
        const filesystem::path sceneDataFilePath = targetDirectoryPath / filesystem::path("southseaSceneData.txt");
        const filesystem::path staticMeshFilePath = targetDirectoryPath / filesystem::path("staticMeshes.txt");
        const filesystem::path collisionObjectsFilePath = targetDirectoryPath / filesystem::path("collisionObjects.txt");
        const filesystem::path dataPointsFilePath = targetDirectoryPath / filesystem::path("dataPoints.txt");

        std::string line;
        std::ifstream myfile(sceneTreeFilePath.str());
        std::ifstream sceneDataFile(sceneDataFilePath.str());
        std::ifstream staticMeshFile(staticMeshFilePath.str());
        std::ifstream dataPointsFile(dataPointsFilePath.str());

        if(collisionObjectsFilePath.exists() && collisionObjectsFilePath.is_file()){
            AV::CollisionObjectSceneParser objectParser;
            bool result = objectParser.parse(collisionObjectsFilePath.str(), collisionChunkData);
            if(!result){
                mSceneDeserialisationErrorReason = "Unable to parse the collision objects file.";
                mSceneDeserialisationErrorReason.append(objectParser.getFailureReason());
                return false;
            }
        }

        if(!myfile.is_open()){
            mSceneDeserialisationErrorReason = "sceneTree.txt file does not exist";
            return false;
        }
        if(!_processFile(treeMan, ogreMan, objectMan, myfile, sceneDataFile, staticMeshFile, dataPointsFile, line, ogreMan.getRootSceneNode(), &collisionChunkData)){
            return false;
        }
        //TODO close the files if false is returned.
        myfile.close();
        sceneDataFile.close();
        staticMeshFile.close();

        //If the procedure succeded we still need to push the root terminator.
        treeMan.mSceneTree.push_back(SceneTreeManager::TERM);

        return true;
    }

    void SceneSerialiser::_clearSceneTree(SceneTreeManager& treeMan){
        treeMan.mSceneTree.clear();

        SceneEntry rootEntry = SceneTreeManager::CHILD;
        rootEntry.id = ROOT_NODE;
        treeMan.mSceneTree.push_back(rootEntry);

        treeMan.mSceneTree.push_back(SceneTreeManager::TERM);
    }

    bool SceneSerialiser::_processFile(SceneTreeManager& treeMan, OgreTreeManager& ogreMan, TreeObjectManager& objectMan, std::ifstream& file, std::ifstream& sceneDataFile, std::ifstream& staticMeshesFile, std::ifstream& dataPointsFile, std::string& line, Ogre::SceneNode* currentParent, AV::CollisionWorldChunkData* collisionData){
        Ogre::SceneNode* previousCreatedNode = 0;

        PhysicsShapeType physicsShapeType;
        std::string nameString;
        while(getline(file, line)){
            HeaderData data;
            if(!_readHeaderInfo(line, data)) {
                mSceneDeserialisationErrorReason = "Error reading header data.";
                return false;
            }

            {
                if(data.type == SceneEntryType::treeTerminator) {
                    treeMan.mSceneTree.push_back(SceneTreeManager::TERM);
                    return true;
                }
                else if(data.type == SceneEntryType::treeChild){
                    treeMan.mSceneTree.push_back(SceneTreeManager::CHILD);
                    if(!_processFile(treeMan, ogreMan, objectMan, file, sceneDataFile, staticMeshesFile, dataPointsFile, line, previousCreatedNode, collisionData)) return false;
                    continue;
                }
            }

            OgreTreeManager::TreeNodeCreationData creationData = OgreTreeManager::ZERO;

            if(data.type == SceneEntryType::physicsShape){
                if(!getline(file, line)){
                    mSceneDeserialisationErrorReason = "Error reading physics shape.";
                    return false;
                }
                if(!_readPhysicsShapeLine(line, &physicsShapeType)){
                    mSceneDeserialisationErrorReason = "Error reading physics shape.";
                    return false;
                }
            }

            if(data.hasPosition){
                if(!getline(file, line)){
                    mSceneDeserialisationErrorReason = "Error reading position data.";
                    return false;
                }
                creationData.position = Ogre::StringConverter::parseVector3(line); //defaults to 0, 0, 0
            }
            if(data.hasScale){
                if(!getline(file, line)){
                    mSceneDeserialisationErrorReason = "Error reading scale data.";
                    return false;
                }
                creationData.scale = Ogre::StringConverter::parseVector3(line);
            }
            if(data.hasOrientation){
                if(!getline(file, line)){
                    mSceneDeserialisationErrorReason = "Error reading orientation data.";
                    return false;
                }
                creationData.orientation = Ogre::StringConverter::parseQuaternion(line);
            }

            bool fileOpen = false;
            bool expanded = false;
            bool visible = true;
            if(sceneDataFile.is_open()){
                if(!getline(sceneDataFile, nameString) || !getline(sceneDataFile, line)){
                    mSceneDeserialisationErrorReason = "Error reading values from southseaSceneData.txt file.";
                    return false;
                }

                populateBool(line[0], expanded);
                populateBool(line[2], visible);
                fileOpen = true;
            }

            EntryId eId = treeMan._getEntryId();
            //Names would be in a separate file. Maybe those true and false would be as well.
            treeMan.mSceneTree.push_back({eId, fileOpen ? nameString : "object", data.type, expanded, visible});

            previousCreatedNode = ogreMan._createTreeItem(eId, currentParent, &creationData);

            //Determine the object data now.
            {
                if(data.type == SceneEntryType::mesh){
                    MeshObjectData objectData;

                    if(staticMeshesFile.is_open()){
                        if(!getline(staticMeshesFile, objectData.meshName)){
                            mSceneDeserialisationErrorReason = "Error reading mesh value from the staticMeshes.txt file.";
                            return false;
                        }
                        if(!getline(staticMeshesFile, objectData.meshMaterial)){
                            mSceneDeserialisationErrorReason = "Error reading mesh material value from the staticMeshes.txt file.";
                            return false;
                        }
                    }

                    objectMan.insertObject(previousCreatedNode, eId, &objectData);
                }else if(data.type == SceneEntryType::physicsShape){
                    PhysicsShapeObjectData objectData;

                    objectData.shapeType = physicsShapeType;
                    objectMan.insertObject(previousCreatedNode, eId, &objectData);
                }else if(data.type == SceneEntryType::collisionSender){
                    //Read the data from the parsed object data to determine what to do.
                    CollisionSenderObjectData objectData;

                    if(!collisionData->collisionObjectRecipeData || mCurrentReadCollisionObject >= collisionData->collisionObjectRecipeData->size()){
                        mSceneDeserialisationErrorReason = "Error reading collision object from collision data.";
                        return false;
                    }
                    const AV::CollisionObjectRecipeData& collisionObjectData = (*collisionData->collisionObjectRecipeData)[mCurrentReadCollisionObject];

                    const AV::CollisionObjectScriptData& scriptTargets = (*collisionData->collisionScriptData)[collisionObjectData.scriptId];
                    const std::string& scriptPath = (*collisionData->collisionScriptAndClosures)[scriptTargets.scriptIdx];
                    const std::string& closureName = (*collisionData->collisionScriptAndClosures)[collisionData->collisionClosuresBegin + scriptTargets.closureIdx];

                    const AV::PhysicsShapeRecipeData& shapeData = (*collisionData->collisionShapeData)[collisionObjectData.shapeId];
                    objectData.shapeType = (PhysicsShapeType)shapeData.physicsShapeType;

                    int packedDataInt = (*collisionData->collisionObjectPackedData)[collisionObjectData.dataId].packedInt;
                    AV::CollisionWorldUtils::PackedIntContents packedData;
                    AV::CollisionWorldUtils::readPackedInt(packedDataInt, &packedData);

                    AV::CollisionWorldUtils::_readValuesToArray<7>(packedData.target, &(objectData.targetValues[0]));
                    AV::CollisionWorldUtils::_readValuesToArray<3>(packedData.eventType, &(objectData.eventValues[0]));

                    objectData.id = (*collisionData->collisionObjectPackedData)[collisionObjectData.dataId].id;
                    objectData.script = scriptPath;
                    objectData.closureName = closureName;

                    objectMan.insertObject(previousCreatedNode, eId, &objectData);
                    mCurrentReadCollisionObject++;
                }else if(data.type == SceneEntryType::navMarker){
                    NavMarkerObjectData objectData;
                    objectMan.insertObject(previousCreatedNode, eId, &objectData);
                }else if(data.type == SceneEntryType::dataPoint){
                    DataPointObjectData objectData;
                    if(!_readDataPoint(dataPointsFile, objectData)){
                        mSceneDeserialisationErrorReason = "Error reading value from data point file.";
                        return false;
                    }
                    objectMan.insertObject(previousCreatedNode, eId, &objectData);
                }else{
                    //Assume it's an empty node.
                    ObjectData objectData;
                    objectMan.insertObject(previousCreatedNode, eId, &objectData);
                }

            }

        }

        return true;
    }

    bool SceneSerialiser::_readDataPoint(std::ifstream& file, DataPointObjectData& outData){
        std::string line;
        //Read the position which is ignored.
        if(!getline(file, line)) return false;
        if(!getline(file, line)) return false;

        Ogre::StringVector vec = Ogre::StringUtil::split(line);
        if(vec.size() != 3) return false;
        unsigned int type = Ogre::StringConverter::parseUnsignedInt(vec[0], 30000);
        unsigned int subType = Ogre::StringConverter::parseUnsignedInt(vec[1], 30000);
        int userValue = Ogre::StringConverter::parseUnsignedInt(vec[2]);
        if(type > 255 || subType > 255) return false;
        outData.userData = userValue;
        outData.dataType = static_cast<DataPointType>(type);
        outData.subDataType = static_cast<DataPointSubType>(subType);

        return true;
    }

    bool SceneSerialiser::_readHeaderInfo(const std::string& line, HeaderData& outData){
        //The line is expected to be of a certain form.
        static const std::regex e("^[01234567] [01] [01] [01]$"); //Header info.
        if(!std::regex_match(line, e)) return false;

        switch(line[0]){
            case '1': outData.type = SceneEntryType::treeChild; break;
            case '2': outData.type = SceneEntryType::treeTerminator; break;
            case '3': outData.type = SceneEntryType::mesh; break;
            case '4': outData.type = SceneEntryType::physicsShape; break;
            case '5': outData.type = SceneEntryType::collisionSender; break;
            case '6': outData.type = SceneEntryType::navMarker; break;
            case '7': outData.type = SceneEntryType::dataPoint; break;
            default: outData.type = SceneEntryType::empty; break;
        }

        if(!populateBool(line[2], outData.hasPosition)) return false;
        if(!populateBool(line[4], outData.hasScale)) return false;
        if(!populateBool(line[6], outData.hasOrientation)) return false;

        return true;
    }

    bool SceneSerialiser::populateBool(char c, bool& b){
        //I farm this off to a function so I can do the return false here as well. It just looks nicer.
        switch(c){
            case '0': b = false; break;
            case '1': b = true; break;
            default: return false;
        }
        return true;
    }

    char SceneSerialiser::objectTypeToChar(SceneEntryType type){
        switch(type){
            //1 - child, 2 - term
            case SceneEntryType::empty: return '0';
            case SceneEntryType::mesh: return '3';
            case SceneEntryType::physicsShape: return '4';
            case SceneEntryType::collisionSender: return '5';
            case SceneEntryType::navMarker: return '6';
            case SceneEntryType::dataPoint: return '7';
            default: return '0';
        }
    }

    char SceneSerialiser::_physicsShapeToChar(PhysicsShapeType shape){
        switch(shape){
            case PhysicsShapeType::box: return '0';
            case PhysicsShapeType::sphere: return '1';
            case PhysicsShapeType::capsule: return '2';
            default: return '0';
        }
    }

    bool SceneSerialiser::_readPhysicsShapeLine(const std::string& line, PhysicsShapeType* outType){
        static const std::regex e("[012]"); //Header info.
        if(!std::regex_match(line, e)) return false;

        switch(line[0]){
            case '0': *outType = PhysicsShapeType::box; break;
            case '1': *outType = PhysicsShapeType::sphere; break;
            case '2': *outType = PhysicsShapeType::capsule; break;
            default: assert(false);
        }

        return true;
    }
}
