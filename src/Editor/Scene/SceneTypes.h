#pragma once

#include "Editor/Resources/ResourceTypes.h"

#include <set>

namespace Southsea{
    typedef unsigned int EntryId;
    static const EntryId INVALID_ID = 0;
    static const EntryId ROOT_NODE = 1;

    typedef unsigned char NavMeshId;
    static const NavMeshId INVALID_NAV_MESH_ID = 0xFF;

    typedef unsigned char DataPointType;
    typedef unsigned char DataPointSubType;

    enum class SceneEntryType{
        treeTerminator,
        treeChild,

        empty,
        mesh,
        physicsShape,
        collisionSender,
        navMarker,
        dataPoint
    };

    struct SceneEntry{
        EntryId id;

        std::string name;
        SceneEntryType type;

        bool expanded;
        bool visible;
    };

    enum class ObjectType{
        empty,
        mesh,
        physicsShape,
        collisionSender,
        navMarker,
        dataPoint
    };

    enum class PhysicsShapeType{
        box,
        sphere,
        capsule
    };

    struct ObjectData{
        virtual ObjectType type() const { return ObjectType::empty; }
    };

    struct MeshObjectData : public ObjectData{
        std::string meshName;
        std::string meshMaterial;

        ObjectType type() const override { return ObjectType::mesh; }
    };

    struct PhysicsShapeObjectData : public ObjectData{
        PhysicsShapeType shapeType;

        ObjectType type() const override { return ObjectType::physicsShape; }
    };

    enum class CollisionSenderObjectDataTypes{
        none,
        shape,
        target,
        event,
        id,
        script,
        closure
    };
    struct CollisionSenderObjectData : public ObjectData{
        PhysicsShapeType shapeType;
        bool targetValues[8];
        bool eventValues[3];
        int id;
        ResourceFSEntry script;
        std::string closureName;

        const CollisionSenderObjectData& operator=(const CollisionSenderObjectData &a){
            shapeType = a.shapeType;
            for(int i = 0; i < 8; i++) targetValues[i] = a.targetValues[i];
            for(int i = 0; i < 3; i++) eventValues[i] = a.eventValues[i];
            id = a.id;
            script = a.script;
            closureName = a.closureName;

            return *this;
        }

        ObjectType type() const override { return ObjectType::collisionSender; }
    };

    struct NavMarkerRules{
        bool acceptPhysicsShapes;
    };
    struct NavMarkerObjectData : ObjectData{
        std::set<NavMeshId> contributingMeshes;
        NavMarkerRules rules;

        const NavMarkerObjectData& operator=(const NavMarkerObjectData &a){
            contributingMeshes = a.contributingMeshes;
            rules = a.rules;

            return *this;
        }

        ObjectType type() const override { return ObjectType::navMarker; }
    };

    struct DataPointObjectData : ObjectData{
        DataPointType dataType;
        DataPointSubType subDataType;
        int userData;
        DataPointObjectData() : userData(0), dataType(0), subDataType(0) {}

        ObjectType type() const override { return ObjectType::dataPoint; }

        enum Type{
            TYPE,
            SUBTYPE,
            USERDATA
        };
    };
}
