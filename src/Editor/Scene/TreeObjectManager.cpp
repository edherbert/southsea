#include "TreeObjectManager.h"

#include "Ogre.h"
#include "OgreItem.h"

#include "Editor/EditorSingleton.h"
#include "Editor/DataPoint/DataPointManager.h"

#include "PhysicsBodyDrawer.h"

namespace Southsea{
    TreeObjectManager::TreeObjectManager(){

    }

    TreeObjectManager::~TreeObjectManager(){
        assert(mPhysicsBodyDrawer);
        delete mPhysicsBodyDrawer;
    }

    void TreeObjectManager::initialise(Ogre::SceneManager* sceneManager){
        mSceneManager = sceneManager;

        mPhysicsBodyDrawer = new PhysicsBodyDrawer();
        mPhysicsBodyDrawer->initialise(sceneManager);
    }

    ObjectData* TreeObjectManager::constructObjectDataFromResource(const Resource& resource, const ResourceFSEntry& fsEntry){
        //Determine the resource and construct the mesh data from it.
        //Return that and then implement the insert action.

        switch(fsEntry.type){
            case(ResourceType::mesh):{
                MeshObjectData* meshData = new MeshObjectData();
                meshData->meshName = fsEntry.name;
                meshData->meshMaterial.clear();

                return meshData;
                break;
            }
            default:{
                assert(false);
                return 0;
            }
        };
    }

    ObjectData* TreeObjectManager::constructPhysicsData(PhysicsShapeType type){
        switch(type){
            case PhysicsShapeType::sphere:
            case PhysicsShapeType::box:{
                PhysicsShapeObjectData* newData = new PhysicsShapeObjectData();
                newData->shapeType = type;

                return newData;
            }
            default:{
                assert(false);
                return 0;
            }
        }
    }

    ObjectData* TreeObjectManager::constructCollisionSenderData(){
        CollisionSenderObjectData* newData = new CollisionSenderObjectData();

        return newData;
    }

    ObjectData* TreeObjectManager::constructNavMarkerData(){
        NavMarkerObjectData* newData = new NavMarkerObjectData();

        return newData;
    }

    ObjectData* TreeObjectManager::constructDataPointData(){
        DataPointObjectData* newData = new DataPointObjectData();

        return newData;
    }

    const ObjectData* TreeObjectManager::getDataObject(EntryId id) const{
        return utilGetDataObject(id, mObjectData);
    }

    CollisionSenderObjectData* TreeObjectManager::getSenderObjectData(EntryId id){
        auto it = mObjectData.find(id);
        assert(it != mObjectData.end());
        assert(it->second->type() == ObjectType::collisionSender);

        return static_cast<CollisionSenderObjectData*>(it->second);
    }

    void TreeObjectManager::insertObject(Ogre::SceneNode* node, EntryId resultId, ObjectData* objectData){
        if(!objectData){
            mObjectData[resultId] = new ObjectData();
            return;
        }

        //For insertion I create an entirely new copy of the object data.
        //This is associated with the scene node, and is what's actually read from by things like the object properties inspector.
        //This separation is made because the object data for the actions needs to be kept read only, to preserve the history of the object. So a copy is necessary.
        ObjectData* newData;
        switch(objectData->type()){
            case ObjectType::empty:{
                newData = new ObjectData();
                break;
            }
            case ObjectType::mesh:{
                //Create the object and attach it.
                MeshObjectData* meshData = static_cast<MeshObjectData*>(objectData);
                Ogre::Item *item = mSceneManager->createItem(meshData->meshName, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
                if(!meshData->meshMaterial.empty()){
                    item->setDatablock(meshData->meshMaterial);
                }
                node->attachObject((Ogre::MovableObject*)item);

                MeshObjectData* newMeshData = new MeshObjectData();
                *newMeshData = *meshData;
                newData = newMeshData;
                break;
            }
            case ObjectType::physicsShape:{
                PhysicsShapeObjectData* shapeData = new PhysicsShapeObjectData();
                PhysicsShapeObjectData* originData = (PhysicsShapeObjectData*)objectData;
                shapeData->shapeType = originData->shapeType;

                mPhysicsBodyDrawer->createShapeForNode(node, originData->shapeType);

                newData = shapeData;
                break;
            }
            case ObjectType::collisionSender:{
                CollisionSenderObjectData* data = new CollisionSenderObjectData();
                CollisionSenderObjectData* originData = (CollisionSenderObjectData*)objectData;

                *data = *originData;

                mPhysicsBodyDrawer->createShapeForNode(node, originData->shapeType);
                newData = data;
                break;
            }
            case ObjectType::navMarker:{
                NavMarkerObjectData* data = new NavMarkerObjectData();
                NavMarkerObjectData* originData = (NavMarkerObjectData*)objectData;

                *data = *originData;
                newData = data;
                break;
            }
            case ObjectType::dataPoint:{
                DataPointObjectData* data = new DataPointObjectData();
                DataPointObjectData* originData = (DataPointObjectData*)objectData;

                const std::string& s = EditorSingleton::getEditor()->getDataPointManager()->getMeshForType(originData->dataType, originData->subDataType);
                if(!s.empty()){
                    Ogre::Item *item = mSceneManager->createItem(s, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
                    node->attachObject((Ogre::MovableObject*)item);
                }

                *data = *originData;
                newData = data;
                break;
            }
            default:{
                assert(false);
                break;
            }
        }

        mObjectData[resultId] = newData;
    }

    void TreeObjectManager::removeObject(Ogre::SceneNode* node, EntryId resultId){
        assert(node);

        auto it = mObjectData.find(resultId);
        assert(it != mObjectData.end());

        ObjectData *objectData = (*it).second;
        switch(objectData->type()){
            case ObjectType::dataPoint:
            case ObjectType::mesh:{
                //Remove and destroy the mesh.
                _destroyMovableObject(node);
                break;
            }
            case ObjectType::physicsShape:
            case ObjectType::collisionSender:{
                //PhysicsShapeObjectData* data = (PhysicsShapeObjectData*)objectData;
                mPhysicsBodyDrawer->destroyShapeForNode(node);
                break;
            }
            case ObjectType::navMarker:
            case ObjectType::empty:{
                break;
            }
            default:{
                assert(false);
                break;
            }
        }

        delete (*it).second;
        mObjectData.erase(it);
    }

    void TreeObjectManager::_destroyMovableObject(Ogre::SceneNode* node){
        //When it comes time to destroy a mesh (movable object) a bit more work needs to be done, as you have to check it's actually there.
        Ogre::SceneNode::ObjectIterator it = node->getAttachedObjectIterator();
        if(it.hasMoreElements()){
            Ogre::MovableObject* obj = it.getNext();
            mSceneManager->destroyMovableObject(obj);
        }
        //At the moment a mesh object should only have the one mesh attached to it, so by this point there should be no more.
        assert(!it.hasMoreElements());
    }

    void TreeObjectManager::changeOgreMesh(Ogre::SceneNode* node, EntryId id, const std::string& newMesh){
        ObjectData* data = mObjectData[id];
        assert(data->type() == ObjectType::mesh || data->type() == ObjectType::dataPoint);

        //Right now it just detaches the object and creates a new one. There might be a way to do this that doesn't involve destruction.
        _destroyMovableObject(node);

        node->detachAllObjects();

        if(data->type() == ObjectType::mesh){
            //Set the data to the new value.
            MeshObjectData* meshData = static_cast<MeshObjectData*>(data);
            meshData->meshName = newMesh;
            if(!newMesh.empty()) meshData->meshMaterial.clear();
        }

        if(newMesh.empty()){
            //Don't attach the new object as the provided string was empty.
            return;
        }

        Ogre::Item *item = mSceneManager->createItem(newMesh, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
        node->attachObject((Ogre::MovableObject*)item);

    }

    void TreeObjectManager::changeOgreMeshMaterial(Ogre::SceneNode* node, EntryId id, const std::string& newMaterial){
        ObjectData* data = mObjectData[id];
        assert(data->type() == ObjectType::mesh);

        //Set the data to the new value.
        MeshObjectData* meshData = static_cast<MeshObjectData*>(data);
        meshData->meshMaterial = newMaterial;

        Ogre::SceneNode::ObjectIterator it = node->getAttachedObjectIterator();
        assert(it.hasMoreElements());
        Ogre::MovableObject* obj = it.getNext();
        Ogre::Item* itm = dynamic_cast<Ogre::Item*>(obj);
        assert(itm);
        itm->setDatablock(newMaterial);

    }

    void TreeObjectManager::setNavMeshMarkerContribution(EntryId markerId, NavMeshId meshId, bool contributes){
        ObjectData* data = mObjectData[markerId];
        assert(data);
        assert(data->type() == ObjectType::navMarker);

        NavMarkerObjectData* navData = static_cast<NavMarkerObjectData*>(data);
        if(contributes){
            navData->contributingMeshes.insert(meshId);
        }else{
            navData->contributingMeshes.erase(meshId);
        }
    }

    void TreeObjectManager::changePhysicsShape(Ogre::SceneNode* node, EntryId id, PhysicsShapeType newType){
        ObjectData* data = mObjectData[id];
        assert(data->type() == ObjectType::physicsShape || data->type() == ObjectType::collisionSender);

        //Set the data to the new value.
        if(data->type() == ObjectType::physicsShape){
            PhysicsShapeObjectData* shapeData = static_cast<PhysicsShapeObjectData*>(data);
            shapeData->shapeType = newType;
        }
        else if(data->type() == ObjectType::collisionSender){
            CollisionSenderObjectData* shapeData = static_cast<CollisionSenderObjectData*>(data);
            shapeData->shapeType = newType;
        }

        mPhysicsBodyDrawer->destroyShapeForNode(node);
        mPhysicsBodyDrawer->createShapeForNode(node, newType);
    }

    void TreeObjectManager::changeDataPointType(Ogre::SceneNode* node, EntryId id, DataPointObjectData::Type type, int newValue){
        ObjectData* data = mObjectData[id];
        assert(data);
        assert(data->type() == ObjectType::dataPoint);

        DataPointObjectData* pointData = static_cast<DataPointObjectData*>(data);
        switch(type){
            case DataPointObjectData::SUBTYPE:
            case DataPointObjectData::TYPE:{
                assert(newValue <= 255);
                unsigned char value = static_cast<unsigned char>(newValue);
                if(type == DataPointObjectData::TYPE) pointData->dataType = value;
                else pointData->subDataType = value;
                break;
            }
            case DataPointObjectData::USERDATA:
                pointData->userData = newValue;
                break;
        }

        const std::string& s = EditorSingleton::getEditor()->getDataPointManager()->getMeshForType(pointData->dataType, pointData->subDataType);
        if(!s.empty()){
            changeOgreMesh(node, id, s);
        }
    }

    const ObjectData* TreeObjectManager::utilGetDataObject(EntryId id, const TreeData& data){
        auto it = data.find(id);
        assert(it != data.end() && "Object does not exist.");

        return (*it).second;
    }

}
