#include "OgreTreeManager.h"

#include <iostream>
#include "OgreSceneManager.h"

#include "TreeObjectManager.h"

namespace Southsea{
    const OgreTreeManager::TreeNodeCreationData OgreTreeManager::ZERO = {Ogre::Vector3::ZERO, Ogre::Vector3(1, 1, 1), Ogre::Quaternion::IDENTITY};

    OgreTreeManager::OgreTreeManager(){

    }

    OgreTreeManager::~OgreTreeManager(){

    }

    void OgreTreeManager::initialise(Ogre::SceneManager* sceneManager){
        mSceneManager = sceneManager;

        mRootNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
        mRootNode->getUserObjectBindings().setUserAny(Ogre::Any(ROOT_NODE));
    }

    Ogre::SceneNode* OgreTreeManager::getNodeById(EntryId id) const{
        return utilGetNodeById(mEntryMap, id);
    }

    Ogre::SceneNode* OgreTreeManager::createTreeItem(EntryId id, EntryId parent, const TreeNodeCreationData* creationData){
        assert(id != 0 && parent != 0); //Terminator or child node.
        assert(id != ROOT_NODE);
        Ogre::SceneNode* parentNode = 0;
        if(parent == ROOT_NODE) parentNode = mRootNode;
        else{
            parentNode = mEntryMap[parent];
        }

        assert(parentNode);

        return _createTreeItem(id, parentNode, creationData);
    }

    Ogre::SceneNode* OgreTreeManager::_createTreeItem(EntryId id, Ogre::SceneNode* parentNode, const TreeNodeCreationData* creationData){
        Ogre::SceneNode* newNode = parentNode->createChildSceneNode();
        newNode->getUserObjectBindings().setUserAny(Ogre::Any(id));

        if(creationData){
            newNode->setPosition(creationData->position);
            newNode->setScale(creationData->scale);
            newNode->setOrientation(creationData->orientation);
        }

        auto it = mEntryMap.find(id);
        assert(it == mEntryMap.end());
        //(*it).second = newNode;
        mEntryMap[id] = newNode;

        //_debugPrintTree();

        return newNode;
    }

    EntryId OgreTreeManager::getParentOfEntry(EntryId id){
        assert(id != 0); //Terminator or child node.
        assert(id != ROOT_NODE);
        auto it = mEntryMap.find(id);
        assert(it != mEntryMap.end());

        Ogre::SceneNode* node = (*it).second;
        Ogre::SceneNode* parentNode = node->getParentSceneNode();
        assert(parentNode);

        return getIdFromNode(parentNode);
    }

    void OgreTreeManager::destroyTreeItem(EntryId id, TreeObjectManager* treeMan){
        assert(id != 0 && id != ROOT_NODE);
        _debugPrintTree();

        auto it = mEntryMap.find(id);
        assert(it != mEntryMap.end());

        std::pair<EntryId, Ogre::SceneNode*> i = *it;
        mEntryMap.erase(it);
        _recursiveDestroyNode(i.second, treeMan);

        if(treeMan) treeMan->removeObject(i.second, id);
        mSceneManager->destroySceneNode(i.second);

        _debugPrintTree();
    }

    void OgreTreeManager::reparentTreeItem(EntryId id, EntryId newParent){
        assert(id != 0 && newParent != 0);
        assert(id != ROOT_NODE);

        auto it = mEntryMap.find(id);
        assert(it != mEntryMap.end());

        Ogre::SceneNode* targetNode = (*it).second;
        Ogre::SceneNode* oldParentNode = targetNode->getParentSceneNode();
        EntryId oldParentId = oldParentNode->getUserObjectBindings().getUserAny().get<EntryId>();

        if(oldParentId == newParent) return; //Nothing to do.
        if(oldParentId != ROOT_NODE) { //If the parent node is not the root node, it should be in the map.
            assert(mEntryMap.find(oldParentId) != mEntryMap.end());
        }

        //Now find the new parent.
        Ogre::SceneNode* targetNewParent = 0;
        if(newParent == ROOT_NODE) targetNewParent = mRootNode;
        else{
            auto parentIt = mEntryMap.find(newParent);
            assert(parentIt != mEntryMap.end());
            targetNewParent = (*parentIt).second;
        }

        assert(targetNewParent);

        oldParentNode->removeChild(targetNode);
        targetNewParent->addChild(targetNode);

        _debugPrintTree();
    }

    void OgreTreeManager::clearCurrentContent(TreeObjectManager* treeMan){
        _recursiveDestroyNode(mRootNode, treeMan);

        mEntryMap.clear();
    }

    std::pair<int,int> OgreTreeManager::_insertItems(const std::vector<SceneEntry>& sceneEntries, const std::vector<ObjectData*>& objData, const std::vector<TreeNodeCreationData>& treeVec, int currentIdx, int endIdx, int currentObj, Ogre::SceneNode* currentParent, TreeObjectManager* treeMan){
        assert(currentParent);

        Ogre::SceneNode* previousCreatedNode = 0;
        while(currentIdx < endIdx){
            if(sceneEntries[currentIdx].type == SceneEntryType::treeChild){
                std::pair<int,int> retVal = _insertItems(sceneEntries, objData, treeVec, currentIdx + 1, endIdx, currentObj, previousCreatedNode, treeMan);
                currentIdx = retVal.first;
                currentObj = retVal.second;
            }else if(sceneEntries[currentIdx].type == SceneEntryType::treeTerminator){
                return {currentIdx + 1, currentObj};
            }else{
                //Just a regular object.
                EntryId targetId = sceneEntries[currentIdx].id;
                previousCreatedNode = OgreTreeManager::_createTreeItem(targetId, currentParent, &(treeVec[currentObj]) );
                treeMan->insertObject(previousCreatedNode, targetId, objData[currentObj]);
                currentIdx++;
                currentObj++;
            }
        }

        return {currentIdx, currentObj};
    }

    void OgreTreeManager::repopulateTree(const std::vector<SceneEntry>& sceneEntries, const std::vector<ObjectData*>& objData, const std::vector<TreeNodeCreationData>& treeVec, int start, int end, int objStart, int objEnd, EntryId parentId, TreeObjectManager* treeMan){
        Ogre::SceneNode* parentNode = 0;
        if(parentId == ROOT_NODE){
            parentNode = mRootNode;
        }else{
            auto it = mEntryMap.find(parentId);
            assert(it != mEntryMap.end());
            parentNode = (*it).second;
        }
        assert(parentNode);

        std::pair<int,int> result = _insertItems(sceneEntries, objData, treeVec, start, end, objStart, parentNode, treeMan);
        assert(result.second == objEnd);

        _debugPrintTree();
    }

    void OgreTreeManager::setTreeItemVisible(EntryId id, bool visible){
        auto it = mEntryMap.find(id);
        assert(it != mEntryMap.end());

        (*it).second->setVisible(visible, false);
    }

    void OgreTreeManager::_recursiveDestroyNode(Ogre::SceneNode* node, TreeObjectManager* treeMan){

        auto it = node->getChildIterator();
        while(it.current() != it.end()){
            Ogre::SceneNode *n = (Ogre::SceneNode*)it.getNext();
            _recursiveDestroyNode(n, treeMan);

            EntryId id = getIdFromNode(n);
            auto idIt = mEntryMap.find(id);
            assert(idIt != mEntryMap.end()); //The id has to be in the list otherwise something went wrong.
            mEntryMap.erase(idIt);

            if(treeMan) treeMan->removeObject(n, id);

            mSceneManager->destroySceneNode(n);
        }
    }

    ObjectData* OgreTreeManager::cloneObjectData(const ObjectData* data){
        switch(data->type()){
            case ObjectType::empty: return new ObjectData();
            case ObjectType::mesh:{
                MeshObjectData *d = new MeshObjectData();
                const MeshObjectData* meshData = static_cast<const MeshObjectData*>(data);
                d->meshName = meshData->meshName;
                return d;
            }
            case ObjectType::physicsShape:{
                const PhysicsShapeObjectData* physicsData = static_cast<const PhysicsShapeObjectData*>(data);
                PhysicsShapeObjectData* d = new PhysicsShapeObjectData();
                d->shapeType = physicsData->shapeType;

                return d;
            }
            case ObjectType::collisionSender:{
                const CollisionSenderObjectData* objectData = dynamic_cast<const CollisionSenderObjectData*>(data);
                CollisionSenderObjectData* d = new CollisionSenderObjectData();
                *d = *objectData;

                return d;
            }
            case ObjectType::navMarker:{
                const NavMarkerObjectData* objectData = dynamic_cast<const NavMarkerObjectData*>(data);
                NavMarkerObjectData* d = new NavMarkerObjectData();
                *d = *objectData;

                return d;
            }
            case ObjectType::dataPoint:{
                const DataPointObjectData* objectData = dynamic_cast<const DataPointObjectData*>(data);
                DataPointObjectData* d = new DataPointObjectData();
                *d = *objectData;

                return d;
            }
            default: assert(false); break;
        }
    }

    void OgreTreeManager::populateVectorWithNodeData(EntryId id, std::vector<ObjectData*>& objVec, std::vector<TreeNodeCreationData>& treeVec, TreeObjectManager* treeMan, Ogre::SceneNode* targetNode){
        Ogre::SceneNode* node = targetNode;
        if(!targetNode){ //In this case find the node from the id.
            auto i = mEntryMap.find(id);
            assert(i != mEntryMap.end());
            node = (*i).second;
        }

        EntryId nodeId = getIdFromNode(node);
        const ObjectData* retData = treeMan->getDataObject(nodeId);
        objVec.push_back(cloneObjectData(retData));

        TreeNodeCreationData treeData{
            node->getPosition(),
            node->getScale(),
            node->getOrientation()
        };
        treeVec.push_back(treeData);

        auto it = node->getChildIterator();
        while(it.current() != it.end()){
            Ogre::SceneNode *n = (Ogre::SceneNode*)it.getNext();
            populateVectorWithNodeData(0, objVec, treeVec, treeMan, n);
        }
    }

    void OgreTreeManager::_debugPrintNode(Ogre::SceneNode* node, int childCount){
        if(childCount == 0){
            std::cout << "===========================" << '\n';
        }

        auto it = node->getChildIterator();
        while(it.current() != it.end()){
            Ogre::SceneNode *n = (Ogre::SceneNode*)it.getNext();
            std::cout << std::string(childCount * 2, ' ') << "node[" << n->getUserObjectBindings().getUserAny() << "]" << '\n';

            _debugPrintNode(n, childCount + 1);
        }

        if(childCount == 0){
            std::cout << "===========================" << '\n';
        }
    }

    EntryId OgreTreeManager::getIdFromNode(Ogre::SceneNode* node) const{
        return node->getUserObjectBindings().getUserAny().get<EntryId>();
    }

    EntryId OgreTreeManager::getIdFromNodeCheck(Ogre::SceneNode* node) const{
        if(!node->getUserObjectBindings().getUserAny().has_value()) return 0;

        return getIdFromNode(node);
    }

    Ogre::SceneNode* OgreTreeManager::utilGetNodeById(const TreeData& data, EntryId id){
        auto it = data.find(id);
        if(it == data.end()) return 0;

        return (*it).second;
    }
}
