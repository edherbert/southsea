#pragma once

#include <string>
#include <fstream>
#include "SceneTypes.h"
#include "OgreVector3.h"
#include "OgreIdString.h"

namespace Ogre{
    class SceneNode;
}

namespace AV{
    struct CollisionWorldChunkData;
}

namespace Southsea{
    class SceneTreeManager;
    class OgreTreeManager;
    class TreeObjectManager;

    class SceneSerialiser{
    public:
         SceneSerialiser();
         ~SceneSerialiser();

         bool serialise(const SceneTreeManager& sceneMan, const OgreTreeManager& ogreMan, const TreeObjectManager& treeManager, const std::string& outDirectory);

         /**
         Deserialise the contents of a scene into memory.
         */
         bool deserialise(SceneTreeManager& treeMan, OgreTreeManager& ogreMan, TreeObjectManager& objectMan, const std::string& targetDirectory);

     private:
         bool _processFileEntry(const std::string& line);
         bool _deserialise(SceneTreeManager& treeMan, OgreTreeManager& ogreMan, TreeObjectManager& objectMan, const std::string& targetDirectory, AV::CollisionWorldChunkData& collisionChunkData);

         struct HeaderData{
             bool hasPosition, hasScale, hasOrientation;
             SceneEntryType type;
         };

         typedef unsigned int VecIdx;
         typedef unsigned int ShapeTypeInt;
         struct PhysicsShapeData{
             Ogre::Vector3 position;
             Ogre::Quaternion orientation;
             VecIdx shapeId;
         };
         struct CollisionObjectPositionData{
             Ogre::Vector3 position;
             VecIdx shapeId;
             VecIdx scriptId;
             VecIdx dataId;
         };
         struct ShapeEntryData{
             Ogre::Vector3 scale;
             ShapeTypeInt shapeType;
         };
         struct ScriptEntryData{
             Ogre::IdString mHash;
             std::string path;
         };
         struct ScriptAndClosureId{
             VecIdx scriptId;
             VecIdx closureId;
         };
         struct CollisionObjectData{
             bool targetValues[7];
             bool eventValues[3];
             int id;
         };
         bool _readHeaderInfo(const std::string& line, HeaderData& outData);
         inline bool populateBool(char c, bool& b);

         bool _processFile(SceneTreeManager& treeMan, OgreTreeManager& ogreMan, TreeObjectManager& objectMan, std::ifstream& file, std::ifstream& sceneDataFile, std::ifstream& staticMeshesFile, std::ifstream& dataPointsFile, std::string& line, Ogre::SceneNode* currentParent, AV::CollisionWorldChunkData* collisionData);

         struct FoundShapeInfo{
             VecIdx shapeIndex;
             Ogre::Vector3 pos;
             Ogre::Quaternion orientation;
         };

         VecIdx _findAndProcessCollisionData(std::vector<CollisionObjectData>* scriptData, const CollisionSenderObjectData* data) const;
         VecIdx _findAndProcessScriptAndClosureId(std::vector<ScriptAndClosureId>* scriptData, const ScriptAndClosureId& targetId) const;
         void _findAndProcessShape(std::vector<ShapeEntryData> *shapeData, ShapeTypeInt shapeType, Ogre::SceneNode* node, FoundShapeInfo* outInfo) const;
         VecIdx _findAndProcessScript(std::vector<ScriptEntryData> *scriptData, const std::string& scriptPath) const;

        bool _readDataPoint(std::ifstream& file, DataPointObjectData& outData);
         /**
         Intended to be called when the deserialisation procedure has failed.
         This will clear the scene tree of any values, and populate it with the root and terminator.
         */
         void _clearSceneTree(SceneTreeManager& treeMan);

         char objectTypeToChar(SceneEntryType type);
         char _physicsShapeToChar(PhysicsShapeType shape);
         bool _readPhysicsShapeLine(const std::string& line, PhysicsShapeType* outType);

         size_t mCurrentReadCollisionObject;
         std::string mSceneDeserialisationErrorReason;
    };
}
