#pragma once

#include <memory>
#include <vector>
#include <iostream>
#include "Editor/Scene/SceneTypes.h"
#include "tinyxml2.h"

namespace Southsea{
    class SceneTreeManager;
    class OgreTreeManager;
    class TreeObjectManager;

    /**
    A class to export a section of a scene tree into an avEngine scene tree file.
    These files can be re-used as required.
    */
    class SceneFileExporter{
    public:
        SceneFileExporter(const std::shared_ptr<SceneTreeManager> sceneMan, const std::shared_ptr<OgreTreeManager> ogreMan, const std::shared_ptr<TreeObjectManager> treeMan);
        ~SceneFileExporter();

        bool exportSceneFile(const std::string& path, const std::vector<int>& indexes, const std::vector<SceneEntry>& sceneTree);
    private:
        const std::shared_ptr<SceneTreeManager> mSceneMan;
        const std::shared_ptr<OgreTreeManager> mOgreMan;
        const std::shared_ptr<TreeObjectManager> mTreeMan;

        int _exportSceneNode(tinyxml2::XMLDocument& xmlDoc, tinyxml2::XMLElement* parent, int index, int layer, const std::vector<SceneEntry>& sceneTree, bool& result);
        tinyxml2::XMLElement* _exportObject(tinyxml2::XMLDocument& xmlDoc, tinyxml2::XMLElement* parent, const SceneEntry& e);
        int _recursiveExportChildNode(tinyxml2::XMLDocument& xmlDoc, tinyxml2::XMLElement* parent, int index, int layer, const std::vector<SceneEntry>& sceneTree, bool& result);
    };
}
