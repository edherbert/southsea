#pragma once

#include "Ogre.h"

namespace Southsea{

    static const Ogre::uint32 PHYSICS_OBJECT_MASK = 0x8000000;
    static const Ogre::uint32 NAV_MESH_OBJECT_MASK = 0x4000000;

}
