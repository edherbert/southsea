#include "SceneTreeManager.h"

#include "Editor/Action/Actions/Scene/TreeRearrangeAction.h"

#include <cassert>
#include <iostream>
#include <nfd.h>

#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "TreeObjectManager.h"

//Possibly temporary
#include "Editor/EditorSingleton.h"
#include "OgreTreeManager.h"

#include "SceneFileExporter.h"

#include "Editor/Action/Actions/Scene/ScenePasteAction.h"
#include "Editor/Action/Actions/Scene/ObjectDeleteAction.h"
#include "Editor/Action/Actions/Scene/ObjectInsertionAction.h"
#include "Editor/Action/ActionStack.h"
#include "Editor/Action/Actions/Scene/ObjectInsertionType.h"

namespace Southsea{

    const SceneEntry SceneTreeManager::TERM{0, "", SceneEntryType::treeTerminator, false};
    const SceneEntry SceneTreeManager::CHILD{0, "", SceneEntryType::treeChild, false};

    SceneTreeManager::SceneTreeManager(){

    }

    SceneTreeManager::~SceneTreeManager(){

    }

    void SceneTreeManager::renameTreeItem(EntryId id, const std::string& newName){
        int idxCount = 0;
        for(SceneEntry& e : mSceneTree){
            if(e.id == id){
                e.name = newName;
                break;
            }
            idxCount++;
        }
    }

    std::string SceneTreeManager::getSceneTreeItemName(EntryId id) const{
        for(const SceneEntry& e : mSceneTree){
            if(e.id == id) return e.name;
        }

        return "";
    }

    void SceneTreeManager::clearCurrentContent(){
        mSceneTree.clear();
        mSelectedIds.clear();

        _clearAllSelection();

        while(!mRecycledIds.empty()) mRecycledIds.pop();
        mUsedIds = _baseId;
        mMostRecentSelection = 0;
    }

    void SceneTreeManager::setupTestSceneTree(){
        auto treeMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto objectMan = EditorSingleton::getEditor()->getTreeObjectManager();
        return;
        SceneEntry rootEntry = CHILD;
        rootEntry.id = ROOT_NODE;
        mSceneTree = {
            rootEntry,
            {_getEntryId(), "first", SceneEntryType::empty, false, true},
                CHILD,
                {_getEntryId(), "second", SceneEntryType::empty, false, true},
                    CHILD,
                    {_getEntryId(), "inside", SceneEntryType::empty, false, true},
                    {_getEntryId(), "moreInside", SceneEntryType::empty, false, true},
                    TERM,
                {_getEntryId(), "third", SceneEntryType::empty, false, true},
                TERM,
            {_getEntryId(), "fourth", SceneEntryType::empty, false, true},
                CHILD,
                {_getEntryId(), "fifth", SceneEntryType::empty, false, true},
                TERM,
            TERM
        };

        EntryId ids[7] = {
            mSceneTree[1].id,
            mSceneTree[3].id,
            mSceneTree[5].id,
            mSceneTree[6].id,
            mSceneTree[8].id,
            mSceneTree[10].id,
            mSceneTree[12].id
        };
        Ogre::SceneNode* nodes[7] = {
            treeMan->createTreeItem(ids[0], mSceneTree[0].id), //first
            treeMan->createTreeItem(ids[1], mSceneTree[1].id), //second
            treeMan->createTreeItem(ids[2], mSceneTree[3].id), //inside
            treeMan->createTreeItem(ids[3], mSceneTree[3].id), //more inside
            treeMan->createTreeItem(ids[4], mSceneTree[1].id), //third
            treeMan->createTreeItem(ids[5], mSceneTree[0].id), //fourth
            treeMan->createTreeItem(ids[6], mSceneTree[10].id) //fourth
        };

        ObjectData data;
        //Populate the object manager with the blank node information.
        for(int i = 0; i < 7; i++){
            objectMan->insertObject(nodes[i], ids[i], &data);
        }
    }

    void SceneTreeManager::_debugPrint() const{
        int idxCount = 0;
        for(const SceneEntry& e : mSceneTree){
            std::string printString = std::to_string(e.id);

            if(e.type == SceneEntryType::treeChild) printString = "CHILD";
            if(e.type == SceneEntryType::treeTerminator) printString = "TERM";

            std::cout << std::string("  ", idxCount) << " " << printString << '\n';

            if(e.type == SceneEntryType::treeChild) {
                idxCount++;
            }
            if(e.type == SceneEntryType::treeTerminator){
                assert(idxCount > 0);
                idxCount--;
            }

        }
    }

    bool SceneTreeManager::itemHasChildren(const std::vector<SceneEntry>& e, int index) const{
        int targetIndex = index + 1;
        if(targetIndex >= e.size()) return false;

        return e[targetIndex].type == SceneEntryType::treeChild;
    }

    int SceneTreeManager::utilGetTerminatorForChild(int start, const std::vector<SceneEntry>& e){
        if(e[start].type != SceneEntryType::treeChild) return -1;

        //A counter of how many child values have been encountered.
        int childIndex = 0;

        int countIndex = start + 1; //+1 should be child, so we start from +1 of that.
        while(countIndex < e.size()){
            const SceneEntry& current = e[countIndex];

            if(current.type == SceneEntryType::treeChild) childIndex++;
            if(current.type == SceneEntryType::treeTerminator){
                if(childIndex == 0){
                    //This is the terminator for the provided entry.
                    return countIndex + 1;
                }

                childIndex--;
            }

            countIndex++;
        }

        return -1;
    }

    int SceneTreeManager::moveEntry(EntryId target, EntryId targetDestination, ObjectInsertionType type, int customDestinationIndex){
        std::cout << "Moving entry" << '\n';

        _debugPrint();

        int targetIndex = _findEntryIdIndexInTree(target);
        assert(targetIndex >= 0);
        int endIndex = targetIndex + 1;
        if(itemHasChildren(mSceneTree, targetIndex)){
            //We need to move the children over as well.
            int termIndex = utilGetTerminatorForChild(targetIndex + 1, mSceneTree) - 1;
            assert(termIndex >= 0);
            endIndex = termIndex + 1;
        }
        //Here I take a copy of what was previously in the vector to copy over.
        //This isn't going to be the fastest approach, but it will save me having to copy values out as I go, which would be more error prone.
        std::vector<SceneEntry> tempVector(mSceneTree.begin() + targetIndex, mSceneTree.begin() + endIndex);
        mSceneTree.erase(mSceneTree.begin() + targetIndex, mSceneTree.begin() + endIndex);

        //If the item has children, and we've dragged them all out, the terminators need to be removed as well.
        //You can tell that by checking if there's now a child and terminator entry with nothing between them.
        if(mSceneTree[targetIndex - 1].type == SceneEntryType::treeChild && mSceneTree[targetIndex].type == SceneEntryType::treeTerminator){
            mSceneTree.erase(mSceneTree.begin() + targetIndex - 1, mSceneTree.begin() + targetIndex + 1);
        }

        int destinationIndex = 0;
        if(customDestinationIndex > 0) destinationIndex = customDestinationIndex;
        else destinationIndex = _findEntryIdIndexInTree(targetDestination);

        assert(destinationIndex >= 0);

        if(type == ObjectInsertionType::above){
            mSceneTree.insert(mSceneTree.begin() + destinationIndex, tempVector.begin(), tempVector.end());
        }
        else if(type == ObjectInsertionType::below){
            int destTarget = destinationIndex + 1;
            if(itemHasChildren(mSceneTree, destinationIndex)){
                destTarget = utilGetTerminatorForChild(destTarget, mSceneTree);
            }
            mSceneTree.insert(mSceneTree.begin() + destTarget, tempVector.begin(), tempVector.end());
        }
        else{ //Insert into item.
            if(itemHasChildren(mSceneTree, destinationIndex)){
                //The item already has children, so the terminators don't need to be created.
                int destTarget = utilGetTerminatorForChild(destinationIndex + 1, mSceneTree) - 1;

                mSceneTree.insert(mSceneTree.begin() + destTarget, tempVector.begin(), tempVector.end());
            }else{
                mSceneTree.insert(mSceneTree.begin() + destinationIndex + 1, CHILD);
                mSceneTree.insert(mSceneTree.begin() + destinationIndex + 2, tempVector.begin(), tempVector.end());
                mSceneTree.insert(mSceneTree.begin() + destinationIndex + 2 + tempVector.size(), TERM);
            }
        }

        _debugPrint();

        return targetIndex;
    }

    EntryId SceneTreeManager::getTrueDestination(EntryId target, ObjectInsertionType type) const{
        if(type == ObjectInsertionType::into){
            return target;
        }else{
            int index = _findEntryIdIndexInTree(target);
            return getIdOfParent(index);
        }
    }

    bool SceneTreeManager::populateReArrangeEvent(TreeRearrangeAction& action, EntryId destination, ObjectInsertionType type){
        assert(!mSelectedIds.empty());
        for(EntryId id : mSelectedIds){
            if(destination == id) return false;
        }

        std::set<EntryId> reduced = getReducedSelection();

        action.populate(reduced, destination, type);

        return true;
    }

    SceneTreeManager::InsertResult SceneTreeManager::insertObjectIntoTree(EntryId target, ObjectInsertionType type, const ObjectInsertData& data){
        if(type == ObjectInsertionType::none) return {INVALID_ID, -1};

        int idxCount = 0;
        for(SceneEntry& e : mSceneTree){
            if(e.id == target){
                break;
            }
            idxCount++;
        }
        assert(idxCount < mSceneTree.size() && "Most likely the target index wasn't found in the list.");
        //assert(mSceneTree[idxCount].type != SceneEntryType::treeTerminator && mSceneTree[idxCount].type != SceneEntryType::treeChild);
        //In the case of a blank scene we can't assume it won't be a child terminator (as it might be the root.), however it should never be a terminator.
        assert(mSceneTree[idxCount].type != SceneEntryType::treeTerminator);

        int outIndex = idxCount;
        EntryId createdId = _getEntryId();
        const SceneEntry creationEntry{createdId, data.itemName, data.type, false, true};

        if(type == ObjectInsertionType::above){
            mSceneTree.insert(mSceneTree.begin() + idxCount, creationEntry);
        }else if(type == ObjectInsertionType::below){
            if(mSceneTree[idxCount + 1].type == SceneEntryType::treeChild){
                //The item being inserted below has children, so we need to find the end of those children and insert after that.
                int targetIdx = utilGetTerminatorForChild(idxCount + 1, mSceneTree);
                assert(targetIdx >= 0);
                //This would be triggered if inserting below the very final element as the entries are now wrapped in terminators.
                //assert(mSceneTree[targetIdx].type != SceneEntryType::treeTerminator);

                mSceneTree.insert(mSceneTree.begin() + targetIdx, creationEntry);
                outIndex = targetIdx;
            }else{
                mSceneTree.insert(mSceneTree.begin() + idxCount + 1, creationEntry);
                outIndex++;
            }
        }else{ //Insert into an item.
            if(mSceneTree[idxCount + 1].type == SceneEntryType::treeChild){
                //This item already has children, so we don't need to create the terminators.
                int targetIdx = utilGetTerminatorForChild(idxCount + 1, mSceneTree) - 1;
                assert(targetIdx >= 0);

                mSceneTree.insert(mSceneTree.begin() + targetIdx, creationEntry);
                outIndex = targetIdx;
            }else{
                mSceneTree.insert(mSceneTree.begin() + idxCount + 1, CHILD);

                mSceneTree.insert(mSceneTree.begin() + idxCount + 2, creationEntry);

                mSceneTree.insert(mSceneTree.begin() + idxCount + 3, TERM);

                outIndex = idxCount + 2;
                //createdId = 0;
            }
        }
        _debugPrint();

        return {createdId, outIndex};
    }

    int SceneTreeManager::populateVectorWithNode(EntryId id, std::vector<SceneEntry>& target) const{
        //target.clear();

        int idx = _findEntryIdIndexInTree(id);
        assert(idx >= 0);

        if(itemHasChildren(mSceneTree, idx)){
            int termIndex = utilGetTerminatorForChild(idx + 1, mSceneTree);
            for(int i = idx; i < termIndex; i++){
                target.push_back(mSceneTree[i]);
            }
        }else{
            target.push_back(mSceneTree[idx]);
        }

        return idx;
    }

    bool SceneTreeManager::insertVectorItemsIntoItem(std::vector<SceneEntry>& vec, EntryId item, int start, int end){
        int idx = 0;
        if(item == ROOT_NODE){
            //Jump to the end of the list.
            //-1 to skip the terminator.
            idx = mSceneTree.size() - 1;
            //Should not reach the initial child.
            assert(idx > 0);
        }else{
            idx = _findEntryIdIndexInTree(item);
        }

        bool recreateTerminators = false;
        bool hasChildren = false;
        if(item != ROOT_NODE){
            hasChildren = itemHasChildren(mSceneTree, idx);
            recreateTerminators = !hasChildren;
            if(hasChildren){
                //Find the end of the children. The index of the terminator is fine as insertVectorItems puts them before it.
                idx = utilGetTerminatorForChild(idx+1, mSceneTree);
                idx--;
            }else{
                //One extra to offset the fact that insertVectorItems reduces the index.
                idx+=2;
            }
        }
        insertVectorItems(vec, idx, start, end, recreateTerminators, true);

        return !hasChildren;
    }

    void SceneTreeManager::insertVectorItems(std::vector<SceneEntry>& vec, int idx, int start, int end, bool recreateTerminators, bool replaceIds){
        //The ids need to be re-claimed so that it's in sync. That's all this loop does.
        for(int i = end - 1; i >= start; i--){
            if( vec[i].type == SceneEntryType::treeChild || vec[i].type == SceneEntryType::treeTerminator){
                continue;
            }

            EntryId id = _getEntryId();
            if(replaceIds){
                vec[i].id = id;
            }else{
                //Make sure the ids match up. Given the id stack system they should aways match up if done correctly.
                assert(id == vec[i].id);
            }
        }

        if(recreateTerminators){
            idx--;
            mSceneTree.insert(mSceneTree.begin() + idx, CHILD);
            //mSceneTree.insert(mSceneTree.begin() + idx + 2, TERM);
            mSceneTree.insert(mSceneTree.begin() + idx + 1, vec.begin() + start, vec.begin() + end);
            mSceneTree.insert(mSceneTree.begin() + idx + 1 + (end - start), TERM);
        }else{
            mSceneTree.insert(mSceneTree.begin() + idx, vec.begin() + start, vec.begin() + end);
        }
        _debugPrint();
    }

    bool SceneTreeManager::deleteObjectFromTree(EntryId resultId, int resultIndex){
        if(resultIndex < 0){
            resultIndex = _findEntryIdIndexInTree(resultId);
        }

        assert(mSceneTree[resultIndex].id == resultId);

        if(mSceneTree[resultIndex].type == SceneEntryType::treeChild){
            _recursiveDeleteInTree(resultIndex);
        }else{
            assert(mSceneTree[resultIndex].type != SceneEntryType::treeTerminator && mSceneTree[resultIndex].type != SceneEntryType::treeChild);
            //recursive delete will work for both single items and those with children.
            _recursiveDeleteInTree(resultIndex);
        }

        bool onlyChild = false;
        {
            //We've only deleted the one entry.
            //We now need to check if the removed item was an only child of a scene tree. In this case the terminator values should be removed as well.
            if(mSceneTree[resultIndex].type == SceneEntryType::treeTerminator &&
                resultIndex - 1 > 0 && //We don't want negative numbers, but the item at index 0 will also be a tree child. We don't want to delete that.
                mSceneTree[resultIndex - 1].type == SceneEntryType::treeChild &&
                resultIndex - 2 > 0 && //The actual index of the parent.
                (mSceneTree[resultIndex - 2].type != SceneEntryType::treeChild && mSceneTree[resultIndex - 2].type != SceneEntryType::treeTerminator)){
                //The node is an only child.
                std::cout << "Only child" << '\n';

                mSceneTree.erase(mSceneTree.begin() + resultIndex - 1, mSceneTree.begin() + resultIndex + 1); //The end value is +1 so its inclusive.
                onlyChild = true;
            }
        }

        _debugPrint();

        return onlyChild;
    }

    bool SceneTreeManager::isObjectOnlyChild(EntryId obj) const{
        if(mSceneTree[obj].type == SceneEntryType::treeTerminator || mSceneTree[obj].type == SceneEntryType::treeChild) return false;

        if(
        obj - 1 > 0 && //We don't want negative numbers, but the item at index 0 will also be a tree child, so don't count that.
        mSceneTree[obj - 1].type == SceneEntryType::treeChild &&
        obj + 1 < mSceneTree.size() - 1 && //-1 for the final terminator.
        mSceneTree[obj + 1].type == SceneEntryType::treeTerminator
        ){
            return true;
        }
        return false;
    }

    int SceneTreeManager::_recursiveDeleteInTree(int index){
        bool isTerminator = mSceneTree[index].type == SceneEntryType::treeChild;
        bool hasChildren = itemHasChildren(mSceneTree, index);

        int entriesDeleted = 0;

        //Done incase of something like insertion into an object causing the creation of child nodes.
        //In this case we want to destroy the children and the terminators, but not the item itself.

        //If we're trying to delete a terminator then just take the index. Otherwise we need to check if it has children to delete those.
        int itemIndex = index;
        if(!isTerminator){
            //If the item does have children switch the index to check to that.
            if(hasChildren) itemIndex++;
        }

        int targetIdx = utilGetTerminatorForChild(itemIndex, mSceneTree) - 1;
        if(targetIdx < 0){ //This will be true if the item has no children, as the terminator was not found.
            targetIdx = index;
            assert(!hasChildren);
        }
        if(hasChildren) assert(mSceneTree[targetIdx].type == SceneEntryType::treeTerminator);

        //Go through and recycle all the entry ids between the terminators.
        bool selectedItemDeleted = false;
        for(int i = index; i <= targetIdx; i++){
            if(mSceneTree[i].type == SceneEntryType::treeTerminator || mSceneTree[i].type == SceneEntryType::treeChild){
                continue;
            }
            //Check if that item is part of the selected list. If it is it should be removed.
            auto it = mSelectedIds.find(mSceneTree[i].id);
            if(it != mSelectedIds.end()){
                mSelectedIds.erase(it);
                selectedItemDeleted = true;
            }

            entriesDeleted++;
            _returnEntryId(mSceneTree[i].id);
        }

        //TODO this has the potential to leave terminators behind, for instance if it deletes an only child, the wrapper terminators will remain. I need to think that through.

        mSceneTree.erase(mSceneTree.begin() + index, mSceneTree.begin() + targetIdx + 1);

        if(selectedItemDeleted){
            _selectedItemsChanged();
        }

        return entriesDeleted;
    }

    EntryId SceneTreeManager::_getEntryId(){
        if(!mRecycledIds.empty()){
            EntryId retVal = mRecycledIds.top();
            mRecycledIds.pop();

            return retVal;
        }

        return mUsedIds++;
    }

    int SceneTreeManager::_getParentIndexOfEntry(int entry) const{
        return SceneTreeManager::utilGetParentIndexOfEntry(entry, mSceneTree);
    }

    bool SceneTreeManager::setItemVisibility(EntryId id, bool visible, OgreTreeManager* ogreMan){
        int idx = _findEntryIdIndexInTree(id);
        SceneEntry& e = mSceneTree[idx];
        assert(e.id != 0); //Not a terminator.

        e.visible = visible;

        //Check what the values of the children are (if it has any children).
        //Don't change the sceneEntry visibility values, but change the ogre nodes depending.
        if(itemHasChildren(mSceneTree, idx)){
            auto it = mSceneTree.begin() + idx + 1;
            assert((*it).type == SceneEntryType::treeChild);

            int childIndex = 0;
            while(it != mSceneTree.end()){
                const SceneEntry& current = *it;

                if(current.type == SceneEntryType::treeChild) childIndex++;
                else if(current.type == SceneEntryType::treeTerminator){
                    childIndex--;
                    if(childIndex == 0){
                        break;
                    }
                }else{
                    //An actual object.
                    if(current.visible){
                        ogreMan->setTreeItemVisible(current.id, visible);
                    }
                }
                it++;
            }

        }

        //The object is being set to invisible, so there's no need to do that loop over the list to check if the item should remain visible.
        if(!visible) return false;

        //By this point we're trying to make the item visible.
        auto it = mSceneTree.begin() + idx;

        bool nextItem = false;
        //while(it != mSceneTree.begin() - 1){ //-1 to make sure we factor in the first entry as well.
        do{
            const SceneEntry& c = *it;
            if(nextItem){
                assert(c.type != SceneEntryType::treeTerminator && c.type != SceneEntryType::treeChild);
                if(!c.visible){
                    //One of the parent items wasn't found to be visible, so this one can't be made visible.
                    return false;
                }
                nextItem = false;
            }
            if(c.type == SceneEntryType::treeChild){
                nextItem = true;
            }

            it--;
        }
        while(it != mSceneTree.begin()); //-1 to make sure we factor in the first entry as well.

        return true;
    }

    EntryId SceneTreeManager::getIdOfParent(int idx) const{
        int parent = _getParentIndexOfEntry(idx) - 1;
        if(parent < 0) return ROOT_NODE;
        //assert(parent != 0);

        return mSceneTree[parent].id;
    }

    void SceneTreeManager::_returnEntryId(EntryId id){
        mRecycledIds.push(id);
    }

    void SceneTreeManager::alterTreeItemExpansion(EntryId item, int entryPos, bool expand, bool recursiveExpand){
        SceneEntry& e = mSceneTree[entryPos];
        int itemChildIndex = entryPos + 1;

        assert(e.id == item);
        assert(mSceneTree[itemChildIndex].type == SceneEntryType::treeChild);
        e.expanded = expand;

        if(recursiveExpand){
            int endIndex = utilGetTerminatorForChild(itemChildIndex, mSceneTree) - 1;
            for(int i = entryPos; i < endIndex; i++){
                if(itemHasChildren(mSceneTree, i)){
                    assert(mSceneTree[i + 1].type == SceneEntryType::treeChild);
                    mSceneTree[i].expanded = expand;
                }
            }
        }
    }

    void SceneTreeManager::_selectedItemsChanged(){
        SceneTreeSelectionChangedEvent e;
        e.selectionCount = mSelectedIds.size();
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void SceneTreeManager::_clearAllSelection(){
        mSelectedIds.clear();
        mMostRecentSelection = 0;
        mTerrainSelected = false;
    }

    void SceneTreeManager::clearAllSelection(){
        _clearAllSelection();
        _selectedItemsChanged();
    }

    void SceneTreeManager::_setSelectedId(EntryId item){
        mSelectedIds.insert(item);
    }

    void SceneTreeManager::setSingleSelection(EntryId item){
        _clearAllSelection();
        mSelectedIds.insert(item);
        _selectedItemsChanged();
    }

    void SceneTreeManager::setSelectionById(EntryId item){
        int idx = _findEntryIdIndexInTree(item);
        if(idx < 0) return;

        _clearAllSelection();
        notifySelection(item, idx, false, false);
    }

    const std::string& SceneTreeManager::getNameForEntry(EntryId id){
        int idx = _findEntryIdIndexInTree(id);
        static std::string retVal("");
        if(idx < 0) return retVal;

        return mSceneTree[idx].name;
    }

    void SceneTreeManager::selectAll(){
        _clearAllSelection();
        for(int i = 0; i < mSceneTree.size(); i++){
            const SceneEntry& e = mSceneTree[i];
            if(e.type == SceneEntryType::treeChild || e.type == SceneEntryType::treeTerminator) continue;
            mSelectedIds.insert(e.id);
        }
        _selectedItemsChanged();
    }

    void SceneTreeManager::notifySelection(EntryId item, int entryIndex, bool ctrlModifier, bool shiftModifier){
        assert(mSceneTree[entryIndex].id == item);

        if(!ctrlModifier && !shiftModifier){
            if(mSelectedIds.find(item) == mSelectedIds.end()){
                //For dragging, we don't want to deselect the other items if an item which is already selected is again selected.
                mSelectedIds.clear();
            }
        }

        if(shiftModifier && mMostRecentSelection != 0){
            int target = _findEntryIdIndexInTree(mMostRecentSelection);
            if(target >= 0){
                int start = target;
                int end = entryIndex;

                if(entryIndex < target){
                    start = entryIndex;
                    end = target;
                }

                for(int i = start; i < end; i++){
                    if(mSceneTree[i].id == 0) continue;
                    mSelectedIds.insert(mSceneTree[i].id);
                }
            }

        }

        mMostRecentSelection = item;
        mSelectedIds.insert(item);
        mTerrainSelected = false;

        _selectedItemsChanged();
    }

    void SceneTreeManager::notifyTerrainSelected(){
        _clearAllSelection();
        mTerrainSelected = true;
        _selectedItemsChanged();
    }

    void SceneTreeManager::_setTerrainSelected(bool selected){
        if(selected) assert(mSelectedIds.empty());
        mTerrainSelected = selected;
    }

    int SceneTreeManager::_findEntryIdIndexInTree(EntryId item) const{
        return SceneTreeManager::utilFindEntryIdIndexInTree(item, mSceneTree);
    }

    bool SceneTreeManager::_isParentSelected(int entryId) const{
        int parentIdx = _getParentIndexOfEntry(entryId) - 1;

        if(parentIdx <= 0) return false; //We've reached the root node, so not found
        if(mSelectedIds.find(mSceneTree[parentIdx].id) != mSelectedIds.end()){ //The item was found in the list.
            return true;
        }

        return _isParentSelected(parentIdx);
    }

    std::set<EntryId> SceneTreeManager::getReducedSelection() const{
        std::set<EntryId> mRemovedItems;

        for(EntryId id : mSelectedIds){
            if(_isParentSelected(_findEntryIdIndexInTree(id))) continue;
            mRemovedItems.insert(id);
        }

        return mRemovedItems;
    }

    void SceneTreeManager::insertObjectIntoTreeFromSelection(const Resource& res, const ResourceFSEntry& resFS, const Ogre::Vector3& pos) const{
        const std::set<EntryId> reducedSelection = getReducedSelection();
        EntryId targetId = ROOT_NODE;
        if(!reducedSelection.empty()){
            //Take the first id for insertion.
            targetId = *(reducedSelection.begin());
        }

        ObjectInsertionType targetInsertionData = targetId == ROOT_NODE ? ObjectInsertionType::below : ObjectInsertionType::into;
        ObjectInsertionAction* a = new ObjectInsertionAction(targetId, targetInsertionData);

        a->populate(res, resFS, pos);
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
    }
    void SceneTreeManager::insertSceneTreeFileIntoTreeFromSelection(const std::string& file, const Ogre::Vector3& pos) const{
        const std::set<EntryId> reducedSelection = getReducedSelection();
        EntryId targetId = ROOT_NODE;
        if(!reducedSelection.empty()){
            //Take the first id for insertion.
            targetId = *(reducedSelection.begin());
        }

        ObjectInsertionType targetInsertionData = targetId == ROOT_NODE ? ObjectInsertionType::below : ObjectInsertionType::into;
        ScenePasteAction* a = new ScenePasteAction(targetId, targetInsertionData, pos);

        SceneFileSceneTreeParserInterface interface(
            EditorSingleton::getEditor()->getSceneTreeManager(), EditorSingleton::getEditor()->getOgreTreeManager(), EditorSingleton::getEditor()->getTreeObjectManager()
        );
        bool result = AV::AVSceneFileParser::loadFile(file, &interface);
        a->populateFromSceneFileInterface(interface);

        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
    }

    void SceneTreeManager::saveCurrentSelection(){
        auto sceneManager = EditorSingleton::getEditor()->getSceneTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto objectMan = EditorSingleton::getEditor()->getTreeObjectManager();

        SceneFileExporter s(sceneManager, treeMan, objectMan);
        std::vector<int> highlightedIndexes;

        const std::set<EntryId> reducedSelection = getReducedSelection();
        for(EntryId i : reducedSelection){
            int idxCount = 0;
            bool found = false;
            for(int y = 0; y < mSceneTree.size(); y++){
                const SceneEntry& e = mSceneTree[y];
                if(e.id == i){
                    found = true;
                    highlightedIndexes.push_back(y);
                }
            }
            assert(found);
        }


        nfdchar_t *outPath = NULL;
        nfdresult_t result = NFD_SaveDialog(&outPath, NULL, 0, NULL, NULL);

        if ( result == NFD_OKAY ) {
            s.exportSceneFile(std::string(outPath), highlightedIndexes, getSceneTree());

            free(outPath);
        }
    }

    void SceneTreeManager::removeInvalidSelections(){
        std::set<EntryId> toRemove;

        for(EntryId i : mSelectedIds){
            bool found = false;
            for(const SceneEntry& e : mSceneTree){
                if(e.id == i){
                    found = true;
                    break;
                }
            }

            if(!found) toRemove.insert(i);
        }

        for(EntryId i : toRemove){
            mSelectedIds.erase(i);
        }
    }

    void SceneTreeManager::deleteCurrentSelection(){
        std::set<Southsea::EntryId> selection = getReducedSelection();
        if(selection.empty()) return;
        ObjectDeleteAction *a = new ObjectDeleteAction();
        a->populate(selection);
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
    }

    EntryId SceneTreeManager::getFirstSelection() const {
        if(mSelectedIds.empty()) return INVALID_ID;
        return *(mSelectedIds.begin());
    }

    /**
    Static utility items
    */
    int SceneTreeManager::utilFindEntryIdIndexInTree(EntryId item, const std::vector<SceneEntry>& list){
        for(int i = 0; i < list.size(); i++){
            if(list[i].id == item) return i;
        }

        return -1;
    }

    int SceneTreeManager::utilGetParentIndexOfEntry(int entry, const std::vector<SceneEntry>& list){
        auto it = list.begin() + entry;

        int childCount = 0;
        do{
            const SceneEntry& c = *it;
            if(c.type == SceneEntryType::treeTerminator) childCount++;
            if(c.type == SceneEntryType::treeChild){
                if(childCount == 0){
                    return entry;
                }
                childCount--;
            }

            it--;
            entry--;
        }while(it != list.begin());

        //If we're at index 0 and child count 0 then the root node was found as the parent.
        if(childCount == 0){
            return 0;
        }

        //Nothing was found, and this most likely means a malformed scene tree.
        return -1;
    }

}
