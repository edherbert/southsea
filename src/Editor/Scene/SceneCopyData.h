#pragma once

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    struct CopiedSelectionInfo{
        int idx;
        int start;
        int end;

        int objStart;
        int objEnd;
        EntryId parentNode;
    };
}
