#pragma once

#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/SceneTypes.h"
#include "Editor/Scene/SceneCopyData.h"
#include <vector>

namespace Southsea{
    class ScenePasteAction;

    /**
    A class to keep track of the currently copied items in the scene tree.
    */
    class CopyStateManager{
    public:
        CopyStateManager();
        ~CopyStateManager();

        /**
        Copy whatever's currently selected in the scene tree.
        */
        void copyCurrentSelection();

        /**
        Populate an action with the current copied state.
        */
        void populatePasteAction(ScenePasteAction* a);

        /**
        Populate the paste action as a duplicate action.
        */
        void populatePasteActionForDuplicate(ScenePasteAction* a);

    private:

        std::vector<SceneEntry> mCopiedSceneEntries;
        std::vector<CopiedSelectionInfo> mEntrySectionList;
        //Ogre objects
        std::vector<ObjectData*> mObjectData;
        std::vector<OgreTreeManager::TreeNodeCreationData> mTreeCreationData;

        void _clearAllState();
    };
}
