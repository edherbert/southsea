#include "SceneRaycastHelper.h"

#include "Editor/EditorSingleton.h"
#include "Editor/ui/ObjectCoordsGizmo.h"
#include "OgreTreeManager.h"
#include "SceneTreeManager.h"

#include "Ogre.h"

namespace Southsea{
    Ogre::SceneManager* SceneRaycastHelper::mSceneManager = 0;
    Ogre::Camera* SceneRaycastHelper::mCamera = 0;
    Ogre::RaySceneQuery* SceneRaycastHelper::mSceneQuery = 0;

    void SceneRaycastHelper::populateSceneManager(Ogre::SceneManager* sceneManager, Ogre::Camera* camera){
        mSceneManager = sceneManager;
        mCamera = camera;

        mSceneQuery = mSceneManager->createRayQuery(Ogre::Ray());
    }

    bool SceneRaycastHelper::castRayInScene(float mouseX, float mouseY){
        auto editor = EditorSingleton::getEditor();
        Ogre::Ray ray = mCamera->getCameraToViewportRay(mouseX, mouseY);

        mSceneQuery->setRay(ray);
        const Ogre::RaySceneQueryResult& result = mSceneQuery->execute();

        if(result.empty()) return false;

        auto coordsGizmo = editor->getObjectCoordsGizmo();
        for(const Ogre::RaySceneQueryResultEntry& e : result){
            CoordsGizmoType type = coordsGizmo->isItemCoordWidget(e.movable);
            if(type == CoordsGizmoType::none || type == CoordsGizmoType::boundingBox) continue;

            coordsGizmo->notifyMouseInteract(type);
            return true;
        }

        //Now that the list has been searched for gizmo objects we can pick an object to select.
        Ogre::Real lowestDistance = 99999999999;
        EntryId lowestId = INVALID_ID;
        for(const Ogre::RaySceneQueryResultEntry& e : result){
            if(e.distance <= 0) continue;
            if(e.distance > lowestDistance) continue;

            assert(e.movable);
            Ogre::SceneNode* node = e.movable->getParentSceneNode();
            EntryId id = editor->getOgreTreeManager()->getIdFromNodeCheck(node);
            if(id == INVALID_ID) continue; //It's found a node with no object attached to it, so just skip it.

            //By this point this is the new lowest, so set it.
            lowestDistance = e.distance;
            lowestId = id;
        }

        if(lowestId != INVALID_ID){
            editor->getSceneTreeManager()->setSingleSelection(lowestId);
            return false;
        }

        //At this point no object was found, so just clear the selection.
        editor->getSceneTreeManager()->clearAllSelection();

        return false;
    }

    bool SceneRaycastHelper::castRayToList(float mouseX, float mouseY, std::vector<RaycastListEntry>& outList){
        //TODO remove duplication.
        outList.clear();
        auto editor = EditorSingleton::getEditor();
        Ogre::Ray ray = mCamera->getCameraToViewportRay(mouseX, mouseY);

        mSceneQuery->setRay(ray);
        const Ogre::RaySceneQueryResult& result = mSceneQuery->execute();

        if(result.empty()) return false;

        auto coordsGizmo = editor->getObjectCoordsGizmo();
        for(const Ogre::RaySceneQueryResultEntry& e : result){
            CoordsGizmoType type = coordsGizmo->isItemCoordWidget(e.movable);
            if(type == CoordsGizmoType::none || type == CoordsGizmoType::boundingBox) continue;

            coordsGizmo->notifyMouseInteract(type);
            return true;
        }

        for(const Ogre::RaySceneQueryResultEntry& e : result){
            if(e.distance <= 0) continue;

            assert(e.movable);
            Ogre::SceneNode* node = e.movable->getParentSceneNode();
            EntryId id = editor->getOgreTreeManager()->getIdFromNodeCheck(node);
            if(id == INVALID_ID) continue;

            const std::string& foundName = editor->getSceneTreeManager()->getNameForEntry(id);
            outList.push_back({id, foundName});
        }

        return false;
    }
}
