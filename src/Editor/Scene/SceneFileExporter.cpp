#include "SceneFileExporter.h"

#include <cassert>
#include "Editor/EditorSingleton.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"

#include "OgreSceneNode.h"

namespace Southsea{

    SceneFileExporter::SceneFileExporter(const std::shared_ptr<SceneTreeManager> sceneMan, const std::shared_ptr<OgreTreeManager> ogreMan, const std::shared_ptr<TreeObjectManager> treeMan)
        : mSceneMan(sceneMan),
        mOgreMan(ogreMan),
        mTreeMan(treeMan) {

    }

    SceneFileExporter::~SceneFileExporter(){

    }

     tinyxml2::XMLElement* SceneFileExporter::_exportObject(tinyxml2::XMLDocument& xmlDoc, tinyxml2::XMLElement* parent, const SceneEntry& e){
        tinyxml2::XMLElement* child = 0;

        if(e.type == SceneEntryType::mesh){
            const ObjectData* objData = mTreeMan->getDataObject(e.id);
            child = xmlDoc.NewElement("mesh");

            assert(objData->type() == ObjectType::mesh);
            const MeshObjectData* meshData = dynamic_cast<const MeshObjectData*>(objData);
            child->SetAttribute("mesh", meshData->meshName.c_str());
        }
        else{
            child = xmlDoc.NewElement("empty");
        }

        child->SetAttribute("name", e.name.c_str());

        { //Export generic objects.
            Ogre::SceneNode* node = mOgreMan->getNodeById(e.id);

            const Ogre::Vector3 position = node->getPosition();
            if(position != Ogre::Vector3::ZERO){
                tinyxml2::XMLElement* posChild = xmlDoc.NewElement("position");
                posChild->SetAttribute("x", position.x);
                posChild->SetAttribute("y", position.y);
                posChild->SetAttribute("z", position.z);
                child->InsertEndChild(posChild);
            }

            const Ogre::Vector3 scale = node->getScale();
            if(scale != Ogre::Vector3(1, 1, 1)){
                tinyxml2::XMLElement* scaleChild = xmlDoc.NewElement("scale");
                scaleChild->SetAttribute("x", scale.x);
                scaleChild->SetAttribute("y", scale.y);
                scaleChild->SetAttribute("z", scale.z);
                child->InsertEndChild(scaleChild);
            }

            const Ogre::Quaternion orientation = node->getOrientation();
            if(orientation != Ogre::Quaternion::IDENTITY){
                tinyxml2::XMLElement* orientationChild = xmlDoc.NewElement("orientation");
                orientationChild->SetAttribute("x", orientation.x);
                orientationChild->SetAttribute("y", orientation.y);
                orientationChild->SetAttribute("z", orientation.z);
                orientationChild->SetAttribute("w", orientation.w);
                child->InsertEndChild(orientationChild);
            }
        }


        parent->InsertEndChild(child);
        return child;
    }

     //TODO get rid of layer.
    int SceneFileExporter::_recursiveExportChildNode(tinyxml2::XMLDocument& xmlDoc, tinyxml2::XMLElement* parent, int index, int layer, const std::vector<SceneEntry>& sceneTree, bool& result){
        assert(sceneTree[index].type == SceneEntryType::treeChild);

        int i = index;
        tinyxml2::XMLElement* prevElement = 0;
        for(; i < sceneTree.size(); i++){
            const SceneEntry& e = sceneTree[i];

            if(e.type == SceneEntryType::treeChild){
                if(prevElement){
                    int newIndex = _recursiveExportChildNode(xmlDoc, prevElement, i, layer+1, sceneTree, result);
                    i = newIndex;
                }
            }
            else if(e.type == SceneEntryType::treeTerminator){
                return i + 1;
            }else{
                tinyxml2::XMLElement* newElem = _exportObject(xmlDoc, parent, sceneTree[i]);
                parent->InsertEndChild(newElem);
                prevElement = newElem;
            }
        }

        return i;
    }

    int SceneFileExporter::_exportSceneNode(tinyxml2::XMLDocument& xmlDoc, tinyxml2::XMLElement* parent, int index, int layer, const std::vector<SceneEntry>& sceneTree, bool& result){
        tinyxml2::XMLElement* newElem = _exportObject(xmlDoc, parent, sceneTree[index]);
        //Check if it has children.
        if(index + 1 >= sceneTree.size()){
            //No children and reached end of list.
            result = true;
            return 0;
        }
        if(sceneTree[index + 1].type == SceneEntryType::treeChild){
            //This object has children so export them along with the object.
            _recursiveExportChildNode(xmlDoc, newElem, index + 1, 0, sceneTree, result);
        }

        result = true;
        return 0;
    }

    bool SceneFileExporter::exportSceneFile(const std::string& path, const std::vector<int>& indexes, const std::vector<SceneEntry>& sceneTree){
        std::cout << "Saving scene to " << path << std::endl;

        tinyxml2::XMLDocument xmlDoc;
        tinyxml2::XMLElement* pRoot = xmlDoc.NewElement("scene");
        xmlDoc.InsertFirstChild(pRoot);

        for(int entryIndex : indexes){
            //The user shouldn't be able to select terminators.
            assert(sceneTree[entryIndex].type != SceneEntryType::treeChild && sceneTree[entryIndex].type != SceneEntryType::treeTerminator);

            bool newResult = true;
            _exportSceneNode(xmlDoc, pRoot, entryIndex, 1, sceneTree, newResult);
            if(!newResult) return false;
        }

        xmlDoc.SaveFile(path.c_str());

        return true;
    }
}

