#pragma once

#include "SceneTypes.h"
#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{

    SceneEntryType _convertSceneTypeTo(ResourceType type){
        switch(type){
            case ResourceType::mesh: return SceneEntryType::mesh;
            default: return SceneEntryType::empty;
        }
    }

}
