#pragma once

#include "OgrePrerequisites.h"
#include "OgreVector3.h"
#include "OgreQuaternion.h"
#include "SceneTypes.h"

#include <map>

namespace Southsea{
    class TreeObjectManager;
    class SceneSerialiser;

    class OgreTreeManager{
        friend SceneSerialiser;
    public:
        OgreTreeManager();
        ~OgreTreeManager();

        struct TreeNodeCreationData{
            Ogre::Vector3 position;
            Ogre::Vector3 scale;
            Ogre::Quaternion orientation;

        };
        static const TreeNodeCreationData ZERO;

        void initialise(Ogre::SceneManager* sceneManager);

        Ogre::SceneNode* getRootSceneNode() const { return mRootNode; }
        Ogre::SceneNode* getNodeById(EntryId id) const;

        Ogre::SceneNode* createTreeItem(EntryId id, EntryId parent, const TreeNodeCreationData* creationData = 0);
        Ogre::SceneNode* _createTreeItem(EntryId id, Ogre::SceneNode* parentNode, const TreeNodeCreationData* creationData = 0);
        void destroyTreeItem(EntryId id, TreeObjectManager* treeMan = 0);
        void reparentTreeItem(EntryId id, EntryId newParent);

        void clearCurrentContent(TreeObjectManager* treeMan);

        void setTreeItemVisible(EntryId id, bool visible);

        void populateVectorWithNodeData(EntryId id, std::vector<ObjectData*>& objVec, std::vector<TreeNodeCreationData>& treeVec, TreeObjectManager* treeMan, Ogre::SceneNode* node = 0);

        EntryId getParentOfEntry(EntryId id);

        void repopulateTree(const std::vector<SceneEntry>& sceneEntries, const std::vector<ObjectData*>& objData, const std::vector<TreeNodeCreationData>& treeVec, int start, int end, int objStart, int objEnd, EntryId parentId, TreeObjectManager* treeMan);

        static ObjectData* cloneObjectData(const ObjectData* data);

        /**
        Get an id for a scene node, first checking if the node even exists in the system.
        */
        EntryId getIdFromNodeCheck(Ogre::SceneNode* node) const;

        typedef std::map<EntryId, Ogre::SceneNode*> TreeData;
        const TreeData& getTreeData() const { return mEntryMap; }

    private:
        Ogre::SceneNode* mRootNode = 0;
        Ogre::SceneManager* mSceneManager = 0;

        TreeData mEntryMap;

        void _debugPrintNode(Ogre::SceneNode* node, int childCount);
        void _debugPrintTree(){ _debugPrintNode(mRootNode, 0); }

        void _recursiveDestroyNode(Ogre::SceneNode* node, TreeObjectManager* treeMan = 0);

        inline EntryId getIdFromNode(Ogre::SceneNode* node) const;

        std::pair<int,int> _insertItems(const std::vector<SceneEntry>& sceneEntries, const std::vector<ObjectData*>& objData, const std::vector<TreeNodeCreationData>& treeVec, int currentIdx, int endIdx, int currentObj, Ogre::SceneNode* currentParent, TreeObjectManager* treeMan);
    public:

        static Ogre::SceneNode* utilGetNodeById(const TreeData& data, EntryId id);
    };
}
