#pragma once

namespace Ogre{
    class SceneNode;
    class SceneManager;
}

#include "SceneTypes.h"
#include <map>

#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{
    class PhysicsBodyDrawer;

    /**
    A class to manage objects within the tree.
    There are a number of object types that can be inserted into the tree. This class is responsible for their creation and destruction.
    */
    class TreeObjectManager{
    public:
        TreeObjectManager();
        ~TreeObjectManager();

        void initialise(Ogre::SceneManager* sceneManager);

        ObjectData* constructObjectDataFromResource(const Resource& resource, const ResourceFSEntry& fsEntry);
        ObjectData* constructPhysicsData(PhysicsShapeType type);
        ObjectData* constructCollisionSenderData();
        ObjectData* constructNavMarkerData();
        ObjectData* constructDataPointData();

        void insertObject(Ogre::SceneNode* node, EntryId resultId, ObjectData* objectData);
        void removeObject(Ogre::SceneNode* node, EntryId resultId);

        void changeOgreMesh(Ogre::SceneNode* node, EntryId id, const std::string& newMesh);
        void changeOgreMeshMaterial(Ogre::SceneNode* node, EntryId id, const std::string& newMaterial);
        void changePhysicsShape(Ogre::SceneNode* node, EntryId id, PhysicsShapeType newType);

        void changeDataPointType(Ogre::SceneNode* node, EntryId id, DataPointObjectData::Type type, int newValue);

        /**
        Deals with setting the correct data for a nav marker so that it contributes to a mesh.
        This function is intended to be called by the NavMeshManager only.
        */
        void setNavMeshMarkerContribution(EntryId markerId, NavMeshId meshId, bool contributes);

        const ObjectData* getDataObject(EntryId id) const;
        CollisionSenderObjectData* getSenderObjectData(EntryId id);

        PhysicsBodyDrawer* getPhysicsBodyDrawer() { return mPhysicsBodyDrawer; }

        typedef std::map<EntryId, ObjectData*> TreeData;
        const TreeData& getObjectData() const { return mObjectData; }

    private:
        Ogre::SceneManager* mSceneManager;
        PhysicsBodyDrawer* mPhysicsBodyDrawer;

        TreeData mObjectData;

        void _destroyMovableObject(Ogre::SceneNode* node);

    public:
        static const ObjectData* utilGetDataObject(EntryId id, const TreeData& data);
    };
}
