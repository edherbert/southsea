#pragma once

#include "OgrePrerequisites.h"
#include "Editor/Scene/SceneTypes.h"

#include <set>

namespace Ogre{
    class HlmsDatablock;
    class SceneNode;
}

namespace Southsea{

    /**
    A class responsible for drawing physics shapes within the scene.

    It keeps track of these shapes, allowig quick access to them at a later date.
    */
    class PhysicsBodyDrawer{
    public:
        PhysicsBodyDrawer();
        ~PhysicsBodyDrawer();

        void initialise(Ogre::SceneManager* sceneManager);

        void createShapeForNode(Ogre::SceneNode* node, PhysicsShapeType shapeType);
        void destroyShapeForNode(Ogre::SceneNode* node);

    private:
        Ogre::SceneManager* mSceneManager;

        static const int NUM_DATABLOCKS = 3;
        Ogre::HlmsDatablock* mDatablocks[NUM_DATABLOCKS];
        static const char* mDbNames[NUM_DATABLOCKS];

        struct ShapeRepresentationData{
            Ogre::SceneNode* node;
            PhysicsShapeType type;
        };

        std::set<Ogre::SceneNode*> mShapeNodes;

        const char* _getShapeName(PhysicsShapeType type);

        void _setupDatablocks();
        void _destroyDatablocks();
    };

}
