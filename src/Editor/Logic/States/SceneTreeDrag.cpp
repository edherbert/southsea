#include "SceneTreeDrag.h"

namespace Southsea{

    StateType SceneTreeDrag::updateState(const StateInformation& info){

        if(!info.sceneTreeDrag) return StateType::Null;

        return StateType::SceneTreeDrag;
    }

}
