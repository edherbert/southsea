#pragma once

#include "StateInformation.h"

namespace Ogre{
    class Camera;
}

namespace Southsea{
    //A state to control the camera for the scene like an fps camera.
    class FPSCameraControllerState{
        FPSCameraControllerState() = delete;
        ~FPSCameraControllerState() = delete;

    public:
        static StateType updateState(const StateInformation& info);
        static void startState(const StateInformation& info);
        static void endState(const StateInformation& info);

    private:
        static inline float radians(float value){
            return value * (3.14159 / 180);
        }

        static void _pointCamera(Ogre::Camera* camera);
        static void _moveCamera(Ogre::Camera* camera);
    };
}
