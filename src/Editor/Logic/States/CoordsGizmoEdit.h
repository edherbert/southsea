#pragma once

#include "StateInformation.h"

namespace Southsea{

    //A state to represent the fact that a resource icon is being dragged around the screen.
    class CoordsGizmoEdit{
        CoordsGizmoEdit() = delete;
        ~CoordsGizmoEdit() = delete;

    public:
        static StateType updateState(const StateInformation& info);

        static void startState(const StateInformation& info);
        static void endState(const StateInformation& info);
    };
}
