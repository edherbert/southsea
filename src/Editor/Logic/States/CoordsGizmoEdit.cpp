#include "CoordsGizmoEdit.h"

#include "Editor/EditorSingleton.h"
#include "Editor/ui/ObjectCoordsGizmo.h"
#include "Editor/Action/Actions/Object/BasicCoordinatesChange.h"
#include "Editor/Action/ActionStack.h"

#include "imgui.h"

namespace Southsea{

    void CoordsGizmoEdit::startState(const StateInformation& info){
    }

    void CoordsGizmoEdit::endState(const StateInformation& info){
        const StateInformation::RenderWindow& win = info.window[0];

        auto coordsGizmo = EditorSingleton::getEditor()->getObjectCoordsGizmo();
        BasicCoordinatesChange *changeAction = new BasicCoordinatesChange();
        coordsGizmo->populateAction(changeAction);
        changeAction->performAction(); //This will update all the dialogs with the new positions.
        EditorSingleton::getEditor()->getActionStack()->pushAction(changeAction);

        const float ogreWinX = float(win.mousePosX) / float(win.windowWidth);
        const float ogreWinY = float(win.mousePosY) / float(win.windowHeight);

        coordsGizmo->updateMousePosition(ogreWinX, ogreWinY);
        coordsGizmo->notifyMouseInteract(CoordsGizmoType::none);
    }

    StateType CoordsGizmoEdit::updateState(const StateInformation& info){
        const StateInformation::RenderWindow& win = info.window[0];

        if(!win.mouseButtonLeft) return StateType::Null;

        if(win.mouseMoved){
            //Originally I had planned to use all the input as stateInformation, which was pretty much just a pre-populated bit of information about input.
            //Now I don't think that was such a good idea. It takes a while to populate it, and it's easier to just get the values like this.
            //I won't change what I already have though because I don't have the time budget.
            const ImGuiIO& io = ImGui::GetIO();

            auto coordsGizmo = EditorSingleton::getEditor()->getObjectCoordsGizmo();
            const float ogreWinX = float(win.mousePosX) / float(win.windowWidth);
            const float ogreWinY = float(win.mousePosY) / float(win.windowHeight);
            coordsGizmo->updateActionPosition(ogreWinX, ogreWinY);

            coordsGizmo->setUniversalScaleModifier(io.KeyShift);
        }

        return StateType::CoordsGizmoEdit;
    }

}
