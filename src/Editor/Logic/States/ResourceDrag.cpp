#include "ResourceDrag.h"

#include "Editor/EditorSingleton.h"
#include "Editor/ui/SceneObjectPreviewer.h"
#include "System/Editor/DraggedResourceInfo.h"
#include "System/BaseSingleton.h"

#include <iostream>

namespace Southsea{

    bool cursorInWindow = false;

    void setScenePreview(bool preview){
        std::shared_ptr<SceneObjectPreviewer> prev = EditorSingleton::getEditor()->getSceneObjectPreviewer();
        prev->shouldPreviewObject(preview);
    }

    StateType ResourceDrag::updateState(const StateInformation& info){

        if(info.resourceDrag != StateInformation::ResourceDragState::DRAGGING) return StateType::Null;

        const StateInformation::RenderWindow& win = info.window[0];

        if(cursorInWindow != win.cursorInWindow){
            //The cursor has either left or entered the window.
            setScenePreview(win.cursorInWindow);

            cursorInWindow = win.cursorInWindow;
        }
        if(win.cursorInWindow){

            const float ogreWinX = float(win.mousePosX) / float(win.windowWidth);
            const float ogreWinY = float(win.mousePosY) / float(win.windowHeight);
            if(win.mouseMoved){
                std::shared_ptr<SceneObjectPreviewer> prev = EditorSingleton::getEditor()->getSceneObjectPreviewer();
                prev->setCursorWindowPosition(ogreWinX, ogreWinY);
            }
        }

        return StateType::ResourceDrag;
    }

    void ResourceDrag::startState(const StateInformation& info){
        const Resource& res = BaseSingleton::getDraggedResourceInfo()->getCurrentDraggedResource();
        const ResourceFSEntry& resFS = BaseSingleton::getDraggedResourceInfo()->getCurrentDraggedResourceEntry();
        const std::string& fullPath = BaseSingleton::getDraggedResourceInfo()->getFullPath();
        EditorSingleton::getEditor()->getSceneObjectPreviewer()->setCurrentObjectPreview(res, resFS, fullPath);
    }

    void ResourceDrag::endState(const StateInformation& info){
        bool successfulDrag = info.resourceDrag == StateInformation::ResourceDragState::FINISHED;
        EditorSingleton::getEditor()->getSceneObjectPreviewer()->notifyDragEnded(successfulDrag);

        //Regardless of whether cursor in window was true or false, at the end of state reset all this.
        cursorInWindow = false;
        setScenePreview(false);
    }

}
