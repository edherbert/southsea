#include "NullState.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/ui/ObjectCoordsGizmo.h"
#include "Editor/Scene/SceneRaycastHelper.h"
#include <iostream>
#include "imgui.h"
#include "Platforms/Window/Input/KeyCodes.h"
#include "Editor/ui/EditorToolDialogManager.h"

#include "System/BaseSingleton.h"
#include "System/Editor/TerrainEditorSettings.h"

#include "OgreRay.h"
#include "OgreCamera.h"

namespace Southsea{

    StateType NullState::updateState(const StateInformation& info){
        for(int i = 0; i < info.numRenderWindows; i++){
            const StateInformation::RenderWindow& win = info.window[i];


            if(win.cursorInWindow){
                if(win.mouseButtonRight){
                    return StateType::FPSCameraController;
                }

                const float ogreWinX = float(win.mousePosX) / float(win.windowWidth);
                const float ogreWinY = float(win.mousePosY) / float(win.windowHeight);
                if(win.mouseMoved){
                    EditorSingleton::getEditor()->getObjectCoordsGizmo()->updateMousePosition(ogreWinX, ogreWinY);
                }

                //const Editor::CurrentlySelectedObject objectType = EditorSingleton::getEditor()->getCurrentlySelectedObject();

                if(win.mouseButtonLeftPressed){
                    const Editor* e = EditorSingleton::getEditor();

                    bool terrainSelected = e->getSceneTreeManager()->isTerrainSelected();
                    if(terrainSelected){
                        Ogre::Ray ray = e->getEditorCamera()->getCameraToViewportRay(ogreWinX, ogreWinY);
                        RayIntersectResult rayResult;
                        e->mEditorTerrainManager.checkRayIntersect(ray, rayResult);
                        if(rayResult.found){
                            //The ray hit the terrain, so an edit should begin.
                            return StateType::TerrainMouseEdit;
                        }
                    }else{
                        const ImGuiIO& io = ImGui::GetIO();

                        bool coordsGizmoCollision = false;
                        //Perform an object selection raycheck.
                        if(ImGui::GetIO().KeyAlt){
                            std::vector<RaycastListEntry> outList;
                            coordsGizmoCollision = SceneRaycastHelper::castRayToList(ogreWinX, ogreWinY, outList);
                            e->getEditorToolDialogManager()->setRaycastPickedObjects(outList);
                        }else{
                            coordsGizmoCollision = SceneRaycastHelper::castRayInScene(ogreWinX, ogreWinY);
                        }

                        if(coordsGizmoCollision) return StateType::CoordsGizmoEdit;
                    }
                }
                const ImGuiIO& io = ImGui::GetIO();
                if(io.KeyCtrl){
                    const Editor* e = EditorSingleton::getEditor();
                    auto s = BaseSingleton::getTerrainEditorSettings();
                    float brushValue = s->getHeightBrushValue();
                    if(e->getSceneTreeManager()->isTerrainSelected() && s->getCurrentTool() == TerrainEditorSettings::TerrainTool::SET_HEIGHT){
                        return StateType::TerrainHeightPick;
                    }
                }

            }

        }

        if(info.chunkSwitcherGridMove){
            return StateType::ChunkGridMove;
        }
        if(info.resourceDrag == StateInformation::ResourceDragState::DRAGGING){
            return StateType::ResourceDrag;
        }
        if(info.sceneTreeDrag){
            return StateType::SceneTreeDrag;
        }

        return StateType::Null;
    }

}
