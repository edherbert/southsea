#include "ChunkGridMove.h"

namespace Southsea{

    StateType ChunkGridMove::updateState(const StateInformation& info){

        if(!info.chunkSwitcherGridMove) return StateType::Null;

        return StateType::ChunkGridMove;
    }

}
