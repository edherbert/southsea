#pragma once

#include "StateInformation.h"

namespace Southsea{
    /**
    A state to keep track of when the user is moving the chunk switcher grid with the mouse.
    It has its own state because I want to make sure other actions aren't picked up on, for instance the terrain camera movement.
    */
    class ChunkGridMove{
        ChunkGridMove() = delete;
        ~ChunkGridMove() = delete;

    public:
        static StateType updateState(const StateInformation& info);
    };
}
