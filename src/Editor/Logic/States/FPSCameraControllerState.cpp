#include "FPSCameraControllerState.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/CommandEvent.h"
#include "Editor/ui/ObjectCoordsGizmo.h"

#include "Editor/Chunk/EditedChunk.h"
#include "System/Project/MapMetaFile.h"
#include "Editor/EditorSingleton.h"
#include "OgreCamera.h"

#include <SDL.h>

#include "imgui.h"

#include <iostream>

namespace Southsea{

    float yaw = -90.0f;
    float pitch = 0.0f;

    Ogre::Camera* sceneCam = 0;
    StateType FPSCameraControllerState::updateState(const StateInformation& info){

        for(int i = 0; i < info.numRenderWindows; i++){
            const StateInformation::RenderWindow& win = info.window[i];

            if(!win.mouseButtonRight){
                return StateType::Null;
            }

        }

        _pointCamera(sceneCam);
        _moveCamera(sceneCam);

        return StateType::FPSCameraController;
    }

    void FPSCameraControllerState::startState(const StateInformation& info){
        //Request for the cursor to be grabbed.
        CommandEventGrabCursor e;
        e.grabCursor = true;

        sceneCam = EditorSingleton::getEditor()->getEditorCamera();

        EventDispatcher::transmitEvent(EventType::Command, e);

        yaw = EditorSingleton::getEditor()->getEditorChunk()->getChunkMetaFile().getCameraYaw();
        pitch = EditorSingleton::getEditor()->getEditorChunk()->getChunkMetaFile().getCameraPitch();
    }

    void FPSCameraControllerState::endState(const StateInformation& info){
        CommandEventGrabCursor e;
        e.grabCursor = false;

        sceneCam = 0;

        EventDispatcher::transmitEvent(EventType::Command, e);
    }

    void FPSCameraControllerState::_pointCamera(Ogre::Camera* camera){
        const ImGuiIO& io = ImGui::GetIO();

        float sense = 0.05;
        float xCamera = io.MouseDelta.x;
        float yCamera = -io.MouseDelta.y; //Had to invert this for some reason.
        xCamera *= sense;
        yCamera *= sense;

        yaw += xCamera;
        pitch += yCamera;
        if(pitch > 89.0f) pitch = 89.0f;
        if(pitch < -89.0f) pitch = -89.0f;
        if(yaw > 360.0f) yaw = 0.0f;
        if(yaw < 0.0f) yaw = 360.0f;

        Ogre::Vector3 front;
        front.x = cos(radians(yaw)) * cos(radians(pitch));
        front.y = sin(radians(pitch));
        front.z = sin(radians(yaw)) * cos(radians(pitch));

        front.normalise();
        camera->setDirection(front);

        EditorSingleton::getEditor()->setCameraOrientation(camera->getOrientation(), yaw, pitch);
    }

    void FPSCameraControllerState::_moveCamera(Ogre::Camera* camera){
        const ImGuiIO& io = ImGui::GetIO();

        Ogre::Vector3 amount = Ogre::Vector3::ZERO;
        if(io.KeysDown[SDL_SCANCODE_W]) amount += camera->getDirection();
        if(io.KeysDown[SDL_SCANCODE_S]) amount -= camera->getDirection();
        if(io.KeysDown[SDL_SCANCODE_A]) amount -= camera->getRight();
        if(io.KeysDown[SDL_SCANCODE_D]) amount += camera->getRight();
        if(amount == Ogre::Vector3::ZERO) return;

        if(io.KeyShift)
            amount *= 9;

        camera->move(amount);

        EditorSingleton::getEditor()->setCameraPosition(camera->getPosition());
    }
}
