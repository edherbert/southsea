#pragma once

#include "StateInformation.h"

namespace Southsea{
    //The 'default' state. Mostly just contains logic to switch to other states.
    class NullState{
        NullState() = delete;
        ~NullState() = delete;

    public:
        static StateType updateState(const StateInformation& info);
    };
}
