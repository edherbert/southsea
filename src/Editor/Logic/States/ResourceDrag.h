#pragma once

#include "StateInformation.h"

namespace Southsea{
    class TerrainMouseAction;

    //A state to represent the fact that a resource icon is being dragged around the screen.
    class ResourceDrag{
        ResourceDrag() = delete;
        ~ResourceDrag() = delete;

    public:
        static StateType updateState(const StateInformation& info);

        static void startState(const StateInformation& info);
        static void endState(const StateInformation& info);
    };
}
