#pragma once

namespace Southsea{
    /**
    Information for states to read and make decisions based on.
    This struct is going to contain data about render windows, as well as other window specific things.
    */
    struct StateInformation{
        struct RenderWindow{
            bool cursorInWindow, mouseMoved;
            int mousePosX, mousePosY;
            int windowWidth, windowHeight;
            bool mouseButtonLeft, mouseButtonRight; //0=left, 1=right
            bool mouseButtonLeftPressed;
        };

        static const int numRenderWindows = 1;
        RenderWindow window[numRenderWindows]; //In the future I might have specific render windows, so this helps to contain them.

        bool chunkSwitcherGridMove = false;
        bool sceneTreeDrag = false;

        enum class ResourceDragState{
            NOT_DRAGGING,
            DRAGGING,
            FINISHED,
            //The user pressed esc or otherwise cancelled the drag.
            ABORTED
        };
        ResourceDragState resourceDrag = ResourceDragState::NOT_DRAGGING;
    };

    enum class StateType{
        Null,
        FPSCameraController,
        TerrainMouseEdit,
        ChunkGridMove,
        ResourceDrag,
        SceneTreeDrag,
        CoordsGizmoEdit,
        TerrainHeightPick
    };

    //Reset a stateInformation struct to the default.
    static void resetStateInformation(StateInformation& info){
        for(int i = 0; i < StateInformation::numRenderWindows; i++){
            StateInformation::RenderWindow& win = info.window[i];

            win.cursorInWindow = false;
            win.mouseMoved = false;

            win.mousePosX = -1;
            win.mousePosY = -1;

            win.mouseButtonLeft = false;
            win.mouseButtonRight = false;
            win.mouseButtonLeftPressed = false;
        }
    }

}
