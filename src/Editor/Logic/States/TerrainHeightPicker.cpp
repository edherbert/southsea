#include "TerrainHeightPicker.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

#include "System/BaseSingleton.h"
#include "System/Editor/TerrainEditorSettings.h"

#include "Editor/Action/ActionStack.h"

#include "imgui.h"
#include "OgreRay.h"
#include "OgreCamera.h"


namespace Southsea{

    void TerrainHeightPicker::startState(const StateInformation& info){

    }

    void TerrainHeightPicker::endState(const StateInformation& info){
        //EditorSingleton::getEditor()->getActionStack()->pushAction(mAction);
        //mAction = 0;
    }

    StateType TerrainHeightPicker::updateState(const StateInformation& info){
        const ImGuiIO& io = ImGui::GetIO();
        for(int i = 0; i < info.numRenderWindows; i++){
            const StateInformation::RenderWindow& win = info.window[i];

            if(!win.cursorInWindow || !io.KeyCtrl){
                return StateType::Null;
            }

            const float ogreWinX = float(win.mousePosX) / float(win.windowWidth);
            const float ogreWinY = float(win.mousePosY) / float(win.windowHeight);

            const Editor* e = EditorSingleton::getEditor();

            Ogre::Ray ray = e->getEditorCamera()->getCameraToViewportRay(ogreWinX, ogreWinY);

            auto val = e->getEditorChunk()->getTerrainManager()->checkRayIntersect(ray);
            if(!val.first) {
                return StateType::TerrainHeightPick;
            }

            float height = e->getEditorChunk()->getTerrainManager()->getHeightAtPosition(val.second);
            std::shared_ptr<TerrainEditorSettings> s = BaseSingleton::getTerrainEditorSettings();
            s->setPreviewHeightBrushValue(height);
            if(win.mouseButtonLeft){
                s->setHeightBrushValue(height);
            }
        }

        return StateType::TerrainHeightPick;
    }

}
