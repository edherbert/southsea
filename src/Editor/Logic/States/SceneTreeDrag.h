#pragma once

#include "StateInformation.h"

namespace Southsea{
    class TerrainMouseAction;

    //A state to represent the fact that a resource icon is being dragged around the screen.
    class SceneTreeDrag{
        SceneTreeDrag() = delete;
        ~SceneTreeDrag() = delete;

    public:
        static StateType updateState(const StateInformation& info);
    };
}
