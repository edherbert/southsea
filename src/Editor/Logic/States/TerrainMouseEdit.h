#pragma once

#include "StateInformation.h"

namespace Southsea{
    class TerrainMouseAction;

    //A state to coordinate terrain edits with the mouse. This could include things like terrain height map, blend map or other manipulation
    class TerrainMouseEdit{
        TerrainMouseEdit() = delete;
        ~TerrainMouseEdit() = delete;

    public:
        static StateType updateState(const StateInformation& info);

        static void startState(const StateInformation& info);
        static void endState(const StateInformation& info);

        static TerrainMouseAction* mAction;
    };
}
