#include "TerrainMouseEdit.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "Editor/EditorTerrain/EditorTerrainManager.h"

#include "System/BaseSingleton.h"
#include "System/Editor/TerrainEditorSettings.h"

#include "Editor/Action/Actions/Terrain/TerrainMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainBlendMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainSmoothMouseAction.h"
#include "Editor/Action/ActionStack.h"

#include "OgreRay.h"
#include "OgreCamera.h"
#include <iostream>

namespace Southsea{

    TerrainMouseAction* TerrainMouseEdit::mAction = 0;

    void TerrainMouseEdit::startState(const StateInformation& info){
        TerrainEditorSettings::TerrainTool t = BaseSingleton::getTerrainEditorSettings()->getCurrentTool();
        if(t == TerrainEditorSettings::TerrainTool::RAISE_LOWER){
            mAction = new TerrainHeightMouseAction();
        }else if(t == TerrainEditorSettings::TerrainTool::BLEND_PAINT){
            mAction = new TerrainBlendMouseAction();
        }else if(t == TerrainEditorSettings::TerrainTool::SMOOTH){
            mAction = new TerrainSmoothMouseAction();
        }else if(t == TerrainEditorSettings::TerrainTool::SET_HEIGHT){
            mAction = new TerrainHeightMouseAction();
        }
    }

    void TerrainMouseEdit::endState(const StateInformation& info){
        EditorSingleton::getEditor()->getActionStack()->pushAction(mAction);
        mAction = 0;
    }

    StateType TerrainMouseEdit::updateState(const StateInformation& info){
        for(int i = 0; i < info.numRenderWindows; i++){
            const StateInformation::RenderWindow& win = info.window[i];

            if(!win.cursorInWindow || !win.mouseButtonLeft){
                return StateType::Null; //The edit has finished, so return.
            }

            const float ogreWinX = float(win.mousePosX) / float(win.windowWidth);
            const float ogreWinY = float(win.mousePosY) / float(win.windowHeight);

            Editor* e = EditorSingleton::getEditor();

            Ogre::Ray ray = e->getEditorCamera()->getCameraToViewportRay(ogreWinX, ogreWinY);

            RayIntersectResult rayResult;
            e->mEditorTerrainManager.checkRayIntersect(ray, rayResult);
            if(!rayResult.found){
                //If the mouse misses the terrain we don't necessarily end the state, just incase that was done accidentally.
                return StateType::TerrainMouseEdit;
            }

            std::shared_ptr<TerrainEditorSettings> s = BaseSingleton::getTerrainEditorSettings();

            TerrainEditorSettings::TerrainTool t = s->getCurrentTool();
            if(t == TerrainEditorSettings::TerrainTool::RAISE_LOWER){
                //By passing in the action, it gets updated with the new data.
                e->mEditorTerrainManager.applyHeightDiff(rayResult, s->getBrushData(), s->getBrushSize(), s->getBrushStrength(), mAction);
            }else if(t == TerrainEditorSettings::TerrainTool::BLEND_PAINT){
                e->mEditorTerrainManager.applyBlendDiff(rayResult, s->getBrushData(), s->getBrushSize(), s->getBrushStrength(), s->getCurrentDetailPaintLayer(), mAction);
            }else if(t == TerrainEditorSettings::TerrainTool::SMOOTH){
                e->mEditorTerrainManager.applySmoothDiff(rayResult, s->getBrushData(), s->getBrushSize(), s->getBrushStrength(), mAction);
            }else if(t == TerrainEditorSettings::TerrainTool::SET_HEIGHT){
                e->mEditorTerrainManager.applySetHeight(rayResult, s->getHeightBrushValue(), s->getBrushSize(), mAction);
            }
        }

        return StateType::TerrainMouseEdit;
    }

}
