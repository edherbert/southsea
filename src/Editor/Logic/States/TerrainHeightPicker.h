#pragma once

#include "StateInformation.h"

namespace Southsea{
    class TerrainMouseAction;

    //A state to check for height picking of the terrain with the mouse.
    class TerrainHeightPicker{
        TerrainHeightPicker() = delete;
        ~TerrainHeightPicker() = delete;

    public:
        static StateType updateState(const StateInformation& info);

        static void startState(const StateInformation& info);
        static void endState(const StateInformation& info);
    };
}
