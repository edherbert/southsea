#pragma once

#include <map>
#include <functional>
#include "States/StateInformation.h"

namespace Southsea{
    class Event;

    class EditorState{
    public:
        EditorState();
        ~EditorState();

        bool editorGuiEventReceiver(const Event &e);

        void update();

        StateType getCurrentState() const { return mCurrentState; }

    private:
        typedef std::function<StateType(const StateInformation&)> UpdateFunctionType;
        typedef std::function<void(const StateInformation&)> StartEndFunctionType;

        struct FunctionWrapper{
            StartEndFunctionType start;
            UpdateFunctionType update;
            StartEndFunctionType end;
        };

        inline const FunctionWrapper& getStateFunction(StateType state){
            //Because of const'ness I can't use the [] operator to get things from the map, so I have to do it like this.
            return EditorState::mFunctions.find(state)->second;
        }

        static const std::map<StateType, FunctionWrapper> mFunctions;

        StateType mCurrentState = StateType::Null;

        StateInformation mInfo;
    };
}
