#include "EditorState.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/Event.h"
#include "Event/Events/EditorGuiEvent.h"

#include "States/FPSCameraControllerState.h"
#include "States/NullState.h"
#include "States/TerrainMouseEdit.h"
#include "States/ChunkGridMove.h"
#include "States/ResourceDrag.h"
#include "States/SceneTreeDrag.h"
#include "States/CoordsGizmoEdit.h"
#include "States/TerrainHeightPicker.h"

namespace Southsea{

    //TODO it might have been better to use polymorphism here. Review that.
    const std::map<StateType, EditorState::FunctionWrapper> EditorState::mFunctions = {
        {StateType::Null, {0, NullState::updateState, 0}},
        {StateType::FPSCameraController, {FPSCameraControllerState::startState, FPSCameraControllerState::updateState, FPSCameraControllerState::endState}},
        {StateType::TerrainMouseEdit, {TerrainMouseEdit::startState, TerrainMouseEdit::updateState, TerrainMouseEdit::endState}},
        {StateType::ChunkGridMove, {0, ChunkGridMove::updateState, 0}},
        {StateType::ResourceDrag, {ResourceDrag::startState, ResourceDrag::updateState, ResourceDrag::endState}},
        {StateType::SceneTreeDrag, {0, SceneTreeDrag::updateState, 0}},
        {StateType::CoordsGizmoEdit, {CoordsGizmoEdit::startState, CoordsGizmoEdit::updateState, CoordsGizmoEdit::endState}},
        {StateType::TerrainHeightPick, {TerrainHeightPicker::startState, TerrainHeightPicker::updateState, TerrainHeightPicker::endState}}
    };

    EditorState::EditorState(){
        EventDispatcher::subscribe(EventType::EditorGui, SOUTHSEA_BIND(EditorState::editorGuiEventReceiver));

        resetStateInformation(mInfo);
    }

    EditorState::~EditorState(){
        EventDispatcher::unsubscribe(EventType::EditorGui, this);

        resetStateInformation(mInfo);
    }

    bool EditorState::editorGuiEventReceiver(const Event &e){
        const EditorGuiEvent& editorEvent = (const EditorGuiEvent&)e;

        if(editorEvent.eventCategory() == EditorGuiEventCategory::CursorLeftEnteredRenderWindow){
            const EditorGuiCursorLeftEnteredRenderWindow& event = (const EditorGuiCursorLeftEnteredRenderWindow&)editorEvent;

            mInfo.window[event.renderWindowId].cursorInWindow = !event.cursorLeft;
        }
        else if(editorEvent.eventCategory() == EditorGuiEventCategory::MouseMovedRenderWindow){
            const EditorGuiMouseMovedRenderWindow& event = (const EditorGuiMouseMovedRenderWindow&)editorEvent;
            mInfo.window[event.renderWindowId].mousePosX = event.mouseX;
            mInfo.window[event.renderWindowId].mousePosY = event.mouseY;
            mInfo.window[event.renderWindowId].mouseMoved = true;
        }
        else if(editorEvent.eventCategory() == EditorGuiEventCategory::MouseButtonRenderWindow){
            const EditorGuiMouseButtonRenderWindow& event = (const EditorGuiMouseButtonRenderWindow&)editorEvent;
            if(event.mouseButton == 0){
                if(event.buttonDown && !mInfo.window[event.renderWindowId].mouseButtonLeft){
                    mInfo.window[event.renderWindowId].mouseButtonLeftPressed = true;
                }
                mInfo.window[event.renderWindowId].mouseButtonLeft = event.buttonDown;
            }
            else if(event.mouseButton == 1){
                mInfo.window[event.renderWindowId].mouseButtonRight = event.buttonDown;
            }
        }
        else if(editorEvent.eventCategory() == EditorGuiEventCategory::ResizeRenderWindow){
            const EditorGuiWindowResize& event = (const EditorGuiWindowResize&)editorEvent;
            mInfo.window[event.renderWindowId].windowWidth = event.newWidth;
            mInfo.window[event.renderWindowId].windowHeight = event.newHeight;
        }
        else if(editorEvent.eventCategory() == EditorGuiEventCategory::ChunkSwitcherGridMove){
            const EditorGuiChunkSwitcherGridMouseMove& event = (const EditorGuiChunkSwitcherGridMouseMove&)editorEvent;

            mInfo.chunkSwitcherGridMove = event.gridMoveStarted;
        }
        else if(editorEvent.eventCategory() == EditorGuiEventCategory::ResourceDrag){
            const EditorGuiResourceDrag& event = (const EditorGuiResourceDrag&)editorEvent;

            StateInformation::ResourceDragState targetState = StateInformation::ResourceDragState::NOT_DRAGGING;
            if(event.dragType == DragEventType::Started) mInfo.resourceDrag = StateInformation::ResourceDragState::DRAGGING;
            else if(event.dragType == DragEventType::Completed) mInfo.resourceDrag = StateInformation::ResourceDragState::FINISHED;
            else if(event.dragType == DragEventType::Aborted) mInfo.resourceDrag = StateInformation::ResourceDragState::ABORTED;
        }
        else if(editorEvent.eventCategory() == EditorGuiEventCategory::SceneTreeDrag){
            const EditorGuiSceneTreeDrag& event = (const EditorGuiSceneTreeDrag&)editorEvent;
            mInfo.sceneTreeDrag = event.dragType == DragEventType::Started;
        }

        return false;
    }

    void EditorState::update(){
        StateType newState = getStateFunction(mCurrentState).update(mInfo);

        if(newState != mCurrentState){ //We're starting a new state, so call the start and end functions.
            const StartEndFunctionType& start = getStateFunction(newState).start;
            const StartEndFunctionType& end = getStateFunction(mCurrentState).end;

            //I don't actually mandate that the start and end functions are valid like I do for the update, so these have to be checked.
            if(start) start(mInfo);
            if(end) end(mInfo);

            mCurrentState = newState;
        }

        for(int i = 0; i < mInfo.numRenderWindows; i++){
            mInfo.window[i].mouseMoved = false;
            mInfo.window[i].mouseButtonLeftPressed = false;
        }
    }
}
