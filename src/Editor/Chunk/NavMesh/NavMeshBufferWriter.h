#pragma once

#include "NavMeshScanner.h"

namespace Southsea{
    class NavMeshBufferWriter{
    public:
        NavMeshBufferWriter() = delete;
        ~NavMeshBufferWriter() = delete;

        static void writePhysicsShapeData(EntryId id, NavMeshScanner::Tree data, NavMeshScanner::OgreTree ogreData, int** triData, float** vertData, int* usedIndexes);
    };
}
