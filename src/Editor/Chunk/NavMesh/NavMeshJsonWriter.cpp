#include "NavMeshJsonWriter.h"

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include <rapidjson/filereadstream.h>
#include <rapidjson/error/en.h>

#include "filesystem/path.h"

#include <fstream>
#include <iostream>

namespace Southsea{

    void NavMeshJsonWriter::writeMeshMeta(const std::string& outputPath, const std::vector<NavMeshData>& data){

        rapidjson::Document document;
        document.SetObject();
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        int currentId = 0;
        rapidjson::Value meshesArray(rapidjson::kArrayType);
        for(const NavMeshData& d : data){
            if(!d.populated) continue;
            rapidjson::Value meshObj(rapidjson::kObjectType);

            meshObj.AddMember("name", rapidjson::Value(d.meshName.c_str(), allocator), allocator);
            meshObj.AddMember("id", currentId, allocator);

            meshObj.AddMember(rapidjson::StringRef(getNavSettingName(NavMeshDataType::INCLUDE_TERRAIN)), d.settings.includeTerrain, allocator);

            for(size_t i = 0; i < static_cast<size_t>(NavMeshDataType::MAX); i++){
                NavMeshDataType t = static_cast<NavMeshDataType>(i);
                const char* targetName = getNavSettingName(t);
                assert(targetName); //There should always be a name.

                NavMeshData& dd = const_cast<NavMeshData&>(d);
                const float *targetValue = getNavMeshValueFromSettings(t, dd.settings);
                if(!targetValue) continue;

                rapidjson::Value v(*targetValue);

                meshObj.AddMember(rapidjson::Value(targetName, allocator), v, allocator);
            }


            meshesArray.PushBack(meshObj, allocator);
            currentId++;
        }
        document.AddMember("Meshes", meshesArray, allocator);

        //TODO link up the markers to the meshes

        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);

        std::cout << buffer.GetString() << std::endl;

        std::ofstream myfile;
        myfile.open(outputPath);
        myfile << buffer.GetString();
        myfile.close();

    }

    /**
    Parse mesh entries, assuming that the outData list is already populated with the definitions.

    */
    bool _parseMeshEntry(const rapidjson::Value& d, std::vector<NavMeshData>& outData, int id){
        using namespace rapidjson;

        //By this point we're done.
        if(id >= outData.size()) return true;

        NavMeshData out;
        memset(&out, 0, sizeof(NavMeshData));

        Value::ConstMemberIterator itr = d.FindMember("name");
        if(itr != d.MemberEnd() && itr->value.IsString()){
            out.meshName = itr->value.GetString();
        }else return false;

        //Names must match between the definition and the meta data.
        if(out.meshName != outData[id].meshName) return false;

        itr = d.FindMember("id");
        if(itr != d.MemberEnd() && itr->value.IsUint()){
            out.id = itr->value.GetUint();
        }else return false;

        itr = d.FindMember(getNavSettingName(NavMeshDataType::INCLUDE_TERRAIN));
        if(itr != d.MemberEnd() && itr->value.IsBool()){
            out.settings.includeTerrain = itr->value.GetBool();
        }else return false;

        for(size_t i = 0; i < static_cast<size_t>(NavMeshDataType::MAX); i++){
            NavMeshDataType t = static_cast<NavMeshDataType>(i);
            const char* targetName = getNavSettingName(t);
            assert(targetName); //There should always be a name.

            float *targetValue = getNavMeshValueFromSettings(t, out.settings);
            if(!targetValue) continue;
            assert(targetValue);

            Value::ConstMemberIterator itr = d.FindMember(targetName);
            if(itr != d.MemberEnd() && itr->value.IsDouble()){
                *targetValue = itr->value.GetDouble();
            }
        }

        //Copy the found values over to the new piece of data.
        memcpy(&outData[id].settings, &out.settings, sizeof(NavMeshSettings));

        //By this point we're pushing a validated mesh, so mark is as populated.
        //out.populated = true;
        //outData.push_back(out);

        return true;
    }

    bool NavMeshJsonWriter::readMeshMeta(const std::string& outputPath, std::vector<NavMeshData>& outData){
        if(outData.empty()) return true;

        const filesystem::path p(outputPath);
        if(!p.exists() || !p.is_file()) return false;

        FILE* fp = fopen(outputPath.c_str(), "r");
        char readBuffer[65536];
        rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        rapidjson::Document d;
        d.ParseStream(is);
        fclose(fp);

        if(d.HasParseError()){
            std::cerr << "Error parsing the setup file." << std::endl;
            std::cerr << rapidjson::GetParseError_En(d.GetParseError()) << std::endl;

            return false;
        }

        using namespace rapidjson;

        Value::ConstMemberIterator meshesArray = d.FindMember("Meshes");
        if(meshesArray == d.MemberEnd() || !meshesArray->value.IsArray()) return false;

        int currentId = -1;
        //Iterate the meshes array.
        for(Value::ConstValueIterator memItr = meshesArray->value.Begin(); memItr != meshesArray->value.End(); ++memItr){
            currentId++;
            if(!memItr->IsObject()) continue;
            const rapidjson::Value& arrayVal = *memItr;

            bool result = _parseMeshEntry(arrayVal, outData, currentId);
            if(!result) return false;
        }


        return true;
    }
}
