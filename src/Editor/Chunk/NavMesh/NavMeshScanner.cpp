#include "NavMeshScanner.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "NavMeshBufferWriter.h"

#include <cassert>
#include <iostream>
#include <cstring>

namespace Southsea{

    NavMeshScanner::ScannedBufferData NavMeshScanner::mScannedData;

    NavMeshScanner::NavMeshScanner(){

    }

    NavMeshScanner::~NavMeshScanner(){

    }

    const TreeObjectManager::TreeData* treeObjectData;
    bool NavMeshScanner::checkObjectCounter(const NavMarkerRules* parentRules, const SceneEntry& e, Tree data, OgreTree ogreData){
        assert(treeObjectData);
        //TODO get the object data, pull the values and build up the buffers.
        //Add the rules to the gui, with an include terrain check box.
        if(
            (e.type == SceneEntryType::physicsShape && parentRules->acceptPhysicsShapes)
        ){
            //Assume it's a cube
            mScannedData.numVerts += 6 * 4;
            mScannedData.numTriangles += 12;

            mScannedData.totalPhysicsShapes++;
            mScannedData.totalComitted++;
        }
        mScannedData.totalChecked++;

        return false;
    }

    int* triData;
    float* vertData;
    int usedIndexes;
    bool NavMeshScanner::writeToBuffers(const NavMarkerRules* parentRules, const SceneEntry& e, Tree data, OgreTree ogreData){
        assert(triData && vertData);
        if(
            (e.type == SceneEntryType::physicsShape && parentRules->acceptPhysicsShapes)
        ){
            NavMeshBufferWriter::writePhysicsShapeData(e.id, data, ogreData, &triData, &vertData, &usedIndexes);
        }
        return false;
    }

    void NavMeshScanner::createNavMeshBuffers(const std::set<EntryId>& contributingMarkers, MeshBuffers* outBuffers, bool includeTerrain){
        memset(&mScannedData, 0, sizeof(ScannedBufferData));
        memset(outBuffers, 0, sizeof(MeshBuffers));

        SceneContainer sceneEntries = EditorSingleton::getEditor()->getSceneTreeManager()->getSceneTree();
        Tree treeData = EditorSingleton::getEditor()->getTreeObjectManager()->getObjectData();
        OgreTree ogreData = EditorSingleton::getEditor()->getOgreTreeManager()->getTreeData();
        treeObjectData = &treeData;

        usedIndexes = 0;
        scanTreeForMarker(sceneEntries, treeData, ogreData, &checkObjectCounter);

        //Obtain and include the terrain metrics here.
        if(includeTerrain){
            auto e = EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager();
            mScannedData.numTriangles += e->getNumTriangles();
            mScannedData.numVerts += e->getNumVertices();
        }

        if(mScannedData.numTriangles > 0 && mScannedData.numVerts > 0){
            //We have something to build into a nav mesh. Allocate buffers.

            //Three points to a triangle.
            int* tData = new int[mScannedData.numTriangles * 3];
            //Points are defined in 3d space.
            float *vData = new float[mScannedData.numVerts * 3];
            triData = tData;
            vertData = vData;
            scanTreeForMarker(sceneEntries, treeData, ogreData, &writeToBuffers);

            //The shapes have been written. Write the terrain if it's included.
            if(includeTerrain){
                auto e = EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager();
                e->writeTriangleDataToBuffer(usedIndexes, triData);
                e->writeVertexDataToBuffer(vertData);
            }

            outBuffers->numTris = mScannedData.numTriangles;
            outBuffers->numVerts = mScannedData.numVerts;
            outBuffers->tris = tData;
            outBuffers->verts = vData;
        }

        //Print stats
        std::cout << "==Parsed object stats==" << std::endl;
        std::cout << "\tTotal checked: " << mScannedData.totalChecked << std::endl;
        std::cout << "\tTotal comitted: " << mScannedData.totalComitted << std::endl;
        std::cout << std::endl;
        std::cout << "\tTotal physics shapes: " << mScannedData.totalPhysicsShapes << std::endl;
        std::cout << "=======================" << std::endl;

        treeObjectData = 0;
    }

    void NavMeshScanner::scanTreeForMarker(SceneContainer sceneEntries, Tree data, OgreTree ogreData, CommitCheckFunction func){
        size_t currentIdx = 0;
        const size_t listSize = sceneEntries.size();
        while(currentIdx < listSize){
            const SceneEntry& e = sceneEntries[currentIdx];
            if(e.type != SceneEntryType::navMarker) {
                currentIdx++;
                continue;
            }

            //A marker was found.
            NavMarkerRules rules;
            currentIdx = processMarker(sceneEntries, data, ogreData, 0, currentIdx, e.id, &rules, func);
        }
    }

    size_t NavMeshScanner::processMarker(SceneContainer scene, Tree data, OgreTree ogreData, int depth, size_t startIdx, EntryId markerId, const NavMarkerRules* parentRules, CommitCheckFunction func){
        assert(scene[startIdx].type == SceneEntryType::navMarker);

        //Marker scanning includes whatever's in the current children, so trace back up the list to find its parent.
        int idx = SceneTreeManager::utilGetParentIndexOfEntry(startIdx, scene);

        //The rules of the markers for this depth.
        Southsea::NavMarkerRules newRules;
        int markersFound = resolveFinalRulesForParent(scene, data, ogreData, idx, parentRules, &newRules);
        assert(markersFound > 0);

        assert(scene[idx].type == SceneEntryType::treeChild);
        //Idx should be a child, so start one infront of that.
        int currentIdx = idx+1;
        //Begin scanning. Any objects found here which match the rules are comitted.
        currentIdx = _parseTree(scene, data, ogreData, 0, currentIdx, &newRules, func);

        return currentIdx;
    }

    size_t NavMeshScanner::_parseTree(SceneContainer scene, Tree data, OgreTree ogreData, int depth, size_t startIdx, const NavMarkerRules* parentRules, CommitCheckFunction func){
        size_t currentIdx = startIdx;
        while(currentIdx < scene.size()){
            const SceneEntry& e = scene[currentIdx];
            if(e.type == SceneEntryType::treeChild){
                currentIdx = _parseTree(scene, data, ogreData, depth+1, currentIdx+1, parentRules, func);
            }
            else if(e.type == SceneEntryType::treeTerminator){
                return currentIdx;
            }else if(e.type == SceneEntryType::navMarker){
                //Don't do anything. Just avoid calling the else.
            }else{
                //Call the function for all object types. It's useful for testing.
                //std::cout << e.id << std::endl;
                bool result = (*func)(parentRules, e, data, ogreData);
            }

            currentIdx++;
        }

        return currentIdx;
    }

    int NavMeshScanner::resolveFinalRulesForParent(SceneContainer scene, Tree data, OgreTree ogreData, size_t idx, const NavMarkerRules* parentRules, NavMarkerRules* outRules){
        int foundMarkers = 0;

        assert(scene[idx].type == SceneEntryType::treeChild);
        idx++;
        while(idx < scene.size()){
            const SceneEntry& e = scene[idx];
            //The end was found.
            if(e.type == SceneEntryType::treeTerminator) break;
            else if(e.type == SceneEntryType::treeChild){
                //If a child is found, skip through it
                int termLoc = SceneTreeManager::utilGetTerminatorForChild(idx, scene);
                assert(termLoc > 0);
                idx = termLoc;
                continue;
            }

            //Note: The marker which triggered this resolve is also included in this search. Not sure if that's a problem.
            if(e.type == SceneEntryType::navMarker){
                const ObjectData* objData = TreeObjectManager::utilGetDataObject(e.id, data);
                assert(objData->type() == ObjectType::navMarker);

                const NavMarkerObjectData* markerData = dynamic_cast<const NavMarkerObjectData*>(objData);
                const NavMarkerRules* rules = &markerData->rules;

                outRules->acceptPhysicsShapes |= rules->acceptPhysicsShapes;

                foundMarkers++;
            }
            idx++;
        }

        return foundMarkers;
    }

}
