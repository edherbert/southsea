#pragma once

#include <string>
#include <vector>

#include "NavMeshData.h"

namespace Southsea{

    /**
    Class to manage writing nav mesh data to the disk as a json file.

    @remarks
    This is just mesh data, it is not recast data.
    This is sort of like the metadata that acompanies the actual meshes.
    */
    class NavMeshJsonWriter{
    public:
        NavMeshJsonWriter() = delete;
        ~NavMeshJsonWriter() = delete;

        /**
        Write the json file to disk.
        */
        static void writeMeshMeta(const std::string& outputPath, const std::vector<NavMeshData>& data);

        /**
        Read the mesh metadata from a file to the output vector.

        @returns Whether the function was able to parse the file correctly.
        */
        static bool readMeshMeta(const std::string& outputPath, std::vector<NavMeshData>& outData);
    };
}
