#pragma once

#include "Editor/Scene/SceneTypes.h"
#include <string>
#include <set>

class dtNavMesh;

namespace Southsea{

    enum class NavMeshDataType{
        CELL_SIZE,
        CELL_HEIGHT,
        AGENT_HEIGHT,
        AGENT_RADIUS,
        AGENT_MAX_CLIMB,
        AGENT_MAX_SLOPE,
        REGION_MIN_SIZE,
        REGION_MERGE_SIZE,
        EDGE_MAX_LEN,
        EDGE_MAX_ERROR,
        VERTS_PERPOLY,
        DETAIL_SAMPLE_DIST,
        DETAIL_SAMPLE_MAX_ERROR,

        INCLUDE_TERRAIN,

        MAX
    };

    struct NavMeshSettings{
        float cellSize = 0.3f;
        float cellHeight = 0.2f;
        float agentHeight = 2.0f;
        float agentRadius = 0.6f;
        float agentMaxClimb = 0.9f;
        float agentMaxSlope = 45.0f;
        float regionMinSize = 8;
        float regionMergeSize = 20;
        float edgeMaxLen = 12.0f;
        float edgeMaxError = 1.3f;
        float vertsPerPoly = 6.0f;
        float detailSampleDist = 6.0f;
        float detailSampleMaxError = 1.0f;

        bool includeTerrain = false;
    };

    static float* getNavMeshValueFromSettings(NavMeshDataType t, NavMeshSettings& d){
        switch(t){
            case NavMeshDataType::CELL_SIZE: return &d.cellSize;
            case NavMeshDataType::CELL_HEIGHT: return &d.cellHeight;
            case NavMeshDataType::AGENT_HEIGHT: return &d.agentHeight;
            case NavMeshDataType::AGENT_RADIUS: return &d.agentRadius;
            case NavMeshDataType::AGENT_MAX_CLIMB: return &d.agentMaxClimb;
            case NavMeshDataType::AGENT_MAX_SLOPE: return &d.agentMaxSlope;
            case NavMeshDataType::REGION_MIN_SIZE: return &d.regionMinSize;
            case NavMeshDataType::REGION_MERGE_SIZE: return &d.regionMergeSize;
            case NavMeshDataType::EDGE_MAX_LEN: return &d.edgeMaxLen;
            case NavMeshDataType::EDGE_MAX_ERROR: return &d.edgeMaxError;
            case NavMeshDataType::VERTS_PERPOLY: return &d.vertsPerPoly;
            case NavMeshDataType::DETAIL_SAMPLE_DIST: return &d.detailSampleDist;
            case NavMeshDataType::DETAIL_SAMPLE_MAX_ERROR: return &d.detailSampleMaxError;
            default:{
                return 0;
            }
        }
    }

    static const char* getNavSettingName(NavMeshDataType t){
        switch(t){
            case NavMeshDataType::CELL_SIZE: return "CellSize";
            case NavMeshDataType::CELL_HEIGHT: return "CellHeight";
            case NavMeshDataType::AGENT_HEIGHT: return "AgentHeight";
            case NavMeshDataType::AGENT_RADIUS: return "AgentRadius";
            case NavMeshDataType::AGENT_MAX_CLIMB: return "AgentMaxClimb";
            case NavMeshDataType::AGENT_MAX_SLOPE: return "AgentMaxSlope";
            case NavMeshDataType::REGION_MIN_SIZE: return "RegionMinSize";
            case NavMeshDataType::REGION_MERGE_SIZE: return "RegionMergeSize";
            case NavMeshDataType::EDGE_MAX_LEN: return "EdgeMaxLength";
            case NavMeshDataType::EDGE_MAX_ERROR: return "EdgeMaxError";
            case NavMeshDataType::VERTS_PERPOLY: return "VertsPerPoly";
            case NavMeshDataType::DETAIL_SAMPLE_DIST: return "DetailSampleDistance";
            case NavMeshDataType::DETAIL_SAMPLE_MAX_ERROR: return "DetailSampleMaxError";
            case NavMeshDataType::INCLUDE_TERRAIN: return "includeTerrain";
            default:{
                return 0;
            }
        }
    }

    //Output data from recast nav mesh generation.
    struct CollectedNavMeshData{
        dtNavMesh* mesh;

        int navDataSize;
    };

    struct NavMeshData{
        std::string meshName;
        bool populated;
        unsigned int id;
        std::set<EntryId> contributingMarkers;

        int tileSize;
        float cellSize;
        float calculatedTileSize;
        float calculatedTilesInChunk;

        NavMeshSettings settings;
        CollectedNavMeshData outData;
    };
}
