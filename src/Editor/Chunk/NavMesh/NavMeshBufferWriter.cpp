#include "NavMeshBufferWriter.h"

#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include <cassert>
#include "OgreSceneNode.h"
#include "OgreVector4.h"
#include "OgreMatrix4.h"

namespace Southsea{

    static const int cubeArraySize = 12 * 3;
    static const int c_indexData[cubeArraySize]{
        0, 1, 2,
        0, 2, 3,
        4, 5, 6,
        4, 6, 7,
        8, 9, 10,
        8, 10, 11,
        12, 13, 14,
        12, 14, 15,
        16, 17, 18,
        16, 18, 19,
        20, 21, 22,
        20, 22, 23
    };

    static const int cubeVerticesCount = 6 * 4 * 3;
    static const float c_originalVertices[cubeVerticesCount] = {
        1.000000,-1.000000,-1.000000,
        1.000000,-1.000000,1.000000,
        -1.000000,-1.000000,1.000000,
        -1.000000,-1.000000,-1.000000,

        1.000000,1.000000,-1.000000,
        -1.000000,1.000000,-1.000000,
        -1.000000,1.000000,1.000000,
        1.000000,1.000000,1.000000,

        1.000000,-1.000000,-1.000000,
        1.000000,1.000000,-1.000000,
        1.000000,1.000000,1.000000,
        1.000000,-1.000000,1.000000,

        1.000000,-1.000000,1.000000,
        1.000000,1.000000,1.000000,
        -1.000000,1.000000,1.000000,
        -1.000000,-1.000000,1.000000,

        -1.000000,-1.000000,1.000000,
        -1.000000,1.000000,1.000000,
        -1.000000,1.000000,-1.000000,
        -1.000000,-1.000000,-1.000000,

        1.000000,1.000000,-1.000000,
        1.000000,-1.000000,-1.000000,
        -1.000000,-1.000000,-1.000000,
        -1.000000,1.000000,-1.000000,
    };

    void NavMeshBufferWriter::writePhysicsShapeData(EntryId id, NavMeshScanner::Tree data, NavMeshScanner::OgreTree ogreData, int** triData, float** vertData, int* usedIndexes){
        const ObjectData* d = TreeObjectManager::utilGetDataObject(id, data);
        assert(d->type() == ObjectType::physicsShape);
        const PhysicsShapeObjectData* shapeData = dynamic_cast<const PhysicsShapeObjectData*>(d);

        Ogre::SceneNode* node = OgreTreeManager::utilGetNodeById(ogreData, id);
        const Ogre::Matrix4 transform = node->_getFullTransformUpdated();

        switch(shapeData->shapeType){
            case PhysicsShapeType::capsule:
            case PhysicsShapeType::sphere:
            case PhysicsShapeType::box:{
                //Assume they're all boxes for now.
                for(int i = 0; i < cubeVerticesCount / 3; i++){
                    const Ogre::Vector4 pos(c_originalVertices[i*3], c_originalVertices[i*3+1], c_originalVertices[i*3+2], 1.0f);
                    const Ogre::Vector4 result = transform * pos;

                    **vertData = result.x;
                    (*vertData)++;
                    **vertData = result.y;
                    (*vertData)++;
                    **vertData = result.z;
                    (*vertData)++;
                }
                //6 faces, 2 tris per face, three points each.
                for(int i = 0; i < cubeArraySize; i++){
                    **triData = c_indexData[i] + *usedIndexes;
                    (*triData)++;
                }
                *usedIndexes+= 24;
                break;
            }
        }

    }
}
