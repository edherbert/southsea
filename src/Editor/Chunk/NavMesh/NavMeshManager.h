#pragma once

#include <vector>
#include <memory>

#include "NavMeshData.h"

namespace Ogre{
    class SceneManager;
}

namespace Southsea{
    class NavMeshDebugDraw;

    class NavMeshManager{
    public:
        NavMeshManager();
        ~NavMeshManager();

        void initialiseDrawer(Ogre::SceneManager* sceneManager);

        /**
        Create a new nav mesh with a given name.
        @returns The id of the created mesh. If there are currently more than 255 meshes, or a mesh with that name already exists, this will return INVALID_NAV_MESH_ID.
        */
        NavMeshId createNewNavMesh(const std::string& meshName);
        /**
        Create a nav mesh, however determine a generic name for it.
        */
        NavMeshId createNewNavMeshGenericName();

        /**
        Set a marker to contribute to a mesh.
        */
        void setMarkerContributesToMesh(EntryId marker, NavMeshId targetMesh, bool contribute);

        void removeNavMesh(NavMeshId id);

        void setNavMeshValue(NavMeshId id, NavMeshDataType type, float value);
        void setNavMeshIncludeTerrain(NavMeshId id, bool include);

        bool buildNavMesh(NavMeshId meshId);
        bool buildNavMeshTiles(NavMeshId meshId);

        bool buildAllTiles(NavMeshId meshId);

        void loadFromMeshFile(const std::string& filePath);

        /**
        Save every nav mesh as a binary file at the provided directory.
        @returns
        True or false depending on whether the save was a success.
        */
        bool saveBinaryNavMeshes(const std::string& outDir) const;
        void loadBinaryNavMeshes(const std::string& targetDir);

        NavMeshId getCurrentSelectedMesh() const { return mCurrentlySelectedNavMesh; }
        void setCurrentSelectedMesh(NavMeshId id) { mCurrentlySelectedNavMesh = id; }

        /**
        Remove and destroy all meshes from the manager.
        */
        void clearAllNavMeshes();

    private:
        std::vector<NavMeshData> mCreatedMeshes;
        std::shared_ptr<NavMeshDebugDraw> mDebugDraw;
        NavMeshId mCurrentlySelectedNavMesh;

        struct LoadedNavTileData{
            unsigned char* data;
            int dataSize;
        };

        /**
        Find a hole in the created nav meshes.
        @returns an index of the hole. If no hold exists -1 is returned.
        */
        int _findHoleInMeshes() const;

        bool _doesMeshNameExist(const std::string& meshName) const;

        void buildTile(const float* pos);

        struct GenerateMeshInput{
            NavMeshData* data;
            const float* verts;
            const int* tris;
            size_t numVerts;
            size_t numTris;
        };
        bool _generateRecastMesh(const GenerateMeshInput& in, CollectedNavMeshData* outData);
        /**
        Build a single mesh tile.
        @returns a null pointer if generation was not succesful.
        */
        unsigned char* _buildTileMesh(const GenerateMeshInput& in, CollectedNavMeshData* outData, int tx, int ty, const float* bmin, const float* bmax);
        void _saveAll(const char* path, const dtNavMesh* mesh) const;
        void _saveAllTiles(const char* dir, const dtNavMesh* mesh, size_t meshId) const;
        dtNavMesh* _loadAll(const char* path) const;

        inline bool _internalBuildNavMesh(NavMeshId, GenerateMeshInput* out);

        bool parseTile(const std::string& filePath, LoadedNavTileData* out) const;
        void _addTileToMesh(dtNavMesh** outMesh, const NavMeshData* data, LoadedNavTileData* includedData);


        void _destroyNavMeshData(NavMeshData& data) const;
        void _resetDataForMesh(NavMeshId id);

    public:
        const std::vector<NavMeshData>& getCurrentMeshes() const { return mCreatedMeshes; }
    };
}
