#pragma once

#include "DebugDraw.h"
#include "DetourDebugDraw.h"
#include "Ogre.h"

#include <vector>

namespace Southsea{

    class NavMeshDebugDraw;

    struct SouthseaDebugDraw : public duDebugDraw{
    public:
        SouthseaDebugDraw(NavMeshDebugDraw* parent);
        virtual ~SouthseaDebugDraw();
        virtual void depthMask(bool state);
        virtual void texture(bool state);
        virtual void begin(duDebugDrawPrimitives prim, float size = 1.0f);
        virtual void vertex(const float* pos, unsigned int color);
        virtual void vertex(const float x, const float y, const float z, unsigned int color);
        virtual void vertex(const float* pos, unsigned int color, const float* uv);
        virtual void vertex(const float x, const float y, const float z, unsigned int color, const float u, const float v);
        virtual void end();

        Ogre::SceneNode* endMesh();
    private:
        void _vertex(float x, float y, float z, unsigned int color);
        void _createFromMesh();

        Ogre::MeshPtr createStaticMesh(Ogre::IndexBufferPacked *indexBuffer, const Ogre::VertexElement2Vec& elemVec, int arraySize, const float* vertexData, Ogre::OperationType t);

        Ogre::MeshPtr mMeshPtr;
        NavMeshDebugDraw* mParent;
        duDebugDrawPrimitives mOpType;
        std::vector<float> mVertices;
    };


    class NavMeshDebugDraw{
        friend SouthseaDebugDraw;
    public:
        NavMeshDebugDraw();
        ~NavMeshDebugDraw();

        void _createFromMesh();

        void debugDrawNavMesh(dtNavMesh* mesh);
        /**
        Remove a drawn object from the drawer. This function accepts many object types, cast to void* for convenience.
        */
        void removeDrawnObject(void* object);

        void initialise(Ogre::SceneManager* sceneManager);

    private:
        Ogre::HlmsDatablock* mBlock = 0;
        std::map<void*, Ogre::SceneNode*> mDrawnMeshes;

        void _destroyShapeForNode(Ogre::SceneNode* node);

        Ogre::SceneNode* mContainerNode = 0;
        Ogre::SceneManager* mSceneManager = 0;
    };
}
