
#include "NavMeshManager.h"

#include "filesystem/path.h"

#include "Recast.h"
#include "DetourNavMesh.h"
#include "DetourNavMeshQuery.h"
#include "DetourNavMeshBuilder.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Event/Events/SceneEvent.h"
#include "Event/EventDispatcher.h"
#include "NavMeshScanner.h"
#include "NavMeshJsonWriter.h"
#include "Debug/NavMeshDebugDraw.h"

#include "System/Project/MapNavFile.h"

#include "ChunkyTriMesh.h"

#include <cassert>
#include <cstring>
#include <iostream>
#include <math.h>

namespace Southsea{

    NavMeshManager::NavMeshManager()
        : mCurrentlySelectedNavMesh(INVALID_NAV_MESH_ID),
        mDebugDraw(std::make_shared<NavMeshDebugDraw>()) {
    }

    NavMeshManager::~NavMeshManager(){

    }

    const float bmin[3] = {0, 0, 0};
    float bmax[3] = {200, 200, 200};
    void NavMeshManager::initialiseDrawer(Ogre::SceneManager* sceneManager){
        mDebugDraw->initialise(sceneManager);

        const float chunkSize = float(EditorSingleton::getEditor()->getSlotSize());

        bmax[0] = chunkSize;
        bmax[1] = chunkSize;
        bmax[2] = chunkSize;
    }

    NavMeshId NavMeshManager::createNewNavMeshGenericName(){
        //Loop through until a number which doesn't exist is found.
        //We're limited to 255 meshes so just fail if none could be found.
        for(int i = 0; i < INVALID_NAV_MESH_ID; i++){
            NavMeshId result = createNewNavMesh("Nav mesh " + std::to_string(i));
            if(result != INVALID_NAV_MESH_ID) return result;
        }

        return INVALID_NAV_MESH_ID;
    }

    NavMeshId NavMeshManager::createNewNavMesh(const std::string& meshName){
        //Don't allow any more than 255 meshes to exist at a time.
        if(mCreatedMeshes.size() >= INVALID_NAV_MESH_ID) return INVALID_NAV_MESH_ID;
        if(_doesMeshNameExist(meshName)) return INVALID_NAV_MESH_ID;

        int targetIdx = _findHoleInMeshes();
        assert(targetIdx < INVALID_NAV_MESH_ID);
        if(targetIdx < 0){
            //No holes were found so create a new mesh.
            size_t currentPos = mCreatedMeshes.size();
            mCreatedMeshes.push_back({meshName, true});
            return currentPos;
        }

        NavMeshData d;
        d.outData.mesh = 0;
        d.meshName = meshName;
        d.populated = true;
        mCreatedMeshes[targetIdx] = d;
        return targetIdx;
    }

    void NavMeshManager::loadFromMeshFile(const std::string& filePath){
        const std::vector<MapNavFile::MeshData>& meshList = EditorSingleton::getEditor()->getMapNavFile()->getMeshData();

        for(const MapNavFile::MeshData& t : meshList){
            NavMeshData out;
            memset(&out, 0, sizeof(NavMeshData));
            //Make sure the default values are copied over.
            NavMeshSettings defaultSettings;
            memcpy(&out.settings, &defaultSettings, sizeof(NavMeshSettings));


            out.populated = true;
            out.meshName = t.meshName;
            out.tileSize = t.tileSize;
            out.cellSize = t.cellSize;

            //Calculate various metrics about the tile.
            float tileSize = t.tileSize * t.cellSize;
            int slotSize = EditorSingleton::getEditor()->getEditorProject().getAVSetupFile().getSlotSize();
            float numTilesInChunk = slotSize / tileSize;
            out.calculatedTileSize = tileSize;
            out.calculatedTilesInChunk = int(numTilesInChunk);

            //If something went wrong
            if(out.calculatedTilesInChunk <= 0){
                out.calculatedTilesInChunk = 1;
                out.calculatedTileSize = slotSize;
            }

            mCreatedMeshes.push_back(out);
        }
        bool result = NavMeshJsonWriter::readMeshMeta(filePath, mCreatedMeshes);
        if(!result){
            std::cout << "Error parsing nav mesh settings file." << std::endl;
            std::cout << filePath << std::endl;
        }
    }

    void NavMeshManager::setMarkerContributesToMesh(EntryId marker, NavMeshId targetMesh, bool contribute){
        assert(targetMesh < mCreatedMeshes.size());
        NavMeshData& data = mCreatedMeshes[targetMesh];
        assert(data.populated);

        if(contribute){
            data.contributingMarkers.insert(marker);
        }else{
            data.contributingMarkers.erase(marker);
        }
        EditorSingleton::getEditor()->getTreeObjectManager()->setNavMeshMarkerContribution(marker, targetMesh, contribute);
    }

    void NavMeshManager::removeNavMesh(NavMeshId id){
        if(id == INVALID_NAV_MESH_ID) return;
        if(id >= mCreatedMeshes.size()) return;

        if(!mCreatedMeshes[id].populated) return;
        _resetDataForMesh(id);
    }

    void NavMeshManager::clearAllNavMeshes(){
        NavMeshId counter = 0;
        for(const NavMeshData& data : mCreatedMeshes){
            if(data.populated){
                removeNavMesh(counter);
            }
            counter++;
        }

        mCurrentlySelectedNavMesh = INVALID_NAV_MESH_ID;
        mCreatedMeshes.clear();
    }

    void NavMeshManager::_resetDataForMesh(NavMeshId id){
        mDebugDraw->removeDrawnObject( static_cast<void*>(mCreatedMeshes[id].outData.mesh) );
        _destroyNavMeshData(mCreatedMeshes[id]);
        mCreatedMeshes[id].populated = false;
    }

    void NavMeshManager::_destroyNavMeshData(NavMeshData& data) const{
        if(data.outData.mesh){
            dtFreeNavMesh(data.outData.mesh);
        }
        memset(&(data.outData), 0, sizeof(CollectedNavMeshData));
    }

    void NavMeshManager::setNavMeshValue(NavMeshId id, NavMeshDataType type, float value){
        assert(id != INVALID_NAV_MESH_ID);
        assert(id < mCreatedMeshes.size());

        float* targetVal = getNavMeshValueFromSettings(type, mCreatedMeshes[id].settings);
        *targetVal = value;


        SceneNavMeshValueChange e;
        e.id = id;
        EventDispatcher::transmitEvent(EventType::Scene, e);
    }

    void NavMeshManager::setNavMeshIncludeTerrain(NavMeshId id, bool include){
        assert(id != INVALID_NAV_MESH_ID);
        assert(id < mCreatedMeshes.size());

        mCreatedMeshes[id].settings.includeTerrain = include;

        SceneNavMeshValueChange e;
        e.id = id;
        EventDispatcher::transmitEvent(EventType::Scene, e);
    }

    bool NavMeshManager::saveBinaryNavMeshes(const std::string& outDir) const{
        const filesystem::path dir(outDir);
        if(!dir.exists() || ! dir.is_directory()) return false;

        size_t currentId = 0;
        for(const NavMeshData& d : mCreatedMeshes){
            if(!d.outData.mesh) continue;

            const filesystem::path outPath = dir / filesystem::path(std::to_string(currentId) + ".nav");
            std::cout << "Saving nav mesh: " << outPath.str() << std::endl;
            //_saveAll(outPath.str().c_str(), d.outData.mesh);
            _saveAllTiles(outDir.c_str(), d.outData.mesh, currentId);

            currentId++;
        }

        return true;
    }

    void NavMeshManager::_addTileToMesh(dtNavMesh** outMesh, const NavMeshData* data, LoadedNavTileData* includedData){
        if(!(*outMesh)){
            //The mesh does not exist so create it.
            dtNavMesh* mesh = dtAllocNavMesh();
            if (!mesh){
                assert(false);
            }

            float m_tileSize = data->calculatedTileSize;
            float m_cellSize = data->cellSize;
            dtNavMeshParams params;
            params.tileWidth = m_tileSize*m_cellSize;
            params.tileHeight = m_tileSize*m_cellSize;
            params.maxTiles = 32;
            params.maxPolys = 1000;
            params.orig[0] = 0.0f;
            params.orig[1] = 0.0f;
            params.orig[2] = 0.0f;

            dtStatus status = mesh->init(&params);
            if(dtStatusFailed(status))
            {
                assert(false);
            }

            *outMesh = mesh;
        }
        dtNavMesh* newMesh = *outMesh;
        assert(newMesh);

        dtMeshHeader* h = (dtMeshHeader*)includedData->data;
        newMesh->removeTile(newMesh->getTileRefAt(h->x,h->y,0),0,0);
        dtStatus addResult = newMesh->addTile(includedData->data, includedData->dataSize, DT_TILE_FREE_DATA, 0, 0);
        assert(!dtStatusFailed(addResult));
    }

    void NavMeshManager::loadBinaryNavMeshes(const std::string& targetDir){
        const filesystem::path dir(targetDir);
        //For each mesh check if a file matching the id exists in the target directory. If it does load it as a detour mesh.
        for(NavMeshData& d : mCreatedMeshes){
            if(!d.populated) continue;

            const int totalExpected = d.calculatedTilesInChunk*d.calculatedTilesInChunk;
            LoadedNavTileData totalExpectedData[totalExpected];

            for(int i = 0; i < totalExpected; i++){
                const std::string targetName(std::to_string(d.id) + "-" + std::to_string(i) + ".nav");
                const filesystem::path outPath = dir / filesystem::path(targetName);

                if(!outPath.exists() || !outPath.is_file()) continue;

                LoadedNavTileData loadedTile;
                bool parseResult = parseTile(outPath.str(), &loadedTile);
                if(!parseResult) continue;

                //Load the found tile into the mesh.
                _addTileToMesh(&d.outData.mesh, &d, &loadedTile);
                assert(d.outData.mesh);

                std::cout << "Read nav mesh tile at " << outPath << std::endl;
            }

            if(d.outData.mesh){
                mDebugDraw->debugDrawNavMesh(d.outData.mesh);
            }
        }
    }



    bool NavMeshManager::_doesMeshNameExist(const std::string& meshName) const{
        for(const NavMeshData& d : mCreatedMeshes){
            if(!d.populated) continue;
            if(d.meshName == meshName) return true;
        }
        return false;
    }

    int NavMeshManager::_findHoleInMeshes() const {
        for(int i = 0; i < mCreatedMeshes.size(); i++){
            if(!mCreatedMeshes[i].populated){
                return i;
            }
        }
        return -1;
    }

    inline bool NavMeshManager::_internalBuildNavMesh(NavMeshId meshId, GenerateMeshInput* out){
        assert(meshId != INVALID_NAV_MESH_ID);
        assert(meshId < mCreatedMeshes.size());

        const std::set<EntryId>& contributingMarkers = mCreatedMeshes[meshId].contributingMarkers;
        NavMeshScanner scanner;

        removeNavMesh(meshId);
        NavMeshData& data = mCreatedMeshes[meshId];
        data.populated = true;
        if(data.settings.includeTerrain){
            std::cout << "Including terrain in build" << std::endl;
        }

        NavMeshScanner::MeshBuffers outBuffers;
        scanner.createNavMeshBuffers(contributingMarkers, &outBuffers, data.settings.includeTerrain);
        if(!outBuffers.tris || !outBuffers.verts) return false;

        {
            out->data = &data;
            out->verts = outBuffers.verts;
            out->tris = outBuffers.tris;
            out->numVerts = outBuffers.numVerts;
            out->numTris = outBuffers.numTris;
        }

        return true;
    }

    bool NavMeshManager::buildAllTiles(NavMeshId meshId){
        GenerateMeshInput input;
        _internalBuildNavMesh(meshId, &input);

        int foundTileSize = input.data->tileSize;
        int gw = 0, gh = 0;
        rcCalcGridSize(bmin, bmax, input.data->settings.cellSize, &gw, &gh);
        const int ts = (int)foundTileSize;
        const int tw = (gw + ts-1) / ts;
        const int th = (gh + ts-1) / ts;
        const float tcs = foundTileSize*input.data->settings.cellSize;

        //-----
        dtNavMesh* m_navMesh = dtAllocNavMesh();
        if (!m_navMesh)
        {
            return false;
        }

        dtNavMeshParams params;
        float boundsMin[3] = {0, 0, 0};
        rcVcopy(params.orig, &(boundsMin[0]));
        params.tileWidth = foundTileSize*input.data->settings.cellSize;
        params.tileHeight = foundTileSize*input.data->settings.cellSize;
        params.maxTiles = 32;
        params.maxPolys = 1000;


        dtStatus status;

        status = m_navMesh->init(&params);
        if (dtStatusFailed(status))
        {
            return false;
        }
        //-----

        std::cout << "y: " << th << std::endl;
        std::cout << "x: " << tw << std::endl;

        for (int y = 0; y < th; ++y){
            for (int x = 0; x < tw; ++x){
                static float m_lastBuiltTileBmin[3];
                static float m_lastBuiltTileBmax[3];

                m_lastBuiltTileBmin[0] = bmin[0] + x*tcs;
                m_lastBuiltTileBmin[1] = bmin[1];
                m_lastBuiltTileBmin[2] = bmin[2] + y*tcs;

                m_lastBuiltTileBmax[0] = bmin[0] + (x+1)*tcs;
                m_lastBuiltTileBmax[1] = bmax[1];
                m_lastBuiltTileBmax[2] = bmin[2] + (y+1)*tcs;

                //unsigned char* data = buildTileMesh(x, y, m_lastBuiltTileBmin, m_lastBuiltTileBmax, dataSize);
                unsigned char* outData = _buildTileMesh(input, &(input.data->outData), x, y, &m_lastBuiltTileBmin[0], &m_lastBuiltTileBmax[0]);
                if(outData){
                    // Remove any previous data (navmesh owns and deletes the data).
                    m_navMesh->removeTile(m_navMesh->getTileRefAt(x,y,0),0,0);
                    // Let the navmesh own the data.
                    dtStatus status = m_navMesh->addTile(outData,input.data->outData.navDataSize,DT_TILE_FREE_DATA,0,0);
                    if (dtStatusFailed(status)){
                        dtFree(outData);
                    }
                }
            }
        }

        input.data->outData.mesh = m_navMesh;

        mDebugDraw->debugDrawNavMesh(input.data->outData.mesh);

        return true;
    }

    bool NavMeshManager::buildNavMeshTiles(NavMeshId meshId){
        GenerateMeshInput input;
        _internalBuildNavMesh(meshId, &input);

        int tileX = 0;
        int tileY = 0;
        unsigned char* outData = _buildTileMesh(input, &(input.data->outData), tileX, tileY, &bmin[0], &bmax[0]);
        if(!outData){
            return false;
        }

        { //Construct the nav mesh
            dtNavMesh* m_navMesh = dtAllocNavMesh();
            if (!m_navMesh)
            {
                dtFree(outData);
                return 0;
            }

            int foundTileSize = input.data->tileSize;
            dtNavMeshParams params;
            float boundsMin[3] = {0, 0, 0};
            rcVcopy(params.orig, &(boundsMin[0]));
            params.tileWidth = foundTileSize*input.data->settings.cellSize;
            params.tileHeight = foundTileSize*input.data->settings.cellSize;
            params.maxTiles = 32;
            params.maxPolys = 1000;


            dtStatus status;

            status = m_navMesh->init(&params);
            if (dtStatusFailed(status))
            {
                dtFree(outData);
                return 0;
            }

            status = m_navMesh->addTile(outData, input.data->outData.navDataSize, DT_TILE_FREE_DATA, 0, 0);
            assert(!dtStatusFailed(status));

            input.data->outData.mesh = m_navMesh;
        }

        mDebugDraw->debugDrawNavMesh(input.data->outData.mesh);

        return true;
    }

    bool NavMeshManager::buildNavMesh(NavMeshId meshId){
        GenerateMeshInput input;
        _internalBuildNavMesh(meshId, &input);
        bool success = _generateRecastMesh(input, &(input.data->outData));

        if(!success){
            return false;
        }

        mDebugDraw->debugDrawNavMesh(input.data->outData.mesh);

        return true;
    }

    /*void NavMeshManager::buildTile(const float* pos)
    {
        //if (!m_geom) return;
        NavMeshData& data = mCreatedMeshes[0];
        //if (!m_navMesh) return;

        //const float* bmin = m_geom->getNavMeshBoundsMin();
        //const float* bmax = m_geom->getNavMeshBoundsMax();
        const float bmin[3] = {0, 0, 0};
        const float bmax[3] = {500, 500, 500};

        const float m_tileSize = 30.0f;

        const float ts = m_tileSize*data.settings.cellSize;
        const int tx = (int)((pos[0] - bmin[0]) / ts);
        const int ty = (int)((pos[2] - bmin[2]) / ts);

        m_lastBuiltTileBmin[0] = bmin[0] + tx*ts;
        m_lastBuiltTileBmin[1] = bmin[1];
        m_lastBuiltTileBmin[2] = bmin[2] + ty*ts;

        m_lastBuiltTileBmax[0] = bmin[0] + (tx+1)*ts;
        m_lastBuiltTileBmax[1] = bmax[1];
        m_lastBuiltTileBmax[2] = bmin[2] + (ty+1)*ts;

        //m_tileCol = duRGBA(255,255,255,64);

        //m_ctx->resetLog();

        int dataSize = 0;
        unsigned char* data = _buildTileMesh(tx, ty, m_lastBuiltTileBmin, m_lastBuiltTileBmax, dataSize);

        // // Remove any previous data (navmesh owns and deletes the data).
        // m_navMesh->removeTile(m_navMesh->getTileRefAt(tx,ty,0),0,0);

        // // Add tile, or leave the location empty.
        // if (data)
        // {
        //     // Let the navmesh own the data.
        //     dtStatus status = m_navMesh->addTile(data,dataSize,DT_TILE_FREE_DATA,0,0);
        //     if (dtStatusFailed(status))
        //         dtFree(data);
        // }

        // m_ctx->dumpLog("Build Tile (%d,%d):", tx,ty);
    }*/

    static const int NAVMESHSET_MAGIC = 'M'<<24 | 'S'<<16 | 'E'<<8 | 'T'; //'MSET';
    static const int NAVMESHSET_VERSION = 1;

    struct NavMeshSetHeader
    {
        int magic;
        int version;
        int numTiles;
        dtNavMeshParams params;
    };

    struct NavMeshTileHeader
    {
        dtTileRef tileRef;
        int dataSize;
    };

    void NavMeshManager::_saveAllTiles(const char* dir, const dtNavMesh* mesh, size_t meshId) const{
        if (!mesh) return;

        for (int i = 0; i < mesh->getMaxTiles(); ++i)
        {
            const dtMeshTile* tile = mesh->getTile(i);
            if (!tile || !tile->header || !tile->dataSize) continue;

            const filesystem::path target = filesystem::path(dir) / (std::to_string(meshId) + std::string("-") + std::to_string(i) + ".nav");
            FILE* fp = fopen(target.str().c_str(), "wb");
            if (!fp)
                return;

            NavMeshTileHeader tileHeader;
            tileHeader.tileRef = mesh->getTileRef(tile);
            tileHeader.dataSize = tile->dataSize;
            fwrite(&tileHeader, sizeof(tileHeader), 1, fp);

            fwrite(tile->data, tile->dataSize, 1, fp);
            fclose(fp);
        }
    }

    void NavMeshManager::_saveAll(const char* path, const dtNavMesh* mesh) const{
        if (!mesh) return;

        FILE* fp = fopen(path, "wb");
        if (!fp)
            return;

        // Store header.
        NavMeshSetHeader header;
        header.magic = NAVMESHSET_MAGIC;
        header.version = NAVMESHSET_VERSION;
        header.numTiles = 0;
        for (int i = 0; i < mesh->getMaxTiles(); ++i)
        {
            const dtMeshTile* tile = mesh->getTile(i);
            if (!tile || !tile->header || !tile->dataSize) continue;
            header.numTiles++;
        }
        memcpy(&header.params, mesh->getParams(), sizeof(dtNavMeshParams));
        fwrite(&header, sizeof(NavMeshSetHeader), 1, fp);

        // Store tiles.
        for (int i = 0; i < mesh->getMaxTiles(); ++i)
        {
            const dtMeshTile* tile = mesh->getTile(i);
            if (!tile || !tile->header || !tile->dataSize) continue;

            NavMeshTileHeader tileHeader;
            tileHeader.tileRef = mesh->getTileRef(tile);
            tileHeader.dataSize = tile->dataSize;
            fwrite(&tileHeader, sizeof(tileHeader), 1, fp);

            fwrite(tile->data, tile->dataSize, 1, fp);
        }

        const float* orig = mesh->getParams()->orig;

        std::cout << "orig: " << *orig << " " << *(orig+1) << " " << *(orig+2) << std::endl;
        std::cout << "tile width: " << mesh->getParams()->tileWidth << std::endl;
        std::cout << "tile height: " << mesh->getParams()->tileHeight << std::endl;

        fclose(fp);
    }

    dtNavMesh* NavMeshManager::_loadAll(const char* path) const{
        FILE* fp = fopen(path, "rb");
        if (!fp) return 0;

        // Read header.
        NavMeshSetHeader header;
        size_t readLen = fread(&header, sizeof(NavMeshSetHeader), 1, fp);
        if (readLen != 1)
        {
            fclose(fp);
            return 0;
        }
        if (header.magic != NAVMESHSET_MAGIC)
        {
            fclose(fp);
            return 0;
        }
        if (header.version != NAVMESHSET_VERSION)
        {
            fclose(fp);
            return 0;
        }

        dtNavMesh* mesh = dtAllocNavMesh();
        if (!mesh)
        {
            fclose(fp);
            return 0;
        }
        dtStatus status = mesh->init(&header.params);
        if (dtStatusFailed(status))
        {
            fclose(fp);
            return 0;
        }

        // Read tiles.
        for (int i = 0; i < header.numTiles; ++i)
        {
            NavMeshTileHeader tileHeader;
            readLen = fread(&tileHeader, sizeof(tileHeader), 1, fp);
            if (readLen != 1)
            {
                fclose(fp);
                return 0;
            }

            if (!tileHeader.tileRef || !tileHeader.dataSize)
                break;

            unsigned char* data = (unsigned char*)dtAlloc(tileHeader.dataSize, DT_ALLOC_PERM);
            if (!data) break;
            memset(data, 0, tileHeader.dataSize);
            readLen = fread(data, tileHeader.dataSize, 1, fp);
            if (readLen != 1)
            {
                dtFree(data);
                fclose(fp);
                return 0;
            }

            mesh->addTile(data, tileHeader.dataSize, DT_TILE_FREE_DATA, tileHeader.tileRef, 0);
        }

        fclose(fp);

        return mesh;
    }

    bool NavMeshManager::parseTile(const std::string& filePath, LoadedNavTileData* out) const{
        FILE* fp = fopen(filePath.c_str(), "rb");
        if (!fp) return false;

        NavMeshTileHeader tileHeader;
        size_t readLen = fread(&tileHeader, sizeof(tileHeader), 1, fp);
        if (readLen != 1)
        {
            fclose(fp);
            return false;
        }

        if (!tileHeader.tileRef || !tileHeader.dataSize)
            return false;

        unsigned char* data = (unsigned char*)dtAlloc(tileHeader.dataSize, DT_ALLOC_PERM);
        if (!data) return false;
        memset(data, 0, tileHeader.dataSize);
        readLen = fread(data, tileHeader.dataSize, 1, fp);
        if (readLen != 1)
        {
            dtFree(data);
            fclose(fp);
            return false;
        }

        out->dataSize = tileHeader.dataSize;
        out->data = data;

        return true;
    }

    bool NavMeshManager::_generateRecastMesh(const GenerateMeshInput& in, CollectedNavMeshData* outData){

        bool mKeepIntermediateResults = false;

        // std::shared_ptr<TerrainManager> terrainManager = EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager();
        // const float* verts = terrainManager->getVertexData();
        // const int* tris = terrainManager->getTriangleData();
        // const int numVerts = terrainManager->getNumVertices();
        // const int numTris = terrainManager->getNumTriangles();

        assert(in.data);
        float mCellHeight = in.data->settings.cellHeight;
        float mCellSize = in.data->settings.cellSize;
        float mAgentHeight = in.data->settings.agentHeight;
        float mAgentMaxClimb = in.data->settings.agentMaxClimb;
        float mAgentRadius = in.data->settings.agentRadius;
        float mAgentMaxSlope = in.data->settings.agentMaxSlope;
        float mEdgeMaxLen = in.data->settings.edgeMaxLen;
        float mEdgeMaxError = in.data->settings.edgeMaxError;
        float mVertsPerPoly = in.data->settings.vertsPerPoly;
        float mDetailSampleDist = in.data->settings.detailSampleDist;
        float mDetailSampleMaxError = in.data->settings.detailSampleMaxError;
        float mRegionMinSize = in.data->settings.regionMinSize;
        float mRegionMergeSize = in.data->settings.regionMergeSize;

        rcConfig mCfg;
        memset(&mCfg, 0, sizeof(mCfg));
        mCfg.cs = mCellSize;
        mCfg.ch = mCellHeight;
        mCfg.walkableSlopeAngle = mAgentMaxSlope;
        mCfg.walkableHeight = (int)ceilf(mAgentHeight / mCfg.ch);
        mCfg.walkableClimb = (int)floorf(mAgentMaxClimb / mCfg.ch);
        mCfg.walkableRadius = (int)ceilf(mAgentRadius / mCfg.cs);
        mCfg.maxEdgeLen = (int)(mEdgeMaxLen / mCellSize);
        mCfg.maxSimplificationError = mEdgeMaxError;
        mCfg.minRegionArea = (int)rcSqr(mRegionMinSize);        // Note: area = size*size
        mCfg.mergeRegionArea = (int)rcSqr(mRegionMergeSize);    // Note: area = size*size
        mCfg.maxVertsPerPoly = (int)mVertsPerPoly;
        mCfg.detailSampleDist = mDetailSampleDist < 0.9f ? 0 : mCellSize * mDetailSampleDist;
        mCfg.detailSampleMaxError = mCellHeight * mDetailSampleMaxError;

        rcVcopy(mCfg.bmin, bmin);
        rcVcopy(mCfg.bmax, bmax);
        rcCalcGridSize(mCfg.bmin, mCfg.bmax, mCfg.cs, &mCfg.width, &mCfg.height);
        rcContext mCtx;

        rcHeightfield* mSolid = rcAllocHeightfield();
        assert(mSolid);
        if (!rcCreateHeightfield(&mCtx, *mSolid, mCfg.width, mCfg.height, mCfg.bmin, mCfg.bmax, mCfg.cs, mCfg.ch))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not create solid heightfield.");
            return false;
        }

        unsigned char* mTriareas = new unsigned char[in.numTris];

        memset(mTriareas, 0, in.numTris*sizeof(unsigned char));

        // Find triangles which are walkable based on their slope and rasterize them.
        // If your input data is multiple meshes, you can transform them here, calculate
        // the are type for each of the meshes and rasterize them.
        rcMarkWalkableTriangles(&mCtx, mCfg.walkableSlopeAngle, in.verts, in.numVerts, in.tris, in.numTris, mTriareas);
        if (!rcRasterizeTriangles(&mCtx, in.verts, in.numVerts, in.tris, mTriareas, in.numTris, *mSolid, mCfg.walkableClimb))
        {
        }

        if (!mKeepIntermediateResults)
        {
            delete [] mTriareas;
            mTriareas = 0;
        }

        // 3. Filter walkable surfaces
        bool filterLowHangingObstacles = false;
        bool filterLedgeSpans = false;
        bool filterWalkableLowHeightSpans = false;
        if (filterLowHangingObstacles)
            rcFilterLowHangingWalkableObstacles(&mCtx, mCfg.walkableClimb, *mSolid);
        if (filterLedgeSpans)
            rcFilterLedgeSpans(&mCtx, mCfg.walkableHeight, mCfg.walkableClimb, *mSolid);
        if (filterWalkableLowHeightSpans)
            rcFilterWalkableLowHeightSpans(&mCtx, mCfg.walkableHeight, *mSolid);

        // 4. Partition walkable surface into simple regions.
        rcCompactHeightfield* m_chf = rcAllocCompactHeightfield();
        if (!m_chf)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'chf'.");
            return false;
        }
        if (!rcBuildCompactHeightfield(&mCtx, mCfg.walkableHeight, mCfg.walkableClimb, *mSolid, *m_chf))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build compact data.");
            return false;
        }

        if (!mKeepIntermediateResults)
        {
            rcFreeHeightField(mSolid);
            mSolid = 0;
        }

        // Erode the walkable area by agent radius.
        if (!rcErodeWalkableArea(&mCtx, mCfg.walkableRadius, *m_chf))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not erode.");
            return false;
        }

        // (Optional) Mark areas.
        // const ConvexVolume* vols = m_geom->getConvexVolumes();
        // for (int i  = 0; i < m_geom->getConvexVolumeCount(); ++i)
        //     rcMarkConvexPolyArea(&mCtx, vols[i].verts, vols[i].nverts, vols[i].hmin, vols[i].hmax, (unsigned char)vols[i].area, *m_chf);


        //Partition watershed
        {
            // Prepare for region partitioning, by calculating distance field along the walkable surface.
            if (!rcBuildDistanceField(&mCtx, *m_chf))
            {
                mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build distance field.");
                return false;
            }

            // Partition the walkable surface into simple regions without holes.
            if (!rcBuildRegions(&mCtx, *m_chf, 0, mCfg.minRegionArea, mCfg.mergeRegionArea))
            {
                mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build watershed regions.");
                return false;
            }
        }
        //rcBuildRegionsMonotone(&mCtx, *m_chf, 0, mCfg.minRegionArea, mCfg.mergeRegionArea);if (!rcBuildLayerReg
        //rcBuildLayerRegions(&mCtx, *m_chf, 0, mCfg.minRegionArea);

        //
        // Step 5. Trace and simplify region contours.
        //

        // Create contours.
        rcContourSet* m_cset = rcAllocContourSet();
        if (!m_cset)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'cset'.");
            return false;
        }
        if (!rcBuildContours(&mCtx, *m_chf, mCfg.maxSimplificationError, mCfg.maxEdgeLen, *m_cset))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not create contours.");
            return false;
        }

        //
        // Step 6. Build polygons mesh from contours.
        //

        // Build polygon navmesh from the contours.
        rcPolyMesh* m_pmesh = rcAllocPolyMesh();
        if (!m_pmesh)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'pmesh'.");
            return false;
        }
        if (!rcBuildPolyMesh(&mCtx, *m_cset, mCfg.maxVertsPerPoly, *m_pmesh))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not triangulate contours.");
            return false;
        }

        //
        // Step 7. Create detail mesh which allows to access approximate height on each polygon.
        //

        rcPolyMeshDetail* m_dmesh = rcAllocPolyMeshDetail();
        if (!m_dmesh)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'pmdtl'.");
            return false;
        }

        if (!rcBuildPolyMeshDetail(&mCtx, *m_pmesh, *m_chf, mCfg.detailSampleDist, mCfg.detailSampleMaxError, *m_dmesh))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build detail mesh.");
            return false;
        }

        std::cout << "num verts " << m_dmesh->nverts << std::endl;
        if (!mKeepIntermediateResults)
        {
            rcFreeCompactHeightfield(m_chf);
            m_chf = 0;
            rcFreeContourSet(m_cset);
            m_cset = 0;
        }


        if (mCfg.maxVertsPerPoly <= DT_VERTS_PER_POLYGON)
        {
            unsigned char* navData = 0;
            int navDataSize = 0;

            // Update poly flags from areas.
            /*for (int i = 0; i < m_pmesh->npolys; ++i)
            {
                if (m_pmesh->areas[i] == RC_WALKABLE_AREA)
                    m_pmesh->areas[i] = SAMPLE_POLYAREA_GROUND;

                if (m_pmesh->areas[i] == SAMPLE_POLYAREA_GROUND ||
                    m_pmesh->areas[i] == SAMPLE_POLYAREA_GRASS ||
                    m_pmesh->areas[i] == SAMPLE_POLYAREA_ROAD)
                {
                    m_pmesh->flags[i] = SAMPLE_POLYFLAGS_WALK;
                }
                else if (m_pmesh->areas[i] == SAMPLE_POLYAREA_WATER)
                {
                    m_pmesh->flags[i] = SAMPLE_POLYFLAGS_SWIM;
                }
                else if (m_pmesh->areas[i] == SAMPLE_POLYAREA_DOOR)
                {
                    m_pmesh->flags[i] = SAMPLE_POLYFLAGS_WALK | SAMPLE_POLYFLAGS_DOOR;
                }
            }*/

            for (int i = 0; i < m_pmesh->npolys; ++i){
                m_pmesh->flags[i] = 0xFFFF;
            }


            dtNavMeshCreateParams params;
            memset(&params, 0, sizeof(params));
            params.verts = m_pmesh->verts;
            params.vertCount = m_pmesh->nverts;
            params.polys = m_pmesh->polys;
            params.polyAreas = m_pmesh->areas;
            params.polyFlags = m_pmesh->flags;
            params.polyCount = m_pmesh->npolys;
            params.nvp = m_pmesh->nvp;
            params.detailMeshes = m_dmesh->meshes;
            params.detailVerts = m_dmesh->verts;
            params.detailVertsCount = m_dmesh->nverts;
            params.detailTris = m_dmesh->tris;
            params.detailTriCount = m_dmesh->ntris;
            // params.offMeshConVerts = m_geom->getOffMeshConnectionVerts();
            // params.offMeshConRad = m_geom->getOffMeshConnectionRads();
            // params.offMeshConDir = m_geom->getOffMeshConnectionDirs();
            // params.offMeshConAreas = m_geom->getOffMeshConnectionAreas();
            // params.offMeshConFlags = m_geom->getOffMeshConnectionFlags();
            // params.offMeshConUserID = m_geom->getOffMeshConnectionId();
            // params.offMeshConCount = m_geom->getOffMeshConnectionCount();
            params.walkableHeight = mAgentHeight;
            params.walkableRadius = mAgentRadius;
            params.walkableClimb = mAgentMaxClimb;
            rcVcopy(params.bmin, m_pmesh->bmin);
            rcVcopy(params.bmax, m_pmesh->bmax);
            params.cs = mCfg.cs;
            params.ch = mCfg.ch;
            params.buildBvTree = true;

            if (!dtCreateNavMeshData(&params, &navData, &navDataSize))
            {
                mCtx.log(RC_LOG_ERROR, "Could not build Detour navmesh.");
                return false;
            }

            dtNavMesh* m_navMesh = dtAllocNavMesh();
            if (!m_navMesh)
            {
                dtFree(navData);
                mCtx.log(RC_LOG_ERROR, "Could not create Detour navmesh");
                return false;
            }

            dtStatus status;

            status = m_navMesh->init(navData, navDataSize, DT_TILE_FREE_DATA);
            if (dtStatusFailed(status))
            {
                dtFree(navData);
                mCtx.log(RC_LOG_ERROR, "Could not init Detour navmesh");
                return false;
            }

            /*dtNavMeshQuery* m_navQuery = dtAllocNavMeshQuery();
            status = m_navQuery->init(m_navMesh, 2048);
            if (dtStatusFailed(status))
            {
                mCtx.log(RC_LOG_ERROR, "Could not init Detour navmesh query");
                return false;
            }*/

            outData->mesh = m_navMesh;
        }


        //delete[] tris;
        //delete[] verts;

        return true;
    }



























    unsigned char* NavMeshManager::_buildTileMesh(const GenerateMeshInput& in, CollectedNavMeshData* outData, int tx, int ty, const float* bmin, const float* bmax){

        bool mKeepIntermediateResults = false;

        //TODO move this somewhere else
        rcChunkyTriMesh* chunkyMesh = new rcChunkyTriMesh;
        assert(chunkyMesh);
        if(!rcCreateChunkyTriMesh(in.verts, in.tris, in.numTris, 256, chunkyMesh))
        {
            //ctx->log(RC_LOG_ERROR, "buildTiledNavigation: Failed to build chunky mesh.");
            return 0;
        }

        assert(in.data);
        float mCellHeight = in.data->settings.cellHeight;
        float mCellSize = in.data->settings.cellSize;
        float mAgentHeight = in.data->settings.agentHeight;
        float mAgentMaxClimb = in.data->settings.agentMaxClimb;
        float mAgentRadius = in.data->settings.agentRadius;
        float mAgentMaxSlope = in.data->settings.agentMaxSlope;
        float mEdgeMaxLen = in.data->settings.edgeMaxLen;
        float mEdgeMaxError = in.data->settings.edgeMaxError;
        float mVertsPerPoly = in.data->settings.vertsPerPoly;
        float mDetailSampleDist = in.data->settings.detailSampleDist;
        float mDetailSampleMaxError = in.data->settings.detailSampleMaxError;
        float mRegionMinSize = in.data->settings.regionMinSize;
        float mRegionMergeSize = in.data->settings.regionMergeSize;

        rcConfig mCfg;
        memset(&mCfg, 0, sizeof(mCfg));
        mCfg.cs = mCellSize;
        mCfg.ch = mCellHeight;
        mCfg.walkableSlopeAngle = mAgentMaxSlope;
        mCfg.walkableHeight = (int)ceilf(mAgentHeight / mCfg.ch);
        mCfg.walkableClimb = (int)floorf(mAgentMaxClimb / mCfg.ch);
        mCfg.walkableRadius = (int)ceilf(mAgentRadius / mCfg.cs);
        mCfg.maxEdgeLen = (int)(mEdgeMaxLen / mCellSize);
        mCfg.maxSimplificationError = mEdgeMaxError;
        mCfg.minRegionArea = (int)rcSqr(mRegionMinSize);        // Note: area = size*size
        mCfg.mergeRegionArea = (int)rcSqr(mRegionMergeSize);    // Note: area = size*size
        mCfg.maxVertsPerPoly = (int)mVertsPerPoly;
        mCfg.detailSampleDist = mDetailSampleDist < 0.9f ? 0 : mCellSize * mDetailSampleDist;
        mCfg.detailSampleMaxError = mCellHeight * mDetailSampleMaxError;

        mCfg.tileSize = in.data->tileSize;
        mCfg.borderSize = mCfg.walkableRadius + 0; // Reserve enough padding.
        mCfg.width = mCfg.tileSize + mCfg.borderSize*2;
        mCfg.height = mCfg.tileSize + mCfg.borderSize*2;

        rcVcopy(mCfg.bmin, bmin);
        rcVcopy(mCfg.bmax, bmax);
        mCfg.bmin[0] -= mCfg.borderSize*mCfg.cs;
        mCfg.bmin[2] -= mCfg.borderSize*mCfg.cs;
        mCfg.bmax[0] += mCfg.borderSize*mCfg.cs;
        mCfg.bmax[2] += mCfg.borderSize*mCfg.cs;

        //rcCalcGridSize(mCfg.bmin, mCfg.bmax, mCfg.cs, &mCfg.width, &mCfg.height);
        rcContext mCtx;

        rcHeightfield* mSolid = rcAllocHeightfield();
        assert(mSolid);
        if (!rcCreateHeightfield(&mCtx, *mSolid, mCfg.width, mCfg.height, mCfg.bmin, mCfg.bmax, mCfg.cs, mCfg.ch))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not create solid heightfield.");
            return 0;
        }

        unsigned char* mTriareas = new unsigned char[in.numTris];

        memset(mTriareas, 0, in.numTris*sizeof(unsigned char));

        int m_tileTriCount = 0;

        float tbmin[2], tbmax[2];
        tbmin[0] = mCfg.bmin[0];
        tbmin[1] = mCfg.bmin[2];
        tbmax[0] = mCfg.bmax[0];
        tbmax[1] = mCfg.bmax[2];
        int cid[512];
        const int ncid = rcGetChunksOverlappingRect(chunkyMesh, tbmin, tbmax, cid, 512);
        if (!ncid)
            return 0;

        m_tileTriCount = 0;

        for (int i = 0; i < ncid; ++i)
        {
            const rcChunkyTriMeshNode& node = chunkyMesh->nodes[cid[i]];
            const int* ctris = &chunkyMesh->tris[node.i*3];
            const int nctris = node.n;

            m_tileTriCount += nctris;

            //NOTE pay attention.
            const int nverts = in.numVerts;
            const int ntris = in.numTris;

            memset(mTriareas, 0, nctris*sizeof(unsigned char));
            rcMarkWalkableTriangles(&mCtx, mCfg.walkableSlopeAngle,
                                    in.verts, nverts, ctris, nctris, mTriareas);

            if (!rcRasterizeTriangles(&mCtx, in.verts, nverts, ctris, mTriareas, nctris, *mSolid, mCfg.walkableClimb))
                return 0;
        }

        if (!mKeepIntermediateResults)
        {
            delete [] mTriareas;
            mTriareas = 0;
        }

        // 3. Filter walkable surfaces
        bool filterLowHangingObstacles = false;
        bool filterLedgeSpans = false;
        bool filterWalkableLowHeightSpans = false;
        if (filterLowHangingObstacles)
            rcFilterLowHangingWalkableObstacles(&mCtx, mCfg.walkableClimb, *mSolid);
        if (filterLedgeSpans)
            rcFilterLedgeSpans(&mCtx, mCfg.walkableHeight, mCfg.walkableClimb, *mSolid);
        if (filterWalkableLowHeightSpans)
            rcFilterWalkableLowHeightSpans(&mCtx, mCfg.walkableHeight, *mSolid);

        // 4. Partition walkable surface into simple regions.
        rcCompactHeightfield* m_chf = rcAllocCompactHeightfield();
        if (!m_chf)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'chf'.");
            return 0;
        }
        if (!rcBuildCompactHeightfield(&mCtx, mCfg.walkableHeight, mCfg.walkableClimb, *mSolid, *m_chf))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build compact data.");
            return 0;
        }

        if (!mKeepIntermediateResults)
        {
            rcFreeHeightField(mSolid);
            mSolid = 0;
        }

        // Erode the walkable area by agent radius.
        if (!rcErodeWalkableArea(&mCtx, mCfg.walkableRadius, *m_chf))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not erode.");
            return 0;
        }

        // (Optional) Mark areas.
        // const ConvexVolume* vols = m_geom->getConvexVolumes();
        // for (int i  = 0; i < m_geom->getConvexVolumeCount(); ++i)
        //     rcMarkConvexPolyArea(&mCtx, vols[i].verts, vols[i].nverts, vols[i].hmin, vols[i].hmax, (unsigned char)vols[i].area, *m_chf);


        //Partition watershed
        {
            // Prepare for region partitioning, by calculating distance field along the walkable surface.
            if (!rcBuildDistanceField(&mCtx, *m_chf))
            {
                mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build distance field.");
                return 0;
            }

            // Partition the walkable surface into simple regions without holes.
            if (!rcBuildRegions(&mCtx, *m_chf, mCfg.borderSize, mCfg.minRegionArea, mCfg.mergeRegionArea))
            {
                mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build watershed regions.");
                return 0;
            }
        }
        //rcBuildRegionsMonotone(&mCtx, *m_chf, 0, mCfg.minRegionArea, mCfg.mergeRegionArea);if (!rcBuildLayerReg
        //rcBuildLayerRegions(&mCtx, *m_chf, 0, mCfg.minRegionArea);

        //
        // Step 5. Trace and simplify region contours.
        //

        // Create contours.
        rcContourSet* m_cset = rcAllocContourSet();
        if (!m_cset)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'cset'.");
            return 0;
        }
        if (!rcBuildContours(&mCtx, *m_chf, mCfg.maxSimplificationError, mCfg.maxEdgeLen, *m_cset))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not create contours.");
            return 0;
        }

        if(m_cset->nconts == 0){
            std::cout << "nconts: " << m_cset->nconts << std::endl;
            return 0;
        }

        //
        // Step 6. Build polygons mesh from contours.
        //

        // Build polygon navmesh from the contours.
        rcPolyMesh* m_pmesh = rcAllocPolyMesh();
        if (!m_pmesh)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'pmesh'.");
            return 0;
        }
        if (!rcBuildPolyMesh(&mCtx, *m_cset, mCfg.maxVertsPerPoly, *m_pmesh))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not triangulate contours.");
            return 0;
        }

        //
        // Step 7. Create detail mesh which allows to access approximate height on each polygon.
        //

        rcPolyMeshDetail* m_dmesh = rcAllocPolyMeshDetail();
        if (!m_dmesh)
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Out of memory 'pmdtl'.");
            return 0;
        }

        if (!rcBuildPolyMeshDetail(&mCtx, *m_pmesh, *m_chf, mCfg.detailSampleDist, mCfg.detailSampleMaxError, *m_dmesh))
        {
            mCtx.log(RC_LOG_ERROR, "buildNavigation: Could not build detail mesh.");
            return 0;
        }

        std::cout << "num verts " << m_dmesh->nverts << std::endl;
        if(m_dmesh->nverts == 0){
            return 0;
        }

        if (!mKeepIntermediateResults)
        {
            rcFreeCompactHeightfield(m_chf);
            m_chf = 0;
            rcFreeContourSet(m_cset);
            m_cset = 0;
        }


        unsigned char* navData = 0;
        int navDataSize = 0;
        if (mCfg.maxVertsPerPoly <= DT_VERTS_PER_POLYGON)
        {

            // Update poly flags from areas.
            /*for (int i = 0; i < m_pmesh->npolys; ++i)
            {
                if (m_pmesh->areas[i] == RC_WALKABLE_AREA)
                    m_pmesh->areas[i] = SAMPLE_POLYAREA_GROUND;

                if (m_pmesh->areas[i] == SAMPLE_POLYAREA_GROUND ||
                    m_pmesh->areas[i] == SAMPLE_POLYAREA_GRASS ||
                    m_pmesh->areas[i] == SAMPLE_POLYAREA_ROAD)
                {
                    m_pmesh->flags[i] = SAMPLE_POLYFLAGS_WALK;
                }
                else if (m_pmesh->areas[i] == SAMPLE_POLYAREA_WATER)
                {
                    m_pmesh->flags[i] = SAMPLE_POLYFLAGS_SWIM;
                }
                else if (m_pmesh->areas[i] == SAMPLE_POLYAREA_DOOR)
                {
                    m_pmesh->flags[i] = SAMPLE_POLYFLAGS_WALK | SAMPLE_POLYFLAGS_DOOR;
                }
            }*/

            for (int i = 0; i < m_pmesh->npolys; ++i){
                m_pmesh->flags[i] = 0xFFFF;
            }


            dtNavMeshCreateParams params;
            memset(&params, 0, sizeof(params));
            params.verts = m_pmesh->verts;
            params.vertCount = m_pmesh->nverts;
            params.polys = m_pmesh->polys;
            params.polyAreas = m_pmesh->areas;
            params.polyFlags = m_pmesh->flags;
            params.polyCount = m_pmesh->npolys;
            params.nvp = m_pmesh->nvp;
            params.detailMeshes = m_dmesh->meshes;
            params.detailVerts = m_dmesh->verts;
            params.detailVertsCount = m_dmesh->nverts;
            params.detailTris = m_dmesh->tris;
            params.detailTriCount = m_dmesh->ntris;
            // params.offMeshConVerts = m_geom->getOffMeshConnectionVerts();
            // params.offMeshConRad = m_geom->getOffMeshConnectionRads();
            // params.offMeshConDir = m_geom->getOffMeshConnectionDirs();
            // params.offMeshConAreas = m_geom->getOffMeshConnectionAreas();
            // params.offMeshConFlags = m_geom->getOffMeshConnectionFlags();
            // params.offMeshConUserID = m_geom->getOffMeshConnectionId();
            // params.offMeshConCount = m_geom->getOffMeshConnectionCount();
            params.walkableHeight = mAgentHeight;
            params.walkableRadius = mAgentRadius;
            params.walkableClimb = mAgentMaxClimb;
            rcVcopy(params.bmin, m_pmesh->bmin);
            rcVcopy(params.bmax, m_pmesh->bmax);
            params.cs = mCfg.cs;
            params.ch = mCfg.ch;
            params.buildBvTree = true;

            params.tileX = tx;
            params.tileY = ty;
            params.tileLayer = 0;

            if (!dtCreateNavMeshData(&params, &navData, &navDataSize))
            {
                mCtx.log(RC_LOG_ERROR, "Could not build Detour navmesh.");
                return 0;
            }
        }

        outData->navDataSize = navDataSize;

        return navData;
    }
}
