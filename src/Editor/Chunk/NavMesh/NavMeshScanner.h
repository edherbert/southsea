#pragma once

#include <set>
#include <vector>
#include <map>
#include "Editor/Scene/SceneTypes.h"

namespace Ogre{
    class SceneNode;
}

namespace Southsea{
    class TreeObjectManager;

    /**
    A class responsible for scanning and creation of buffers for recast.
    It accepts a list of nav markers and is able to generate buffers by scanning the scene tree for included shapes.
    */
    class NavMeshScanner{
    public:
        NavMeshScanner();
        ~NavMeshScanner();
        typedef const std::vector<SceneEntry>& SceneContainer;
        typedef const std::map<EntryId, ObjectData*>& Tree;
        typedef const std::map<EntryId, Ogre::SceneNode*>& OgreTree;

        //A function which should specify whether or not a certain scene object should be comitted for nav mesh generation with given rules.
        typedef bool(*CommitCheckFunction)(const NavMarkerRules* parentRules, const SceneEntry& e, Tree data, OgreTree ogreData);

        struct MeshBuffers{
            int* tris;
            float* verts;
            size_t numTris;
            size_t numVerts;
        };
        void createNavMeshBuffers(const std::set<EntryId>& contributingMarkers, MeshBuffers* outBuffers, bool includeTerrain);

        void scanTreeForMarker(SceneContainer sceneEntries, Tree data, OgreTree ogreData, CommitCheckFunction func);
        size_t processMarker(SceneContainer sceneEntries, Tree data, OgreTree ogreData, int depth, size_t startIdx, EntryId parentNode, const NavMarkerRules* parentRules, CommitCheckFunction func);

        int resolveFinalRulesForParent(SceneContainer scene, Tree data, OgreTree ogreData, size_t startIndex, const NavMarkerRules* parentRules, NavMarkerRules* outRules);

    private:
        size_t _parseTree(SceneContainer scene, Tree data, OgreTree ogreData, int depth, size_t startIdx, const NavMarkerRules* parentRules, CommitCheckFunction func);

        static bool checkObjectCounter(const NavMarkerRules* parentRules, const SceneEntry& e, Tree data, OgreTree ogreData);
        static bool writeToBuffers(const NavMarkerRules* parentRules, const SceneEntry& e, Tree data, OgreTree ogreData);

        struct ScannedBufferData{
            size_t numTriangles;
            size_t numVerts;

            size_t totalPhysicsShapes;
            size_t totalChecked;
            size_t totalComitted;
        };
        static ScannedBufferData mScannedData;
    };
}
