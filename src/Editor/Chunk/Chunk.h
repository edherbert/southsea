#pragma once

#include <memory>
#include "System/Project/Map.h"

#include "OgreVector3.h"

namespace Ogre{
    class Texture;
    class SceneManager;
}

namespace Southsea{
    class TerrainManager;
    class NavMeshManager;
    class Editor;

    /**
    A class to encapsulate what goes on in the editor.
    It's similar to the world in the engine, as it contains lots of logic about what's currently being rendered (terrain, item hierarchy).

    Created and destroyed on editor startup and shutdown.
    When the chunk is switched in the editor, functions should be called on this class to update it.
    There should only ever be a single EditedChunk instance at a time, but this base class can have as many as needed.
    */
    class Chunk{
    public:
        Chunk(Editor* creatorEditor, const Map& map, int chunkSize, int chunkId);
        ~Chunk();
        virtual void initialise(Ogre::SceneManager* manager, const Ogre::Vector3& pos);

        typedef std::pair<int, int> ChunkCoordinate;

        virtual void setCurrentChunk(int chunkX, int chunkY);
        void prepareChunkSwitch();
        void setVisible(bool visible);

        virtual void destroy();

        void update();

        bool assureChunkDirectory();
        bool assureTerrainDirectory();
        bool assureNavDirectory();

        std::string getChunkDirectory() const;
        std::string getTerrainDirectory() const;
        std::string getNavDirectory() const;
        std::string getCoordsString() const;
        std::string getDatablockName() const;
        std::string getEngineMetaFilePath() const;

        std::string getNavDirectoryMetaFile() const;

        ChunkCoordinate getChunkCoordinates() const { return {mChunkX, mChunkY}; }

        virtual void save();

        std::shared_ptr<TerrainManager> getTerrainManager() { return mTerrainManager; }

        static const int MAX_CHUNK;
        static const unsigned char CHUNK_DIGITS;

    protected:
        Editor* mEditor;
        int mChunkX, mChunkY;
        int mChunkSize;
        int mChunkId;
        Map mMap; //So the chunk knows which map it's a part of, and where to find its contents.
        Ogre::Vector3 mPosition;

        std::shared_ptr<TerrainManager> mTerrainManager;

        //Check if a file exists in the directory specified.
        bool _fileExists(const std::string fileName, const std::string dirPath);

        //Prepare the texture for the newly created terrain.
        void _prepareTerrainTexture(Ogre::Texture& tex);

        bool _assureDirectory(const std::string& path);
        void _destroyOgreGroup();

        void _setupOgre();

        //Whether this chunk was created previously. This is used in the teardown procedure.
        bool mUsedPreviously = false;
    };
}
