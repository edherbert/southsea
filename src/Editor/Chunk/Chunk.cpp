#include "Chunk.h"

#include "Terrain/TerrainManager.h"

#include "OgreRoot.h"
#include "OgreHlmsManager.h"
#include "OgreHlms.h"
#include "OgreTextureManager.h"
#include <OgreHardwarePixelBuffer.h>
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraDatablock.h"
#include "Editor/Chunk/Terrain/TerrainDatablockInfoManager.h"
#include "Editor/Chunk/Terrain/DatablockJsonWriter.h"

#include "Editor/Editor.h"
#include "Editor/Scene/SceneSerialiser.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"

#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "OgreCamera.h"
#include <iostream>

#include "filesystem/path.h"

namespace Southsea{

    const int Chunk::MAX_CHUNK = 9999;
    const unsigned char Chunk::CHUNK_DIGITS = 4;

    Chunk::Chunk(Editor* creatorEditor, const Map& map, int chunkSize, int chunkId)
        : mChunkX(0),
          mChunkY(0),
          mChunkId(chunkId),
          mMap(map),
          mEditor(creatorEditor),
          mChunkSize(chunkSize),
          mTerrainManager(std::make_shared<TerrainManager>(chunkId)) {

    }

    Chunk::~Chunk(){

    }

    void Chunk::initialise(Ogre::SceneManager* manager, const Ogre::Vector3& pos){
        mPosition = pos;
        mTerrainManager->setup(manager);
    }

    void Chunk::prepareChunkSwitch(){
        if(mUsedPreviously){
            //If this chunk was used previously (this is a chunk switch rather than first creation), then destroy what came before.
            _destroyOgreGroup();
        }
    }

    void Chunk::setCurrentChunk(int chunkX, int chunkY){

        mChunkX = chunkX;
        mChunkY = chunkY;

        _setupOgre();

        mUsedPreviously = true;
    }

    void Chunk::setVisible(bool visible){
        mTerrainManager->setVisible(visible);
    }

    void Chunk::_setupOgre(){
        Ogre::Root& root = Ogre::Root::getSingleton();
        const std::string coordsString(getCoordsString());
        const std::string terrainDirectory(getTerrainDirectory());

        _assureDirectory(terrainDirectory); //Make sure it actually exists before creating it as a resouce location. Even though it might be empty.
        Ogre::ResourceGroupManager::getSingleton().createResourceGroup(coordsString, false);
        root.addResourceLocation(terrainDirectory, "FileSystem", coordsString); //The resource group is the coordinates of this chunk.

        Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup(coordsString, false);


        const Ogre::String datablockName = getDatablockName();
        TerrainDatablockInfoManager::assureDatablockEntry(datablockName);
        {
            //Determine the datablock for terra to use.
            Ogre::Hlms* terraHlms = root.getHlmsManager()->getHlms("Terra");
            Ogre::HlmsDatablock *datablock = terraHlms->getDatablock(datablockName);
            if(datablock){ //If a datablock was found, check it was created within our chunk resource group.
                Ogre::String const *fileName;
                Ogre::String const *resourceGroup;
                terraHlms->getFilenameAndResourceGroup(datablockName, &fileName, &resourceGroup);
                if(*resourceGroup != coordsString){
                    datablock = 0; //If not from the correct group, set it to an invalid pointer. The default datablock will be used instead.
                }else{
                    //Check which of the textures were defined in the datablock. Otherwise we need to insert the default ones.
                    Ogre::HlmsTerraDatablock* tBlock = dynamic_cast<Ogre::HlmsTerraDatablock*>(datablock);
                    const Ogre::TerraTextureTypes t[4] = {Ogre::TERRA_DETAIL0, Ogre::TERRA_DETAIL1, Ogre::TERRA_DETAIL2, Ogre::TERRA_DETAIL3};
                    for(int i = 0; i < 4; i++){

                        const TerrainDatablockData& d = TerrainDatablockInfoManager::getDatablockData(datablockName);

                        if(!d.layers[i].diffuseTexture.empty()) continue; //We only want to insert the dummy texture into blank slots in the list.

                        Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName("terraDefaultTex" + std::to_string(i));

                        tBlock->setTexture(t[i], 0, tex);
                    }
                }
            }

            if(!datablock){
                //If the named datablock wasn't found use the default one.

                datablock = root.getHlmsManager()->getDatablock("defaultTerra"); //I'm assuming one with this name always exists.

                datablock->clone(datablockName); //I need to create a clone to enable editing without altering the default block.
            }


            TerrainDatablockInfoManager::setupDatablockData((Ogre::HlmsTerraDatablock*)datablock);
        }
        //By this point there should always be a datablock with the same name as the variable datablockName above.

        Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName("blendMapTarget" + std::to_string(mChunkId));
        _prepareTerrainTexture(*tex);

        //Determine which terrain textures are available.
        bool heightExists = _fileExists("height.png", terrainDirectory);
        int slotSize = mChunkSize;
        if(heightExists){
            mTerrainManager->initialise("height.png", coordsString, datablockName, slotSize, mPosition);
        }else{
            mTerrainManager->initialise(0.0f, datablockName, slotSize, mPosition); //If none exist use a flat value rather than trying to load something in.
        }
    }

    void Chunk::destroy(){
        mTerrainManager->tearDown(); //Destroy the terrain.

        _destroyOgreGroup();

    }

    void Chunk::_destroyOgreGroup(){
        Ogre::Root& root = Ogre::Root::getSingleton();
        const std::string coordsString(getCoordsString());
        const std::string terrainDirectory(getTerrainDirectory());

        //When removing the group we need to make sure the terrain isn't still using the datablock.
        mTerrainManager->resetTerrainDatablock();

        root.removeResourceLocation(terrainDirectory);
        Ogre::ResourceGroupManager::getSingleton().destroyResourceGroup(coordsString);

        const Ogre::String dbName(getDatablockName());
        //Destroy the datablock created for the terrain separately, as there's no guarantee it's part of the resource group (if it was a clone of the default).
        root.getHlmsManager()->getHlms("Terra")->destroyDatablock(dbName);
        TerrainDatablockInfoManager::removeDatablockEntry(dbName);

        //Destroy the datablocks in this group.
        std::vector<Ogre::HlmsDatablock*> blocksToRemove;
        Ogre::HlmsManager* man = Ogre::Root::getSingleton().getHlmsManager();
        auto itor = man->getDatablocks().begin();
        auto end = man->getDatablocks().end();
        while( itor != end ){

            Ogre::String const *fileName;
            Ogre::String const *resourceGroup;
            itor->second->getFilenameAndResourceGroup(&fileName, &resourceGroup);
            if(fileName && resourceGroup && !fileName->empty() && !resourceGroup->empty()){
                //Valid filename & resource group.
                //I don't touch the filename itself, but having it there does mean that the datablock came from a file originally.
                if(*resourceGroup == coordsString){
                    //I push them to a vector rather than destroying them at this point. This is mostly to avoid the concern that I'm invalidating the iterator.
                    blocksToRemove.push_back(itor->second);
                }
            }

            ++itor;
        }

        for(Ogre::HlmsDatablock* d : blocksToRemove){
            const Ogre::String dbName( *(d->getNameStr()) );
            std::cout << "Destroying datablock with name " << dbName << '\n';
            Ogre::Hlms* h = d->getCreator();
            h->destroyDatablock(d->getName());

            TerrainDatablockInfoManager::removeDatablockEntry(dbName);
        }
    }

    void Chunk::_prepareTerrainTexture(Ogre::Texture& tex){
        const std::string coordsString(getCoordsString());
        const std::string terrainDirectory(getTerrainDirectory());

        const std::string detailMapName = getDatablockName() + "detail.png";
        bool detailExists = _fileExists(detailMapName, terrainDirectory);

        using namespace Ogre;
        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = tex.getBuffer(0);
        Box b(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight());
        const PixelBox &destination = pixelBufferBuf->lock(b, v1::HardwareBuffer::HBL_NORMAL);

        //Clear the texture
        //The texture gets cleared even if there is an image to load, just to make sure there's no garbled data.
        Ogre::uint8* pDest = static_cast<Ogre::uint8*>(destination.data);
        for(int y = 0; y < tex.getHeight(); y++){
            for(int x = 0; x < tex.getWidth(); x++){
                *pDest++ = 0;
                *pDest++ = 0;
                *pDest++ = 255; //Populate it with all of a single colour just to show something.
                *pDest++ = 0;
            }
        }

        if(detailExists){
            Ogre::Image img;
            img.load(detailMapName, coordsString);
            PixelUtil::bulkPixelConversion( img.getPixelBox(0), destination );
        }

        pixelBufferBuf->unlock();
    }

    void Chunk::update(){
        mTerrainManager->updateTerrain();
    }

    void Chunk::save(){
        const Ogre::String terrainDirectory(getTerrainDirectory());

        assureTerrainDirectory();
        assureNavDirectory();

        { //Save the datablock to a json file.
            Ogre::Hlms* terraHlms = Ogre::Root::getSingleton().getHlmsManager()->getHlms("Terra");
            Ogre::HlmsDatablock *datablock = terraHlms->getDatablock(getDatablockName());

            DatablockJsonWriter::writeDatablock(terrainDirectory, datablock);
        }

        mTerrainManager->saveTextures(terrainDirectory, getDatablockName());
    }

    std::string Chunk::getNavDirectory() const{
        const filesystem::path p(getChunkDirectory());

        return (p / filesystem::path("nav")).str();
    }

    std::string Chunk::getTerrainDirectory() const{
        const filesystem::path p(getChunkDirectory());

        return (p / filesystem::path("terrain")).str();
    }

    std::string Chunk::getChunkDirectory() const{
        const filesystem::path mapPath(mMap.getMapPath());
        const filesystem::path chunkPath(getCoordsString());

        return filesystem::path(mapPath / chunkPath).str();
    }

    std::string Chunk::getEngineMetaFilePath() const{
        const filesystem::path navPath(getChunkDirectory());

        return filesystem::path(navPath / "chunkMeta.json").str();
    }

    std::string Chunk::getNavDirectoryMetaFile() const {
        const filesystem::path navPath(getNavDirectory());

        return filesystem::path(navPath / "navMeshData.json").str();
    }

    std::string Chunk::getCoordsString() const{
        std::string xVal = std::to_string(abs(mChunkX));
        std::string yVal = std::to_string(abs(mChunkY));

        std::string xString = std::string(4 - xVal.length(), '0') + xVal;
        std::string yString = std::string(4 - yVal.length(), '0') + yVal;
        if(mChunkX < 0) xString = "-" + xString;
        if(mChunkY < 0) yString = "-" + yString;

        return xString + yString;
    }

    std::string Chunk::getDatablockName() const{
        return "Terra" + mMap.getMapName() + getCoordsString();
    }

    bool Chunk::_fileExists(const std::string fileName, const std::string dirPath){
        const filesystem::path filePath(filesystem::path(dirPath) / filesystem::path(fileName));

        if(filePath.exists() && filePath.is_file()) return true;

        return false;
    }

    bool Chunk::_assureDirectory(const std::string& path){
        filesystem::path p(path);

        if(!p.exists()){
            filesystem::create_directory(p);
            return true;
        }

        if(p.is_file()){
            p.remove_file();
            filesystem::create_directory(p);
        }

        assert(p.is_directory()); //Should be a directory at this point.

        //It exists
        return true;
    }

    bool Chunk::assureChunkDirectory(){
        return _assureDirectory(getChunkDirectory());
    }

    bool Chunk::assureTerrainDirectory(){
        assureChunkDirectory(); //Make sure of this before the terrain.
        return _assureDirectory(getTerrainDirectory());
    }

    bool Chunk::assureNavDirectory(){
        assureChunkDirectory();
        return _assureDirectory(getNavDirectory());
    }

}
