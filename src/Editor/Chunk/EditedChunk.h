#pragma once

#include "Chunk.h"
#include "System/Project/ChunkMetaFile.h"
#include "Meta/EngineChunkMeta.h"

namespace Southsea{

    /**
    The current chunk being edited by the editor.
    This is different from regular chunks which might just be loaded as a visual guideline.
    */
    class EditedChunk : public Chunk{
    public:
        EditedChunk(Editor* creatorEditor, const Map& map, int chunkSize);
        ~EditedChunk();

        void initialise(Ogre::SceneManager* manager, const Ogre::Vector3& pos);
        void destroy();

        void save();

        void setCurrentChunk(int chunkX, int chunkY);

        std::shared_ptr<NavMeshManager> getNavMeshManager() { return mNavMeshManager; }
        ChunkMetaFile& getChunkMetaFile() { return mMetaFile; }
        EngineChunkMeta& getEngineChunkMetaFile() { return mEngineChunkMeta; }
    private:
        //Stores settings specific to this chunk.
        ChunkMetaFile mMetaFile;
        //Meta file specifically for this chunk, which is used by the engine.
        EngineChunkMeta mEngineChunkMeta;

        std::shared_ptr<NavMeshManager> mNavMeshManager;
    };
}
