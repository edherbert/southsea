#include "EditedChunk.h"

#include "NavMesh/NavMeshJsonWriter.h"
#include "NavMesh/NavMeshManager.h"
#include "Terrain/TerrainManager.h"
#include "OgreCamera.h"

#include "Editor/Scene/SceneSerialiser.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"

#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "Editor/Editor.h"

namespace Southsea{
    EditedChunk::EditedChunk(Editor* creatorEditor, const Map& map, int chunkSize)
        : Chunk(creatorEditor, map, chunkSize, -1),
          mNavMeshManager(std::make_shared<NavMeshManager>()) {

    }

    EditedChunk::~EditedChunk(){

    }

    void EditedChunk::initialise(Ogre::SceneManager* manager, const Ogre::Vector3& pos){
        Chunk::initialise(manager, pos);
        mNavMeshManager->initialiseDrawer(manager);
    }

    void EditedChunk::destroy(){
        Chunk::destroy();
        mMetaFile.writeFile(mEditor->getSceneTreeManager(), mNavMeshManager);
    }

    void EditedChunk::save(){
        { //Save the nav mesh data.
            //TODO These get functions need to be cleaned up. They're pretty inefficient right now.
            const std::string dirPath = getNavDirectoryMetaFile();
            const std::vector<NavMeshData>& d = mNavMeshManager->getCurrentMeshes();
            NavMeshJsonWriter::writeMeshMeta(dirPath, d);

            mNavMeshManager->saveBinaryNavMeshes(getNavDirectory());
        }

        {
            mMetaFile.writeFile(mEditor->getSceneTreeManager(), mNavMeshManager);
            mEngineChunkMeta.writeFile(getEngineMetaFilePath());
        }
        Chunk::save();
    }

    void EditedChunk::setCurrentChunk(int chunkX, int chunkY){
        auto treeMan = mEditor->getSceneTreeManager();
        auto ogreMan = mEditor->getOgreTreeManager();
        auto objectMan = mEditor->getTreeObjectManager();

        if(mUsedPreviously){
            mMetaFile.writeFile(treeMan, mNavMeshManager);
        }

        //---Parent---
        Chunk::setCurrentChunk(chunkX, chunkY);

        mMetaFile.setChunk(*this);
        bool parseResult = mMetaFile.parseFile(treeMan, mNavMeshManager);
        if(!parseResult){
            //If this is the first chunks switch, the user is starting up a project at a chunk which was not saved.
            //If that's the case, set a default position. If this is not the first chunk switch,
            //the idea is that the user would have positioned the camera in some way anyway, so just keep that.
            if(!mUsedPreviously){
                //Set some defaults if the file could not be parsed.
                Ogre::Camera* cam = mEditor->getEditorCamera();
                cam->setPosition(0, 0, 100);
                cam->lookAt(Ogre::Vector3::ZERO);

                mMetaFile.setCameraPosition(cam->getPosition());
                mMetaFile.setCameraOrientation(cam->getOrientation(), 0, 0);
            }
        }

        treeMan->clearCurrentContent();
        ogreMan->clearCurrentContent(objectMan.get());


        SceneSerialiser serialiser;
        serialiser.deserialise(*treeMan, *ogreMan, *objectMan, getChunkDirectory());

        //Sanity check incase some valid ids are provided (or scene parsing failed in which case the ids will be invalid)
        treeMan->removeInvalidSelections();


        //Read engine meta file.
        {
            mEngineChunkMeta.parseFile(getEngineMetaFilePath());
            mTerrainManager->setTerrainEnabled(mEngineChunkMeta.getTerrainEnabled());
        }

        NavMeshId id = mNavMeshManager->getCurrentSelectedMesh();
        mNavMeshManager->clearAllNavMeshes();
        mNavMeshManager->setCurrentSelectedMesh(id);
        mNavMeshManager->loadFromMeshFile(getNavDirectoryMetaFile());
        mNavMeshManager->loadBinaryNavMeshes(getNavDirectory());

        mEditor->setCameraPosition(mMetaFile.getCameraPosition());
        mEditor->setCameraOrientation(mMetaFile.getCameraOrientation(), mMetaFile.getCameraYaw(), mMetaFile.getCameraPitch());

        {
            size_t selectedCount = treeMan->getSelectedCount();
            if(selectedCount > 0){
                //If something is selected at this point, it was because the selection was read from the meta file.
                SceneTreeSelectionChangedEvent e;
                e.selectionCount = selectedCount;
                EventDispatcher::transmitEvent(EventType::Editor, e);
            }
        }

        ChunkSwitchEvent e;
        e.oldChunkX = mChunkX;
        e.oldChunkY = mChunkY;
        e.newChunkX = chunkX;
        e.newChunkY = chunkY;

        EventDispatcher::transmitEvent(EventType::Editor, e);
    }
}
