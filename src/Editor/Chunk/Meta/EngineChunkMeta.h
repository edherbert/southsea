#pragma once

#include <string>

namespace Southsea{

    /**
    A class to encapsulate the chunk meta information used by the engine.
    This is a metadata file which includes information such as whether the terrain should be enabled, etc.
    */
    class EngineChunkMeta{
    public:
        EngineChunkMeta();
        ~EngineChunkMeta();

        bool parseFile(const std::string& path);
        bool writeFile(const std::string& path);

        bool getTerrainEnabled() const { return mTerrainEnabled; }
        void setTerrainEnabled(bool value) { mTerrainEnabled = value; }

    private:
        bool mTerrainEnabled;
    };
}
