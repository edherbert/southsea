#include "EngineChunkMeta.h"

#include "filesystem/path.h"
#include <fstream>
#include <iostream>

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"

namespace Southsea{
    EngineChunkMeta::EngineChunkMeta()
        : mTerrainEnabled(true) {

    }

    EngineChunkMeta::~EngineChunkMeta(){

    }

    bool EngineChunkMeta::parseFile(const std::string& path){
        const filesystem::path filePath(path);
        if(!filePath.exists() || !filePath.is_file()) return false;


        FILE* fp = fopen(path.c_str(), "r");

        {
            using namespace rapidjson;

            char readBuffer[65536];
            FileReadStream is(fp, readBuffer, sizeof(readBuffer));

            Document doc;
            doc.ParseStream(is);

            if(!doc.IsObject()) return false; //Error reading the file.

            auto it = doc.FindMember("terrainEnabled");
            if(it != doc.MemberEnd()){
                if(it->value.IsBool()){
                    bool value = it->value.GetBool();
                    mTerrainEnabled = value;
                }
            }
        }

        return true;
    }

    bool EngineChunkMeta::writeFile(const std::string& path){
        std::cout << "Writing engine meta file." << '\n';

        rapidjson::Document document;

        document.SetObject();

        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        document.AddMember("terrainEnabled", mTerrainEnabled, allocator);

        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);

        std::cout << buffer.GetString() << std::endl;

        std::ofstream myfile;
        myfile.open(path);
        myfile << buffer.GetString();
        myfile.close();

        return true;
    }
}
