#include "DatablockJsonWriter.h"

#include "terra/Hlms/OgreHlmsJsonTerra.h"

#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"

#include "TerrainDatablockInfoManager.h"

#include <fstream>

#include <iostream>

namespace Southsea{
    void DatablockJsonWriter::writeDatablock(const Ogre::String& outputDirectoryPath, Ogre::HlmsDatablock* db){
        assert( dynamic_cast<Ogre::HlmsTerraDatablock*>(db) );
        Ogre::HlmsTerraDatablock *terraDatablock = static_cast<Ogre::HlmsTerraDatablock*>(db);

        const Ogre::String* nameStr = db->getNameStr();
        const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*nameStr);

        std::cout << "Writing json" << '\n';
        {
            // document is the root of a json message
            rapidjson::Document document;

            // define the document as an object rather than an array
            document.SetObject();

            // must pass an allocator when the object may need to allocate memory
            rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

            rapidjson::Value TerraObj(rapidjson::kObjectType);
            {
                rapidjson::Value datablockObj(rapidjson::kObjectType);
                { //Diffuse
                    rapidjson::Value diffuseObj(rapidjson::kObjectType);

                    rapidjson::Value array(rapidjson::kArrayType);
                    const Ogre::Vector3& diff = data.diffuse;
                    array.PushBack(diff.x, allocator).PushBack(diff.y, allocator).PushBack(diff.z, allocator);
                    diffuseObj.AddMember("value", array, allocator);

                    datablockObj.AddMember("diffuse", diffuseObj, allocator);
                }

                { //Detail Weight
                    rapidjson::Value weightObj(rapidjson::kObjectType);

                    const std::string detailNameStr = *nameStr + "detail.png";
                    rapidjson::Value nameStr(detailNameStr.c_str(), allocator);
                    weightObj.AddMember("texture", nameStr, allocator);
                    weightObj.AddMember("sampler", "unique_name", allocator);

                    datablockObj.AddMember("detail_weight", weightObj, allocator);
                }

                { //Detail layers

                    for(int i = 0; i < 4; i++){

                        const TerrainDatablockLayerData& layerData = data.layers[i];

                        if(!layerData.enabled) continue;
                        if(layerData == TerrainDatablockLayerData::DEFAULT_LAYER) continue;

                        const Ogre::String detailName = "detail" + std::to_string(i);
                        rapidjson::Value detailObj(rapidjson::kObjectType);
                        rapidjson::Value nameStr(detailName.c_str(), allocator);

                        const Ogre::Vector4& offsetScale = layerData.offsetScale;
                        { //Offset

                            rapidjson::Value offsetArray(rapidjson::kArrayType);
                            offsetArray.PushBack(offsetScale.x, allocator).PushBack(offsetScale.y, allocator);
                            detailObj.AddMember("offset", offsetArray, allocator);
                        }
                        { //Scale

                            rapidjson::Value scaleArray(rapidjson::kArrayType);
                            scaleArray.PushBack(offsetScale.z, allocator).PushBack(offsetScale.w, allocator);
                            detailObj.AddMember("scale", scaleArray, allocator);
                        }


                        detailObj.AddMember("roughness", layerData.roughness, allocator);
                        detailObj.AddMember("metalness", layerData.metalness, allocator);

                        const Ogre::String& texStr = layerData.diffuseTexture;

                        if(!texStr.empty()){
                            rapidjson::Value texStrValue(texStr.c_str(), allocator);
                            detailObj.AddMember("diffuse_map", texStrValue, allocator);
                        }


                        datablockObj.AddMember(nameStr, detailObj, allocator);
                    }

                }

                rapidjson::Value blockNameStr(terraDatablock->getNameStr()->c_str(), allocator);
                TerraObj.AddMember(blockNameStr, datablockObj, allocator);
            }
            document.AddMember("Terra", TerraObj, allocator);


            // 3. Stringify the DOM
            rapidjson::StringBuffer buffer;
            rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
            document.Accept(writer);

            // Output {"project":"rapidjson","stars":11}
            std::cout << buffer.GetString() << std::endl;

            std::ofstream myfile;
            myfile.open(outputDirectoryPath + "/datablock.material.json");
            myfile << buffer.GetString();
            myfile.close();
        }
    }
}
