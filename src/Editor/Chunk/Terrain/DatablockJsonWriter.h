#pragma once

#include "OgreString.h"

namespace Ogre{
    class HlmsTerraDatablock;
}

namespace Southsea{

    /**
    Class to manage writing terra datablocks to the disk as a json file.

    @remarks
    I didn't want to use the terra json datablock class purely because I want to keep my custom stuff as separate as possible.
    It's easier if I write my own because then I have more control over it anyway.
    */
    class DatablockJsonWriter{
    public:
        DatablockJsonWriter() = delete;
        ~DatablockJsonWriter() = delete;

        /**
        Write the json datablock to disk.
        The file will be named datablock.material.json.
        */
        static void writeDatablock(const Ogre::String& outputDirectoryPath, Ogre::HlmsDatablock* db);
    };
}
