#pragma once

namespace Ogre{
    class HlmsTerraDatablock;
}

#include "OgreString.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraPrerequisites.h"

namespace Southsea{
    class TerrainDatablockLayerData;

    /**
    A simple class to provide utility functions for altering terrain datablocks.
    */
    class TerrainDatablockUtils{
    public:
        TerrainDatablockUtils() = delete;
        ~TerrainDatablockUtils() = delete;

        /**
        Alter a texture layer without creating an action.
        If an invalid texture string was provided the default texture (coloured checker texture) for that layer will be used.
        This function updates the TerrainDatablockStringManager with the new value.
        */
        static void setTextureToLayer(Ogre::HlmsTerraDatablock* db, const Ogre::String& textureName, int layer);

        //Reset a single layer to the default values.
        static void resetDatablockLayer(Ogre::HlmsTerraDatablock* db, int layer);

        static Ogre::TexturePtr getDefaultTextureForLayer(int layer);

        static void setDatablockLayer(Ogre::HlmsTerraDatablock* db, int layer, const TerrainDatablockLayerData& data);

    private:
        static const Ogre::TerraTextureTypes _terraTexTypes[4];
    };
}
