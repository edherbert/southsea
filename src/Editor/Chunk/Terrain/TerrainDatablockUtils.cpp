#include "TerrainDatablockUtils.h"

#include "OgreTextureManager.h"
#include "OgreRoot.h"
#include "OgreHlmsManager.h"
#include "OgreHlms.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraDatablock.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerra.h"

#include "Editor/Chunk/Terrain/TerrainDatablockInfoManager.h"

namespace Southsea{
    const Ogre::TerraTextureTypes TerrainDatablockUtils::_terraTexTypes[4] = {Ogre::TERRA_DETAIL0, Ogre::TERRA_DETAIL1, Ogre::TERRA_DETAIL2, Ogre::TERRA_DETAIL3};

    void TerrainDatablockUtils::setTextureToLayer(Ogre::HlmsTerraDatablock* db, const Ogre::String& textureName, int layer){
        bool textureValid = true;
        if(textureName.size() <= 0) textureValid = false;

        //TODO at some point do a check of whether the resource is actually a texture.
        if(!Ogre::ResourceGroupManager::getSingleton().resourceExistsInAnyGroup(textureName)){
            textureValid = false;
        }

        Ogre::String targetValue = textureName;
        Ogre::TexturePtr targetTex;
        Ogre::uint16 sliceId = 0;
        if(textureValid){
            Ogre::HlmsTextureManager::TextureLocation loc = Ogre::Root::getSingletonPtr()->getHlmsManager()->getTextureManager()->createOrRetrieveTexture(textureName, Ogre::HlmsTextureManager::TEXTURE_TYPE_DETAIL);

            sliceId = loc.xIdx;
            targetTex = loc.texture;
        }else{
            targetTex = getDefaultTextureForLayer(layer);
            targetValue = ""; //The blank textures are id'd as just an empty string.
        }

        db->setTexture(_terraTexTypes[layer], sliceId, targetTex);
        TerrainDatablockInfoManager::setDiffuseTexture(*db->getNameStr(), textureName, layer);
    }

    Ogre::TexturePtr TerrainDatablockUtils::getDefaultTextureForLayer(int layer){
        assert(layer >= 0 && layer < 4);
        return Ogre::TextureManager::getSingleton().getByName("terraDefaultTex" + std::to_string(layer));
    }

    void TerrainDatablockUtils::resetDatablockLayer(Ogre::HlmsTerraDatablock* db, int layer){

        db->setDetailMapOffsetScale(layer, Ogre::Vector4(0, 0, 1, 1));
        db->setTexture(_terraTexTypes[layer], 0, getDefaultTextureForLayer(layer));
        db->setRoughness(layer, 1.0f);
        db->setMetalness(layer, 1.0f);

        // This shouldn't be setting the value to an empty string. Really this is just a utility function to reset values. It shouldn't do anything else.
        //TerrainDatablockStringManager::notifyDatablockTextureName(*db->getNameStr(), "", layer);
        //TerrainDatablockInfoManager::setDiffuseTexture(*currentTerrainDatablock->getNameStr(), targetValue, layer);
    }

    void TerrainDatablockUtils::setDatablockLayer(Ogre::HlmsTerraDatablock* db, int layer, const TerrainDatablockLayerData& data){
        db->setDetailMapOffsetScale(layer, data.offsetScale);
        db->setRoughness(layer, data.roughness);
        db->setMetalness(layer, data.metalness);

        setTextureToLayer(db, data.diffuseTexture, layer);
    }
}
