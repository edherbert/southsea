
#ifndef _OgreTerra_H_
#define _OgreTerra_H_

#include "OgrePrerequisites.h"
#include "OgreMovableObject.h"
#include "OgreShaderParams.h"

#include "TerrainCell.h"

namespace Southsea{
    class TerrainMouseAction;
    class TerrainHeightMouseAction;
    class TerrainBlendMouseAction;
    class LevelTerrainAction;
}

namespace Ogre
{
    struct GridPoint
    {
        int32 x;
        int32 z;
    };

    struct GridDirection
    {
        int x;
        int z;
    };

    class ShadowMapper;
    class CompositorWorkspace;
    class Camera;

    class Terra : public MovableObject
    {
        friend class TerrainCell;

        std::vector<float>          m_heightMap;
        uint32                      m_width;
        uint32                      m_depth; //PNG's Height
        float                       m_depthWidthRatio;
        float                       m_skirtSize;
        float                       m_invWidth;
        float                       m_invDepth;

        Vector2     m_xzDimensions;
        Vector2     m_xzInvDimensions;
        Vector2     m_xzRelativeSize; // m_xzDimensions / [m_width, m_height]
        float       m_height;
        Vector3     m_terrainOrigin;
        uint32      m_basePixelDimension;
        int mTerrainId;

        std::vector<TerrainCell>   m_terrainCells;
        std::vector<TerrainCell*>  m_collectedCells[2];
        size_t                     m_currentCell;

        Ogre::TexturePtr    m_heightMapTex;
        Ogre::TexturePtr    m_normalMapTex;
        Ogre::TexturePtr    m_blendWeightTex;

        Vector3             m_prevLightDir;
        ShadowMapper        *m_shadowMapper;

        CompositorWorkspace *mNormalMapperWorkspace;
        Camera* mNormalMapCamera;

        //Ogre stuff
        CompositorManager2      *m_compositorManager;
        Camera                  *m_camera;

        float invMaxValue = 1.0f;

        void destroyHeightmapTexture(void);

        /// Creates the Ogre texture based on the image data.
        /// Called by @see createHeightmap
        void createHeightmapTexture( const Ogre::Image &image, const String &imageName );
        void createHeightmapTexture(float height);

        /// Calls createHeightmapTexture, loads image data to our CPU-side buffers
        void createHeightmap( Image &image, const String &imageName );

        void createHeightmap(float height);

        void createNormalTexture(void);
        void destroyNormalTexture(void);

        inline uint8 _alterBlend(uint8 blend, float diff);

        ///	Automatically calculates the optimum skirt size (no gaps with
        /// lowest overdraw possible).
        ///	This is done by taking the heighest delta between two adjacent
        /// pixels in a 4x4 block.
        ///	This calculation may not be perfect, as the block search should
        /// get bigger for higher LODs.
        void calculateOptimumSkirtSize(void);

        bool isVisible( const GridPoint &gPos, const GridPoint &gSize ) const;

        void addRenderable( const GridPoint &gridPos, const GridPoint &cellSize, uint32 lodLevel );

        void optimizeCellsAndAdd(void);

        void _loadInternal(Image* img, const Vector3 center, const Vector3 &dimensions, const String &imageName, float height = 0.0f);

        void _calculateBoxSpecification(int x, int y, int boxSize, uint32 pitch, uint32 depth, uint32* movAmount, uint32* shouldMoveAmount, int* boxStartX, int* boxWidth, int* boxStartY, int *boxHeight);

        uint32 _wrapTerrainId(uint32 pos) const;
        uint32 _stripTerrainId(uint32 value, int& targetTerrain) const;

    public:
        Terra( IdType id, ObjectMemoryManager *objectMemoryManager, SceneManager *sceneManager,
               uint8 renderQueueId, CompositorManager2 *compositorManager, Camera *camera, int terrainId );
        ~Terra();

        /** Must be called every frame so we can check the camera's position
            (passed in the constructor) and update our visible batches (and LODs)
            We also update the shadow map if the light direction changed.
        @param lightDir
            Light direction for computing the shadow map.
        @param lightEpsilon
            Epsilon to consider how different light must be from previous
            call to recompute the shadow map.
            Interesting values are in the range [0; 2], but any value is accepted.
        @par
            Large epsilons will reduce the frequency in which the light is updated,
            improving performance (e.g. only compute the shadow map when needed)
        @par
            Use an epsilon of <= 0 to force recalculation every frame. This is
            useful to prevent heterogeneity between frames (reduce stutter) if
            you intend to update the light slightly every frame.
        */
        void update( const Vector3 &lightDir, float lightEpsilon=1e-6f );

        void load( const String &texName, const Vector3 center, const Vector3 &dimensions );
        void load( Image &image, const Vector3 center, const Vector3 &dimensions, const String &imageName = BLANKSTRING );
        void load(float height, const Vector3 center, const Vector3 &dimensions);

        /** Gets the interpolated height at the given location.
            If outside the bounds, it leaves the height untouched.
        @param vPos
            [in] XZ position, Y for default height.
            [out] Y height, or default Y (from input) if outside terrain bounds.
        @return
            True if Y component was changed
        */
        bool getHeightAt( Vector3 &vPos ) const;

        /// load must already have been called.
        void setDatablock( HlmsDatablock *datablock );

        //MovableObject overloads
        const String& getMovableType(void) const;

        Camera* getCamera() const                       { return m_camera; }
        void setCamera( Camera *camera )                { m_camera = camera; }

        const ShadowMapper* getShadowMapper(void) const { return m_shadowMapper; }

        Ogre::TexturePtr getHeightMapTex(void) const    { return m_heightMapTex; }
        Ogre::TexturePtr getNormalMapTex(void) const    { return m_normalMapTex; }
        Ogre::TexturePtr _getShadowMapTex(void) const;

        const Vector2& getXZDimensions(void) const      { return m_xzDimensions; }
        const Vector2& getXZInvDimensions(void) const   { return m_xzInvDimensions; }
        float getHeight(void) const                     { return m_height; }
        const Vector3& getTerrainOrigin(void) const     { return m_terrainOrigin; }

        inline GridPoint worldToGrid( const Vector3 &vPos ) const{
            GridPoint retVal;
            const float fWidth = static_cast<float>( m_width );
            const float fDepth = static_cast<float>( m_depth );

            const float fX = floorf( ((vPos.x - m_terrainOrigin.x) * m_xzInvDimensions.x) * fWidth );
            const float fZ = floorf( ((vPos.z - m_terrainOrigin.z) * m_xzInvDimensions.y) * fDepth );
            retVal.x = fX >= 0.0f ? static_cast<uint32>( fX ) : 0xffffffff;
            retVal.z = fZ >= 0.0f ? static_cast<uint32>( fZ ) : 0xffffffff;

            return retVal;
        }
        inline GridPoint worldToGridWithNegative( const Vector3 &vPos ) const{
            GridPoint retVal;
            const float fWidth = static_cast<float>( m_width );
            const float fDepth = static_cast<float>( m_depth );

            const float fX = floorf( ((vPos.x - m_terrainOrigin.x) * m_xzInvDimensions.x) * fWidth );
            const float fZ = floorf( ((vPos.z - m_terrainOrigin.z) * m_xzInvDimensions.y) * fDepth );
            retVal.x = static_cast<uint32>( fX );
            retVal.z = static_cast<uint32>( fZ );

            return retVal;
        }
        inline Vector2 gridToWorld( const GridPoint &gPos ) const{
            Vector2 retVal;
            const float fWidth = static_cast<float>( m_width );
            const float fDepth = static_cast<float>( m_depth );

            retVal.x = (gPos.x / fWidth) * m_xzDimensions.x + m_terrainOrigin.x;
            retVal.y = (gPos.z / fDepth) * m_xzDimensions.y + m_terrainOrigin.z;

            return retVal;
        }

        /**
        Set the height of the terrain to be a single value.
        */
        void levelTerrain(float height);

        void applyHeightDiff(int x, int y, const std::vector<float>& data, int boxSize, float value, Southsea::TerrainMouseAction* action);
        void applyBlendDiff(int x, int y, const std::vector<float>& data, int boxSize, float value, int blendLayer, Southsea::TerrainMouseAction* action);
        void applySmoothDiff(int x, int y, const std::vector<float>& data, int boxSize, float strength, Southsea::TerrainMouseAction* action);
        void applySetHeight(int x, int y, float height, int boxSize, Southsea::TerrainMouseAction* action);

        void applyHeightMouseAction(Southsea::TerrainMouseAction* action, bool performAction);
        void applyBlendMouseAction(Southsea::TerrainBlendMouseAction* action, bool performAction);

        void fillLevelTerrainAction(Southsea::LevelTerrainAction* action);

        void levelToData(const std::vector<Ogre::uint16>& data);

        void saveTextures(const std::string& path, const Ogre::String& namePrefix);

        /**
        Update the textures related to the height map (such as the generated normal map).
        This should be called each time the heightmap is changed.
        */
        void updateHeightTextures();

        /**
        Check ray collisions against the window. Taken almost verbatum from ogre 1.x's terrain.
        */
        std::pair<bool, Vector3> checkRayIntersect(Ogre::Ray ray);
        std::pair<bool, Vector3> checkQuadIntersection(int x, int z, const Ray& ray);
        Real getHeightData(int x, int y);

        inline float valToHeight(uint16 height){
            return (height * invMaxValue) * m_height;
        }
        inline uint16 heightToVal(float height){
            return (height / invMaxValue) / m_height;
        }

        void writeVertexDataToBuffer(float* bufferStart) const;
        void writeTriangleDataToBuffer(int offset, int* bufferStart) const;
        int getNumTriangles() const;
        int getNumVertices() const;

    };
}

#endif
