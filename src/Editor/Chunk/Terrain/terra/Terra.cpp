
#include "Terra.h"
#include "TerraShadowMapper.h"

#include "OgreImage.h"
#include "OgreTextureManager.h"
#include "OgreHardwarePixelBuffer.h"

#include "OgreCamera.h"
#include "OgreSceneManager.h"
#include "Compositor/OgreCompositorManager2.h"
#include "Compositor/OgreCompositorWorkspace.h"
#include "Compositor/OgreCompositorChannel.h"
#include "OgreMaterialManager.h"
#include "OgreTechnique.h"
#include "OgreRenderTexture.h"

#include "Compositor/OgreCompositorNodeDef.h"
#include "Compositor/Pass/PassQuad/OgreCompositorPassQuadDef.h"

#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainBlendMouseAction.h"
#include "Editor/Action/Actions/Terrain/LevelTerrainAction.h"

#include "Hlms/OgreHlmsJsonTerra.h"

#include <iostream>

namespace Ogre
{
    Terra::Terra( IdType id, ObjectMemoryManager *objectMemoryManager,
                  SceneManager *sceneManager, uint8 renderQueueId,
                  CompositorManager2 *compositorManager, Camera *camera, int terrainId ) :
        MovableObject( id, objectMemoryManager, sceneManager, renderQueueId ),
        m_width( 0u ),
        m_depth( 0u ),
        m_depthWidthRatio( 1.0f ),
        m_skirtSize( 10.0f ),
        m_invWidth( 1.0f ),
        m_invDepth( 1.0f ),
        m_xzDimensions( Vector2::UNIT_SCALE ),
        m_xzInvDimensions( Vector2::UNIT_SCALE ),
        m_xzRelativeSize( Vector2::UNIT_SCALE ),
        m_height( 1.0f ),
        m_terrainOrigin( Vector3::ZERO ),
        m_basePixelDimension( 256u ),
        m_currentCell( 0u ),
        m_prevLightDir( Vector3::ZERO ),
        m_shadowMapper( 0 ),
        m_compositorManager( compositorManager ),
        m_camera( camera ),
        mTerrainId( terrainId )
    {
    }
    //-----------------------------------------------------------------------------------
    Terra::~Terra()
    {
        if( m_shadowMapper )
        {
            m_shadowMapper->destroyShadowMap();
            delete m_shadowMapper;
            m_shadowMapper = 0;
        }
        destroyNormalTexture();
        destroyHeightmapTexture();
        m_terrainCells.clear();
    }
    //-----------------------------------------------------------------------------------
    void Terra::destroyHeightmapTexture(void)
    {
        if( !m_heightMapTex.isNull() )
        {
            ResourcePtr resPtr = m_heightMapTex;
            TextureManager::getSingleton().remove( resPtr );
            m_heightMapTex.setNull();
        }
    }
    //-----------------------------------------------------------------------------------
    void Terra::createHeightmapTexture( const Ogre::Image &image, const String &imageName )
    {
        destroyHeightmapTexture();

        if( image.getBPP() != 8 && image.getBPP() != 16 && image.getFormat() != PF_FLOAT32_R )
        {
            OGRE_EXCEPT( Exception::ERR_INVALIDPARAMS,
                         "Texture " + imageName + "must be 8 bpp, 16 bpp, or 32-bit Float",
                         "Terra::createHeightmapTexture" );
        }

        //const uint8 numMipmaps = image.getNumMipmaps();
        const uint8 numMipmaps = 0u;

        m_heightMapTex = TextureManager::getSingleton().createManual(
                    "HeightMapTex" + StringConverter::toString( getId() ),
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    TEX_TYPE_2D, (uint)image.getWidth(), (uint)image.getHeight(),
                    numMipmaps, image.getFormat(), TU_STATIC_WRITE_ONLY );

        for( uint8 mip=0; mip<=numMipmaps; ++mip )
        {
            v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, mip);
            const PixelBox &currImage = pixelBufferBuf->lock( Box( 0, 0,
                                                                   pixelBufferBuf->getWidth(),
                                                                   pixelBufferBuf->getHeight() ),
                                                              v1::HardwareBuffer::HBL_DISCARD );
            PixelUtil::bulkPixelConversion( image.getPixelBox(0, mip), currImage );
            pixelBufferBuf->unlock();
        }
    }
    void Terra::createHeightmapTexture(float height){
        destroyHeightmapTexture();
        const uint8 numMipmaps = 0u;

        m_heightMapTex = TextureManager::getSingleton().createManual(
                    "HeightMapTex" + StringConverter::toString( getId() ),
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    TEX_TYPE_2D, (uint)m_width, (uint)m_depth,
                    numMipmaps, Ogre::PF_L16, TU_STATIC_WRITE_ONLY );

        const uint16 targetVal = heightToVal(height);

        for( uint8 mip=0; mip<=numMipmaps; ++mip )
        {
            v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, mip);
            const PixelBox &pixBox = pixelBufferBuf->lock( Box( 0, 0,
                                                                   pixelBufferBuf->getWidth(),
                                                                   pixelBufferBuf->getHeight() ),
                                                              v1::HardwareBuffer::HBL_DISCARD );

            Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

            for(int y = 0; y < m_heightMapTex->getHeight(); y++){
              for(int x = 0; x < m_heightMapTex->getWidth(); x++){
                  *pDest++ = targetVal;
              }
            }

            pixelBufferBuf->unlock();
        }
    }
    void Terra::createHeightmap(float height){
        //I'm using this as the default size right now.
        m_width = 1024;
        m_depth = 1024;
        m_depthWidthRatio = m_depth / (float)(m_width);
        m_invWidth = 1.0f / m_width;
        m_invDepth = 1.0f / m_depth;

        const uchar bpp = 16; //Possible placeholder
        const float maxValue = powf( 2.0f, (float)bpp) - 1.0f;
        invMaxValue = 1.0f / maxValue;

        createHeightmapTexture(height);

        m_heightMap = std::vector<float>(m_width * m_depth, height);


        m_xzRelativeSize = m_xzDimensions / Vector2( static_cast<Real>(m_width),
                                                     static_cast<Real>(m_depth) );

        createNormalTexture();

        m_prevLightDir = Vector3::ZERO;

        delete m_shadowMapper;
        m_shadowMapper = new ShadowMapper( mManager, m_compositorManager );
        m_shadowMapper->createShadowMap( getId(), m_heightMapTex );

        calculateOptimumSkirtSize();
    }
    //-----------------------------------------------------------------------------------
    void Terra::createHeightmap( Image &image, const String &imageName )
    {
        m_width = image.getWidth();
        m_depth = image.getHeight();
        m_depthWidthRatio = m_depth / (float)(m_width);
        m_invWidth = 1.0f / m_width;
        m_invDepth = 1.0f / m_depth;

        if( PixelUtil::getComponentCount( image.getFormat() ) != 1 )
        {
            OGRE_EXCEPT( Exception::ERR_INVALIDPARAMS,
                         "Only grayscale images supported! " + imageName,
                         "Terra::createHeightmap" );
        }

        //image.generateMipmaps( false, Image::FILTER_NEAREST );

        createHeightmapTexture( image, imageName );

        m_heightMap.resize( m_width * m_depth );

        const float maxValue = powf( 2.0f, (float)image.getBPP() ) - 1.0f;
        //const float invMaxValue = 1.0f / maxValue;
        invMaxValue = 1.0f / maxValue;

        if( image.getBPP() == 8 )
        {
            const uint8 * RESTRICT_ALIAS data = reinterpret_cast<uint8*RESTRICT_ALIAS>(image.getData());
            for( uint32 y=0; y<m_depth; ++y )
            {
                for( uint32 x=0; x<m_width; ++x )
                    m_heightMap[y * m_width + x] = (data[y * m_width + x] * invMaxValue) * m_height;
            }
        }
        else if( image.getBPP() == 16 )
        {
            const uint16 * RESTRICT_ALIAS data = reinterpret_cast<uint16*RESTRICT_ALIAS>(
                                                                                        image.getData());
            for( uint32 y=0; y<m_depth; ++y )
            {
                for( uint32 x=0; x<m_width; ++x )
                    m_heightMap[y * m_width + x] = (data[y * m_width + x] * invMaxValue) * m_height;
            }
        }
        else if( image.getFormat() == PF_FLOAT32_R )
        {
            const float * RESTRICT_ALIAS data = reinterpret_cast<float*RESTRICT_ALIAS>(image.getData());
            for( uint32 y=0; y<m_depth; ++y )
            {
                for( uint32 x=0; x<m_width; ++x )
                    m_heightMap[y * m_width + x] = data[y * m_width + x] * m_height;
            }
        }

        m_xzRelativeSize = m_xzDimensions / Vector2( static_cast<Real>(m_width),
                                                     static_cast<Real>(m_depth) );

        createNormalTexture();

        m_prevLightDir = Vector3::ZERO;

        delete m_shadowMapper;
        m_shadowMapper = new ShadowMapper( mManager, m_compositorManager );
        m_shadowMapper->createShadowMap( getId(), m_heightMapTex );

        calculateOptimumSkirtSize();
    }
    //-----------------------------------------------------------------------------------
    void Terra::createNormalTexture(void)
    {
        destroyNormalTexture();

        m_normalMapTex = TextureManager::getSingleton().createManual(
                    "NormalMapTex_" + StringConverter::toString( getId() ),
                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                    TEX_TYPE_2D, m_heightMapTex->getWidth(), m_heightMapTex->getHeight(),
                    PixelUtil::getMaxMipmapCount( m_heightMapTex->getWidth(),
                                                  m_heightMapTex->getHeight() ),
                    PF_A2B10G10R10, TU_RENDERTARGET|TU_AUTOMIPMAP );

        MaterialPtr normalMapperMat = MaterialManager::getSingleton().load(
                    "Terra/GpuNormalMapper",
                    ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME ).
                staticCast<Material>();
        //Clone the material for each terrain.
        std::string materialName = "Terra/GpuNormalMapper" + std::to_string(mTerrainId);
        if(MaterialManager::getSingleton().resourceExists(materialName)){
            MaterialManager::getSingleton().remove(materialName);
        }
        assert(!MaterialManager::getSingleton().resourceExists(materialName));
        MaterialPtr newMaterial = normalMapperMat->clone(materialName);
        Pass *pass = newMaterial->getTechnique(0)->getPass(0);
        TextureUnitState *texUnit = pass->getTextureUnitState(0);
        texUnit->setTexture( m_heightMapTex );

        //Normalize vScale for better precision in the shader math
        const Vector3 vScale = Vector3( m_xzRelativeSize.x, m_height, m_xzRelativeSize.y ).normalisedCopy();

        GpuProgramParametersSharedPtr psParams = pass->getFragmentProgramParameters();
        psParams->setNamedConstant( "heightMapResolution", Vector4( static_cast<Real>( m_width ),
                                                                    static_cast<Real>( m_depth ),
                                                                    1, 1 ) );
        psParams->setNamedConstant( "vScale", vScale );

        CompositorChannelVec finalTargetChannels( 1, CompositorChannel() );
        finalTargetChannels[0].target = m_normalMapTex->getBuffer()->getRenderTarget();
        finalTargetChannels[0].textures.push_back( m_normalMapTex );

        mNormalMapCamera = mManager->createCamera( "NormalMapperDummyCamera" + std::to_string(mTerrainId) );

        {
            Ogre::CompositorNodeDef* nodeDef = m_compositorManager->getNodeDefinitionNonConst("Terra/GpuNormalMapper");
            Ogre::CompositorTargetDef* compTarget = nodeDef->getTargetPass(0);
            Ogre::CompositorPassDef* pass = compTarget->getCompositorPassesNonConst()[0];
            Ogre::CompositorPassQuadDef* quadPass = dynamic_cast<Ogre::CompositorPassQuadDef*>(pass);
            assert(quadPass);
            quadPass->mMaterialName = materialName;
        }

        mNormalMapperWorkspace = m_compositorManager->addWorkspace( mManager, finalTargetChannels, mNormalMapCamera, "Terra/GpuNormalMapperWorkspace", false );

        mNormalMapperWorkspace->_beginUpdate( true );
        mNormalMapperWorkspace->_update();
        mNormalMapperWorkspace->_endUpdate( true );

        //m_compositorManager->removeWorkspace( workspace );
        //mManager->destroyCamera( dummyCamera );
    }
    //-----------------------------------------------------------------------------------
    void Terra::destroyNormalTexture(void)
    {
        if( !m_normalMapTex.isNull() )
        {
            ResourcePtr resPtr = m_normalMapTex;
            TextureManager::getSingleton().remove( resPtr );
            m_normalMapTex.setNull();

            //As the normal mapper workspace now persists we destroy it when destroying the texture.
            m_compositorManager->removeWorkspace(mNormalMapperWorkspace);
            mNormalMapperWorkspace = 0;
            mManager->destroyCamera(mNormalMapCamera);
            mNormalMapCamera = 0;
        }
    }
    //-----------------------------------------------------------------------------------
    void Terra::calculateOptimumSkirtSize(void)
    {
        m_skirtSize = std::numeric_limits<float>::max();

        const uint32 basePixelDimension = m_basePixelDimension;
        const uint32 vertPixelDimension = static_cast<uint32>(m_basePixelDimension * m_depthWidthRatio);

        for( size_t y=vertPixelDimension-1u; y<m_depth-1u; y += vertPixelDimension )
        {
            const size_t ny = y + 1u;

            bool allEqualInLine = true;
            float minHeight = m_heightMap[y * m_width];
            for( size_t x=0; x<m_width; ++x )
            {
                const float minValue = Ogre::min( m_heightMap[y * m_width + x],
                                                  m_heightMap[ny * m_width + x] );
                minHeight = Ogre::min( minValue, minHeight );
                allEqualInLine &= m_heightMap[y * m_width + x] == m_heightMap[ny * m_width + x];
            }

            if( !allEqualInLine )
                m_skirtSize = Ogre::min( minHeight, m_skirtSize );
        }

        for( size_t x=basePixelDimension-1u; x<m_width-1u; x += basePixelDimension )
        {
            const size_t nx = x + 1u;

            bool allEqualInLine = true;
            float minHeight = m_heightMap[x];
            for( size_t y=0; y<m_depth; ++y )
            {
                const float minValue = Ogre::min( m_heightMap[y * m_width + x],
                                                  m_heightMap[y * m_width + nx] );
                minHeight = Ogre::min( minValue, minHeight );
                allEqualInLine &= m_heightMap[y * m_width + x] == m_heightMap[y * m_width + nx];
            }

            if( !allEqualInLine )
                m_skirtSize = Ogre::min( minHeight, m_skirtSize );
        }

        m_skirtSize /= m_height;
        //TODO temporary. This value seemed to work well to avoid artifacts.
        m_skirtSize = 0.000839246;
    }
    //-----------------------------------------------------------------------------------
    bool Terra::isVisible( const GridPoint &gPos, const GridPoint &gSize ) const
    {
        if( gPos.x >= static_cast<int32>( m_width ) ||
            gPos.z >= static_cast<int32>( m_depth ) ||
            gPos.x + gSize.x <= 0 ||
            gPos.z + gSize.z <= 0 )
        {
            //Outside terrain bounds.
            return false;
        }

//        return true;

        const Vector2 cellPos = gridToWorld( gPos );
        const Vector2 cellSize( (gSize.x + 1u) * m_xzRelativeSize.x,
                                (gSize.z + 1u) * m_xzRelativeSize.y );

        const Vector3 vHalfSize = Vector3( cellSize.x, m_height, cellSize.y ) * 0.5f;
        const Vector3 vCenter = Vector3( cellPos.x, m_terrainOrigin.y, cellPos.y ) + vHalfSize;

        for( int i=0; i<6; ++i )
        {
            //Skip far plane if view frustum is infinite
            if( i == FRUSTUM_PLANE_FAR && m_camera->getFarClipDistance() == 0 )
                continue;

            Plane::Side side = m_camera->getFrustumPlane(i).getSide( vCenter, vHalfSize );

            //We only need one negative match to know the obj is outside the frustum
            if( side == Plane::NEGATIVE_SIDE )
                return false;
        }

        return true;
    }
    //-----------------------------------------------------------------------------------
    void Terra::addRenderable( const GridPoint &gridPos, const GridPoint &cellSize, uint32 lodLevel )
    {
        TerrainCell *cell = &m_terrainCells[m_currentCell++];
        cell->setOrigin( gridPos, cellSize.x, cellSize.z, lodLevel );
        m_collectedCells[0].push_back( cell );
    }
    //-----------------------------------------------------------------------------------
    void Terra::optimizeCellsAndAdd(void)
    {
        //Keep iterating until m_collectedCells[0] stops shrinking
        size_t numCollectedCells = std::numeric_limits<size_t>::max();
        while( numCollectedCells != m_collectedCells[0].size() )
        {
            numCollectedCells = m_collectedCells[0].size();

            if( m_collectedCells[0].size() > 1 )
            {
                m_collectedCells[1].clear();

                std::vector<TerrainCell*>::const_iterator itor = m_collectedCells[0].begin();
                std::vector<TerrainCell*>::const_iterator end  = m_collectedCells[0].end();

                while( end - itor >= 2u )
                {
                    TerrainCell *currCell = *itor;
                    TerrainCell *nextCell = *(itor+1);

                    m_collectedCells[1].push_back( currCell );
                    if( currCell->merge( nextCell ) )
                        itor += 2;
                    else
                        ++itor;
                }

                while( itor != end )
                    m_collectedCells[1].push_back( *itor++ );

                m_collectedCells[1].swap( m_collectedCells[0] );
            }
        }

        std::vector<TerrainCell*>::const_iterator itor = m_collectedCells[0].begin();
        std::vector<TerrainCell*>::const_iterator end  = m_collectedCells[0].end();
        while( itor != end )
            mRenderables.push_back( *itor++ );

        m_collectedCells[0].clear();
    }
    //-----------------------------------------------------------------------------------
    void Terra::update( const Vector3 &lightDir, float lightEpsilon )
    {
        const float lightCosAngleChange = Math::Clamp(
                    (float)m_prevLightDir.dotProduct( lightDir.normalisedCopy() ), -1.0f, 1.0f );
        if( lightCosAngleChange <= (1.0f - lightEpsilon) )
        {
            m_shadowMapper->updateShadowMap( lightDir, m_xzDimensions, m_height );
            m_prevLightDir = lightDir.normalisedCopy();
        }

        mRenderables.clear();
        m_currentCell = 0;

        Vector3 camPos = m_camera->getDerivedPosition();

        const uint32 basePixelDimension = m_basePixelDimension;
        const uint32 vertPixelDimension = static_cast<uint32>(m_basePixelDimension * m_depthWidthRatio);

        GridPoint cellSize;
        cellSize.x = basePixelDimension;
        cellSize.z = vertPixelDimension;

        //Quantize the camera position to basePixelDimension steps
        GridPoint camCenter = worldToGrid( camPos );
        camCenter.x = (camCenter.x / basePixelDimension) * basePixelDimension;
        camCenter.z = (camCenter.z / vertPixelDimension) * vertPixelDimension;

        uint32 currentLod = 0;

//        camCenter.x = 64;
//        camCenter.z = 64;

        //LOD 0: Add full 4x4 grid
        for( int32 z=-2; z<2; ++z )
        {
            for( int32 x=-2; x<2; ++x )
            {
                GridPoint pos = camCenter;
                pos.x += x * cellSize.x;
                pos.z += z * cellSize.z;

                if( isVisible( pos, cellSize ) )
                    addRenderable( pos, cellSize, currentLod );
            }
        }

        optimizeCellsAndAdd();

        m_currentCell = 16u; //The first 16 cells don't use skirts.

        const uint32 maxRes = std::max( m_width, m_depth );
        //TODO: When we're too far (outside the terrain), just display a 4x4 grid or something like that.

        size_t numObjectsAdded = std::numeric_limits<size_t>::max();
        //LOD n: Add 4x4 grid, ignore 2x2 center (which
        //is the same as saying the borders of the grid)
        while( numObjectsAdded != m_currentCell ||
               (mRenderables.empty() && (1u << currentLod) <= maxRes) )
        {
            numObjectsAdded = m_currentCell;

            cellSize.x <<= 1u;
            cellSize.z <<= 1u;
            ++currentLod;

            //Row 0
            {
                const int32 z = 1;
                for( int32 x=-2; x<2; ++x )
                {
                    GridPoint pos = camCenter;
                    pos.x += x * cellSize.x;
                    pos.z += z * cellSize.z;

                    if( isVisible( pos, cellSize ) )
                        addRenderable( pos, cellSize, currentLod );
                }
            }
            //Row 3
            {
                const int32 z = -2;
                for( int32 x=-2; x<2; ++x )
                {
                    GridPoint pos = camCenter;
                    pos.x += x * cellSize.x;
                    pos.z += z * cellSize.z;

                    if( isVisible( pos, cellSize ) )
                        addRenderable( pos, cellSize, currentLod );
                }
            }
            //Cells [0, 1] & [0, 2];
            {
                const int32 x = -2;
                for( int32 z=-1; z<1; ++z )
                {
                    GridPoint pos = camCenter;
                    pos.x += x * cellSize.x;
                    pos.z += z * cellSize.z;

                    if( isVisible( pos, cellSize ) )
                        addRenderable( pos, cellSize, currentLod );
                }
            }
            //Cells [3, 1] & [3, 2];
            {
                const int32 x = 1;
                for( int32 z=-1; z<1; ++z )
                {
                    GridPoint pos = camCenter;
                    pos.x += x * cellSize.x;
                    pos.z += z * cellSize.z;

                    if( isVisible( pos, cellSize ) )
                        addRenderable( pos, cellSize, currentLod );
                }
            }

            optimizeCellsAndAdd();
        }
    }
    //-----------------------------------------------------------------------------------
    void Terra::load(float height, const Vector3 center, const Vector3 &dimensions){
        _loadInternal(0, center, dimensions, "", height);
    }
    //-----------------------------------------------------------------------------------
    void Terra::load( const String &texName, const Vector3 center, const Vector3 &dimensions )
    {
        Ogre::Image image;
        image.load( texName, ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );

        load( image, center, dimensions, texName );
    }
    //-----------------------------------------------------------------------------------
    void Terra::load( Image &image, const Vector3 center, const Vector3 &dimensions, const String &imageName )
    {
        _loadInternal(&image, center, dimensions, imageName, 0);
    }
    //-----------------------------------------------------------------------------------
    void Terra::_loadInternal(Image* img, const Vector3 center, const Vector3 &dimensions, const String &imageName, float height){
        m_terrainOrigin = center - dimensions * 0.5f;
        m_xzDimensions = Vector2( dimensions.x, dimensions.z );
        m_xzInvDimensions = 1.0f / m_xzDimensions;
        m_height = dimensions.y;
        m_basePixelDimension = 64u;

        if(img){
            createHeightmap(*img, imageName);
        }else{
            createHeightmap(height); //If an image was not provided it's assumed we're working with the singular height rather than the image.
        }

        {
            //Find out how many TerrainCells we need. I think this might be
            //solved analitically with a power series. But my math is rusty.
            const uint32 basePixelDimension = m_basePixelDimension;
            const uint32 vertPixelDimension = static_cast<uint32>( m_basePixelDimension *
                                                                   m_depthWidthRatio );
            const uint32 maxPixelDimension = std::max( basePixelDimension, vertPixelDimension );
            const uint32 maxRes = std::max( m_width, m_depth );

            uint32 numCells = 16u; //4x4
            uint32 accumDim = 0u;
            uint32 iteration = 1u;
            while( accumDim < maxRes )
            {
                numCells += 12u; //4x4 - 2x2
                accumDim += maxPixelDimension * (1u << iteration);
                ++iteration;
            }

            numCells += 12u;
            accumDim += maxPixelDimension * (1u << iteration);
            ++iteration;

            m_terrainCells.clear();
            m_terrainCells.resize( numCells, TerrainCell( this ) );
        }

        VaoManager *vaoManager = mManager->getDestinationRenderSystem()->getVaoManager();
        std::vector<TerrainCell>::iterator itor = m_terrainCells.begin();
        std::vector<TerrainCell>::iterator end  = m_terrainCells.end();

        const std::vector<TerrainCell>::iterator begin = itor;

        while( itor != end )
        {
            itor->initialize( vaoManager, (itor - begin) >= 16u );
            ++itor;
        }

        m_blendWeightTex = Ogre::TextureManager::getSingleton().getByName("blendMapTarget" + std::to_string(mTerrainId));
    }
    //-----------------------------------------------------------------------------------
    bool Terra::getHeightAt( Vector3 &vPos ) const
    {
        bool retVal = false;
        GridPoint pos2D = worldToGrid( vPos );

        if( pos2D.x < m_width-1 && pos2D.z < m_depth-1 )
        {
            const Vector2 vPos2D = gridToWorld( pos2D );

            const float dx = (vPos.x - vPos2D.x) * m_width * m_xzInvDimensions.x;
            const float dz = (vPos.z - vPos2D.y) * m_depth * m_xzInvDimensions.y;

            float a, b, c;
            const float h00 = m_heightMap[ pos2D.z * m_width + pos2D.x ];
            const float h11 = m_heightMap[ (pos2D.z+1) * m_width + pos2D.x + 1 ];

            c = h00;
            if( dx < dz )
            {
                //Plane eq: y = ax + bz + c
                //x=0 z=0 -> c		= h00
                //x=0 z=1 -> b + c	= h01 -> b = h01 - c
                //x=1 z=1 -> a + b + c  = h11 -> a = h11 - b - c
                const float h01 = m_heightMap[ (pos2D.z+1) * m_width + pos2D.x ];

                b = h01 - c;
                a = h11 - b - c;
            }
            else
            {
                //Plane eq: y = ax + bz + c
                //x=0 z=0 -> c		= h00
                //x=1 z=0 -> a + c	= h10 -> a = h10 - c
                //x=1 z=1 -> a + b + c  = h11 -> b = h11 - a - c
                const float h10 = m_heightMap[ pos2D.z * m_width + pos2D.x + 1 ];

                a = h10 - c;
                b = h11 - a - c;
            }

            vPos.y = a * dx + b * dz + c;
            retVal = true;
        }

        return retVal;
    }
    //-----------------------------------------------------------------------------------
    void Terra::setDatablock( HlmsDatablock *datablock )
    {
        std::vector<TerrainCell>::iterator itor = m_terrainCells.begin();
        std::vector<TerrainCell>::iterator end  = m_terrainCells.end();

        while( itor != end )
        {
            itor->setDatablock( datablock );
            ++itor;
        }

        //The texture is created only once, and named blend map target.
        TexturePtr tex = Ogre::TextureManager::getSingleton().getByName("blendMapTarget" + std::to_string(mTerrainId));
        assert(tex);

        Ogre::HlmsTerraDatablock* terraDb = dynamic_cast<Ogre::HlmsTerraDatablock*>(datablock);
        terraDb->setTexture(Ogre::TerraTextureTypes::TERRA_DETAIL_WEIGHT, 0, tex);
        //Ogre::HlmsMacroblock macroblock;
        //macroblock.mPolygonMode = Ogre::PM_WIREFRAME;
        //terraDb->setMacroblock(macroblock);
    }
    //-----------------------------------------------------------------------------------
    Ogre::TexturePtr Terra::_getShadowMapTex(void) const
    {
        return m_shadowMapper->getShadowMapTex();
    }
    //-----------------------------------------------------------------------------------
    const String& Terra::getMovableType(void) const
    {
        static const String movType = "Terra";
        return movType;
    }




    //Custom stuff

    void Terra::levelTerrain(float height){
        const uint16 trueValue = heightToVal(height);

        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        size_t pitch = pixBox.rowPitch;
        for(int y = 0; y < m_heightMapTex->getHeight(); y++){
            for(int x = 0; x < m_heightMapTex->getWidth(); x++){
                *pDest++ = trueValue;
            }
        }

        pixelBufferBuf->unlock();

        updateHeightTextures();

        for(int i = 0; i < m_heightMap.size(); i++){
            m_heightMap[i] = height;
        }

    }

    void Terra::_calculateBoxSpecification(int x, int y, int boxSize, uint32 pitch, uint32 depth, uint32* movAmount, uint32* shouldMoveAmount, int* boxStartX, int* boxStartY, int* boxWidth, int *boxHeight){
        int drawX = x;
        int drawY = y;

        *boxWidth = boxSize;
        *boxHeight = boxSize;
        *boxStartX = 0;
        *boxStartY = 0;

        if(x < 0){
            drawX = 0;
            *boxStartX = -x;
        }
        if(y < 0){
            drawY = 0;
            *boxStartY = -y;
        }
        if(x + boxSize > int(pitch)){
            int amount = (x + boxSize) - pitch;
            *boxWidth = boxSize - amount;
        }
        if(y + boxSize > int(depth)){
            int amount = (y + boxSize) - depth;
            *boxHeight = boxSize - amount;
        }

        *movAmount = (drawY * pitch) + drawX;
        *shouldMoveAmount = (pitch - (x + *boxWidth)) + drawX;
    }

    void Terra::applyBlendDiff(int x, int y, const std::vector<float>& data, int boxSize, float value, int blendLayer, Southsea::TerrainMouseAction* action){
        assert(data.size() == boxSize * boxSize);
        assert(m_blendWeightTex);

        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_blendWeightTex->getBuffer(0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixBox.data);

        uint32 movAmount, shouldMoveAmount;
        int boxStartX, boxStartY, boxWidth, boxHeight;
        _calculateBoxSpecification(x, y, boxSize, pixBox.getWidth(), pixBox.getHeight(), &movAmount, &shouldMoveAmount, &boxStartX, &boxStartY, &boxWidth, &boxHeight);

        //TODO find some way to get rid of all the hard coded * 4s.
        //Align up the pointer to the start of the box.
        movAmount *= 4;
        pDest += movAmount;

        Ogre::Vector4 vals(-1, -1, -1, -1);
        // if(blendLayer == 0) vals.w = -vals.w;
        // if(blendLayer == 1) vals.z = -vals.z;
        // if(blendLayer == 2) vals.y = -vals.y;
        // if(blendLayer == 3) vals.x = -vals.x;
        if(blendLayer == 0) vals.y = -vals.y;
        if(blendLayer == 1) vals.z = -vals.z;
        if(blendLayer == 2) vals.w = -vals.w;
        if(blendLayer == 3) vals.x = -vals.x;
        vals *= value;

        for(int yy = boxStartY; yy < boxHeight; yy++){
            for(int xx = boxStartX; xx < boxWidth; xx++){
                const float brushVal = data[xx + yy * boxSize];
                const uint32* startPos = (uint32*)pDest;
                const uint32 original = *startPos;

                *pDest++ = _alterBlend(*pDest, vals.w * brushVal);
                *pDest++ = _alterBlend(*pDest, vals.z * brushVal);
                *pDest++ = _alterBlend(*pDest, vals.y * brushVal);
                *pDest++ = _alterBlend(*pDest, vals.x * brushVal);

                action->addData( _wrapTerrainId(movAmount / 4), *startPos, original);
                movAmount+=4;
            }
            movAmount += shouldMoveAmount * 4; //Align to the start of the next line.
            pDest += shouldMoveAmount * 4;
        }

        pixelBufferBuf->unlock();
    }

    uint8 Terra::_alterBlend(uint8 blend, float diff){
        uint8 retVal = blend;

        retVal += diff;

        if(diff < 0 && retVal > blend){ //If the value has rolled over.
            retVal = 0;
        }
        if(diff > 0 && retVal < blend){
            retVal = 255;
        }

        return retVal;
    }

    void Terra::applyHeightDiff(int x, int y, const std::vector<float>& data, int boxSize, float value, Southsea::TerrainMouseAction* action){
        assert(data.size() == boxSize * boxSize);
        const uint16 heightVal = heightToVal(m_height);


        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        uint32 movAmount, shouldMoveAmount;
        int boxStartX, boxStartY, boxWidth, boxHeight;
        _calculateBoxSpecification(x, y, boxSize, pixBox.getWidth(), pixBox.getHeight(), &movAmount, &shouldMoveAmount, &boxStartX, &boxStartY, &boxWidth, &boxHeight);

        //Align up the pointer to the start of the box.
        pDest += movAmount;

        for(int yy = boxStartY; yy < boxHeight; yy++){
            for(int xx = boxStartX; xx < boxWidth; xx++){
                uint16 oldValue = *pDest;
                uint16 determinedValue = oldValue + data[xx + yy * boxSize] * value;
                if(value < 0 && determinedValue > oldValue){ //If value is 0 (we're lowering the terrain), the value should never be greater than what it previously was (possible with integer wrap-arounds).
                    determinedValue = 0;
                }
                else if(value > 0 && determinedValue < oldValue){
                    determinedValue = heightVal;
                }
                *pDest++ = determinedValue;
                m_heightMap[movAmount] = valToHeight(determinedValue);

                action->addData(_wrapTerrainId(movAmount), determinedValue, oldValue);
                movAmount++;
            }
            movAmount += shouldMoveAmount; //Align to the start of the next line.
            pDest += shouldMoveAmount;
        }

        pixelBufferBuf->unlock();

        updateHeightTextures();
    }

    void Terra::applySetHeight(int x, int y, float height, int boxSize, Southsea::TerrainMouseAction* action){
        const uint16 heightVal = heightToVal(m_height);

        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        uint32 movAmount, shouldMoveAmount;
        int boxStartX, boxStartY, boxWidth, boxHeight;
        _calculateBoxSpecification(x, y, boxSize, pixBox.getWidth(), pixBox.getHeight(), &movAmount, &shouldMoveAmount, &boxStartX, &boxStartY, &boxWidth, &boxHeight);

        //Align up the pointer to the start of the box.
        pDest += movAmount;

        for(int yy = boxStartY; yy < boxHeight; yy++){
            for(int xx = boxStartX; xx < boxWidth; xx++){
                uint16 oldValue = *pDest;
                uint16 newValue = heightToVal(height);
                *pDest++ = newValue;
                m_heightMap[movAmount] = height;

                action->addData(_wrapTerrainId(movAmount), newValue, oldValue);
                movAmount++;
            }
            movAmount += shouldMoveAmount; //Align to the start of the next line.
            pDest += shouldMoveAmount;
        }

        pixelBufferBuf->unlock();

        updateHeightTextures();
    }

    void Terra::applySmoothDiff(int x, int y, const std::vector<float>& data, int boxSize, float strength, Southsea::TerrainMouseAction* action){
        assert(data.size() == boxSize * boxSize);
        const uint16 heightVal = heightToVal(m_height);
        strength = strength / 100;

        int targetStartX = x;
        int targetStartY = y;
        if(x < 0) targetStartX = 0;
        if(y < 0) targetStartY = 0;

        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        uint32 movAmount, shouldMoveAmount;
        int boxStartX, boxStartY, boxWidth, boxHeight;
        _calculateBoxSpecification(x, y, boxSize, pixBox.getWidth(), pixBox.getHeight(), &movAmount, &shouldMoveAmount, &boxStartX, &boxStartY, &boxWidth, &boxHeight);

        //Align up the pointer to the start of the box.
        pDest += movAmount;

        int xVal = targetStartX;
        int yVal = targetStartY;
        for(int yy = boxStartY; yy < boxHeight; yy++){
            for(int xx = boxStartX; xx < boxWidth; xx++){
                uint16 oldValue = *pDest;
                assert(xVal >= 0);
                const float prevCurrentHeight = m_heightMap[xVal + yVal * pixBox.getHeight()];
                float currentHeight = prevCurrentHeight;

                float determinedValue = 0.0f;
                uint8 totalAmount = 0;
                //Find the average around this point.
                for(int ya = yVal - 1; ya <= yVal + 1; ya++){
                    if(ya < 0 || ya >= pixBox.getHeight()) continue;
                    for(int xa = xVal - 1; xa <= xVal + 1; xa++){
                        if(xa < 0 || xa >= pixBox.getWidth()) continue;
                        determinedValue += m_heightMap[xa + ya * pixBox.getHeight()];
                        totalAmount++;
                    }
                }
                if(totalAmount == 0){
                    currentHeight = prevCurrentHeight;
                }else{
                    determinedValue /= totalAmount;
                    float diffVal = (determinedValue - currentHeight) * strength;
                    currentHeight += diffVal;
                }

                Ogre::uint16 heightDest = heightToVal(currentHeight);
                *pDest++ = heightDest;
                m_heightMap[movAmount] = currentHeight;

                action->addData(_wrapTerrainId(movAmount), heightDest, oldValue);
                movAmount++;
                xVal++;
            }
            movAmount += shouldMoveAmount; //Align to the start of the next line.
            pDest += shouldMoveAmount;
            yVal++;
            xVal = targetStartX;
        }

        pixelBufferBuf->unlock();

        updateHeightTextures();
    }

    std::pair<bool, Vector3> Terra::checkRayIntersect(Ogre::Ray ray){
        typedef std::pair<bool, Vector3> Result;

        assert(m_width == m_depth); //Just to make sure the terrain has consistant vertex resolution.

        Vector3 rayOrigin = ray.getOrigin() - m_terrainOrigin;

        Vector3 rayDirection = ray.getDirection();
        const float scale = m_xzDimensions.x / (Real)(m_width);

        rayOrigin.x /= scale;
        rayOrigin.z /= scale;
        rayDirection.x /= scale;
        rayDirection.z /= scale;
        rayDirection.normalise();

        Ray localRay(rayOrigin, rayDirection);

        Real maxHeight = m_height;
        Real minHeight = 0;

        AxisAlignedBox aabb (Vector3(0, minHeight, 0), Vector3(m_width, maxHeight, m_width));
        std::pair<bool, Real> aabbTest = localRay.intersects(aabb);

        if (!aabbTest.first){
            return Result(false, Vector3());
        }

        Vector3 cur = localRay.getPoint(aabbTest.second);

        int quadX = std::min(std::max(static_cast<int>(cur.x), 0), (int)m_width-2);
        int quadZ = std::min(std::max(static_cast<int>(cur.z), 0), (int)m_width-2);
        int flipX = (rayDirection.x < 0 ? 0 : 1);
        int flipZ = (rayDirection.z < 0 ? 0 : 1);
        int xDir = (rayDirection.x < 0 ? -1 : 1);
        int zDir = (rayDirection.z < 0 ? -1 : 1);

        Result result(true, Vector3::ZERO);
        Real dummyHighValue = (Real)m_width * 10000.0f;

        while (cur.y >= (minHeight - 1e-3) && cur.y <= (maxHeight + 1e-3)){
            if (quadX < 0 || quadX >= (int)m_width-1 || quadZ < 0 || quadZ >= (int)m_depth-1){
                break;
            }

            result = checkQuadIntersection(quadX, quadZ, localRay);
            if (result.first) break;

            // determine next quad to test
            Real xDist = Math::RealEqual(rayDirection.x, 0.0) ? dummyHighValue :
                (quadX - cur.x + flipX) / rayDirection.x;
            Real zDist = Math::RealEqual(rayDirection.z, 0.0) ? dummyHighValue :
                (quadZ - cur.z + flipZ) / rayDirection.z;
            if (xDist < zDist)
            {
                quadX += xDir;
                cur += rayDirection * xDist;
            }
            else
            {
                quadZ += zDir;
                cur += rayDirection * zDist;
            }
        }

        if(result.first){
            result.second.x *= scale;
            result.second.z *= scale;
            result.second += m_terrainOrigin;
        }

        return result;
    }

    Real Terra::getHeightData(int x, int y){
        assert (x >= 0 && x < m_width && y >= 0 && y < m_depth);
        return m_heightMap[y * m_width + x];
    }

    std::pair<bool, Vector3> Terra::checkQuadIntersection(int x, int z, const Ray& ray){
        Vector3 v1 ((Real)x, getHeightData(x,z), (Real)z);
        Vector3 v2 ((Real)x+1, getHeightData(x+1,z), (Real)z);
        Vector3 v3 ((Real)x, getHeightData(x,z+1), (Real)z+1);
        Vector3 v4 ((Real)x+1, getHeightData(x+1,z+1), (Real)z+1);

        Vector4 p1, p2;
        bool oddRow = false;
        if (z % 2){
        /*  3---4
            | \ |
            1---2*/
            p1 = Math::calculateFaceNormalWithoutNormalize(v2, v4, v3);
            p2 = Math::calculateFaceNormalWithoutNormalize(v1, v2, v3);
            oddRow = true;
        }
        else{
        /*  3---4
            | / |
            1---2*/
            p1 = Math::calculateFaceNormalWithoutNormalize(v1, v2, v4);
            p2 = Math::calculateFaceNormalWithoutNormalize(v1, v4, v3);
        }

        // Test for intersection with the two planes.
        // Then test that the intersection points are actually
        // still inside the triangle (with a small error margin)
        // Also check which triangle it is in
        std::pair<bool, Real> planeInt = ray.intersects(Plane(p1.x, p1.y, p1.z, p1.w));

        if (planeInt.first){
            Vector3 where = ray.getPoint(planeInt.second);
            Vector3 rel = where - v1;
            if (rel.x >= -0.01 && rel.x <= 1.01 && rel.z >= -0.01 && rel.z <= 1.01 // quad bounds
                && ((rel.x >= rel.z && !oddRow) || (rel.x >= (1 - rel.z) && oddRow))) // triangle bounds
                return std::pair<bool, Vector3>(true, where);
        }
        planeInt = ray.intersects(Plane(p2.x, p2.y, p2.z, p2.w));
        if (planeInt.first){
            Vector3 where = ray.getPoint(planeInt.second);
            Vector3 rel = where - v1;
            if (rel.x >= -0.01 && rel.x <= 1.01 && rel.z >= -0.01 && rel.z <= 1.01 // quad bounds
                && ((rel.x <= rel.z && !oddRow) || (rel.x <= (1 - rel.z) && oddRow))) // triangle bounds
                return std::pair<bool, Vector3>(true, where);
        }
        //Essentually, it checks the planes first as sort of like a broad phase, then it actually checks the individual triangles.
        return std::pair<bool, Vector3>(false, Vector3());
    }

    void Terra::applyBlendMouseAction(Southsea::TerrainBlendMouseAction* action, bool performAction){
        auto data = action->getDataMap32();

        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_blendWeightTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        auto it = data.begin();
        Ogre::uint32* pDest = static_cast<Ogre::uint32*>(pixBox.data);

        for(;it != data.end(); it++){
            int targetTerrain = 0;
            uint32 id = _stripTerrainId( (*it).first, targetTerrain );
            if(targetTerrain != mTerrainId) continue;

            uint32 value;
            if(!performAction)
                value = (*it).second.second;
            else
                value = (*it).second.first;

            *(pDest + id) = value;
        }

        pixelBufferBuf->unlock();
    }

    void Terra::applyHeightMouseAction(Southsea::TerrainMouseAction* action, bool performAction){
        assert(action->getActionType() == Southsea::TerrainMouseAction::HEIGHT || action->getActionType() == Southsea::TerrainMouseAction::SMOOTH);
        auto data = action->getDataMap16();

        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        auto it = data.begin();
        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        for(;it != data.end(); it++){
            int targetTerrain = 0;
            uint32 id = _stripTerrainId( (*it).first, targetTerrain );
            if(targetTerrain != mTerrainId) continue;

            uint16 value;
            if(!performAction)
                value = (*it).second.second;
            else
                value = (*it).second.first;

            *(pDest + id) = value;
            m_heightMap[id] = valToHeight(value);
        }

        pixelBufferBuf->unlock();

        updateHeightTextures();
    }

    void Terra::fillLevelTerrainAction(Southsea::LevelTerrainAction* action){
        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        action->fillData(m_heightMapTex->getWidth(), m_heightMapTex->getHeight(), pDest);

        pixelBufferBuf->unlock();
    }

    void Terra::levelToData(const std::vector<Ogre::uint16>& data){
        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = m_heightMapTex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint16* pDest = static_cast<Ogre::uint16*>(pixBox.data);

        const uint16 width = m_heightMapTex->getWidth();
        for(int y = 0; y < m_heightMapTex->getHeight(); y++){
            for(int x = 0; x < width; x++){
                uint16 val = data[x + y * width];
                *pDest++ = val;
                m_heightMap[x + y * width] = valToHeight(val);
            }
        }

        pixelBufferBuf->unlock();

        updateHeightTextures();
    }

    void Terra::updateHeightTextures(){
        mNormalMapperWorkspace->_beginUpdate(true);
        mNormalMapperWorkspace->_update();
        mNormalMapperWorkspace->_endUpdate(true);
    }

    void Terra::saveTextures(const std::string& path, const Ogre::String& namePrefix){
        Ogre::Image heightImg;
        m_heightMapTex->convertToImage(heightImg);
        heightImg.save(path + "/height.png");

        Ogre::Image blendImg;
        m_blendWeightTex->convertToImage(blendImg);
        blendImg.save(path + "/" + namePrefix + "detail.png");
    }

    void Terra::writeVertexDataToBuffer(float* bufferStart) const {
        //float* vertexData = new float[getNumVertices() * 3];
        //float* movData = vertexData;
        for(int y = 0; y < m_depth; y++){
            for(int x = 0; x < m_width; x++){
                *bufferStart++ = x * m_xzRelativeSize.x;
                *bufferStart++ = m_heightMap[x + y * m_width] - (m_height / 2);
                *bufferStart++ = y * m_xzRelativeSize.y;
            }
        }

        //return vertexData;
    }

    void Terra::writeTriangleDataToBuffer(int offset, int* bufferStart) const {
        int newWidth = m_width - 1;
        int newDepth = m_depth - 1;
        //int* triData = new int[getNumTriangles() * 3];
        //int* movData = triData;
        int counter =0;
        for(int y = 0; y < newDepth; y++){
            for(int x = 0; x < newWidth; x++){
                //Write out two triangles
                *bufferStart++ = offset + x + y * newWidth;
                *bufferStart++ = offset + x + 1 + (y+1) * newWidth;
                *bufferStart++ = offset + x + 1 + y * newWidth;

                *bufferStart++ = offset + x + y * newWidth;
                *bufferStart++ = offset + x + 1 + (y+1) * newWidth;
                *bufferStart++ = offset + x + (y+1) * newWidth;

                counter += 6;
            }
        }

        //return triData;
    }

    int Terra::getNumTriangles() const{
        int newWidth = m_width - 1;
        int newDepth = m_depth - 1;
        //2 triangles are needed to make up a single square of vertices.
        int numTriangles = newDepth * newWidth * 2;

        return numTriangles;
    }

    int Terra::getNumVertices() const{
        return m_depth * m_width;
    }

    uint32 Terra::_wrapTerrainId(uint32 value) const {
        //-1 (the edited chunk) becomes 0.
        uint8 targetId = mTerrainId + 1;
        value |= targetId << 28;
        return value;
    }

    uint32 Terra::_stripTerrainId(uint32 value, int& targetTerrain) const {
        targetTerrain = static_cast<int>((value >> 28) & 0xF) - 1;
        value &= 0xFFFFFFF;
        return value;
    }


}
