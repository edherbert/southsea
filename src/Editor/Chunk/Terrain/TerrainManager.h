#pragma once

#include "OgrePlatform.h"
#include <vector>

#include "OgreRay.h"

namespace Ogre{
    class Terra;
    class SceneNode;
}

namespace Southsea{
    class TerrainMouseAction;
    class TerrainHeightMouseAction;
    class TerrainBlendMouseAction;
    class TerrainSmoothMouseAction;
    class TerrainSetHeightMouseAction;
    class LevelTerrainAction;

    /**
    Manages a single piece of terrain within the world.
    */
    class TerrainManager{
    public:
        TerrainManager(int chunkId);
        ~TerrainManager();
        void setup(Ogre::SceneManager* sceneManager);

        void tearDown();
        void initialise(const Ogre::String& heightMapName, const Ogre::String& groupName, const std::string& datablockName, int slotSize, const Ogre::Vector3& terrainpos);
        void initialise(float height, const std::string& datablockName, int slotSize, const Ogre::Vector3& terrainpos);
        void updateTerrain();

        void levelTerrain(float height);
        void applyHeightDiff(Ogre::Vector3 pos, const std::vector<float>& data, int boxSize, float value, TerrainMouseAction* action);
        void applyBlendDiff(Ogre::Vector3 pos, const std::vector<float>& data, int boxSize, float value, int blendLayer, TerrainMouseAction* action);
        void applySmoothDiff(Ogre::Vector3 pos, const std::vector<float>& data, int boxSize, float strength, TerrainMouseAction* action);

        void setVisible(bool visible);

        /**
        Apply the contents of a height map action to this terrain.

        @param performAction
        Whether or not it should perform an action or an anti action.
        */
        void applyHeightMouseAction(TerrainHeightMouseAction* action, bool performAction);
        void applyBlendMouseAction(TerrainBlendMouseAction* action, bool performAction);
        void applySmoothMouseAction(TerrainSmoothMouseAction* action, bool performAction);
        void applySetHeight(Ogre::Vector3 pos, float height, int boxSize, TerrainMouseAction* action);
        void applySetHeightMouseAction(TerrainSetHeightMouseAction* action, bool performAction);

        /**
        Fill a level terrain command with the information it needs to perform its action.
        This makes no direct alterations to the contents of the terrain.
        */
        void fillLevelTerrainAction(LevelTerrainAction* action);

        /**
        Level the terrain according to a list of height data.
        */
        void levelToData(const std::vector<Ogre::uint16>& data);

        /**
        Save the textures of this terrain.

        @param path
        The path where the textures should be saved to.
        */
        void saveTextures(const Ogre::String& path, const Ogre::String& namePrefix);

        std::pair<bool, Ogre::Vector3> checkRayIntersect(Ogre::Ray ray);

        //Wrapper around Terra getHeightAt
        bool getHeightAt(Ogre::Vector3& worldPos);

        //Set the datablock of the terrain to be the default.
        void resetTerrainDatablock();

        //TODO temporary until I have the infrustructure to return the actual size of the chunk.
        float getMaximumTerrainHeight() const { return mTerrainSize.y; }

        void setTerrainEnabled(bool enabled);

        bool isTerrainEnabled() const { return mTerrainEnabled; }

        void writeVertexDataToBuffer(float* bufferStart) const;
        void writeTriangleDataToBuffer(int offset, int* bufferStart) const;
        int getNumTriangles() const;
        int getNumVertices() const;

        float getHeightAtPosition(const Ogre::Vector3& pos);

    private:
        Ogre::Terra* mTerra = 0;
        Ogre::SceneNode* mTerrainNode = 0;
        Ogre::Vector3 mTerrainSize;
        Ogre::SceneManager* mSceneManager;
        int mChunkId;

        bool mTerrainEnabled = false;

        void _initialise(float height, const Ogre::String& heightMapName, const Ogre::String& groupName, const std::string& datablockName, int slotSize, const Ogre::Vector3& terrainpos);

        void _shutdownTerrain();
        void _createTerrain();
    };
}
