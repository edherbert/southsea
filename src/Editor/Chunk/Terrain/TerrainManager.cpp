#include "TerrainManager.h"

#include "terra/Terra.h"
#include <Ogre.h>

#include "terra/Hlms/OgreHlmsTerra.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"

#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"
#include "Editor/Action/Actions/Terrain/TerrainSmoothMouseAction.h"

namespace Southsea{
    TerrainManager::TerrainManager(int chunkId)
        : mChunkId(chunkId) {

    }

    TerrainManager::~TerrainManager(){

    }

    void TerrainManager::tearDown(){
        _shutdownTerrain();
    }

    void TerrainManager::setup(Ogre::SceneManager* sceneManager){
        mSceneManager = sceneManager;
    }

    void TerrainManager::initialise(const Ogre::String& heightMapName, const Ogre::String& groupName, const std::string& datablockName, int slotSize, const Ogre::Vector3& terrainpos){
        _initialise(-1, heightMapName, groupName, datablockName, slotSize, terrainpos);
    }

    void TerrainManager::initialise(float height, const std::string& datablockName, int slotSize, const Ogre::Vector3& terrainpos){
        _initialise(height, "", "", datablockName, slotSize, terrainpos);
    }

    void TerrainManager::_initialise(float height, const Ogre::String& heightMapName, const Ogre::String& groupName, const std::string& datablockName, int slotSize, const Ogre::Vector3& terrainpos){

        Ogre::Root* root = Ogre::Root::getSingletonPtr();
        if(!mTerra){
            _createTerrain();
        }
        assert(mTerra);

        const Ogre::Real rel = (float)slotSize / 1024.0f;
        const Ogre::Vector3 targetTerrainPos = terrainpos + Ogre::Vector3(slotSize / 2 + rel / 2, 0, slotSize / 2 + rel / 2);

        mTerrainSize = Ogre::Vector3(slotSize + rel, slotSize, slotSize + rel);
        if(height < 0){
            Ogre::Image image;
            image.load(heightMapName, groupName);

            mTerra->load(image, targetTerrainPos, mTerrainSize);
        }else{
            //The default height will be half of the slot size.
            //In the future I might add an option to set specific height for each piece of terrain.
            mTerra->load(slotSize / 2, targetTerrainPos, mTerrainSize);
        }

        mTerra->setDatablock( root->getHlmsManager()->getDatablock(datablockName) );

        setTerrainEnabled(true);
    }

    void TerrainManager::setVisible(bool visible){
        mTerra->setVisible(visible);
    }

    void TerrainManager::resetTerrainDatablock(){
        if(!mTerra) return;

        Ogre::HlmsDatablock* db = Ogre::Root::getSingleton().getHlmsManager()->getHlms("Terra")->getDefaultDatablock();
        mTerra->setDatablock(db);
    }

    void TerrainManager::_createTerrain(){
        //If height is less than 0 it is assumed we're using the strings to construct the terrain.

        mTerra = new Ogre::Terra( Ogre::Id::generateNewId<Ogre::MovableObject>(),
                                        &mSceneManager->_getEntityMemoryManager( Ogre::SCENE_STATIC ),
                                        mSceneManager, 0, Ogre::Root::getSingletonPtr()->getCompositorManager2(),
                                        mSceneManager->getCameras()[0], mChunkId);

        mTerra->setCastShadows( false );

        mTerrainNode = mSceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_STATIC);

        mTerrainNode->attachObject( mTerra );
    }

    void TerrainManager::_shutdownTerrain(){
        if(mTerra){
            mTerrainNode->detachObject(mTerra);
            mSceneManager->getRootSceneNode()->removeChild(mTerrainNode);
            mTerrainNode = 0;

            delete mTerra;
            mTerra = 0;
        }
    }

    void TerrainManager::updateTerrain(){
        mTerra->update( Ogre::Vector3( -1, -1, -1 ).normalisedCopy(), 1);
    }

    void TerrainManager::levelTerrain(float height){
        mTerra->levelTerrain(height);
    }

    void TerrainManager::applyHeightDiff(Ogre::Vector3 pos, const std::vector<float>& data, int boxSize, float value, TerrainMouseAction* action){
        Ogre::GridPoint point = mTerra->worldToGridWithNegative(pos);
        //Offset the box so that the corner isn't the centre.
        point.x -= boxSize / 2;
        point.z -= boxSize / 2;
        mTerra->applyHeightDiff(point.x, point.z, data, boxSize, value, action);

        EditorDirtySave e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void TerrainManager::applyBlendDiff(Ogre::Vector3 pos, const std::vector<float>& data, int boxSize, float value, int blendLayer, TerrainMouseAction* action){
        Ogre::GridPoint point = mTerra->worldToGridWithNegative(pos);
        //Offset the box so that the corner isn't the centre.
        point.x -= boxSize / 2;
        point.z -= boxSize / 2;
        mTerra->applyBlendDiff(point.x, point.z, data, boxSize, value, blendLayer, action);

        EditorDirtySave e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void TerrainManager::applySmoothDiff(Ogre::Vector3 pos, const std::vector<float>& data, int boxSize, float strength, TerrainMouseAction* action){
        Ogre::GridPoint point = mTerra->worldToGridWithNegative(pos);
        //Offset the box so that the corner isn't the centre.
        point.x -= boxSize / 2;
        point.z -= boxSize / 2;
        mTerra->applySmoothDiff(point.x, point.z, data, boxSize, strength, action);

        EditorDirtySave e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void TerrainManager::applySetHeight(Ogre::Vector3 pos, float height, int boxSize, TerrainMouseAction* action){
        //TODO remove duplication and maybe look to remove the transmit in the functions.
        Ogre::GridPoint point = mTerra->worldToGridWithNegative(pos);
        //Offset the box so that the corner isn't the centre.
        point.x -= boxSize / 2;
        point.z -= boxSize / 2;
        mTerra->applySetHeight(point.x, point.z, height, boxSize, action);

        EditorDirtySave e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    std::pair<bool, Ogre::Vector3> TerrainManager::checkRayIntersect(Ogre::Ray ray){
        return mTerra->checkRayIntersect(ray);
    }

    void TerrainManager::applyHeightMouseAction(TerrainHeightMouseAction* action, bool performAction){
        mTerra->applyHeightMouseAction(action, performAction);
    }

    void TerrainManager::applyBlendMouseAction(TerrainBlendMouseAction* action, bool performAction){
        mTerra->applyBlendMouseAction(action, performAction);
    }

    void TerrainManager::applySmoothMouseAction(TerrainSmoothMouseAction* action, bool performAction){
        mTerra->applyHeightMouseAction(action, performAction);
    }

    void TerrainManager::applySetHeightMouseAction(TerrainSetHeightMouseAction* action, bool performAction){
        mTerra->applyHeightMouseAction((TerrainMouseAction*)action, performAction);
    }

    void TerrainManager::fillLevelTerrainAction(LevelTerrainAction* action){
        mTerra->fillLevelTerrainAction(action);
    }

    void TerrainManager::levelToData(const std::vector<Ogre::uint16>& data){
        mTerra->levelToData(data);
    }

    void TerrainManager::saveTextures(const Ogre::String& path, const Ogre::String& namePrefix){
        mTerra->saveTextures(path, namePrefix);
    }

    bool TerrainManager::getHeightAt(Ogre::Vector3& worldPos){
        return mTerra->getHeightAt(worldPos);
    }

    void TerrainManager::setTerrainEnabled(bool enabled){
        mTerrainEnabled = enabled;

        mTerrainNode->setVisible(enabled);
    }

    void TerrainManager::writeVertexDataToBuffer(float* bufferStart) const{
        return mTerra->writeVertexDataToBuffer(bufferStart);
    }

    void TerrainManager::writeTriangleDataToBuffer(int offset, int* bufferStart) const{
        return mTerra->writeTriangleDataToBuffer(offset, bufferStart);
    }

    int TerrainManager::getNumTriangles() const{
        return mTerra->getNumTriangles();
    }

    int TerrainManager::getNumVertices() const{
        return mTerra->getNumVertices();
    }

    float TerrainManager::getHeightAtPosition(const Ogre::Vector3& pos){
        Ogre::Vector3 vec(pos.x, 0, pos.z);
        mTerra->getHeightAt(vec);
        return vec.y;
    }

}
