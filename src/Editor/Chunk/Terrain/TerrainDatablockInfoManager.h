#pragma once

#include <map>

#include "OgreString.h"
#include "OgreVector4.h"
#include "OgreVector3.h"

namespace Ogre{
    class HlmsTerraDatablock;
}

namespace Southsea{

    struct TerrainDatablockLayerData{
        Ogre::Vector4 offsetScale = Ogre::Vector4(0, 0, 1, 1);
        float roughness = 1;
        float metalness = 1;
        Ogre::String diffuseTexture = "";

        bool enabled = false;

        static const TerrainDatablockLayerData DEFAULT_LAYER;

        bool operator == (const TerrainDatablockLayerData &l) const{
            if(
                l.roughness == roughness &&
                l.metalness == metalness &&
                l.offsetScale == offsetScale &&
                l.diffuseTexture == diffuseTexture
            ) return true;

            return false;
        }

        bool operator != (const TerrainDatablockLayerData &l) const{
            return !(*this == l);
        }
    };

    struct TerrainDatablockData{
        Ogre::Vector3 diffuse = Ogre::Vector3(1, 1, 1);

        TerrainDatablockLayerData layers[4];
    };


    /**
    A class to manage datablock info.

    The reason this class is necessary is so that I have somewhere to save data between datablock layer enable and disable phases.
    In those phases the datablock is cleared and so are the input dialogs.
    So there would otherwise be no way to determine what the current datablock data should be if that layer was re-enabled.
    This class manages that by keeping track of the data.
    */
    class TerrainDatablockInfoManager{
    public:
        TerrainDatablockInfoManager();
        ~TerrainDatablockInfoManager();

        // static void notifyDatablockTextureName(const Ogre::String& datablockName, const Ogre::String& textureName, int layer);
        //
        // struct StringEntry{
        //     Ogre::String textureStrings[4] = {"", "", "", ""};
        // };

        //static const Ogre::String& getDatablockTextureString(const Ogre::String& datablockName, int layer);

        static void setDiffuse(const Ogre::String& datablockName, Ogre::Vector3 diffuse);
        static void setDiffuseTexture(const Ogre::String& datablockName, const Ogre::String& tex, int layer);
        static void setLayerMetalness(const Ogre::String& datablockName, int layer, float metalness);
        static void setLayerRoughness(const Ogre::String& datablockName, int layer, float roughness);
        static void setLayerOffsetScale(const Ogre::String& datablockName, int layer, Ogre::Vector4 offsetScale);

        static void setLayerEnableDisable(const Ogre::String& datablockName, int layer, bool enabled);

        static void setupDatablockData(Ogre::HlmsTerraDatablock* datablock);

        static const TerrainDatablockData& getDatablockData(const Ogre::String& datablockName);

        static void removeDatablockEntry(const Ogre::String& datablockName);

        //Make sure that a datablock entry for a certain datablock exists. It will be empty.
        static void assureDatablockEntry(const std::string& datablockName);

    private:
        typedef std::map<std::string, TerrainDatablockData> DataMap;

        //Datablock name, data
        static DataMap m_dataMap;

        static const Ogre::String EMPTY;

        static TerrainDatablockData& _getDatablockData(const Ogre::String& datablockName);
    };
}
