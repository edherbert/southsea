#include "TerrainDatablockInfoManager.h"

#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraDatablock.h"

namespace Southsea{
    TerrainDatablockInfoManager::DataMap TerrainDatablockInfoManager::m_dataMap;
    const Ogre::String TerrainDatablockInfoManager::EMPTY = "";
    const TerrainDatablockLayerData TerrainDatablockLayerData::DEFAULT_LAYER;

    TerrainDatablockInfoManager::TerrainDatablockInfoManager(){

    }

    TerrainDatablockInfoManager::~TerrainDatablockInfoManager(){

    }

    void TerrainDatablockInfoManager::setDiffuseTexture(const Ogre::String& datablockName, const Ogre::String& tex, int layer){
        TerrainDatablockData& d = _getDatablockData(datablockName);

        d.layers[layer].diffuseTexture = tex;
    }

    void TerrainDatablockInfoManager::setLayerMetalness(const Ogre::String& datablockName, int layer, float metalness){
        TerrainDatablockData& d = _getDatablockData(datablockName);
        d.layers[layer].metalness = metalness;
    }

    void TerrainDatablockInfoManager::setLayerRoughness(const Ogre::String& datablockName, int layer, float roughness){
        TerrainDatablockData& d = _getDatablockData(datablockName);
        d.layers[layer].roughness = roughness;
    }

    void TerrainDatablockInfoManager::setLayerOffsetScale(const Ogre::String& datablockName, int layer, Ogre::Vector4 offsetScale){
        TerrainDatablockData& d = _getDatablockData(datablockName);
        d.layers[layer].offsetScale = offsetScale;
    }

    void TerrainDatablockInfoManager::setLayerEnableDisable(const Ogre::String& datablockName, int layer, bool enabled){
        TerrainDatablockData& d = _getDatablockData(datablockName);
        d.layers[layer].enabled = enabled;
    }

    void TerrainDatablockInfoManager::setDiffuse(const Ogre::String& datablockName, Ogre::Vector3 diffuse){
        TerrainDatablockData& d = _getDatablockData(datablockName);
        d.diffuse = diffuse;
    }


    TerrainDatablockData& TerrainDatablockInfoManager::_getDatablockData(const Ogre::String& datablockName){
        auto it = m_dataMap.find(datablockName);

        if(it == m_dataMap.end()){ //Not found in the map
            TerrainDatablockData d;
            m_dataMap[datablockName] = d;
        }else{
            return (*it).second;
        }

        //I'm not all that sure about memory issues with a map.
        //I do know that you shouldn't return a pointer from something like a vector as there's no guarantee that memory address will contain valid data all the time.
        //However, this is a reference, and as long as there's no insertion in the mean time I can't imagine that it would be a problem.
        return m_dataMap[datablockName];
    }

    void TerrainDatablockInfoManager::assureDatablockEntry(const std::string& datablockName){
        auto it = m_dataMap.find(datablockName);

        if(it == m_dataMap.end()){ //Not found in the map
            TerrainDatablockData d;
            m_dataMap[datablockName] = d;
        }
    }

    const TerrainDatablockData& TerrainDatablockInfoManager::getDatablockData(const Ogre::String& datablockName){
        auto it = m_dataMap.find(datablockName);

        if(it == m_dataMap.end()){ //Not found in the map
            assert(false && "Datablock with that name not found in the map.");
        }

        return (*it).second;
    }

    void TerrainDatablockInfoManager::removeDatablockEntry(const Ogre::String& datablockName){
        auto it = m_dataMap.find(datablockName);
        if(it == m_dataMap.end()) return;

        m_dataMap.erase(it);
    }

    void TerrainDatablockInfoManager::setupDatablockData(Ogre::HlmsTerraDatablock* datablock){
        TerrainDatablockData& d = _getDatablockData(*datablock->getNameStr());

        d.diffuse = datablock->getDiffuse();

        for(int i = 0; i < 4; i++){
            TerrainDatablockLayerData& layer = d.layers[i];
            layer.roughness = datablock->getRoughness(i);
            layer.metalness = datablock->getMetalness(i);
            layer.offsetScale = datablock->getDetailMapOffsetScale(i);

            if(layer != TerrainDatablockLayerData::DEFAULT_LAYER)
                layer.enabled = true;

            //Don't copy the textures here.
            //The values obtained from the datablock itself are useless anyway.
            //The texture strings should have been populated during json load.
        }
    }
}
