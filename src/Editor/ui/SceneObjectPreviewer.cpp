#include "SceneObjectPreviewer.h"

#include "OgreCamera.h"
#include "OgreSceneManager.h"

//#include "Editor/Scene/SceneFileParserInterface.h"

#include "Editor/Editor.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Resources/ResourceTypes.h"
#include "Editor/Scene/SceneTreeManager.h"
#include <iostream>

//From the avEngine.
#include "World/Slot/Recipe/AvScene/AvSceneFileParserInterface.h"

namespace Southsea{
    SceneObjectPreviewer::SceneObjectPreviewer()
        : mSceneManager(0),
        mEditor(0) {

    }

    SceneObjectPreviewer::~SceneObjectPreviewer(){

    }

    void SceneObjectPreviewer::initialise(Editor* editor, Ogre::SceneManager* sceneManager){
        mSceneManager = sceneManager;
        mEditor = editor;

        mSceneNode = sceneManager->getRootSceneNode()->createChildSceneNode();
        mSceneNode->setVisible(false);
    }

    void SceneObjectPreviewer::shouldPreviewObject(bool preview){
        mPreviewObject = preview;
        mSceneNode->setVisible(preview);

        std::cout << (preview ? "previewing object" : "ending preview") << '\n';
    }

    void SceneObjectPreviewer::setCursorWindowPosition(float x, float y){
        if(!mPreviewObject) return;

        //Right now I only check the terrain. In future I could check ogre aabbs as well.
        Ogre::Ray ray = mEditor->getEditorCamera()->getCameraToViewportRay(x, y);

        auto val = mEditor->getEditorChunk()->getTerrainManager()->checkRayIntersect(ray);
        if(!val.first) {
            //The object missed the terrain.
            mSceneNode->setVisible(false);
            mObjectFoundLocation = false;
            return;
        }

        //The object hit the terrain at this point, so make it visible.
        mSceneNode->setVisible(mPreviewObject);
        mSceneNode->setPosition(val.second);
        mObjectFoundLocation = true;
    }

    void SceneObjectPreviewer::notifyDragEnded(bool succesfulDrag){
        if(!succesfulDrag){
            return;
        }
        if(!mObjectFoundLocation || !mPreviewObject){
            return;
        }


        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        if(mResFSEntry.type == ResourceType::mesh){
            sceneMan->insertObjectIntoTreeFromSelection(mResource, mResFSEntry, mSceneNode->getPosition());
        }
        else if(mResFSEntry.type == ResourceType::avScene){
            sceneMan->insertSceneTreeFileIntoTreeFromSelection(mFullPath, mSceneNode->getPosition());
        }
    }

    void SceneObjectPreviewer::setCurrentObjectPreview(const Resource& res, const ResourceFSEntry& resEntry, const std::string& fullPath){
        _removeAttachedObjectFromNode();

        if(resEntry.type == ResourceType::mesh){
            _setCurrentAttachedObject(res, resEntry);
        }
        else if(resEntry.type == ResourceType::avScene){
            _setCurrentAttachedObjectScene(fullPath);
        }
        mCanPreview = _canPreviewResourceType(resEntry.type);
        mResource = res;
        mResFSEntry = resEntry;
        mFullPath = fullPath;
    }

    bool SceneObjectPreviewer::_canPreviewResourceType(ResourceType type) const {
        //In future there will be more of these.
        return type == ResourceType::mesh || type == ResourceType::avScene;
    }

    void SceneObjectPreviewer::_removeAttachedObjectFromNode(){
        //Check for and destroy any old movable objects.
        Ogre::SceneNode::ObjectIterator it = mSceneNode->getAttachedObjectIterator();
        if(it.hasMoreElements()){
            Ogre::MovableObject* obj = it.getNext();
            mSceneManager->destroyMovableObject(obj);
        }
        //TODO update this with recursive deletion.
    }

    void SceneObjectPreviewer::_setCurrentAttachedObjectScene(const std::string& fullPath){
        AV::SimpleSceneFileParserInterface interface(mSceneManager, mSceneNode);
        bool result = AV::AVSceneFileParser::loadFile(fullPath, &interface);
        if(!result){
            std::cout << "Error previewing scene file" << std::endl;
        }
    }

    void SceneObjectPreviewer::_setCurrentAttachedObject(const Resource& res, const ResourceFSEntry& resEntry){
        Ogre::Item *item = mSceneManager->createItem(resEntry.name, res.group, Ogre::SCENE_DYNAMIC);
        mSceneNode->attachObject((Ogre::MovableObject*)item);
        mSceneNode->setVisible(false);
    }
}
