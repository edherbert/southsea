#include "ObjectCoordsGizmo.h"

#include "OgreSceneManager.h"
#include "OgreRoot.h"
#include "OgreHlmsUnlit.h"
#include "OgreHlmsUnlitDatablock.h"
#include "OgreItem.h"
#include "OgreCamera.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Action/Actions/Object/BasicCoordinatesChange.h"

#include <iostream>

#include "Editor/EditorSingleton.h"

namespace Southsea{
    const char* datablockNames[10] = {
        "Southsea/xArrowDatablock", "Southsea/yArrowDatablock", "Southsea/zArrowDatablock",
        "Southsea/boundingBoxDatablock",
        "Southsea/xScaleDatablock", "Southsea/yScaleDatablock", "Southsea/zScaleDatablock",
        "Southsea/xOrientateDatablock", "Southsea/yOrientateDatablock", "Southsea/zOrientateDatablock"
    };

    ObjectCoordsGizmo::ObjectCoordsGizmo(){

    }

    ObjectCoordsGizmo::~ObjectCoordsGizmo(){
        EventDispatcher::unsubscribe(EventType::Editor, this);

        Ogre::Hlms* hlms = Ogre::Root::getSingletonPtr()->getHlmsManager()->getHlms(Ogre::HLMS_UNLIT);
        Ogre::HlmsUnlit* unlit = dynamic_cast<Ogre::HlmsUnlit*>(hlms);
        for(int i = 0; i < sizeof(datablockNames) / sizeof(const char*); i++){
            unlit->destroyDatablock(datablockNames[i]);
        }
    }

    void ObjectCoordsGizmo::initialise(Ogre::SceneManager* sceneManager){
        mSceneManager = sceneManager;

        mSceneQuery = mSceneManager->createRayQuery(Ogre::Ray());

        EventDispatcher::subscribe(EventType::Editor, SOUTHSEA_BIND(ObjectCoordsGizmo::editorEventReceiver));

        _createCoordsMeshes();

        focusToObject(0); //Hide the gizmo to start.
    }

    void ObjectCoordsGizmo::setUniversalScaleModifier(bool scale){
        mUniversalScaleModifier = scale;
    }

    void ObjectCoordsGizmo::_createCoordsMeshes(){
        Ogre::Hlms* hlms = Ogre::Root::getSingletonPtr()->getHlmsManager()->getHlms(Ogre::HLMS_UNLIT);
        Ogre::HlmsUnlit* unlit = dynamic_cast<Ogre::HlmsUnlit*>(hlms);

        mItemNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
        mArrowContainerNode = mItemNode->createChildSceneNode();

        const Ogre::Quaternion orientationVals[3] = {Ogre::Quaternion(Ogre::Degree(-90), Ogre::Vector3(0, 0, 1)), Ogre::Quaternion(), Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3(1, 0, 0))};

        //Create the position arrows.
        for(int i = 0; i < 3; i++){
            Ogre::SceneNode* arrowNode = mArrowContainerNode->createChildSceneNode();
            Ogre::Item *item = mSceneManager->createItem("arrow.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
            arrowNode->attachObject((Ogre::MovableObject*)item);
            arrowNode->setOrientation(orientationVals[i]);
            mArrowNodes[i] = arrowNode;

            //Create the line for each axis.
            Ogre::SceneNode* lineNode = arrowNode->createChildSceneNode();
            Ogre::Item *lineItem = mSceneManager->createItem("line", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
            lineNode->attachObject((Ogre::MovableObject*)lineItem);
            lineNode->setPosition(Ogre::Vector3(0, -500, 0)); //An offset so the line is drawn in the middle.
            lineNode->setScale(Ogre::Vector3(1000, 1000, 1000));
            lineNode->setVisible(false);

            item->setRenderQueueGroup(80);

            Ogre::HlmsMacroblock mm;
            mm.mDepthCheck = false;

            const char* targetName = datablockNames[i];
            Ogre::HlmsDatablock* block = unlit->createDatablock(targetName, targetName, mm, Ogre::HlmsBlendblock(), Ogre::HlmsParamVec(), false);
            item->setDatablock(block);
            lineItem->setDatablock(block);
            Ogre::HlmsUnlitDatablock* unlitBlock = dynamic_cast<Ogre::HlmsUnlitDatablock*>(block);
            unlitBlock->setUseColour(true);

            mArrowDatablocks[i] = unlitBlock;
            mAttachedItems[i] = item;
            mArrowLines[i] = lineNode;
        }

        //Create the scale handles.
        for(int i = 0; i < 3; i++){
            Ogre::SceneNode* arrowNode = mArrowContainerNode->createChildSceneNode();
            Ogre::Item *item = mSceneManager->createItem("scaleHandle.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
            arrowNode->attachObject((Ogre::MovableObject*)item);
            arrowNode->setOrientation(orientationVals[i]);
            arrowNode->setScale(0.28, 0.28, 0.28);
            mScaleNodes[i] = arrowNode;

            //Create the line for each axis.
            Ogre::SceneNode* lineNode = arrowNode->createChildSceneNode();
            Ogre::Item *lineItem = mSceneManager->createItem("line", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
            lineNode->attachObject((Ogre::MovableObject*)lineItem);
            lineNode->setPosition(Ogre::Vector3(0, -500, 0)); //An offset so the line is drawn in the middle.
            lineNode->setScale(Ogre::Vector3(1000, 1000, 1000));
            lineNode->setVisible(false);

            item->setRenderQueueGroup(80);

            Ogre::HlmsMacroblock mm;
            mm.mDepthCheck = false;

            const char* targetName = datablockNames[i + 4];
            Ogre::HlmsDatablock* block = unlit->createDatablock(targetName, targetName, mm, Ogre::HlmsBlendblock(), Ogre::HlmsParamVec(), false);
            item->setDatablock(block);
            lineItem->setDatablock(block);
            Ogre::HlmsUnlitDatablock* unlitBlock = dynamic_cast<Ogre::HlmsUnlitDatablock*>(block);
            unlitBlock->setUseColour(true);

            mScaleDatablocks[i] = unlitBlock;
            mScaleHandleItems[i] = item;
            //TODO these lines could be shared.
            mScaleHandleLines[i] = lineNode;
        }

        //Create the orientate handles.
        for(int i = 0; i < 3; i++){
            Ogre::SceneNode* arrowNode = mArrowContainerNode->createChildSceneNode();
            Ogre::Item *item = mSceneManager->createItem("circle.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
            arrowNode->attachObject((Ogre::MovableObject*)item);
            arrowNode->setOrientation(orientationVals[i]);
            mOrientateNodes[i] = arrowNode;

            item->setRenderQueueGroup(80);

            Ogre::HlmsMacroblock mm;
            mm.mDepthCheck = false;

            const char* targetName = datablockNames[i + 7];
            Ogre::HlmsDatablock* block = unlit->createDatablock(targetName, targetName, mm, Ogre::HlmsBlendblock(), Ogre::HlmsParamVec(), false);
            item->setDatablock(block);
            Ogre::HlmsUnlitDatablock* unlitBlock = dynamic_cast<Ogre::HlmsUnlitDatablock*>(block);
            unlitBlock->setUseColour(true);

            mOrientateDatablocks[i] = unlitBlock;
            mOrientateHandleItems[i] = item;
        }

        mBoundingBox = mItemNode->createChildSceneNode();
        mBoundingBoxItem = mSceneManager->createItem("lineBox", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
        mBoundingBox->attachObject(mBoundingBoxItem);

        Ogre::HlmsMacroblock mm;
        //mm.mDepthCheck = false;
        //mm.mPolygonMode = Ogre::PM_WIREFRAME;
        Ogre::HlmsDatablock* block = unlit->createDatablock(datablockNames[3], datablockNames[3], mm, Ogre::HlmsBlendblock(), Ogre::HlmsParamVec(), false);
        mBoundingBoxItem->setDatablock(block);
        Ogre::HlmsUnlitDatablock* unlitBlock = dynamic_cast<Ogre::HlmsUnlitDatablock*>(block);
        unlitBlock->setUseColour(true);
        unlitBlock->setColour(Ogre::ColourValue(0.9, 0.9, 0.9));

        {
            mTerrainBoxNode = mArrowContainerNode->createChildSceneNode();
            mTerrainBoxItem = mSceneManager->createItem("cube", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
            mTerrainBoxNode->attachObject((Ogre::MovableObject*)mTerrainBoxItem);
            mTerrainBoxNode->setScale(0.3, 0.3, 0.3);
            mTerrainBoxItem->setDatablock(mArrowDatablocks[0]);
        }

        mItemNode->setScale(Ogre::Vector3(2, 2, 2));
        _setHighlightObject(CoordsGizmoType::none);
    }

    void ObjectCoordsGizmo::updateActionPosition(float mouseX, float mouseY){
        const Editor* e = EditorSingleton::getEditor();
        Ogre::Ray ray = e->getEditorCamera()->getCameraToViewportRay(mouseX, mouseY);

        if(mCurrentSelectionTool == SelectionTool::ObjectPicker){
            std::pair<bool, Ogre::Real> result = ray.intersects(mCurrentCheckingPlane);

            if(result.first){
                Ogre::Vector3 targetPos = ray.getPoint(result.second);

                Ogre::Vector3 currentPos = mItemNode->getPosition();

                if(mFirstMoveFrame){ //This is the first frame, so populate the movement offset.
                    //The movement offset avoids making the object jump to where the mouse is clicked.
                    //If this wasn't here, the object would just jump to the position where the mouse ray intersects.
                    //However, the user is more likely to have their mouse on one of the arrows, and this number is the difference between the first ray cast and the object origin.
                    if(mCurrentlyInteractingType == CoordsGizmoType::posX) mMovementOffset = targetPos.x - currentPos.x;
                    else if(mCurrentlyInteractingType == CoordsGizmoType::posY) mMovementOffset = targetPos.y - currentPos.y;
                    else if(mCurrentlyInteractingType == CoordsGizmoType::posZ) mMovementOffset = targetPos.z - currentPos.z;

                    mFirstMoveFrame = false;
                }

                if(mCurrentlyInteractingType == CoordsGizmoType::posX) currentPos.x = targetPos.x - mMovementOffset;
                else if(mCurrentlyInteractingType == CoordsGizmoType::posY) currentPos.y = targetPos.y - mMovementOffset;
                else if(mCurrentlyInteractingType == CoordsGizmoType::posZ) currentPos.z = targetPos.z - mMovementOffset;


                _moveSelectedObject(currentPos, e->getGridSnapEnabled(), e->getSnapAmount());
            }
        }else if(mCurrentSelectionTool == SelectionTool::ObjectScale){
            static Ogre::Vector3 prevPos = Ogre::Vector3::ZERO;
            static Ogre::Vector3 startingPos = Ogre::Vector3::ZERO;
            std::pair<bool, Ogre::Real> result = ray.intersects(mCurrentCheckingPlane);

            if(result.first){
                Ogre::Vector3 targetPos = ray.getPoint(result.second);

                if(mFirstMoveFrame){
                    //Keep a track of where the mouse started, scaling is performed relative to this.
                    startingPos = targetPos;
                    prevPos = Ogre::Vector3::ZERO;
                    mFirstMoveFrame = false;
                }
                targetPos -= startingPos;

                Ogre::Vector3 currentScale = mCurrentObject->getScale();

                Ogre::Vector3 diff = targetPos - prevPos;
                if(mUniversalScaleModifier){
                    if(mCurrentlyInteractingType == CoordsGizmoType::scaleX) currentScale += diff.x;
                    else if(mCurrentlyInteractingType == CoordsGizmoType::scaleY) currentScale += diff.y;
                    else if(mCurrentlyInteractingType == CoordsGizmoType::scaleZ) currentScale += diff.z;
                }else{
                    if(mCurrentlyInteractingType == CoordsGizmoType::scaleX) currentScale.x += diff.x;
                    else if(mCurrentlyInteractingType == CoordsGizmoType::scaleY) currentScale.y += diff.y;
                    else if(mCurrentlyInteractingType == CoordsGizmoType::scaleZ) currentScale.z += diff.z;
                }

                _scaleSelectedObject(currentScale, e->getGridSnapEnabled(), e->getSnapAmount());
                prevPos = targetPos;
            }
        }else if(mCurrentSelectionTool == SelectionTool::ObjectOrientate){
            static Ogre::Vector3 prevPos = Ogre::Vector3::ZERO;
            static Ogre::Vector3 startingPos = Ogre::Vector3::ZERO;
            std::pair<bool, Ogre::Real> result = ray.intersects(mCurrentCheckingPlane);

            if(result.first){
                Ogre::Vector3 targetPos = ray.getPoint(result.second);

                if(mFirstMoveFrame){
                    //Keep a track of where the mouse started, scaling is performed relative to this.
                    startingPos = targetPos;
                    prevPos = Ogre::Vector3::ZERO;
                    mFirstMoveFrame = false;
                }
                targetPos -= startingPos;

                //Ogre::Vector3 currentScale = mCurrentObject->getScale();
                Ogre::Quaternion currentOrientation = mCurrentObject->getOrientation();

                Ogre::Vector3 diff = targetPos - prevPos;

                {
                    diff *= 0.1;
                    if(mCurrentlyInteractingType == CoordsGizmoType::orientateX){
                        Ogre::Radian rad = currentOrientation.getPitch();
                        currentOrientation = currentOrientation + Ogre::Quaternion(rad + Ogre::Radian(diff.x), Ogre::Vector3(1, 0, 0));
                    }
                    else if(mCurrentlyInteractingType == CoordsGizmoType::orientateY){
                        Ogre::Radian rad = currentOrientation.getYaw();
                        currentOrientation = currentOrientation + Ogre::Quaternion(rad + Ogre::Radian(diff.y), Ogre::Vector3(0, 1, 0));
                    }
                    else if(mCurrentlyInteractingType == CoordsGizmoType::orientateZ){
                        Ogre::Radian rad = currentOrientation.getRoll();
                        currentOrientation = currentOrientation + Ogre::Quaternion(rad + Ogre::Radian(diff.z), Ogre::Vector3(0, 0, 1));
                    }
                }

                mCurrentObject->setOrientation(currentOrientation);
                prevPos = targetPos;
            }
        }else if(mCurrentSelectionTool == SelectionTool::TerrainPlacer){
            std::pair<bool, Ogre::Vector3> result = e->getEditorChunk()->getTerrainManager()->checkRayIntersect(ray);
            if(result.first){
                _moveSelectedObject(result.second, e->getGridSnapEnabled(), e->getSnapAmount());
            }
        }

    }

    void ObjectCoordsGizmo::populateAction(BasicCoordinatesChange* coordAction){
        Ogre::Vector4 newData = Ogre::Vector4::ZERO;
        BasicCoordinatesChange::Type type;
        switch(mCurrentSelectionTool){
            case SelectionTool::ObjectPicker:
            case SelectionTool::TerrainPlacer:{
                newData = mCurrentObject->_getDerivedPositionUpdated();
                type = BasicCoordinatesChange::Type::position;
                break;
            }
            case SelectionTool::ObjectScale:{
                newData = mCurrentObject->getScale();
                type = BasicCoordinatesChange::Type::scale;
                break;
            }
            case SelectionTool::ObjectOrientate:{
                const Ogre::Quaternion d = mCurrentObject->getOrientation();
                newData = Ogre::Vector4(d.x, d.y, d.z, d.w);
                type = BasicCoordinatesChange::Type::orientate;
                break;
            }
            default: assert(false);
        }
        assert(mCurrentObjectData);
        coordAction->populate(mCurrentlyFocusedObject, mCoordStart, newData, type, true, mCurrentObjectData->type());
    }

    inline Ogre::Vector3 ObjectCoordsGizmo::_floorVectorToSnap(const Ogre::Vector3& vec) const{
        bool activeVals[3] = {
            mCurrentlyInteractingType == CoordsGizmoType::posX || mCurrentlyInteractingType == CoordsGizmoType::scaleX,
            mCurrentlyInteractingType == CoordsGizmoType::posY || mCurrentlyInteractingType == CoordsGizmoType::scaleY,
            mCurrentlyInteractingType == CoordsGizmoType::posZ || mCurrentlyInteractingType == CoordsGizmoType::scaleZ
        };
        return Ogre::Vector3(
            activeVals[0] ? Ogre::Math::Floor(vec.x) : vec.x,
            activeVals[1] ? Ogre::Math::Floor(vec.y) : vec.y,
            activeVals[2] ? Ogre::Math::Floor(vec.z) : vec.z
        );
    }

    Ogre::Vector3 ObjectCoordsGizmo::_processSnapValue(const Ogre::Vector3& value, bool gridSnapEnabled, float snapAmount){
        Ogre::Vector3 targetPos(value);
        if(gridSnapEnabled){
            targetPos /= snapAmount;
            targetPos = _floorVectorToSnap(targetPos);
            if(!mSnappedPreviously){
                //No previous snap has occured yet, so check if one has now.
                //For some reason I can't just use .xyz.
                const Ogre::Vector3 startVal = Ogre::Vector3(mCoordStart.x, mCoordStart.y, mCoordStart.z) / snapAmount;
                if(_floorVectorToSnap(startVal) != targetPos){
                    //A snap occured.
                    mSnappedPreviously = true;
                }
                //This is to prevent an initial jump in coordinates when the manipulation begins.
                targetPos = startVal;
            }
            targetPos *= snapAmount;
        }

        return targetPos;
    }

    void ObjectCoordsGizmo::_scaleSelectedObject(const Ogre::Vector3& scale, bool gridSnapEnabled, float snapAmount){
        //const Ogre::Vector3 val = _processSnapValue(scale, gridSnapEnabled, snapAmount);

        mCurrentObject->setScale(scale);
    }

    void ObjectCoordsGizmo::_moveSelectedObject(const Ogre::Vector3& pos, bool gridSnapEnabled, float snapAmount){
        const Ogre::Vector3 val = _processSnapValue(pos, gridSnapEnabled, snapAmount);

        mItemNode->setPosition(val);
        mCurrentObject->_setDerivedPosition(val);
        _updateGizmoSizeFromCamera();
    }

    void ObjectCoordsGizmo::updateMousePosition(float mouseX, float mouseY){
        CoordsGizmoType insersectedObject = _castRay(mouseX, mouseY);

        _setHighlightObject(insersectedObject);
    }

    CoordsGizmoType ObjectCoordsGizmo::_castRay(float mouseX, float mouseY){
        const Editor* e = EditorSingleton::getEditor();

        Ogre::Ray ray = e->getEditorCamera()->getCameraToViewportRay(mouseX, mouseY);

        //Ensure position and AABB data is up to date.
        mSceneManager->updateSceneGraph();
        mSceneQuery->setRay(ray);
        Ogre::RaySceneQueryResult& result = mSceneQuery->execute();
        for(const Ogre::RaySceneQueryResultEntry& e : result){
            if(e.distance <= 0) continue; //It found the camera.

            switch(mCurrentSelectionTool){
                case SelectionTool::ObjectPicker:{
                    for(int i = 0; i < 3; i++){
                        //If it's one of the movable arrows
                        if(mAttachedItems[i] == e.movable){
                            return (CoordsGizmoType)i;
                        }
                    }
                    break;
                }
                case SelectionTool::ObjectScale:{
                    for(int i = 0; i < 3; i++){
                        if(mScaleHandleItems[i] == e.movable){
                            return (CoordsGizmoType)(i + 6);
                        }
                    }
                    break;
                }
                case SelectionTool::ObjectOrientate:{
                    for(int i = 0; i < 3; i++){
                        if(mOrientateHandleItems[i] == e.movable){
                            return (CoordsGizmoType)(i + 9);
                        }
                    }
                    break;
                }
                case SelectionTool::TerrainPlacer:{
                    if(e.movable == mTerrainBoxItem) return CoordsGizmoType::terrainBox;
                    break;
                }
            }
        }

        return CoordsGizmoType::none;
    }

    void ObjectCoordsGizmo::_setHighlightObject(CoordsGizmoType obj){
        static const Ogre::ColourValue colourVals[3] = {Ogre::ColourValue(1, 0, 1, 0), Ogre::ColourValue(0, 1, 0, 0), Ogre::ColourValue(0, 0, 1, 0)};
        static const Ogre::ColourValue highlightColourVals[3] = {Ogre::ColourValue(0.6, 0, 0.6, 0), Ogre::ColourValue(0, 0.6, 0, 0), Ogre::ColourValue(0, 0, 0.6, 0)};

        if(obj == CoordsGizmoType::none && !mGizmoObjectHovered) return; //Nothing to do.

        for(int i = 0; i < 3; i++){
            mArrowDatablocks[i]->setColour(colourVals[i]);
            mScaleDatablocks[i]->setColour(colourVals[i]);
            mOrientateDatablocks[i]->setColour(colourVals[i]);
        }

        if(obj == CoordsGizmoType::none){
            mGizmoObjectHovered = false;
            return;
        }

        mGizmoObjectHovered = true;
        if(obj == CoordsGizmoType::posX || obj == CoordsGizmoType::posY || obj == CoordsGizmoType::posZ){
            int target = (int)obj;
            mArrowDatablocks[target]->setColour(highlightColourVals[target]);
        }else if(obj == CoordsGizmoType::scaleX || obj == CoordsGizmoType::scaleY || obj == CoordsGizmoType::scaleZ){
            int target = (int)obj;
            target -= 6;
            mScaleDatablocks[target]->setColour(highlightColourVals[target]);
        }else if(obj == CoordsGizmoType::orientateX || obj == CoordsGizmoType::orientateY || obj == CoordsGizmoType::orientateZ){
            int target = (int)obj;
            target -= 9;
            mOrientateDatablocks[target]->setColour(highlightColourVals[target]);
        }else if(obj == CoordsGizmoType::terrainBox){
            mArrowDatablocks[0]->setColour(highlightColourVals[0]);
        }else{
            //Assume something was hovered by default, unless we reach this if branch.
            mGizmoObjectHovered = false;
        }
    }

    CoordsGizmoType ObjectCoordsGizmo::isItemCoordWidget(Ogre::MovableObject* object){
        if(object == mBoundingBoxItem) return CoordsGizmoType::boundingBox;
        for(int i = 0; i < 3; i++){
            if(mAttachedItems[i] == object){
                return (CoordsGizmoType)i;
            }
        }
        for(int i = 0; i < 3; i++){
            if(mScaleHandleItems[i] == object){
                return (CoordsGizmoType)(i + 6);
            }
        }
        for(int i = 0; i < 3; i++){
            if(mOrientateHandleItems[i] == object){
                return (CoordsGizmoType)(i + 9);
            }
        }
        if(object == mTerrainBoxItem) return CoordsGizmoType::terrainBox;

        return CoordsGizmoType::none;
    }

    void ObjectCoordsGizmo::notifyMouseInteract(CoordsGizmoType interactedType){
        mCurrentlyInteractingType = interactedType;
        if(interactedType == CoordsGizmoType::none){
            for(int i = 0; i < 3; i++){
                mArrowLines[i]->setVisible(false);
                mScaleHandleLines[i]->setVisible(false);
            }
            mCoordStart = Ogre::Vector4::ZERO;

            return;
        }
        mFirstMoveFrame = true;

        if(mCurrentSelectionTool == SelectionTool::ObjectPicker){
            mArrowLines[(int)interactedType]->setVisible(true);
        }
        else if(mCurrentSelectionTool == SelectionTool::ObjectScale){
            mScaleHandleLines[ ((int)interactedType) - 6 ]->setVisible(true);
        }

        Ogre::Vector3 gizmoPos = mItemNode->getPosition();
        if(interactedType == CoordsGizmoType::posY || interactedType == CoordsGizmoType::scaleY){
            mCurrentCheckingPlane = Ogre::Plane(Ogre::Vector3::UNIT_Z, gizmoPos.z);
        }else{
            mCurrentCheckingPlane = Ogre::Plane(Ogre::Vector3::UNIT_Y, gizmoPos.y);
        }

        if(mCurrentSelectionTool == SelectionTool::ObjectPicker || mCurrentSelectionTool == SelectionTool::TerrainPlacer){
            mCoordStart = gizmoPos;
        }else if(mCurrentSelectionTool == SelectionTool::ObjectScale){
            mCoordStart = mCurrentObject->getScale();
        }else if(mCurrentSelectionTool == SelectionTool::ObjectOrientate){
            const Ogre::Quaternion d = mCurrentObject->getOrientation();
            mCoordStart = Ogre::Vector4(d.x, d.y, d.z, d.w);
        }
    }

    void ObjectCoordsGizmo::notifyCameraMovement(const Ogre::Vector3& pos){
        mStoredCameraPos = pos;

        if(mGizmoVisible) _updateGizmoSizeFromCamera();
    }

    void ObjectCoordsGizmo::_updateGizmoSizeFromCamera(){
        Ogre::Real dist = mStoredCameraPos.distance(mItemNode->getPosition());
        dist *= 0.015;

        mArrowContainerNode->setScale(Ogre::Vector3(dist, dist, dist));
    }

    void ObjectCoordsGizmo::_setGizmoVisible(bool visible){
        //Set everything to not visible regardless. This will cascade to each object.
        //Depending on the current selector tool, we later go through setting each item to visible depending on whether it's necessary.
        mItemNode->setVisible(false);
        mGizmoVisible = visible;
        if(!visible) return; //Nothing more to do.

        mBoundingBox->setVisible(visible && mBoundingBoxVisible);

        switch(mCurrentSelectionTool){
            case SelectionTool::ObjectPicker:{
                for(int i = 0; i < 3; i++){
                    mArrowNodes[i]->setVisible(true);
                    mArrowLines[i]->setVisible(false);
                }
                break;
            }
            case SelectionTool::ObjectScale:{
                for(int i = 0; i < 3; i++){
                    mScaleNodes[i]->setVisible(true);
                    mScaleHandleLines[i]->setVisible(false);
                }
                break;
            }
            case SelectionTool::ObjectOrientate:{
                for(int i = 0; i < 3; i++){
                    mOrientateNodes[i]->setVisible(true);
                    //mOrientateHandleLines[i]->setVisible(false);
                }
                break;
            }
            case SelectionTool::TerrainPlacer:{
                mTerrainBoxNode->setVisible(true);
                break;
            }
        }
    }

    void ObjectCoordsGizmo::focusToObject(EntryId id){
        mCurrentlyFocusedObject = id;

        if(id == 0){
            mCurrentObject = 0;
            mBoundingBoxVisible = false;
            _setGizmoVisible(false);
            return;
        }

        mCurrentObject = EditorSingleton::getEditor()->getOgreTreeManager()->getNodeById(id);
        mCurrentObjectData = EditorSingleton::getEditor()->getTreeObjectManager()->getDataObject(id);
        Ogre::Vector3 pos = mCurrentObject->_getDerivedPositionUpdated();

        mItemNode->setPosition(pos);

        //Reset the current snapping.
        mSnappedPreviously = false;

        Ogre::SceneNode::ObjectIterator it = mCurrentObject->getAttachedObjectIterator();
        if(it.hasMoreElements()){
            Ogre::MovableObject* obj = it.getNext();
            Ogre::Aabb box = obj->getWorldAabbUpdated();

            //The bounding box's centre is in global coordinates, so do a derived position.
            mBoundingBox->_setDerivedPosition(box.mCenter);
            mBoundingBox->_getDerivedPositionUpdated(); //This forces the node position to be flushed so the scale can be applied correctly.
            mBoundingBox->setScale(box.mHalfSize / 2);
            mBoundingBoxVisible = true;
        }else{
            //This scene node has nothing attached, so the bounding box needs to be made invisible.
            mBoundingBoxVisible = false;
        }
        assert(!it.hasMoreElements()); //For now there should only be one object attached.

        _setGizmoVisible(true);
        _updateGizmoSizeFromCamera(); //Make sure the gizmo is the correct size.
    }

    void ObjectCoordsGizmo::_notifyNewSelectionTool(SelectionTool newTool){
        mCurrentSelectionTool = newTool;
        if(mGizmoVisible){
            //This will change the visibility of the gizmo controller objects to match the new selection tool.
            _setGizmoVisible(mGizmoVisible);
        }
    }

    bool ObjectCoordsGizmo::editorEventReceiver(const Event &e){
        const EditorEvent& editorEvent = (const EditorEvent&)e;
        if(editorEvent.eventCategory() == EditorEventCategory::SceneTreeSelectionChanged){
            const SceneTreeSelectionChangedEvent& selectionEvent = (const SceneTreeSelectionChangedEvent&)e;

            if(selectionEvent.selectionCount > 0){
                const std::set<EntryId>& selectedItems = EditorSingleton::getEditor()->getSceneTreeManager()->getSelectedIds();
                assert(selectedItems.size() > 0);

                //TODO this won't work so well with multiple selection.
                focusToObject( *(selectedItems.begin()) );
            }else{
                focusToObject(0);
            }
        }
        else if(editorEvent.eventCategory() == EditorEventCategory::SceneTreeValueChange){
            const SceneTreeValueChangeEvent& valueEvent = (const SceneTreeValueChangeEvent&)e;

            if(valueEvent.id == mCurrentlyFocusedObject){
                focusToObject(mCurrentlyFocusedObject);
            }
        }
        else if(editorEvent.eventCategory() == EditorEventCategory::ChunkSwitch){
            focusToObject(0);
        }
        else if(editorEvent.eventCategory() == EditorEventCategory::SelectionToolChange){
            const EditorSelectionToolChange& selectionToolEvent = (const EditorSelectionToolChange&)e;

            _notifyNewSelectionTool(selectionToolEvent.newTool);
        }

        return false;
    }
}
