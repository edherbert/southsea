#pragma once

#include "Editor/Scene/SceneRaycastHelper.h"

namespace Southsea{
    /**
    A class responsible for managing editor popups and their content.
    This does not include modal popups, but instead things like right click options and menus.
    */
    class EditorToolDialogManager{
    public:
        EditorToolDialogManager();
        ~EditorToolDialogManager();

        /**
        Set the objects to display as part of the raycast list.
        Setting a list with one or more entries will display the menu.
        */
        void setRaycastPickedObjects(std::vector<RaycastListEntry>& outList);

        static void drawPopups();

    private:
        static std::vector<RaycastListEntry> mStoredRaycastResults;
    };
}
