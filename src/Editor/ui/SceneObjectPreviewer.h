#pragma once

#include "Editor/Resources/ResourceTypes.h"

namespace Ogre{
    class SceneManager;
    class SceneNode;
    class Vector3;
}

namespace Southsea{
    class Editor;

    class SceneObjectPreviewer{
    public:
        SceneObjectPreviewer();
        ~SceneObjectPreviewer();

        void initialise(Editor* editor, Ogre::SceneManager* sceneManager);

        /**
        Whether an object should be previewed in the viewport during a resource drag.
        This function should be called to enable or disable when the user's mouse enters or leaves the viewport region.
        */
        void shouldPreviewObject(bool preview);

        /**
        Call each time the cursor moves in the viewport.

        @param x
        Normalised x coordinate
        @param y
        Normalised y coordinate
        */
        void setCursorWindowPosition(float x, float y);

        void setCurrentObjectPreview(const Resource& res, const ResourceFSEntry& resEntry, const std::string& fullPat);

        /**
        Notify the manager that the drag ended, and it should try and insert the object into the scene.
        */
        void notifyDragEnded(bool succesfulDrag);

        bool isPreviewingObject() const { return mPreviewObject && mCanPreview; }

    private:
        bool mPreviewObject;
        bool mObjectFoundLocation;
        bool mCanPreview;

        ResourceFSEntry mResFSEntry;
        Resource mResource;
        std::string mFullPath;

        Ogre::SceneManager* mSceneManager;
        Ogre::SceneNode* mSceneNode;
        Editor* mEditor;

        void _setCurrentAttachedObject(const Resource& res, const ResourceFSEntry& resEntry);
        void _setCurrentAttachedObjectScene(const std::string& fullPath);

        bool _canPreviewResourceType(ResourceType type) const;
        void _removeAttachedObjectFromNode();
    };
}
