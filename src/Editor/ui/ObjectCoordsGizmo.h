#pragma once

namespace Ogre{
    class SceneManager;
    class SceneNode;
    class Item;
    class HlmsUnlitDatablock;
    class RaySceneQuery;
    class Camera;
}

#include "OgrePlane.h"
#include "OgreVector3.h"
#include "OgreVector4.h"

#include "Editor/Scene/SceneTypes.h"
#include "Editor/EditorSelectionTools.h"

namespace Southsea{
    class Event;
    class BasicCoordinatesChange;

    enum class CoordsGizmoType{
        posX, posY, posZ, none, boundingBox, terrainBox, scaleX, scaleY, scaleZ, orientateX, orientateY, orientateZ
    };

    /**
    A class responsible for drawing a gizmo to the scene which allows mouse manipulation of objects.
    */
    class ObjectCoordsGizmo{
    public:
        ObjectCoordsGizmo();
        ~ObjectCoordsGizmo();

        void initialise(Ogre::SceneManager* sceneManager);

        void updateMousePosition(float mouseX, float mouseY);
        void updateActionPosition(float mouseX, float mouseY);

        /**
        When enabled, all scale edits will increase every axis.
        */
        void setUniversalScaleModifier(bool scale);

        void notifyCameraMovement(const Ogre::Vector3& pos);
        void notifyMouseInteract(CoordsGizmoType interactedType);
        void focusToObject(EntryId id);

        bool editorEventReceiver(const Event &e);

        void populateAction(BasicCoordinatesChange* coordAction);

        CoordsGizmoType isItemCoordWidget(Ogre::MovableObject* object);

    private:
        Ogre::SceneManager* mSceneManager = 0;
        Ogre::SceneNode* mItemNode = 0;
        Ogre::SceneNode* mArrowContainerNode = 0;
        Ogre::RaySceneQuery* mSceneQuery = 0;

        EntryId mCurrentlyFocusedObject = 0;
        SelectionTool mCurrentSelectionTool = SelectionTool::ObjectPicker;

        Ogre::Item* mAttachedItems[3];
        Ogre::SceneNode* mArrowNodes[3];
        Ogre::HlmsUnlitDatablock* mArrowDatablocks[3];
        Ogre::SceneNode* mArrowLines[3];
        Ogre::Item* mBoundingBoxItem = 0;
        Ogre::SceneNode* mBoundingBox = 0;
        bool mBoundingBoxVisible = true;
        bool mGizmoVisible = true;

        Ogre::SceneNode* mScaleNodes[3];
        Ogre::HlmsUnlitDatablock* mScaleDatablocks[3];
        Ogre::Item* mScaleHandleItems[3];
        Ogre::SceneNode* mScaleHandleLines[3];

        Ogre::SceneNode* mOrientateNodes[3];
        Ogre::HlmsUnlitDatablock* mOrientateDatablocks[3];
        Ogre::Item* mOrientateHandleItems[3];

        Ogre::SceneNode* mTerrainBoxNode = 0;
        Ogre::Item* mTerrainBoxItem = 0;

        bool mUniversalScaleModifier = true;
        //Whether the current manipulation of the object was previously snapped to a value.
        bool mSnappedPreviously = false;

        bool mFirstMoveFrame = true;
        //Offset of where the mouse was clicked on the movement arrow. This helps reduce jitter when moving an object.
        float mMovementOffset = 0.0f;
        Ogre::SceneNode* mCurrentObject = 0;
        const ObjectData* mCurrentObjectData = 0;
        bool mGizmoObjectHovered = true;

        Ogre::Vector4 mCoordStart;
        //Store the position of the camera to determine widget size when needed.
        Ogre::Vector3 mStoredCameraPos;

        Ogre::Plane mCurrentCheckingPlane;

        CoordsGizmoType mCurrentlyInteractingType = CoordsGizmoType::none;

        void _setHighlightObject(CoordsGizmoType obj);

        void _notifyNewSelectionTool(SelectionTool tool);

        void _moveSelectedObject(const Ogre::Vector3& pos, bool gridSnapEnabled, float snapAmount);
        void _scaleSelectedObject(const Ogre::Vector3& scale, bool gridSnapEnabled, float snapAmount);
        Ogre::Vector3 _processSnapValue(const Ogre::Vector3& value, bool gridSnapEnabled, float snapAmount);

        void _updateGizmoSizeFromCamera();

        void _createCoordsMeshes();
        CoordsGizmoType _castRay(float mouseX, float mouseY);
        void _setGizmoVisible(bool visible);

        inline Ogre::Vector3 _floorVectorToSnap(const Ogre::Vector3& vec) const;

    };
}
