#include "EditorToolDialogManager.h"

#include "imgui.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"

namespace Southsea{

    bool popupRaycastObjects = false;
    std::vector<RaycastListEntry> EditorToolDialogManager::mStoredRaycastResults;

    EditorToolDialogManager::EditorToolDialogManager(){

    }

    EditorToolDialogManager::~EditorToolDialogManager(){

    }

    void EditorToolDialogManager::setRaycastPickedObjects(std::vector<RaycastListEntry>& outList){
        if(outList.empty()) return;
        mStoredRaycastResults = outList;
        popupRaycastObjects = true;
    }

    void EditorToolDialogManager::drawPopups(){
        if(popupRaycastObjects){
            ImGui::OpenPopup("raycastObjectSelect");
            popupRaycastObjects = false;
        }
        const bool escape = ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Escape));
        if(ImGui::BeginPopup("raycastObjectSelect")){
            if(escape)ImGui::CloseCurrentPopup();
            for(const RaycastListEntry& e : mStoredRaycastResults){
                if(ImGui::MenuItem(e.s.c_str())){
                    EditorSingleton::getEditor()->getSceneTreeManager()->setSelectionById(e.i);
                }
            }

            ImGui::EndPopup();
        }
    }
}
