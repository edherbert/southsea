#include "ActionStack.h"

#include "Action.h"
#include <iostream>

#include "Event/EventDispatcher.h"
#include "Event/Events/CommandEvent.h"
#include "Event/Events/EditorEvent.h"

namespace Southsea{

    const int ActionStack::MAX_ACTIONS = 30;

    ActionStack::ActionStack(){
        EventDispatcher::subscribe(EventType::Command, SOUTHSEA_BIND(ActionStack::commandEventReceiver));
        EventDispatcher::subscribe(EventType::Editor, SOUTHSEA_BIND(ActionStack::editorEventReceiver));
    }

    ActionStack::~ActionStack(){
        EventDispatcher::unsubscribe(EventType::Command, this);
        EventDispatcher::unsubscribe(EventType::Editor, this);
    }

    void ActionStack::undo(){
        if(mUndoStack.size() <= 0) return;

        mUndoStack.back()->performAntiAction();
        mRedoStack.push_back(mUndoStack.back());
        mUndoStack.pop_back();
    }

    void ActionStack::redo(){
        if(mRedoStack.size() <= 0) return;

        mRedoStack.back()->performAction();
        mUndoStack.push_back(mRedoStack.back());
        mRedoStack.pop_back();
    }

    void ActionStack::pushAction(Action* a){
        mUndoStack.push_back(a);

        _clearRedoStack();

        //Check if the undo stack now exceeds the max actions.
        if(mUndoStack.size() > MAX_ACTIONS){
            auto i = mUndoStack.begin();
            delete (*i);
            mUndoStack.erase(i); //Erase the first action if the list is too big. I know this would require a shift.
        }

        std::cout << "Action pushed" << std::endl;
    }

    bool ActionStack::commandEventReceiver(const Event &e){
        const CommandEvent& commandEvent = (const CommandEvent&)e;
        if(commandEvent.eventCategory() == CommandEventCategory::Undo){
            undo();
        }
        if(commandEvent.eventCategory() == CommandEventCategory::Redo){
            redo();
        }

        return false;
    }

    void ActionStack::_clearRedoStack(){
        for(Action* a : mRedoStack){
            delete a;
        }

        mRedoStack.clear();
    }

    void ActionStack::clearAllActions(){
        for(Action* a : mRedoStack) delete a;
        for(Action* a : mUndoStack) delete a;

        mUndoStack.clear();
        mRedoStack.clear();
    }

    bool ActionStack::editorEventReceiver(const Event &e){
        const EditorEvent& commandEvent = (const EditorEvent&)e;
        if(commandEvent.eventCategory() == EditorEventCategory::ChunkSwitch){
            clearAllActions();
        }

        return false;
    }
}
