#pragma once

#include "Editor/Action/Action.h"

#include "OgreVector3.h"
#include "OgreVector4.h"
#include "OgreQuaternion.h"
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class BasicCoordinatesChange : public Action{
    public:
        enum class Type{
            position,
            scale,
            orientate
        };

        BasicCoordinatesChange();
        ~BasicCoordinatesChange();

        void performAction();
        void performAntiAction();

        void populate(EntryId id, Ogre::Vector4 oldData, Ogre::Vector4 newData, Type type, bool absolute, ObjectType objectType);
        void populate(EntryId id, Ogre::Vector3 oldData, Ogre::Vector3 newData, Type type, bool absolute, ObjectType objectType);

    private:
        EntryId mId;
        Type mType;
        Ogre::Vector4 mOldData;
        Ogre::Vector4 mNewData;
        bool mAbsolute;
        ObjectType mObjectType;

        void _perform(const Ogre::Vector4& data);
    };
}
