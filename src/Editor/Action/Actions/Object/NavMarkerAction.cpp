#include "NavMarkerAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/NavMesh/NavMeshManager.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

namespace Southsea{

    NavMarkerAction::NavMarkerAction(EntryId id, Type type)
        : mId(id),
        mType(type)
        {

    }

    NavMarkerAction::~NavMarkerAction(){

    }

    void NavMarkerAction::performAction(){
        _perform(true);
    }

    void NavMarkerAction::performAntiAction(){
        _perform(false);
    }

    void NavMarkerAction::populateMarkerContribution(NavMeshId targetMesh, bool contribute){
        mTargetMesh = targetMesh;
        mBoolVal = contribute;
    }

    void NavMarkerAction::_perform(bool perform){
        bool target = mBoolVal;
        if(!perform) target = !target;
        EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager()->setMarkerContributesToMesh(mId, mTargetMesh, target);

        SceneTreeValueChangeEvent e;
        e.id = mId;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

}
