#include "DataPointAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "OgreSceneNode.h"

namespace Southsea{

    DataPointAction::DataPointAction(EntryId id, DataPointObjectData::Type type)
        : mId(id),
        mType(type)
        {

    }

    DataPointAction::~DataPointAction(){

    }

    void DataPointAction::performAction(){
        _perform(true);
    }

    void DataPointAction::performAntiAction(){
        _perform(false);
    }

    void DataPointAction::populate(int oldType, int newType){
        mOldType = oldType;
        mNewType = newType;
    }

    void DataPointAction::_perform(bool perform){
        Ogre::SceneNode* node = EditorSingleton::getEditor()->getOgreTreeManager()->getNodeById(mId);

        EditorSingleton::getEditor()->getTreeObjectManager()->changeDataPointType(node, mId, mType, perform ? mNewType : mOldType);

        SceneTreeValueChangeEvent e;
        e.id = mId;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

}
