#include "PhysicsCollisionAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "OgreSceneNode.h"

namespace Southsea{

    PhysicsCollisionAction::PhysicsCollisionAction(EntryId id)
        : mId(id) {

    }

    PhysicsCollisionAction::~PhysicsCollisionAction(){

    }

    void PhysicsCollisionAction::performAction(){
        _perform(true);
    }

    void PhysicsCollisionAction::performAntiAction(){
        _perform(false);
    }

    void PhysicsCollisionAction::populate(CollisionSenderObjectDataTypes type, CollisionSenderObjectData* old, CollisionSenderObjectData* current){
        mType = type;

        switch(mType){
            case CollisionSenderObjectDataTypes::shape:{
                newData.i = (int)current->shapeType;
                oldData.i = (int)old->shapeType;
                break;
            }
            case CollisionSenderObjectDataTypes::target:{
                int changedIdx = _findChangedItem(8, &(current->targetValues[0]), &(old->targetValues[0]));
                assert(changedIdx >= 0);
                newData = {changedIdx, current->targetValues[changedIdx], ResourceFSEntry()};
                oldData = {changedIdx, old->targetValues[changedIdx], ResourceFSEntry()};
                assert(newData.b == !oldData.b);
                break;
            }
            case CollisionSenderObjectDataTypes::event:{
                int changedIdx = _findChangedItem(3, &(current->eventValues[0]), &(old->eventValues[0]));
                assert(changedIdx >= 0);
                newData = {changedIdx, current->eventValues[changedIdx], ResourceFSEntry()};
                oldData = {changedIdx, old->eventValues[changedIdx], ResourceFSEntry()};
                assert(newData.b == !oldData.b);
                break;
            }
            case CollisionSenderObjectDataTypes::id:{
                //The boolean isn't used.
                newData = {current->id, false, ResourceFSEntry()};
                oldData = {old->id, false, ResourceFSEntry()};
                break;
            }
            case CollisionSenderObjectDataTypes::script:{
                newData = {0, false, current->script};
                oldData = {0, false, old->script};
                break;
            }
            case CollisionSenderObjectDataTypes::closure:{
                newData = {0, false, current->closureName};
                oldData = {0, false, old->closureName};
                break;
            }
            default:
                assert(false);
                break;
        }
    }

    int PhysicsCollisionAction::_findChangedItem(size_t arraySize, bool* newArrayPtr, bool* oldArrayPtr){
        for(size_t i = 0; i < arraySize; i++){
            if(*(newArrayPtr+i) != *(oldArrayPtr+i)){
                return i;
            }
        }
        return -1;
    }

    void PhysicsCollisionAction::_perform(bool perform){

        CollisionSenderObjectData* d = EditorSingleton::getEditor()->getTreeObjectManager()->getSenderObjectData(mId);

        switch(mType){
            case CollisionSenderObjectDataTypes::shape:{
                Ogre::SceneNode* node = EditorSingleton::getEditor()->getOgreTreeManager()->getNodeById(mId);
                assert(node);
                EditorSingleton::getEditor()->getTreeObjectManager()->changePhysicsShape(node, mId, static_cast<PhysicsShapeType>(perform ? newData.i : oldData.i));

                break;
            }
            case CollisionSenderObjectDataTypes::target:{
                d->targetValues[newData.i] = perform ? newData.b : oldData.b;

                break;
            }
            case CollisionSenderObjectDataTypes::event:{
                d->eventValues[newData.i] = perform ? newData.b : oldData.b;
                break;
            }
            case CollisionSenderObjectDataTypes::id:{
                d->id = perform ? newData.i : oldData.i;
                break;
            }
            case CollisionSenderObjectDataTypes::script:{
                d->script = perform ? newData.r : oldData.r;
                break;
            }
            case CollisionSenderObjectDataTypes::closure:{
                d->closureName = perform ? newData.r.name : oldData.r.name;
                break;
            }
            default:
                assert(false);
                break;
        }

        SceneTreeValueChangeEvent e;
        e.id = mId;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

}
