#pragma once

#include "Editor/Action/Action.h"

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class PhysicsCollisionAction : public Action{
    public:

        PhysicsCollisionAction(EntryId id);
        ~PhysicsCollisionAction();

        void performAction();
        void performAntiAction();

        void populate(CollisionSenderObjectDataTypes type, CollisionSenderObjectData* oldData, CollisionSenderObjectData* newData);

    private:
        EntryId mId;
        CollisionSenderObjectDataTypes mType;

        void _perform(bool perform);
        //Find the difference index from two arrays of size arraySize.
        int _findChangedItem(size_t arraySize, bool* newArrayPtr, bool* oldArrayPtr);

        struct ActionData{
            int i;
            bool b;
            ResourceFSEntry r;
        };

        ActionData newData;
        ActionData oldData;
    };
}
