#pragma once

#include "Editor/Action/Action.h"

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class DataPointAction : public Action{
    public:
        DataPointAction(EntryId id, DataPointObjectData::Type type);
        ~DataPointAction();

        void performAction();
        void performAntiAction();

        void populate(int oldType, int newType);

    private:
        EntryId mId;
        DataPointObjectData::Type mType;

        int mOldType;
        int mNewType;

        void _perform(bool perform);
    };
}
