#pragma once

#include "Editor/Action/Action.h"

#include "OgreVector3.h"
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class MeshObjectAction : public Action{
    public:
        enum class Type{
            meshChange,
            materialChange
        };

        MeshObjectAction(EntryId id, Type type);
        ~MeshObjectAction();

        void performAction();
        void performAntiAction();

        void populate(const std::string& oldStr, const std::string& newStr);

    private:
        EntryId mId;
        Type mType;

        std::string mOldStr;
        std::string mNewStr;

        void _performAction(bool perform);
    };
}
