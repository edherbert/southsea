#include "MeshObjectAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "OgreSceneNode.h"

#include "Editor/Scene/TreeObjectManager.h"

namespace Southsea{
    MeshObjectAction::MeshObjectAction(EntryId id, Type type)
    : mId(id),
      mType(type),
      Action(){

    }

    MeshObjectAction::~MeshObjectAction(){

    }

    void MeshObjectAction::performAction(){
        _performAction(true);
    }

    void MeshObjectAction::performAntiAction(){
        _performAction(false);
    }

    void MeshObjectAction::populate(const std::string& oldStr, const std::string& newStr){
        mOldStr = oldStr;
        mNewStr = newStr;
    }

    void MeshObjectAction::_performAction(bool perform){
        Ogre::SceneNode* node = EditorSingleton::getEditor()->getOgreTreeManager()->getNodeById(mId);

        if(mType == Type::meshChange){
            EditorSingleton::getEditor()->getTreeObjectManager()->changeOgreMesh(node, mId, perform ? mNewStr : mOldStr);
        }else if(mType == Type::materialChange){
            EditorSingleton::getEditor()->getTreeObjectManager()->changeOgreMeshMaterial(node, mId, perform ? mNewStr : mOldStr);
        }

        SceneTreeValueChangeEvent e;
        e.id = mId;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }
}
