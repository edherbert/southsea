#include "BasicCoordinatesChange.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/PhysicsBodyDrawer.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "OgreSceneNode.h"

namespace Southsea{
    BasicCoordinatesChange::BasicCoordinatesChange() : Action(){

    }

    BasicCoordinatesChange::~BasicCoordinatesChange(){

    }

    void BasicCoordinatesChange::performAction(){
        _perform(mNewData);
    }

    void BasicCoordinatesChange::performAntiAction(){
        _perform(mOldData);
    }

    void BasicCoordinatesChange::_perform(const Ogre::Vector4& data){
        Ogre::SceneNode* node = EditorSingleton::getEditor()->getOgreTreeManager()->getNodeById(mId);
        Ogre::Vector3 vec3Data(data.x, data.y, data.z);
        switch(mType){
            case Type::position:{
                if(mAbsolute) node->_setDerivedPosition(vec3Data);
                else node->setPosition(vec3Data);
                break;
            }
            case Type::scale:{
                node->setScale(vec3Data);
                break;
            }
            case Type::orientate:{
                node->setOrientation(Ogre::Quaternion(data.w, data.x, data.y, data.z));
                break;
            }
            default: assert(false); break;
        }

        SceneTreeValueChangeEvent e;
        e.id = mId;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void BasicCoordinatesChange::populate(EntryId id, Ogre::Vector3 oldData, Ogre::Vector3 newData, Type type, bool absolute, ObjectType objectType){
        populate(id, Ogre::Vector4(oldData), Ogre::Vector4(newData), type, absolute, objectType);
    }

    void BasicCoordinatesChange::populate(EntryId id, Ogre::Vector4 oldData, Ogre::Vector4 newData, Type type, bool absolute, ObjectType objectType){
        mId = id;
        mOldData = oldData;
        mNewData = newData;
        mType = type;
        mAbsolute = absolute;
        mObjectType = objectType;
    }
}
