#include "PhysicsShapeAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include "OgreSceneNode.h"

namespace Southsea{

    PhysicsShapeAction::PhysicsShapeAction(EntryId id, Type type)
        : mId(id),
        mType(type)
        {

    }

    PhysicsShapeAction::~PhysicsShapeAction(){

    }

    void PhysicsShapeAction::performAction(){
        _perform(true);
    }

    void PhysicsShapeAction::performAntiAction(){
        _perform(false);
    }

    void PhysicsShapeAction::populate(PhysicsShapeType oldType, PhysicsShapeType newType){
        mOldType = oldType;
        mNewType = newType;
    }

    void PhysicsShapeAction::_perform(bool perform){
        Ogre::SceneNode* node = EditorSingleton::getEditor()->getOgreTreeManager()->getNodeById(mId);

        EditorSingleton::getEditor()->getTreeObjectManager()->changePhysicsShape(node, mId, perform ? mNewType : mOldType);

        SceneTreeValueChangeEvent e;
        e.id = mId;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

}
