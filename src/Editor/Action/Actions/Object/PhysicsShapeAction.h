#pragma once

#include "Editor/Action/Action.h"

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class PhysicsShapeAction : public Action{
    public:
        enum class Type{
            shapeChange
        };

        PhysicsShapeAction(EntryId id, Type type);
        ~PhysicsShapeAction();

        void performAction();
        void performAntiAction();

        void populate(PhysicsShapeType oldType, PhysicsShapeType newType);

    private:
        EntryId mId;
        Type mType;

        PhysicsShapeType mOldType;
        PhysicsShapeType mNewType;

        void _perform(bool perform);
    };
}
