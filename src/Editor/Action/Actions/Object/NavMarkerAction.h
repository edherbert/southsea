#pragma once

#include "Editor/Action/Action.h"

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class NavMarkerAction : public Action{
    public:
        enum class Type{
            contributionChange
        };

        NavMarkerAction(EntryId id, Type type);
        ~NavMarkerAction();

        void performAction();
        void performAntiAction();

        void populateMarkerContribution(NavMeshId targetMesh, bool contribute);

    private:
        EntryId mId;
        Type mType;
        bool mBoolVal;
        NavMeshId mTargetMesh;

        void _perform(bool perform);
    };
}
