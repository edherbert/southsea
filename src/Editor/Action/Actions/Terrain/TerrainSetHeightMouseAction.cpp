#include "TerrainSetHeightMouseAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

namespace Southsea{
    TerrainSetHeightMouseAction::TerrainSetHeightMouseAction(){

    }

    TerrainSetHeightMouseAction::~TerrainSetHeightMouseAction(){

    }

    void TerrainSetHeightMouseAction::performAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applySetHeightMouseAction(this, true);
    }

    void TerrainSetHeightMouseAction::performAntiAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applySetHeightMouseAction(this, false);
    }
};
