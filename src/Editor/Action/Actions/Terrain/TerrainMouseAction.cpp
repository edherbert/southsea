#include "TerrainMouseAction.h"

namespace Southsea{

    void TerrainMouseAction::addData(uint32 posId, uint16 newVal, uint16 oldVal){
        auto it = m_commandData16.find(posId);
        if(it != m_commandData16.end()){
            //The value was already in the map.
            //In this case we only need to update the new val.
            it->second.first = newVal;
            return;
        }

        //If it's never been inserted before do that now.
        m_commandData16[posId] = {newVal, oldVal};
    }

    void TerrainMouseAction::addData(uint32 posId, uint32 newVal, uint32 oldVal){
        auto it = m_commandData32.find(posId);
        if(it != m_commandData32.end()){
            //The value was already in the map.
            //In this case we only need to update the new val.
            it->second.first = newVal;
            return;
        }

        //If it's never been inserted before do that now.
        m_commandData32[posId] = {newVal, oldVal};
    }

}
