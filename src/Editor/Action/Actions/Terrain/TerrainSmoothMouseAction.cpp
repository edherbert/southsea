#include "TerrainSmoothMouseAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

namespace Southsea{
    TerrainSmoothMouseAction::TerrainSmoothMouseAction(){

    }

    TerrainSmoothMouseAction::~TerrainSmoothMouseAction(){

    }

    void TerrainSmoothMouseAction::performAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applySmoothMouseAction(this, true);
    }

    void TerrainSmoothMouseAction::performAntiAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applySmoothMouseAction(this, false);
    }
};
