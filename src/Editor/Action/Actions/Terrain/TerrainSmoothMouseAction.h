#pragma once

#include "TerrainMouseAction.h"

namespace Southsea{

    class TerrainSmoothMouseAction : public TerrainMouseAction{
    public:
        TerrainSmoothMouseAction();
        ~TerrainSmoothMouseAction();

        void performAction();
        void performAntiAction();

        ActionType getActionType() { return SMOOTH; }

    };
}
