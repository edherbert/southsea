#pragma once

#include "Editor/Action/Action.h"

#include <vector>

namespace Southsea{
    typedef unsigned short uint16;
    typedef unsigned int uint32;

    class LevelTerrainAction : public Action{
    public:
        LevelTerrainAction();
        ~LevelTerrainAction();

        void performAction();
        void performAntiAction();

        const std::vector<uint16>& getPreviousData() { return m_previousData; }

        void setLevelHeight(uint16 height) { mLevelHeight = height; }
        void fillData(uint16 width, uint16 height, uint16* data);

    private:
        std::vector<uint16> m_previousData;
        float mLevelHeight;
    };
}
