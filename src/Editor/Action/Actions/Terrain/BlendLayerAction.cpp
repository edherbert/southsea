#include "BlendLayerAction.h"

#include "Editor/Chunk/Terrain/TerrainDatablockUtils.h"
#include "Editor/Chunk/Terrain/TerrainDatablockInfoManager.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"

#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraDatablock.h"

#include <algorithm>

namespace Southsea{
    BlendLayerAction::BlendLayerAction(ActionType type, Ogre::HlmsTerraDatablock* db, int layer)
        : Action(),
        mType(type),
        datablock(db),
        dbLayer(layer),

        vecVal(Ogre::Vector4::ZERO),
        secondVec(Ogre::Vector4::ZERO){

    }

    BlendLayerAction::~BlendLayerAction(){

    }

    void BlendLayerAction::performAction(){
        _performAction(true);

        TerrainDatablockChange e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void BlendLayerAction::performAntiAction(){
        _performAction(false);

        TerrainDatablockChange e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void BlendLayerAction::_performAction(bool perform){
        switch(mType){
            case ENABLE_DISABLE: {
                bool first = vecVal != Ogre::Vector4::ZERO;
                if(!perform) first = !first;
                _performEnableDisable(first);
                break;
            }
            case OFFSET_SCALE: {
                _performOffsetScale(perform);
                break;
            }
            case METALNESS:
            case ROUGHNESS : {
                _performMetalnessRoughness(perform);
                break;
            }
            case DIFFUSE: {
                _performDiffuse(perform);
                break;
            }
        }
    }


    void BlendLayerAction::setEnableDisable(bool enable){
        assert(mType == ENABLE_DISABLE);

        //The vector is used to represent enabled or disabled.
        vecVal = enable ? Ogre::Vector4(1, 1, 1, 1) : Ogre::Vector4::ZERO;

    }

    void BlendLayerAction::setOffsetScale(const Ogre::Vector4& oldVec, const Ogre::Vector4& newVec){
        assert(mType == OFFSET_SCALE);

        vecVal = newVec;
        secondVec = oldVec;
    }

    void BlendLayerAction::setFloatValue(float oldVal, float newVal){
        assert(mType == METALNESS || mType == ROUGHNESS);

        float targetOld = std::max(0.0f, std::min(oldVal, 1.0f));
        float targetNew = std::max(0.0f, std::min(newVal, 1.0f));

        vecVal.x = targetNew;
        secondVec.x = targetOld;
    }

    void BlendLayerAction::setDiffuse(Ogre::Vector3 oldVal, Ogre::Vector3 newVal){
        assert(mType == DIFFUSE);

        for(int i = 0; i < 3; i++){
            vecVal[i] = oldVal[i];
            newVal[i] = secondVec[i];
        }
    }

    void BlendLayerAction::_performOffsetScale(bool perform){
        Ogre::Vector4* target;
        if(perform)
            target = &vecVal;
        else
            target = &secondVec;

        datablock->setDetailMapOffsetScale(dbLayer, *target);
        TerrainDatablockInfoManager::setLayerOffsetScale(*datablock->getNameStr(), dbLayer, *target);
    }

    void BlendLayerAction::_performEnableDisable(bool enable){
        if(enable){
            const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*datablock->getNameStr());

            TerrainDatablockUtils::setDatablockLayer(datablock, dbLayer, data.layers[dbLayer]);
        }else{
            TerrainDatablockUtils::resetDatablockLayer(datablock, dbLayer);
        }

        TerrainDatablockInfoManager::setLayerEnableDisable(*datablock->getNameStr(), dbLayer, enable);
    }

    void BlendLayerAction::_performMetalnessRoughness(bool perform){

        Ogre::Vector4* target;
        if(perform)
            target = &vecVal;
        else
            target = &secondVec;

        if(mType == METALNESS){
            datablock->setMetalness(dbLayer, (*target).x);
            TerrainDatablockInfoManager::setLayerMetalness(*datablock->getNameStr(), dbLayer, (*target).x);
        }else{
            datablock->setRoughness(dbLayer, (*target).x);
            TerrainDatablockInfoManager::setLayerRoughness(*datablock->getNameStr(), dbLayer, (*target).x);
        }
    }

    void BlendLayerAction::_performDiffuse(bool perform){
        Ogre::Vector4* target;
        if(perform)
            target = &vecVal;
        else
            target = &secondVec;

        const Ogre::Vector3 s((*target)[0], (*target)[1], (*target)[2]);

        datablock->setDiffuse(s);
        TerrainDatablockInfoManager::setDiffuse(*datablock->getNameStr(), s);
    }
};
