#pragma once

#include "Editor/Action/Action.h"

#include "OgreString.h"
#include "OgreVector4.h"

namespace Ogre{
    class HlmsTerraDatablock;
}

namespace Southsea{
    class BlendLayerAction : public Action{
    public:
        enum ActionType{
            ENABLE_DISABLE,
            OFFSET_SCALE,
            METALNESS,
            ROUGHNESS,
            DIFFUSE //It's not a layer action, but at least this way I don't have to create another action.
        };

    public:
        BlendLayerAction(ActionType type, Ogre::HlmsTerraDatablock* db, int layer);
        ~BlendLayerAction();

        void performAction();
        void performAntiAction();

        void setEnableDisable(bool enable);
        void setOffsetScale(const Ogre::Vector4& oldVec, const Ogre::Vector4& newVec);
        void setFloatValue(float oldVal, float newVal);
        void setDiffuse(Ogre::Vector3 oldVal, Ogre::Vector3 newVal);

    private:
        ActionType mType;
        Ogre::HlmsTerraDatablock* datablock;

        int dbLayer;

        Ogre::Vector4 vecVal, secondVec;

        void _performAction(bool perform);

        void _performEnableDisable(bool enable);
        void _performOffsetScale(bool perform);
        void _performMetalnessRoughness(bool perform);
        void _performDiffuse(bool perform);
    };
}
