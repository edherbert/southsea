#pragma once

#include "TerrainMouseAction.h"

namespace Southsea{

    class TerrainSetHeightMouseAction : public TerrainMouseAction{
    public:
        TerrainSetHeightMouseAction();
        ~TerrainSetHeightMouseAction();

        void performAction();
        void performAntiAction();

        ActionType getActionType() { return SET_HEIGHT; }

    };
}
