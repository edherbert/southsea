#pragma once

#include "TerrainMouseAction.h"

namespace Southsea{

    class TerrainHeightMouseAction : public TerrainMouseAction{
    public:
        TerrainHeightMouseAction();
        ~TerrainHeightMouseAction();

        void performAction();
        void performAntiAction();

        ActionType getActionType() { return HEIGHT; }

    };
}
