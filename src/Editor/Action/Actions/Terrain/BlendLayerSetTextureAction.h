#pragma once

#include "Editor/Action/Action.h"

#include "OgreString.h"

namespace Ogre{
    class HlmsTerraDatablock;
}

namespace Southsea{
    class BlendLayerSetTextureAction : public Action{
    public:
        BlendLayerSetTextureAction(Ogre::HlmsTerraDatablock* db, int layer, const Ogre::String& oldName, const Ogre::String& newName);
        ~BlendLayerSetTextureAction();

        void performAction();
        void performAntiAction();

    private:
        Ogre::String oldTexName;
        Ogre::String newTexName;

        Ogre::HlmsTerraDatablock* datablock;

        int texLayer;
    };
}
