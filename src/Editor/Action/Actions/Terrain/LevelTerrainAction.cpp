#include "LevelTerrainAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

namespace Southsea{
    LevelTerrainAction::LevelTerrainAction() : Action(){

    }

    LevelTerrainAction::~LevelTerrainAction(){

    }

    void LevelTerrainAction::performAction(){
        EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager()->levelTerrain(mLevelHeight);
    }

    void LevelTerrainAction::performAntiAction(){
        EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager()->levelToData(m_previousData);
    }

    void LevelTerrainAction::fillData(uint16 width, uint16 height, uint16* data){
        m_previousData.clear();
        m_previousData.resize(width * height);

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                m_previousData[x + y * width] = (*data++);
            }
        }
    }
};
