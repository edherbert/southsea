#include "TerrainHeightMouseAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

namespace Southsea{
    TerrainHeightMouseAction::TerrainHeightMouseAction(){

    }

    TerrainHeightMouseAction::~TerrainHeightMouseAction(){

    }

    void TerrainHeightMouseAction::performAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applyHeightMouseAction(this, true);
    }

    void TerrainHeightMouseAction::performAntiAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applyHeightMouseAction(this, false);
    }
};
