#pragma once

#include "TerrainMouseAction.h"

namespace Southsea{

    class TerrainBlendMouseAction : public TerrainMouseAction{
    public:
        TerrainBlendMouseAction();
        ~TerrainBlendMouseAction();

        void performAction();
        void performAntiAction();

        ActionType getActionType() { return BLEND; }

    };
}
