#pragma once

#include "Editor/Action/Action.h"
#include <cstdint>

#include <map>

namespace Southsea{
    typedef unsigned short uint16;
    typedef unsigned int uint32;

    class TerrainMouseAction : public Action{
    public:

        TerrainMouseAction() {};
        ~TerrainMouseAction() {};

        enum ActionType{
            NONE,
            HEIGHT,
            BLEND,
            SMOOTH,
            SET_HEIGHT
        };

        virtual ActionType getActionType() { return NONE; }

        virtual void performAction() { }
        virtual void performAntiAction() { }

        void addData(uint32 posId, uint16 newVal, uint16 oldVal);
        void addData(uint32 posId, uint32 newVal, uint32 oldVal);

        const std::map<uint32, std::pair<uint16,uint16>>& getDataMap16() { return m_commandData16; }
        const std::map<uint32, std::pair<uint32,uint32>>& getDataMap32() { return m_commandData32; }

        void addEffectedTerrains(uint16_t terrains) { mEffectedTerrains |= terrains; }
        uint16_t getEffectedTerrains() const { return mEffectedTerrains; }

    private:
        //id, new, old
        std::map<uint32, std::pair<uint16,uint16>> m_commandData16;
        std::map<uint32, std::pair<uint32,uint32>> m_commandData32;

        uint16_t mEffectedTerrains;
    };
}
