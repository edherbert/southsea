#include "BlendLayerSetTextureAction.h"

#include "Editor/Chunk/Terrain/TerrainDatablockUtils.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"

namespace Southsea{
    BlendLayerSetTextureAction::BlendLayerSetTextureAction(Ogre::HlmsTerraDatablock* db, int layer, const Ogre::String& oldName, const Ogre::String& newName)
        : Action(),
        datablock(db),
        texLayer(layer),
        oldTexName(oldName),
        newTexName(newName){

    }

    BlendLayerSetTextureAction::~BlendLayerSetTextureAction(){

    }

    void BlendLayerSetTextureAction::performAction(){
        TerrainDatablockUtils::setTextureToLayer(datablock, newTexName, texLayer);

        TerrainDatablockChange e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void BlendLayerSetTextureAction::performAntiAction(){
        TerrainDatablockUtils::setTextureToLayer(datablock, oldTexName, texLayer);

        TerrainDatablockChange e;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }
};
