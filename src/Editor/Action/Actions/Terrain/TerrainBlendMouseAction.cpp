#include "TerrainBlendMouseAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

namespace Southsea{
    TerrainBlendMouseAction::TerrainBlendMouseAction(){

    }

    TerrainBlendMouseAction::~TerrainBlendMouseAction(){

    }

    void TerrainBlendMouseAction::performAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applyBlendMouseAction(this, true);
    }

    void TerrainBlendMouseAction::performAntiAction(){
        EditorSingleton::getEditor()->mEditorTerrainManager.applyBlendMouseAction(this, false);
    }
};
