#pragma once

#include "Editor/Action/Action.h"

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class ChangeVisibilityAction : public Action{
    public:
        ChangeVisibilityAction(EntryId id, bool nowVisible);
        ~ChangeVisibilityAction();

        void performAction();
        void performAntiAction();

    private:
        EntryId mId;
        bool mNowVisible;
    };
}
