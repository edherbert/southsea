#include "ObjectInsertionAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"

#include "Editor/Scene/SceneTypeHelper.h"
#include "OgreSceneNode.h"

#include <cassert>

namespace Southsea{
    static const OgreTreeManager::TreeNodeCreationData defaultCreationData = {Ogre::Vector3::ZERO, Ogre::Vector3(1, 1, 1), Ogre::Quaternion::IDENTITY};

    ObjectInsertionAction::ObjectInsertionAction(EntryId id, ObjectInsertionType type)
    : mId(id),
      mType(type),
      resultId(INVALID_ID),
      resultIndex(-1),
      mInsertPos(Ogre::Vector3::ZERO),
      Action(){

    }

    ObjectInsertionAction::~ObjectInsertionAction(){

    }

    void ObjectInsertionAction::performAction(){
        SceneTreeManager::ObjectInsertData data{mEntryName, mSceneType};

        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto result = sceneMan->insertObjectIntoTree(mId, mType, data);
        resultId = result.first;
        resultIndex = result.second;
        EntryId parentId = sceneMan->getIdOfParent(resultIndex);

        Ogre::SceneNode* newNode = EditorSingleton::getEditor()->getOgreTreeManager()->createTreeItem(resultId, parentId, &defaultCreationData);
        newNode->_setDerivedPosition(mInsertPos);
        EditorSingleton::getEditor()->getTreeObjectManager()->insertObject(newNode, resultId, mObjectData);
    }

    void ObjectInsertionAction::performAntiAction(){
        assert(resultIndex >= 0);
        EditorSingleton::getEditor()->getSceneTreeManager()->deleteObjectFromTree(resultId, resultIndex);

        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        Ogre::SceneNode* targetNode = ogreMan->getNodeById(resultId);
        assert(targetNode);

        //EditorSingleton::getEditor()->getTreeObjectManager()->removeObject(targetNode, resultId, mObjectData);
        EditorSingleton::getEditor()->getTreeObjectManager()->removeObject(targetNode, resultId);
        ogreMan->destroyTreeItem(resultId);
        //TODO this crashes with the insert into action which causes terminator creation.

        SceneTreeItemDeleted a;
        a.id = resultId;
        EventDispatcher::transmitEvent(EventType::Editor, a);

        resultId = INVALID_ID;
        resultIndex = -1;
    }

    void ObjectInsertionAction::populate(){
        mInsertionType = empty;
        mObjectData = 0;

        mEntryName = "empty";
        mSceneType = SceneEntryType::empty;
    }

    void ObjectInsertionAction::populate(const Resource& resource, const ResourceFSEntry& fsEntry, const Ogre::Vector3& pos){
        mInsertPos = pos;
        mObjectData = EditorSingleton::getEditor()->getTreeObjectManager()->constructObjectDataFromResource(resource, fsEntry);

        mSceneType = _convertSceneTypeTo(fsEntry.type);
        mEntryName = fsEntry.name.substr(0, fsEntry.name.find(".")); //Strip off any file endings, i.e thing.mesh -> thing

        mInsertionType = mesh;
    }

    void ObjectInsertionAction::populate(PhysicsShapeType type){
        mObjectData = EditorSingleton::getEditor()->getTreeObjectManager()->constructPhysicsData(type);

        mSceneType = SceneEntryType::physicsShape;

        switch(type){
            case PhysicsShapeType::box: mEntryName = "Box"; break;
            case PhysicsShapeType::sphere: mEntryName = "Sphere"; break;
            default: mEntryName = "Physics shape"; break;
        };

        mInsertionType = physicsShape;
    }

    void ObjectInsertionAction::populateCollisionSender(){
        mObjectData = EditorSingleton::getEditor()->getTreeObjectManager()->constructCollisionSenderData();

        mSceneType = SceneEntryType::collisionSender;
        mEntryName = "Collision Sender";

        mInsertionType = collisionSender;
    }

    void ObjectInsertionAction::populateNavMeshMarker(){
        mObjectData = EditorSingleton::getEditor()->getTreeObjectManager()->constructNavMarkerData();

        mSceneType = SceneEntryType::navMarker;
        mEntryName = "Nav mesh marker";

        mInsertionType = navMarker;
    }

    void ObjectInsertionAction::populateDataPoint(){
        mObjectData = EditorSingleton::getEditor()->getTreeObjectManager()->constructDataPointData();

        mSceneType = SceneEntryType::dataPoint;
        mEntryName = "Data point";

        mInsertionType = dataPoint;
    }

}
