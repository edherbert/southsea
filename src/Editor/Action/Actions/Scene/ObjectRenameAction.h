#pragma once

#include "Editor/Action/Action.h"

#include <string>
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class ObjectRenameAction : public Action{
    public:
        ObjectRenameAction(EntryId id, const std::string& oldName, const std::string& newName);
        ~ObjectRenameAction();

        void performAction();
        void performAntiAction();

    private:
        EntryId mId;
        std::string mOldName, mNewName;
    };
}
