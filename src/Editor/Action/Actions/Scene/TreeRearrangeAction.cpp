#include "TreeRearrangeAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"

#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include <cassert>

namespace Southsea{
    TreeRearrangeAction::TreeRearrangeAction()
    : Action(){

    }

    TreeRearrangeAction::~TreeRearrangeAction(){

    }

    void TreeRearrangeAction::performAction(){
        assert(populated);
        mEntryIndexes.clear();
        mEntryIndexes.reserve(mSelectedEntries.size());

        auto treeMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        for(EntryId id : mSelectedEntries){
            int idx = treeMan->moveEntry(id, mDestination, mType);
            ogreMan->reparentTreeItem(id, treeMan->getTrueDestination(mDestination, mType));

            mEntryIndexes.push_back(idx);
        }

        _notifyChange();
    }

    void TreeRearrangeAction::performAntiAction(){
        assert(populated);
        auto treeMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();

        assert(mSelectedEntries.size() == mEntryIndexes.size());

        //We have to iterate the list backwards when performing the anti action. That way the indexes will match up correctly.
        auto it = mSelectedEntries.rbegin();
        auto destIndex = mEntryIndexes.rbegin();
        while(it != mSelectedEntries.rend()){
            treeMan->moveEntry(*it, 0, ObjectInsertionType::above, *destIndex);
            ogreMan->reparentTreeItem(*it, treeMan->getIdOfParent(*destIndex));

            destIndex++;
            it++;
        }

        _notifyChange();
    }

    void TreeRearrangeAction::populate(const std::set<EntryId>& selectedEntries, EntryId destination, ObjectInsertionType type){
        assert(!populated);

        //I push them into a vector here so that I have continuous entries.
        //I had a sneaking suspician that iterating the set backwards later on might mess up the order.
        //Might not, but I'm sure it won't in a vector.
        mSelectedEntries.reserve(selectedEntries.size());
        for(EntryId i : selectedEntries){
            mSelectedEntries.push_back(i);
        }
        mDestination = destination;
        mType = type;

        populated = true;
    }

    void TreeRearrangeAction::_notifyChange(){
        //Right now it only notifies the first one. Multi selection with the highlight system still needs to be figured out.
        if(mSelectedEntries.size() > 0){
            SceneTreeValueChangeEvent e;
            e.id = mSelectedEntries[0];
            EventDispatcher::transmitEvent(EventType::Editor, e);
        }
    }
}
