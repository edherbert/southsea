#include "ScenePasteAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"

namespace Southsea{
    ScenePasteAction::ScenePasteAction()
        : mPopulated(false),
        mDestination(INVALID_ID) {

    }

    ScenePasteAction::ScenePasteAction(EntryId id, ObjectInsertionType type, const Ogre::Vector3& targetPos)
        : mType(type),
        mPopulated(false),
        mDestination(id),
        mTargetPos(targetPos)
    {

    }


    ScenePasteAction::~ScenePasteAction(){
        for(ObjectData* d : mObjectData){
            delete d;
        }
    }

    void ScenePasteAction::performAction(){
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();

        for(const CopiedSelectionInfo& info : mEntrySectionList){
            sceneMan->insertVectorItemsIntoItem(mCopiedSceneEntries, mDestination, info.start, info.end);

            //Populate the ogre tree with the new items.
            ogreMan->repopulateTree(mCopiedSceneEntries, mObjectData, mTreeCreationData, info.start, info.end, info.objStart, info.objEnd, mDestination, treeMan.get());
        }
    }

    void ScenePasteAction::performAntiAction(){
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();

        auto it = mEntrySectionList.rbegin();
        while(it != mEntrySectionList.rend()){
            const CopiedSelectionInfo& i = *it;

            //Find the first entry in each section and delete it.
            //Only the first one, as any subsequent ones in the same block will be children, which are deleted as part of it.
            //The ids should match up if set correctly previously.
            EntryId targetId = mCopiedSceneEntries[i.start].id;
            sceneMan->deleteObjectFromTree(targetId);
            ogreMan->destroyTreeItem(targetId, treeMan.get());

            it++;
        }
    }

    void ScenePasteAction::populateFromSceneFileInterface(SceneFileSceneTreeParserInterface& interface){
        assert(!mPopulated);
        mPopulated = true;

        mCopiedSceneEntries = interface.mCopiedSceneEntries;
        mEntrySectionList = interface.mEntrySectionList;
        mTreeCreationData = interface.mTreeCreationData;
        //No need to clone the data as I do in the regular populate as this object data isn't being used for anything else.
        mObjectData = interface.mObjectData;

        EntryId parentNode;
        mEntrySectionList.push_back({0, 0, static_cast<int>(mCopiedSceneEntries.size()), 0, static_cast<int>(mObjectData.size())});
        assert(interface.mPrevParent.size() == 1);

        mTreeCreationData[0].position = mTargetPos;
    }

    void ScenePasteAction::populate(
        EntryId destination,
        const std::vector<SceneEntry>& copiedSceneEntries,
        const std::vector<CopiedSelectionInfo>& entrySectionList,
        const std::vector<ObjectData*>& objectData,
        const std::vector<OgreTreeManager::TreeNodeCreationData>& treeCreationData
    ){
        assert(!mPopulated);
        mPopulated = true;
        mDestination = destination;
        mCopiedSceneEntries = copiedSceneEntries;
        mEntrySectionList = entrySectionList;
        mTreeCreationData = treeCreationData;

        //Clone and insert the object data.
        mObjectData.reserve(objectData.size());
        for(ObjectData* d : objectData){
            ObjectData* newData = OgreTreeManager::cloneObjectData(d);
            mObjectData.push_back(newData);
        }
    }

}
