#pragma once

#include "ObjectInsertionType.h"
#include "Editor/Action/Action.h"
#include "Editor/Scene/SceneTypes.h"
#include "Editor/Scene/OgreTreeManager.h"

#include <vector>
#include <set>

namespace Southsea{


    class ObjectDeleteAction : public Action{
    public:
        ObjectDeleteAction();
        ~ObjectDeleteAction();

        void performAction();
        void performAntiAction();

        void populate(const std::set<EntryId>& selectedEntries);

    private:
        //typedef std::pair<int, int> DeletedSelectionInfo;
        struct DeletedSelectionInfo{
            int idx; //The index in the scene tree list, not the deleted scene list.
            int start;
            int end;
            bool deletedTerminators;

            int objStart;
            int objEnd;
            EntryId parentNode;
        };

        std::vector<EntryId> mSelectedEntries;
        std::vector<SceneEntry> mDeletedSceneEntries;
        std::vector<DeletedSelectionInfo> mEntrySectionList;
        //ogre objects
        std::vector<ObjectData*> mObjectData;
        std::vector<OgreTreeManager::TreeNodeCreationData> mTreeCreationData;
    };
}
