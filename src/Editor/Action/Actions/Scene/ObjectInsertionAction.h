#pragma once

#include "ObjectInsertionType.h"
#include "Editor/Action/Action.h"
#include "Editor/Scene/SceneTypes.h"
#include "OgreVector3.h"

#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{
    struct ObjectData;

    class ObjectInsertionAction : public Action{
    public:
        ObjectInsertionAction(EntryId id, ObjectInsertionType type);
        ~ObjectInsertionAction();

        void performAction();
        void performAntiAction();

        void populate(const Resource& resource, const ResourceFSEntry& fsEntry, const Ogre::Vector3& pos = Ogre::Vector3::ZERO);
        /**
        Populate to insert an empty node.
        */
        void populate();

        void populate(PhysicsShapeType type);

        /**
        Populate the action to insert a collision sender.
        */
        void populateCollisionSender();

        /**
        Populate to insert a nav mesh marker.
        */
        void populateNavMeshMarker();

        /**
        Populate to insert a data point.
        */
        void populateDataPoint();

    private:
        EntryId mId;

        enum InsertActionType{
            empty,
            mesh,
            physicsShape,
            collisionSender,
            navMarker,
            dataPoint
        };

        InsertActionType mInsertionType = empty;
        EntryId resultId; //The id that was created during the performAction.
        int resultIndex;

        SceneEntryType mSceneType;
        std::string mEntryName;
        Ogre::Vector3 mInsertPos;

        ObjectInsertionType mType;
        ObjectData* mObjectData = 0;
    };
}
