#include "ObjectDeleteAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"
#include "Event/Events/EditorEvent.h"
#include "Event/EventDispatcher.h"

#include <iostream>

#include <cassert>

namespace Southsea{
    ObjectDeleteAction::ObjectDeleteAction()
    : Action(){

    }

    ObjectDeleteAction::~ObjectDeleteAction(){

    }

    void ObjectDeleteAction::performAction(){
        mEntrySectionList.clear();
        mDeletedSceneEntries.clear();
        for(ObjectData* data : mObjectData){
            delete data;
        }
        mObjectData.clear();
        mTreeCreationData.clear();

        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();
        assert(mSelectedEntries.size() > 0);
        //In case a node with children was selected, this action needs to remember all that information.
        //So it takes a snapshot of what the children of that node are, and if multiple nodes are selected, it also has to remember those.
        for(EntryId i : mSelectedEntries){
            int startIdx = mDeletedSceneEntries.size();
            int idx = sceneMan->populateVectorWithNode(i, mDeletedSceneEntries);
            int finalSize = mDeletedSceneEntries.size();

            //Now the copy have been made, it's fine to delete things.
            //This is done for each selection so the indexes match up correctly.
            bool deletedTerminators = sceneMan->deleteObjectFromTree(i);

            int objStart = mObjectData.size();
            //Pass in the TreeObjectManager here so that the ogre manager can call it to delete its objects.
            //If it ends up doing a recursive delete it can call the destruction function itself.
            ogreMan->populateVectorWithNodeData(i, mObjectData, mTreeCreationData, treeMan.get());
            int objEnd = mObjectData.size();
            EntryId parentEntry = ogreMan->getParentOfEntry(i);
            ogreMan->destroyTreeItem(i, treeMan.get());
            //TODO in the case of multiple objects being selected, make that work with this.


            mEntrySectionList.push_back({idx, startIdx, finalSize, deletedTerminators, objStart, objEnd, parentEntry});

            SceneTreeItemDeleted a;
            a.id = i;
            EventDispatcher::transmitEvent(EventType::Editor, a);
        }

        assert(mEntrySectionList.size() == mSelectedEntries.size()); //Each selected item should have an entry in the meta section list.
    }

    void ObjectDeleteAction::performAntiAction(){
        assert(mDeletedSceneEntries.size() > 0);
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();

        //We have to iterate the list backwards when performing the anti action. That way the indexes will match up correctly.
        auto it = mEntrySectionList.rbegin();
        while(it != mEntrySectionList.rend()){
            const DeletedSelectionInfo& info = *it;

            sceneMan->insertVectorItems(mDeletedSceneEntries, info.idx, info.start, info.end, info.deletedTerminators);

            //repopulate the ogre tree with the new items.
            ogreMan->repopulateTree(mDeletedSceneEntries, mObjectData, mTreeCreationData, info.start, info.end, info.objStart, info.objEnd, info.parentNode, treeMan.get());

            it++;
        }
    }

    void ObjectDeleteAction::populate(const std::set<EntryId>& selectedEntries){
        mSelectedEntries.reserve(selectedEntries.size());
        for(EntryId i : selectedEntries){
            mSelectedEntries.push_back(i);
        }
    }
}
