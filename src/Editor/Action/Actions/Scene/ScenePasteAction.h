#pragma once

#include "Editor/Scene/SceneCopyData.h"
#include "Editor/Action/Action.h"
#include "Editor/Scene/SceneTypes.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/SceneFileParserInterface.h"

#include <vector>

namespace Southsea{

    class ScenePasteAction : public Action{
        friend class CopyStateManager;
    public:
        ScenePasteAction();
        ScenePasteAction(EntryId id, ObjectInsertionType type, const Ogre::Vector3& targetPos = Ogre::Vector3::ZERO);
        ~ScenePasteAction();

        void performAction();
        void performAntiAction();

        void populate(
            EntryId destination,
            const std::vector<SceneEntry>& mCopiedSceneEntries,
            const std::vector<CopiedSelectionInfo>& mEntrySectionList,
            const std::vector<ObjectData*>& mObjectData,
            const std::vector<OgreTreeManager::TreeNodeCreationData>& mTreeCreationData
        );

        void populateFromSceneFileInterface(SceneFileSceneTreeParserInterface& interface);

    private:
        bool mPopulated = false;

        std::vector<SceneEntry> mCopiedSceneEntries;
        std::vector<CopiedSelectionInfo> mEntrySectionList;
        std::vector<ObjectData*> mObjectData;
        std::vector<OgreTreeManager::TreeNodeCreationData> mTreeCreationData;

        EntryId mDestination;
        ObjectInsertionType mType;
        Ogre::Vector3 mTargetPos;
    };
}
