#pragma once

#include "Editor/Action/Action.h"

#include <string>
#include "Editor/Scene/SceneTypes.h"
#include "Editor/Chunk/NavMesh/NavMeshData.h"

namespace Southsea{
    class NavMeshChangeVariable : public Action{
    public:

        NavMeshChangeVariable();
        ~NavMeshChangeVariable();

        void populate(NavMeshDataType t, NavMeshId meshId, float oldValue, float newValue);
        void populateIncludeTerrain(NavMeshId meshId, bool includeTerrain);

        void performAction();
        void performAntiAction();


    private:
        NavMeshDataType mType;
        NavMeshId mMeshId;
        float mOldValue;
        float mNewValue;

        void _perform(bool perform);
    };
}
