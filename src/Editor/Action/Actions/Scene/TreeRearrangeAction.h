#pragma once

#include "Editor/Action/Action.h"
#include "Editor/Scene/SceneTypes.h"
#include "ObjectInsertionType.h"

#include <set>
#include <vector>

namespace Southsea{

    class TreeRearrangeAction : public Action{
    public:
        TreeRearrangeAction();
        ~TreeRearrangeAction();

        void performAction();
        void performAntiAction();

        void populate(const std::set<EntryId>& selectedEntries, EntryId destination, ObjectInsertionType type);

    private:
        bool populated = false;
        std::vector<EntryId> mSelectedEntries;
        std::vector<int> mEntryIndexes;
        EntryId mDestination;
        ObjectInsertionType mType;

        void _notifyChange();
    };
}
