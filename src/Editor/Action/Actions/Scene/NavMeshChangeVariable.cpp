#include "NavMeshChangeVariable.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Chunk/NavMesh/NavMeshManager.h"

namespace Southsea{
    NavMeshChangeVariable::NavMeshChangeVariable() : Action(){

    }

    NavMeshChangeVariable::~NavMeshChangeVariable(){

    }

    void NavMeshChangeVariable::populate(NavMeshDataType t, NavMeshId meshId, float oldValue, float newValue){
        mType = t;
        mOldValue = oldValue;
        mNewValue = newValue;
        mMeshId = meshId;
    }

    void NavMeshChangeVariable::populateIncludeTerrain(NavMeshId meshId, bool includeTerrain){
        mType = NavMeshDataType::INCLUDE_TERRAIN;
        //Use the float to represent a boolean.
        mNewValue = includeTerrain ? 1.0f : 0.0f;
        mOldValue = includeTerrain ? 0.0f : 1.0f;
        mMeshId = meshId;
    }

    void NavMeshChangeVariable::performAction(){
        _perform(true);
    }

    void NavMeshChangeVariable::performAntiAction(){
        _perform(false);
    }

    void NavMeshChangeVariable::_perform(bool perform){
        float target = perform ? mNewValue : mOldValue;
        auto meshManager = EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager();
        if(mType == NavMeshDataType::INCLUDE_TERRAIN){
            meshManager->setNavMeshIncludeTerrain(mMeshId, target > 0);
        }else{
            meshManager->setNavMeshValue(mMeshId, mType, target);
        }
    }
}
