#include "ViewportSettingsAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Compositor/EditorCompositorManager.h"

namespace Southsea{
    ViewportSettingsAction::ViewportSettingsAction(int windowId)
    : mWindow(windowId),
      mPerform(false),
      Action(){

    }

    ViewportSettingsAction::~ViewportSettingsAction(){

    }

    void ViewportSettingsAction::populateForVisibilityChange(ActionType a, bool visible){
        mType = a;

        Ogre::uint32 targetMask = 0;
        switch(a){
            default:
            case PHYSICS_SHAPE_VISIBLE: targetMask = PHYSICS_OBJECT_MASK; break;
            case NAV_MESH_VISIBLE: targetMask = NAV_MESH_OBJECT_MASK; break;
        }

        mNew.visibilityMask = targetMask;

        mPerform = visible;
    }

    void ViewportSettingsAction::performAction(){
        _perform(mPerform);
    }

    void ViewportSettingsAction::performAntiAction(){
        _perform(!mPerform);
    }

    void ViewportSettingsAction::_perform(bool perform){
        Editor* e = EditorSingleton::getEditor();
        e->getEditorCompositorManager()->updateRenderViewportVisibilityMask(mWindow, perform, mNew.visibilityMask);

        if(mType == PHYSICS_SHAPE_VISIBLE){
            e->setPhysicsShapesVisible(perform);
        }
        else if(mType == NAV_MESH_VISIBLE){
            e->setNavMeshVisible(perform);
        }else{
            assert(false);
        }

    }
}
