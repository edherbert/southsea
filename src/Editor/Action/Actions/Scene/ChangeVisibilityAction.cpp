#include "ChangeVisibilityAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"

namespace Southsea{
    ChangeVisibilityAction::ChangeVisibilityAction(EntryId id, bool nowVisible)
    : mId(id),
      mNowVisible(nowVisible),
      Action(){

    }

    ChangeVisibilityAction::~ChangeVisibilityAction(){

    }

    void ChangeVisibilityAction::performAction(){
        //I assume that the previous visibility would have been the opposite of what we're setting it to.
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        bool makeVisible = EditorSingleton::getEditor()->getSceneTreeManager()->setItemVisibility(mId, mNowVisible, ogreMan.get());
        ogreMan->setTreeItemVisible(mId, makeVisible);
    }

    void ChangeVisibilityAction::performAntiAction(){
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        bool makeVisible = EditorSingleton::getEditor()->getSceneTreeManager()->setItemVisibility(mId, !mNowVisible, ogreMan.get());
        ogreMan->setTreeItemVisible(mId, makeVisible);
    }
}
