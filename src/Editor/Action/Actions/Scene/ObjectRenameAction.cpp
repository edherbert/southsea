#include "ObjectRenameAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"

namespace Southsea{
    ObjectRenameAction::ObjectRenameAction(EntryId id, const std::string& oldName, const std::string& newName)
    : mId(id),
      mOldName(oldName),
      mNewName(newName),
      Action(){

    }

    ObjectRenameAction::~ObjectRenameAction(){

    }

    void ObjectRenameAction::performAction(){
        EditorSingleton::getEditor()->getSceneTreeManager()->renameTreeItem(mId, mNewName);
    }

    void ObjectRenameAction::performAntiAction(){
        EditorSingleton::getEditor()->getSceneTreeManager()->renameTreeItem(mId, mOldName);
    }
}
