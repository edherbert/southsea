#pragma once

#include "Editor/Action/Action.h"
#include "Editor/Scene/SceneTypeMasks.h"

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class ViewportSettingsAction : public Action{
    public:
        enum ActionType{
            PHYSICS_SHAPE_VISIBLE,
            NAV_MESH_VISIBLE
        };

        ViewportSettingsAction(int windowId);
        ~ViewportSettingsAction();

        void populateForVisibilityChange(ActionType a, bool visible);

        void performAction();
        void performAntiAction();

    private:
        int mWindow;
        ActionType mType;

        union Data{
            Ogre::uint32 visibilityMask;
        };

        Data mOld;
        Data mNew;

        bool mPerform;

        void _perform(bool perform);
    };
}
