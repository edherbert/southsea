#pragma once

namespace Southsea{
    enum class ObjectInsertionType{
        none,
        into,
        above,
        below
    };
}
