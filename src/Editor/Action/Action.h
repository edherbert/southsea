#pragma once

namespace Southsea{
    class Action{
    public:

        virtual ~Action() { }

        virtual void performAction() = 0;
        virtual void performAntiAction() = 0;
    };
}
