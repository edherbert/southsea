#pragma once

#include <vector>

namespace Southsea{
    class Action;
    class Event;

    class ActionStack{
        public:
            ActionStack();
            ~ActionStack();

            void undo();
            void redo();

            /**
            Push an action into the undo stack. If there are any remaining actions in the redo stack they will be cleared.

            Note.
            Once an action is pushed the actionStack takes ownership of it. You should not try and access the action through pointer after it has been pushed.
            For instance, the actionStack will determine when to call delete on the action, and therefore there is no guarantee that the memory will always be valid.
            */
            void pushAction(Action* a);

            void clearAllActions();

            bool commandEventReceiver(const Event &e);
            bool editorEventReceiver(const Event &e);

            int getUndoStackCount() const { return mUndoStack.size(); }
            int getRedoStackCount() const { return mRedoStack.size(); }

            int getMaxActions() const { return MAX_ACTIONS; }

        private:
            std::vector<Action*> mUndoStack;
            std::vector<Action*> mRedoStack;

            void _clearRedoStack();

            static const int MAX_ACTIONS;
    };
}
