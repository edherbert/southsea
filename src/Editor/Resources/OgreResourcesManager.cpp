#include "OgreResourcesManager.h"

#include "filesystem/path.h"
#include <OgreConfigFile.h>
#include "System/Project/Project.h"
#include "ResourcePathUtils.h"

#include "OgreResourceGroupManager.h"
#include "Platforms/OgreSetup/SouthseaResourceGroups.h"

#include "OgreRoot.h"

#include <iostream>

namespace Southsea{
    OgreResourcesManager::OgreResourcesManager(){

    }

    OgreResourcesManager::~OgreResourcesManager(){

    }

    void OgreResourcesManager::_determineResourceLocations(ResourceLocationContainer &resourceLocations, const std::string& resourcesFilePath){
        resourceLocations.clear();

        const filesystem::path filePath(resourcesFilePath);
        if(!filePath.exists() || !filePath.is_file()) return;

        Ogre::ConfigFile cf;
        cf.load(resourcesFilePath);

        Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();

        filesystem::path relativePath(resourcesFilePath);
        relativePath = relativePath.parent_path();

        Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
        while (seci.hasMoreElements()) {
            Ogre::String secName = seci.peekNextKey();
            if(secName.empty()) {
                seci.getNext();
                continue;
            }

            std::vector<ResourceLocationEntry>& targetVec = resourceLocations[secName];

            Ogre::String groupName;
            SouthseaResourceGroupUtils::_toSouthseaResLocation(secName, groupName);

            Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
            for(Ogre::ConfigFile::SettingsMultiMap::iterator i = settings->begin();
                i != settings->end(); ++i) {
                Ogre::String typeName = i->first;
                Ogre::String archName = i->second;


                filesystem::path valuePath(archName);
                filesystem::path resolvedPath;
                if(!valuePath.is_absolute()){
                    //If an absolute path was not provided determine an absolute.
                    resolvedPath = relativePath / valuePath;
                    if(!resolvedPath.exists() || !resolvedPath.is_directory()) continue;

                    resolvedPath = resolvedPath.make_absolute();
                }else{
                    resolvedPath = valuePath;
                    if(!resolvedPath.exists() || !resolvedPath.is_directory()) continue;
                }

                const std::string resolvedPathString = resolvedPath.str();
                std::string formattedPath = resolvedPathString;
                ResourcePathUtils::formatPath(resolvedPathString, formattedPath);

                std::cout << formattedPath << '\n';

                Ogre::Root::getSingleton().addResourceLocation(resolvedPathString, typeName, groupName);
                targetVec.push_back({resolvedPathString, formattedPath});
            }
            Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup(groupName, false);
        }
    }

    bool OgreResourcesManager::isPathResourceLocation(const std::string& path, std::string* outResName){
        if(outResName) outResName->clear();

        for(const auto& i : mResources){
            for(const ResourceLocationEntry& y : i.second){
                if(y.first == path) {
                    if(outResName) *outResName = i.first;

                    return true;
                }
            }
        }

        return false;
    }

    void OgreResourcesManager::scanFileForLocations(const std::string& filePath){

        mResources.clear();

        if(filePath.empty()) return; //Most likely a resources file wasn't found, so no path was provided.

        _determineResourceLocations(mResources, filePath);
    }

    void OgreResourcesManager::removeResourceLocations(){
        Ogre::Root& root = Ogre::Root::getSingleton();
        for(const auto& i : mResources){
            for(const auto& y : i.second){
                root.removeResourceLocation(y.first);
            }
        }
    }
}
