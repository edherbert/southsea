#pragma once

#include <string>
#include <vector>
#include <map>

namespace Southsea{
    /**
    A class to manage the ogre resource locations for this project.
    */
    class OgreResourcesManager{
    public:
        typedef std::pair<std::string, std::string> ResourceLocationEntry;
        typedef std::map<std::string, std::vector<ResourceLocationEntry>> ResourceLocationContainer;

        const ResourceLocationContainer& getOgreResources() const { return mResources; }

    public:
        OgreResourcesManager();
        ~OgreResourcesManager();

        void scanFileForLocations(const std::string& filePath);

        void removeResourceLocations();

        /**
        Determine whether or not a path is an ogre resources location.

        @arg path
        The path to check.
        @arg outResName
        An optional string pointer which if provided will be populated with the group this path is a part of.
        If this directory is not a path the string will be empty.

        @returns
        True if the provided path is an ogre directory.
        */
        bool isPathResourceLocation(const std::string& path, std::string* outResName = 0);

    private:
        ResourceLocationContainer mResources;

        void _determineResourceLocations(ResourceLocationContainer &resourceLocations, const std::string& resourcesFilePath);
    };
}
