#pragma once

#include <string>
#include "filesystem/path.h"

namespace Southsea{
    class ExposedPath : public filesystem::path{
    public:
        ExposedPath();
        ExposedPath(const std::string& path);

        ExposedPath lexicallyRelative(const ExposedPath& path) const;

        inline void appendPathString(const std::string& path);

        const std::vector<std::string>& getPathsVector() const { return m_path; }
    };

    class ResourcePathUtils{
    public:
        //In future I might want other specifiers like map://, save://. I really don't know but it's there if I need it.
        enum class PathType{
            res
        };

        static const std::string ResHeader;

    public:

        ResourcePathUtils() = delete;
        ~ResourcePathUtils() = delete;

        /**
        Takes a real file system path and tries to convert it into the specified path, e.g res://.
        This will fail if the requested path isn't at the front of the string, as absolute paths are expected.

        @returns
        True or false depending on whether the conversion was a success.
        */
        static void formatPath(const std::string& path, std::string& outPath, PathType t = PathType::res);

        /**
        Parse a formated string to a real string. For instance res://something to /dataDirectory/something
        */
        static bool parsePath(const std::string& path, std::string& outPath, PathType t = PathType::res);

        /**
        Simply remove a path header from a string. This function will fail if no header is present.
        */
        static bool removeHeader(const std::string& path, std::string& outPath, PathType t = PathType::res);

        static void _setDataDirectory(const std::string& dataDirectory);

    private:
        static std::string mDataDirectory;

        static ExposedPath mParsedDataDirectory;
    };
}
