#pragma once

#include <memory>

namespace Southsea{
    class Project;
    class OgreResourcesManager;

    /**
    A class to encapsulate and manage all the parts of the resource system for a single project.
    This includes ogre resource locations, directory scanning, etc.
    */
    class ResourceManager{
    public:
        ResourceManager();
        ~ResourceManager();

        void initialise(const Project& proj);
        void shutdown();

        std::shared_ptr<OgreResourcesManager> getOgreResourcesManager() const { return mOgreResourcesManager; }

    private:
        std::shared_ptr<OgreResourcesManager> mOgreResourcesManager;
    };
}
