#include "ResourceManager.h"

#include "OgreResourcesManager.h"
#include "ResourcePathUtils.h"
#include "System/Project/Project.h"

#include "Editor/EditorSingleton.h"
#include "System/Project/MapMetaFile.h"

#include <cassert>

namespace Southsea{
    ResourceManager::ResourceManager()
        : mOgreResourcesManager(std::make_shared<OgreResourcesManager>()) {

    }

    ResourceManager::~ResourceManager(){

    }

    void ResourceManager::initialise(const Project& proj){

        //Due to the way the getOgreResourcesFile isn't const, I have to create a new copy here.
        //Maybe those functions don't need to be const.
        //AVSetupFile file(proj.getAVSetupFile());
        const AVSetupFile& file = proj.getAVSetupFile();

        const std::string& dataDirectory = file.getDataDirectory();

        ResourcePathUtils::_setDataDirectory(dataDirectory);

        //Scan the ogre resources file for locations.
        mOgreResourcesManager->scanFileForLocations(file.getOgreResourcesFile());

    }

    void ResourceManager::shutdown(){
        ResourcePathUtils::_setDataDirectory("");

        mOgreResourcesManager->removeResourceLocations();
    }
}
