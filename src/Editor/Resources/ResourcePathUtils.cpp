#include "ResourcePathUtils.h"

#include <iostream>
#include <cassert>

namespace Southsea{

    std::string ResourcePathUtils::mDataDirectory;

    const std::string ResourcePathUtils::ResHeader = "res://";

    ExposedPath ResourcePathUtils::mParsedDataDirectory;

    void ResourcePathUtils::formatPath(const std::string& path, std::string& outPath, PathType t){

        // if(path.rfind(mDataDirectory) != 0){
        //     return false;
        // }

        const ExposedPath donePath = ExposedPath(path).lexicallyRelative( mParsedDataDirectory );

        //std::cout << donePath.str() << '\n';
        //assert(false);

        // outPath = path;
        //
        // outPath.erase(0, mDataDirectory.size());
        // if(outPath[0] == '/'){
        //     outPath.erase(0, 1);
        // }

        outPath = donePath.str();
        if(outPath == ".") outPath.clear(); //In this case just the header will suffice.
        outPath.insert(0, ResHeader);
    }

    bool ResourcePathUtils::parsePath(const std::string& path, std::string& outPath, PathType t){
        if(path.rfind(ResHeader) != 0){
            return false;
        }

        outPath = path;
        outPath.erase(0, ResHeader.size());
        //const filesystem::path p = filesystem::path(mDataDirectory) / filesystem::path(resolvedPath);

        //To make sure the paths are separated correctly.
        //This might need a re-think on Windows.
        if(outPath[0] != '/'){
            outPath.insert(0, mDataDirectory + "/");
        }else{
            outPath.insert(0, mDataDirectory);
        }

        return true;
    }

    bool ResourcePathUtils::removeHeader(const std::string& path, std::string& outPath, PathType t){
        if(path.rfind(ResHeader) != 0){
            return false;
        }

        outPath = path;
        outPath.erase(0, ResHeader.size());

        return true;
    }

    void ResourcePathUtils::_setDataDirectory(const std::string& dataDirectory){
        ResourcePathUtils::mDataDirectory = dataDirectory;

        ResourcePathUtils::mParsedDataDirectory = ExposedPath(mDataDirectory);
    }


    //Exposed path

    ExposedPath::ExposedPath() : filesystem::path(){

    }

    ExposedPath::ExposedPath(const std::string& p) : filesystem::path(p){

    }

    ExposedPath ExposedPath::lexicallyRelative(const ExposedPath& path) const{
        //Taken and modified from gulrak/filesystem. I had to make it work around the current filesystem library I'm using before the eventual switch to c++17.
        //if (root_name() != base.root_name() || is_absolute() != base.is_absolute() || (!has_root_directory() && base.has_root_directory())) {
        if (is_absolute() != path.is_absolute()) {
            return ExposedPath();
        }
        std::vector<std::string>::const_iterator a = m_path.begin(), b = path.m_path.begin();
        while (a != m_path.end() && b != path.m_path.end() && *a == *b) {
            ++a;
            ++b;
        }
        if (a == m_path.end() && b == path.m_path.end()) {
            return ExposedPath(".");
        }
        int count = 0;
        auto it = b;
        while(it != path.m_path.end()){
            const std::string& element = *it;
            if (element != "." && element != "..") {
                ++count;
            }
            else if (element == "..") {
                --count;
            }
            it++;
        }
        if (count < 0) {
            return ExposedPath();
        }
        ExposedPath result;
        for (int i = 0; i < count; ++i) {
            //result /= "..";
            static const std::string separator = "..";
            result.appendPathString(separator);
        }
        it = a;
        //for (const auto& element : input_iterator_range<const_iterator>(a, end())) {
        while(it != m_path.end()){
            const std::string& element = *it;
            //result /= element;
            result.appendPathString(element);
            it++;
        }
        return result;
    }

    inline void ExposedPath::appendPathString(const std::string& path){
        m_path.push_back(path);
    }

}
