#pragma once

#include <string>

namespace Southsea{
    enum class ResourceType{
        unknown,
        directory,
        resourcesDirectory,
        script, //.nut
        texture, //.jpg .png
        textFile, //.cfg
        mesh, //.mesh
        material, //.json.material
        avScene //.avscene
    };
    /**
    A wrapper around a resource type which contains two strings, i.e a name and a group name.
    Really these strings can be used for anything though, for instance name might also be a res path.
    */
    struct Resource{
        std::string name;
        std::string group;

        Resource& operator=(const Resource& res){
            name = res.name;
            group = res.group;

            return *this;
        }

        Resource& operator=(const std::string& resName){
            name = resName;
            group.clear();

            return *this;
        }

        bool empty() const {
            return name.empty();
        }
        void clear(){
            name.clear();
            group.clear();
        }
    };

    /**
    An entry of a resource which would exist in the resource browser.
    This is intended to be a resource file system entry, as in type of file, and just a filename.
    This is not a resource from ogre's perspective but just a file from the filesystem.
    */
    struct ResourceFSEntry{
        std::string name;
        ResourceType type;

        bool ogreResource;

        ResourceFSEntry& operator=(const std::string& e){
            name = e;
            type = ResourceType::unknown;
            ogreResource = false;

            return *this;
        }
        ResourceFSEntry& operator=(const ResourceFSEntry& e){
            name = e.name;
            type = e.type;
            ogreResource = e.ogreResource;

            return *this;
        }
        ResourceFSEntry& operator=(const Resource& e){
            *this = e.name;
            return *this;
        }
    };

}
