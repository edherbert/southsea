#include "DataPointManager.h"

#include "Editor/Editor.h"
#include "System/Project/Project.h"

#include <rapidjson/filereadstream.h>
#include <rapidjson/error/en.h>

#include "filesystem/path.h"

#include <iostream>

namespace Southsea{
    DataPointManager::DataPointManager(Editor* editor)
        : mEditor(editor){

    }

    DataPointManager::~DataPointManager(){

    }

    bool DataPointManager::parseFile(){
        const std::string& dataDirectory = mEditor->getEditorProject().getAVSetupFile().getDataDirectory();

        filesystem::path p = filesystem::path(dataDirectory) / filesystem::path("dataPointTypes.json");
        if(!p.exists() || p.is_directory()) return false;

        FILE* fp = fopen(p.str().c_str(), "r");
        char readBuffer[65536];
        rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        rapidjson::Document d;
        d.ParseStream(is);
        fclose(fp);

        if(d.HasParseError()){
            std::cout << "Error parsing the data point file. '" << p.str() << "'" << std::endl;
            std::cout << rapidjson::GetParseError_En(d.GetParseError()) << std::endl;

            return false;
        }

        rapidjson::Value::ConstMemberIterator itr = d.MemberBegin();
        for(; itr != d.MemberEnd(); itr++){
            const rapidjson::Value& value = itr->value;
            size_t startIndex = mDataSubTypePoints.size();
            _parseDataType(value);
            size_t endIndex = mDataSubTypePoints.size();

            const char* c = itr->name.GetString();
            mDataPoints.push_back({startIndex, endIndex, c});
        }

        return true;
    }

    void DataPointManager::_parseDataType(const rapidjson::Value& v){
        rapidjson::Value::ConstMemberIterator itr = v.MemberBegin();
        for(; itr != v.MemberEnd(); itr++){
            if(!itr->value.IsObject()) continue;
            const char* c = itr->name.GetString();

            rapidjson::Value::ConstMemberIterator valIterator = itr->value.FindMember("mesh");
            if(valIterator != itr->value.MemberEnd() && valIterator->value.IsString()){
                //A valid entry.
                mDataSubTypePoints.push_back({c, valIterator->value.GetString()});
            }
        }
    }

    int DataPointManager::getNumSubTypesForDataType(int id) const{
        const DataPointEntry& e = mDataPoints[id];
        return static_cast<int>(e.end - e.start);
    }

    const std::string& DataPointManager::getMeshForType(DataPointType type, DataPointSubType subType){
        static const std::string emptyString("");
        if(type >= mDataPoints.size()){
            return emptyString;
        }
        const DataPointEntry& typeEntry = mDataPoints[type];

        assert(typeEntry.end - typeEntry.start < 255);
        size_t target = typeEntry.start + subType;
        if(target >= mDataSubTypePoints.size()){
            return emptyString;
        }

        const DataPointSubTypeEntry& targetEntry = mDataSubTypePoints[target];
        return targetEntry.targetMesh;
    }
}
