#pragma once

#include "Editor/Scene/SceneTypes.h"
#include <vector>
#include <string>
#include <rapidjson/document.h>

namespace Southsea{
    class Editor;

    class DataPointManager{
    public:
        DataPointManager(Editor* editor);
        ~DataPointManager();

        struct DataPointEntry{
            size_t start;
            size_t end;
            std::string name;
        };

        struct DataPointSubTypeEntry{
            std::string name;
            std::string targetMesh;
        };

        /**
        Parse the data point file and load all the data point types to this manager.
        */
        bool parseFile();

        int getNumDataTypes() const { return static_cast<int>(mDataPoints.size()); }
        int getNumSubTypesForDataType(int id) const;

        const std::string& getMeshForType(DataPointType type, DataPointSubType subType);

        const std::vector<DataPointEntry>& getDataPoints() const { return mDataPoints; };
        const std::vector<DataPointSubTypeEntry>& getDataSubTypePoints() const { return mDataSubTypePoints; };

    private:
        Editor* mEditor;

        void _parseDataType(const rapidjson::Value& v);

        std::vector<DataPointEntry> mDataPoints;
        std::vector<DataPointSubTypeEntry> mDataSubTypePoints;
    };
}
