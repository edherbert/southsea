#include "EditorSingleton.h"

#include <cassert>

namespace Southsea{
    Editor* EditorSingleton::mEditor = 0;
    bool EditorSingleton::mCurrentlyEditing = false;

    bool EditorSingleton::startEditor(const Project& proj, const Map& map){
        if(!map.isValid() || !proj.isValid()) return false;

        mEditor = new Editor(proj, map);
        mEditor->initialise();
        mCurrentlyEditing = true;

        return true;
    }

    bool EditorSingleton::stopEditor(){
        if(!mCurrentlyEditing) return false;
        assert(mEditor->isPendingShutdown() && "Please call editor->requestShutdown() to stop the editor.");

        delete mEditor;
        mEditor = 0;
        mCurrentlyEditing = false;

        return true;
    }
}
