#pragma once

#include "OgreVector3.h"
#include "OgreVector2.h"
#include "OgreTextureManager.h"
#include <vector>

namespace Ogre{
    class CompositorNodeDef;
}

namespace Southsea{
    /**
    A class to manage the render scene to texture compositors.

    These compositors are slightly more complex than you might otherwise expect.
    The render window is likely to change size, and thus the resolution required by the texture is also like to change.
    Instead of having to re-create a texture when the size grows bigger, I create a big texture upfront for the target area to grow into.
    This class manages that among other things.
    */
    class EditorCompositorManager{
    public:
        struct ViewportInformation{
            Ogre::Texture* ptr;
            Ogre::Vector2 uv;
        };

        EditorCompositorManager(Ogre::SceneManager* sceneManager, Ogre::Camera* camera);
        ~EditorCompositorManager();

        static const int MAX_RENDER_VIEWPORTS = 1;

        void update();

        void notifyRenderViewportExists(int windowId, int width, int height);
        void notifyRenderViewportRemoved(int windowId);

        void updateRenderViewportVisibilityMask(int windowId, bool add, Ogre::uint32 mask);
        void updateRenderViewportSize(int windowId, int newWidth, int newHeight);

        /**
        Obtain information for drawing a specific viewport.
        */
        bool getViewportInformation(int viewportId, ViewportInformation& info);

        void shutdown();

        void setupCompositorProvider();

        struct CompositorSettings{
            Ogre::uint32 visibilityMask;
            //Normalised size
            Ogre::Vector2 viewportSizes;
            Ogre::Vector2 UVSizes;

            int width;
            int height;
        };

    private:
        static inline bool _validRenderViewport(int id){
            return id >= 0 && id < MAX_RENDER_VIEWPORTS;
        }

        bool _setupTextureOfSize(int viewportId, int width, int height);
        Ogre::TexturePtr _createTexture(int viewportId, int width, int height);

        /**
        Determine the best size to create an image based on width and height parameters.
        Often an image is created to be a few times larger than the requested size to provide some redundancy.
        */
        Ogre::Vector2 _getBestSizeForSize(int width, int height);

        void _updateCompositorWithSettings(int windowId);

        void _updateViewportSetup(int windowId, int newWidth, int newHeight);

        void _clearViewportSettings(int windowId);

        void _setupViewportNode(Ogre::CompositorNodeDef* def, Ogre::uint8 target, Ogre::TexturePtr viewportTexture, int windowId, int newWidth, int newHeight, Ogre::uint32 visibilityMask);

        Ogre::CompositorWorkspace* mViewportWorkspaces[MAX_RENDER_VIEWPORTS];
        Ogre::TexturePtr mViewportTextures[MAX_RENDER_VIEWPORTS];
        CompositorSettings mSettings[MAX_RENDER_VIEWPORTS];

        std::vector<Ogre::TexturePtr> mPendingDestruction;

        Ogre::SceneManager* mSceneManager;
        Ogre::Camera* mCamera;
    };
}
