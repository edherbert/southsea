#include "EditorCompositorManager.h"

#include "Ogre.h"
#include "Compositor/OgreCompositorNodeDef.h"
#include "Compositor/Pass/PassScene/OgreCompositorPassSceneDef.h"
#include "Compositor/OgreCompositorManager2.h"
#include "Compositor/OgreCompositorWorkspace.h"
#include "Compositor/Pass/OgreCompositorPassProvider.h"

#include "Platforms/OgreSetup/SouthseaResourceGroups.h"

#include <iostream>

//Dummy compositor passes to trick Ogre into not complaining if the user provides a custom pass in one of their scripts.
class DummyCompositorPassDef : public Ogre::CompositorPassDef{
public:
    DummyCompositorPassDef(Ogre::CompositorTargetDef *parentTargetDef)
        : Ogre::CompositorPassDef(Ogre::PASS_CUSTOM, parentTargetDef){
        mProfilingId = "DummyCompositor";
    }
};

class DummyCompositorPassProvider : public Ogre::CompositorPassProvider{
    public:
    DummyCompositorPassProvider(){

    }

    Ogre::CompositorPassDef* addPassDef( Ogre::CompositorPassType passType,
                                   Ogre::IdString customId,
                                   Ogre::CompositorTargetDef *parentTargetDef,
                                   Ogre::CompositorNodeDef *parentNodeDef){
        DummyCompositorPassDef* def = OGRE_NEW DummyCompositorPassDef(parentTargetDef);
        def->mIdentifier = 100;
        return def;
    }

    Ogre::CompositorPass* addPass( const Ogre::CompositorPassDef *definition, Ogre::Camera *defaultCamera,
                                     Ogre::CompositorNode *parentNode, const Ogre::CompositorChannel& target,
                                     Ogre::SceneManager *sceneManager){
        assert(false);
    }
};

namespace Southsea{

    EditorCompositorManager::EditorCompositorManager(Ogre::SceneManager* sceneManager, Ogre::Camera* camera)
        : mSceneManager(sceneManager),
          mCamera(camera) {

        for(int i = 0; i < MAX_RENDER_VIEWPORTS; i++){
            _clearViewportSettings(i);
        }

    }

    EditorCompositorManager::~EditorCompositorManager(){

    }

    void EditorCompositorManager::setupCompositorProvider(){
        DummyCompositorPassProvider *compoProvider = OGRE_NEW DummyCompositorPassProvider();
        Ogre::Root::getSingleton().getCompositorManager2()->setCompositorPassProvider(compoProvider);
    }

    void EditorCompositorManager::shutdown(){
        Ogre::CompositorManager2 *compositorManager = Ogre::Root::getSingleton().getCompositorManager2();

        for(int i = 0; i < MAX_RENDER_VIEWPORTS; i++){
            if(mViewportWorkspaces[i]){
                compositorManager->removeWorkspace(mViewportWorkspaces[i]);
            }
            mViewportTextures[i].reset();
        }
    }

    void EditorCompositorManager::update(){
        for(int i = 0; i < MAX_RENDER_VIEWPORTS; i++){
            if(mViewportWorkspaces[i])
                mViewportWorkspaces[i]->_update();
        }

        //I don't destroy the textures inline, as I had problems with imgui still retaining a pointer to the old texture for a single frame.
        //So I just queue the destruction here.
        for(Ogre::TexturePtr p : mPendingDestruction){
            Ogre::TextureManager::getSingleton().remove(p);
        }
        mPendingDestruction.clear();
    }

    void EditorCompositorManager::notifyRenderViewportRemoved(int windowId){
        if(!_validRenderViewport(windowId)) return;

        if(!mViewportWorkspaces[windowId]) return;

        Ogre::CompositorManager2 *compositorManager = Ogre::Root::getSingleton().getCompositorManager2();

        compositorManager->removeWorkspace(mViewportWorkspaces[windowId]);
        _clearViewportSettings(windowId);
    }

    void EditorCompositorManager::updateRenderViewportSize(int windowId, int newWidth, int newHeight){
        if(!_validRenderViewport(windowId)) return;
        if(!mViewportWorkspaces[windowId]) return;

        _updateViewportSetup(windowId, newWidth, newHeight);

    }

    void EditorCompositorManager::updateRenderViewportVisibilityMask(int windowId, bool add, Ogre::uint32 mask){
        if(!_validRenderViewport(windowId)) return;
        if(!mViewportWorkspaces[windowId]) return;
        if(mask == mSettings[windowId].visibilityMask) return;

        if(add){
            mSettings[windowId].visibilityMask |= mask & Ogre::VisibilityFlags::RESERVED_VISIBILITY_FLAGS;
        }else{
            mSettings[windowId].visibilityMask &= ~(mask & Ogre::VisibilityFlags::RESERVED_VISIBILITY_FLAGS);
        }

        _updateCompositorWithSettings(windowId);

    }

    void EditorCompositorManager::notifyRenderViewportExists(int windowId, int width, int height){
        if(!_validRenderViewport(windowId)) return;

        if(mViewportWorkspaces[windowId]) return; //If the workspace already exists there's nothing to do.

        //The compositor workspace needs to be created.

        _updateViewportSetup(windowId, width, height);

    }


    void EditorCompositorManager::_clearViewportSettings(int windowId){
        mViewportWorkspaces[windowId] = 0;
        mViewportTextures[windowId].reset();
        mSettings[windowId].viewportSizes = Ogre::Vector2(-1, -1);
        mSettings[windowId].UVSizes = Ogre::Vector2(-1, -1);
        mSettings[windowId].visibilityMask = 0x3FFFFFFF;
        mSettings[windowId].width = 100;
        mSettings[windowId].height = 100;
    }

    void EditorCompositorManager::_updateViewportSetup(int windowId, int newWidth, int newHeight){

        const Ogre::Vector2 sizeVec(newWidth, newHeight);
        if(sizeVec == mSettings[windowId].viewportSizes) return; //No size change, so nothing to do.

        mSettings[windowId].viewportSizes = sizeVec;
        mSettings[windowId].width = newWidth;
        mSettings[windowId].height = newHeight;

        //Might return the same texture as used previously (because the size is within the bounds). We do need to re-create the workspaces to change the viewport though.
        bool textureChange = _setupTextureOfSize(windowId, newWidth, newHeight);

        std::cout << "Resizing render window compositor " << windowId << " to " << newWidth << " " << newHeight << '\n';
        std::cout << "Using texture size of " << mViewportTextures[windowId]->getWidth() << " " << mViewportTextures[windowId]->getHeight() << '\n';

        _updateCompositorWithSettings(windowId);
    }

    void EditorCompositorManager::_updateCompositorWithSettings(int windowId){
        Ogre::CompositorManager2 *compositorManager = Ogre::Root::getSingleton().getCompositorManager2();

        if(mViewportWorkspaces[windowId]) compositorManager->removeWorkspace(mViewportWorkspaces[windowId]);
        Ogre::TexturePtr viewportTexture = mViewportTextures[windowId];

        const CompositorSettings& viewportSettings = mSettings[windowId];

        {
            //Apply the viewport alteration to the node definition. When the workspace is created it will use this updated node.
            Ogre::CompositorNodeDef* def = compositorManager->getNodeDefinitionNonConst("Southsea/SceneViewportNode");
            _setupViewportNode(def, 1, viewportTexture, windowId, viewportSettings.width, viewportSettings.height, viewportSettings.visibilityMask);
        }

        mViewportWorkspaces[windowId] = compositorManager->addWorkspace(mSceneManager, viewportTexture->getBuffer()->getRenderTarget(), mCamera, "Southsea/SceneViewportWorkspace", false );
    }

    void EditorCompositorManager::_setupViewportNode(Ogre::CompositorNodeDef* def, Ogre::uint8 target, Ogre::TexturePtr viewportTexture, int windowId, int newWidth, int newHeight, Ogre::uint32 visibilityMask){
        Ogre::CompositorPassDef* d = def->getTargetPass(0)->getCompositorPassesNonConst()[target];
        assert(d->getType() == Ogre::PASS_SCENE);
        Ogre::CompositorPassSceneDef* sceneDef = dynamic_cast<Ogre::CompositorPassSceneDef*>(d);

        mSettings[windowId].UVSizes = Ogre::Vector2(float(newWidth) / viewportTexture->getWidth(), float(newHeight) / viewportTexture->getHeight());
        sceneDef->mVpWidth = mSettings[windowId].UVSizes.x;
        sceneDef->mVpHeight = mSettings[windowId].UVSizes.y;
        sceneDef->mVpScissorWidth = mSettings[windowId].UVSizes.x;
        sceneDef->mVpScissorHeight = mSettings[windowId].UVSizes.y;

        sceneDef->mVisibilityMask = visibilityMask;
    }

    bool EditorCompositorManager::_setupTextureOfSize(int viewportId, int width, int height){

        const Ogre::Vector2 bestSize = _getBestSizeForSize(width, height);

        if(mViewportTextures[viewportId]){

            if(mViewportTextures[viewportId]->getWidth() != bestSize.x || mViewportTextures[viewportId]->getHeight() != bestSize.y){
                //The recommended size is now different from the actual texture size, so it needs to be re-created.

                mPendingDestruction.push_back(mViewportTextures[viewportId]); //Queue the old texture for destruction.
                mViewportTextures[viewportId].reset();

                mViewportTextures[viewportId] = _createTexture(viewportId, bestSize.x, bestSize.y);

                return true;
            }

        }else{
            //There is no set viewport texture.
            //So regardless of size we need to create a new one.
            mViewportTextures[viewportId] = _createTexture(viewportId, bestSize.x, bestSize.y);

            return true;
        }

        return false;
        //return mViewportTextures[viewportId];
    }

    bool EditorCompositorManager::getViewportInformation(int viewportId, ViewportInformation& info){
        if(!_validRenderViewport(viewportId)) return false;
        info.ptr = mViewportTextures[viewportId].get();
        info.uv = mSettings[viewportId].UVSizes;

        return true;
    }

    Ogre::Vector2 EditorCompositorManager::_getBestSizeForSize(int width, int height){
        //2000, 4000, 8000

        Ogre::Vector2 retVal;

        float resolvedSize = 2000.0f;
        while(width > resolvedSize){
            resolvedSize *= 2;
        }
        retVal.x = resolvedSize;

        resolvedSize = 2000.0f;
        while(height > resolvedSize){
            resolvedSize *= 2;
        }
        retVal.y = resolvedSize;

        return retVal;
    }

    Ogre::TexturePtr EditorCompositorManager::_createTexture(int viewportId, int width, int height){
        using namespace Ogre;

        Ogre::TexturePtr tex = TextureManager::getSingleton().createManual(
            "renderViewportTarget" + std::to_string(viewportId),
            SOUTHSEA_INTERNAL_GROUP_NAME,
            TEX_TYPE_2D, width, height,
            1, PF_R8G8B8, TU_RENDERTARGET );

        return tex;
    }
}
