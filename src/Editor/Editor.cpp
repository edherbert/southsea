#include "Editor.h"

#include "Logic/EditorState.h"

#include "Resources/ResourceManager.h"
#include "Chunk/Terrain/TerrainManager.h"

#include "Action/ActionStack.h"

#include "Ogre.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/CommandEvent.h"
#include "Event/Events/EditorEvent.h"
#include "Event/Events/EditorGuiEvent.h"
#include "Event/Events/SystemEvent.h"

#include "Editor/Compositor/EditorCompositorManager.h"
#include "Editor/ui/ObjectCoordsGizmo.h"
#include "Editor/ui/SceneObjectPreviewer.h"
#include "Editor/ui/EditorToolDialogManager.h"

#include "Scene/SceneTreeManager.h"
#include "Scene/OgreTreeManager.h"
#include "Scene/TreeObjectManager.h"
#include "Scene/SceneSerialiser.h"
#include "Scene/SceneRaycastHelper.h"
#include "Scene/CopyStateManager.h"

#include "System/Project/ChunkMetaFile.h"
#include "System/Editor/EngineRunner.h"
#include "System/Editor/EditorResourceSetup.h"

#include "Editor/Scene/SceneTypeMasks.h"

#include "DataPoint/DataPointManager.h"

#include "Ogre.h"
#include "OgreHlmsUnlit.h"
#include "OgreHlmsUnlitDatablock.h"
#include "OgreItem.h"


#include <iostream>

namespace Southsea{
    Editor::Editor(const Project& proj, const Map& map)
        : mMap(map),
          mProject(proj),
          mChunkSize(proj.getAVSetupFile().getSlotSize()),
          mMapMetaFile(std::make_shared<MapMetaFile>(map)),
          mMapNavFile(std::make_shared<MapNavFile>(map)),
          mEditorState(std::make_shared<EditorState>()),
          mResourceManager(std::make_shared<ResourceManager>()),
          mSceneTreeManager(std::make_shared<SceneTreeManager>()),
          mOgreTreeManager(std::make_shared<OgreTreeManager>()),
          mTreeObjectManager(std::make_shared<TreeObjectManager>()),
          mChunk(std::make_shared<EditedChunk>(this, map, mChunkSize)),
          mObjectCoordsGizmo(std::make_shared<ObjectCoordsGizmo>()),
          mSceneObjectPreviewer(std::make_shared<SceneObjectPreviewer>()),
          mEditorToolDialogManager(std::make_shared<EditorToolDialogManager>()),
          mCopyStateManager(std::make_shared<CopyStateManager>()),
          mActionStack(std::make_shared<ActionStack>()),
          mEditorTerrainManager(EditorTerrainManager(this)),
          mDataPointManager(std::make_shared<DataPointManager>(this)) {

        for(char i = 0; i < NUM_OUTER_CHUNKS; i++){
            mOutsideChunks[i] = std::make_shared<Chunk>(this, map, mChunkSize, i);
        }

    }

    Editor::~Editor(){
        _writeMetaFile();

        _destroyOgreEssentials();

        mResourceManager->shutdown();

        EventDispatcher::unsubscribe(EventType::Command, this);
        EventDispatcher::unsubscribe(EventType::Editor, this);
        EventDispatcher::unsubscribe(EventType::System, this);
        EventDispatcher::unsubscribe(EventType::EditorGui, this);
    }

    void Editor::initialise(){
        mMapMetaFile->parseFile();
        mMapNavFile->parseFile();
        mMap.scanChunkEntries();
        mDataPointManager->parseFile();

        _setupOgreEssentials();

        mCompositorManager = std::make_shared<EditorCompositorManager>(mSceneManager, mCamera);
        mCompositorManager->setupCompositorProvider();

        Ogre::Real slotSize = mProject.getAVSetupFile().getSlotSize();
        mChunk->initialise(mSceneManager, Ogre::Vector3::ZERO);
        const Ogre::Vector3 positions[NUM_OUTER_CHUNKS] = {
            {0, 0, -slotSize},
            {-slotSize, 0, 0},
            {slotSize, 0, 0},
            {0, 0, slotSize},
        };
        for(char i = 0; i < NUM_OUTER_CHUNKS; i++)
            mOutsideChunks[i]->initialise(mSceneManager, positions[i]);
        mResourceManager->initialise(mProject);

        mCompositorManager->notifyRenderViewportExists(0, 500, 500); //To start say 500 for the window size.
        requestChunkSwitch(mMapMetaFile->getMapChunkX(), mMapMetaFile->getMapChunkY());


        EventDispatcher::subscribe(EventType::Command, SOUTHSEA_BIND(Editor::commandEventReceiver));
        EventDispatcher::subscribe(EventType::Editor, SOUTHSEA_BIND(Editor::editorEventReceiver));
        EventDispatcher::subscribe(EventType::System, SOUTHSEA_BIND(Editor::systemEventReceiver));
        EventDispatcher::subscribe(EventType::EditorGui, SOUTHSEA_BIND(Editor::editorGuiEventReceiver));

        //mSceneTreeManager->setupTestSceneTree();
        SceneRaycastHelper::populateSceneManager(mSceneManager, mCamera);

        setCurrentSelectionTool(mMapMetaFile->getCurrentSelectionTool());
        setOuterTerrainsVisible(mMapMetaFile->getDrawOuterTerrainsEnabled());

        //Set the visibility mask for certain types.
        mCompositorManager->updateRenderViewportVisibilityMask(0, mMapMetaFile->getPhysicsShapeVisible(), PHYSICS_OBJECT_MASK);
        mCompositorManager->updateRenderViewportVisibilityMask(0, mMapMetaFile->getNavMeshVisible(), NAV_MESH_OBJECT_MASK);
    }

    void Editor::requestShutdown(){
        //At some point here I should do a dirty check (unsaved changes).
        //Right now I just shutdown regardless.

        mShutdownRequested = true;
        if(!mSaveDirty){
            //If the save isn't dirty at all (nothing changed) we can just go straight to the confirmation.
            confirmShutdown();
        }
    }

    void Editor::confirmShutdown(){
        assert(mShutdownRequested); //request shutdown must have been called first.

        mPendingShutdown = true;
    }

    void Editor::abortShutdown(){
        assert(!mPendingShutdown); //The shutdown shouldn't have been confirmed if it's going to be aborted.

        mShutdownRequested = false;
        mSystemCompleteShutdown = false;
    }

    void Editor::requestViewInEngine(){
        mViewInEngineRequested = true;
        if(!mSaveDirty){
            confirmViewInEngine();
        }
    }

    void Editor::abortViewInEngine(){
        assert(mViewInEngineRequested);
        mViewInEngineRequested = false;
    }

    void Editor::confirmViewInEngine(){
        assert(mViewInEngineRequested);
        mViewInEngineRequested = false;

        EngineRunner::viewInEditor();
    }


    void Editor::_performSave(){
        Chunk::ChunkCoordinate coords = mChunk->getChunkCoordinates();
        std::cout << "Saving chunk " << coords.first << " " << coords.second << '\n';

        mChunk->save();
        mMap.notifySave(coords);

        SceneSerialiser serialise;
        serialise.serialise(*mSceneTreeManager, *mOgreTreeManager, *mTreeObjectManager, mChunk->getChunkDirectory());

        mSaveDirty = false;
    }

    void Editor::abortChunkSwitch(){
        assert(mChunkSwitchRequested);
        mChunkSwitchRequested = false;
        //I don't need to reset mTargetSwitch values here as they'll be re-populated when a switch is requested again.

        ChunkSwitchDeclinedEvent e;
        e.requestedChunkX = mTargetSwitchX;
        e.requestedChunkY = mTargetSwitchY;

        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void Editor::confirmChunkSwitch(){
        assert(mChunkSwitchRequested);
        mChunkSwitchRequested = false;

        _switchChunk();
    }

    bool Editor::requestChunkSwitch(int chunkX, int chunkY){

        static const int MAX_CHUNK = 9999;
        if(chunkX < -MAX_CHUNK ||chunkY < -MAX_CHUNK || chunkX > MAX_CHUNK || chunkY > MAX_CHUNK) return false;

        mTargetSwitchX = chunkX;
        mTargetSwitchY = chunkY;

        mChunkSwitchRequested = true;
        if(!mSaveDirty){
            confirmChunkSwitch();
        }

        return true;
    }

    void Editor::_switchChunk(){
        assert(!mChunkSwitchRequested);

        mSaveDirty = false;
        mChunk->prepareChunkSwitch();
        for(char i = 0; i < NUM_OUTER_CHUNKS; i++)
            mOutsideChunks[i]->prepareChunkSwitch();

        mOutsideChunks[0]->setCurrentChunk(mTargetSwitchX, mTargetSwitchY - 1);
        mOutsideChunks[1]->setCurrentChunk(mTargetSwitchX - 1, mTargetSwitchY);
        mOutsideChunks[2]->setCurrentChunk(mTargetSwitchX + 1, mTargetSwitchY);
        mOutsideChunks[3]->setCurrentChunk(mTargetSwitchX, mTargetSwitchY + 1);

        mChunk->setCurrentChunk(mTargetSwitchX, mTargetSwitchY);
    }

    void Editor::_setupOgreEssentials(){
        mSceneManager = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC, 2, Ogre::INSTANCING_CULLING_SINGLETHREAD, "EditorScene");
        mOgreTreeManager->initialise(mSceneManager);
        mTreeObjectManager->initialise(mSceneManager);
        mObjectCoordsGizmo->initialise(mSceneManager);
        mSceneObjectPreviewer->initialise(this, mSceneManager);

        mSceneManager->getRenderQueue()->setRenderQueueMode(80, Ogre::RenderQueue::Modes::FAST);

        Ogre::Camera *camera = mSceneManager->createCamera("EditorCamera");
        camera->setNearClipDistance( 0.2f );
        camera->setFarClipDistance( 2000.0f );
        camera->setAutoAspectRatio( true );
        mCamera = camera;

        mSceneManager->setAmbientLight( Ogre::ColourValue( 1.0f, 1.0f, 1.0f ), Ogre::ColourValue( 0.5f, 0.5f, 0.5f ) * 0.2f, Ogre::Vector3::UNIT_Y );

        Ogre::Light *light = mSceneManager->createLight();
        Ogre::SceneNode *lightNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
        lightNode->attachObject( light );
        light->setPowerScale( Ogre::Math::PI * 2 );
        light->setType( Ogre::Light::LT_DIRECTIONAL );
        light->setDirection( Ogre::Vector3( -1, -1, -1 ).normalisedCopy() );

        //NOTE: This causes assertions in an ogre debug build, relating to how I operate the compositor.
        //I'm not convinced it's my fault, but I'd prefer to be using a newer version of Ogre before I investigate further.
        //mSceneManager->setForward3D( true, 4,4,5,96,3,200 );

        /*Ogre::SceneNode* node = mSceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_DYNAMIC);
        node->setScale(Ogre::Vector3(0.1, 0.1, 0.1));
        Ogre::Item *item = mSceneManager->createItem("ogrehead2.mesh", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_DYNAMIC);
        node->attachObject((Ogre::MovableObject*)item);*/

        Ogre::SceneNode* node = mSceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_STATIC);
        Ogre::Item *item = mSceneManager->createItem("floorGrid", Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME, Ogre::SCENE_STATIC);
        item->setDatablock(EditorResourceSetup::sceneGridDatablock);

        node->attachObject((Ogre::MovableObject*)item);
        //Add a slight y offset to the grid to avoid z fighting between it and the default terrain height.
        node->setPosition(0, 0.02f, 0);
    }

    void Editor::_destroyOgreEssentials(){
        mChunk->destroy();
        for(char i = 0; i < NUM_OUTER_CHUNKS; i++)
            mOutsideChunks[i]->destroy();

        mCompositorManager->shutdown();

        mSceneManager->destroyCamera(mCamera);

        Ogre::Root::getSingleton().destroySceneManager(mSceneManager);
    }

    void Editor::update(){
        mEditorState->update();

        mChunk->update();
        for(char i = 0; i < NUM_OUTER_CHUNKS; i++)
            mOutsideChunks[i]->update();
    }

    void Editor::updateRenderables(){
        mSceneManager->updateAllTransforms();
        mCompositorManager->update();
    }

    bool Editor::commandEventReceiver(const Event &e){
        const CommandEvent& commandEvent = (const CommandEvent&)e;
        if(commandEvent.eventCategory() == CommandEventCategory::Save){
            _performSave();
        }

        return false;
    }

    bool Editor::editorEventReceiver(const Event &e){
        const EditorEvent& editorEvent = (const EditorEvent&)e;
        if(editorEvent.eventCategory() == EditorEventCategory::DirtySaveAction){
            mSaveDirty = true;
        }
        if(editorEvent.eventCategory() == EditorEventCategory::ChunkSwitch){
            const ChunkSwitchEvent& event = (const ChunkSwitchEvent&)editorEvent;
            mMapMetaFile->setMapChunkCoords(event.newChunkX, event.newChunkY);

            _writeMetaFile();
        }

        return false;
    }

    void Editor::setOuterTerrainsVisible(bool visible){
        mMapMetaFile->setDrawOuterTerrainsEnabled(visible);
        for(char i = 0; i < NUM_OUTER_CHUNKS; i++)
            mOutsideChunks[i]->setVisible(visible);
    }

    void Editor::setPhysicsShapesVisible(bool visible){
        mMapMetaFile->setPhysicsShapeVisible(visible);
    }

    void Editor::setNavMeshVisible(bool visible){
        mMapMetaFile->setNavMeshVisible(visible);
    }

    bool Editor::systemEventReceiver(const Event &e){
        const SystemEvent& systemEvent = (const SystemEvent&)e;
        if(systemEvent.eventCategory() == SystemEventCategory::WindowCloseRequest){
            //The close button of the window was pressed.
            mSystemCompleteShutdown = true;
            requestShutdown();
        }

        return false;
    }

    bool Editor::editorGuiEventReceiver(const Event& e){
        const EditorGuiEvent& guiEvent = (const EditorGuiEvent&)e;
        if(guiEvent.eventCategory() == EditorGuiEventCategory::ResizeRenderWindow){
            const EditorGuiWindowResize& resizeEvent = (const EditorGuiWindowResize&)e;
            mCompositorManager->updateRenderViewportSize(0, resizeEvent.newWidth, resizeEvent.newHeight);
        }

        return false;
    }

    void Editor::setCurrentSelectionTool(SelectionTool tool){
        mCurrentSelectionTool = tool;
        mMapMetaFile->setCurrentSelectionTool(tool);

        EditorSelectionToolChange e;
        e.newTool = tool;
        EventDispatcher::transmitEvent(EventType::Editor, e);
    }

    void Editor::resetCameraPosition(){
        setCameraPosition(Ogre::Vector3::ZERO);
        setCameraOrientation(Ogre::Quaternion::IDENTITY, -90.0f, 0);
    }

    void Editor::setCameraPosition(const Ogre::Vector3& cameraPos){
        mCamera->setPosition(cameraPos);
        mObjectCoordsGizmo->notifyCameraMovement(cameraPos);

        mChunk->getChunkMetaFile().setCameraPosition(cameraPos);
    }

    void Editor::setCameraOrientation(const Ogre::Quaternion& cameraOrientation, float yaw, float pitch){
        mCamera->setOrientation(cameraOrientation);

        mChunk->getChunkMetaFile().setCameraOrientation(cameraOrientation, yaw, pitch);
    }

    void Editor::_writeMetaFile(){
        mMapMetaFile->writeFile();
    }

    void Editor::setGridSnapEnabled(bool enabled){
        mMapMetaFile->setGridSnapEnabled(enabled);
    }

    bool Editor::getGridSnapEnabled() const{
        return mMapMetaFile->getGridSnapEnabled();
    }
}
