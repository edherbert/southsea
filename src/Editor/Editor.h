#pragma once

#include "System/Project/Map.h"
#include "System/Project/Project.h"
#include "System/Project/MapMetaFile.h"
#include "System/Project/MapNavFile.h"
#include "EditorTerrain/EditorTerrainManager.h"
#include "Chunk/EditedChunk.h"

#include <memory>
#include "EditorSelectionTools.h"

namespace Ogre{
    class Camera;
    class SceneManager;
    class Vector3;
    class Quaternion;
}

namespace Southsea{
    class EditorState;
    class ResourceManager;
    class Chunk;
    class ActionStack;
    class Event;
    class EditorCompositorManager;
    class SceneTreeManager;
    class OgreTreeManager;
    class TreeObjectManager;
    class ObjectCoordsGizmo;
    class SceneObjectPreviewer;
    class EditorToolDialogManager;
    class CopyStateManager;
    class DataPointManager;

    class Editor{
        friend EditorTerrainManager;
    public:
        Editor(const Project& proj, const Map& map);
        ~Editor();

        void initialise();
        void update();
        //Update the Compositor system only.
        void updateRenderables();

        /**
        Request that the editor shuts itself down.
        This is just a request though, as the editor is able to decline it.
        Say for instance the user has unsaved changes and a request is made, the editor will ask the user if they really do want to shutdown without saving.
        The user might not want this, and the request will be averted.
        */
        void requestShutdown();
        /**
        Confirm that the editor shutdown should procede.
        This would be called by something like a dialog popup asking the user if they want to shutdown or not.
        */
        void confirmShutdown();
        //Cancels a shutdown
        void abortShutdown();

        bool commandEventReceiver(const Event &e);
        bool editorEventReceiver(const Event &e);
        bool systemEventReceiver(const Event &e);
        bool editorGuiEventReceiver(const Event& e);

        /**
        Request that the current chunk be switched.
        The chunk itself will not be switched until it has been confirmed as switchable.
        You are able to call the chunk switch function directly within the chunk class, however this function will check for save state and show the user a popup before switch.

        @returns
        True or false depending on whether the chunk could be switched.
        For instance, if the chunk coordinates are out of the valid range false will be returned.
        */
        bool requestChunkSwitch(int chunkX, int chunkY);
        void abortChunkSwitch();
        void confirmChunkSwitch();

        void requestViewInEngine();
        void abortViewInEngine();
        void confirmViewInEngine();

        /**
        Set the position of the camera for all editor related components.
        */
        void setCameraPosition(const Ogre::Vector3& cameraPos);
        void setCameraOrientation(const Ogre::Quaternion& cameraOrientation, float yaw, float pitch);
        void resetCameraPosition();

        /**
        Set the visibility of the outer terrains.
        This function performs a set visible operation.
        */
        void setOuterTerrainsVisible(bool visible);
        bool getOuterTerrainsVisible() const { return mMapMetaFile->getDrawOuterTerrainsEnabled(); }
        void setPhysicsShapesVisible(bool visible);
        bool getPhysicsShapesVisible() const { return mMapMetaFile->getPhysicsShapeVisible(); }
        void setNavMeshVisible(bool visible);
        bool getNavMeshVisible() const { return mMapMetaFile->getNavMeshVisible(); }

        bool isShutdownRequested() const { return mShutdownRequested; }
        bool isPendingShutdown() const { return mPendingShutdown; }
        bool isCompleteShutdown() const { return mSystemCompleteShutdown; }

        bool isChunkSwitchRequested() const { return mChunkSwitchRequested; }
        bool isViewInEngineRequested() const { return mViewInEngineRequested; }

        std::shared_ptr<Chunk> getChunkById(int chunkId){
            if(chunkId < 0) return std::dynamic_pointer_cast<Chunk>(mChunk);
            assert(chunkId < NUM_OUTER_CHUNKS);
            return mOutsideChunks[chunkId];
        }

        Ogre::Camera* getEditorCamera() const { return mCamera; }

        std::shared_ptr<EditedChunk> getEditorChunk() const { return mChunk; }
        std::shared_ptr<EditorState> getEditorState() const { return mEditorState; }
        std::shared_ptr<ActionStack> getActionStack() const { return mActionStack; }
        std::shared_ptr<MapMetaFile> getMapMetaFile() const { return mMapMetaFile; }
        std::shared_ptr<MapNavFile> getMapNavFile() const { return mMapNavFile; }
        std::shared_ptr<EditorCompositorManager> getEditorCompositorManager() const { return mCompositorManager; }
        std::shared_ptr<ResourceManager> getResourceManager() const { return mResourceManager; }
        std::shared_ptr<SceneTreeManager> getSceneTreeManager() const { return mSceneTreeManager; }
        std::shared_ptr<OgreTreeManager> getOgreTreeManager() const { return mOgreTreeManager; }
        std::shared_ptr<TreeObjectManager> getTreeObjectManager() const { return mTreeObjectManager; }
        std::shared_ptr<ObjectCoordsGizmo> getObjectCoordsGizmo() const { return mObjectCoordsGizmo; }
        std::shared_ptr<SceneObjectPreviewer> getSceneObjectPreviewer() const { return mSceneObjectPreviewer; }
        std::shared_ptr<EditorToolDialogManager> getEditorToolDialogManager() const { return mEditorToolDialogManager; }
        std::shared_ptr<CopyStateManager> getCopyStateManager() const { return mCopyStateManager; }
        std::shared_ptr<DataPointManager> getDataPointManager() const { return mDataPointManager; }

        const Map& getEditorMap() { return mMap; }
        const Project& getEditorProject() { return mProject; }

        void setCurrentSelectionTool(SelectionTool tool);
        SelectionTool getCurrentSelectionTool() const { return mCurrentSelectionTool; }
        void setGridSnapEnabled(bool enabled);
        bool getGridSnapEnabled() const;
        void setSnapAmount(float amount) { mSnapAmount = amount; }
        float getSnapAmount() const { return mSnapAmount; }
        int getSlotSize() const { return mChunkSize; }

        EditorTerrainManager mEditorTerrainManager;

    private:
        Map mMap;
        Project mProject;
        int mChunkSize;
        std::shared_ptr<MapMetaFile> mMapMetaFile;
        std::shared_ptr<MapNavFile> mMapNavFile;
        std::shared_ptr<EditorState> mEditorState;
        std::shared_ptr<ResourceManager> mResourceManager;
        std::shared_ptr<EditedChunk> mChunk;
        std::shared_ptr<ActionStack> mActionStack;
        std::shared_ptr<EditorCompositorManager> mCompositorManager;
        std::shared_ptr<SceneTreeManager> mSceneTreeManager;
        std::shared_ptr<OgreTreeManager> mOgreTreeManager;
        std::shared_ptr<TreeObjectManager> mTreeObjectManager;
        std::shared_ptr<ObjectCoordsGizmo> mObjectCoordsGizmo;
        std::shared_ptr<SceneObjectPreviewer> mSceneObjectPreviewer;
        std::shared_ptr<EditorToolDialogManager> mEditorToolDialogManager;
        std::shared_ptr<CopyStateManager> mCopyStateManager;
        std::shared_ptr<DataPointManager> mDataPointManager;

        static const char NUM_OUTER_CHUNKS = 4;
        std::shared_ptr<Chunk> mOutsideChunks[NUM_OUTER_CHUNKS];

        //Whether a shutdown was requested, and the editor is waiting on confirmation of this.
        bool mShutdownRequested = false;
        //Whether a shutdown has been confirmed.
        bool mPendingShutdown = false;

        bool mViewInEngineRequested = false;
        bool mChunkSwitchRequested = false;

        //A flag to keep track of whether a complete shutdown was requested, i.e a press of the window close button.
        //If base sees a pending shutdown, as well as this flag enabled it will shutdown the entire programme.
        bool mSystemCompleteShutdown = false;

        /**
        When a new map starts up lots of ogre stuff (scene manager, cameras, rendering workspaces) need to be setup.
        */
        void _setupOgreEssentials();
        void _destroyOgreEssentials();

        void _switchChunk();
        int mTargetSwitchX, mTargetSwitchY;

        /**
        Save the current contents of the map.
        */
        void _performSave();

        bool mSaveDirty = false;

        SelectionTool mCurrentSelectionTool = SelectionTool::ObjectPicker;
        float mSnapAmount = 5.0f;

        void _writeMetaFile();

        Ogre::SceneManager* mSceneManager;
        Ogre::Camera* mCamera;
    };
}
