#pragma once

namespace Southsea{
    enum class SelectionTool{
        ObjectPicker,
        ObjectScale,
        ObjectOrientate,
        TerrainPlacer
    };
}
