#pragma once

#include <string>
#include <squirrel.h>

namespace Southsea{
    class CallbackScript;
    class TestManager;

    class ScriptManager{
    public:
        ScriptManager(TestManager* testManager);
        ~ScriptManager();

        void executeTestFile(const std::string& filePath);

        void setupVM();

        void update();

    private:
        HSQUIRRELVM vm;

        CallbackScript* mScript = 0;
        TestManager *mTestManager;

        bool mFirstUpdate = true;
        //I keep hold of this for later.
        //If I was to print out the error message when it arrived it would appear right before all the ogre stuff, which I don't want.
        //With this I can save the message for later.
        bool mScriptFailed = false;
    };
}
