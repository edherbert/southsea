#ifdef TEST_MODE_ENABLE

#include "TestManager.h"

#include "filesystem/path.h"

#include "ScriptManager.h"
#include <cassert>

#include <iostream>

namespace Southsea{
    TestManager::TestManager(){

    }

    TestManager::~TestManager(){

    }

    std::string TestManager::_getFilePathFromArg(const std::string& str){
        filesystem::path argPathFile(str);

        if(argPathFile.exists() && argPathFile.is_file()){
            return argPathFile.make_absolute().str();
        }

        return "";
    }

    void TestManager::parseArgs(const std::vector<std::string>& args){
        if(args.size() <= 1) return;

        mRequestedScriptFilePath = _getFilePathFromArg(args[1]);

        if(mRequestedScriptFilePath.empty()) return;

        _startTest(mRequestedScriptFilePath);
    }

    void TestManager::_startTest(const std::string& filePath){
        testFileProvided = true;

        mScriptManager = new ScriptManager(this);
        mScriptManager->setupVM();

        std::cout << "Starting test execution." << '\n';

        mScriptManager->executeTestFile(filePath);
    }

    void TestManager::shutdown(){
        testFileProvided = false;

        if(mScriptManager){
            delete mScriptManager;
        }
    }

    void TestManager::update(){
        if(!testFileProvided) return; //The user might not provide any arguments, so we might have nothing to update.

        if(mScriptManager){
            mScriptManager->update();
        }
    }

    void TestManager::notifyTestEnded(const testEndInfo& info){
        if(info.testEndSuccess){
            _printTestPassedMessage();
        }else{
            _printEndTestWithFalseMessage(info);
        }
        reachedTestEnd = true;
    }

    void TestManager::notifyBooleanAssertFailed(bool expected, const testEndInfo& info){
        _printBooleanAssertError(info, expected);
        reachedTestEnd = true;
    }

    void TestManager::notifyComparisonFailed(const comparisonInfo& compInfo, const testEndInfo& info){
        _printComparisonAssertFailed(info, compInfo);
        reachedTestEnd = true;
    }

    void TestManager::notifyBrokenScriptFile(const std::string& filePath, const std::string& failureReason){
        _printBrokenScriptMessage(filePath, failureReason);
        reachedTestEnd = true;
    }

    void TestManager::notifyCallError(const std::string& filePath, const std::string& failureReason){
        //In order to stop the script execution the assert functions return an error, which counts as a call error.
        //So if the test has already finished don't bother printing out the call error message.
        if(reachedTestEnd) return;

        _printCallErrorMessage(filePath, failureReason);
        reachedTestEnd = true;
    }

    void TestManager::_printTestPassedMessage(){
        static const std::string passString("Test Mode Pass!");
        std::cout << std::string(passString.size(), '=') << std::endl;

        std::cout << passString << std::endl;
        std::cout << std::string(passString.size(), '=') << std::endl;
    }

    void TestManager::_printEndTestWithFalseMessage(const testEndInfo& info){
        static const std::string passString("Test Mode Failure!");
        std::cerr << std::string(passString.size(), '=') << std::endl;

        std::cerr << passString << std::endl;
        std::cerr << "endTest called with false." << std::endl;
        std::cerr << "On line " << info.lineNum << std::endl;
        std::cerr << std::string(passString.size(), '=') << std::endl;
    }

    void TestManager::_printBooleanAssertError(const testEndInfo& info, bool e){
        static const std::string bannerString(10, '=');
        std::cerr << bannerString << std::endl;

        const std::string expected = e ? "True" : "False";
        const std::string received = !e ? "True" : "False";

        std::cerr << "Assert " << expected << " failed!" << std::endl;
        std::cerr << "  Expected: " << expected << std::endl;
        std::cerr << "  Received: " << received << std::endl;
        std::cerr << std::endl;
        std::cerr << "On line " << info.lineNum << std::endl;
        std::cerr << "Of source file " << info.srcFile << std::endl;

        std::cerr << bannerString << std::endl;
    }

    void TestManager::_printComparisonAssertFailed(const testEndInfo& info, const comparisonInfo& compInfo){
        static const std::string bannerString(10, '=');
        std::cerr << bannerString << std::endl;

        std::string assertType = compInfo.equalsComparison ? "equal" : "not equal";
        std::cerr << "Assert " << assertType << " failed!" << std::endl;
        std::cerr << "  Expected: " << compInfo.firstType << std::endl;
        std::cerr << "      With value: " << compInfo.firstValue << std::endl;
        std::cerr << "  Received: " << compInfo.secondType << std::endl;
        std::cerr << "      With value: " << compInfo.secondValue << std::endl;
        std::cerr << "" << std::endl;
        std::cerr << "On line " << info.lineNum << std::endl;
        std::cerr << "Of source file " << info.srcFile << std::endl;

        std::cerr << bannerString << std::endl;
    }

    void TestManager::_printBrokenScriptMessage(const std::string& filePath, const std::string& failureReason){
        static const std::string bannerString(10, '=');
        std::cerr << bannerString << std::endl;

        std::cerr << "An error occured while executing the test file." << std::endl;
        std::cerr << "    " << filePath << std::endl;
        std::cerr << "Reason" << std::endl;
        std::cerr << "    " << failureReason << std::endl;

        std::cerr << bannerString << std::endl;
    }

    void TestManager::_printCallErrorMessage(const std::string& filePath, const std::string& failureReason){
        static const std::string bannerString(10, '=');
        std::cerr << bannerString << std::endl;

        std::cerr << "Failed to call the script file" << std::endl;
        std::cerr << "    " << filePath << std::endl;
        std::cerr << "Reason" << std::endl;
        std::cerr << "    " << failureReason << std::endl;

        std::cerr << bannerString << std::endl;
    }
}

#endif
