#ifdef TEST_MODE_ENABLE

#include "ScriptManager.h"

#include "TestNamespace/TestNamespace.h"
#include "TestNamespace/Namespaces/ProjectNamespace.h"
#include "TestNamespace/Namespaces/TerrainNamespace.h"
#include "TestNamespace/Namespaces/ActionStackNamespace.h"
#include "TestNamespace/Namespaces/CameraNamespace.h"
#include "TestNamespace/Namespaces/ChunkNamespace.h"
#include "TestNamespace/Namespaces/SceneNamespace.h"

#include "Script/CallbackScript.h"
#include "Testing/TestManager.h"

#include <stdio.h>
#include <stdarg.h>
#include <sqstdio.h>
#include <sqstdmath.h>
#include <sqstdsystem.h>
#include <iostream>

#ifdef SQUNICODE
#define scvprintf vwprintf
#else
#define scvprintf vprintf
#endif

namespace Southsea{
    ScriptManager::ScriptManager(TestManager* testManager)
        : mTestManager(testManager) {

    }

    ScriptManager::~ScriptManager(){

    }

    void printfunc(HSQUIRRELVM v, const SQChar *s, ...){
        va_list arglist;
        va_start(arglist, s);
        scvprintf(s, arglist);
        va_end(arglist);
        std::cout << '\n';
    }

    void ScriptManager::setupVM(){
        vm = sq_open(1024);

        sq_setprintfunc(vm, printfunc, NULL);

        sq_pushroottable(vm);

        sqstd_register_mathlib(vm);

        TestNamespace testNamespace(mTestManager);
        testNamespace.setupNamespace(vm);

        ProjectNamespace::setupNamespace(vm);
        TerrainNamespace::setupNamespace(vm);
        ActionStackNamespace::setupNamespace(vm);
        CameraNamespace::setupNamespace(vm);
        ChunkNamespace::setupNamespace(vm);
        SceneNamespace::setupNamespace(vm);

        sq_pop(vm, 1);
    }

    void ScriptManager::executeTestFile(const std::string& filePath){
        mScript = new CallbackScript();
        mScript->initialise(vm);
        mScriptFailed = !mScript->prepare(filePath);
    }

    void ScriptManager::update(){
        if(mScriptFailed){
            mTestManager->notifyBrokenScriptFile(mScript->getScriptFilePath(), mScript->getLastFailureReason());
            return;
        }

        bool success = false;
        if(mFirstUpdate){
            success = mScript->call("start");
            mFirstUpdate = false;
        }else{
            success = mScript->call("update");
        }

        if(!success){
            mTestManager->notifyCallError(mScript->getScriptFilePath(), mScript->getLastFailureReason());
        }

    }
}

#endif
