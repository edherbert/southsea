#ifdef TEST_MODE_ENABLE

#include "TestNamespace.h"

#include <iostream>
#include <fstream>

#include "Testing/TestManager.h"
#include "NamespaceUtils.h"

#include "filesystem/path.h"

namespace Southsea{
    TestManager* TestNamespace::mTestManager = 0;

    TestNamespace::TestNamespace(TestManager* testManager){
        mTestManager = testManager;
    }

    SQInteger TestNamespace::assertTrue(HSQUIRRELVM vm){
        return _processBooleanAssert(vm, true);
    }

    SQInteger TestNamespace::assertFalse(HSQUIRRELVM vm){
        return _processBooleanAssert(vm, false);
    }

    SQInteger TestNamespace::assertEqual(HSQUIRRELVM vm){
        return _processComparisonAssert(vm, true);
    }

    SQInteger TestNamespace::assertNotEqual(HSQUIRRELVM vm){
        return _processComparisonAssert(vm, false);
    }

    SQInteger TestNamespace::getScriptFilePath(HSQUIRRELVM vm){
        sq_pushstring(vm, _SC(mTestManager->getScriptPath().c_str()), -1);
        return 1;
    }

    SQInteger TestNamespace::getScriptFileDirectory(HSQUIRRELVM vm){
        const filesystem::path p(mTestManager->getScriptPath());


        sq_pushstring(vm, _SC(p.parent_path().str().c_str()), -1);
        return 1;
    }


    SQInteger TestNamespace::endTest(HSQUIRRELVM vm){
        SQObjectType type = sq_gettype(vm, -1);
        SQBool testEndSuccess = true;
        if(type == OT_BOOL){
            sq_getbool(vm, -1, &testEndSuccess);
        }

        SQStackInfos si;
        sq_stackinfos(vm, 1, &si);

        TestManager::testEndInfo info = {
            (bool)testEndSuccess,
            si.line,
            si.source
        };
        mTestManager->notifyTestEnded(info);

        return SQ_ERROR;
    }

    SQInteger TestNamespace::_processBooleanAssert(HSQUIRRELVM vm, bool intendedResult){
        SQBool assertBool;
        sq_getbool(vm, -1, &assertBool);
        sq_pop(vm, -1);

        if(assertBool != intendedResult){
            //fail.
            SQStackInfos si;
            sq_stackinfos(vm, 1, &si);
            TestManager::testEndInfo info = {
                false,
                si.line,
                si.source
            };
            mTestManager->notifyBooleanAssertFailed(intendedResult, info);

            return SQ_ERROR;
        }
        return 0;
    }

    SQInteger TestNamespace::_processComparisonAssert(HSQUIRRELVM vm, bool equalsComparison){
        SQInteger e = sq_cmp(vm);

        bool comparisonFailed = equalsComparison ? e != 0 : e == 0;
        if(comparisonFailed){
            TestManager::comparisonInfo compInfo;
            //First value
            compInfo.firstType = _getTypeString(sq_gettype(vm, -2));
            //Second value
            compInfo.secondType = _getTypeString(sq_gettype(vm, -1));

            sq_tostring(vm, -1);
            //-3 because the previous string has just been pushed onto the stack.
            sq_tostring(vm, -3);

            const SQChar *firstValue;
            const SQChar *secondValue;
            sq_getstring(vm, -1, &firstValue);
            sq_getstring(vm, -2, &secondValue);
            sq_pop(vm, 2);

            compInfo.firstValue = std::string(firstValue);
            compInfo.secondValue = std::string(secondValue);
            compInfo.equalsComparison = equalsComparison;

            SQStackInfos si;
            sq_stackinfos(vm, 1, &si);

            TestManager::testEndInfo info = {
                false,
                si.line,
                si.source
            };

            mTestManager->notifyComparisonFailed(compInfo, info);
            return SQ_ERROR;
        }
        return 0;
    }

    std::string TestNamespace::_getTypeString(SQObjectType type){
        std::string retString = "Unknown Type";

        if(type == OT_NULL)
            retString = "null";
        else if(type == OT_INTEGER)
            retString = "integer";
        else if(type == OT_FLOAT)
            retString = "float";
        else if(type == OT_BOOL)
            retString = "bool";
        else if(type == OT_STRING)
            retString = "string";
        else if(type == OT_TABLE)
            retString = "table";
        else if(type == OT_ARRAY)
            retString = "array";
        else if(type == OT_USERDATA)
            retString = "userdata";
        else if(type == OT_CLOSURE)
            retString = "closure(function)";
        else if(type == OT_NATIVECLOSURE)
            retString = "native closure(C function)";
        else if(type == OT_GENERATOR)
            retString = "generator";
        else if(type == OT_USERPOINTER)
            retString = "userpointer";
        else if(type == OT_CLASS)
            retString = "class";
        else if(type == OT_INSTANCE)
            retString = "instance";
        else if(type == OT_WEAKREF)
            retString = "weak reference";

        return retString;
    }

    void TestNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_test"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, assertTrue, "assertTrue", 2, ".b");
        NamespaceUtils::_addFunction(vm, assertFalse, "assertFalse", 2, ".b");
        NamespaceUtils::_addFunction(vm, assertEqual, "assertEqual", 3, "...");
        NamespaceUtils::_addFunction(vm, assertNotEqual, "assertNotEqual", 3, "...");
        NamespaceUtils::_addFunction(vm, endTest, "endTest", -1, ".|.b");

        NamespaceUtils::_addFunction(vm, getScriptFilePath, "getScriptFilePath", -1, ".");
        NamespaceUtils::_addFunction(vm, getScriptFileDirectory, "getScriptFileDirectory", -1, ".");

        sq_newslot(vm, -3 , false);
    }
}

#endif
