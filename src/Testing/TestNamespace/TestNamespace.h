#pragma once

#include <squirrel.h>
#include <string>

namespace Southsea{
    class TestManager;

    class TestNamespace{
    public:
        TestNamespace(TestManager* testManager);

        void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger assertTrue(HSQUIRRELVM vm);
        static SQInteger assertFalse(HSQUIRRELVM vm);
        static SQInteger assertEqual(HSQUIRRELVM vm);
        static SQInteger assertNotEqual(HSQUIRRELVM vm);
        static SQInteger endTest(HSQUIRRELVM vm);

        static std::string _getTypeString(SQObjectType type);
        static SQInteger _processBooleanAssert(HSQUIRRELVM vm, bool intendedResult);
        static SQInteger _processComparisonAssert(HSQUIRRELVM vm, bool equalsComparison);

        static SQInteger getScriptFilePath(HSQUIRRELVM vm);
        static SQInteger getScriptFileDirectory(HSQUIRRELVM vm);

        //For letting the test manager know what's going on.
        static TestManager* mTestManager;
    };
}
