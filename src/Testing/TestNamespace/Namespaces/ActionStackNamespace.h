#ifdef TEST_MODE_ENABLE

#pragma once

#include <squirrel.h>

#include <string>

namespace Southsea{
    class ActionStackNamespace{
    public:
        ActionStackNamespace() = delete;
        ~ActionStackNamespace() = delete;

        static void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger undo(HSQUIRRELVM vm);
        static SQInteger redo(HSQUIRRELVM vm);

        static SQInteger getUndoStackCount(HSQUIRRELVM vm);
        static SQInteger getRedoStackCount(HSQUIRRELVM vm);

        static SQInteger getMaxActions(HSQUIRRELVM vm);
    };
}

#endif
