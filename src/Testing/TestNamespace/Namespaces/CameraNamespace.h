#ifdef TEST_MODE_ENABLE

#pragma once

#include <squirrel.h>

#include <string>

namespace Southsea{
    class CameraNamespace{
    private:
        enum CURRENT_PERFORMED_ACTION{
            NONE,
            HEIGHT
        };

    public:
        CameraNamespace() = delete;
        ~CameraNamespace() = delete;

        static void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger setCameraPosition(HSQUIRRELVM vm);
        static SQInteger cameraLookat(HSQUIRRELVM vm);
    };
}

#endif
