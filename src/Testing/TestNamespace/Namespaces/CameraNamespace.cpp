#ifdef TEST_MODE_ENABLE

#include "CameraNamespace.h"

#include "Testing/TestNamespace/NamespaceUtils.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"

#include <OgreCamera.h>

namespace Southsea{

    SQInteger CameraNamespace::setCameraPosition(HSQUIRRELVM vm){
        SQFloat x, y, z;
        sq_getfloat(vm, -1, &z);
        sq_getfloat(vm, -2, &y);
        sq_getfloat(vm, -3, &x);

        Ogre::Camera* cam = EditorSingleton::getEditor()->getEditorCamera();
        cam->setPosition(Ogre::Vector3(x, y, z));

        sq_pop(vm, 3);

        return 0;
    }

    SQInteger CameraNamespace::cameraLookat(HSQUIRRELVM vm){
        SQFloat x, y, z;
        sq_getfloat(vm, -1, &z);
        sq_getfloat(vm, -2, &y);
        sq_getfloat(vm, -3, &x);

        Ogre::Camera* cam = EditorSingleton::getEditor()->getEditorCamera();
        cam->lookAt(Ogre::Vector3(x, y, z));

        sq_pop(vm, 3);

        return 0;
    }

    void CameraNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_camera"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, setCameraPosition, "setPosition", 4, ".nnn");
        NamespaceUtils::_addFunction(vm, cameraLookat, "lookAt", 4, ".nnn");

        sq_newslot(vm, -3 , false);
    }
}

#endif
