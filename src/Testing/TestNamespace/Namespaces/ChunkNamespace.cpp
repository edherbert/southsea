#ifdef TEST_MODE_ENABLE

#include "ChunkNamespace.h"

#include "Testing/TestNamespace/NamespaceUtils.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"

#include "Editor/Chunk/EditedChunk.h"

#include <OgreCamera.h>

namespace Southsea{

    SQInteger ChunkNamespace::switchChunk(HSQUIRRELVM vm){
        SQInteger x, y;
        sq_getinteger(vm, -1, &y);
        sq_getinteger(vm, -2, &x);

        EditorSingleton::getEditor()->getEditorChunk()->setCurrentChunk(x, y);


        sq_pop(vm, 2);

        return 0;
    }

    void ChunkNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_chunk"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, switchChunk, "switchChunk", 3, ".nn");

        sq_newslot(vm, -3 , false);
    }
}

#endif
