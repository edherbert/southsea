#ifdef TEST_MODE_ENABLE

#pragma once

#include <squirrel.h>

#include <string>

namespace Southsea{
    class ChunkNamespace{
    private:
        enum CURRENT_PERFORMED_ACTION{
            NONE,
            HEIGHT
        };

    public:
        ChunkNamespace() = delete;
        ~ChunkNamespace() = delete;

        static void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger switchChunk(HSQUIRRELVM vm);
    };
}

#endif
