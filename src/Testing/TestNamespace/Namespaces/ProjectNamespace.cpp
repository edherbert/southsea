#ifdef TEST_MODE_ENABLE

#include "ProjectNamespace.h"

#include "Testing/TestNamespace/NamespaceUtils.h"
#include "Editor/EditorSingleton.h"

#include <iostream>

namespace Southsea{

    std::string ProjectNamespace::setProjectPath;
    std::string ProjectNamespace::setMapPath;

    SQInteger ProjectNamespace::specifyProjectPath(HSQUIRRELVM vm){
        const SQChar *projectPath;
        sq_getstring(vm, -1, &projectPath);

        setProjectPath = std::string(projectPath);
    }

    SQInteger ProjectNamespace::specifyMapPath(HSQUIRRELVM vm){
        const SQChar *mapPath;
        sq_getstring(vm, -1, &mapPath);

        setMapPath = std::string(mapPath);
    }

    SQInteger ProjectNamespace::loadSpecifiedProject(HSQUIRRELVM vm){
        if(setMapPath.size() > 0 && setProjectPath.size() > 0){
            Map map(setMapPath);
            Project proj(setProjectPath);
            EditorSingleton::startEditor(proj, map);
        }
    }

    SQInteger ProjectNamespace::closeProject(HSQUIRRELVM vm){
        EditorSingleton::getEditor()->requestShutdown();
    }

    void ProjectNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_project"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, specifyProjectPath, "specifyProjectPath", 2, ".s");
        NamespaceUtils::_addFunction(vm, specifyMapPath, "specifyMapPath", 2, ".s");
        NamespaceUtils::_addFunction(vm, loadSpecifiedProject, "loadSpecifiedProject", 1, ".");
        NamespaceUtils::_addFunction(vm, closeProject, "closeProject", 1, ".");

        sq_newslot(vm, -3 , false);
    }
}

#endif
