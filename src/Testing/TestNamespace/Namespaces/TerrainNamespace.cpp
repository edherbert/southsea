#ifdef TEST_MODE_ENABLE

#include "TerrainNamespace.h"

#include "Testing/TestNamespace/NamespaceUtils.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Action/Actions/Terrain/TerrainHeightMouseAction.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"
#include "Editor/Action/ActionStack.h"

#include <vector>
#include <cassert>

namespace Southsea{

    TerrainNamespace::CURRENT_PERFORMED_ACTION TerrainNamespace::currentAction = NONE;
    TerrainMouseAction* TerrainNamespace::mActionPtr = 0;

    SQInteger TerrainNamespace::beginMouseAction(HSQUIRRELVM vm){
        assert(currentAction == NONE && "Please make sure any action you've previously started is finished.");

        const SQChar *projectPath;
        sq_getstring(vm, -1, &projectPath);
        const std::string s(projectPath);

        if(s == "height"){
            currentAction = HEIGHT;
            mActionPtr = new TerrainHeightMouseAction();
        }


        return 0;
    }

    SQInteger TerrainNamespace::heightBrush(HSQUIRRELVM vm){
        assert(currentAction == HEIGHT && "Please start a height action before calling this function.");

        SQFloat terrainPosX, terrainPosY, strength;
        SQInteger brushSize;
        SQBool raiseTerrain;

        sq_getfloat(vm, -1, &strength);
        sq_getinteger(vm, -2, &brushSize);
        sq_getfloat(vm, -3, &terrainPosY);
        sq_getfloat(vm, -4, &terrainPosX);
        sq_getbool(vm, -5, &raiseTerrain);

        sq_pop(vm, 5);

        const int size = brushSize * brushSize;
        std::vector<float> dataVec(size, 1.0f);

        const int slotSize = 100; //TODO temporary! I need the infrustructure to pull this from the file.
        const Ogre::Vector3 posVec( (terrainPosX * slotSize) - slotSize / 2, 0, (terrainPosY * slotSize) - slotSize / 2);

        EditorSingleton::getEditor()
            ->getEditorChunk()->getTerrainManager()->applyHeightDiff(posVec, dataVec, brushSize, strength, mActionPtr);

        return 0;
    }

    SQInteger TerrainNamespace::endMouseAction(HSQUIRRELVM vm){
        assert(currentAction != NONE && "Please start an action before trying to end it.");

        currentAction = NONE;
        Editor* e = EditorSingleton::getEditor();
        assert(e);

        e->getActionStack()->pushAction(mActionPtr);
        mActionPtr = 0;

        return 0;
    }

    SQInteger TerrainNamespace::getHeightAt(HSQUIRRELVM vm){
        SQFloat terrainPosX, terrainPosY;

        sq_getfloat(vm, -1, &terrainPosY);
        sq_getfloat(vm, -2, &terrainPosX);

        const int slotSize = 100; //TODO temporary! I need the infrustructure to pull this from the file.
        Ogre::Vector3 posVec( (terrainPosX * slotSize) - slotSize / 2, 0, (terrainPosY * slotSize) - slotSize / 2);

        Editor* e = EditorSingleton::getEditor();
        assert(e);
        e->getEditorChunk()->getTerrainManager()->getHeightAt(posVec);

        sq_pushfloat(vm, posVec.y);

        return 1;
    }

    void TerrainNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_terrain"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, beginMouseAction, "beginMouseAction", 2, ".s");
        NamespaceUtils::_addFunction(vm, endMouseAction, "endMouseAction", 0, ".");
        NamespaceUtils::_addFunction(vm, heightBrush, "heightBrush", 6, ".bnnnn");
        NamespaceUtils::_addFunction(vm, getHeightAt, "getHeightAt", 3, ".nn");

        sq_newslot(vm, -3 , false);
    }
}

#endif
