#ifdef TEST_MODE_ENABLE

#include "ActionStackNamespace.h"

#include "Testing/TestNamespace/NamespaceUtils.h"
#include "Editor/EditorSingleton.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/CommandEvent.h"
#include "Editor/Action/ActionStack.h"

namespace Southsea{

    SQInteger ActionStackNamespace::undo(HSQUIRRELVM vm){
        CommandEventUndo e;
        EventDispatcher::transmitEvent(EventType::Command, e);

        return 0;
    }

    SQInteger ActionStackNamespace::redo(HSQUIRRELVM vm){
        CommandEventRedo e;
        EventDispatcher::transmitEvent(EventType::Command, e);

        return 0;
    }

    SQInteger ActionStackNamespace::getUndoStackCount(HSQUIRRELVM vm){
        SQInteger val = EditorSingleton::getEditor()->getActionStack()->getUndoStackCount();
        sq_pushinteger(vm, val);

        return 1;
    }

    SQInteger ActionStackNamespace::getRedoStackCount(HSQUIRRELVM vm){
        SQInteger val = EditorSingleton::getEditor()->getActionStack()->getRedoStackCount();
        sq_pushinteger(vm, val);

        return 1;
    }

    SQInteger ActionStackNamespace::getMaxActions(HSQUIRRELVM vm){
        SQInteger val = EditorSingleton::getEditor()->getActionStack()->getMaxActions();
        sq_pushinteger(vm, val);
        return 1;
    }

    void ActionStackNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_actionStack"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, undo, "undo", 0, "");
        NamespaceUtils::_addFunction(vm, redo, "redo", 0, "");

        NamespaceUtils::_addFunction(vm, getUndoStackCount, "getUndoStackCount", 0, "");
        NamespaceUtils::_addFunction(vm, getRedoStackCount, "getRedoStackCount", 0, "");

        NamespaceUtils::_addFunction(vm, getMaxActions, "getMaxActions", 0, "");

        sq_newslot(vm, -3 , false);
    }
}

#endif
