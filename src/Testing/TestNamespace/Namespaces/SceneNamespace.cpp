#ifdef TEST_MODE_ENABLE

#include "SceneNamespace.h"

#include "Testing/TestNamespace/NamespaceUtils.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Action/Actions/Scene/ObjectRenameAction.h"
#include "Editor/Action/Actions/Scene/ObjectDeleteAction.h"
#include "Editor/Action/Actions/Scene/ObjectInsertionAction.h"
#include "Editor/Action/ActionStack.h"
#include "Editor/Action/Actions/Scene/TreeRearrangeAction.h"

#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"

namespace Southsea{

    SQInteger SceneNamespace::getSceneNameOfEntry(HSQUIRRELVM vm){
        SQInteger entryId;
        sq_getinteger(vm, -1, &entryId);

        const std::string result = EditorSingleton::getEditor()->getSceneTreeManager()->getSceneTreeItemName(entryId);
        sq_pushstring(vm, result.c_str(), -1);

        return 1;
    }

    SQInteger SceneNamespace::getIdByName(HSQUIRRELVM vm){
        const SQChar *name;
        sq_getstring(vm, -1, &name);

        EntryId retId = 0;
        for(const SceneEntry& e : EditorSingleton::getEditor()->getSceneTreeManager()->mSceneTree){
            if(e.name == name){
                retId = e.id;
                break;
            }
        }

        sq_pushinteger(vm, retId);

        return 1;
    }

    SQInteger SceneNamespace::performSceneRenameAction(HSQUIRRELVM vm){
        SQInteger entryId;
        sq_getinteger(vm, -2, &entryId);

        const SQChar *newName;
        sq_getstring(vm, -1, &newName);

        const std::string foundName = EditorSingleton::getEditor()->getSceneTreeManager()->getSceneTreeItemName(entryId);

        ObjectRenameAction *a = new ObjectRenameAction(entryId, foundName, std::string(newName));
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

        return 0;
    }

    SQInteger SceneNamespace::performSceneDeleteAction(HSQUIRRELVM vm){
        SQInteger entryId;
        sq_getinteger(vm, -1, &entryId);

        ObjectDeleteAction *a = new ObjectDeleteAction();

        EntryId targetId = (EntryId)entryId;
        a->populate({targetId});
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

        return 0;
    }

    SQInteger SceneNamespace::performSceneInsertAction(HSQUIRRELVM vm){
        SQInteger entryId, insertionType;
        sq_getinteger(vm, -2, &entryId);
        sq_getinteger(vm, -1, &insertionType);

        EntryId targetId = (EntryId)entryId;
        ObjectInsertionType t = (ObjectInsertionType)(insertionType + 1);
        ObjectInsertionAction *a = new ObjectInsertionAction(targetId, t);
        a->populate();
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

        return 0;
    }

    SQInteger SceneNamespace::setSceneTree(HSQUIRRELVM vm){
        /*sq_getsize(vm, -1);
        SQInteger i;
        sq_getinteger(vm, -1, &i);
        sq_pop(vm, 1);
        if(i <= 0) return 0; //Nothing was supplied by the user.*/

        std::vector<SqSceneEntry> totalEntries;
        //totalEntries.reserve(i);

        sq_pushnull(vm);  //null iterator
        while(SQ_SUCCEEDED(sq_next(vm, -2))){
            _processEntryArray(vm, totalEntries);

           sq_pop(vm, 2);
        }

        sq_pop(vm, 1); //pop the null iterator.

        const auto treeMan = EditorSingleton::getEditor()->getSceneTreeManager();
        const auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        const auto objectMan = EditorSingleton::getEditor()->getTreeObjectManager();

        std::vector<SceneEntry> sceneTree;
        sceneTree.reserve(totalEntries.size() + 2);

        SceneEntry rootEntry = SceneTreeManager::CHILD;
        rootEntry.id = ROOT_NODE;
        sceneTree.push_back(rootEntry);
        /*for(const SqSceneEntry& e : totalEntries){
            //TODO have this set to 0 for terms and children.
            EntryId id = 0;
            if(e.second != 1 && e.second != 0) id = treeMan->_getEntryId();
            sceneTree.push_back({id, e.first, (SceneEntryType)e.second, false});

            if(id != 0){
                Ogre::SceneNode* node = ogreMan->createTreeItem(id, ROOT_NODE);

                ObjectData data;
                objectMan->insertObject(node, id, &data);
            }
        }*/
        _populateSceneTree(totalEntries, sceneTree, 0, ogreMan->getRootSceneNode());

        sceneTree.push_back(SceneTreeManager::TERM);

        treeMan->mSceneTree = sceneTree;

        return 0;
    }

    int SceneNamespace::_populateSceneTree(const std::vector<SqSceneEntry>& dataVec, std::vector<SceneEntry>& entries, int currentIdx, Ogre::SceneNode* parentNode){
        const auto treeMan = EditorSingleton::getEditor()->getSceneTreeManager();
        const auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();
        const auto objectMan = EditorSingleton::getEditor()->getTreeObjectManager();

        Ogre::SceneNode* currentSceneNode = 0;
        while(currentIdx < dataVec.size()){
            const SqSceneEntry& e = dataVec[currentIdx];
            SceneEntryType type = (SceneEntryType)e.second;
            if(type == SceneEntryType::treeChild){
                entries.push_back(SceneTreeManager::CHILD);
                currentIdx = _populateSceneTree(dataVec, entries, currentIdx + 1, currentSceneNode);
            }else if(type == SceneEntryType::treeTerminator){
                entries.push_back(SceneTreeManager::TERM);
                return currentIdx + 1;
            }else{
                EntryId id = treeMan->_getEntryId();

                entries.push_back({id, e.first, (SceneEntryType)e.second, false});
                currentSceneNode = ogreMan->_createTreeItem(id, parentNode);

                ObjectData data;
                objectMan->insertObject(currentSceneNode, id, &data);

                currentIdx++;
            }
        }

        return currentIdx;
    }

    void SceneNamespace::_processEntryArray(HSQUIRRELVM vm, std::vector<SqSceneEntry>& container){
        sq_pushnull(vm);  //null iterator
        int count = 0;

        SqSceneEntry e;

        while(SQ_SUCCEEDED(sq_next(vm, -2))){

            if(count == 0){
                const SQChar *entryName;
                sq_getstring(vm, -1, &entryName);

                e.first = entryName;
            }
            else if(count == 1){
                SQInteger i;
                sq_getinteger(vm, -1, &i);
                e.second = i;
            }

           sq_pop(vm, 2);
           count++;
        }
        container.push_back(e);

        sq_pop(vm, 1); //pop the null iterator.
    }

    SQInteger SceneNamespace::moveSceneTreeEntry(HSQUIRRELVM vm){

        SQInteger first, second, type;
        sq_getinteger(vm, -1, &type);
        sq_getinteger(vm, -2, &second);
        sq_getinteger(vm, -3, &first);

        //EditorSingleton::getEditor()->getSceneTreeManager()->moveEntry(first, second, (ObjectInsertionType)type);

        const std::set<EntryId> entries = {(EntryId)first};

        TreeRearrangeAction *a = new TreeRearrangeAction();
        a->populate(entries, second, (ObjectInsertionType)type);
        a->performAction();

        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

        return 0;
    }

    SQInteger SceneNamespace::compareCurrentTree(HSQUIRRELVM vm){

        std::vector<SqSceneEntry> totalEntries;

        sq_pushnull(vm);  //null iterator
        while(SQ_SUCCEEDED(sq_next(vm, -2))){
            _processEntryArray(vm, totalEntries);

           sq_pop(vm, 2);
        }
        sq_pop(vm, 1); //pop the null iterator.

        const auto& sceneTree = EditorSingleton::getEditor()->getSceneTreeManager()->mSceneTree;

        if(totalEntries.size() != sceneTree.size() - 2){
            sq_pushbool(vm, false);
            return 1;
        }

        bool result = true;
        for(int i = 1; i < sceneTree.size() - 1; i++){ //Skip the terminators.
            const SceneEntry& e = sceneTree[i];
            if(totalEntries[i - 1].second == -1) continue;
            if(e.id != totalEntries[i - 1].second){
                result = false;
                break;
            }
        }

        sq_pushbool(vm, result);

        return 1;
    }

    void SceneNamespace::setupNamespace(HSQUIRRELVM vm){

        sq_pushstring(vm, _SC("_scene"), -1);
        sq_newtable(vm);

        NamespaceUtils::_addFunction(vm, getSceneNameOfEntry, "getSceneNameOfEntry", 2, ".n");
        NamespaceUtils::_addFunction(vm, performSceneRenameAction, "performSceneRenameAction", 3, ".ns");

        NamespaceUtils::_addFunction(vm, getIdByName, "getIdByName", 2, ".s");
        NamespaceUtils::_addFunction(vm, setSceneTree, "setSceneTree", 2, ".a");

        NamespaceUtils::_addFunction(vm, moveSceneTreeEntry, "moveSceneTreeEntry", 4, ".iii");
        NamespaceUtils::_addFunction(vm, compareCurrentTree, "compareCurrentTree", 2, ".a");

        NamespaceUtils::_addFunction(vm, performSceneDeleteAction, "performSceneDeleteAction", 2, ".i");
        NamespaceUtils::_addFunction(vm, performSceneInsertAction, "performSceneInsertAction", 3, ".ii");

        sq_newslot(vm, -3 , false);
    }
}

#endif
