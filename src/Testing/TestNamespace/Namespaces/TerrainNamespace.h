#ifdef TEST_MODE_ENABLE

#pragma once

#include <squirrel.h>

#include <string>

namespace Southsea{
    class TerrainMouseAction;

    class TerrainNamespace{
    private:
        enum CURRENT_PERFORMED_ACTION{
            NONE,
            HEIGHT
        };

    public:
        TerrainNamespace() = delete;
        ~TerrainNamespace() = delete;

        static void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger beginMouseAction(HSQUIRRELVM vm);
        static SQInteger endMouseAction(HSQUIRRELVM vm);
        static SQInteger heightBrush(HSQUIRRELVM vm);

        static SQInteger getHeightAt(HSQUIRRELVM vm);

        static TerrainMouseAction* mActionPtr;
        static CURRENT_PERFORMED_ACTION currentAction;
    };
}

#endif
