#ifdef TEST_MODE_ENABLE

#pragma once

#include <squirrel.h>

#include <vector>
#include <string>
#include "Editor/Scene/SceneTypes.h"

namespace Ogre{
    class SceneNode;
}

namespace Southsea{
    class SceneNamespace{
    private:
        enum CURRENT_PERFORMED_ACTION{
            NONE,
            HEIGHT
        };

    public:
        SceneNamespace() = delete;
        ~SceneNamespace() = delete;

        static void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger getSceneNameOfEntry(HSQUIRRELVM vm);
        static SQInteger performSceneRenameAction(HSQUIRRELVM vm);
        static SQInteger getIdByName(HSQUIRRELVM vm);

        typedef std::pair<std::string, int> SqSceneEntry;
        static SQInteger setSceneTree(HSQUIRRELVM vm);

        static SQInteger moveSceneTreeEntry(HSQUIRRELVM vm);
        static SQInteger compareCurrentTree(HSQUIRRELVM vm);

        static SQInteger performSceneDeleteAction(HSQUIRRELVM vm);
        static SQInteger performSceneInsertAction(HSQUIRRELVM vm);

        static void _processEntryArray(HSQUIRRELVM vm, std::vector<SqSceneEntry>& container);

        static int _populateSceneTree(const std::vector<SqSceneEntry>& dataVec, std::vector<SceneEntry>& entries, int currentIdx, Ogre::SceneNode* parentNode);
    };
}

#endif
