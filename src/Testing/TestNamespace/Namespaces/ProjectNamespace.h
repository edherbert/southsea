#ifdef TEST_MODE_ENABLE

#pragma once

#include <squirrel.h>

#include <string>

namespace Southsea{
    class ProjectNamespace{
    public:
        ProjectNamespace() = delete;
        ~ProjectNamespace() = delete;

        static void setupNamespace(HSQUIRRELVM vm);

    private:
        static SQInteger specifyProjectPath(HSQUIRRELVM vm);
        static SQInteger specifyMapPath(HSQUIRRELVM vm);
        static SQInteger loadSpecifiedProject(HSQUIRRELVM vm);
        static SQInteger closeProject(HSQUIRRELVM vm);

        static std::string setProjectPath;
        static std::string setMapPath;
    };
}

#endif
