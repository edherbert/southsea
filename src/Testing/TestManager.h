#pragma once

#include <vector>
#include <string>

namespace Southsea{
    class ScriptManager;

    /**
    A class to manage running tests.

    Southsea allows specifying a squirrel file which can be used to execute tests.
    Squirrel is used because it's used in the engine as well, although it should be noted the api is completely different.
    */
    class TestManager{
    public:
        TestManager();
        ~TestManager();

        struct testEndInfo{
            bool testEndSuccess;
            long long int lineNum;
            std::string srcFile;
        };

        struct comparisonInfo{
            std::string firstType;
            std::string secondType;
            std::string firstValue;
            std::string secondValue;
            bool equalsComparison;
        };


        void parseArgs(const std::vector<std::string>& args);

        void update();

        void shutdown();

        void notifyTestEnded(const testEndInfo& info);
        void notifyBooleanAssertFailed(bool expected, const testEndInfo& info);
        void notifyComparisonFailed(const comparisonInfo& compInfo, const testEndInfo& info);
        void notifyBrokenScriptFile(const std::string& filePath, const std::string& failureReason);
        void notifyCallError(const std::string& filePath, const std::string& failureReason);

        bool getTestReachedEnd(){ return reachedTestEnd; }

        const std::string& getScriptPath() { return mRequestedScriptFilePath; }

    private:
        bool testFileProvided = false;
        bool reachedTestEnd = false;

        std::string _getFilePathFromArg(const std::string& str);

        /**
        Call to start a test execution.
        By this point we've determined that the user has provided a valid path to a file.
        This will begin the motion to execute that script file and begin the test.
        There are no actual checks that this file is a valid script.
        If it can't be executed the test will just fail.
        */
        void _startTest(const std::string& filePath);

        ScriptManager* mScriptManager;

        std::string mRequestedScriptFilePath;


        //Test output specific stuff.
        void _printTestPassedMessage();
        void _printEndTestWithFalseMessage(const testEndInfo& info);
        void _printBooleanAssertError(const testEndInfo& info, bool expected);
        void _printComparisonAssertFailed(const testEndInfo& info, const comparisonInfo& compInfo);
        void _printBrokenScriptMessage(const std::string& filePath, const std::string& failureReason);
        void _printCallErrorMessage(const std::string& filePath, const std::string& failureReason);
    };
}
