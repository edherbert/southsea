#pragma once

#include <map>
#include <vector>
#include <string>

#include <squirrel.h>

namespace Southsea{
    /**
     A class to encapsulate callback functionality of scripts.
     Callback scripts are intended to simply contain a list of functions which can be executed individually by this class.
     */
    class CallbackScript{
    public:
        CallbackScript(HSQUIRRELVM vm);
        CallbackScript();
        ~CallbackScript();

        /**
         Setup the script by specifying a path to a squirrel file.
         This function is responsible for parsing the contents of the file, and preparing the closures for execution.
         The script must first be initalised before this can be called.

         @param path
         The path to the script that should be processed.
         @return
         Whether or not the preparation work was successful.
         */
        bool prepare(const std::string& path);
        /**
         Initialise this script with a vm. Either this or the vm constructor needs to be called before the script can be used.
         */
        void initialise(HSQUIRRELVM vm);

        /**
         Call a callback function.
         This function expects the script to have been prepared and initialised before this will work.

         @param functionName
         The name of the callback that should be executed.
         @return
         Whether or not this call was successful.
         */
        bool call(const std::string& functionName);

        bool call(int closureId, SQObject* obj = 0);

        /**
         Get the int id of a callback.
         This id can later be used for more efficient calling.

         @return
         A positive number if a callback with that name was found. -1 if not.
         */
        int getCallbackId(const std::string& functionName);

        /**
         Whether or not a callback with the specified name exists in this callback script.

         @param functionName
         The name of the callback that should be executed.
         */
        bool containsCallback(const std::string& functionName);

        /**
         Release this script and all the resources held by it.
         */
        void release();

        const std::string& getLastFailureReason() { return failureReason; }
        const std::string& getScriptFilePath() { return filePath; }

    private:
        HSQUIRRELVM mVm;
        HSQOBJECT mMainClosure;
        HSQOBJECT mMainTable;

        bool _compileMainClosure(const std::string& path);
        bool _createMainTable();
        bool _callMainClosure();
        bool _parseClosureTable();

        void _processSquirrelFailure(HSQUIRRELVM vm);

        bool _call(int closureId, SQObject *obj = 0);

        bool mPrepared = false;
        bool mInitialised = false;

        std::vector<HSQOBJECT> mClosures;
        std::map<std::string, int> mClosureMap;

        std::string filePath;
        std::string failureReason = "";
    };
}
