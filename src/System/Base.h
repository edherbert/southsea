#pragma once

#include <memory>

namespace Ogre{
    class Root;
    class SceneManager;
    class Camera;
    class CompositorWorkspace;
}

namespace Southsea{
    class SDL2Window;
    class ImguiBase;
    class Event;

    class Base{
    public:
        Base();
        ~Base();

        void update();
        void shutdown();

        bool hasShutdown() { return mHasShutdown; }

        bool systemEventReceiver(const Event& e);

    private:
        std::shared_ptr<SDL2Window> mWindow;
        std::shared_ptr<ImguiBase> mImguiBase;

        Ogre::Root *mRoot;
        Ogre::Camera *mCamera;
        Ogre::SceneManager *mSceneManager;
        Ogre::CompositorWorkspace* mViewportWorkspace;

        //Whether or not the shutdown has finished.
        bool mHasShutdown = false;
        //Whether or not base is preparing to shutdown.
        bool mShuttingDown = false;

        void _initialise();
        void _setupOgre();
    };
}
