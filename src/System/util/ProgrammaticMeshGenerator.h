#pragma once

#include "Ogre.h"

namespace Southsea{

    /**
    A class to create basic shapes programmatically without needing to load them from the file system.
    */
    class ProgrammaticMeshGenerator{
    public:
        static void createMesh();

    private:
        static Ogre::IndexBufferPacked* createIndexBuffer(int cubeArraySize, const Ogre::uint16* indexData);
        static Ogre::MeshPtr createStaticMesh(const Ogre::String& name, Ogre::IndexBufferPacked *indexBuffer, const Ogre::VertexElement2Vec& elemVec, int arraySize, const float* vertexData, Ogre::OperationType t = Ogre::OT_TRIANGLE_LIST);

        static void generateFloorGrid();
        static void generateLineVao();
        static void generateLineBox();
        static void generateFullLineBox();
        static void generateCube();
        static void generateLineSphere();

    };
}
