#include "ProgrammaticMeshGenerator.h"

#include <OgreMeshManager2.h>
#include <OgreMesh2.h>
#include <OgreItem.h>
#include "OgreSubMesh2.h"

namespace Southsea{
    void ProgrammaticMeshGenerator::createMesh(){
        generateLineVao();
        generateLineBox();
        generateCube();
        generateFloorGrid();
        generateFullLineBox();
        generateLineSphere();
    }

    void ProgrammaticMeshGenerator::generateCube(){
        static const int cubeArraySize = 12 * 3;
        static const Ogre::uint16 c_indexData[cubeArraySize]{
            0, 1, 2,
            0, 2, 3,
            4, 5, 6,
            4, 6, 7,
            8, 9, 10,
            8, 10, 11,
            12, 13, 14,
            12, 14, 15,
            16, 17, 18,
            16, 18, 19,
            20, 21, 22,
            20, 22, 23
        };

        Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer(cubeArraySize, &c_indexData[0]);


        //static const int cubeVerticesCount = (6 * 4) * 6;
        const int cubeVerticesCount = 6 * 4 * 6;
        static const float c_originalVertices[cubeVerticesCount] = {
            1.000000,-1.000000,-1.000000, 0.000000,-1.000000,-0.000000,
            1.000000,-1.000000,1.000000, 0.000000,-1.000000,-0.000000,
            -1.000000,-1.000000,1.000000, 0.000000,-1.000000,-0.000000,
            -1.000000,-1.000000,-1.000000, 0.000000,-1.000000,-0.000000,

            1.000000,1.000000,-1.000000, 0.000000,1.000000,0.000000,
            -1.000000,1.000000,-1.000000, 0.000000,1.000000,0.000000,
            -1.000000,1.000000,1.000000, 0.000000,1.000000,0.000000,
            1.000000,1.000000,1.000001, 0.000000,1.000000,0.000000,

            1.000000,-1.000000,-1.000000, 1.000000,0.000000,0.000000,
            1.000000,1.000000,-1.000000, 1.000000,0.000000,0.000000,
            1.000000,1.000000,1.000001, 1.000000,0.000000,0.000000,
            1.000000,-1.000000,1.000000, 1.000000,0.000000,0.000000,

            1.000000,-1.000000,1.000000, -0.000000,-0.000000,1.000000,
            1.000000,1.000000,1.000001, -0.000000,-0.000000,1.000000,
            -1.000000,1.000000,1.000000, -0.000000,-0.000000,1.000000,
            -1.000000,-1.000000,1.000000, -0.000000,-0.000000,1.000000,

            -1.000000,-1.000000,1.000000, -1.000000,-0.000000,-0.000000,
            -1.000000,1.000000,1.000000, -1.000000,-0.000000,-0.000000,
            -1.000000,1.000000,-1.000000, -1.000000,-0.000000,-0.000000,
            -1.000000,-1.000000,-1.000000, -1.000000,-0.000000,-0.000000,

            1.000000,1.000000,-1.000000, 0.000000,0.000000,-1.000000,
            1.000000,-1.000000,-1.000000, 0.000000,0.000000,-1.000000,
            -1.000000,-1.000000,-1.000000, 0.000000,0.000000,-1.000000,
            -1.000000,1.000000,-1.000000, 0.000000,0.000000,-1.000000
        };

        Ogre::VertexElement2Vec vertexElements;
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_NORMAL));

        //createStaticMesh("cube", indexBuffer, cubeVerticesCount, &c_originalVertices[0]);
        createStaticMesh("cube", indexBuffer, vertexElements, cubeVerticesCount, &c_originalVertices[0], Ogre::OT_TRIANGLE_LIST);
    }

    void ProgrammaticMeshGenerator::generateFloorGrid(){
        const int gridArraySpacing = 5;
        const int gridSize = 100;
        const int numLines = gridSize / gridArraySpacing;

        //+2 so that the lines are closed off. The loops run for <=, meaning there's +1 of whatever numLines is. As there's 2 loops we add 2 here.
        const int verticesCount = (numLines + numLines + 2) * 3 * 2;
        float c_originalVertices[verticesCount];

        const int indexSize = (numLines + numLines + 2) * 2;
        Ogre::uint16 c_indexData[indexSize];

        int count = 0;
        int idxCount = 0;
        for(int y = 0; y <= numLines; y++){
            c_originalVertices[count++] = 0;
            c_originalVertices[count++] = 0;
            c_originalVertices[count++] = y * gridArraySpacing;

            c_originalVertices[count++] = gridSize;
            c_originalVertices[count++] = 0;
            c_originalVertices[count++] = y * gridArraySpacing;

            c_indexData[idxCount] = idxCount;
            idxCount++;
            c_indexData[idxCount] = idxCount;
            idxCount++;
        }
        for(int x = 0; x <= numLines; x++){
            c_originalVertices[count++] = x * gridArraySpacing;
            c_originalVertices[count++] = 0;
            c_originalVertices[count++] = 0;

            c_originalVertices[count++] = x * gridArraySpacing;
            c_originalVertices[count++] = 0;
            c_originalVertices[count++] = gridSize;

            c_indexData[idxCount] = idxCount;
            idxCount++;
            c_indexData[idxCount] = idxCount;
            idxCount++;
        }
        assert(idxCount == indexSize);
        assert(count == verticesCount);

        Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer(idxCount, &c_indexData[0]);

        Ogre::VertexElement2Vec vertexElements;
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));

        Ogre::MeshPtr createdMesh = createStaticMesh("floorGrid", indexBuffer, vertexElements, verticesCount, &c_originalVertices[0], Ogre::OT_LINE_LIST);
        createdMesh->_setBounds( Ogre::Aabb( Ogre::Vector3::ZERO, Ogre::Vector3(gridSize, 1, gridSize) ), false );
        //mesh->_setBoundingSphereRadius( 1.732f );
    }

    void ProgrammaticMeshGenerator::generateLineBox(){
        static const int cubeArraySize = 2 * 3 * 8;
        static const Ogre::uint16 c_indexData[cubeArraySize]{
            0, 0 + 1,
            0, 0 + 2,
            0, 0 + 3,

            4, 4 + 1,
            4, 4 + 2,
            4, 4 + 3,

            8, 8 + 1,
            8, 8 + 2,
            8, 8 + 3,

            12, 12 + 1,
            12, 12 + 2,
            12, 12 + 3,

            16, 16 + 1,
            16, 16 + 2,
            16, 16 + 3,

            20, 20 + 1,
            20, 20 + 2,
            20, 20 + 3,

            24, 24 + 1,
            24, 24 + 2,
            24, 24 + 3,

            28, 28 + 1,
            28, 28 + 2,
            28, 28 + 3,
        };

        Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer(cubeArraySize, &c_indexData[0]);


        static const int cubeVerticesCount = 3 * 4 * 8;
        static const float offset = 0.2f;
        static const float c_originalVertices[cubeVerticesCount] = {
            -1.0f, -1.0f, -1.0f,
            -1.0f + offset, -1.0f, -1.0f,
            -1.0f, -1.0f + offset, -1.0f,
            -1.0f, -1.0f, -1.0f + offset,

            -1.0f, 1.0f, -1.0f,
            -1.0f + offset, 1.0f, -1.0f,
            -1.0f, 1.0f - offset, -1.0f,
            -1.0f, 1.0f, -1.0f + offset,

            1.0f, 1.0f, -1.0f,
            1.0f - offset, 1.0f, -1.0f,
            1.0f, 1.0f - offset, -1.0f,
            1.0f, 1.0f, -1.0f + offset,

            1.0f, -1.0f, -1.0f,
            1.0f - offset, -1.0f, -1.0f,
            1.0f, -1.0f + offset, -1.0f,
            1.0f, -1.0f, -1.0f + offset,

            1.0f, -1.0f, 1.0f,
            1.0f - offset, -1.0f, 1.0f,
            1.0f, -1.0f + offset, 1.0f,
            1.0f, -1.0f, 1.0f - offset,

            1.0f, 1.0f, 1.0f,
            1.0f - offset, 1.0f, 1.0f,
            1.0f, 1.0f - offset, 1.0f,
            1.0f, 1.0f, 1.0f - offset,

            -1.0f, 1.0f, 1.0f,
            -1.0f + offset, 1.0f, 1.0f,
            -1.0f, 1.0f - offset, 1.0f,
            -1.0f, 1.0f, 1.0f - offset,

            -1.0f, -1.0f, 1.0f,
            -1.0f + offset, -1.0f, 1.0f,
            -1.0f, -1.0f + offset, 1.0f,
            -1.0f, -1.0f, 1.0f - offset,
        };

        Ogre::VertexElement2Vec vertexElements;
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));

        createStaticMesh("lineBox", indexBuffer, vertexElements, cubeVerticesCount, &c_originalVertices[0], Ogre::OT_LINE_LIST);
    }

    void ProgrammaticMeshGenerator::generateFullLineBox(){
        static const int cubeArraySize = 2 * 3 * 4;
        static const Ogre::uint16 c_indexData[cubeArraySize]{
            0, 0 + 1,
            0, 0 + 2,
            0, 0 + 3,

            4, 4 + 1,
            4, 4 + 2,
            4, 4 + 3,

            8, 8 + 1,
            8, 8 + 2,
            8, 8 + 3,

            12, 12 + 1,
            12, 12 + 2,
            12, 12 + 3,
        };

        Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer(cubeArraySize, &c_indexData[0]);


        static const int cubeVerticesCount = 3 * 4 * 4;
        static const float offset = 0.2f;
        static const float c_originalVertices[cubeVerticesCount] = {
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,

            1.0f, -1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,

            1.0f, 1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,

            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
        };

        Ogre::VertexElement2Vec vertexElements;
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));

        createStaticMesh("FullLineBox", indexBuffer, vertexElements, cubeVerticesCount, &c_originalVertices[0], Ogre::OT_LINE_LIST);
    }

    void ProgrammaticMeshGenerator::generateLineVao(){
        static const int cubeArraySize = 2 * 1;
        static const Ogre::uint16 c_indexData[cubeArraySize]{
            0, 1,
        };

        Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer(cubeArraySize, &c_indexData[0]);


        static const int cubeVerticesCount = 3 * 2;
        static const float c_originalVertices[cubeVerticesCount] = {
            0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f
        };

        Ogre::VertexElement2Vec vertexElements;
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));

        createStaticMesh("line", indexBuffer, vertexElements, cubeVerticesCount, &c_originalVertices[0], Ogre::OT_LINE_LIST);
    }

    void ProgrammaticMeshGenerator::generateLineSphere(){
        static const float radius = 1.0f;
        static const float PI_VAL = 3.141592653f;
        static const int NUM_CIRCLE_VIRTS = 30;
        static const int NUM_CIRCLES = 3;

        const int indexSize = NUM_CIRCLE_VIRTS * 2 * NUM_CIRCLES;
        Ogre::uint16 indexData[indexSize];

        for(int c = 0; c < NUM_CIRCLES; c++){
            for(int i = 0; i < NUM_CIRCLE_VIRTS; i++){
                indexData[c * (NUM_CIRCLE_VIRTS * 2) + i * 2] = (c * NUM_CIRCLE_VIRTS) + i;
                if(i == NUM_CIRCLE_VIRTS-1){
                    indexData[c * (NUM_CIRCLE_VIRTS * 2) + i * 2 + 1] = (c * NUM_CIRCLE_VIRTS) + 0;
                }else{
                    indexData[c * (NUM_CIRCLE_VIRTS * 2) + i * 2 + 1] = (c * NUM_CIRCLE_VIRTS) + i + 1;
                }
            }
        }

        Ogre::IndexBufferPacked *indexBuffer = createIndexBuffer(indexSize, &indexData[0]);

        const int vertexListSize = NUM_CIRCLE_VIRTS * 3 * NUM_CIRCLES;
        float originalVertices[vertexListSize];
        for(int c = 0; c < NUM_CIRCLES; c++){
            for(int i = 0; i < NUM_CIRCLE_VIRTS; i++){
                const float piVal = (2 * PI_VAL) * (float(i) / NUM_CIRCLE_VIRTS);
                const float x = radius * cosf(piVal);
                const float z = radius * sinf(piVal);
                if(c == 0){
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3] = x;
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3 + 1] = 0.0f;
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3 + 2] = z;
                }else if(c == 1){
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3] = 0.0f;
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3 + 1] = x;
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3 + 2] = z;
                }else{
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3] = x;
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3 + 1] = z;
                    originalVertices[c * (NUM_CIRCLE_VIRTS * 3) + i * 3 + 2] = 0.0f;
                }
            }
        }

        Ogre::VertexElement2Vec vertexElements;
        vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));

        createStaticMesh("LineSphere", indexBuffer, vertexElements, vertexListSize, &originalVertices[0], Ogre::OT_LINE_LIST);
    }

    Ogre::IndexBufferPacked* ProgrammaticMeshGenerator::createIndexBuffer(int cubeArraySize, const Ogre::uint16* indexData){
        Ogre::IndexBufferPacked *indexBuffer = 0;

        Ogre::uint16 *cubeIndices = reinterpret_cast<Ogre::uint16*>( OGRE_MALLOC_SIMD(sizeof(Ogre::uint16) * cubeArraySize, Ogre::MEMCATEGORY_GEOMETRY));
        //memcpy( cubeIndices, indexData, sizeof( *indexData ) );
        memcpy( cubeIndices, indexData, sizeof(Ogre::uint16) * cubeArraySize );

        Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
        Ogre::VaoManager *vaoManager = renderSystem->getVaoManager();

        try{
            //Actually create an index buffer and assign it to the pointer created earlier.
            //Also populate the index buffer with these values.
            //This goes, type, number of indices, Buffer type, the actual data, keep as shadow
            indexBuffer = vaoManager->createIndexBuffer(Ogre::IndexBufferPacked::IT_16BIT, cubeArraySize, Ogre::BT_IMMUTABLE, cubeIndices, true);
        }
        catch(Ogre::Exception &e){
            OGRE_FREE_SIMD( indexBuffer, Ogre::MEMCATEGORY_GEOMETRY );
            indexBuffer = 0;
            throw e;
        }

        return indexBuffer;
    }


    Ogre::MeshPtr ProgrammaticMeshGenerator::createStaticMesh(const Ogre::String& name, Ogre::IndexBufferPacked *indexBuffer, const Ogre::VertexElement2Vec& elemVec, int arraySize, const float* vertexData, Ogre::OperationType t){
        Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
        Ogre::VaoManager *vaoManager = renderSystem->getVaoManager();

        Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().createManual(name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        Ogre::SubMesh *subMesh = mesh->createSubMesh();

        float *cubeVertices = reinterpret_cast<float*>( OGRE_MALLOC_SIMD(sizeof(float) * arraySize, Ogre::MEMCATEGORY_GEOMETRY ) );

        memcpy(cubeVertices, vertexData, sizeof(float) * arraySize);

        Ogre::VertexBufferPacked *vertexBuffer = 0;
        try{
            vertexBuffer = vaoManager->createVertexBuffer(elemVec, arraySize/3, Ogre::BT_IMMUTABLE, cubeVertices, true);
        }catch(Ogre::Exception &e){
            vertexBuffer = 0;
            throw e;
        }

        Ogre::VertexBufferPackedVec vertexBuffers;
        vertexBuffers.push_back(vertexBuffer);

        Ogre::VertexArrayObject* vao = vaoManager->createVertexArrayObject(vertexBuffers, indexBuffer, t);

        subMesh->mVao[Ogre::VpNormal].push_back(vao);
        subMesh->mVao[Ogre::VpShadow].push_back(vao);

        mesh->_setBounds( Ogre::Aabb( Ogre::Vector3::ZERO, Ogre::Vector3::UNIT_SCALE ), false );
        mesh->_setBoundingSphereRadius( 1.732f );

        return mesh;
    }


}
