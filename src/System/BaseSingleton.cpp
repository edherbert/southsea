#include "BaseSingleton.h"

namespace Southsea{
    std::shared_ptr<PreviousProjects> BaseSingleton::mPreviousProjects;
    std::shared_ptr<TerrainEditorSettings> BaseSingleton::mTerrainEditorSettings;
    std::shared_ptr<DraggedResourceInfo> BaseSingleton::mDraggedResourceInfo;
    std::shared_ptr<BrushThumbnailManager> BaseSingleton::mBrushThumbnailManager;

    void BaseSingleton::initialise(
        std::shared_ptr<PreviousProjects> previousProjects,
        std::shared_ptr<TerrainEditorSettings> terrainEditorSettings,
        std::shared_ptr<DraggedResourceInfo> draggedResourceInfo,
        std::shared_ptr<BrushThumbnailManager> brushThumbnailManager
    ){
        mPreviousProjects = previousProjects;
        mTerrainEditorSettings = terrainEditorSettings;
        mDraggedResourceInfo = draggedResourceInfo;
        mBrushThumbnailManager = brushThumbnailManager;
    }

    std::shared_ptr<PreviousProjects> BaseSingleton::getPreviousProjects(){
        return mPreviousProjects;
    }

    std::shared_ptr<TerrainEditorSettings> BaseSingleton::getTerrainEditorSettings(){
        return mTerrainEditorSettings;
    }

    std::shared_ptr<DraggedResourceInfo> BaseSingleton::getDraggedResourceInfo(){
        return mDraggedResourceInfo;
    }

    std::shared_ptr<BrushThumbnailManager> BaseSingleton::getBrushThumbnailManager(){
        return mBrushThumbnailManager;
    }
}
