#pragma once

#include <string>
#include <vector>

#include "Map.h"

namespace Southsea{
    class MapsDirectory{
    public:
        MapsDirectory();
        MapsDirectory(const std::string& mapsDir);
        ~MapsDirectory();

        /**
        Find all the available maps in this directory.

        @param mapsOutput
        The output vector where the list of map directories should be placed.
        If there was an error reading the directory the list will be empty.

        @return
        True or false depending on whether the directory could be read.
        */
        bool findAvailableMaps(std::vector<std::string>& mapsOutput) const;

        /**
        Create a map, complete with directories with the specified name.
        @return
        True if the map was created. If a map with that name already exists in this directory this function will return false.
        */
        bool createMap(const std::string& mapName) const;

        void setDirectoryPath(const std::string& mapsDir);

        /**
        Get a maps class from this directory.

        @param mapName
        The name of the map to return. This should not be a path.
        */
        Map getMap(const std::string& mapName);

        const std::string& getMapsDirectory() { return mMapsDirectoryPath; }

    private:
        std::string mMapsDirectoryPath;
    };
}
