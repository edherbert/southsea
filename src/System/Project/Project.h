#pragma once

#include <string>

#include "AVSetupFile.h"

namespace filesystem{
    class path;
}

namespace Southsea{

    /**
    Wrapper around an avEngine project on the file system.
    */
    class Project{
    public:
        Project();
        //Constructs a project handle based on a directory. The directory should contain a project, complete with project things like an avSetup.cfg file.
        Project(const std::string& projectDirPath);
        //Construct a project handle with a name. The name is appended to the end of the directoryPath, and is useful if you want to specify a project within a directory.
        Project(const std::string& projectDirPath, const std::string& name);
        ~Project();

        /**
        Create the basic files of this project.
        */
        bool createBaseProjectFiles();

        const std::string& getProjectPath() const { return mProjectPath; }
        const AVSetupFile& getAVSetupFile() const { return mAVSetupFile; }

        /**
        Determine whether a path contains a valid avEngine project.
        */
        static bool _determineProjectPathValidity(const std::string& path);
        static bool _determineProjectPathValidity(const filesystem::path& path);

        bool isValid() const;

        bool operator==(const Project &pos) const;

    private:
        std::string mProjectPath;

        AVSetupFile mAVSetupFile;

        /**
        Create a minimal avSetup.cfg file in the provided directory.
        */
        void _createAVCfgFile(filesystem::path& p, const std::string& projectName);
    };
}
