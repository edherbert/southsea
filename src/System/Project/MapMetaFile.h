#pragma once

#include <string>
#include <memory>
#include "Editor/EditorSelectionTools.h"

namespace Southsea{
    class Map;

    /**
    A class to encapsulate the contents of a map metadata file.

    The metadata file is a json file which contains logic such as the previous selected chunk of this map.
    This class provides functionality to read and write these values to and from the disk.
    */
    class MapMetaFile{
    public:
        MapMetaFile(const Map& map);
        ~MapMetaFile();

        /**
        Parse the meta file, populating all the data fields with the relevant values.

        @returns
        False if there was an error reading the file contents. True otherwise.
        */
        bool parseFile();

        /**
        Write the settings currently in memory out to the file location.
        */
        bool writeFile();

        SelectionTool getCurrentSelectionTool() const { return currentSelectionTool; }
        void setCurrentSelectionTool(SelectionTool tool) { currentSelectionTool = tool; }

        int getMapChunkX() const { return mapChunks[0]; }
        int getMapChunkY() const { return mapChunks[1]; }
        void setMapChunkCoords(int x, int y) {
            mapChunks[0] = x;
            mapChunks[1] = y;
        }

        int getSwitcherGridCoordX() const { return switcherGridCoords[0]; }
        int getSwitcherGridCoordY() const { return switcherGridCoords[1]; }
        int getSwitcherGridZoom() const { return switcherGridZoom; }
        void setSwitcherGridCoords(int x, int y){
            switcherGridCoords[0] = x;
            switcherGridCoords[1] = y;
        }
        void setSwitcherGridZoom(int zoom) { switcherGridZoom = zoom; }

        const std::string& getResourceManagerCurrentDirectory() const { return mResourceManagerCurrentDirectory; }
        void setResourceManagerCurrentDirectory(const std::string& dir) { mResourceManagerCurrentDirectory = dir; }

        void setGridSnapEnabled(bool enabled) { mGridSnapEnabled = enabled; }
        bool getGridSnapEnabled() const { return mGridSnapEnabled; }

        void setDrawOuterTerrainsEnabled(bool enabled) { mDrawOuterTerrains = enabled; }
        bool getDrawOuterTerrainsEnabled() const { return mDrawOuterTerrains; }
        void setPhysicsShapeVisible(bool enabled) { mPhysicsShapesVisible = enabled; }
        bool getPhysicsShapeVisible() const { return mPhysicsShapesVisible; }
        void setNavMeshVisible(bool enabled) { mNavMeshVisible = enabled; }
        bool getNavMeshVisible() const { return mNavMeshVisible; }

    private:
        std::string mMetaFilePath;

        static const std::string FILE_NAME;

        void _resolveMetaFilePath(const Map& map);

        int mapChunks[2] = {0, 0};
        int switcherGridCoords[2] = {0, 0};
        int switcherGridZoom = 50;
        SelectionTool currentSelectionTool = SelectionTool::ObjectPicker;
        bool mGridSnapEnabled = false;

        bool mDrawOuterTerrains = false;
        bool mPhysicsShapesVisible = true;
        bool mNavMeshVisible = true;

        std::string mResourceManagerCurrentDirectory;
    };
}
