#pragma once

#include <string>
#include <set>

namespace Southsea{
    class Map{
    public:
        typedef std::pair<int, int> MapChunkCoordinate;

    public:
        Map();
        Map(const std::string& mapPath);
        ~Map();

        bool isValid() const { return mMapValid; }
        const std::string& getMapPath() const { return mMapPath; }
        const std::string& getMapName() const { return mMapName; }

        //Reset the values of this map, making it blank and invalid.
        void reset();

        //Scan this map for chunk entries. This largely just searches for directories.
        void scanChunkEntries();

        Map& operator=(const Map &map);

        /**
        Determine whether the specified chunk exists within the parsed chunk list.
        */
        bool chunkExists(MapChunkCoordinate chunk) const;

        /**
        Notify the map of the save of a chunk.
        This will update the data about which chunks actually exist.
        */
        void notifySave(MapChunkCoordinate chunk);

    private:
        std::string mMapPath;
        std::string mMapName = ""; //Derived from the map path.
        bool mMapValid;

        bool _checkMapValid(const std::string& mapPath) const;
        void _determineMapName();

        void _parseDirectoryEntry(const std::string& dirName);

        std::set<MapChunkCoordinate> mParsedChunks;

    };
}
