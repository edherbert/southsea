#include "AVSetupFile.h"

#include <OgreConfigFile.h>
#include "filesystem/path.h"
#include "OgreStringConverter.h"

#include <rapidjson/filereadstream.h>
#include <rapidjson/error/en.h>
#include <rapidjson/document.h>

#include <iostream>

namespace Southsea{
    const std::string AVSetupFile::AV_FILE_NAME = "avSetup.cfg";

    AVSetupFile::AVSetupFile()
        : mFilePath("") {
            _clearCachedValues();
    }

    AVSetupFile::AVSetupFile(const std::string& filePath)
        : mFilePath(filePath) {

    }

    AVSetupFile::~AVSetupFile(){

    }

    void AVSetupFile::setFilePath(const std::string& filePath){
        filesystem::path givenPath(filePath);

        _clearCachedValues();

        //I don't mandate that the file is called avSetup.cfg anymore.
        if(givenPath.exists() && givenPath.is_file()){
            mFilePath = filePath;
            return;
        }

        //In this case the path might just be some directory, so determine where the file would be.
        filesystem::path determined = givenPath / filesystem::path(AV_FILE_NAME);
        mFilePath = determined.str();

    }

    bool AVSetupFile::parse(){
        filesystem::path p(mFilePath);
        if(!p.exists() || p.is_directory()) return false;


        FILE* fp = fopen(mFilePath.c_str(), "r");
        char readBuffer[65536];
        rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
        rapidjson::Document d;
        d.ParseStream(is);
        fclose(fp);

        if(d.HasParseError()){
            std::cout << "Error parsing the setup file." << std::endl;
            std::cout << rapidjson::GetParseError_En(d.GetParseError()) << std::endl;

            return false;
        }


        //_processAVSetupDocument(d);

        rapidjson::Value::ConstMemberIterator itr;

        //Default values
        mSlotSize = 100;

        //Resolve the data directory first.
        itr = d.FindMember("DataDirectory");
        if(itr != d.MemberEnd() && itr->value.IsString()){

            if(!_resolveFSPath(filesystem::path(mFilePath).parent_path(), itr->value.GetString(), mDataDirectory)){
                mDataDirectory = "."; //If the data directory could not be determined (we were give a path that doesn't exist) make it the current directory.
                return false;
            }
        }

        const filesystem::path parentDirectory(mDataDirectory);

        itr = d.FindMember("MapsDirectory");
        if(itr != d.MemberEnd() && itr->value.IsString()){
            _resolveFSPath(parentDirectory, itr->value.GetString(), mMapsDirectory);
        }else{
            _resolveFSPath(parentDirectory, "maps", mMapsDirectory);
        }
        itr = d.FindMember("OgreResourcesFile");
        if(itr != d.MemberEnd() && itr->value.IsString()){
            _resolveFSPath(parentDirectory, itr->value.GetString(), mOgreResourcesFile);
        }else{
            //Default
            _resolveFSPath(parentDirectory, "OgreResources.cfg", mOgreResourcesFile);
        }
        itr = d.FindMember("WorldSlotSize");
        if(itr != d.MemberEnd() && itr->value.IsInt()){
            mSlotSize = itr->value.GetInt();
        }

        return true;
    }

    bool AVSetupFile::_resolveFSPath(const filesystem::path& dataDirectoryPath, const std::string &targetPath, std::string& outPath){

        filesystem::path out(targetPath);
        if(!out.is_absolute()){ //If the provided path is absolute just go with that. If it's relative it needs to be resolved.
            out = dataDirectoryPath / out;
            if(out.exists()) out = out.make_absolute();
        }

        if(!out.exists()){
            return false;
        }
        outPath = out.str();

        return true;
    }

    void AVSetupFile::_clearCachedValues(){
        mDataDirectory = "";
        mMapsDirectory = "";
        mOgreResourcesFile = "";
    }

    bool AVSetupFile::valid(){
        filesystem::path p(mFilePath);

        if(!p.exists() || !p.is_file()) return false;
        //If we know it's a file, check that the filename is correct.
        return p.filename() == AV_FILE_NAME;
    }

    bool AVSetupFile::_determineFSObjectWithPath(const std::string& dataDirectoryPath, const std::string &targetPath, filesystem::path& resolvedPath){
        filesystem::path dataDirPath(dataDirectoryPath);

        resolvedPath = filesystem::path(targetPath);
        if(!resolvedPath.is_absolute()){ //If the provided path is absolute just go with that. If it's relative it needs to be resolved.
            resolvedPath = dataDirPath / resolvedPath;
            if(resolvedPath.exists()) resolvedPath = resolvedPath.make_absolute();
        }

        if(!resolvedPath.exists()){
            return false;
        }

        return true;
    }
}
