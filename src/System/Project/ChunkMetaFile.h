#pragma once

#include <string>
#include "OgreVector3.h"
#include "OgreQuaternion.h"

namespace Southsea{
    class Chunk;
    class SceneTreeManager;
    class Editor;
    class NavMeshManager;

    /**
    A class to encapsulate the contents of a chunk metadata file.

    Chunk metadata files are similar to map metadata files, although one is created per chunk.
    These files store data such as camera position, selected object information, etc.
    The sort of thing that changes per chunk.
    */
    class ChunkMetaFile{
    public:
        ChunkMetaFile();
        ~ChunkMetaFile();

        void setChunk(const Chunk& chunk);

        bool parseFile(std::shared_ptr<SceneTreeManager> sceneTreeManager, std::shared_ptr<NavMeshManager> navManager);
        bool writeFile(const std::shared_ptr<SceneTreeManager> sceneTreeManager, std::shared_ptr<NavMeshManager> navManager);

        void setCameraPosition(const Ogre::Vector3& pos) { mCameraPos = pos; }
        const Ogre::Vector3& getCameraPosition() const { return mCameraPos; }
        void setCameraOrientation(const Ogre::Quaternion& orientation, float yaw, float pitch){
            mCameraOrientation = orientation;
            mCameraYaw = yaw;
            mCameraPitch = pitch;
        }
        const Ogre::Quaternion& getCameraOrientation() const { return mCameraOrientation; }
        float getCameraYaw() const { return mCameraYaw; }
        float getCameraPitch() const { return mCameraPitch; }

    private:
        std::string mMetaFilePath;

        Ogre::Vector3 mCameraPos;
        Ogre::Quaternion mCameraOrientation;
        float mCameraYaw, mCameraPitch;
    };
}
