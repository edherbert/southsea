#pragma once

#include <string>
#include <vector>
#include "Editor/EditorSelectionTools.h"

namespace Southsea{
    class Map;

    /**
    Stores functions relating to the map's nav file.
    The file, nav.json, stored in the map's parent directory contains details about what nav meshes are loaded and active at a time.
    The values in the file describe what nav meshes exist in a map and what their parameters are.
    These values are map global, because in order to tile a streamable world, each nav mesh chunk must contain tiles of the same dimensions.
    */
    class MapNavFile{
    public:
        struct MeshData{
            std::string meshName;
            int tileSize;
            float cellSize;
        };

    private:
        static const std::string FILE_NAME;
    public:
        MapNavFile(const Map& map);
        ~MapNavFile();

        /**
        Parse the val file, populating all the data fields with the relevant values.

        @returns
        False if there was an error reading the file contents. True otherwise.
        */
        bool parseFile();

        /**
        Write the settings currently in memory out to the file location.
        */
        bool writeFile();

        const std::string& getMeshName(int mesh) const;
        void setMeshName(int mesh, const std::string& targetName);

    private:
        std::string mNavFilePath;

        std::vector<MeshData> mMeshData;

        void _resolveNavFilePath(const Map& map);

    public:
        const std::vector<MeshData>& getMeshData() const { return mMeshData; };
    };
}
