#include "MapNavFile.h"

#include "Map.h"
#include "filesystem/path.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include <cstdio>
#include <fstream>
#include <algorithm>

#include <iostream>

namespace Southsea{
    const std::string MapNavFile::FILE_NAME = "nav.json";

    MapNavFile::MapNavFile(const Map& map){

        _resolveNavFilePath(map);
    }

    MapNavFile::~MapNavFile(){

    }

    void MapNavFile::_resolveNavFilePath(const Map& map){
        const filesystem::path filePath( filesystem::path(map.getMapPath()) / filesystem::path(FILE_NAME));

        mNavFilePath = filePath.str();
    }

    bool MapNavFile::parseFile(){
        mMeshData.clear();

        const filesystem::path filePath(mNavFilePath);
        if(!filePath.exists() || !filePath.is_file()) return false;


        FILE* fp = fopen(filePath.str().c_str(), "r");

        {
            using namespace rapidjson;

            char readBuffer[65536];
            FileReadStream is(fp, readBuffer, sizeof(readBuffer));

            Document doc;
            doc.ParseStream(is);

            if(!doc.IsObject()) return false; //Error reading the file.

            auto arrayIt = doc.FindMember("Meshes");
            if(arrayIt != doc.MemberEnd()){
                if(!arrayIt->value.IsArray()) return false;
                const rapidjson::SizeType arraySize = arrayIt->value.Size();
                for(rapidjson::SizeType i=0; i<arraySize; ++i){
                    if(!arrayIt->value[i].IsObject()) continue;
                    MeshData newMeshData;
                    //rapidjson::Value objType = arrayIt->value;

                    auto it = arrayIt->value[i].FindMember("name");
                    if(it != arrayIt->value[i].MemberEnd() && it->value.IsString()){
                        const char* meshName = it->value.GetString();
                        newMeshData.meshName = meshName;
                    }else return false;

                    it = arrayIt->value[i].FindMember("tileSize");
                    if(it != arrayIt->value[i].MemberEnd() && it->value.IsNumber()){
                        int meshTileSize = it->value.GetInt();
                        newMeshData.tileSize = meshTileSize;
                    }else return false;

                    it = arrayIt->value[i].FindMember("cellSize");
                    if(it != arrayIt->value[i].MemberEnd() && it->value.IsNumber()){
                        float meshCellSize = it->value.GetDouble();
                        newMeshData.cellSize = meshCellSize;
                    }else return false;

                    mMeshData.push_back(newMeshData);
                }
            }

        }

        return true;
    }

    bool MapNavFile::writeFile(){

        std::cout << "Writing map nav file." << '\n';

        rapidjson::Document document;

        // define the document as an object rather than an array
        document.SetObject();

        // must pass an allocator when the object may need to allocate memory
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        rapidjson::Value parentArray(rapidjson::kArrayType);
        for(const MeshData& meshData : mMeshData){
            rapidjson::Value meshObj(rapidjson::kObjectType);

            meshObj.AddMember("name", rapidjson::Value(meshData.meshName.c_str(), allocator), allocator);
            meshObj.AddMember("tileSize", meshData.tileSize, allocator);
            meshObj.AddMember("cellSize", meshData.cellSize, allocator);

            parentArray.PushBack(meshObj, allocator);
        }

        document.AddMember("Meshes", parentArray, allocator);


        // 3. Stringify the DOM
        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);

        std::ofstream myfile;
        myfile.open(mNavFilePath);
        myfile << buffer.GetString();
        myfile.close();

        return true;
    }
}
