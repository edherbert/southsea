#include "ChunkMetaFile.h"

#include "Editor/Chunk/Chunk.h"
#include "filesystem/path.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Chunk/NavMesh/NavMeshManager.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"

#include <fstream>
#include <iostream>

namespace Southsea{
    const char* FILE_NAME = "southseaChunkMeta.json";

    ChunkMetaFile::ChunkMetaFile()
        : mMetaFilePath(""),
        mCameraPos(Ogre::Vector3(0, 50, 100)),
        mCameraOrientation(Ogre::Quaternion::ZERO),
        mCameraYaw(0),
        mCameraPitch(0) {

    }

    ChunkMetaFile::~ChunkMetaFile(){

    }

    void ChunkMetaFile::setChunk(const Chunk& chunk){
        const filesystem::path filePath( filesystem::path(chunk.getChunkDirectory()) / filesystem::path(FILE_NAME));

        mMetaFilePath = filePath.str();
    }

    bool ChunkMetaFile::parseFile(std::shared_ptr<SceneTreeManager> sceneTreeManager, std::shared_ptr<NavMeshManager> navManager){
        if(mMetaFilePath.empty()) return false;

        const filesystem::path filePath(mMetaFilePath);
        if(!filePath.exists() || !filePath.is_file()) return false;


        FILE* fp = fopen(filePath.str().c_str(), "r");

        {
            using namespace rapidjson;

            char readBuffer[65536];
            FileReadStream is(fp, readBuffer, sizeof(readBuffer));

            Document doc;
            doc.ParseStream(is);

            if(!doc.IsObject()) return false; //Error reading the file.

            auto it = doc.FindMember("cameraPosition");
            if(it != doc.MemberEnd()){
                const rapidjson::SizeType arraySize = (std::min)( 3u, it->value.Size() );
                for(rapidjson::SizeType i=0; i<arraySize; ++i){
                    if(it->value[i].IsNumber()){
                        mCameraPos[i] = it->value[i].GetDouble();
                    }
                }
            }

            it = doc.FindMember("cameraOrientation");
            if(it != doc.MemberEnd()){
                const rapidjson::SizeType arraySize = (std::min)( 4u, it->value.Size() );
                for(rapidjson::SizeType i=0; i<arraySize; ++i){
                    if(it->value[i].IsNumber()){
                        mCameraOrientation[i] = it->value[i].GetDouble();
                    }
                }
            }

            it = doc.FindMember("cameraYaw");
            if(it != doc.MemberEnd()){
                if(it->value.IsDouble()) mCameraYaw = it->value.GetDouble();
            }
            it = doc.FindMember("cameraPitch");
            if(it != doc.MemberEnd()){
                if(it->value.IsDouble()) mCameraPitch = it->value.GetDouble();
            }

            it = doc.FindMember("selectedObjects");
            if(it != doc.MemberEnd()){
                const rapidjson::SizeType arraySize = it->value.Size();
                for(rapidjson::SizeType i=0; i<arraySize; ++i){
                    if(it->value[i].IsInt()){
                        EntryId id = static_cast<EntryId>(it->value[i].GetInt());
                        sceneTreeManager->_setSelectedId(id);
                        //mCameraOrientation[i] = it->value[i].GetDouble();
                    }
                }
            }

            it = doc.FindMember("terrainSelected");
            if(it != doc.MemberEnd()){
                if(it->value.IsBool()){
                    bool value = it->value.GetBool();
                    sceneTreeManager->_setTerrainSelected(value);
                }
            }

            it = doc.FindMember("currentNavMesh");
            if(it != doc.MemberEnd()){
                if(it->value.IsInt()){
                    int value = it->value.GetInt();
                    NavMeshId targetId = -1;
                    if(value >= 0 && value < INVALID_NAV_MESH_ID) targetId = static_cast<NavMeshId>(value);
                    navManager->setCurrentSelectedMesh(targetId);
                }
            }
        }

        return true;
    }

    bool ChunkMetaFile::writeFile(const std::shared_ptr<SceneTreeManager> sceneTreeManager, std::shared_ptr<NavMeshManager> navManager){
        std::cout << "Writing chunk meta file." << '\n';

        rapidjson::Document document;

        document.SetObject();

        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        {
            rapidjson::Value array(rapidjson::kArrayType);
            array.PushBack(mCameraPos.x, allocator).PushBack(mCameraPos.y, allocator).PushBack(mCameraPos.z, allocator);

            document.AddMember("cameraPosition", array, allocator);
        }

        {
            rapidjson::Value array(rapidjson::kArrayType);
            array.PushBack(mCameraOrientation.w, allocator).PushBack(mCameraOrientation.x, allocator).PushBack(mCameraOrientation.y, allocator).PushBack(mCameraOrientation.z, allocator);

            document.AddMember("cameraOrientation", array, allocator);
        }

        {
            rapidjson::Value yawVal(mCameraYaw);
            document.AddMember("cameraYaw", yawVal, allocator);

            rapidjson::Value pitchVal(mCameraPitch);
            document.AddMember("cameraPitch", pitchVal, allocator);
        }

        {
            const std::set<EntryId>& selectedIds = sceneTreeManager->getSelectedIds();
            rapidjson::Value array(rapidjson::kArrayType);

            for(const EntryId& e : selectedIds){
                int value = static_cast<int>(e);
                array.PushBack(value, allocator);
            }

            document.AddMember("selectedObjects", array, allocator);
        }

        {
            rapidjson::Value terrainSelected(sceneTreeManager->isTerrainSelected());
            document.AddMember("terrainSelected", terrainSelected, allocator);
        }

        document.AddMember("currentNavMesh", static_cast<int>(navManager->getCurrentSelectedMesh()), allocator);

        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);

        std::cout << "Writing chunk meta file." << std::endl;
        std::cout << buffer.GetString() << std::endl;

        std::ofstream myfile;
        myfile.open(mMetaFilePath);
        myfile << buffer.GetString();
        myfile.close();

        return true;
    }
}
