#include "MapMetaFile.h"

#include "Map.h"
#include "filesystem/path.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include <cstdio>
#include <fstream>
#include <algorithm>

#include <iostream>

namespace Southsea{
    const std::string MapMetaFile::FILE_NAME = "southseaMapMeta.json";

    MapMetaFile::MapMetaFile(const Map& map){

        _resolveMetaFilePath(map);
    }

    MapMetaFile::~MapMetaFile(){

    }

    void MapMetaFile::_resolveMetaFilePath(const Map& map){
        const filesystem::path filePath( filesystem::path(map.getMapPath()) / filesystem::path(FILE_NAME));

        mMetaFilePath = filePath.str();
    }

    bool MapMetaFile::parseFile(){
        const filesystem::path filePath(mMetaFilePath);
        if(!filePath.exists() || !filePath.is_file()) return false;


        FILE* fp = fopen(filePath.str().c_str(), "r");

        {
            using namespace rapidjson;

            char readBuffer[65536];
            FileReadStream is(fp, readBuffer, sizeof(readBuffer));

            Document doc;
            doc.ParseStream(is);

            if(!doc.IsObject()) return false; //Error reading the file.

            auto it = doc.FindMember("selectedChunk");
            if(it != doc.MemberEnd()){
                const rapidjson::SizeType arraySize = (std::min)( 2u, it->value.Size() ); //std::min is wrapped like this because windows.h is included by the filesystem library, which has its own min macro.
                for(rapidjson::SizeType i=0; i<arraySize; ++i){
                    if(it->value[i].IsNumber()){
                        mapChunks[i] = it->value[i].GetInt();
                    }
                }
            }

            it = doc.FindMember("switcherGridCoords");
            if(it != doc.MemberEnd()){
                const rapidjson::SizeType arraySize = (std::min)( 2u, it->value.Size() );
                for(rapidjson::SizeType i=0; i<arraySize; ++i){
                    if(it->value[i].IsNumber()){
                        switcherGridCoords[i] = it->value[i].GetInt();
                    }
                }
            }

            it = doc.FindMember("switcherGridZoom");
            if(it != doc.MemberEnd()){
                if(it->value.IsNumber()) switcherGridZoom = it->value.GetInt();
            }

            it = doc.FindMember("currentSelectionTool");
            if(it != doc.MemberEnd()){
                if(it->value.IsNumber()) {
                    int val = it->value.GetInt();
                    switch(val){
                        case 0: currentSelectionTool = SelectionTool::ObjectPicker; break;
                        case 1: currentSelectionTool = SelectionTool::TerrainPlacer; break;
                        case 2: currentSelectionTool = SelectionTool::ObjectScale; break;
                        case 3: currentSelectionTool = SelectionTool::ObjectOrientate; break;
                        default: currentSelectionTool = SelectionTool::ObjectPicker; break;
                    }
                }
            }

            it = doc.FindMember("resourceManagerCurrentDirectory");
            if(it != doc.MemberEnd()){
                if(it->value.IsString()) mResourceManagerCurrentDirectory = it->value.GetString();
            }

            it = doc.FindMember("gridSnapEnabled");
            if(it != doc.MemberEnd()){
                if(it->value.IsBool()){
                    mGridSnapEnabled = it->value.GetBool();
                }
            }

            it = doc.FindMember("drawOuterTerrains");
            if(it != doc.MemberEnd()){
                if(it->value.IsBool()){
                    mDrawOuterTerrains = it->value.GetBool();
                }
            }

            it = doc.FindMember("physicsShapeVisible");
            if(it != doc.MemberEnd()){
                if(it->value.IsBool()){
                    mPhysicsShapesVisible = it->value.GetBool();
                }
            }

            it = doc.FindMember("navMeshVisible");
            if(it != doc.MemberEnd()){
                if(it->value.IsBool()){
                    mNavMeshVisible = it->value.GetBool();
                }
            }

        }

        return true;
    }

    bool MapMetaFile::writeFile(){

        std::cout << "Writing map meta file." << '\n';

        rapidjson::Document document;

        // define the document as an object rather than an array
        document.SetObject();

        // must pass an allocator when the object may need to allocate memory
        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        {
            rapidjson::Value array(rapidjson::kArrayType);
            array.PushBack(mapChunks[0], allocator).PushBack(mapChunks[1], allocator);

            document.AddMember("selectedChunk", array, allocator);
        }
        {
            rapidjson::Value array(rapidjson::kArrayType);
            array.PushBack(switcherGridCoords[0], allocator).PushBack(switcherGridCoords[1], allocator);

            document.AddMember("switcherGridCoords", array, allocator);
        }
        {
            rapidjson::Value val(switcherGridZoom);

            document.AddMember("switcherGridZoom", val, allocator);
        }
        {
            rapidjson::Value val(mResourceManagerCurrentDirectory.c_str(), allocator);

            document.AddMember("resourceManagerCurrentDirectory", val, allocator);
        }
        {
            rapidjson::Value val(mDrawOuterTerrains);

            document.AddMember("drawOuterTerrains", val, allocator);
        }

        {
            document.AddMember("physicsShapeVisible", rapidjson::Value(mPhysicsShapesVisible), allocator);
            document.AddMember("navMeshVisible", rapidjson::Value(mNavMeshVisible), allocator);
        }

        {
            int targetVal = 0;
            switch(currentSelectionTool){
                case SelectionTool::ObjectPicker: targetVal = 0; break;
                case SelectionTool::TerrainPlacer: targetVal = 1; break;
                case SelectionTool::ObjectScale: targetVal = 2; break;
                case SelectionTool::ObjectOrientate: targetVal = 3; break;
                default: targetVal = 0; break;
            }
            rapidjson::Value val(targetVal);

            document.AddMember("currentSelectionTool", val, allocator);
        }

        {
            document.AddMember("gridSnapEnabled", mGridSnapEnabled, allocator);
        }


        // 3. Stringify the DOM
        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);

        std::cout << "Writing map meta file." << std::endl;
        std::cout << buffer.GetString() << std::endl;

        std::ofstream myfile;
        myfile.open(mMetaFilePath);
        myfile << buffer.GetString();
        myfile.close();

        return true;
    }
}
