#include "Project.h"

#include <fstream>
#include <iostream>
#include "filesystem/path.h"

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"

#include <cassert>

namespace Southsea{

    Project::Project(){

    }

    Project::Project(const std::string& projectDirPath)
        : mProjectPath(projectDirPath){

        mAVSetupFile.setFilePath(mProjectPath);
        mAVSetupFile.parse();
    }

    Project::Project(const std::string& projectDirPath, const std::string& name)
        : mProjectPath( (filesystem::path(projectDirPath) / filesystem::path(name)).str() ) {

        mAVSetupFile.setFilePath(mProjectPath);
        mAVSetupFile.parse();
    }

    Project::~Project(){

    }

    bool Project::createBaseProjectFiles(){
        filesystem::path dirPath(mProjectPath);

        if(dirPath.exists()) return false; // I don't want to risk overriding anything, so if that directory (or file) already exists then don't bother.

        if(!filesystem::create_directory(dirPath)){
            return false; //Failed to create
        }
        assert(dirPath.exists()); //If we've got to this point the directory should definitely exist.

        //Start populating the new directory!
        _createAVCfgFile(dirPath, "EmptyProject");

        filesystem::create_directory(dirPath / filesystem::path("mapsDir"));


        return true;
    }


    bool Project::_determineProjectPathValidity(const filesystem::path& projPath){
        if(!projPath.exists()) return false;

        if(projPath.is_file()){
            if(projPath.filename() == AVSetupFile::AV_FILE_NAME){
                return true; //If the file pointed to is an avSetup.cfg file then it's valid.
            }
        }

        //Should be a valid directory at this point.
        //If the path is a directory, a project is valid if it contains an avSetup.cfg file.
        {
            filesystem::path avFilePath = projPath / filesystem::path(AVSetupFile::AV_FILE_NAME);
            if(avFilePath.exists() && avFilePath.is_file()) return true;
        }

        return false;
    }

    bool Project::_determineProjectPathValidity(const std::string& path){
        return _determineProjectPathValidity(filesystem::path(path));
    }

    bool Project::isValid() const{
        return _determineProjectPathValidity(mProjectPath);
    }

    bool Project::operator==(const Project &pos) const{
        return mProjectPath == pos.getProjectPath();
    }

    void Project::_createAVCfgFile(filesystem::path& p, const std::string& projectName){
        filesystem::path pathHandle = p / filesystem::path(AVSetupFile::AV_FILE_NAME);

        if(pathHandle.exists() && pathHandle.is_file()){
            pathHandle.remove_file();
        }


        std::cout << "Creating avSetup.cfg file." << '\n';

        rapidjson::Document document;
        document.SetObject();

        rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

        document.AddMember("ProjectName", rapidjson::Value(projectName.c_str(), allocator), allocator);
        document.AddMember("DataDirectory", ".", allocator);
        document.AddMember("MapsDirectory", "mapsDir", allocator);

        rapidjson::StringBuffer buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);

        std::cout << buffer.GetString() << std::endl;

        std::ofstream myfile;
        myfile.open(pathHandle.str().c_str());
        myfile << buffer.GetString();
        myfile.close();
    }
}
