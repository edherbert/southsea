#include "Map.h"

#include "filesystem/path.h"

#include "Editor/Chunk/Chunk.h"
#include <regex>

#include <iostream>

#ifndef _WIN32
    //Linux and mac
    #include <sys/types.h>
    #include <dirent.h>
#else
    //Windows
    #include <windows.h>
    #include <tchar.h>
    #include <stdio.h>
    #include <strsafe.h>
#endif

namespace Southsea{
    Map::Map()
        : mMapValid(false) {

    }

    Map::Map(const std::string& mapPath)
        : mMapPath(mapPath),
          mMapValid(_checkMapValid(mapPath)){
        _determineMapName();
    }

    Map::~Map(){

    }

    bool Map::_checkMapValid(const std::string& mapPath) const{
        const filesystem::path dirPath(mapPath);

        if(!dirPath.exists() || ! dirPath.is_directory()) return false;

        return true;
    }

    void Map::reset(){
        mMapPath = "";
        mMapValid = false;

        mParsedChunks.clear();
    }

    Map& Map::operator=(const Map &map){

        mMapValid = map.isValid();
        mMapPath = map.getMapPath();
        mMapName = map.getMapName();

        return *this;
    }

    void Map::_determineMapName(){
        mMapName = filesystem::path(mMapPath).filename();
    }

    void Map::scanChunkEntries(){
        mParsedChunks.clear();

        #ifndef _WIN32
            DIR* dirp = opendir(mMapPath.c_str());
            struct dirent * dp;
            while ((dp = readdir(dirp)) != NULL) {
                std::string dir(dp->d_name);
                if(dir == "." || dir == "..") continue;
                if(dp->d_type != DT_DIR) continue; //Has to be a directory.
                _parseDirectoryEntry(dir);
            }
            closedir(dirp);
        #else
            WIN32_FIND_DATA ffd;
            TCHAR szDir[MAX_PATH];
            HANDLE hFind = INVALID_HANDLE_VALUE;

            StringCchCopy(szDir, MAX_PATH, (mMapPath + "/*").c_str());
            hFind = FindFirstFile(szDir, &ffd);

           do
           {
              if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
                 const std::string dir(ffd.cFileName);
                 if (dir == "." || dir == "..") continue;
                 _parseDirectoryEntry(dir);
              }
           }
           while(FindNextFile(hFind, &ffd) != 0);
        #endif
    }

    void Map::_parseDirectoryEntry(const std::string& dirName){
        static const int maxChunkCoords = Chunk::CHUNK_DIGITS * 2;

        //For validity checking, there are only so many lengths the string can be.
        //+2 to take account of the potential -.
        if( !(dirName.size() >= maxChunkCoords && dirName.size() <= maxChunkCoords + 2) ) return;

        static const std::regex e("(-?\\d{4}){2}");
        if(!std::regex_match(dirName, e)) return;

        std::string searchString(dirName);
        std::smatch m;
        static const std::regex reg("-?\\d{4}");

        std::string outStr[2];

        unsigned char count = 0;
        while (std::regex_search (searchString, m, reg)) {
            outStr[count] = m[0];

            count++;
            searchString = m.suffix().str();
        }

        if(count != 2) return; //Each chunk directory should have two coordiate values in it. If this isn't the case then it's buggered.

        //At this point the parsed chunk is considered valid.
        mParsedChunks.insert({
            std::stoi(outStr[0]),
            std::stoi(outStr[1])
        });
    }

    bool Map::chunkExists(MapChunkCoordinate chunk) const{
        return mParsedChunks.find(chunk) != mParsedChunks.end();
    }

    void Map::notifySave(MapChunkCoordinate chunk){
        if(chunkExists(chunk)) return;

        //This newly saved chunk is not known about. Re-scan the directory to find the latest chunks.
        scanChunkEntries();
    }
}
