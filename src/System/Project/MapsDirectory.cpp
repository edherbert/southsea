#include "MapsDirectory.h"

#include "filesystem/path.h"

#ifndef _WIN32
    //Linux and mac
    #include <sys/types.h>
    #include <dirent.h>
#else
    #include <windows.h>
    #include <tchar.h>
    #include <stdio.h>
    #include <strsafe.h>
#endif

namespace Southsea{
    MapsDirectory::MapsDirectory(){

    }

    MapsDirectory::MapsDirectory(const std::string& mapsDir)
        : mMapsDirectoryPath(mapsDir) {

    }

    MapsDirectory::~MapsDirectory(){

    }

    void MapsDirectory::setDirectoryPath(const std::string& mapsDir){
        mMapsDirectoryPath = mapsDir;
    }

    bool MapsDirectory::findAvailableMaps(std::vector<std::string>& mapsOutput) const{
        mapsOutput.clear(); //Clear the output regardless of whether anything was found or not.

        filesystem::path dirPath(mMapsDirectoryPath);
        if(!dirPath.exists() || !dirPath.is_directory()) return false;

        //Start reading
        #ifndef _WIN32
            DIR* dirp = opendir(mMapsDirectoryPath.c_str());
            struct dirent * dp;
            while ((dp = readdir(dirp)) != NULL) {
                std::string dir(dp->d_name);
                if(dir == "." || dir == "..") continue;
                if(dp->d_type != DT_DIR) continue; //Has to be a directory.
                mapsOutput.push_back(dir);
            }
            closedir(dirp);
        #else
            //Windows stuff.
            WIN32_FIND_DATA ffd;
            TCHAR szDir[MAX_PATH];
            HANDLE hFind = INVALID_HANDLE_VALUE;

            StringCchCopy(szDir, MAX_PATH, (mMapsDirectoryPath + "/*").c_str());
            hFind = FindFirstFile(szDir, &ffd);

           do
           {
              if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
                 const std::string dir(ffd.cFileName);
                 if (dir == "." || dir == "..") continue;
                 mapsOutput.push_back(dir);
              }
           }
           while(FindNextFile(hFind, &ffd) != 0);
        #endif

        return true;
    }

    bool MapsDirectory::createMap(const std::string& mapName) const{
        const filesystem::path dirPath(mMapsDirectoryPath);
        if(!dirPath.exists() || !dirPath.is_directory()) return false;

        const filesystem::path mapPath = dirPath / filesystem::path(mapName);
        if(mapPath.exists()) return false; //If a map with that name already exists, for instance.

        filesystem::create_directory(mapPath);

        return true;
    }

    Map MapsDirectory::getMap(const std::string& mapName){
        const filesystem::path dirPath(mMapsDirectoryPath);
        if(!dirPath.exists() || !dirPath.is_directory()) return Map();

        const filesystem::path mapPath = dirPath / filesystem::path(mapName);
        if(!mapPath.exists()){
            //That map doesn't exist. Return an invalid map in this case.
            return Map();
        }

        return Map(mapPath.str());
    }
}
