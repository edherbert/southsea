#pragma once

#include <string>

namespace Ogre{
    class ConfigFile;
}

namespace filesystem{
    class path;
}

namespace Southsea{

    /**
    A class to wrap around the functionality of an avSetup.cfg file.
    */
    class AVSetupFile{
    public:
        AVSetupFile();
        AVSetupFile(const std::string& filePath);
        ~AVSetupFile();

        static const std::string AV_FILE_NAME;

        //Returns whether or not this setup file is valid (as in the path points to a file)
        bool valid();

        std::string getFilePath() const { return mFilePath; }

        const std::string& getMapsDirectory() const { return mMapsDirectory; }
        const std::string& getDataDirectory() const { return mDataDirectory; }
        const std::string& getOgreResourcesFile() const { return mOgreResourcesFile; }

        int getSlotSize() const { return mSlotSize; }

        //Set the path of the setup file. This will clear any cached values.
        void setFilePath(const std::string& filePath);
        //Call this after setting a file path either through the constructor or setFilePath. This will fill the class with values from the file.
        bool parse();

    private:
        std::string mFilePath;

        //I don't populate many of these variables until they are actually requested.
        //They're not always necessary, so that's why.
        std::string mDataDirectory;
        std::string mMapsDirectory;
        std::string mOgreResourcesFile;

        int mSlotSize = 100;

        enum class PathType{
            Directory,
            File
        };

        /**
        Determine the path to a directory supplied from paths which are intended to be obtained from the avSetup.cfg file.
        All paths mentioned in the avSetup directory are resolved relative to the dataDirectory (also mentioned in the avSetup.cfg file).
        This function will resolve an absolute path to whatever was mentioned in the file.

        @param dataDirectoryPath
        The path to the data directory mentioned in the avSetup.cfg file.
        @param targetPath
        The path you want to resolve, obtained from the avSetup.cfg file.
        @param resolvedPath
        Pointer to a string where an absolute path determined from the dataDirectoryPath and the targetPath should be inserted.
        If the path was not resolved as intended, false will be returned and this string will be set to empty.

        @return
        A boolean of whether or not the path could be resolved successfuly.
        */
        //bool _determineItemWithPath(const std::string &dataDirectoryPath, const std::string &targetPath, std::string* resolvedPath, PathType pathType);

        /**
        Determine the absolute path of a path described in the avSetup.cfg file.
        Paths need to be resolved relative to the data directory described in the setup file.

        @param file
        A config file which should be the loaded avSetup.cfg file, with all the correct values.
        @param pathKey
        The key (i.e MapsDirectory) of the entry in the avSetup.cfg file. This key will be queried to find the target path.
        @param determinedPath
        Pointer to a string where the output path should be inserted.
        If there was an error this will be set to an empty string.

        @return
        True or false depending on whether the operation was successful or not.
        */
        //bool _determinePath(Ogre::ConfigFile &file, const std::string &pathKey, std::string* determinedPath, PathType pathType);
        //bool _parseFile(Ogre::ConfigFile& file);

        bool _determineFSObjectWithPath(const std::string& dataDirectoryPath, const std::string &targetPath, filesystem::path& resolvedPath);

        /**
        Resolve the data directory from a config file.
        The data directory is always specified relative to the avSetup.cfg file.
        */
        //bool _resolveDataDirectory(Ogre::ConfigFile &file);

        bool _resolveFSPath(const filesystem::path& dataDirectoryPath, const std::string &targetPath, std::string& outPath);

        void _clearCachedValues();
    };
}
