#include "Base.h"

#include "Platforms/Window/SDL2Window.h"

#include "Gui/ImguiBase.h"
#include "BaseSingleton.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/SystemEvent.h"

#include "System/Setup/PreviousProjects.h"
#include "System/Setup/DevSetup.h"
#include "System/Editor/TerrainEditorSettings.h"
#include "System/Editor/DraggedResourceInfo.h"
#include "System/Editor/EditorResourceSetup.h"
#include "util/ProgrammaticMeshGenerator.h"
#include "System/Editor/BrushThumbnailManager.h"

#include "Editor/EditorSingleton.h"

#ifdef __APPLE__
    #include "Platforms/OgreSetup/MacOSOgreSetup.h"
#elif __linux__
    #include "Platforms/OgreSetup/LinuxOgreSetup.h"
#elif _WIN32
    #include "Platforms/OgreSetup/WindowsOgreSetup.h"
#endif

namespace Southsea{
    Base::Base(){
        mImguiBase = std::make_shared<ImguiBase>();
        mWindow = std::make_shared<SDL2Window>();

        _initialise();
    }

    Base::~Base(){

    }

    void Base::_initialise(){
        std::shared_ptr<PreviousProjects> previousProjects = std::make_shared<PreviousProjects>();
        std::shared_ptr<TerrainEditorSettings> terrainSettings = std::make_shared<TerrainEditorSettings>();
        std::shared_ptr<BrushThumbnailManager> thumbnailManager = std::make_shared<BrushThumbnailManager>();
        BaseSingleton::initialise(
            previousProjects,
            terrainSettings,
            std::make_shared<DraggedResourceInfo>(),
            thumbnailManager
        );

        mWindow->open();

        EventDispatcher::subscribe(EventType::System, SOUTHSEA_BIND(Base::systemEventReceiver));

        _setupOgre();

        mCamera->setPosition(Ogre::Vector3(0, 0, 200));
        mCamera->lookAt(Ogre::Vector3(0, 0, 0));

        mImguiBase->initialise(mSceneManager);
        mWindow->setupImgui();

        previousProjects->scanPreviousProjects();
        DevSetup::actuateSettings();

        terrainSettings->setupBrushes(thumbnailManager);
    }

    void Base::update(){
        if(mShuttingDown){
            shutdown();
            return;
        }

        mWindow->update();

        Editor* mEditor = EditorSingleton::getEditor();

        //Check if the editor is due to shutdown.
        //This is done first so I can correctly call the ogre shutdown functions without any scene rendering starting.
        if(mEditor){
            if(mEditor->isPendingShutdown()){
                if(mEditor->isCompleteShutdown()){
                    mShuttingDown = true;
                }
                EditorSingleton::stopEditor();
                mEditor = 0;
            }
        }

        //Update the render window textures. I need to have called SceneManager::updateSceneGraph(), which is called by RenderOneFrame, before I call this, which is why the two update functions are separate.
        //An editor creation (and therefore scene manager creation) can happen as part of imguiBase::update.
        //In this case the series of events would be, updateRenderables (updates nothing as no scene manager), Scene Manager created, renderOneFrame. Then next frame the updateRenderables is called, and things happen in the correct order.
        if(mEditor){
            mEditor->updateRenderables();
            mEditor->update();
        }

        mImguiBase->update();
        BaseSingleton::getDraggedResourceInfo()->prepareNextFrame();

        if(!mShuttingDown) mRoot->renderOneFrame();
    }

    bool Base::systemEventReceiver(const Event& e){
        const SystemEvent& systemEvent = (SystemEvent&)e;
        if(systemEvent.eventCategory() == SystemEventCategory::WindowCloseRequest){
            //A close was requested.
            //If there's no editor we shut everything down straight away.
            //If there is an editor, we just need to wait for it to tell us when it's ready to be shut down, so do nothing here.
            if(!EditorSingleton::getEditor()){
                mShuttingDown = true;
            }
        }

        return false;
    }

    void Base::_setupOgre(){
        #ifdef __APPLE__
            MacOSOgreSetup setup;
        #elif __linux__
            LinuxOgreSetup setup;
        #elif _WIN32
            WindowsOgreSetup setup;
        #endif

        mRoot = setup.setupRoot();

        setup.setupOgreWindow(mWindow.get());
        setup.setupOgreResources(mRoot);
        setup.setupHLMS(mRoot);

        setup.setupScene(mRoot, &mSceneManager, &mCamera);
        setup.setupCompositor(mRoot, mSceneManager, mCamera, mWindow->getRenderWindow());

        ProgrammaticMeshGenerator::createMesh();
        EditorResourceSetup::setupSimpleResources(mRoot);
    }

    void Base::shutdown(){
        mRoot->shutdown();
        mWindow->close();

        mHasShutdown = true;
    }
}
