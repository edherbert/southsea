#include "SystemSetup.h"

#include "SystemSettings.h"
#include <SDL.h>
#include <cassert>
#include <OgreConfigFile.h>
#include <OgreStringConverter.h>

#include "DevSetup.h"

#include "filesystem/path.h"

#if defined __linux__ || defined __APPLE__
    #include <unistd.h>
    #include <sys/types.h>
    #include <pwd.h>
#elif _WIN32
    #include <shlwapi.h>
    #include "shlobj.h"
#endif

namespace Southsea{
    void SystemSetup::setup(){
        //Get the master path.
        char *base_path = SDL_GetBasePath();
        SystemSettings::_masterPath = std::string(base_path);
        SDL_free(base_path);

        std::string configPath = _getSystemConfigPath();
        SystemSettings::_configPath = _setupConfigDirectory(configPath);

        filesystem::path userSettingsPath = filesystem::path(SystemSettings::_configPath) / filesystem::path("userSettings.cfg");
        SystemSettings::_userSettingsFilePath = userSettingsPath.str();

        _loadUserSettings(userSettingsPath);

        //Now the settings have been read, check if they're valid.
        updateEngineExecutablePath();

        DevSetup::parseDevFile();

    }

    //Should hopefully use move semantics when returning this string!
    std::string SystemSetup::_getSystemConfigPath(){
        #if defined __linux__ || defined __APPLE__
            const char *homedir;

            if ((homedir = getenv("HOME")) == NULL) {
                homedir = getpwuid(getuid())->pw_dir;
            }

            filesystem::path configPath = filesystem::path(homedir) / filesystem::path(".config");

            assert(configPath.exists() && "The .config path in this user's home directory doesn't exist.");

            return configPath.str();
        #elif _WIN32
            TCHAR szPath[MAX_PATH];
            SUCCEEDED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, 0, szPath));

            const std::string appData(szPath);

            return appData;
        #endif
    }

    void SystemSetup::writeUserSettingsFile(){
        assert(!SystemSettings::_userSettingsFilePath.empty());

        std::ofstream outFile;
        outFile.open(SystemSettings::_userSettingsFilePath);
        if(!outFile.is_open()) return;

        outFile << "EnginePath\t" << SystemSettings::_engineExecutablePath;

        outFile << "\n";

        outFile.close();
    }

    std::string SystemSetup::_setupConfigDirectory(const std::string& configDirectoryPath){
        filesystem::path southseaPath = filesystem::path(configDirectoryPath) / filesystem::path("Southsea");
        if(southseaPath.exists() && southseaPath.is_file()){
            //If for some reason it's a file, remove it and create the directory in the next step.
            southseaPath.remove_file();
        }
        if(!southseaPath.exists()){
            filesystem::create_directory(southseaPath);
        }

        return southseaPath.str();
    }

    void SystemSetup::updateEngineExecutablePath(){
        const filesystem::path enginePath(SystemSettings::_engineExecutablePath);
        if(enginePath.exists() && enginePath.is_file()){
            SystemSettings::_engineExecutablePathValid = true;
        }else{
            SystemSettings::_engineExecutablePathValid = false;
        }
    }

    void SystemSetup::_loadUserSettings(const filesystem::path& userSettingsPath){
        if(!userSettingsPath.exists() || !userSettingsPath.is_file()) return;

        Ogre::ConfigFile file;
        try{
            file.load(userSettingsPath.str());

            _processUserSettings(file);
        }
        catch(Ogre::Exception& e){
            if (e.getNumber() == Ogre::Exception::ERR_FILE_NOT_FOUND){
                //Could not open the settings file.
            }else throw;
        }
    }

    void SystemSetup::_processUserSettings(Ogre::ConfigFile& file){
        Ogre::ConfigFile::SettingsIterator iter = file.getSettingsIterator();
        while(iter.hasMoreElements()){
            Ogre::String archType = iter.peekNextKey();
            Ogre::String filename = iter.getNext();

            _processSettingsFileEntry(archType, filename);
        }
    }

    void SystemSetup::_processSettingsFileEntry(const std::string &key, const std::string &value){
        if(key == "GUIScale"){
            SystemSettings::_guiScale = Ogre::StringConverter::parseReal(value);
        }
        else if(key == "EnginePath"){
            SystemSettings::_engineExecutablePath = value;
        }
    }
}
