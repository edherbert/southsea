#include "PreviousProjects.h"

#include "System/Setup/SystemSettings.h"
#include <iostream>
#include <fstream>

#include "Event/EventDispatcher.h"
#include "Event/Events/ProjectEvent.h"

#include "System/Project/MapsDirectory.h"
#include "filesystem/path.h"

#include <cassert>
#include <iostream>

namespace Southsea{
    const char* _previousProjectsFileName = "previousProjects";
    const char* _mostRecentProjectFileName = "mostRecentProject";

    PreviousProjects::PreviousProjects(){
        _initialise();
    }

    PreviousProjects::~PreviousProjects(){

    }

    void PreviousProjects::_initialise(){
        EventDispatcher::subscribe(EventType::Project, SOUTHSEA_BIND(PreviousProjects::projectEventReceiver));
    }

    bool PreviousProjects::projectEventReceiver(const Event& event){
        const ProjectEvent& projEvent = (ProjectEvent&)event;

        if(projEvent.eventCategory() == ProjectEventCategory::BasicProjectCreated){
            //A project was created through one of the create project dialogs.
            //In this case it needs to be added to the list of recent projects.
            const ProjectEventBasicProjectCreated& p = (ProjectEventBasicProjectCreated&)projEvent;
            _projects.push_back( *(p.createdProject) );

            //As something new has been created, write out the recent projects again.
            _writeProjectsFile(SystemSettings::getConfigPath());
        }
        else if(projEvent.eventCategory() == ProjectEventCategory::ProjectOpened){
            //A project was opened, and it's known to be valid.
            const ProjectEventProjectOpened& p = (ProjectEventProjectOpened&)projEvent;

            const Project& proj = *(p.openedProject);
            int projIndex = _determineProjectListed(proj);
            if(projIndex >= 0){
                //The project was listed. It now needs to be moved to the end of the list.
                //Just delete the old one so all the others are shifted to the left.
                _projects.erase(_projects.begin() + projIndex);
            }

            _projects.push_back( proj ); //Push it to the back of the list. The back means most recent.

            //Write out the projects file because a new project was used in the editor.
            _writeProjectsFile(SystemSettings::getConfigPath());
        }
        else if(projEvent.eventCategory() == ProjectEventCategory::ProjectStartedEditing){
            const ProjectEventProjectStartedEditing& p = (ProjectEventProjectStartedEditing&)projEvent;

            _writeMostRecentProject(SystemSettings::getConfigPath(), *p.openedProject, *p.openedMap);
        }

        return false;
    }

    void PreviousProjects::scanPreviousProjects(){
        _projects.clear();

        _projects = _readProjectsFile();
        _readMostRecentProject();

        //Do a check of the list to see if the previous project is in the projects list.
        mMostRecentProjectIdx = mMostRecentMapIdx = -1;
        const std::string& mapName = mMostRecentMap.getMapName();

        for(int i = 0; i < _projects.size(); i++){
            const Project& p = _projects[i];
            if(p == mMostRecentProject){
                mMostRecentProjectIdx = i;
                break;
            }
        }

        //A project was found, so check for a map.
        if(mMostRecentProjectIdx >= 0){
            assert(mMostRecentProjectIdx >= 0 && mMostRecentProjectIdx < _projects.size());
            const Project& proj = _projects[mMostRecentProjectIdx];
            const AVSetupFile& file = proj.getAVSetupFile();

            MapsDirectory mapsDir;
            mapsDir.setDirectoryPath(file.getMapsDirectory());
            std::vector<std::string> currentMaps;
            mapsDir.findAvailableMaps(currentMaps);
            for(int i = 0; i < currentMaps.size(); i++){
                const std::string& m = currentMaps[i];
                if(m == mapName){
                    mMostRecentMapIdx = i;
                    break;
                }
            }
        }

    }

    int PreviousProjects::_determineProjectListed(const Project& proj){
        for(int i = 0; i < _projects.size(); i++){
            if(_projects[i] == proj) return i;
        }

        return -1;
    }

    void PreviousProjects::_readMostRecentProject(){
        const std::string& configDirectory = SystemSettings::getConfigPath();

        filesystem::path filePath = filesystem::path(configDirectory) / filesystem::path(_mostRecentProjectFileName);

        std::string line;
        std::ifstream file(filePath.str().c_str());
        if(file.is_open()){

            getline(file, line);
            mMostRecentProject = Project(line);
            getline(file, line);
            mMostRecentMap = Map(line);

            file.close();
        }
    }

    std::vector<Project> PreviousProjects::_readProjectsFile(){
        std::vector<Project> retVector;

        const std::string& configDirectory = SystemSettings::getConfigPath();

        filesystem::path filePath = filesystem::path(configDirectory) / filesystem::path(_previousProjectsFileName);
        //If that file path is for some reason invalid, just return an empty vector.
        if(!filePath.exists() || filePath.is_directory()) return retVector;

        //At this point this file should exist. Try and open it.
        std::string line;
        std::ifstream file(filePath.str().c_str());
        if (file.is_open()){

            while(getline(file, line)){
                if(Project::_determineProjectPathValidity(line)){
                    retVector.push_back( Project(line) );
                }
            }

            file.close();
        }else{
            //There was a problem opening the previous projects file.
        }

        return retVector;
    }

    bool PreviousProjects::_writeProjectsFile(const std::string& configDirectoryPath){
        filesystem::path filePath = filesystem::path(configDirectoryPath) / filesystem::path(_previousProjectsFileName);

        if(filePath.exists() && filePath.is_file()){
            //If a file already exists get rid of it, as we're going to override it.
            filePath.remove_file();
        }

        //Create the new file.
        std::ofstream outfile;
        outfile.open(filePath.str().c_str());

        for(const Project& p : _projects){
            outfile << p.getProjectPath() << std::endl;
        }

        outfile.close();

        return true;
    }

    bool PreviousProjects::_writeMostRecentProject(const std::string& configDirectoryPath, const Project& proj, const Map& map){
        filesystem::path filePath = filesystem::path(configDirectoryPath) / filesystem::path(_mostRecentProjectFileName);

        if(filePath.exists() && filePath.is_file()){
            filePath.remove_file();
        }

        //Create the new file.
        std::ofstream outfile;
        outfile.open(filePath.str().c_str());

        outfile << proj.getProjectPath() << '\n';
        outfile << map.getMapPath() << '\n';

        outfile.close();

        return true;
    }

}
