#pragma once

#include <string>

namespace Southsea{
    class SystemSetup;
    class SettingsView;

    class SystemSettings{
        friend class SystemSetup;
        friend class SettingsView;
    private:
        static std::string _masterPath;
        static std::string _configPath;
        static std::string _userSettingsFilePath;
        static std::string _engineExecutablePath;

        static bool _engineExecutablePathValid;
        static float _guiScale;

    public:
        static const std::string& getMasterPath() { return _masterPath; }
        static const std::string& getConfigPath() { return _configPath; }
        static const std::string& getEngineExecutablePath() { return _engineExecutablePath; }
        static float getGuiScale() { return _guiScale; }

        static bool isEnginePathValid() { return _engineExecutablePathValid; }
    };
}
