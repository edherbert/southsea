#pragma once

#include <string>

namespace Ogre{
    class ConfigFile;
}

namespace filesystem{
    class path;
}

namespace Southsea{
    class SystemSetup{
    public:
        /**
        Parses the user settings file.
        */
        static void setup();

        //Assumes a new engine executable path has been set.
        static void updateEngineExecutablePath();

        static void writeUserSettingsFile();

    private:
        static std::string _setupConfigDirectory(const std::string& configDirectoryPath);
        //Get the system specific path to where the Soutsea config directory should go.
        static std::string _getSystemConfigPath();

        static void _loadUserSettings(const filesystem::path& userSettingsPath);
        static void _processUserSettings(Ogre::ConfigFile& file);
        static void _processSettingsFileEntry(const std::string &key, const std::string &value);
    };
}
