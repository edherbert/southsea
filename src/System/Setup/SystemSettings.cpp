#include "SystemSettings.h"

namespace Southsea{
    std::string SystemSettings::_masterPath;
    std::string SystemSettings::_configPath;
    std::string SystemSettings::_userSettingsFilePath;

    std::string SystemSettings::_engineExecutablePath;
    bool SystemSettings::_engineExecutablePathValid = false;

    float SystemSettings::_guiScale = 1.0f;
}
