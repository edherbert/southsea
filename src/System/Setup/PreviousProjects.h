#pragma once

#include <string>
#include <vector>

#include "System/Project/Project.h"
#include "System/Project/Map.h"

namespace Southsea{
    class Event;

    /**
    A class to manage and provide functionality for previous projects.

    Projects that are created or loaded in are remembered and stored in a file in the SystemSettings::configPath directory.
    By doing this the editor is able to provide a list of the most recently used projects.
    */
    class PreviousProjects{
    public:
        PreviousProjects();
        ~PreviousProjects();

        void _initialise();

        /**
        Scan the projects mentioned in the configPath file.

        @remarks
        This will clear the projects list and re-populate it from scratch.
        */
        void scanPreviousProjects();

        const std::vector<Project>& getPreviousProjects(bool rescan = false){
            if(rescan){
                scanPreviousProjects();
            }
            return _projects;
        }

        bool projectEventReceiver(const Event& event);

        int getMostRecentProjectIdx() const { return mMostRecentProjectIdx; }
        int getMostRecentMapIdx() const { return mMostRecentMapIdx; }


    private:
        std::vector<Project> _projects;

        std::vector<Project> _readProjectsFile();

        Project mMostRecentProject;
        Map mMostRecentMap;

        int mMostRecentProjectIdx;
        int mMostRecentMapIdx;

        /**
        Write the current contents of the previous projects vector to the file system.
        */
        bool _writeProjectsFile(const std::string& configDirectoryPath);
        bool _writeMostRecentProject(const std::string& configDirectoryPath, const Project& proj, const Map& map);

        /**
        Returns -1 if the project is not in the list. Otherwise it will return an index of where that project is.
        It returns an id so that it can be referenced later.
        */
        int _determineProjectListed(const Project& proj);

        /**
        Read the previous projects file and populate the values of this class.
        */
        void _readMostRecentProject();
    };
}
