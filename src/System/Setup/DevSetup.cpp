#include "DevSetup.h"

#include "filesystem/path.h"
#include <OgreConfigFile.h>

#include "Editor/EditorSingleton.h"

#include "SystemSettings.h"

namespace Southsea{
    std::string DevSetup::_mapPath;
    std::string DevSetup::_projectPath;

    void DevSetup::parseDevFile(){
        filesystem::path filePath = filesystem::path(SystemSettings::getMasterPath()) / filesystem::path("devSetup.cfg");

        if(!filePath.exists() || !filePath.is_file()) return;

        Ogre::ConfigFile file;
        if(!_loadConfigFile(file, filePath.str())) return;

        _parseConfigFile(file);
    }

    bool DevSetup::_loadConfigFile(Ogre::ConfigFile &file, const std::string& path){

        try{
            file.load(path);

            return true;
        }
        catch(Ogre::Exception& e){
            if (e.getNumber() == Ogre::Exception::ERR_FILE_NOT_FOUND){
                //Could not open the settings file.
                return false;
            }else throw;
        }

        return false;
    }

    void DevSetup::_parseConfigFile(Ogre::ConfigFile &file){
        Ogre::ConfigFile::SettingsIterator iter = file.getSettingsIterator();
        while(iter.hasMoreElements()){
            Ogre::String archType = iter.peekNextKey();
            Ogre::String filename = iter.getNext();

            _processFileEntry(archType, filename);
        }
    }

    void DevSetup::_processFileEntry(const std::string &key, const std::string &value){
        if(key == "MapPath"){
            _mapPath = value;
        }
        else if(key == "ProjectPath"){
            _projectPath = value;
        }
    }

    void DevSetup::actuateSettings(){
        if(_mapPath.size() > 0 && _projectPath.size() > 0){
            Map map(_mapPath);
            Project proj(_projectPath);
            EditorSingleton::startEditor(proj, map);
        }
    }
}
