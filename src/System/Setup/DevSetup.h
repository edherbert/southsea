#pragma once

#include <string>

namespace Ogre{
    class ConfigFile;
}

namespace Southsea{

    /**
    A class to obtain developer specific settings.
    Say for instance the developer is working on something, they wouldn't want to go straight to the startup screen.
    So in the developer file you can specify this sort of thing to speedup development time without having to set this stuff up in the code.
    */
    class DevSetup{
    public:
        //Look for the developer file to parse. This should exist in the working directory of the executable, and be called devSetup.cfg
        static void parseDevFile();

        //Setup any values in the dev setup which might need to wait for startup to finish (such as setting the current map).
        static void actuateSettings();

    private:
        static bool _loadConfigFile(Ogre::ConfigFile &file, const std::string& path);
        static void _parseConfigFile(Ogre::ConfigFile &file);
        static void _processFileEntry(const std::string &key, const std::string &value);

    private:

        static std::string _mapPath;
        static std::string _projectPath;
    };
}
