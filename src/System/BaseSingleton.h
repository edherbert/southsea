#pragma once

#include <memory>

namespace Southsea{
    class Base;
    class PreviousProjects;
    class TerrainEditorSettings;
    class DraggedResourceInfo;
    class BrushThumbnailManager;

    class BaseSingleton{
        friend class Base;
    public:
        static std::shared_ptr<PreviousProjects> getPreviousProjects();
        static std::shared_ptr<TerrainEditorSettings> getTerrainEditorSettings();
        static std::shared_ptr<DraggedResourceInfo> getDraggedResourceInfo();
        static std::shared_ptr<BrushThumbnailManager> getBrushThumbnailManager();

    private:
        static void initialise(
            std::shared_ptr<PreviousProjects> previousProjects,
            std::shared_ptr<TerrainEditorSettings> terrainEditorSettings,
            std::shared_ptr<DraggedResourceInfo> draggedResourceInfo,
            std::shared_ptr<BrushThumbnailManager> brushThumbnailManager
        );

        static std::shared_ptr<PreviousProjects> mPreviousProjects;
        static std::shared_ptr<TerrainEditorSettings> mTerrainEditorSettings;
        static std::shared_ptr<DraggedResourceInfo> mDraggedResourceInfo;
        static std::shared_ptr<BrushThumbnailManager> mBrushThumbnailManager;
    };
}
