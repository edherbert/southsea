#pragma once

namespace Southsea{
    static const unsigned int BRUSH_DATA_SIZE = 50;

    enum class ShapeBrushType{
        CIRCLE,
        SQUARE
    };

    enum class ShapeBrushFunction{
        SIN,
        LINEAR,
        POINT
    };

    enum class BrushMode{
        SHAPE,
        TEXTURE
    };
}
