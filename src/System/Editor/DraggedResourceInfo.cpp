#include "DraggedResourceInfo.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorGuiEvent.h"
#include "Gui/Views/ResourceButton.h"

#include <cassert>

namespace Southsea{
    const Resource DraggedResourceInfo::EMPTY = {"", ""};
    const ResourceFSEntry DraggedResourceInfo::EMPTYENTRY = {"", ResourceType::unknown};

    DraggedResourceInfo::DraggedResourceInfo()
        : mCurrentlyDraggingResource(false) {
        EventDispatcher::subscribe(EventType::EditorGui, SOUTHSEA_BIND(DraggedResourceInfo::editorGuiEventReceiver));
    }

    DraggedResourceInfo::~DraggedResourceInfo(){
        EventDispatcher::unsubscribe(EventType::EditorGui, this);
    }

    void DraggedResourceInfo::setCurrentDragResource(const ResourceFSEntry& entry, const Resource& draggedEntry, const std::string& fullPath){
        assert(!mCurrentlyDraggingResource);

        mCurrentDragResourceEntry = entry;
        mCurrentDragResource = draggedEntry;
        mFullPath = fullPath;

        mCurrentlyDraggingResource = true;
    }

    void DraggedResourceInfo::clearCurrentDragResource(){
        assert(mCurrentlyDraggingResource);

        mCurrentDragResource = EMPTY;
        mCurrentDragResourceEntry = EMPTYENTRY;
        mFullPath.clear();

        mCurrentlyDraggingResource = false;

        mCurrentHoveredData = 0;
        mOldHoveredData = 0;
    }

    void DraggedResourceInfo::notifyHoverItem(ResourceButtonData* data, ResourceButtonData* oldData){
        mCurrentHoveredData = data;
        mOldHoveredData = oldData;
    }

    void DraggedResourceInfo::prepareNextFrame(){
        if(mDragCompletedThisFrame > 0){
            if(mCurrentHoveredData && mDragCompletedThisFrame == 1
                && mCurrentHoveredData->type == mCurrentDragResourceEntry.type){
                    if(mOldHoveredData){
                        mOldHoveredData->res = mCurrentHoveredData->res;
                        mOldHoveredData->displayName = mCurrentHoveredData->displayName;
                    }

                mCurrentHoveredData->res = mCurrentDragResource;
                mCurrentHoveredData->displayName = mCurrentDragResourceEntry.name;
                mCurrentHoveredData->state = ResButtonState::Dirty;
            }
            clearCurrentDragResource();
        }

        mCurrentHoveredData = 0;
        mOldHoveredData = 0;
        mDragCompletedThisFrame = 0;
    }

    bool DraggedResourceInfo::editorGuiEventReceiver(const Event& e){
        const EditorGuiEvent& guiEvent = (const EditorGuiEvent&)e;
        if(guiEvent.eventCategory() == EditorGuiEventCategory::ResourceDrag){
            const EditorGuiResourceDrag& dragEvent = (const EditorGuiResourceDrag&)e;

            if(dragEvent.dragType == DragEventType::Started){
                setCurrentDragResource( *(dragEvent.entry), *(dragEvent.draggedResource), dragEvent.fullPath );
            }else{
                mDragCompletedThisFrame = dragEvent.dragType == DragEventType::Completed ? 1 : 2;
            }
        }

        return false;
    }
}
