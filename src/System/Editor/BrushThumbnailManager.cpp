#include "BrushThumbnailManager.h"

#include "Platforms/OgreSetup/SouthseaResourceGroups.h"
#include "Ogre.h"

namespace Southsea{
    const size_t BrushThumbnailManager::SHAPE_BRUSH_THUMBNAIL = 500;

    BrushThumbnailManager::BrushThumbnailManager(){

    }

    BrushThumbnailManager::~BrushThumbnailManager(){

    }

    Ogre::Texture* BrushThumbnailManager::getTextureForBrush(size_t id){

        return mThumbnails[id].ptr.get();
    }

    void BrushThumbnailManager::generateThumbnailForFloatArray(size_t id, float* array, size_t arraySize){
        int size = BRUSH_DATA_SIZE;
        int totalSize = size*size;
        using namespace Ogre;

        const Ogre::String name = "brushThumbnail" + std::to_string(id);

        TexturePtr tex;
        if(id == SHAPE_BRUSH_THUMBNAIL && mShapeBrushTex){
            //Reuse the textures if possible.
            tex = mShapeBrushTex;
        }else{
            tex = TextureManager::getSingleton().createManual(
                name,
                SOUTHSEA_INTERNAL_GROUP_NAME,
                TEX_TYPE_2D, size, size, 1, 0, PF_R8G8B8, TU_WRITE_ONLY);
        }

        //Write the pixels.
        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = tex->getBuffer(0, 0);
        const Ogre::PixelBox &pixBox =
        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

        Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixBox.data);

        for(int y = 0; y < size; y++){
            int yVal = (float(y) / size) * arraySize;
            for(int x = 0; x < size; x++){
                int xVal = (float(x) / size) * arraySize;
                //size_t movAmount = x + y * size;
                //Ogre::uint8 amount = *(array) * 255;
                Ogre::uint8 amount = *(array + xVal + yVal*arraySize) * 255;

                *pDest++ = amount;
                *pDest++ = amount;
                *pDest++ = amount;
                *pDest++ = 0;
            }
        }

        pixelBufferBuf->unlock();

        if(id == SHAPE_BRUSH_THUMBNAIL){
            mShapeBrushTex = tex;
        }else{
            //Right now I don't allow texture brush values to be set twice, so check it only happens once.
            assert(id >= mThumbnails.size());
            mThumbnails.push_back({tex});
        }
    }
}
