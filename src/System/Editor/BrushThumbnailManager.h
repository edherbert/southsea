#pragma once

#include "BrushSettings.h"
#include "Ogre.h"
#include <vector>

namespace Southsea{
    class BrushThumbnailManager{
    public:
        BrushThumbnailManager();
        ~BrushThumbnailManager();

        static const size_t SHAPE_BRUSH_THUMBNAIL;

        /**
        Generate a thumbnail from a float array.
        The array must be 1000 entries in size.
        */
        void generateThumbnailForFloatArray(size_t id, float* array, size_t arraySize = 50);

        Ogre::Texture* getTextureForBrush(size_t id);
        Ogre::Texture* getShapeBrushThumbnail() const { return mShapeBrushTex.get(); }

    private:
        struct ThumbnailData{
            Ogre::TexturePtr ptr;
        };

        Ogre::TexturePtr mShapeBrushTex;
        std::vector<ThumbnailData> mThumbnails;
    };
}
