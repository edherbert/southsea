#include "EditorResourceSetup.h"

#include "OgreRoot.h"
#include "OgreHlmsUnlit.h"
#include "OgreHlmsUnlitDatablock.h"
#include "OgreHlmsManager.h"

namespace Southsea{

    const char* EditorResourceSetup::sceneGridDatablockName = "sceneGridDatablock";
    const Ogre::IdString EditorResourceSetup::sceneGridDatablock = Ogre::IdString(sceneGridDatablockName);

    void EditorResourceSetup::setupSimpleResources(Ogre::Root* root){
        //Setup the scene grid datablock.
        {
            Ogre::Hlms* hlms = root->getSingletonPtr()->getHlmsManager()->getHlms(Ogre::HLMS_UNLIT);
            Ogre::HlmsUnlit* unlit = dynamic_cast<Ogre::HlmsUnlit*>(hlms);

            Ogre::HlmsDatablock* block = unlit->createDatablock(sceneGridDatablock, sceneGridDatablockName, Ogre::HlmsMacroblock(), Ogre::HlmsBlendblock(), Ogre::HlmsParamVec());
            Ogre::HlmsUnlitDatablock* unlitBlock = dynamic_cast<Ogre::HlmsUnlitDatablock*>(block);
            unlitBlock->setUseColour(true);
            unlitBlock->setColour(Ogre::ColourValue(0.35, 0.35, 0.35));
        }
    }
}
