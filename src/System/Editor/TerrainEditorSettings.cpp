#include "TerrainEditorSettings.h"

#include <cmath>
#include <cassert>
#include <iostream>

#include "Brushes/grungeBrush.h"
#include "Brushes/groundBrush.h"
#include "Brushes/bristlesBrush.h"
#include "Brushes/vegetationBrush.h"
#include "Brushes/starBrush.h"
#include "BrushThumbnailManager.h"

namespace Southsea{
    const uint8 TerrainEditorSettings::MaxDetailPaintLayers = 4;

    TerrainEditorSettings::TerrainEditorSettings(){

    }

    TerrainEditorSettings::~TerrainEditorSettings(){

    }

    void TerrainEditorSettings::setupBrushes(std::shared_ptr<BrushThumbnailManager> thumbnailManager){
        thumbnailManager->generateThumbnailForFloatArray(0, &grungeBrush[0]);
        thumbnailManager->generateThumbnailForFloatArray(1, &groundBrush[0]);
        thumbnailManager->generateThumbnailForFloatArray(2, &bristlesBrush[0]);
        thumbnailManager->generateThumbnailForFloatArray(3, &vegetationBrush[0]);
        thumbnailManager->generateThumbnailForFloatArray(4, &starBrush[0]);
        mThumbnailManager = thumbnailManager;
    }

    void TerrainEditorSettings::setShapeBrushType(int type){
        switch(type){
            case 0: mCurrentShapeBrushType = ShapeBrushType::CIRCLE; break;
            case 1: mCurrentShapeBrushType = ShapeBrushType::SQUARE; break;
            default:{
                assert(false);
            }
        }
        mBrushDataDirty = true;
        mShapeBrushThumbnailDirty = true;
    }

    void TerrainEditorSettings::setShapeBrushFunction(int type){
        switch(type){
            case 0: mCurrentShapeBrushFunction = ShapeBrushFunction::SIN; break;
            case 1: mCurrentShapeBrushFunction = ShapeBrushFunction::LINEAR; break;
            case 2: mCurrentShapeBrushFunction = ShapeBrushFunction::POINT; break;
            default:{
                assert(false);
            }
        }
        mBrushDataDirty = true;
        mShapeBrushThumbnailDirty = true;
    }

    void TerrainEditorSettings::setCurrentBrushMode(BrushMode mode){
        mCurrentBrushMode = mode;
        mBrushDataDirty = true;
    }

    void TerrainEditorSettings::_genBrushData(){
        mBrushData.clear();

        mBrushData.reserve(mBrushSize * mBrushSize);
        if(mCurrentBrushMode == BrushMode::SHAPE){
            _genShapeBrushData();
        }else{
            //Assume texture brush
            _genTextureBrushData();
        }

        mBrushDataDirty = false;
    }

    Ogre::Texture* TerrainEditorSettings::getShapeBrushThumbnail(){
        if(mShapeBrushThumbnailDirty){
            if(mBrushDataDirty) _genBrushData();
            std::cout << "Generating brush thumbnail." << '\n';
            mThumbnailManager->generateThumbnailForFloatArray(BrushThumbnailManager::SHAPE_BRUSH_THUMBNAIL, &(mBrushData[0]), mBrushSize);
            mShapeBrushThumbnailTexture = mThumbnailManager->getShapeBrushThumbnail();
            mShapeBrushThumbnailDirty = false;
        }

        return mShapeBrushThumbnailTexture;
    }

    void TerrainEditorSettings::_genShapeBrushData(){
        if(mCurrentShapeBrushType == ShapeBrushType::CIRCLE){

            const float centre = mBrushSize / 2;
            for(int y = 0; y < mBrushSize; y++){
                for(int x = 0; x < mBrushSize; x++){
                    float distance = sqrt(pow(centre - y, 2) + pow(centre - x, 2));

                    distance /= centre;
                    distance = 1 - distance;
                    if(distance < 0) distance = 0;
                    //Hopefully the compiler will resolve out this switch statement.
                    switch(mCurrentShapeBrushFunction){
                        case ShapeBrushFunction::LINEAR:{
                            distance = distance * (1 + mCurrentShapeBrushHardness * 5);
                            break;
                        }
                        case ShapeBrushFunction::SIN:{
                            distance = sin(distance) * (1 + mCurrentShapeBrushHardness * 5);
                            break;
                        }
                        case ShapeBrushFunction::POINT:{
                            distance = tan(distance*1.5)*0.2 * (1 + mCurrentShapeBrushHardness * 5);
                            break;
                        }
                    }
                    if(distance > 1) distance = 1;

                    mBrushData.push_back(distance);
                }
            }

        }else if(mCurrentShapeBrushType == ShapeBrushType::SQUARE){

            const float centre = mBrushSize / 2;
            for(int y = 0; y < mBrushSize; y++){
                for(int x = 0; x < mBrushSize; x++){

                    //For now.
                    float value = 1.0f;

                    mBrushData.push_back(value);
                }
            }

        }else{
            assert(false);
        }

    }

    void TerrainEditorSettings::_genTextureBrushData(){
        //Copy the values to the vector.
        //This method scales if the brush size is bigger than the data.
        static const size_t numBrushes = 5;
        static const float* brushes[numBrushes] = { &grungeBrush[0], &groundBrush[0], &bristlesBrush[0], &vegetationBrush[0], &starBrush[0] };
        assert(mCurrentSelectedTextureBrush < numBrushes);
        for(int y = 0; y < mBrushSize; y++){
            int yVal = (y / mBrushSize) * BRUSH_DATA_SIZE;
            for(int x = 0; x < mBrushSize; x++){
                int xVal = (x / mBrushSize) * BRUSH_DATA_SIZE;

                mBrushData.push_back( *(brushes[mCurrentSelectedTextureBrush] + xVal + yVal * BRUSH_DATA_SIZE) );
            }
        }
    }

    void TerrainEditorSettings::setCurrentDetailPaintLayer(uint8 layer){
        assert(layer >= 0 && layer < MaxDetailPaintLayers);

        mDetailPaintLayer = layer;
    }
}
