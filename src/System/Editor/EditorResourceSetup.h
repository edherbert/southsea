#pragma once

#include "OgreIdString.h"

namespace Southsea{

    class EditorResourceSetup{
    public:
        static const char* sceneGridDatablockName;
        static const Ogre::IdString sceneGridDatablock;

        //Setup resources specific to Southsea that the editor needs to run.
        static void setupSimpleResources(Ogre::Root* root);
    };
}
