#include "EngineRunner.h"

#include <iostream>
#include <fstream>
#include "filesystem/path.h"
#include "System/Project/AVSetupFile.h"
#include "Editor/EditorSingleton.h"

#include "Editor/Resources/ResourceManager.h"
#include "Editor/Resources/OgreResourcesManager.h"

#include "rapidjson/document.h"
#include <rapidjson/filereadstream.h>
#include "rapidjson/filewritestream.h"
#include <rapidjson/writer.h>

//avEngine settings
#include "System/EnginePrerequisites.h"

#include "System/Setup/SystemSettings.h"
#include <cassert>

namespace Southsea{
    void EngineRunner::viewInEditor(){
        std::cout << "Executing in engine" << '\n';

        if(!SystemSettings::isEnginePathValid()){
            std::cout << "Unable to execute. The engine path is not valid." << '\n';
            std::cout << SystemSettings::getEngineExecutablePath() << '\n';
            return;
        }

        filesystem::path avSetupPath;
        _setupTemporaryProjectFiles(avSetupPath);
        _beginExecution(avSetupPath);
    }

    void EngineRunner::_setupTemporaryProjectFiles(filesystem::path &outAvSetupPath){
        const char* targetDir = "/tmp";

        filesystem::path targetDirPath(targetDir);

        filesystem::path targetDirFilePath(targetDirPath / filesystem::path("avSetup.cfg"));
        filesystem::path targetDirOgreFilePath(targetDirPath / filesystem::path("OgreResources.cfg"));
        _createOgreResourcesFile(targetDirOgreFilePath);
        _createAvSetupFile(targetDirFilePath, targetDirOgreFilePath);

        outAvSetupPath = targetDirFilePath;
    }

    void EngineRunner::_beginExecution(const filesystem::path& avSetupPath){
        const std::string& execPath = SystemSettings::getEngineExecutablePath();
        assert(filesystem::path(execPath).exists());
        assert(filesystem::path(execPath).is_file());

        const std::string targetPathString = execPath + " " + avSetupPath.str();
        system(targetPathString.c_str());
    }

    bool EngineRunner::_createOgreResourcesFile(filesystem::path& targetPath){
        std::ofstream myfile;
        myfile.open(targetPath.str());
        if(!myfile.is_open()) return false;

        const OgreResourcesManager::ResourceLocationContainer& resources = EditorSingleton::getEditor()->getResourceManager()->getOgreResourcesManager()->getOgreResources();

        for(auto it = resources.begin(); it != resources.end(); it++){
            myfile << "[" << (*it).first << "]" << std::endl;

            const std::vector<OgreResourcesManager::ResourceLocationEntry>& eVec = (*it).second;
            for(const OgreResourcesManager::ResourceLocationEntry& i : eVec){
                myfile << "FileSystem=";
                myfile << i.first;
                myfile << std::endl;
            }
        }

        myfile.close();

        return true;
    }

    bool EngineRunner::_createAvSetupFile(filesystem::path& targetPath, filesystem::path& ogreFilePath){
        const AVSetupFile& f = EditorSingleton::getEditor()->getEditorProject().getAVSetupFile();
        const std::string& s = f.getMapsDirectory();
        const std::string& mapName = EditorSingleton::getEditor()->getEditorMap().getMapName();

        rapidjson::Document d;
        d.SetObject();
        rapidjson::Document::AllocatorType& allocator = d.GetAllocator();

        d.AddMember("WindowTitle", "Southsea Project Viewer", allocator);
        d.AddMember("CompositorBackground", "1 0 1 1", allocator);
        d.AddMember("SquirrelEntryFile", "main.nut", allocator);
        d.AddMember("DataDirectory", "/home/edward/Documents/Southsea/projectViewer", allocator);

        rapidjson::Value mapsDir;
        mapsDir.SetString(s.c_str(), allocator);
        d.AddMember("MapsDirectory", mapsDir, allocator);

        rapidjson::Value ogreResourcesFile;
        ogreResourcesFile.SetString(ogreFilePath.str().c_str(), allocator);
        d.AddMember("OgreResourcesFile", ogreResourcesFile, allocator);

        d.AddMember("WorldSlotSize", f.getSlotSize(), allocator);

        { //Add user settings
            rapidjson::Value userDataObj(rapidjson::kObjectType);

            rapidjson::Value mapNameObj;
            mapNameObj.SetString(mapName.c_str(), allocator);

            userDataObj.AddMember("mapName", mapNameObj, allocator);
            d.AddMember("UserSettings", userDataObj, allocator);
        }

        { //Enable all the collision worlds.
            for(int i = 0; i < MAX_COLLISION_WORLDS; i++){
                rapidjson::Value userDataObj(rapidjson::kObjectType);

                {
                    rapidjson::Value worldNameObj;
                    worldNameObj.SetString(std::to_string(i).c_str(), allocator);

                    userDataObj.AddMember("name", worldNameObj, allocator);
                }

                std::string collisionName("Collision");
                collisionName += std::to_string(i);
                rapidjson::Value val;
                val.SetString(collisionName.c_str(), allocator);
                d.AddMember(val, userDataObj, allocator);
            }
        }

        const char* openType = 0;
        #ifdef _WIN32
        openType = "wb";
        #else
        openType = "w";
        #endif
        FILE* fp = fopen(targetPath.str().c_str(), openType);
        char writeBuffer[65536];
        rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
        rapidjson::Writer<rapidjson::FileWriteStream> writer(os);
        d.Accept(writer);
        fclose(fp);

        return true;
    }
}
