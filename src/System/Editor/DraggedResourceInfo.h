#pragma once

#include <vector>
#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{
    class Event;
    class ResourceButtonData;

    /**
    A simple class to keep track of which resource (if any) is currently being dragged around the editor.
    */
    class DraggedResourceInfo{
    public:
        DraggedResourceInfo();
        ~DraggedResourceInfo();

        void setCurrentDragResource(const ResourceFSEntry& entry, const Resource& draggedEntry, const std::string& fullPath);
        void clearCurrentDragResource();

        bool editorGuiEventReceiver(const Event& e);

        /**
        Notify the manager that an item is currently being hovered.
        With this it can tell what to do when the state ends. For instance whether to confirm a hover.
        */
        void notifyHoverItem(ResourceButtonData* data, ResourceButtonData* oldData);

        /**
        Prepare the next frame / update cycle of the dragging system.
        This function is called each frame, and deals with dispatching drag ended events.
        Drag complete events are deferred until the end of the frame so they can be correctly populated.
        This function dispatches these events, and clears the state ready for the next frame.
        */
        void prepareNextFrame();

        const Resource& getCurrentDraggedResource() const { return mCurrentDragResource; }
        const ResourceFSEntry& getCurrentDraggedResourceEntry() const { return mCurrentDragResourceEntry; }
        const std::string& getFullPath() const { return mFullPath; }
        bool getDraggingResource() const { return mCurrentlyDraggingResource; }

    private:
        Resource mCurrentDragResource;
        ResourceFSEntry mCurrentDragResourceEntry;
        std::string mFullPath;
        ResourceButtonData* mCurrentHoveredData = 0;
        ResourceButtonData* mOldHoveredData = 0;

        bool mCurrentlyDraggingResource;
        int mDragCompletedThisFrame = 0; //0 no drag, 1 completed, 2 aborted

        static const Resource EMPTY;
        static const ResourceFSEntry EMPTYENTRY;
    };
}
