#pragma once

namespace filesystem{
    class path;
}

namespace Southsea{

    /**
    Class for running the editor viewer.
    Responsible for creating a simplistic project complete with avSetup file specifically for viewing the scene.
    */
    class EngineRunner{
    public:
        EngineRunner() = delete;
        ~EngineRunner() = delete;

        static void viewInEditor();

    private:
        static void _setupTemporaryProjectFiles(filesystem::path &outAvSetupPath);
        static bool _createAvSetupFile(filesystem::path& targetPath, filesystem::path& ogreFilePath);
        static bool _createOgreResourcesFile(filesystem::path& targetPath);

        static void _beginExecution(const filesystem::path& avSetupPath);
    };
}
