#pragma once

#include <vector>
#include <cmath>
#include <memory>
#include "BrushSettings.h"

namespace Ogre{
    class Texture;
}

namespace Southsea{

    typedef unsigned char uint8;

    class BrushThumbnailManager;

    /**
    A class to manage settings for the terrain editor.
    It is responsible for generating brush data as well as storing setting values.
    It is not created as part of the TerrainManager so that these brush settings persist change / editor restart.
    */
    class TerrainEditorSettings{
    public:
        TerrainEditorSettings();
        ~TerrainEditorSettings();

        enum class TerrainTool{
            RAISE_LOWER,
            BLEND_PAINT,
            SMOOTH,
            SET_HEIGHT
        };

        void setCurrentBrushMode(BrushMode mode);

        void setBrushSize(float size) {
            mBrushSize = std::floor(size);
            mBrushDataDirty = true;
        }

        void setBrushStrength(float strength) {
            mBrushStrength = strength;
            mBrushDataDirty = true;
        }

        void setShapeBrushType(int type);
        void setShapeBrushFunction(int type);

        void setShapeBrushHardness(float value){
            mCurrentShapeBrushHardness = value;
            mBrushDataDirty = true;
            mShapeBrushThumbnailDirty = true;
        }

        const std::vector<float>& getBrushData(){
            if(mBrushDataDirty) _genBrushData(); //The data is generated lazily.

            return mBrushData;
        }

        float getBrushSize() { return mBrushSize; }
        float getBrushStrength() {
            if(!raiseLowerRaise)
                return -mBrushStrength;
            return mBrushStrength;
        }

        void setupBrushes(std::shared_ptr<BrushThumbnailManager> thumbnailManager);

        void setCurrentTool(TerrainTool tool) { mCurrentTool = tool; }
        TerrainTool getCurrentTool() const { return mCurrentTool; }

        void setHeightBrushValue(float value) { mSetHeightBrushSize = value; }
        float getHeightBrushValue() const { return mSetHeightBrushSize; }

        void setPreviewHeightBrushValue(float value) { mPreviewHeightBrushSize = value; }
        float getPreviewHeightBrushValue() const { return mPreviewHeightBrushSize; }

        void setRaiseLowerPaintType(int paintType){
            raiseLowerRaise = paintType == 0;
        }

        void setCurrentDetailPaintLayer(uint8 layer);
        uint8 getCurrentDetailPaintLayer() const { return mDetailPaintLayer; }

        Ogre::Texture* getShapeBrushThumbnail();

        int getCurrentSelectedTextureBrush() const { return mCurrentSelectedTextureBrush; }
        void setCurrentSelectedTextureBrush(int brush) {
            mCurrentSelectedTextureBrush = brush;
            mBrushDataDirty = true;
        }

    private:
        bool mBrushDataDirty = true;
        bool mShapeBrushThumbnailDirty = true;

        float mBrushSize = 0;
        float mBrushStrength = 0;

        float mSetHeightBrushSize = 0;
        float mPreviewHeightBrushSize = 0;

        int mCurrentSelectedTextureBrush = 0;

        uint8 mDetailPaintLayer = 0;
        static const uint8 MaxDetailPaintLayers;

        std::vector<float> mBrushData;

        TerrainTool mCurrentTool = TerrainTool::RAISE_LOWER; //This one by default.

        //Whether or not the raiseLower tool is set to the raise mode.
        bool raiseLowerRaise = true;

        ShapeBrushType mCurrentShapeBrushType = ShapeBrushType::CIRCLE;
        ShapeBrushFunction mCurrentShapeBrushFunction = ShapeBrushFunction::SIN;
        float mCurrentShapeBrushHardness = 0;

        BrushMode mCurrentBrushMode = BrushMode::SHAPE;

        void _genBrushData();
        void _genShapeBrushData();
        void _genTextureBrushData();

        std::shared_ptr<BrushThumbnailManager> mThumbnailManager;
        Ogre::Texture* mShapeBrushThumbnailTexture = 0;
    };
}
