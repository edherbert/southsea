#include "EditorView.h"

#include "imgui.h"

#include "System/SouthsePrerequisites.h"
#include "System/Project/Map.h"
#include "Platforms/Window/Input/KeyCodes.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorGuiEvent.h"
#include "Event/Events/CommandEvent.h"

#include "Editor/Chunk/Chunk.h"
#include "Editor/Compositor/EditorCompositorManager.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "System/BaseSingleton.h"
#include "SettingsView.h"

#include "Gui/Views/TerrainPropertiesView.h"
#include "Gui/Views/ChunkSwitcherView.h"
#include "Gui/Views/SceneHierarchyView.h"
#include "Gui/Views/ObjectPropertiesView.h"
#include "Gui/Views/EditorViewDropDown.h"
#include "Gui/Views/NavMeshView.h"
#include "Platforms/OgreSetup/SouthseaResourceGroups.h"
#include "Editor/ui/EditorToolDialogManager.h"
#include "Editor/Logic/EditorState.h"
#include "System/Editor/TerrainEditorSettings.h"

#include "Resource/ResourceBrowserView.h"
#include "Resource/ResourceSelectorPopup.h"

#include <iostream>

namespace Southsea{
    bool EditorView::mFirstFrame = false;

    bool EditorView::renderWindowOpen = true;
    bool EditorView::chunkSwitcherViewOpen = true;
    bool EditorView::resourceBrowserViewOpen = true;
    bool EditorView::sceneHierarchyViewOpen = true;
    bool EditorView::ObjectPropertiesViewOpen = true;
    bool EditorView::navMeshViewOpen = true;

    bool showAboutPopup = false;
    bool showSettingsPopup = false;

    void EditorView::_drawEditorMenuBar(){
        //Menu bar
        ImGui::BeginMenuBar();
        if (ImGui::BeginMenu("File")){

            if(ImGui::MenuItem("Save", "Ctrl+s")){
                CommandEventSave e;
                EventDispatcher::transmitEvent(EventType::Command, e);
            }
            ImGui::Separator();
            if(ImGui::MenuItem("Return to startup screen")){
                EditorSingleton::getEditor()->requestShutdown();
            }

            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit")){
            if(ImGui::MenuItem("Undo", "Ctrl+z")){
                CommandEventUndo e;
                EventDispatcher::transmitEvent(EventType::Command, e);
            }
            if(ImGui::MenuItem("Redo", "Ctrl+Shift+z")){
                CommandEventRedo e;
                EventDispatcher::transmitEvent(EventType::Command, e);
            }

            ImGui::Separator();
            if(ImGui::MenuItem("Cut", "Ctrl+x")) {
                CommandEventCut e;
                EventDispatcher::transmitEvent(EventType::Command, e);
            }
            if(ImGui::MenuItem("Copy", "Ctrl+c")) {
                CommandEventCopy e;
                EventDispatcher::transmitEvent(EventType::Command, e);
            }
            if(ImGui::MenuItem("Paste", "Ctrl+v")) {
                CommandEventPaste e;
                EventDispatcher::transmitEvent(EventType::Command, e);
            }
            ImGui::Separator();

            if(ImGui::MenuItem("Settings")){
                showSettingsPopup = true;
            }

            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Scene")){
            if(ImGui::MenuItem("Reset camera position")){
                EditorSingleton::getEditor()->resetCameraPosition();
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Window")){
            if(ImGui::MenuItem("Render Window", NULL, renderWindowOpen)) renderWindowOpen = !renderWindowOpen;
            if(ImGui::MenuItem("Chunk Switcher", NULL, chunkSwitcherViewOpen)) chunkSwitcherViewOpen = !chunkSwitcherViewOpen;
            if(ImGui::MenuItem("Resource Browser", NULL, resourceBrowserViewOpen)) resourceBrowserViewOpen = !resourceBrowserViewOpen;
            if(ImGui::MenuItem("Object Properties", NULL, ObjectPropertiesViewOpen)) ObjectPropertiesViewOpen = !ObjectPropertiesViewOpen;
            if(ImGui::MenuItem("Nav Mesh", NULL, navMeshViewOpen)) navMeshViewOpen = !navMeshViewOpen;

            ImGui::Separator();
            if(ImGui::MenuItem("About")){
                showAboutPopup = true;
            }

            ImGui::EndMenu();
        }

        {
            const char* viewText = "View in engine";
            ImGuiStyle* style = &ImGui::GetStyle();
            const ImVec2 viewTextSize = ImGui::CalcTextSize(viewText);

            ImVec2 pos = ImGui::GetCursorPos();
            pos = ImVec2(pos.x + 20, pos.y);
            ImVec2 end(pos.x + viewTextSize.x + 16, pos.y + viewTextSize.y + 30);

            ImGui::SetCursorPos(pos);
            ImGui::Dummy(ImVec2(viewTextSize.x + 16, 20));
            bool hovered = ImGui::IsItemHovered();

            static const int textOffset = 13;
            ImDrawList* drawList = ImGui::GetWindowDrawList();
            ImU32 col = IM_COL32(80, 80, 80, 80);
            if(hovered){
                bool mouseDown = ImGui::IsMouseDown(0);
                bool clicked = ImGui::IsItemClicked(0);

                if(clicked){
                    EditorSingleton::getEditor()->requestViewInEngine();
                }

                ImGuiCol_ targetCol = mouseDown ? ImGuiCol_ButtonActive : ImGuiCol_ButtonHovered;
                col = ImGui::GetColorU32(style->Colors[targetCol]);
            }
            drawList->AddRectFilled(pos, ImVec2(end.x + textOffset, end.y), col);

            drawList->AddTriangleFilled(ImVec2(pos.x + 5, pos.y + 4), ImVec2(pos.x + 5, pos.y + 14), ImVec2(pos.x + 5 + 10, pos.y + 9), IM_COL32(255, 255, 255, 255));
            drawList->AddText(ImVec2(pos.x + 8 + textOffset, pos.y + 2), IM_COL32(255, 255, 255, 255), viewText);
        }

        {
            ImDrawList* drawList = ImGui::GetWindowDrawList();
            ImGuiIO& io = ImGui::GetIO();
            const float windowSize = io.DisplaySize.x;

            const std::string& mapName = EditorSingleton::getEditor()->getEditorMap().getMapName();

            const char* viewText = mapName.c_str();
            ImGuiStyle* style = &ImGui::GetStyle();
            const ImVec2 viewTextSize = ImGui::CalcTextSize(viewText);

            ImVec2 pos = ImGui::GetCursorPos();

            ImGui::SetCursorPos(pos);

            drawList->AddText(ImVec2(windowSize - viewTextSize.x, pos.y + 2), IM_COL32(150, 150, 150, 255), viewText);
        }

        ImGui::EndMenuBar();
    }

    void EditorView::_checkKeyCommands(){
        ImGuiIO& io = ImGui::GetIO();
        if(io.WantCaptureKeyboard) return;

        //NOTE: If this grows much bigger I should move it somewhere else.
        int target = -1;
        if(ImGui::IsKeyPressed(SOUTHSEA_KEY_CODE_1)) target = 0;
        else if(ImGui::IsKeyPressed(SOUTHSEA_KEY_CODE_2)) target = 1;
        else if(ImGui::IsKeyPressed(SOUTHSEA_KEY_CODE_3)) target = 2;
        else if(ImGui::IsKeyPressed(SOUTHSEA_KEY_CODE_4)) target = 3;
        if(target >= 0){
            auto editor = EditorSingleton::getEditor();
            editor->setCurrentSelectionTool( static_cast<SelectionTool>(target) );
        }

        if(ImGui::IsKeyPressed(SOUTHSEA_KEY_CODE_DELETE) || ImGui::IsKeyPressed(SOUTHSEA_KEY_CODE_BACKSPACE)){
            auto manager = EditorSingleton::getEditor()->getSceneTreeManager();
            manager->deleteCurrentSelection();
        }
    }

    void EditorView::drawEditorView(){
        //Start the dockspace stuff.
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
        ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        static bool p_open;
        ImGui::Begin("DockSpace Demo", &p_open, window_flags);
        ImGui::PopStyleVar(3);

        ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), 0);

        _drawEditorMenuBar();
        _checkKeyCommands();

        _drawRenderWindow();

        ChunkSwitcherView::drawChunkSwitcherView(chunkSwitcherViewOpen);
        ResourceBrowserView::drawResourceBrowserView(resourceBrowserViewOpen);
        SceneHierarchyView::drawSceneHierarchyView(sceneHierarchyViewOpen);
        ObjectPropertiesView::drawObjectPropertiesView(ObjectPropertiesViewOpen);
        NavMeshView::drawNavMeshView(navMeshViewOpen);

        _drawSystemPopups();
        EditorToolDialogManager::drawPopups();


        ImGui::End();

        mFirstFrame = false; //If an update was done, we're no longer in the first frame since the view's creation.
    }

    void EditorView::_drawSystemPopups(){
        Editor* editor = EditorSingleton::getEditor();
        if(editor->isShutdownRequested()){
            ImGui::OpenPopup("Close without saving");
        }
        if(editor->isChunkSwitchRequested()){
            ImGui::OpenPopup("Switch chunk without saving");
        }
        if(editor->isViewInEngineRequested()){
            ImGui::OpenPopup("Save before viewing");
        }
        if(showAboutPopup){
            ImGui::OpenPopup("About");
            showAboutPopup = false;
        }
        if(showSettingsPopup){
            ImGui::OpenPopup("Settings");
            SettingsView::startup();
            showSettingsPopup = false;
        }

        bool switchWinOpen = true;
        if (ImGui::BeginPopupModal("Switch chunk without saving", &switchWinOpen, ImGuiWindowFlags_AlwaysAutoResize)){
            ImGui::Text("There are unsaved changes to this chunk.");
            ImGui::NewLine();
            ImGui::Text("Would you like to save them before switching?");

            if(ImGui::Button("Cancel")){
                ImGui::CloseCurrentPopup();
                editor->abortChunkSwitch();
            }
            ImGui::SameLine();

            if(ImGui::Button("Save")){
                //Request a save.
                CommandEventSave e;
                EventDispatcher::transmitEvent(EventType::Command, e);

                editor->confirmChunkSwitch();
                ImGui::CloseCurrentPopup();
            }
            ImGui::SameLine();

            if(ImGui::Button("Don't Save")){
                editor->confirmChunkSwitch();
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
        if(!switchWinOpen){
            ImGui::CloseCurrentPopup();
            editor->abortChunkSwitch();
        }

        bool closeWinOpen = true;
        if (ImGui::BeginPopupModal("Close without saving", &closeWinOpen, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("There are unsaved changes to this chunk.");
            ImGui::NewLine();
            ImGui::Text("Would you like to save them before exiting?");

            if(ImGui::Button("Cancel")){
                ImGui::CloseCurrentPopup();
                editor->abortShutdown();
            }
            ImGui::SameLine();

            if(ImGui::Button("Save")){
                //Request a save.
                CommandEventSave e;
                EventDispatcher::transmitEvent(EventType::Command, e);

                editor->confirmShutdown();
            }
            ImGui::SameLine();

            if(ImGui::Button("Don't Save")){
                editor->confirmShutdown();
            }

            ImGui::EndPopup();
        }
        if(!closeWinOpen){
            ImGui::CloseCurrentPopup();
            editor->abortShutdown();
        }

        bool viewWinOpen = true;
        bool shouldViewInEngine = false;
        if (ImGui::BeginPopupModal("Save before viewing", &viewWinOpen, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("There are unsaved changes to this chunk.");
            ImGui::NewLine();
            ImGui::Text("Would you like to save them before viewing in the engine?");

            if(ImGui::Button("Cancel")){
                ImGui::CloseCurrentPopup();
                editor->abortViewInEngine();
            }
            ImGui::SameLine();

            if(ImGui::Button("Save")){
                //Request a save.
                CommandEventSave e;
                EventDispatcher::transmitEvent(EventType::Command, e);

                shouldViewInEngine = true;
                ImGui::CloseCurrentPopup();
            }
            ImGui::SameLine();

            if(ImGui::Button("Don't Save")){
                shouldViewInEngine = true;
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
        if(!viewWinOpen){
            ImGui::CloseCurrentPopup();
            editor->abortViewInEngine();
        }
        if(shouldViewInEngine){
            //Defere it until later just so it doesn't mess with imgui.
            editor->confirmViewInEngine();
        }

        bool aboutWinOpen = true;
        if(ImGui::BeginPopupModal("About", &aboutWinOpen, ImGuiWindowFlags_AlwaysAutoResize)){
            ImGui::Text("Southsea");
            ImGui::Text("A level editor for the avEngine.");
            ImGui::NewLine();

            //char c[50];
            //sprintf(c, "Version %i.%i", SOUTHSEA_VERSION_MAJOR, SOUTHSEA_VERSION_MINOR);
            ImGui::Text("Version %i.%i", SOUTHSEA_VERSION_MAJOR, SOUTHSEA_VERSION_MINOR);

            ImGui::EndPopup();
        }
        if(!aboutWinOpen){
            ImGui::CloseCurrentPopup();
        }

        bool settingsWinOpen = true;
        if (ImGui::BeginPopupModal("Settings", &settingsWinOpen, ImGuiWindowFlags_AlwaysAutoResize)){
            bool shouldClose = SettingsView::drawSettingsContent();
            if(shouldClose) {
                ImGui::CloseCurrentPopup();
                SettingsView::shutdown();
            }

            ImGui::EndPopup();
        }

        ResourceSelectorPopup::updateResourceSelectorPopup();
    }

    bool _drawSelectionToolIcon(SelectionTool tool, const char* toolDescription, SelectionTool currentTool, ImVec2 startPos){
        bool retVal = false;
        static Ogre::TexturePtr iconTex;
        static bool first = true;
        if(first){
            first = false;
            if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "visibleIcon.png")){
                iconTex = Ogre::TextureManager::getSingleton().load("visibleIcon.png", SOUTHSEA_INTERNAL_GROUP_NAME);
            }
        }

        ImVec2 uv0, uv1;
        switch(tool){
            case SelectionTool::ObjectPicker:{
                uv0 = ImVec2(0.3, 0);
                uv1 = ImVec2(0.4, 1);
                break;
            }
            case SelectionTool::TerrainPlacer:{
                uv0 = ImVec2(0.4, 0);
                uv1 = ImVec2(0.5, 1);
                break;
            }
            case SelectionTool::ObjectScale:{
                uv0 = ImVec2(0.5, 0);
                uv1 = ImVec2(0.6, 1);
                break;
            }
            case SelectionTool::ObjectOrientate:{
                uv0 = ImVec2(0.6, 0);
                uv1 = ImVec2(0.7, 1);
                break;
            }
        }

        ImGui::SetCursorPos(startPos);
        if(currentTool == tool){
            ImGuiStyle* style = &ImGui::GetStyle();
            ImGui::PushStyleColor(ImGuiCol_Button, style->Colors[ImGuiCol_ButtonActive]);
        }
        ImGui::PushID((int)tool);
        if(ImGui::ImageButton(iconTex.getPointer(), ImVec2(14, 12), uv0, uv1)){
            auto editor = EditorSingleton::getEditor();
            editor->setCurrentSelectionTool(tool);
        }
        ImGui::PopID();
        if(currentTool == tool){
            ImGui::PopStyleColor(1);
        }
        if(ImGui::IsItemHovered()){
            ImGui::SetTooltip("%s", toolDescription);
            retVal = true;
        }
        return retVal;
    }

    bool _drawGridSnapToolIcon(bool snapEnabled, ImVec2 startPos){
        bool retVal = false;
        static Ogre::TexturePtr iconTex;
        static bool first = true;
        if(first){
            first = false;
            if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "visibleIcon.png")){
                iconTex = Ogre::TextureManager::getSingleton().load("visibleIcon.png", SOUTHSEA_INTERNAL_GROUP_NAME);
            }
        }

        ImGui::SetCursorPos(startPos);
        ImGui::PushID(1234); //Generic id.
        if(snapEnabled){
            ImGuiStyle* style = &ImGui::GetStyle();
            ImGui::PushStyleColor(ImGuiCol_Button, style->Colors[ImGuiCol_ButtonActive]);
        }
        if(ImGui::ImageButton(iconTex.getPointer(), ImVec2(14, 12), ImVec2(0.7, 0), ImVec2(0.8, 1))){
            auto editor = EditorSingleton::getEditor();
            editor->setGridSnapEnabled(!editor->getGridSnapEnabled());
        }
        ImGui::PopID();
        if(snapEnabled){
            ImGui::PopStyleColor(1);
        }
        if(ImGui::IsItemHovered()){
            ImGui::SetTooltip("%s", "Grid Snap");
            retVal = true;
        }
        return retVal;
    }

    void EditorView::_drawRenderWindow(){
        if(!renderWindowOpen) return;

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));

        if(!ImGui::Begin("RenderWindow", &renderWindowOpen, 0)){
            ImGui::PopStyleVar(1);
            ImGui::End();
            return;
        }

        //Window pos is obtained first because if we draw anything the cursor will move from the left corner.
        const ImVec2 windowPos = ImGui::GetCursorScreenPos();
        const ImVec2 windowSize = ImGui::GetContentRegionAvail();
        const bool windowHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem);
        const ImVec2 actualWindowPos = ImGui::GetWindowPos();

        EditorCompositorManager::ViewportInformation info;
        auto editor = EditorSingleton::getEditor();
        editor->getEditorCompositorManager()->getViewportInformation(0, info);

        ImVec2 startPos = ImGui::GetCursorPos();
        ImGui::Image(info.ptr, windowSize, ImVec2(0, 0), ImVec2(info.uv.x, info.uv.y));

        ImGui::PopStyleVar(1);

        ImDrawList* drawList = ImGui::GetWindowDrawList();

        //Keeps track of whether to accept a mouse down action.
        //If one of the selector buttons was hovered then the render window shouldn't register any sort of click.
        bool selectorButtonHovered = false;
        { //Draw the selection tool icons.

            SelectionTool currentTool = editor->getCurrentSelectionTool();

            selectorButtonHovered |= _drawSelectionToolIcon(SelectionTool::ObjectPicker, "Object Picker", currentTool, ImVec2(startPos.x + 10, startPos.y + 10));
            selectorButtonHovered |= _drawSelectionToolIcon(SelectionTool::ObjectScale, "Scale Object", currentTool, ImVec2(startPos.x + 40, startPos.y + 10));
            selectorButtonHovered |= _drawSelectionToolIcon(SelectionTool::ObjectOrientate, "Rotate Object", currentTool, ImVec2(startPos.x + 70, startPos.y + 10));
            selectorButtonHovered |= _drawSelectionToolIcon(SelectionTool::TerrainPlacer, "Terrain Placer", currentTool, ImVec2(startPos.x + 100, startPos.y + 10));

            //Separator line
            drawList->AddLine(ImVec2(windowPos.x + 130, windowPos.y + 10), ImVec2(windowPos.x + 130, windowPos.y + 30), IM_COL32(255, 255, 255, 255));

            //Grid snap button
            selectorButtonHovered |= _drawGridSnapToolIcon(editor->getGridSnapEnabled(), ImVec2(startPos.x + 140, startPos.y + 10));

            drawList->AddLine(ImVec2(windowPos.x + 170, windowPos.y + 10), ImVec2(windowPos.x + 170, windowPos.y + 30), IM_COL32(255, 255, 255, 255));

            ImGui::SetCursorPos(ImVec2(180, 30));
            selectorButtonHovered |= EditorViewDropDown::drawEditorViewDropDown();
        }

        ImGui::End();

        //-----------------
        {
            static bool renderWindowPreviouslyHovered = false;
            static ImVec2 previousMousePosition;


            if(windowHovered){
                ImGuiIO& io = ImGui::GetIO();
                const ImVec2 mousePos(io.MousePos.x - windowPos.x, io.MousePos.y - windowPos.y);

                //Mouse moved
                if(previousMousePosition.x != mousePos.x || previousMousePosition.y != mousePos.y){
                    EditorGuiMouseMovedRenderWindow e;
                    e.mouseX = mousePos.x;
                    e.mouseY = mousePos.y;

                    EventDispatcher::transmitEvent(EventType::EditorGui, e);
                }
                previousMousePosition = mousePos;

                //Mouse buttons
                //-------------------------
                {
                    static const int numButtons = 2;
                    static bool prevButtonDown[numButtons];
                    //Imgui ids mouse buttons based on ints. 0=left, 1=right, 2=middle.
                    //At the moment I only care about the left and right, so just check the first two.

                    bool buttonDown[numButtons];
                    for(int i = 0; i < numButtons; i++){
                        //If any of the selector buttons are hovered, don't count this as a click.
                        buttonDown[i] = ImGui::IsMouseDown(i) && !selectorButtonHovered;
                        if(buttonDown[i] != prevButtonDown[i]){
                            //Button change
                            EditorGuiMouseButtonRenderWindow e;
                            e.mouseX = mousePos.x;
                            e.mouseY = mousePos.y;
                            e.mouseButton = i;
                            e.buttonDown = buttonDown[i];

                            EventDispatcher::transmitEvent(EventType::EditorGui, e);
                        }
                    }

                    //Copy the new values over
                    for(int i = 0; i < numButtons; i++){
                        prevButtonDown[i] = buttonDown[i];
                    }
                }

            }

            if(renderWindowPreviouslyHovered != windowHovered){
                //There was an enter or leave of the window

                EditorGuiCursorLeftEnteredRenderWindow e;
                e.cursorLeft = !windowHovered;
                EventDispatcher::transmitEvent(EventType::EditorGui, e);

            }
            renderWindowPreviouslyHovered = windowHovered;

            static ImVec2 previousWindowSize(-1, -1); //Set it to something unlikely so the change is picked up on startup.
            if(previousWindowSize.x != windowSize.x || previousWindowSize.y != windowSize.y || mFirstFrame){ //If the view has just started out (first frame), send out the new size to make sure things are up to date elsewhere.
                //The window size has changed.
                EditorGuiWindowResize e;
                e.oldWidth = previousWindowSize.x;
                e.oldHeight = previousWindowSize.y;
                e.newWidth = windowSize.x;
                e.newHeight = windowSize.y;

                EventDispatcher::transmitEvent(EventType::EditorGui, e);

                previousWindowSize = windowSize;
            }
        }

        StateType currentState = EditorSingleton::getEditor()->getEditorState()->getCurrentState();
        if(currentState == StateType::TerrainHeightPick){
            std::shared_ptr<TerrainEditorSettings> s = BaseSingleton::getTerrainEditorSettings();
            ImGui::SetTooltip("Height: %f", s->getPreviewHeightBrushValue());
        }

    }

    void EditorView::startupEditor(){
        mFirstFrame = true;

        SceneHierarchyView::startup();
        TerrainPropertiesView::startup();
        ChunkSwitcherView::startup();
        ResourceBrowserView::startup();
        ObjectPropertiesView::startup();
        NavMeshView::startup();
    }

    void EditorView::shutdownEditor(){

        SceneHierarchyView::shutdown();
        TerrainPropertiesView::shutdown();
        ChunkSwitcherView::shutdown();
        ResourceBrowserView::shutdown();
        ObjectPropertiesView::shutdown();
    }
}
