#include "SettingsView.h"

#include "imgui.h"
#include "System/Setup/SystemSettings.h"
#include "System/Setup/SystemSetup.h"

#include <nfd.h>

namespace Southsea{

    char enginePath[128];

    bool SettingsView::drawSettingsContent(){
        ImGui::Text("Path to engine");

        if(ImGui::InputText("##enginePath", enginePath, IM_ARRAYSIZE(enginePath), ImGuiInputTextFlags_EnterReturnsTrue)){

        }
        ImGui::SameLine();
        if(ImGui::Button("Select")){
            nfdchar_t *outPath = NULL;
            nfdresult_t result = NFD_OpenDialog( &outPath, NULL, NULL, NULL );

            if ( result == NFD_OKAY ) {
                strncpy(enginePath, outPath, 128);

                free(outPath);
            }
        }

        ImGui::Separator();

        if(ImGui::Button("Save")){
            _saveSettings();
            return true;
        }

        return false;
    }

    void SettingsView::startup(){
        _resetValues();
    }

    void SettingsView::shutdown(){

    }

    void SettingsView::_resetValues(){
        strcpy(enginePath, SystemSettings::getEngineExecutablePath().c_str());
    }

    void SettingsView::_saveSettings(){
        SystemSettings::_engineExecutablePath = enginePath;

        SystemSetup::updateEngineExecutablePath();

        SystemSetup::writeUserSettingsFile();
    }

}
