#include "ChunkSwitcherView.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Logic/EditorState.h"

#include <stdlib.h>

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"
#include <math.h>
#include <iostream>

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorGuiEvent.h"

namespace Southsea{

    int ChunkSwitcherView::chunkInputBox[2];
    int ChunkSwitcherView::xPos = 0;
    int ChunkSwitcherView::yPos = 0;
    bool ChunkSwitcherView::mouseGridMovementInProgress = false;
    int ChunkSwitcherView::zoomRate = 50;
    int ChunkSwitcherView::zoomVisibleCooldown = 0;
    bool ChunkSwitcherView::mouseButtonChunkSwitchToggle = false;
    bool ChunkSwitcherView::manualChunkSwitchRequested = false;

    int ChunkSwitcherView::canvasWidth = 50;
    int ChunkSwitcherView::canvasHeight = 50;

    void ChunkSwitcherView::drawChunkSwitcherView(bool &windowOpen){

        if(!windowOpen) return;

        if(!ImGui::Begin("ChunkSwitcher", &windowOpen, 0)){
            ImGui::End();
            return;
        }

        Editor* editor = EditorSingleton::getEditor();
        const Chunk::ChunkCoordinate chunkCoords = editor->getEditorChunk()->getChunkCoordinates();

        {
            ImGui::Text("Current Chunk: %i %i", chunkCoords.first, chunkCoords.second);
        }

        {
            ImGui::InputInt2("##chunkCoordsInput", &chunkInputBox[0]);

            if(ImGui::Button("load chunk")){
                manualChunkSwitchRequested = true;

                bool requestSuccess = EditorSingleton::getEditor()->requestChunkSwitch(chunkInputBox[0], chunkInputBox[1]);
                if(!requestSuccess){
                    ImGui::OpenPopup("Failed to switch chunk");
                    manualChunkSwitchRequested = false;
                }
            }

            static bool pOpen2 = true;
            if(ImGui::BeginPopupModal("Failed to switch chunk", &pOpen2, ImGuiWindowFlags_AlwaysAutoResize))
            {
                ImGui::Text("The entered chunk is not within the valid chunk range.");
                ImGui::Text("Please enter a different value.");

                if(ImGui::Button("Ok", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }
        }

        _drawSwitcherGrid();

        ImGui::End();

    }

    void ChunkSwitcherView::startup(){
        Editor* editor = EditorSingleton::getEditor();
        const Chunk::ChunkCoordinate chunkCoords = editor->getEditorChunk()->getChunkCoordinates();
        //Populate the box on ititial startup.
        chunkInputBox[0] = chunkCoords.first;
        chunkInputBox[1] = chunkCoords.second;

        {
            auto metaFile = editor->getMapMetaFile();

            xPos = metaFile->getSwitcherGridCoordX();
            yPos = metaFile->getSwitcherGridCoordY();
            zoomRate = metaFile->getSwitcherGridZoom();
        }

        EventDispatcher::subscribeStatic(EventType::Editor, ChunkSwitcherView::editorEventReceiver);
    }

    void ChunkSwitcherView::shutdown(){
        EventDispatcher::unsubscribeStatic(EventType::Editor, ChunkSwitcherView::editorEventReceiver);
    }

    void ChunkSwitcherView::_drawSwitcherGrid(){
        ImDrawList* draw_list = ImGui::GetWindowDrawList();

        Editor* editor = EditorSingleton::getEditor();
        const Chunk::ChunkCoordinate selectedChunkCoord = editor->getEditorChunk()->getChunkCoordinates();
        const Map& editorMap = editor->getEditorMap();

        const ImVec2 windowSize = ImGui::GetContentRegionAvail();
        const ImVec2 canvas_pos = ImGui::GetCursorScreenPos();

        const int squareSize = zoomRate;
        canvasWidth = windowSize.x;
        canvasHeight = windowSize.x;


        ImGuiIO& io = ImGui::GetIO();
        static ImVec2 prevCanvasMousePos(0, 0);
        const ImVec2 canvasMousePos(io.MousePos.x - canvas_pos.x, io.MousePos.y - canvas_pos.y);
        const ImVec2 mousePosDiff(canvasMousePos.x - prevCanvasMousePos.x, canvasMousePos.y - prevCanvasMousePos.y);
        prevCanvasMousePos = canvasMousePos;
        bool checkHover = false;
        if(ImGui::IsWindowHovered()){
            if(canvasMousePos.x >= 0 && canvasMousePos.y >= 0 && canvasMousePos.x <= canvasWidth && canvasMousePos.y <= canvasHeight){
                checkHover = true;
            }
        }

        if(checkHover){
            if(io.MouseDown[1]){
                if(!mouseGridMovementInProgress){
                    mouseGridMovementInProgress = true;

                    EditorGuiChunkSwitcherGridMouseMove e;
                    e.gridMoveStarted = true;
                    EventDispatcher::transmitEvent(EventType::EditorGui, e);
                }
            }
            else{
                if(mouseGridMovementInProgress){
                    mouseGridMovementInProgress = false;

                    EditorGuiChunkSwitcherGridMouseMove e;
                    EventDispatcher::transmitEvent(EventType::EditorGui, e);

                    //Place the new coordinates into the mapMetaFile.
                    editor->getMapMetaFile()->setSwitcherGridCoords(xPos, yPos);
                }
            }
        }

        int xSquareStart = floor(xPos / squareSize);
        int xSquareEnd = ceil((xPos + canvasWidth) / squareSize);
        int ySquareStart = floor(yPos / squareSize);
        int ySquareEnd = ceil((yPos + canvasHeight) / squareSize);

        ImVec2 canvasEnd_pos(canvas_pos.x + canvasWidth, canvas_pos.y + canvasHeight);
        //TODO might not even need to draw a rect below later on.
        draw_list->AddRectFilled(canvas_pos, canvasEnd_pos, IM_COL32(255, 255, 255, 255));

        int numSquaresX = (canvasWidth / squareSize) + 2;
        int numSquaresY = (canvasHeight / squareSize) + 2;

        Chunk::ChunkCoordinate mouseChunkCoord;

        for(int y = -1; y < numSquaresY; y++){
            for(int x = -1; x < numSquaresX; x++){

                const Chunk::ChunkCoordinate chunkCoord({
                    xSquareStart + x,
                    ySquareStart + y
                });

                bool hovered = false;
                if(checkHover && !mouseGridMovementInProgress){
                    mouseChunkCoord = Chunk::ChunkCoordinate({
                        floor((xPos + canvasMousePos.x) / squareSize),
                        floor((yPos + canvasMousePos.y) / squareSize)
                    });
                    hovered = mouseChunkCoord == chunkCoord;
                }

                if(!editorMap.chunkExists(chunkCoord) && chunkCoord != selectedChunkCoord && !hovered) continue;

                int drawX = canvas_pos.x + x * squareSize;
                int drawY = canvas_pos.y + y * squareSize;

                drawX -= (xPos - (xSquareStart * squareSize));
                drawY -= (yPos - (ySquareStart * squareSize));


                int xStart = drawX;
                int yStart = drawY;
                int xEnd = drawX + squareSize;
                int yEnd = drawY + squareSize;

                if(xEnd <= canvas_pos.x || yEnd <= canvas_pos.y ||
                    xStart >= canvasEnd_pos.x || yStart >= canvasEnd_pos.y) continue;

                if(xStart < canvas_pos.x) xStart = canvas_pos.x;
                if(yStart < canvas_pos.y) yStart = canvas_pos.y;
                if(yEnd > canvasEnd_pos.y) yEnd = canvasEnd_pos.y;
                if(xEnd > canvasEnd_pos.x) xEnd = canvasEnd_pos.x;

                ImU32 col = IM_COL32(200, 200, 200, 255);
                if(chunkCoord == selectedChunkCoord){
                    col = IM_COL32(100, 100, 100, 255);
                }

                draw_list->AddRectFilled(ImVec2(xStart, yStart), ImVec2(xEnd, yEnd), col);

                if(hovered){
                    static const int offset = 2;
                    //+1 as the left needs slightly more padding than the right.
                    ImVec2 coords[4] = {
                        ImVec2(drawX + offset + 1, drawY + offset + 1),
                        ImVec2(drawX + offset + 1, drawY + squareSize - offset),
                        ImVec2(drawX + squareSize - offset, drawY + squareSize - offset),
                        ImVec2(drawX + squareSize - offset, drawY + offset + 1)
                    };

                    _clampQuadCoords(&coords[0], canvas_pos, canvasEnd_pos);

                    draw_list->AddQuad(
                        coords[0], coords[1], coords[2], coords[3],
                        IM_COL32(50, 50, 50, 255), 2.0f
                    );
                }
            }
        }

        _drawAxisLines(true, draw_list, canvas_pos, canvasEnd_pos, xPos, numSquaresX, xSquareStart, squareSize);
        _drawAxisLines(false, draw_list, canvas_pos, canvasEnd_pos, yPos, numSquaresY, ySquareStart, squareSize);

        if(checkHover){
            if(!mouseGridMovementInProgress){
                char c[50];
                sprintf(c, "X: %i   Y: %i", mouseChunkCoord.first, mouseChunkCoord.second);

                ImVec2 drawStart = canvas_pos;
                ImVec2 textSize = ImGui::CalcTextSize(c);
                ImVec2 drawEnd = ImVec2(drawStart.x + textSize.x, drawStart.y + textSize.y);

                draw_list->AddRectFilled(drawStart, drawEnd, IM_COL32(255, 255, 255, 255));

                draw_list->AddText(canvas_pos, IM_COL32(0, 0, 0, 255), c);
            }

            if(io.MouseDown[0] && !mouseButtonChunkSwitchToggle){
                //Request a switch to the clicked chunk.
                editor->requestChunkSwitch(mouseChunkCoord.first, mouseChunkCoord.second);
                mouseButtonChunkSwitchToggle = true;
            }
            if(mouseGridMovementInProgress && editor->getEditorState()->getCurrentState() == StateType::ChunkGridMove){
                xPos -= mousePosDiff.x;
                yPos -= mousePosDiff.y;

                ImGui::SetMouseCursor(ImGuiMouseCursor_ResizeNWSE);
            }
            if(io.MouseWheel != 0){
                zoomRate += io.MouseWheel;
                if(zoomRate < zoomMin) zoomRate = zoomMin;
                if(zoomRate > zoomMax) zoomRate = zoomMax;

                zoomVisibleCooldown = 70;
                editor->getMapMetaFile()->setSwitcherGridZoom(zoomRate);
            }
        }

        if(!io.MouseDown[0] && mouseButtonChunkSwitchToggle)
            mouseButtonChunkSwitchToggle = false;

        //TODO this is different on different systems depending on the timestep, so I need to look into fixing that.
        if(zoomVisibleCooldown > 0){
            zoomVisibleCooldown--;
            _drawZoomBar(canvas_pos, canvasEnd_pos);
        }

    }

    void ChunkSwitcherView::_drawZoomBar(ImVec2 canvasStart, ImVec2 canvasEnd){
        static const int padding = 10;
        static const int barHeight = 10;
        const ImVec2 drawStart(canvasStart.x + padding, canvasEnd.y - padding - barHeight);
        const ImVec2 drawEnd(canvasEnd.x - padding, canvasEnd.y - padding);

        ImDrawList* draw_list = ImGui::GetWindowDrawList();

        draw_list->AddRectFilled(drawStart, drawEnd, IM_COL32(255, 255, 255, 255));

        float percent = float(zoomRate - zoomMin) / float(totalZoomRange);

        const int totalPercent = percent * 100;
        char c[50];
        sprintf(c, "%i%%", totalPercent);

        ImVec2 textSize = ImGui::CalcTextSize("100\% "); //The max length that can be represented.
        draw_list->AddText(drawStart, IM_COL32(0, 0, 0, 255), c);

        const ImVec2 recDrawStart(drawStart.x + textSize.x, drawStart.y);

        const float rectTotalDraw = drawEnd.x - recDrawStart.x;

        draw_list->AddQuad(recDrawStart, ImVec2(recDrawStart.x, drawEnd.y), drawEnd, ImVec2(drawEnd.x, recDrawStart.y), IM_COL32(50, 50, 50, 255), 2.0f);
        draw_list->AddRectFilled(recDrawStart, ImVec2(recDrawStart.x + percent * rectTotalDraw, drawEnd.y), IM_COL32(0, 0, 0, 255));
    }

    void ChunkSwitcherView::_clampQuadCoords(ImVec2* coord, ImVec2 canvasStart, ImVec2 canvasEnd){
        for(int i = 0; i < 4; i++){
            ImVec2* c = coord + i;
            if(c->x < canvasStart.x) c->x = canvasStart.x;
            if(c->y < canvasStart.y) c->y = canvasStart.y;
            if(c->x > canvasEnd.x) c->x = canvasEnd.x;
            if(c->y > canvasEnd.y) c->y = canvasEnd.y;
        }
    }

    void ChunkSwitcherView::_centreOnChunk(int chunkX, int chunkY){
        xPos = chunkX * zoomRate;
        yPos = chunkY * zoomRate;

        const int squareSize = zoomRate;
        const int squaresX = (canvasWidth / squareSize) / 2;
        const int squaresY = (canvasHeight / squareSize) / 2;

        xPos -= (squaresX * zoomRate) - squareSize / 2;
        yPos -= (squaresY * zoomRate) - squareSize / 2;


        EditorSingleton::getEditor()->getMapMetaFile()->setSwitcherGridCoords(xPos, yPos);
    }

    void ChunkSwitcherView::_drawAxisLines(bool xAxis, ImDrawList* dl, ImVec2 canvasPos, ImVec2 canvasEndPos, int posAxis, int numSquaresAxis, int viewportStartAxis, int squareSize){
        for(int i = -1; i < numSquaresAxis; i++){
            float cPos = xAxis ? canvasPos.x : canvasPos.y;
            float cEndPos = xAxis ? canvasEndPos.x : canvasEndPos.y;

            int drawAxis = cPos + i * squareSize;

            drawAxis -= (posAxis - (viewportStartAxis * squareSize));

            int iStart = drawAxis;
            int iEnd = drawAxis + squareSize;

            if(iStart < cPos || iStart >= cEndPos) continue;

            float lineWidth = 1.0f;
            int chunkIdAxis = viewportStartAxis + i;
            if(chunkIdAxis % 5 == 0) lineWidth = 1.5f;

            ImU32 col = IM_COL32(0, 0, 0, 255);
            if(chunkIdAxis == 0) col = IM_COL32(0, 100, 200, 255);

            if(xAxis)
                dl->AddLine(ImVec2(iStart, canvasPos.y), ImVec2(iStart, canvasEndPos.y), col, lineWidth);
            else
                dl->AddLine(ImVec2(canvasPos.x, iStart), ImVec2(canvasEndPos.x, iStart), col, lineWidth);
        }
    }

    bool ChunkSwitcherView::editorEventReceiver(const Event& event){
        const EditorEvent& e = (const EditorEvent&)event;
        if(e.eventCategory() == EditorEventCategory::ChunkSwitch){
            const ChunkSwitchEvent& event = (const ChunkSwitchEvent&)e;
            chunkInputBox[0] = event.newChunkX;
            chunkInputBox[1] = event.newChunkY;

            if(manualChunkSwitchRequested){
                _centreOnChunk(event.newChunkX, event.newChunkY);
                manualChunkSwitchRequested = false;
            }
        }
        if(e.eventCategory() == EditorEventCategory::ChunkSwitchDeclined){
            manualChunkSwitchRequested = false; //In the event that a manual chunk was requested, but was declined we shouldn't do the switching procedure.
        }

        return false;
    }
}
