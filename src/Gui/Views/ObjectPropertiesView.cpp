#include "ObjectPropertiesView.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/OgreTreeManager.h"
#include "Editor/Scene/TreeObjectManager.h"

#include "Editor/Action/Actions/Object/BasicCoordinatesChange.h"
#include "Editor/Action/ActionStack.h"

#include "ObjectPropertyWidgets/OgreMeshWidget.h"
#include "ObjectPropertyWidgets/PhysicsShapeWidget.h"
#include "ObjectPropertyWidgets/CollisionSenderWidget.h"
#include "ObjectPropertyWidgets/NavMarkerWidget.h"
#include "ObjectPropertyWidgets/DataPointWidget.h"
#include "ResourceButton.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"

#include "Editor/Action/Actions/Object/MeshObjectAction.h"
#include "Editor/Action/Actions/Object/PhysicsShapeAction.h"
#include "Editor/Action/Actions/Object/PhysicsCollisionAction.h"
#include "Gui/Views/TerrainPropertiesView.h"

#include "OgreVector3.h"
#include "OgreSceneNode.h"

#include <iostream>

namespace Southsea{

    static CoordinatesWidgetInput in;
    static CoordinatesWidgetInput oldIn; //Keep a track of the old values for the actions.

    static ResourceButtonData meshButtonData;
    static ResourceButtonData oldMeshButtonData;
    static ResourceButtonData meshMaterialButtonData;
    static ResourceButtonData oldMeshMaterialButtonData;

    static std::set<EntryId> currentSelection;
    static const ObjectData* currentObjectData = 0;

    static PhysicsShapeType oldPhysicsShape = PhysicsShapeType::box;
    static PhysicsShapeType currentPhysicsShape = PhysicsShapeType::box;

    //Collision settings
    static CollisionSenderWidgetData collisionSenderData;
    static CollisionSenderWidgetData oldCollisionSenderData;

    //Nav marker settings
    static NavMarkerObjectData navMarkerData;

    //Data point settings
    static DataPointObjectData dataPointData;

    static bool terrainSelected = false;

    void ObjectPropertiesView::drawObjectPropertiesView(bool &windowOpen){

        if(!windowOpen) return;

        if(!ImGui::Begin("Properties", &windowOpen, 0)){
            ImGui::End();
            return;
        }

        //const auto& selectedIds = EditorSingleton::getEditor()->getSceneTreeManager()->getSelectedIds();
        if(terrainSelected){
            //ImGui::Text("Terrain selected");
            assert(currentSelection.size() <= 0);

            TerrainPropertiesView::drawTerrainProperties();

            ImGui::End();
            return;
        }else if(currentSelection.size() <= 0){
            const char* objString = "No objects selected.";
            const ImVec2 textSize = ImGui::CalcTextSize(objString);

            const ImVec2 windowSize = ImGui::GetContentRegionAvail();
            ImGui::SetCursorPosY(30 + textSize.y);
            ImGui::SetCursorPosX((windowSize.x / 2) - textSize.x / 2 );
            ImGui::Text("%s", objString);

            ImGui::End();
            return;
        }else if(currentSelection.size() > 1){
            ImGui::Text("%i objects selected.", static_cast<int>(currentSelection.size()));
        }


        bool out[3];
        if(drawBasicCoordinatesWidget(in, out)){
            //Something changed.
            assert(out[0] || out[1] || out[2]);

            _changeBasicCoordinates(out, oldIn);
        }

        EntryId id = *(currentSelection.begin());
        if(currentObjectData){
            if(currentObjectData->type() == ObjectType::mesh){
                unsigned int result = drawOgreMeshWidget(&meshButtonData, &oldMeshButtonData, &meshMaterialButtonData, &oldMeshMaterialButtonData);
                if(result != 0){
                    if( (OgreMeshWidgetResult::Mesh & result) != 0){
                        MeshObjectAction* obj = new MeshObjectAction(id, MeshObjectAction::Type::meshChange);
                        obj->populate(oldMeshButtonData.displayName, meshButtonData.displayName);
                        obj->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(obj);
                    }
                    else if((OgreMeshWidgetResult::Material & result) != 0){
                        MeshObjectAction* obj = new MeshObjectAction(id, MeshObjectAction::Type::materialChange);
                        obj->populate(oldMeshMaterialButtonData.displayName, meshMaterialButtonData.displayName);
                        obj->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(obj);
                    }else assert(false);
                }
            }
            else if(currentObjectData->type() == ObjectType::physicsShape){
                int oldShape = (int)oldPhysicsShape;
                int newShape = oldShape;
                if(drawPhysicsShapeWidget(oldShape, &newShape)){
                    //Change the shape used.
                    PhysicsShapeAction* action = new PhysicsShapeAction(id, PhysicsShapeAction::Type::shapeChange);
                    PhysicsShapeType newType = (PhysicsShapeType)newShape;
                    action->populate(oldPhysicsShape, newType);
                    action->performAction();
                    EditorSingleton::getEditor()->getActionStack()->pushAction(action);
                }
            }
            else if(currentObjectData->type() == ObjectType::collisionSender){
                CollisionSenderObjectDataTypes result = drawCollisionSenderWidget(&oldCollisionSenderData, &collisionSenderData);
                if(result != CollisionSenderObjectDataTypes::none){
                    PhysicsCollisionAction* action = new PhysicsCollisionAction(id);
                    action->populate(result, &(oldCollisionSenderData.data), &(collisionSenderData.data));
                    action->performAction();
                    EditorSingleton::getEditor()->getActionStack()->pushAction(action);
                }
            }
            else if(currentObjectData->type() == ObjectType::navMarker){
                drawNavMarkerWidget(id, &navMarkerData);
            }
            else if(currentObjectData->type() == ObjectType::dataPoint){
                drawDataPointWidget(id, &dataPointData);
            }
        }

        ImGui::End();

    }

    void ObjectPropertiesView::_changeBasicCoordinates(const bool (&out)[3], const CoordinatesWidgetInput& old){
        //auto treeMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();

        //const auto& selectedIds = treeMan->getSelectedIds();

        if(currentSelection.size() == 1){
            EntryId targetId = *(currentSelection.begin());

            //These will have actions in future.
            BasicCoordinatesChange::Type t;
            Ogre::Vector3 oldData;
            Ogre::Vector3 newData;
            if(out[0]){
                oldData = Ogre::Vector3(old.position[0], old.position[1], old.position[2]);
                newData = Ogre::Vector3(in.position[0], in.position[1], in.position[2]);
                t = BasicCoordinatesChange::Type::position;
            }
            if(out[1]){
                oldData = Ogre::Vector3(old.scale[0], old.scale[1], old.scale[2]);
                newData = Ogre::Vector3(in.scale[0], in.scale[1], in.scale[2]);
                t = BasicCoordinatesChange::Type::scale;
            }
            if(out[2]){
                oldData = Ogre::Vector3(old.orientation[0], old.orientation[1], old.orientation[2]);
                newData = Ogre::Vector3(in.orientation[0], in.orientation[1], in.orientation[2]);
                t = BasicCoordinatesChange::Type::orientate;
            }
            BasicCoordinatesChange* changeAction = new BasicCoordinatesChange();
            changeAction->populate(targetId, oldData, newData, t, false, currentObjectData->type());
            changeAction->performAction();
            EditorSingleton::getEditor()->getActionStack()->pushAction(changeAction);
        }
    }

    void ObjectPropertiesView::_resetSelectionInfo(){
        auto sceneTreeMan = EditorSingleton::getEditor()->getSceneTreeManager();
        auto treeMan = EditorSingleton::getEditor()->getTreeObjectManager();

        currentSelection = sceneTreeMan->getSelectedIds();
        //std::cout << currentSelection.size() << '\n';
        if(currentSelection.size() == 1){
            currentObjectData = treeMan->getDataObject(*(currentSelection.begin()));
        }else{
            currentObjectData = 0;
        }

        terrainSelected = sceneTreeMan->isTerrainSelected();

        _updateObjectInfo();
    }

    void ObjectPropertiesView::_updateObjectInfo(){
        auto ogreMan = EditorSingleton::getEditor()->getOgreTreeManager();

        //Clear the old values.
        meshButtonData.type = ResourceType::mesh;
        meshButtonData.clear();
        oldPhysicsShape = PhysicsShapeType::box;
        currentPhysicsShape = PhysicsShapeType::box;

        meshMaterialButtonData.type = ResourceType::material;
        meshMaterialButtonData.clear();

        if(currentSelection.size() == 1){
            Ogre::SceneNode* node = ogreMan->getNodeById( *(currentSelection.begin()) );

            const Ogre::Vector3 pos = node->getPosition();
            in.position[0] = pos.x;
            in.position[1] = pos.y;
            in.position[2] = pos.z;

            const Ogre::Vector3 scale = node->getScale();
            in.scale[0] = scale.x;
            in.scale[1] = scale.y;
            in.scale[2] = scale.z;

            if(currentObjectData){
                switch(currentObjectData->type()){
                    case ObjectType::mesh:{
                        const MeshObjectData* meshData = static_cast<const MeshObjectData*>(currentObjectData);
                        meshButtonData.state = ResButtonState::None;
                        meshButtonData.res.name = meshData->meshName;
                        meshButtonData.displayName = meshData->meshName;

                        meshMaterialButtonData.displayName = meshData->meshMaterial;
                        meshMaterialButtonData.res.name = meshData->meshMaterial;
                        break;
                    }
                    case ObjectType::physicsShape:{
                        const PhysicsShapeObjectData* shapeData = static_cast<const PhysicsShapeObjectData*>(currentObjectData);
                        oldPhysicsShape = shapeData->shapeType;
                        currentPhysicsShape = shapeData->shapeType;
                        break;
                    }
                    case ObjectType::collisionSender:{
                        const CollisionSenderObjectData* senderData = static_cast<const CollisionSenderObjectData*>(currentObjectData);
                        collisionSenderData = *senderData;
                        oldCollisionSenderData = *senderData;
                        break;
                    }
                    case ObjectType::navMarker:{
                        const NavMarkerObjectData* senderData = static_cast<const NavMarkerObjectData*>(currentObjectData);
                        navMarkerData = *senderData;
                        break;
                    }
                    case ObjectType::dataPoint:{
                        const DataPointObjectData* objectData = static_cast<const DataPointObjectData*>(currentObjectData);
                        dataPointData = *objectData;
                        break;
                    }
                    default: break;
                }
            }
        }else{
            in.position[0] = 0.0f;
            in.position[1] = 0.0f;
            in.position[2] = 0.0f;

            in.scale[0] = 1.0f;
            in.scale[1] = 1.0f;
            in.scale[2] = 1.0f;
        }
        oldIn = in;

    }

    void ObjectPropertiesView::_updateDataForId(EntryId id){
        if( *(currentSelection.begin()) == id){
            _updateObjectInfo();
        }
    }

    void ObjectPropertiesView::_processObjectDeleted(EntryId id){
        auto it = currentSelection.find(id);
        if(it == currentSelection.end()) return; //There is nothing to deal with.

        currentSelection.erase(id);
        currentObjectData = 0;
    }

    void ObjectPropertiesView::startup(){
        EventDispatcher::subscribeStatic(EventType::Editor, ObjectPropertiesView::editorEventReceiver);

        _resetSelectionInfo();
    }

    void ObjectPropertiesView::shutdown(){
        EventDispatcher::unsubscribeStatic(EventType::Editor, ObjectPropertiesView::editorEventReceiver);
    }

    bool ObjectPropertiesView::editorEventReceiver(const Event& event){
        const EditorEvent& e = (const EditorEvent&)event;
        if(e.eventCategory() == EditorEventCategory::SceneTreeSelectionChanged){
            _resetSelectionInfo();
        }
        if(e.eventCategory() == EditorEventCategory::SceneTreeValueChange){
            _resetSelectionInfo();
        }
        if(e.eventCategory() == EditorEventCategory::SceneTreeItemDeleted){
            const SceneTreeItemDeleted& deletedEvent = (const SceneTreeItemDeleted&)event;
            _processObjectDeleted(deletedEvent.id);
        }
        if(e.eventCategory() == EditorEventCategory::ChunkSwitch){
            _resetSelectionInfo();
        }

        return false;
    }

}
