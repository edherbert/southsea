#include "TerrainPropertiesView.h"

#include "imgui.h"
#include <iostream>

#include "System/BaseSingleton.h"
#include "System/Editor/TerrainEditorSettings.h"
#include "System/Editor/BrushThumbnailManager.h"

#include "OgreRoot.h"
#include "OgreHlmsManager.h"
#include "OgreHlms.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraDatablock.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerra.h"
#include "OgreTextureManager.h"

#include "Editor/Chunk/EditedChunk.h"
#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/Terrain/TerrainManager.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/Meta/EngineChunkMeta.h"

#include "Editor/Chunk/Terrain/TerrainDatablockInfoManager.h"
#include "Editor/Chunk/Terrain/TerrainDatablockUtils.h"
#include <cmath>

#include "Editor/Action/ActionStack.h"
#include "Editor/Action/Actions/Terrain/LevelTerrainAction.h"
#include "Editor/Action/Actions/Terrain/BlendLayerSetTextureAction.h"
#include "Editor/Action/Actions/Terrain/BlendLayerAction.h"

#include "Editor/Chunk/NavMesh/NavMeshManager.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorEvent.h"
#include "ResourceButton.h"

namespace Southsea{
    Ogre::HlmsTerraDatablock* TerrainPropertiesView::currentTerrainDatablock = 0;
    TerrainPropertiesView::DetailLayerData TerrainPropertiesView::DetailLayerData::DEFAULT;

    float TerrainPropertiesView::diffuseCol[3] = {1, 1, 1};
    TerrainPropertiesView::DetailLayerData TerrainPropertiesView::layerDetail[4];
    bool TerrainPropertiesView::terrainEnabled = false;

    void TerrainPropertiesView::drawTerrainProperties(){

        EngineChunkMeta& engineMeta = EditorSingleton::getEditor()->getEditorChunk()->getEngineChunkMetaFile();
        terrainEnabled = engineMeta.getTerrainEnabled();
        if(ImGui::Checkbox("Enable terrain", &terrainEnabled)){
            auto terrainManager = EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager();
            terrainManager->setTerrainEnabled(terrainEnabled);
            engineMeta.setTerrainEnabled(terrainEnabled);
        }
        ImGui::NewLine();

        ImGui::Text("Terrain Tool");
        ImGui::Separator();

        //Tool selection
        {
            //static int prevTool = 0;
            bool changed = false;
            static int toolType = 0;
            changed |= ImGui::RadioButton("Raise Lower", &toolType, 0);
            changed |= ImGui::RadioButton("Blend Paint", &toolType, 1);
            changed |= ImGui::RadioButton("Smooth", &toolType, 2);
            changed |= ImGui::RadioButton("Set Height", &toolType, 3);
            ImGui::NewLine();

            if(changed){
                static const TerrainEditorSettings::TerrainTool tools[] = {
                    TerrainEditorSettings::TerrainTool::RAISE_LOWER,
                    TerrainEditorSettings::TerrainTool::BLEND_PAINT,
                    TerrainEditorSettings::TerrainTool::SMOOTH,
                    TerrainEditorSettings::TerrainTool::SET_HEIGHT
                };
                TerrainEditorSettings::TerrainTool t = tools[toolType];

                BaseSingleton::getTerrainEditorSettings()->setCurrentTool(t);
            }

        }

        ImGui::Text("Terrain Brush Settings");
        ImGui::Separator();
        ImGui::NewLine();

        //Tool specific settings.
        TerrainEditorSettings::TerrainTool t = BaseSingleton::getTerrainEditorSettings()->getCurrentTool();
        if(t == TerrainEditorSettings::TerrainTool::RAISE_LOWER){
            _drawRaiseLowerSettings();
        }
        else if(t == TerrainEditorSettings::TerrainTool::BLEND_PAINT){
            _drawDetailLevelSelector();
        }
        else if(t == TerrainEditorSettings::TerrainTool::SET_HEIGHT){
            _drawSetHeightBrushView();
        }

        _drawTerrainBrushSettings();


        //I'm most likely going to move this out of the panel and to a menu item.
        ImGui::NewLine();
        ImGui::Separator();
        static float val = 0;
        ImGui::InputFloat("terrainHeight", &val);
        if(ImGui::Button("Set Terrain Height")){
            LevelTerrainAction* a = new LevelTerrainAction();
            auto terrainManager = EditorSingleton::getEditor()->getEditorChunk()->getTerrainManager();

            const float maxTerrainHeight = terrainManager->getMaximumTerrainHeight();
            if(val < 0) val = 0;
            if(val > maxTerrainHeight) val = maxTerrainHeight;

            a->setLevelHeight(val);
            terrainManager->fillLevelTerrainAction(a);

            a->performAction();
            EditorSingleton::getEditor()->getActionStack()->pushAction(a);
        }

        ImGui::Separator();
        ImGui::Text("Material Settings");
        ImGui::NewLine();

        {
            static Ogre::Vector3 prev(-1, -1, -1);
            ImGui::ColorEdit3("Diffuse", diffuseCol);
            if(_vec3Colour(diffuseCol, prev)){ //Change
                const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*currentTerrainDatablock->getNameStr());

                /*BlendLayerAction* a = new BlendLayerAction(BlendLayerAction::DIFFUSE, currentTerrainDatablock, 0); //0 because there's no layers in diffuse.
                a->setDiffuse(data.diffuse, prev);
                a->performAction();
                EditorSingleton::getEditor()->getActionStack()->pushAction(a);*/

                //TODO this still needs to be performed and pushed. The way to do that would be with a timer, but I don't feel like doing that at the moment.
                BlendLayerAction a(BlendLayerAction::DIFFUSE, currentTerrainDatablock, 0);
                a.setDiffuse(prev, Ogre::Vector3::ZERO);
                a.performAction();
                // currentTerrainDatablock->setDiffuse(prev);
                // TerrainDatablockInfoManager::setDiffuse(*currentTerrainDatablock->getNameStr(), prev);
            }
        }
        for(int i = 0; i < 4; i++){
            if(i == 0) ImGui::Separator();

            DetailLayerData &d = layerDetail[i];

            char string[20];
            sprintf(string, "Detail %i", i);
            if(ImGui::Checkbox(string, &d.enabled)){
                _invertDatablockLayer(i, d.enabled);
            }

            if(!d.enabled){
                ImGui::Separator();
                continue;
            }

            char offsetS[20];
            sprintf(offsetS, "Offset##Off%i", i);

            char scaleS[20];
            sprintf(scaleS, "Scale##Sca%i", i);
            if(ImGui::InputFloat2(offsetS, d.offsetVec) || ImGui::InputFloat2(scaleS, d.scaleVec)){

                const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*currentTerrainDatablock->getNameStr());

                const Ogre::Vector4& vec = data.layers[i].offsetScale;
                const Ogre::Vector4 s(d.offsetVec[0], d.offsetVec[1], d.scaleVec[0], d.scaleVec[1]);

                BlendLayerAction* a = new BlendLayerAction(BlendLayerAction::OFFSET_SCALE, currentTerrainDatablock, i);
                a->setOffsetScale(vec, s);
                a->performAction();
                EditorSingleton::getEditor()->getActionStack()->pushAction(a);
            }

            char roughnessS[20];
            uint8 idVal = 0;
            sprintf(roughnessS, "Roughness##Rou%i", i);
            if(ImGui::InputFloat(roughnessS, &d.roughness)){
                idVal = 1;
            }

            char metalnessS[20];
            sprintf(metalnessS, "Metalness##Met%i", i);
            if(ImGui::InputFloat(metalnessS, &d.metalness)){
                idVal = 2;
            }

            {
                //Checking for metalnes / roughness change.
                if(idVal != 0){
                    static const BlendLayerAction::ActionType ac[2] = {BlendLayerAction::ROUGHNESS, BlendLayerAction::METALNESS};
                    const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*currentTerrainDatablock->getNameStr());

                    float oldVal, newVal;
                    if(idVal == 1){ //Roughness
                        oldVal = data.layers[i].roughness;
                        newVal = d.roughness;
                    }else{ //Metalness
                        oldVal = data.layers[i].metalness;
                        newVal = d.metalness;
                    }

                    BlendLayerAction* a = new BlendLayerAction(ac[idVal - 1], currentTerrainDatablock, i);
                    a->setFloatValue(oldVal, newVal);
                    a->performAction();
                    EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                }
            }

            ImGui::NewLine();

            {
                // char textureS[20];
                // sprintf(textureS, "Texture##Tex%i", i);
                // ImGui::InputText(textureS, &(d.textureString[0]), IM_ARRAYSIZE(d.textureString));

                d.textureButtonData.type = ResourceType::texture;
                if(Southsea::ResourceButton( &(d.textureButtonData) )){
                    const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*currentTerrainDatablock->getNameStr());
                    const Ogre::String& old = data.layers[i].diffuseTexture;

                    BlendLayerSetTextureAction* a = new BlendLayerSetTextureAction(currentTerrainDatablock, i, old, d.textureButtonData.displayName);
                    a->performAction();
                    EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                }
            }

            ImGui::Separator();
        }
    }

    void TerrainPropertiesView::_drawRaiseLowerSettings(){
        static int prevPaintType = 0;
        static int paintType = prevPaintType;
        ImGui::RadioButton("Raise", &paintType, 0); ImGui::SameLine();
        ImGui::RadioButton("Lower", &paintType, 1);

        if(prevPaintType != paintType){
            prevPaintType = paintType;
            BaseSingleton::getTerrainEditorSettings()->setRaiseLowerPaintType(paintType);
        }
    }

    void TerrainPropertiesView::_drawDetailLevelSelector(){
        ImGui::Text("Paint layer");

        static int prevPaintLayer = -1;
        static int paintLayer = 0;
        ImGui::RadioButton("0", &paintLayer, 0); ImGui::SameLine();
        ImGui::RadioButton("1", &paintLayer, 1); ImGui::SameLine();
        ImGui::RadioButton("2", &paintLayer, 2); ImGui::SameLine();
        ImGui::RadioButton("3", &paintLayer, 3);

        if(prevPaintLayer != paintLayer){
            prevPaintLayer = paintLayer;
            BaseSingleton::getTerrainEditorSettings()->setCurrentDetailPaintLayer(paintLayer);
        }

        ImGui::NewLine();
    }

    void TerrainPropertiesView::_drawTerrainBrushSettings(){
        //Set these to be something impossible so the editor settings are populated right away.
        static float prevBrushSize = -1.0f;
        static float prevBrushStrength = -1.0f;

        static float brushSize = 100.0f;
        ImGui::DragFloat("Brush Size", &brushSize, 1.0f, 1.0f, 100.0f);

        static float brushStrength = 100.0f;
        ImGui::DragFloat("Brush Strength", &brushStrength, 0.05f, 0.1f, 100.0f);

        if(prevBrushSize != brushSize){
            //Brush size is limited to whole numbers.
            //Later on I need to build a box which needs to be a whole number.
            brushSize = std::floor(brushSize);
            prevBrushSize = brushSize;
            BaseSingleton::getTerrainEditorSettings()->setBrushSize(brushSize);
        }
        if(prevBrushStrength != brushStrength){
            prevBrushStrength = brushStrength;
            BaseSingleton::getTerrainEditorSettings()->setBrushStrength(brushStrength);
        }

        _drawShapeBrushView();
        _drawTextureBrushView();
    }

    static int brushType;
    void TerrainPropertiesView::_drawShapeBrushView(){
        ImGui::Separator();
        if(ImGui::RadioButton("Shape Brushes", &brushType, 0)){
            BaseSingleton::getTerrainEditorSettings()->setCurrentBrushMode(BrushMode::SHAPE);
        }
        if(brushType != 0) return;

        //For now
        //Ogre::Texture* brushTex = BaseSingleton::getBrushThumbnailManager()->getTextureForBrush(0);
        Ogre::Texture* brushTex = BaseSingleton::getTerrainEditorSettings()->getShapeBrushThumbnail();
        ImGui::Image(brushTex, ImVec2(50, 50));

        {
            const char* items[] = {"Circle", "Square"};
            static int current = 0;
            if(ImGui::Combo("Brush type", &current, items, IM_ARRAYSIZE(items))){
                BaseSingleton::getTerrainEditorSettings()->setShapeBrushType(current);
            }
        }

        {
            const char* items[] = {"Sin", "Linear", "Point"};
            static int current = 0;
            if(ImGui::Combo("Function", &current, items, IM_ARRAYSIZE(items))){
                BaseSingleton::getTerrainEditorSettings()->setShapeBrushFunction(current);
            }
        }

        {
            static float hardness = 0.0f;
            if(ImGui::SliderFloat("Hardness", &hardness, 0.0f, 1.0f)){
                BaseSingleton::getTerrainEditorSettings()->setShapeBrushHardness(hardness);
            }
        }

        //Hardness
    }

    void TerrainPropertiesView::_drawTextureBrushView(){
        ImGui::Separator();
        if(ImGui::RadioButton("Texture Brushes", &brushType, 1)){
            BaseSingleton::getTerrainEditorSettings()->setCurrentBrushMode(BrushMode::TEXTURE);
        }
        if(brushType != 1) return;

        int currentBrush = BaseSingleton::getTerrainEditorSettings()->getCurrentSelectedTextureBrush();

        const ImVec2 regionSize = ImGui::GetContentRegionAvail();

        const char* brushNames[] = {"Grunge", "Ground", "Bristles", "Vegetation", "Star"};
        if(ImGui::BeginChild("mapScroller", ImVec2(regionSize.x, 100), true)){
            const ImVec2 canvas_pos = ImGui::GetCursorScreenPos();
            for(int i = 0; i < sizeof(brushNames) / sizeof(const char*); i++){
                const ImVec2 startPos = ImGui::GetCursorPos();

                if(currentBrush == i){
                    static const int padding = 7;
                    ImDrawList* drawList = ImGui::GetWindowDrawList();
                    const ImVec2 drawStart(startPos.x + canvas_pos.x - padding, startPos.y + canvas_pos.y - padding);
                    drawList->AddRectFilled(drawStart, ImVec2(drawStart.x + regionSize.x, drawStart.y + 50), IM_COL32(80, 80, 80, 80));
                }

                Ogre::Texture* brushTex = BaseSingleton::getBrushThumbnailManager()->getTextureForBrush(i);
                ImGui::Image(brushTex, ImVec2(50, 50));

                ImGui::SameLine();
                ImGui::Text("%s", brushNames[i]);

                ImGui::SetCursorPos(startPos);
                ImGui::Dummy(ImVec2(regionSize.x, 50));

                if(ImGui::IsItemClicked(0)){
                    std::cout << "Brush selected " << i << '\n';
                    BaseSingleton::getTerrainEditorSettings()->setCurrentSelectedTextureBrush(i);
                }
            }
        }
        ImGui::EndChild();
    }

    void TerrainPropertiesView::_drawSetHeightBrushView(){
        auto s = BaseSingleton::getTerrainEditorSettings();
        float brushValue = s->getHeightBrushValue();
        if(ImGui::InputFloat("Height##terrainHeight", &brushValue)){
            s->setHeightBrushValue(brushValue);
        }
    }

    inline void TerrainPropertiesView::_copyToVector(const Ogre::Vector3& target, float (&destination)[3]){
        destination[0] = target[0];
        destination[1] = target[1];
        destination[2] = target[2];
    }


    bool TerrainPropertiesView::_vec3Colour(float (&colour)[3], Ogre::Vector3& prev){
        const Ogre::Vector3 current(colour[0], colour[1], colour[2]);
        if(current != prev){
            prev = current;
            return true;
        }

        return false;
    }

    void TerrainPropertiesView::startup(){
        _clearDetailLayerData();

        _reobtainTerrainDatablock();

        EventDispatcher::subscribeStatic(EventType::Editor, TerrainPropertiesView::editorEventReceiver);
    }

    void TerrainPropertiesView::_reobtainTerrainDatablock(){
        Ogre::Root& root = Ogre::Root::getSingleton();
        const std::string datablockName = EditorSingleton::getEditor()->getEditorChunk()->getDatablockName();
        Ogre::HlmsDatablock *datablock = root.getHlmsManager()->getHlms("Terra")->getDatablock(datablockName);
        currentTerrainDatablock = dynamic_cast<Ogre::HlmsTerraDatablock*>(datablock);
        _populateEditorFromDatablock(currentTerrainDatablock);
    }

    void TerrainPropertiesView::shutdown(){
        EventDispatcher::unsubscribeStatic(EventType::Editor, TerrainPropertiesView::editorEventReceiver);
    }

    void TerrainPropertiesView::_clearDetailLayerData(){
        for(int i = 0; i < 4; i++){
            DetailLayerData &d = layerDetail[i];

            d.offsetVec[0] = DetailLayerData::DEFAULT.offsetVec[0];
            d.offsetVec[1] = DetailLayerData::DEFAULT.offsetVec[1];
            d.scaleVec[0] = DetailLayerData::DEFAULT.scaleVec[0];
            d.scaleVec[1] = DetailLayerData::DEFAULT.scaleVec[1];

            d.roughness = DetailLayerData::DEFAULT.roughness;
            d.metalness = DetailLayerData::DEFAULT.metalness;

            d.textureButtonData = DetailLayerData::DEFAULT.textureButtonData;
        }
    }

    void TerrainPropertiesView::_populateEditorFromDatablock(Ogre::HlmsTerraDatablock* db){
        const TerrainDatablockData& data = TerrainDatablockInfoManager::getDatablockData(*db->getNameStr());

        _copyToVector(data.diffuse, diffuseCol);

        for(int i = 0; i < 4; i++){
            DetailLayerData &d = layerDetail[i];
            const TerrainDatablockLayerData& layerData = data.layers[i];

            d.roughness = layerData.roughness;
            d.metalness = layerData.metalness;

            const Ogre::Vector4& offsetScale = layerData.offsetScale;
            d.offsetVec[0] = offsetScale[0];
            d.offsetVec[1] = offsetScale[1];
            d.scaleVec[0] = offsetScale[2];
            d.scaleVec[1] = offsetScale[3];

            d.textureButtonData.setName(layerData.diffuseTexture); //Even if the string is empty, we still want to copy it, as the user might have wanted a blank string (represents the checkered dummy texture).

            d.enabled = layerData.enabled;
        }
    }

    void TerrainPropertiesView::_invertDatablockLayer(int layer, bool enabled){
        const DetailLayerData &d = layerDetail[layer];

        BlendLayerAction* a = new BlendLayerAction(BlendLayerAction::ENABLE_DISABLE, currentTerrainDatablock, layer);
        a->setEnableDisable(enabled);
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

    }

    bool TerrainPropertiesView::editorEventReceiver(const Event& event){
        const EditorEvent& e = (const EditorEvent&)event;
        if(e.eventCategory() == EditorEventCategory::TerrainDatablockChange){
            _populateEditorFromDatablock(currentTerrainDatablock); //Don't update the editor checkbox, as here we're performing an action.
        }
        if(e.eventCategory() == EditorEventCategory::ChunkSwitch){
            _reobtainTerrainDatablock();
        }

        return false;
    }
}
