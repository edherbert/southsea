#pragma once

#include "Editor/Action/Actions/Scene/ObjectInsertionType.h"

#include "imgui.h"
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class DialogTreeManager;
    class Event;

    class SceneHierarchyView{
    public:
        static void drawSceneHierarchyView(bool& windowOpen);

        static void startup();
        static void shutdown();

        static bool commandEventReceiver(const Event& e);

    private:
        static void _drawSceneHierarchy();

        static void _endRename();
        static void _updateEmptyDrag();

        static void _drawSceneTreeDragIcons();

        static void _drawPopups(int totalSelectedEntries);
        static void _checkMouseButtons(int& anyClickRegistered, bool& backgroundItemClicked);

        static void _getSceneTypeUVCoords(SceneEntryType t, ImVec2* uv0, ImVec2* uv1);

        static DialogTreeManager* mDialogTreeManager;

        static int mTreeIndexCurrentlyRenaming;
        static char renamingItemString[128];

        static int mInsertItemIndex;
        static ObjectInsertionType mInsertItemType;

        //The index of the item which was clicked most recently. This will revert back to -1 when the mouse is released.
        static int mCurrentlyClicked;
    };
}
