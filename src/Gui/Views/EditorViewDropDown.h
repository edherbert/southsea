#pragma once

namespace Southsea{
    class EditorViewDropDown{
    public:
        EditorViewDropDown() = delete;
        ~EditorViewDropDown() = delete;

        static bool drawEditorViewDropDown();

    private:
        static bool _drawDropdown(float x, float y);
    };
}
