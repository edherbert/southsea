#include "StartupView.h"

#include "imgui.h"

#include "System/BaseSingleton.h"
#include "System/Setup/PreviousProjects.h"

#include "System/Project/Project.h"
#include "DisabledButton.h"

#include <string>
#include <iostream>

#include <nfd.h>

#include "Editor/EditorSingleton.h"
#include "SettingsView.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/ProjectEvent.h"

namespace Southsea{

    int StartupView::mSelectedProject = -1;
    int StartupView::mSelectedMap = -1;
    std::vector<std::string> StartupView::mCurrentMapsList;
    MapsDirectory StartupView::mMapsDir;

    void StartupView::drawStartupView(){

        ImGuiIO& io = ImGui::GetIO();

        //Add a bit of an offset to the coordinates to hide the rounded edges of the window.
        //I'm sure there's a way to style it accordingly, but this is easier :)
        ImGui::SetNextWindowPos(ImVec2(-10, -10), ImGuiCond_Always);
        ImGui::SetNextWindowSize(ImVec2(io.DisplaySize.x + 20, io.DisplaySize.y + 20), ImGuiCond_Always);

        bool pOpen;
        ImGui::Begin("StartupView", &pOpen,
                     ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav);

        float panelPadding = io.DisplaySize.x * 0.1;
        float panelSize = io.DisplaySize.x * 0.4;
        float halfWidth = io.DisplaySize.x / 2;

        {
            const static std::string editorNameString("==Southsea==");
            float titleWidth = ImGui::CalcTextSize(editorNameString.c_str()).x;
            const static std::string descriptionNameString("A level editor for the avEngine");
            float descriptionWidth = ImGui::CalcTextSize(descriptionNameString.c_str()).x;


            ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 100);
            ImGui::SetCursorPosX(halfWidth - titleWidth / 2 );
            ImGui::Text("%s", editorNameString.c_str());
            ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 10);
            ImGui::SetCursorPosX(halfWidth - descriptionWidth / 2);
            ImGui::Text("%s", descriptionNameString.c_str());
        }

        ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 100);

        {
            ImGui::SetCursorPosX(panelPadding);
            ImGui::Text("--Recent Projects--");
            ImGui::SameLine(panelPadding + panelSize);
            ImGui::Text("--Maps--");
        }

        //-------------------
        {
            //Each box should take up 40% of the space.
            //The remaing 10% on either side is used as padding.
            ImGui::SetCursorPosX(panelPadding);

            const std::vector<Project>& prevEntries = BaseSingleton::getPreviousProjects()->getPreviousProjects();

            if(ImGui::BeginChild("previousProjectsScroller", ImVec2(panelSize, 200), true, 0)){
                ImVec2 pos = ImGui::GetCursorPos();
                ImGui::Dummy(ImVec2(panelSize, 175));
                bool clicked = ImGui::IsItemClicked(0);
                ImGui::SetCursorPos(pos);

                if(prevEntries.size() <= 0){
                    ImGui::Text("Please create or load a project to begin.");
                }

                bool anySelected = false;
                for(int i = prevEntries.size() - 1; i >= 0; i--){
                    const Project& e = prevEntries[i];
                    bool selected = i == mSelectedProject;

                    ImGui::Selectable(e.getProjectPath().c_str(), &selected, 0);
                    anySelected |= ImGui::IsItemClicked(0);
                    if(selected){
                        //If that selectable was selected in the mean time, the current index should change to it.
                        if(mSelectedProject != i){
                            _updateMapsList(e);
                            mSelectedProject = i;
                            mSelectedMap = -1; //If we're changing selected project, make it so no maps are selected.
                        }
                    }
                    ImGui::Separator();

                    if(clicked && !anySelected){
                        mSelectedProject = -1;
                    }
                }
            }
            ImGui::EndChild();

            ImGui::SameLine();
            if(ImGui::BeginChild("mapScroller", ImVec2(panelSize, 200), true, 0)){
                if(mSelectedProject < 0){
                    ImGui::Text("Please select a project on the left.");
                }else{
                    if(mCurrentMapsList.size() <= 0){
                        ImGui::Text("No maps could be found at the path \n %s", mMapsDir.getMapsDirectory().c_str());
                    }else{
                        ImVec2 pos = ImGui::GetCursorPos();
                        ImGui::Dummy(ImVec2(panelSize, 175));
                        bool clicked = ImGui::IsItemClicked(0);
                        ImGui::SetCursorPos(pos);

                        bool anySelected = false;
                        for(int i = 0; i < mCurrentMapsList.size(); i++){
                            const std::string& mapString = mCurrentMapsList[i];
                            bool selected = i == mSelectedMap;

                            ImGui::Selectable(mapString.c_str(), &selected, 0);
                            anySelected |= ImGui::IsItemClicked(0);
                            if(selected){
                                mSelectedMap = i;
                            }
                        }

                        if(clicked && !anySelected){
                            mSelectedMap = -1;
                        }
                    }
                }
            }
            ImGui::EndChild();
        }

        //-------------------
        {
            ImGui::SetCursorPosX(panelPadding);
            if(ImGui::Button("Create new project", ImVec2(panelSize, 0))){
                ImGui::OpenPopup("New Project");
            }
            if(mSelectedProject >= 0){ //If we have a selection show the button.
                ImGui::SameLine();
                if(ImGui::Button("Create new map", ImVec2(panelSize, 0))){
                    ImGui::OpenPopup("Create New Map");
                }
            }
        }

        //-------------------
        {
            ImGui::SetCursorPosX(panelPadding);
            if(ImGui::Button("Open project", ImVec2(panelSize, 0))){
                nfdchar_t *outPath = NULL;
                nfdresult_t result = NFD_OpenDialog(&outPath, NULL, NULL, NULL);

                if ( result == NFD_OKAY ) {
                    std::cout << "Project provided at: " << outPath << '\n';
                    Project proj(outPath);

                    if(proj.isValid()){
                        //That project does actually exist.
                        //Notify that a project was opened.
                        ProjectEventProjectOpened e;
                        e.openedProject = &proj;

                        EventDispatcher::transmitEvent(EventType::Project, e);

                        ImGui::CloseCurrentPopup();
                    }else{
                        ImGui::OpenPopup("Failed to open project!");
                    }
                    free(outPath);
                }
                else if ( result == NFD_CANCEL ) {
                    puts("User pressed cancel.");
                }
                else {
                    printf("Error: %s\n", NFD_GetError() );
                }
            }
        }

        //-------------------
        {
            ImGui::NewLine();
            ImGui::SetCursorPosX(panelPadding + (panelSize / 2));
            if(DisabledButton("Start Editing", mSelectedMap >= 0, ImVec2(panelSize, 0))){
                const std::string& mapName = mCurrentMapsList[mSelectedMap];
                Map map = mMapsDir.getMap(mapName);

                const std::vector<Project>& prevEntries = BaseSingleton::getPreviousProjects()->getPreviousProjects();

                //I copy it here, just incase the event leads to memory corruption if I passed the pointer into the vector (something else might touch the vector while doing the event).
                Project proj = prevEntries[mSelectedProject];

                if(!EditorSingleton::startEditor(proj, map)){
                    //TODO Error in startup, so do some error popup or something.
                }

                ProjectEventProjectStartedEditing e;
                e.openedProject = &proj;
                e.openedMap = &map;
                EventDispatcher::transmitEvent(EventType::Project, e);
            }
        }

        {
            const char* buttonText = "Settings";
            const ImVec2 textSize = ImGui::CalcTextSize(buttonText);
            ImGui::SetCursorPos(ImVec2(io.DisplaySize.x - textSize.x - 5.0f, io.DisplaySize.y - textSize.y - 5.0f));
            if(ImGui::Button(buttonText)){
                ImGui::OpenPopup("Settings");
                SettingsView::startup();
            }
        }



        if (ImGui::BeginPopupModal("New Project", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("Name:");
            static char projectNameStr[128] = "";
            ImGui::SetItemDefaultFocus();
            ImGui::InputText("##label1", projectNameStr, IM_ARRAYSIZE(projectNameStr));

            ImGui::NewLine();

            ImGui::Text("Project path:");
            static char projectPathStr[128] = "";
            ImGui::InputText("##label2", projectPathStr, IM_ARRAYSIZE(projectPathStr));
            ImGui::SameLine();
            if(ImGui::Button("Select")){
                nfdchar_t *outPath = NULL;
                nfdresult_t result = NFD_PickFolder( &outPath, NULL );

                if ( result == NFD_OKAY ) {
                    std::cout << "Project provided at: " << outPath << '\n';
                    strncpy(projectPathStr, outPath, 128);

                    free(outPath);
                }
            }

            ImGui::Separator();

            if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
            ImGui::SameLine();

            if (ImGui::Button("OK", ImVec2(120, 0))) {

                Project proj(projectPathStr, projectNameStr);

                if(proj.createBaseProjectFiles()){ //True means it was succesful.
                    ImGui::CloseCurrentPopup();

                    ProjectEventBasicProjectCreated e;
                    e.createdProject = &proj;

                    EventDispatcher::transmitEvent(EventType::Project, e);
                }else{
                    ImGui::OpenPopup("Create New Project Failed!");
                }
            }

                //Failure to create project popup.
                static bool pOpen2 = true;
                if(ImGui::BeginPopupModal("Create New Project Failed!", &pOpen2, ImGuiWindowFlags_AlwaysAutoResize))
                {
                    ImGui::Text("Failed to create a project at that location.");
                    if(ImGui::Button("Ok", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                    ImGui::EndPopup();
                }

            ImGui::EndPopup();
        }

        /*if (ImGui::BeginPopupModal("Open Project", NULL, ImGuiWindowFlags_AlwaysAutoResize)){
            ImGui::Text("Project path:");
            static char projectPathStr[128] = "";
            ImGui::InputText("##projPath", projectPathStr, IM_ARRAYSIZE(projectPathStr));

            ImGui::Separator();
            if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
            ImGui::SameLine();
            if (ImGui::Button("Ok", ImVec2(120, 0))) {

            }

            static bool pOpen2 = true;
            if(ImGui::BeginPopupModal("Failed to open project!", &pOpen2, ImGuiWindowFlags_AlwaysAutoResize))
            {
                ImGui::Text("Could not find a valid avEngine project at that location.");
                ImGui::NewLine();
                ImGui::Text("Please make sure it contains a valid avSetup.cfg file.");
                ImGui::Separator();
                if(ImGui::Button("Ok", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }

            ImGui::EndPopup();
        }*/

        if (ImGui::BeginPopupModal("Create New Map", NULL, ImGuiWindowFlags_AlwaysAutoResize)){
            ImGui::Text("Map Name:");
            static char mapNameStr[128] = "";
            ImGui::InputText("##projPath", mapNameStr, IM_ARRAYSIZE(mapNameStr));

            ImGui::Separator();
            if (ImGui::Button("Cancel", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
            ImGui::SameLine();
            if (ImGui::Button("Ok", ImVec2(120, 0))) {
                if(mMapsDir.createMap(std::string(mapNameStr))){
                    mMapsDir.findAvailableMaps(mCurrentMapsList); //Rescan the directories to get the newest values.
                    ImGui::CloseCurrentPopup();
                }else{
                    //Error
                    ImGui::OpenPopup("Failed to create map!");
                }
            }

            static bool pOpen = true;
            if(ImGui::BeginPopupModal("Failed to create map!", &pOpen, ImGuiWindowFlags_AlwaysAutoResize))
            {
                ImGui::Text("Could not create a map with that name.");
                ImGui::NewLine();
                ImGui::Text("Please make sure there are no other maps with the same name.");
                ImGui::Separator();
                if(ImGui::Button("Ok", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
                ImGui::EndPopup();
            }

            ImGui::EndPopup();
        }

        bool settingsWinOpen = true;
        if(ImGui::BeginPopupModal("Settings", &settingsWinOpen, ImGuiWindowFlags_AlwaysAutoResize)){
            bool shouldClose = SettingsView::drawSettingsContent();
            if(shouldClose){
                ImGui::CloseCurrentPopup();
                settingsWinOpen = false;
            }

            ImGui::EndPopup();
        }
        if(!settingsWinOpen){
            ImGui::CloseCurrentPopup();
            SettingsView::shutdown();
        }


        ImGui::End();
    }

    void StartupView::_updateMapsList(const Project& proj){
        const AVSetupFile& file = proj.getAVSetupFile();

        mMapsDir.setDirectoryPath(file.getMapsDirectory());
        mMapsDir.findAvailableMaps(mCurrentMapsList); //Populate the maps list here.
    }

    void StartupView::start(){
        std::shared_ptr<PreviousProjects> previousProjects = BaseSingleton::getPreviousProjects();

        int selectedProject = previousProjects->getMostRecentProjectIdx();
        int selectedMap = previousProjects->getMostRecentMapIdx();

        if(selectedProject >= 0 && selectedMap >= 0){
            mSelectedProject = selectedProject;
            mSelectedMap = selectedMap;
            _updateMapsList(previousProjects->getPreviousProjects()[mSelectedProject]);
        }
    }

}
