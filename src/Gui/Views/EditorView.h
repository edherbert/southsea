#pragma once

namespace Southsea{
    class EditorView{
    public:
        static void drawEditorView();

        static void startupEditor();
        static void shutdownEditor();

    private:
        static void _drawEditorMenuBar();
        static void _checkKeyCommands();

        static void _drawRenderWindow();

        static void _drawSystemPopups();

        static bool mFirstFrame;

        static bool renderWindowOpen;
        static bool chunkSwitcherViewOpen;
        static bool resourceBrowserViewOpen;
        static bool sceneHierarchyViewOpen;
        static bool ObjectPropertiesViewOpen;
        static bool navMeshViewOpen;

    };
}
