#pragma once

#include <vector>
#include <string>

#include "System/Project/MapsDirectory.h"

namespace Southsea{
    class Project;

    class StartupView{
    public:
        static void drawStartupView();

        static void start();

    private:
        //The index of the currently selected project. If -1 that means no project is selected.
        static int mSelectedProject;
        static int mSelectedMap;

        static void _updateMapsList(const Project& proj);

        //The list of maps belonging to the current project.
        //We store them here so we don't have to query the file system each frame.
        static std::vector<std::string> mCurrentMapsList;

        //static std::string mMapsSearchPath;
        static MapsDirectory mMapsDir;
    };
}
