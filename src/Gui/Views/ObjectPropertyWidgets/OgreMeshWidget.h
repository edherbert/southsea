#pragma once

namespace Southsea{
    struct ResourceButtonData;

    namespace OgreMeshWidgetResult{
        enum OgreMeshWidgetResult : unsigned int{
            Mesh = 1u << 0,
            Material = 1u << 1
        };
    };

    unsigned int drawOgreMeshWidget(ResourceButtonData* data, ResourceButtonData* oldData, ResourceButtonData* materialData, ResourceButtonData* materialOldData);
}
