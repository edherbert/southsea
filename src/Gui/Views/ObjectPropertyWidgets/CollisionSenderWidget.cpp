#include "CollisionSenderWidget.h"

#include "World/Physics/Worlds/CollisionWorldUtils.h"
#include "imgui.h"
#include <iostream>

namespace Southsea{
    CollisionSenderObjectDataTypes drawCollisionSenderWidget(CollisionSenderWidgetData* oldData, CollisionSenderWidgetData* newData){

        ImGui::Text("Collision Sender");
        ImGui::Separator();

        CollisionSenderObjectDataTypes retVal = CollisionSenderObjectDataTypes::none;

        const char* items[] = { "Box", "Sphere", "Capsule"};
        const char* item_current = items[(int)newData->data.shapeType];
        if(ImGui::BeginCombo("shape", item_current, 0)){
            for(int n = 0; n < IM_ARRAYSIZE(items); n++){
                bool is_selected = (item_current == items[n]);
                if(ImGui::Selectable(items[n], is_selected)){
                    item_current = items[n];
                    newData->data.shapeType = static_cast<PhysicsShapeType>(n);
                    retVal = CollisionSenderObjectDataTypes::shape;
                }
                if(is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }
        ImGui::Separator();

        {
            ImGui::Text("Object Target");

            const AV::uint8 numTargetNames = AV::CollisionObjectTarget::numTargetNames;
            for(int i = 0; i < numTargetNames; i++){
                ImGui::SetCursorPosX(20.0f);
                if(ImGui::Checkbox(AV::CollisionObjectTarget::targetNames[i], &(newData->data.targetValues[i]) )){
                    //newData->data.targetValues[i] = !(newData->data.targetValues[i]);
                    retVal = CollisionSenderObjectDataTypes::target;
                }
            }
        }

        ImGui::Separator();

        {
            ImGui::Text("Event Type");

            const AV::uint8 numObjectEvents = AV::CollisionObjectEvent::numObjectEvents;
            for(int i = 0; i < numObjectEvents; i++){
                ImGui::SetCursorPosX(20.0f);
                if(ImGui::Checkbox(AV::CollisionObjectEvent::eventNames[i], &(newData->data.eventValues[i]) )){
                    retVal = CollisionSenderObjectDataTypes::event;
                }
            }
        }

        ImGui::Separator();

        {
            if(ImGui::InputInt("id", &(newData->data.id))){
                retVal = CollisionSenderObjectDataTypes::id;
            }
        }

        ImGui::Separator();

        {
            ImGui::Text("Callback Script");

            newData->scriptButtonData.type = ResourceType::script;
            if(Southsea::ResourceButton(&(newData->scriptButtonData))){
                retVal = CollisionSenderObjectDataTypes::script;
                //Write the values from the button data to the resource.
                //I didn't want to have a resourcebutton data in the sceneTypes struct, so I just copy it here.
                newData->data.script = newData->scriptButtonData.res;
                oldData->data.script = oldData->scriptButtonData.res;
            }

            ImGui::Text("Function:");
            ImGui::InputText("##functionInput", newData->functionNameStr, IM_ARRAYSIZE(newData->functionNameStr));
            if(ImGui::IsItemDeactivatedAfterEdit()){
                retVal = CollisionSenderObjectDataTypes::closure;
                newData->data.closureName = newData->functionNameStr;
            }
        }

        return retVal;
    }
}
