#include "OgreMeshWidget.h"

#include "Gui/Views/ResourceButton.h"

#include "imgui.h"

namespace Southsea{

    unsigned int drawOgreMeshWidget(ResourceButtonData* data, ResourceButtonData* oldData,
        ResourceButtonData* materialData, ResourceButtonData* materialOldData){

        ImGui::Text("Ogre Mesh");

        unsigned int retVal = 0;

        if(Southsea::ResourceButton(data, oldData)){
            retVal |= OgreMeshWidgetResult::Mesh;
        }

        ImGui::Separator();

        ImGui::Text("Material");
        if(Southsea::ResourceButton(materialData, materialOldData)){
            retVal |= OgreMeshWidgetResult::Material;
        }

        return retVal;
    }
}
