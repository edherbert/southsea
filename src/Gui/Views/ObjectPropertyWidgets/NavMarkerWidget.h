#pragma once

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    struct ResourceButtonData;

    bool drawNavMarkerWidget(EntryId currentId, NavMarkerObjectData* currentData);
}
