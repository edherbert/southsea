#pragma once

namespace Southsea{

    struct CoordinatesWidgetInput{
        float position[3];
        float scale[3];
        float orientation[3];
    };

    bool drawBasicCoordinatesWidget(CoordinatesWidgetInput& in, bool (&out)[3]);
}
