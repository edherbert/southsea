#include "BasicCoordinatesWidget.h"

#include "OgreTextureManager.h"
#include "Platforms/OgreSetup/SouthseaResourceGroups.h"

#include <imgui.h>

namespace Southsea{
    bool insertResetButton(CoordinatesWidgetInput& in, int id){

        bool different = false;
        if(id == 0 || id == 2){ //Position or orientation
            float* target = id == 0 ? &in.position[0] : &in.orientation[0];
            if( (*target++) != 0.0f || (*target++) != 0.0f || (*target++) != 0.0f) different = true;
        }else{
            if( in.scale[0] != 1.0f || in.scale[1] != 1.0f || in.scale[2] != 1.0f) different = true;
        }
        if(!different) return false; //If nothing is different from the default don't bother drawing the button.

        static Ogre::TexturePtr iconTex;
        static bool first = true;
        if(first){
            first = false;
            if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "visibleIcon.png")){
                iconTex = Ogre::TextureManager::getSingleton().load("visibleIcon.png", SOUTHSEA_INTERNAL_GROUP_NAME);
            }
        }

        ImGui::SameLine();
        ImGui::PushID(id);
        bool changed = false;
        if(ImGui::ImageButton(iconTex.getPointer(), ImVec2(14, 12), ImVec2(0.6, 0), ImVec2(0.7, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(0.8, 0.8, 0.8, 1))){
            //Reset the values.
            if(id == 0 || id == 2){
                float* target = id == 0 ? &in.position[0] : &in.orientation[0];
                for(int i = 0; i < 3; i++) *target++ = 0.0f;
            }else{
                float* target = &in.scale[0];
                for(int i = 0; i < 3; i++) *target++ = 1.0f;
            }
            changed = true;
        }
        ImGui::PopID();

        return changed;
    }

    bool drawBasicCoordinatesWidget(CoordinatesWidgetInput& in, bool (&out)[3]){

        ImGui::Text("Transform");
        ImGui::Separator();

        out[0] = out[1] = out[2] = false;

        ImGui::InputFloat3("position", in.position, "%.3f");
        out[0] |= ImGui::IsItemDeactivatedAfterEdit();
        out[0] |= insertResetButton(in, 0);

        ImGui::InputFloat3("scale", in.scale, "%.3f");
        out[1] |= ImGui::IsItemDeactivatedAfterEdit();
        out[1] |= insertResetButton(in, 1);

        ImGui::InputFloat3("orientation", in.orientation, "%.3f");
        out[2] |= ImGui::IsItemDeactivatedAfterEdit();
        out[2] |= insertResetButton(in, 2);


        ImGui::Separator();

        return out[0] || out[1] || out[2];
    }
}
