#include "NavMarkerWidget.h"

#include "imgui.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/EditedChunk.h"
#include "Editor/Chunk/NavMesh/NavMeshManager.h"
#include "Editor/Action/Actions/Object/NavMarkerAction.h"
#include "Editor/Action/ActionStack.h"

namespace Southsea{
    bool drawNavMarkerWidget(EntryId currentId, NavMarkerObjectData* currentData){

        ImGui::Text("Nav marker");
        ImGui::Separator();

        ImGui::Text("Contributes to meshes");

        std::shared_ptr<NavMeshManager> meshManager = EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager();
        const std::vector<NavMeshData>& meshData = meshManager->getCurrentMeshes();

        for(NavMeshId i = 0; i < meshData.size(); i++){
            if(!meshData[i].populated) continue;
            const NavMeshData& d = meshData[i];
            //This isn't as efficient as I might have wanted. It's doing a few list searches here.
            bool selected = currentData->contributingMeshes.find(i) != currentData->contributingMeshes.end();
            if(ImGui::Checkbox(d.meshName.c_str(), &selected)){
                //Value changed.
                NavMarkerAction *a = new NavMarkerAction(currentId, NavMarkerAction::Type::contributionChange);
                a->populateMarkerContribution(i, selected);
                a->performAction();
                EditorSingleton::getEditor()->getActionStack()->pushAction(a);
            }
        }

        return true;
    }
}
