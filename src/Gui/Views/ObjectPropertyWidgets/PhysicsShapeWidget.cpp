#include "PhysicsShapeWidget.h"

#include "imgui.h"

namespace Southsea{
    bool drawPhysicsShapeWidget(int oldShape, int* outShape){

        ImGui::Text("Physics Shape");
        ImGui::Separator();

        bool retVal = false;

        const char* items[] = { "Box", "Sphere", "Capsule"};
        const char* item_current = items[oldShape];
        if(ImGui::BeginCombo("shape", item_current, 0)){
            for(int n = 0; n < IM_ARRAYSIZE(items); n++){
                bool is_selected = (item_current == items[n]);
                if(ImGui::Selectable(items[n], is_selected)){
                    item_current = items[n];
                    retVal = true;
                    *outShape = n;
                }
                if(is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        ImGui::Separator();

        return retVal;
    }
}
