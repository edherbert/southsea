#pragma once

#include "Gui/Views/ResourceButton.h"
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    struct ResourceButtonData;

    /**
    A wrapper around the collision sender data, specifically including resource button data.
    */
    struct CollisionSenderWidgetData{
        CollisionSenderObjectData data;

        ResourceButtonData scriptButtonData;
        char functionNameStr[128];

        const CollisionSenderWidgetData& operator=(const CollisionSenderObjectData& a){
            data = a;

            scriptButtonData = a.script;
            //Copy the closure name into the string buffer.
            snprintf(functionNameStr, 128, "%s", a.closureName.c_str());

            return *this;
        }
    };

    CollisionSenderObjectDataTypes drawCollisionSenderWidget(CollisionSenderWidgetData* oldData, CollisionSenderWidgetData* newData);
}
