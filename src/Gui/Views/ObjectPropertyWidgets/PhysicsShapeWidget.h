#pragma once

namespace Southsea{
    struct ResourceButtonData;

    bool drawPhysicsShapeWidget(int oldShape, int* outShape);
}
