#pragma once

#include "Editor/Scene/SceneTypes.h"

namespace Southsea{

    bool drawDataPointWidget(EntryId id, DataPointObjectData* data);
}
