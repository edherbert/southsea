#include "DataPointWidget.h"

#include "Editor/EditorSingleton.h"
#include "Editor/DataPoint/DataPointManager.h"
#include "Editor/Action/ActionStack.h"
#include "Editor/Action/Actions/Object/DataPointAction.h"

#include "imgui.h"

namespace Southsea{
    bool drawDataPointWidget(EntryId id, DataPointObjectData* data){

        ImGui::Text("Data Point");
        std::shared_ptr<DataPointManager> dataManager = EditorSingleton::getEditor()->getDataPointManager();
        const std::vector<DataPointManager::DataPointEntry>& pointData = dataManager->getDataPoints();
        const std::vector<DataPointManager::DataPointSubTypeEntry>& subPointData = dataManager->getDataSubTypePoints();

        bool valuesEnabled =
            data->dataType < pointData.size() &&
            data->subDataType < subPointData.size();

        if(!valuesEnabled){
            ImGui::NewLine();
            ImGui::Text("No data points defined.");
            ImGui::Text("Define them in the dataPointTypes.json file.");
            return false;
        }

        {
            const char* itemCurrent = pointData[data->dataType].name.c_str();
            if(ImGui::BeginCombo("Type", itemCurrent, 0)){
                for(int i = 0; i < pointData.size(); i++){
                    bool is_selected = (data->dataType == i);
                    if(ImGui::Selectable(pointData[i].name.c_str(), is_selected)){
                        DataPointAction* a = new DataPointAction(id, DataPointObjectData::TYPE);
                        a->populate(static_cast<int>(data->dataType), i);
                        a->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                    }
                    if(is_selected)
                        ImGui::SetItemDefaultFocus();
                }
                ImGui::EndCombo();
            }
        }

        {
            const DataPointManager::DataPointEntry& e = pointData[data->dataType];
            const char* itemCurrent = subPointData[e.start + data->subDataType].name.c_str();
            {
                if(ImGui::BeginCombo("Sub type", itemCurrent, 0)){
                    for(int i = e.start; i < e.end; i++){
                        bool is_selected = (data->subDataType + e.start == i);
                        if(ImGui::Selectable(subPointData[i].name.c_str(), is_selected)){
                            int realIdx = i - e.start;
                            DataPointAction* a = new DataPointAction(id, DataPointObjectData::SUBTYPE);
                            a->populate(static_cast<int>(data->subDataType), realIdx);
                            a->performAction();
                            EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                        }
                        if(is_selected)
                            ImGui::SetItemDefaultFocus();
                    }
                    ImGui::EndCombo();
                }
            }
        }

        {
            int oldValue = data->userData;
            if(ImGui::InputInt("id", &(data->userData))){
                DataPointAction* a = new DataPointAction(id, DataPointObjectData::USERDATA);
                a->populate(oldValue, data->userData);
                a->performAction();
                EditorSingleton::getEditor()->getActionStack()->pushAction(a);
            }
        }

        return false;
    }
}
