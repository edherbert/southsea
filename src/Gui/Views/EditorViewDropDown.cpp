#include "EditorViewDropDown.h"

#include "Editor/Action/Actions/Scene/ViewportSettingsAction.h"

#include "Editor/Action/ActionStack.h"
#include "Editor/EditorSingleton.h"
#include "imgui.h"

namespace Southsea{

    bool showDropDown;

    bool EditorViewDropDown::drawEditorViewDropDown(){
        static bool first = true;
        if(first){
            first = false;
            showDropDown = false;
        }

        const ImVec2 windowPos = ImGui::GetWindowPos();
        const ImVec2 buttonPos = ImGui::GetCursorPos();
        const char* buttonLabel = "View";
        const ImVec2 labelSize = ImGui::CalcTextSize(buttonLabel);
        bool buttonPressed = ImGui::Button(buttonLabel, ImVec2(labelSize.x + 20, 20));

        bool buttonHovered = ImGui::IsItemHovered();

        if(buttonPressed){
            showDropDown = !showDropDown;
        }

        if(showDropDown){
            const ImGuiStyle& style = ImGui::GetStyle();
            bool dropDownHovered = _drawDropdown(buttonPos.x + windowPos.x, buttonPos.y + windowPos.y + 10 + style.WindowPadding.y);
            if(!dropDownHovered && (ImGui::IsMouseClicked(0) || ImGui::IsMouseClicked(1)) ){
                showDropDown = false;
            }

            return dropDownHovered | buttonHovered;
        }
        else return false | buttonHovered;
    }

    bool EditorViewDropDown::_drawDropdown(float x, float y){
        ImGui::SetNextWindowPos(ImVec2(x, y));
        ImGui::SetNextWindowFocus();

        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        static bool winOpen = true;
        if(!ImGui::Begin("EditorViewPopup", &winOpen, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize)){
            ImGui::End();
            ImGui::PopStyleVar();
            return false;
        }

        const bool hovered = ImGui::IsWindowHovered();
        {

            //TODO save and remember some of these values.
            Editor* e = EditorSingleton::getEditor();
            bool showPhysicsShapes = e->getPhysicsShapesVisible();
            if(ImGui::Checkbox("Show Physics Shapes", &showPhysicsShapes)){
                //Use window id 0 for now.
                ViewportSettingsAction* obj = new ViewportSettingsAction(0);
                obj->populateForVisibilityChange(ViewportSettingsAction::PHYSICS_SHAPE_VISIBLE, showPhysicsShapes);
                obj->performAction();
                EditorSingleton::getEditor()->getActionStack()->pushAction(obj);
            }

            bool showNavMeshes = e->getNavMeshVisible();
            if(ImGui::Checkbox("Show Nav Meshes", &showNavMeshes)){
                ViewportSettingsAction* obj = new ViewportSettingsAction(0);
                obj->populateForVisibilityChange(ViewportSettingsAction::NAV_MESH_VISIBLE, showNavMeshes);
                obj->performAction();
                EditorSingleton::getEditor()->getActionStack()->pushAction(obj);
            }

            bool showOuterTerrains = e->getOuterTerrainsVisible();
            if(ImGui::Checkbox("Draw outer terrains", &showOuterTerrains)){
                e->setOuterTerrainsVisible(showOuterTerrains);
            }

            ImGui::End();
            ImGui::PopStyleVar();
        }

        return hovered;
    }
}
