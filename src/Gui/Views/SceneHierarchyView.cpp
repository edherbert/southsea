#include "SceneHierarchyView.h"

#include "OgreTextureManager.h"
#include "Platforms/OgreSetup/SouthseaResourceGroups.h"
#include "System/BaseSingleton.h"
#include "System/Editor/DraggedResourceInfo.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorGuiEvent.h"
#include "Event/Events/CommandEvent.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Action/ActionStack.h"
#include "Editor/Action/Actions/Scene/ObjectRenameAction.h"
#include "Editor/Action/Actions/Scene/ObjectInsertionAction.h"
#include "Editor/Action/Actions/Scene/TreeRearrangeAction.h"
#include "Editor/Action/Actions/Scene/ChangeVisibilityAction.h"
#include "Editor/Action/Actions/Scene/ScenePasteAction.h"
#include "Editor/Logic/EditorState.h"

#include "Editor/Scene/SceneTreeManager.h"
#include "Editor/Scene/CopyStateManager.h"

#include "Editor/Scene/SceneFileParserInterface.h"
//From the avEngine.
#include "World/Slot/Recipe/AvScene/AvSceneFileParserInterface.h"

#include <iostream>

namespace Southsea{
    int SceneHierarchyView::mTreeIndexCurrentlyRenaming = -1;
    char SceneHierarchyView::renamingItemString[128] = "";

    int SceneHierarchyView::mInsertItemIndex = -1;
    ObjectInsertionType SceneHierarchyView::mInsertItemType = ObjectInsertionType::none;

    int SceneHierarchyView::mCurrentlyClicked = -1;

    void SceneHierarchyView::startup(){
        EventDispatcher::subscribeStatic(EventType::Command, SceneHierarchyView::commandEventReceiver);
    }

    void SceneHierarchyView::shutdown(){
        EventDispatcher::unsubscribeStatic(EventType::Command, SceneHierarchyView::commandEventReceiver);
    }

    void _performPaste(){
        ScenePasteAction* a = new ScenePasteAction();
        EditorSingleton::getEditor()->getCopyStateManager()->populatePasteAction(a);
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
    }

    void _performCut(){
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        if(sceneMan->getSelectedCount() <= 0) return;
        EditorSingleton::getEditor()->getCopyStateManager()->copyCurrentSelection();
        sceneMan->deleteCurrentSelection();
    }

    void _performSaveNodes(){
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        if(sceneMan->getSelectedCount() <= 0) return;
        EditorSingleton::getEditor()->getSceneTreeManager()->saveCurrentSelection();
    }

    void _performDuplicate(){
        auto sceneMan = EditorSingleton::getEditor()->getSceneTreeManager();
        if(sceneMan->getSelectedCount() <= 0) return;

        ScenePasteAction* a = new ScenePasteAction();
        EditorSingleton::getEditor()->getCopyStateManager()->populatePasteActionForDuplicate(a);
        a->performAction();
        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
    }

    void SceneHierarchyView::drawSceneHierarchyView(bool& windowOpen){
        if(!windowOpen) return;

        if(!ImGui::Begin("Scene", &windowOpen, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse)){
            ImGui::End();
            return;
        }

        _drawSceneHierarchy();

        ImGui::End();
    }

    void SceneHierarchyView::_drawSceneHierarchy(){
        static const ImU32 HOVER_COLOUR = IM_COL32(150, 150, 150, 255);
        static const ImU32 SELECTED_COLOUR = IM_COL32(220, 220, 220, 255);
        static const ImU32 INVISIBLE_COLOUR = IM_COL32(0, 0, 0, 0);
        static const ImU32 INSERT_COLOUR = IM_COL32(67, 148, 69, 255);

        ImGuiIO& io = ImGui::GetIO();
        const ImGuiStyle& style = ImGui::GetStyle();

        ImDrawList* drawList = ImGui::GetWindowDrawList();
        const ImVec2 regionSize = ImGui::GetContentRegionAvail();
        const ImVec2 windowPos = ImGui::GetWindowPos();
        const ImVec2 canvasPos = ImGui::GetCursorScreenPos();

        auto sceneTreeManager = EditorSingleton::getEditor()->getSceneTreeManager();

        bool emptyScene = false;
        const std::vector<SceneEntry>& entries = sceneTreeManager->getSceneTree();
        if(entries.size() <= 2) {
            //2 entries are the smallest expected, as you have the root node and its associate terminator.
            //If it's less than that something went wrong in the deserialisation procedure.
            assert(entries.size() == 2);
            emptyScene = true;

            //The user might be trying to do something like drag an object into an empty hierarchy.
            _updateEmptyDrag();
            //return;
        }

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        const int hierarchyScrollHeight = regionSize.y - 28;
        ImGui::BeginChild("sceneHierarchyScroller", ImVec2(regionSize.x, hierarchyScrollHeight), true, ImGuiWindowFlags_NoBackground);

        static bool dragBegin = false;
        if(!ImGui::IsMouseDown(0)){
            mCurrentlyClicked = -1;
            if(dragBegin){
                //The scene tree drag ended.
                EditorGuiSceneTreeDrag e;
                e.dragType = DragEventType::Completed;
                EventDispatcher::transmitEvent(EventType::EditorGui, e);
            }
            dragBegin = false;
        }
        _drawSceneTreeDragIcons();

        const bool windowHovered = ImGui::IsWindowHovered();

        StateType currentState = EditorSingleton::getEditor()->getEditorState()->getCurrentState();
        const bool resourceDragging = currentState == StateType::ResourceDrag;
        const bool sceneTreeDragging = currentState == StateType::SceneTreeDrag;

        const float sz = ImGui::GetFrameHeight(); //Even though this is height it also represents the width of an arrow button.
        const float startX = ImGui::GetCursorPosX();
        static const float offsetAmount = 10.0f;
        static const ImVec2 hideButtonSize = ImVec2(14, 12);
        static bool firstRenameFrame = false;

        static Ogre::TexturePtr tex;
        static Ogre::TexturePtr iconTex;
        static bool first = true;
        if(first){
            first = false;
            if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "visibleIcon.png")){
                tex = Ogre::TextureManager::getSingleton().load("visibleIcon.png", SOUTHSEA_INTERNAL_GROUP_NAME);
            }
            if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "objectIcons.png")){
                iconTex = Ogre::TextureManager::getSingleton().load("objectIcons.png", SOUTHSEA_INTERNAL_GROUP_NAME);
            }
        }

        bool expandRequested = false;
        //Keeps track of whether any of the lines received input. This is used to detect an outside click.
        //0 means no click. 1, 2, are the respective left and right mouse buttons.
        int anyClickRegistered = 0;
        EntryId expandId;
        int entryPos;
        bool expand;
        bool recursiveExpand = false;
        const float scrollBarPaddingSize = style.ScrollbarSize * 2;

        mInsertItemIndex = -1;
        mInsertItemType = ObjectInsertionType::none;

        int childIndex = 0;
        //int entryIndex = 0;

        bool skippingThroughNextChild = false;

        if(mCurrentlyClicked > 0){
            if(ImGui::IsMouseDragging(0)){
                if(!dragBegin){
                    dragBegin = true; //Drag begin is most likely temporary while I sort things out.

                    std::cout << "Drag begin" << '\n';
                    EditorGuiSceneTreeDrag e;
                    e.dragType = DragEventType::Started;
                    EventDispatcher::transmitEvent(EventType::EditorGui, e);
                }
            }
        }

        if(emptyScene){
            if(resourceDragging){
                //I get a new hover value here, so as not to interfere with the previous hover information with these new flags.
                bool newHover = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem);
                //Draw a highlight around the window.
                drawList->AddRectFilled(canvasPos, ImVec2(canvasPos.x + regionSize.x, canvasPos.y + regionSize.y), newHover ? INSERT_COLOUR : HOVER_COLOUR);
                drawList->AddRectFilled(ImVec2(canvasPos.x + 5, canvasPos.y + 5), ImVec2(canvasPos.x + regionSize.x - 5, canvasPos.y + regionSize.y - 5), ImGui::ColorConvertFloat4ToU32(style.Colors[ImGuiCol_WindowBg]));
            }

            const char* emptySceneString = "Empty scene.";
            const ImVec2 emptySceneStringSize = ImGui::CalcTextSize(emptySceneString);

            ImGui::SetCursorPosY(30 + emptySceneStringSize.y);
            ImGui::SetCursorPosX((regionSize.x / 2) - emptySceneStringSize.x / 2 );
            ImGui::Text("%s", emptySceneString);
        }

        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0,0));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.75, 0.75, 0.75, 1));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.75, 0.75, 0.75, 1));
        ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));

        int totalSelectedEntries = 0;
        //for(const SceneEntry& entry : entries){
        for(int entryIndex = 0; entryIndex < entries.size(); entryIndex++){
            const SceneEntry& e = entries[entryIndex];

            bool hasChildren = sceneTreeManager->itemHasChildren(entries, entryIndex);
            bool entrySelected = sceneTreeManager->isEntrySelected(e.id);
            if(entrySelected) totalSelectedEntries++;

            if(skippingThroughNextChild){
                //This is the entry following the item which was not expanded. If it really did have a child then there should be a child terminator after it.
                assert(e.type == SceneEntryType::treeChild);

                //The index is one less so we get the terminator, not the entry after.
                entryIndex = sceneTreeManager->utilGetTerminatorForChild(entryIndex, entries) - 1;
                skippingThroughNextChild = false;
                //We've landed on the terminator value, so can just skip it.
                continue;
            }

            if(hasChildren && !e.expanded && entryIndex > 0){
                //That entry has a child, and is not expanded. In this case we need to skip past it.
                skippingThroughNextChild = true;
            }


            if(e.type != SceneEntryType::treeChild && e.type != SceneEntryType::treeTerminator){

                const float currentPosition = ImGui::GetCursorPosY() - ImGui::GetScrollY();
                if(currentPosition > hierarchyScrollHeight){
                    ImGui::Dummy(ImVec2(1, 18));
                    continue;
                }

                bool backgroundItemClicked = false;
                //TODO I only need to create the dummy entries when the window is hovered. That will make the system more efficient.
                {
                    //Create a dummy item to be able to check item hover.
                    const ImVec2 initialCursorPos = ImGui::GetCursorPos();
                    const ImVec2 dummyStartPos = ImVec2(initialCursorPos.x + windowPos.x, initialCursorPos.y + windowPos.y);
                    ImGui::Dummy(ImVec2(regionSize.x, sz));

                    bool hovered = ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem);
                    if(ImGui::IsItemClicked(0)){
                        backgroundItemClicked = true;
                        anyClickRegistered = 1;
                        mCurrentlyClicked = entryIndex;

                        if(ImGui::IsMouseDoubleClicked(0) && mTreeIndexCurrentlyRenaming < 0){
                            std::cout << "Rename " << entryIndex << '\n';
                            firstRenameFrame = true;
                            mTreeIndexCurrentlyRenaming = entryIndex;

                            strcpy(renamingItemString, e.name.c_str());
                        }
                    }
                    if(ImGui::IsItemClicked(1)){
                        backgroundItemClicked = true;
                        anyClickRegistered = 2;
                    }

                    if(entrySelected || hovered){
                        int yScroll = ImGui::GetScrollY();
                        drawList->AddRectFilled(ImVec2(dummyStartPos.x, dummyStartPos.y - yScroll + 28), ImVec2(dummyStartPos.x + regionSize.x, dummyStartPos.y + sz - yScroll + 28),
                            entrySelected ? SELECTED_COLOUR : !(resourceDragging || sceneTreeDragging) ? HOVER_COLOUR : INVISIBLE_COLOUR
                        );
                    }

                    if(hovered && (resourceDragging || sceneTreeDragging)){
                        //Draw the drag indicators.
                        static const float padding = 2.0f;
                        int yScroll = ImGui::GetScrollY();
                        const ImVec2 innerStart(dummyStartPos.x, dummyStartPos.y - yScroll + 28);
                        const ImVec2 innerEnd(dummyStartPos.x + regionSize.x, dummyStartPos.y + sz - yScroll + 28);

                        //Determine the highlight type relative to the mouse coordinates.
                        const ImVec2 relativeMousePos(io.MousePos.x - dummyStartPos.x, io.MousePos.y - dummyStartPos.y - 28 + yScroll);
                        ObjectInsertionType ObjectInsertionType = ObjectInsertionType::into;
                        if(relativeMousePos.y < sz * 0.4){
                            const float width = sz * 0.2;
                            const ImVec2 drawEnd(innerEnd.x, innerStart.y + width);
                            drawList->AddTriangleFilled(ImVec2(innerStart.x, innerStart.y - 4), ImVec2(innerStart.x + 12, innerStart.y + width / 2), ImVec2(innerStart.x, innerStart.y + width + 4), INSERT_COLOUR);
                            drawList->AddRectFilled(innerStart, drawEnd, INSERT_COLOUR);
                            ObjectInsertionType = ObjectInsertionType::above;
                        }
                        if(relativeMousePos.y > sz * 0.8){
                            const float width = sz * 0.2;
                            const ImVec2 drawEnd(innerEnd.x, innerStart.y + width);
                            const ImVec2 drawStart(innerStart.x, innerEnd.y - sz * 0.2);
                            drawList->AddTriangleFilled(ImVec2(drawStart.x, drawStart.y - 4), ImVec2(drawStart.x + 12, drawStart.y + width / 2), ImVec2(drawStart.x, drawStart.y + width + 4), INSERT_COLOUR);
                            drawList->AddRectFilled(drawStart, innerEnd, INSERT_COLOUR);
                            ObjectInsertionType = ObjectInsertionType::below;
                        }

                        if(ObjectInsertionType == ObjectInsertionType::into){
                            //None of the edges were selected, and as a hover happened that means this is just a centre insert.
                            drawList->AddRectFilled(innerStart, innerEnd, INSERT_COLOUR);
                        }

                        mInsertItemIndex = entryIndex;
                        mInsertItemType = ObjectInsertionType;
                    }
                    ImGui::SameLine();
                }

                bool renamingEntry = mTreeIndexCurrentlyRenaming == entryIndex;

                //As we have a tree child at the beginning of the list -1 offset so we start at 0.
                ImGui::SetCursorPosX(-offsetAmount + startX + offsetAmount * childIndex);

                if(hasChildren){
                    char arrowId[20];
                    sprintf(arrowId, "id%i", e.id);

                    if(ImGui::ArrowButton(arrowId, e.expanded ? ImGuiDir_Down : ImGuiDir_Right)){
                        if(io.KeyShift){
                            recursiveExpand = true;
                        }
                        expandRequested = true;
                        expandId = e.id;
                        entryPos = entryIndex;
                        expand = !e.expanded;
                    }
                    //Here I use the item clicked function rather than the return.
                    //This is because I need to nullify the dummy clicked if this was the case, and the return behaves differently.
                    _checkMouseButtons(anyClickRegistered, backgroundItemClicked);
                    ImGui::SameLine();
                }else{
                    ImGui::Dummy(ImVec2(sz, sz));
                    ImGui::SameLine();
                }

                {
                    ImVec2 uv0, uv1;
                    _getSceneTypeUVCoords(e.type, &uv0, &uv1);

                    ImGui::Dummy(ImVec2(6, 1));
                    ImGui::SameLine();
                    float cursorY = ImGui::GetCursorPosY();
                    ImGui::SetCursorPosY(cursorY + 3);
                    ImGui::Image(iconTex.getPointer(), hideButtonSize, uv0, uv1, entrySelected ? ImVec4(0.5, 0.5, 0.5, 1) : ImVec4(1, 1, 1, 1));
                    ImGui::SameLine();
                    ImGui::Dummy(ImVec2(6, 1));
                    ImGui::SameLine();
                    ImGui::SetCursorPosY(cursorY);
                }

                if(renamingEntry){
                    if(firstRenameFrame){
                        ImGui::SetKeyboardFocusHere();
                        firstRenameFrame = false;
                    }
                    if(ImGui::InputText("##renamingInput", renamingItemString, IM_ARRAYSIZE(renamingItemString), ImGuiInputTextFlags_EnterReturnsTrue)){
                        ObjectRenameAction *a = new ObjectRenameAction(e.id, e.name, std::string(renamingItemString));
                        a->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

                        _endRename(); //At a later point this would be confirm rename.
                    }
                    if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Escape))){
                        _endRename();
                    }

                    _checkMouseButtons(anyClickRegistered, backgroundItemClicked);
                }else{
                    if(entrySelected) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0, 0, 0, 1));
                    ImGui::Text("%s", e.name.c_str());
                    if(entrySelected) ImGui::PopStyleColor();
                }

                ImGui::SameLine();
                ImGui::SetCursorPosX(regionSize.x - hideButtonSize.x - scrollBarPaddingSize);
                const bool visible = e.visible;
                //ImGui::ImageButton(tex.getPointer(), hideButtonSize, visible ? ImVec2(0, 0) : ImVec2(0.5, 0), visible ? ImVec2(0.5, 1) : ImVec2(1, 1));
                //ImGui::ImageButton(tex.getPointer(), hideButtonSize, visible ? ImVec2(0, 0) : ImVec2(0.5, 0), visible ? ImVec2(0.5, 1) : ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), entrySelected ? ImVec4(0, 0, 0, 1) : ImVec4(1, 1, 1, 1));

                //For image buttons you have to do this. Bit weird.
                ImGui::PushID(entryIndex);
                if(ImGui::ImageButton(tex.getPointer(), hideButtonSize, visible ? ImVec2(0, 0) : ImVec2(0.1, 0), visible ? ImVec2(0.1, 1) : ImVec2(0.2, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(0.8, 0.8, 0.8, 1))){
                    ChangeVisibilityAction *a = new ChangeVisibilityAction(e.id, !visible);
                    a->performAction();
                    EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                }
                ImGui::PopID();
                _checkMouseButtons(anyClickRegistered, backgroundItemClicked);

                if(backgroundItemClicked){
                    //Set this item to be selected.
                    sceneTreeManager->notifySelection(e.id, entryIndex, io.KeyCtrl, io.KeyShift);
                    _endRename();
                }
            }

            if(e.type == SceneEntryType::treeChild){
                childIndex++;
            }
            if(e.type == SceneEntryType::treeTerminator && entryIndex <= entries.size()){
                assert(childIndex > 0);
                childIndex--;
            }

        }
        assert(childIndex == 0);

        ImGui::PopStyleVar(2);
        ImGui::PopStyleColor(3);
        ImGui::EndChild();

        {
            const char* buttonString = "a";
            const ImVec2 buttonStringSize = ImGui::CalcTextSize(buttonString);

            ImVec2 dummyStartPos = ImGui::GetCursorPos();
            ImGui::Dummy(ImVec2(1, 5));
            //Dummy to test collision and stuff.
            ImGui::Dummy(ImVec2(regionSize.x, buttonStringSize.y));
            bool hovered = ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem);
            if(ImGui::IsItemClicked(0)){
                sceneTreeManager->notifyTerrainSelected();
                anyClickRegistered = 1;
            }
            bool terrainSelected = sceneTreeManager->isTerrainSelected();

            ImGui::SetCursorPosY(dummyStartPos.y);
            ImVec2 targetPos = ImGui::GetCursorPos();
            ImGui::Separator();

            if(hovered || terrainSelected){
                //For some reason I can't seem to figure out the draw position doesn't start where I expect.
                //I've largely just determined sensible padding to these items to make it work properly.
                drawList->AddRectFilled(ImVec2(targetPos.x, targetPos.y + buttonStringSize.y * 1.5 + 4), ImVec2(targetPos.x + regionSize.x, targetPos.y + buttonStringSize.y * 3 + 4),
                    terrainSelected ? SELECTED_COLOUR : !(resourceDragging || sceneTreeDragging) ? HOVER_COLOUR : INVISIBLE_COLOUR
                );
            }

            ImGui::SetCursorPosY(targetPos.y + 3);

            ImGui::Dummy(ImVec2(1, 1));
            ImGui::Dummy(ImVec2(5, 1)); ImGui::SameLine();
            ImGui::Image(tex.getPointer(), hideButtonSize, ImVec2(0.2, 0), ImVec2(0.3, 1), terrainSelected ? ImVec4(0.5, 0.5, 0.5, 1) : ImVec4(1, 1, 1, 1));
            ImGui::SameLine();
            ImGui::Dummy(ImVec2(5, 1)); ImGui::SameLine();

            if(terrainSelected) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0, 0, 0, 1));
            ImGui::Text("Terrain");
            if(terrainSelected) ImGui::PopStyleColor();
        }

        if(expandRequested){
            sceneTreeManager->alterTreeItemExpansion(expandId, entryPos, expand, recursiveExpand);

            _endRename();
        }
        if(anyClickRegistered <= 0){
            if( (ImGui::IsMouseClicked(0) || ImGui::IsMouseClicked(1)) && windowHovered){
                //If none of the window dialogs received an input, but the mouse is down anyway that means a click outside any of the entries was registered.
                sceneTreeManager->clearAllSelection();

                totalSelectedEntries = 0;
                _endRename();
            }
        }
        if(!ImGui::IsMouseDown(0)){
            if( (mInsertItemIndex != -1 || emptyScene) && windowHovered ){
                if(resourceDragging){
                    std::cout << "Inserting resource" << '\n';

                    auto info = BaseSingleton::getDraggedResourceInfo();
                    auto fsEntry = info->getCurrentDraggedResourceEntry();
                    if(fsEntry.type == ResourceType::mesh){
                        ObjectInsertionAction *a = 0;
                        if(emptyScene){
                            a = new ObjectInsertionAction(ROOT_NODE, ObjectInsertionType::below);
                        }else{
                            a = new ObjectInsertionAction(entries[mInsertItemIndex].id, mInsertItemType);
                        }

                        a->populate(info->getCurrentDraggedResource(), fsEntry);
                        a->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                    }
                    else if(fsEntry.type == ResourceType::avScene){
                        ScenePasteAction* a = 0;
                        if(emptyScene){
                            a = new ScenePasteAction(ROOT_NODE, ObjectInsertionType::below);
                        }else{
                            a = new ScenePasteAction(entries[mInsertItemIndex].id, mInsertItemType);
                        }

                        //TODO replace this path.
                        SceneFileSceneTreeParserInterface interface(
                            sceneTreeManager, EditorSingleton::getEditor()->getOgreTreeManager(), EditorSingleton::getEditor()->getTreeObjectManager()
                        );
                        bool result = AV::AVSceneFileParser::loadFile(info->getFullPath(), &interface);
                        a->populateFromSceneFileInterface(interface);

                        a->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                    }

                }
                else if(sceneTreeDragging){
                    assert(!emptyScene);
                    TreeRearrangeAction *a = new TreeRearrangeAction();
                    bool result = sceneTreeManager->populateReArrangeEvent(*a, entries[mInsertItemIndex].id, mInsertItemType);
                    if(result){
                        a->performAction();
                        EditorSingleton::getEditor()->getActionStack()->pushAction(a);
                    }else delete a;
                }
            }
        }
        //If no click is registered, we want to check if the right mouse button was clicked on a blank part of the hierarcy.
        if(anyClickRegistered <= 0){
            if(windowHovered && ImGui::IsMouseClicked(1)){
                anyClickRegistered = 2;
            }
        }
        if(anyClickRegistered == 2){
            ImGui::OpenPopup("rightClickPopup");
        }

        _drawPopups(totalSelectedEntries);
    }

    void SceneHierarchyView::_endRename(){
        mTreeIndexCurrentlyRenaming = -1;
    }

    void SceneHierarchyView::_updateEmptyDrag(){

    }

    void SceneHierarchyView::_checkMouseButtons(int& anyClickRegistered, bool& backgroundItemClicked){
        bool left = ImGui::IsItemClicked(0);
        bool right = ImGui::IsItemClicked(1);
        if(left || right){
            //If its not 1 it must be 2.
            anyClickRegistered = left ? 1 : 2;
            //This is always used for the items in the front, so it should always be false.
            backgroundItemClicked = false;
        }
    }

    void SceneHierarchyView::_drawSceneTreeDragIcons(){
        if(EditorSingleton::getEditor()->getEditorState()->getCurrentState() != StateType::SceneTreeDrag) return;

        ImDrawList* draw_list = ImGui::GetForegroundDrawList();

        const ImVec2 mousePos = ImGui::GetMousePos();
        //At some point I'll dress this up with the actual icon of the current dragged item, but for now its fine.
        draw_list->AddRectFilled(mousePos, ImVec2(mousePos.x + 50, mousePos.y + 50), IM_COL32(150, 150, 150, 255));
    }

    enum class ObjectInsertType{
        null,
        meshResource,
        physicsShape,
        collisionSender,
        navMeshMarker,
        dataPoint
    };
    ObjectInsertionAction* objectInsertIntoTree(ObjectInsertType type){
        auto sceneTreeManager = EditorSingleton::getEditor()->getSceneTreeManager();
        const std::set<EntryId> selection = sceneTreeManager->getReducedSelection();
        EntryId targetId = 0;
        if(selection.empty()){
            targetId = 1; //Insert into the root node.
        }else{
            targetId = *(selection.begin()); //Just take the first selection for now.
        }
        const std::vector<SceneEntry>& entries = sceneTreeManager->getSceneTree();

        ObjectInsertionAction *a = new ObjectInsertionAction(targetId, targetId == 1 ? ObjectInsertionType::below : ObjectInsertionType::into);

        EditorSingleton::getEditor()->getActionStack()->pushAction(a);

        return a;
    }
    void objectInsertNull(){
        ObjectInsertionAction* a = objectInsertIntoTree(ObjectInsertType::null);
        a->populate();
        a->performAction();
    }
    void objectInsertPhysicsShape(PhysicsShapeType type){
        ObjectInsertionAction* a = objectInsertIntoTree(ObjectInsertType::physicsShape);
        a->populate(type);
        a->performAction();
    }

    void SceneHierarchyView::_drawPopups(int totalSelectedEntries){
        if(ImGui::BeginPopup("rightClickPopup")){
            if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Escape))) ImGui::CloseCurrentPopup();

            if(ImGui::BeginMenu("Add child")){
                if(ImGui::MenuItem("Empty")){
                    objectInsertNull();
                }
                ImGui::Separator();
                ImGui::MenuItem("Cube");
                ImGui::MenuItem("Sphere");
                ImGui::MenuItem("Triangle");
                ImGui::MenuItem("Capsule");
                ImGui::Separator();
                if(ImGui::MenuItem("Physics box")){
                    objectInsertPhysicsShape(PhysicsShapeType::box);
                }
                if(ImGui::MenuItem("Physics sphere")){
                    objectInsertPhysicsShape(PhysicsShapeType::sphere);
                }
                ImGui::Separator();
                if(ImGui::MenuItem("Collision sender")){
                    ObjectInsertionAction* a = objectInsertIntoTree(ObjectInsertType::collisionSender);
                    a->populateCollisionSender();
                    a->performAction();
                }
                ImGui::Separator();
                if(ImGui::MenuItem("Nav mesh marker")){
                    ObjectInsertionAction* a = objectInsertIntoTree(ObjectInsertType::navMeshMarker);
                    a->populateNavMeshMarker();
                    a->performAction();
                }
                ImGui::Separator();
                if(ImGui::MenuItem("Data point")){
                    ObjectInsertionAction* a = objectInsertIntoTree(ObjectInsertType::dataPoint);
                    a->populateDataPoint();
                    a->performAction();
                }
                ImGui::EndMenu();
            }
            ImGui::Separator();
            if(ImGui::MenuItem("Delete", NULL, false, totalSelectedEntries > 0)){
                EditorSingleton::getEditor()->getSceneTreeManager()->deleteCurrentSelection();
            }
            ImGui::Separator();
            if(ImGui::MenuItem("Cut", NULL, false, totalSelectedEntries > 0)){
                _performCut();
            }
            if(ImGui::MenuItem("Copy", NULL, false, totalSelectedEntries > 0)){
                EditorSingleton::getEditor()->getCopyStateManager()->copyCurrentSelection();
            }
            if(ImGui::MenuItem("Paste")){
                _performPaste();
            }
            if(ImGui::MenuItem("Duplicate")){
                _performDuplicate();
            }
            ImGui::Separator();
            if(ImGui::MenuItem("Save nodes to file")){
                _performSaveNodes();
            }

            ImGui::EndPopup();
        }
    }

    void SceneHierarchyView::_getSceneTypeUVCoords(SceneEntryType t, ImVec2* uv0, ImVec2* uv1){
        switch(t){
            case SceneEntryType::mesh:{
                *uv0 = {0.1, 0};
                *uv1 = {0.2, 1};
                break;
            }
            case SceneEntryType::physicsShape:{
                *uv0 = {0.2, 0};
                *uv1 = {0.3, 1};
                break;
            }
            case SceneEntryType::collisionSender:{
                *uv0 = {0.3, 0};
                *uv1 = {0.4, 1};
                break;
            }
            case SceneEntryType::dataPoint:
            case SceneEntryType::navMarker:{
                *uv0 = {0.4, 0};
                *uv1 = {0.5, 1};
                break;
            }
            default:{
                *uv0 = {0, 0};
                *uv1 = {0.1, 1};
                break;
            }
        }

    }

    bool SceneHierarchyView::commandEventReceiver(const Event& event){
        const CommandEvent& e = (const CommandEvent&)event;
        if(e.eventCategory() == CommandEventCategory::Selection){
            const CommandEventSelection& selectEvent = (const CommandEventSelection&)e;
            auto sceneTreeManager = EditorSingleton::getEditor()->getSceneTreeManager();
            if(selectEvent.t == CommandEventSelection::SelectionType::SelectAll){
                sceneTreeManager->selectAll();
            }else{
                sceneTreeManager->clearAllSelection();
            }
        }
        else if(e.eventCategory() == CommandEventCategory::Copy){
            EditorSingleton::getEditor()->getCopyStateManager()->copyCurrentSelection();
        }
        else if(e.eventCategory() == CommandEventCategory::Paste){
            _performPaste();
        }
        else if(e.eventCategory() == CommandEventCategory::Cut){
            _performCut();
        }
        else if(e.eventCategory() == CommandEventCategory::Duplicate){
            StateType currentState = EditorSingleton::getEditor()->getEditorState()->getCurrentState();
            if(currentState == StateType::Null){
                _performDuplicate();
            }
        }

        return false;
    }
}
