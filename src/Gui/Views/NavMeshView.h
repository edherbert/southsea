#pragma once

#include "imgui.h"
#include "Editor/Chunk/NavMesh/NavMeshData.h"

namespace Southsea{
    class Event;

    class NavMeshView{
    public:
        NavMeshView() = delete;
        ~NavMeshView() = delete;

        static void startup();

        static void drawNavMeshView(bool& windowOpen);

        static bool sceneEventReceiver(const Event& e);

    private:
        static void _drawCurrentMeshes();
        static void _drawSelectedMeshInfo(const NavMeshData& selectedMesh);


    };
}