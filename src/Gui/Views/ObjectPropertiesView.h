#pragma once

#include "imgui.h"
#include "ObjectPropertyWidgets/BasicCoordinatesWidget.h"
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{
    class Event;

    class ObjectPropertiesView{
    public:
        static void drawObjectPropertiesView(bool& windowOpen);

        static void startup();
        static void shutdown();

        static bool editorEventReceiver(const Event& event);

    private:

        static void _resetSelectionInfo();

        static void _changeBasicCoordinates(const bool (&out)[3], const CoordinatesWidgetInput& oldData);
        //Check if the provided id is currently being displayed by the properties view. If it is, re-determine all the data, as this function indicates that something has changed.
        static void _updateDataForId(EntryId id);
        static void _updateObjectInfo();
        static void _processObjectDeleted(EntryId id);
    };
}
