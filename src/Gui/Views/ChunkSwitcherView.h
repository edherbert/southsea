#pragma once

#include "imgui.h"

namespace Southsea{
    class Event;

    class ChunkSwitcherView{
    public:
        static void drawChunkSwitcherView(bool &windowOpen);

        static void startup();
        static void shutdown();

        static bool editorEventReceiver(const Event& event);

    private:
        static int chunkInputBox[2];

        static void _drawSwitcherGrid();
        static void _drawAxisLines(bool xAxis, ImDrawList* dl, ImVec2 canvasPos, ImVec2 canvasEndPos, int posAxis, int numSquaresAxis, int viewportStartAxis, int squareSize);
        static void _clampQuadCoords(ImVec2* coord, ImVec2 canvasStart, ImVec2 canvasEnd);


        static int xPos;
        static int yPos;

        static int zoomRate;
        static const int zoomMin = 10;
        static const int zoomMax = 90;
        static const int totalZoomRange = zoomMax - zoomMin;
        static int zoomVisibleCooldown;

        static int canvasWidth, canvasHeight;

        static void _drawZoomBar(ImVec2 canvasStart, ImVec2 canvasEnd);

        static void _centreOnChunk(int chunkX, int chunkY);

        static bool mouseGridMovementInProgress;
        static bool mouseButtonChunkSwitchToggle; //The user has to release the mouse button before another switch request can happen. This keeps track of that.
        static bool manualChunkSwitchRequested; //When a manual chunk coordinate is entered, this flag is set so that when the chunk request happens the editor knows to centre on that chunk.
    };
}
