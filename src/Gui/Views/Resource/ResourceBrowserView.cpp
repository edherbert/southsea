#include "ResourceBrowserView.h"

#include "ResourceSelectorScreen.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/EditorGuiEvent.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Logic/EditorState.h"

#include "System/BaseSingleton.h"
#include "System/Editor/DraggedResourceInfo.h"
#include "Editor/ui/SceneObjectPreviewer.h"

#include <iostream>

namespace Southsea{
    ResourceSelectorScreen* ResourceBrowserView::screen = 0;

    void ResourceBrowserView::drawResourceBrowserView(bool &windowOpen){

        if(!windowOpen) return;

        if(!ImGui::Begin("ResourceBrowser", &windowOpen, 0)){
            ImGui::End();
            return;
        }

        bool currentlyDragging = EditorSingleton::getEditor()->getEditorState()->getCurrentState() == StateType::ResourceDrag;

        ImGuiIO& io = ImGui::GetIO();
        if(io.KeysDown[io.KeyMap[ImGuiKey_Escape]] && currentlyDragging){
            //End the dragging state with an abort (means nothing gets dropped into anything).
            EditorGuiResourceDrag e;
            e.dragType = DragEventType::Aborted;
            EventDispatcher::transmitEvent(EventType::EditorGui, e);

            currentlyDragging = false;
            screen->setResourceDragging(false);
        }
        if(!io.MouseDown[0] && currentlyDragging){
            EditorGuiResourceDrag e;
            e.dragType = DragEventType::Completed;
            EventDispatcher::transmitEvent(EventType::EditorGui, e);

            currentlyDragging = false;
            screen->setResourceDragging(false);
        }
        if(!currentlyDragging && screen->wantsResourceDrag()){
            std::cout << "Dragging began" << '\n';

            EditorGuiResourceDrag e;

            const int draggedIndex = screen->getDraggedResourceIndex();
            Resource r = {
                screen->getDialogResourcesManager()->getIndexItemResPath(draggedIndex),
                screen->getDialogResourcesManager()->getCurrentDirectoryOgreGroupName()
            };

            const std::string fullPath = screen->getDialogResourcesManager()->getFullPathForFile(draggedIndex);

            e.draggedResource = &r;
            e.entry = &(screen->getDraggedResource());
            e.fullPath = fullPath;
            e.dragType = DragEventType::Started;
            EventDispatcher::transmitEvent(EventType::EditorGui, e);

            screen->setResourceDragging(true);
            screen->notifyDragRequestReceived(); //Sets wantsResourceDrag to false.
        }

        screen->drawResourceSelectorScreen();

        ImGui::End();

        if(currentlyDragging){
            bool preview = EditorSingleton::getEditor()->getSceneObjectPreviewer()->isPreviewingObject();
            if(!preview){
                _drawDraggedResourceIcon(screen);
            }
        }

    }

    void ResourceBrowserView::_drawDraggedResourceIcon(ResourceSelectorScreen* screen){
        ImGuiIO& io = ImGui::GetIO();
        const ImVec2 mousePos(io.MousePos.x, io.MousePos.y);

        ImDrawList* draw_list = ImGui::GetForegroundDrawList();

        const ResourceFSEntry& e = BaseSingleton::getDraggedResourceInfo()->getCurrentDraggedResourceEntry();
        screen->_drawResourceFileIcon(draw_list, mousePos, ImVec2(100, 100), true, false, 0, e, true);
    }

    void ResourceBrowserView::startup(){
        if(!screen){
            //The id here could be anything, but it's used to avoid conflicts with ids if there are other resourceSelectorScreen instances
            screen = new ResourceSelectorScreen(20);
            screen->allowResourceDragging(true);
        }
    }

    void ResourceBrowserView::shutdown(){
        delete screen;
        screen = 0;
    }
}
