#include "ResourceSelectorPopup.h"

#include "ResourceSelectorScreen.h"
#include "MaterialSelectorScreen.h"
#include "Gui/Views/ResourceButton.h"

#include "imgui.h"
#include "Gui/Views/DisabledButton.h"

#include <iostream>

namespace Southsea{
    bool ResourceSelectorPopup::popupVisible = false;
    bool ResourceSelectorPopup::makePopupAppear = false;
    ResourceScreen* ResourceSelectorPopup::mScreen = 0;
    ResourceSelectorScreen* ResourceSelectorPopup::mResourceScreen = 0;
    MaterialSelectorScreen* ResourceSelectorPopup::mMaterialScreen = 0;
    ResourceButtonData* ResourceSelectorPopup::mCurrentButtonData = 0;
    ResourceButtonData* ResourceSelectorPopup::mOldButtonData = 0;

    void ResourceSelectorPopup::updateResourceSelectorPopup(){
        _drawPopup();
    }

    bool ResourceSelectorPopup::requestPopupAppear(ResourceButtonData* data, ResourceButtonData* oldData){
        assert(data->state == ResButtonState::None);
        if(popupVisible){
            return false;
        }

        makePopupAppear = true;
        mCurrentButtonData = data;
        mCurrentButtonData->state = ResButtonState::Selecting;
        mOldButtonData = oldData;

        if(!mScreen){
            //TODO delete this.
            mResourceScreen = new ResourceSelectorScreen(10);
            mMaterialScreen = new MaterialSelectorScreen(11);
        }

        if(data->type == ResourceType::material){
            mScreen = mMaterialScreen;
        }else{
            mScreen = mResourceScreen;
        }

        return true;
    }

    void ResourceSelectorPopup::_closePopup(){
        assert(popupVisible);

        if(mCurrentButtonData->state == ResButtonState::Selecting){
            mCurrentButtonData->state = ResButtonState::None;
        }
        mCurrentButtonData = 0;
        mOldButtonData = 0;
        popupVisible = false;
    }

    void ResourceSelectorPopup::_performSelection(int selectionIndex){
        ResourceScreen::OutputResourceData data;
        mScreen->getCurrentSelectionData(data);

        std::cout << data.group << '\n';
        std::cout << data.displayName << '\n';

        _copyToOldData();
        mCurrentButtonData->res.name = data.name;
        mCurrentButtonData->res.group = data.group;
        mCurrentButtonData->displayName = data.displayName;
        mCurrentButtonData->state = ResButtonState::Dirty;
    }

    void ResourceSelectorPopup::_copyToOldData(){
        if(!mOldButtonData) return;

        mOldButtonData->res = mCurrentButtonData->res;
        mOldButtonData->displayName = mCurrentButtonData->displayName;
    }

    void ResourceSelectorPopup::_drawPopup(){
        static const char* popupTitle = "Select a resource";

        if(makePopupAppear){
            mScreen->resetState();
            assert(mCurrentButtonData);
            mScreen->setFilterType(mCurrentButtonData->type);

            ImGui::OpenPopup(popupTitle);
            makePopupAppear = false;

            //Called here so the popup can be correctly resized.
            ImGui::SetNextWindowSize(ImVec2(750, 750));
        }

        bool p_open = true;
        if(ImGui::BeginPopupModal(popupTitle, &p_open, 0)){
            popupVisible = true;

            ImVec2 availableSize = ImGui::GetContentRegionAvail();
            //I'm not sure if there's any more intelligent way to do this than a hard coded value.
            availableSize = ImVec2(availableSize.x, availableSize.y - 35);
            if(ImGui::BeginChild("PopupScrollWindow", availableSize, false, 0)){
                mScreen->drawResourceSelectorScreen();
            }
            ImGui::EndChild();

            ImGui::Separator();

            if(ImGui::Button("Cancel")){
                ImGui::CloseCurrentPopup();
            }

            ImGui::SameLine();

            int selectedIndex = mScreen->getCurrentSelection();
            if(DisabledButton("Select", selectedIndex >= 0)
                || mScreen->getCurrentSelectionConfirmed()){
                assert(selectedIndex >= 0);
                _performSelection(selectedIndex);
                ImGui::CloseCurrentPopup();
            }

            static const ImVec2 clearSelectionButtonSize(150, 0);
            ImGui::SameLine();
            ImGui::SetCursorPosX(availableSize.x - clearSelectionButtonSize.x);

            if(ImGui::Button("Clear Selection", clearSelectionButtonSize)){
                _copyToOldData();
                mCurrentButtonData->clear();
                mCurrentButtonData->state = ResButtonState::Dirty;
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }else{
            if(popupVisible){
                _closePopup();
            }
            popupVisible = false;
        }
    }
}
