#pragma once

#include <string>
#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{
    class ResourceScreen{
    public:
        ResourceScreen(int selectorId);
        ~ResourceScreen();

        virtual void drawResourceSelectorScreen();
        virtual void resetState();

        virtual void setFilterType(ResourceType t);

        struct OutputResourceData{
            std::string name;
            std::string group;
            std::string displayName;
        };
        virtual void getCurrentSelectionData(OutputResourceData& data);

    protected:
        //Checked by the selector popup. If this ever becomes true it means an action like a double click took place.
        bool mCurrentSelectionConfirmed = false;

        int mCurrentSelection = -1;

        ResourceType mFilterType = ResourceType::unknown;

    public:
        bool getCurrentSelectionConfirmed() const { return mCurrentSelectionConfirmed; }

        int getCurrentSelection() const { return mCurrentSelection; }
    };
}
