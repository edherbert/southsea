#include "DialogResourcesManager.h"

#include "Editor/Resources/ResourcePathUtils.h"

#ifndef _WIN32
    #include <sys/types.h>
    #include <dirent.h>
#else
    #include <windows.h>
    #include <tchar.h>
    #include <stdio.h>
    #include <strsafe.h>
#endif

#include "filesystem/path.h"
#include <iostream>
#include <cassert>

#include "Editor/EditorSingleton.h"
#include "Editor/Resources/ResourceManager.h"
#include "Editor/Resources/OgreResourcesManager.h"

namespace Southsea{

    DialogResourcesManager::DialogResourcesManager(){

    }

    DialogResourcesManager::~DialogResourcesManager(){

    }

    ResourceType DialogResourcesManager::_determineFileType(const std::string& fileName) const{
        size_t pos = fileName.find_last_of(".");
        if (pos == std::string::npos) return ResourceType::unknown;

        const std::string extension = fileName.substr(pos+1);

        if(extension == "nut") return ResourceType::script;
        if(extension == "mesh") return ResourceType::mesh;

        if(extension == "jpg" || extension == "png" || extension == "jpeg") return ResourceType::texture;

        if(extension == "cfg") return ResourceType::textFile;

        if(extension == "avscene") return ResourceType::avScene;

        return ResourceType::unknown;
    }

    bool DialogResourcesManager::_resourceTypeOgre(ResourceType t) const{
        switch(t){
            case ResourceType::mesh:
            case ResourceType::texture:
                return true;
        };

        return false;
    }

    void DialogResourcesManager::moveCurrentDirectory(const std::string& directoryName){
        const filesystem::path currentPath(mCurrentDirectory);
        filesystem::path derivedPath = currentPath / filesystem::path(directoryName);

        if(!derivedPath.exists()) return;

        if(directoryName == ".."){
            derivedPath = currentPath.parent_path();
        }

        setCurrentDirectory(derivedPath.str());
    }

    void DialogResourcesManager::_scanDirectory(const std::string& path, std::vector<ScannedPathResult>& outVec){
        #ifndef _WIN32
            DIR* dirp = opendir(path.c_str());
            struct dirent * dp;
            while ((dp = readdir(dirp)) != NULL) {
                std::string d(dp->d_name);
                if(d == "." || d == "..") continue;

                outVec.push_back({d, dp->d_type == DT_DIR});

            }
            closedir(dirp);
        #else
            //Windows stuff.
            WIN32_FIND_DATA ffd;
            TCHAR szDir[MAX_PATH];
            HANDLE hFind = INVALID_HANDLE_VALUE;

            StringCchCopy(szDir, MAX_PATH, (path + "/*").c_str());
            hFind = FindFirstFile(szDir, &ffd);

           do
           {
              if(ffd.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY){
                const std::string dir(ffd.cFileName);
                if (dir == "." || dir == "..") continue;

                outVec.push_back({dir, true});
             }else{
                const std::string dir(ffd.cFileName);

                outVec.push_back({dir, false});
             }
           }
           while(FindNextFile(hFind, &ffd) != 0);
        #endif
    }

    bool DialogResourcesManager::setCurrentDirectory(const std::string& path){
        const filesystem::path plainPath(path);
        if(!plainPath.exists()) return false;

        Editor* editor = EditorSingleton::getEditor();

        mCurrentDirectoryList.clear();
        const filesystem::path dataPath(mDataDirectory);
        const filesystem::path pathP = plainPath.make_absolute(); //If there are any .. characters in the string, remove them.

        const std::string resolvedPath = pathP.str();

        std::cout << resolvedPath << '\n';

        if(dataPath != pathP){ //We shouldn't be able to move above the data directory.
            mCurrentDirectoryList.push_back({true, {"..", ResourceType::directory, false}}); //Each directory is guaranteed to have one of these.
        }

        {
            std::vector<ScannedPathResult> containerVec;
            _scanDirectory(resolvedPath, containerVec);

            for(const ScannedPathResult& s : containerVec){

                ResourceType type = ResourceType::unknown;
                if(s.second){
                    //Now determine if this new path is a resource location.
                    filesystem::path p = pathP / filesystem::path(s.first);
                    p = p.make_absolute();

                    type = editor->getResourceManager()->getOgreResourcesManager()->isPathResourceLocation(p.str()) ? ResourceType::resourcesDirectory : ResourceType::directory;
                }else{
                    //If this is a file, determine what type of file this is.
                    type = _determineFileType(s.first);
                }

                mCurrentDirectoryList.push_back({true, {s.first, type, _resourceTypeOgre(type)}});
            }

        }


        mCurrentDirectory = resolvedPath;
        mPathChangeOccured = true;

        {
            ResourcePathUtils::formatPath(mCurrentDirectory, mFormattedCurrentDirectory);
        }

        _determineSwitcherList(mFormattedCurrentDirectory);

        mCurrentDirectoryOgreGroup = editor->getResourceManager()->getOgreResourcesManager()->isPathResourceLocation(resolvedPath, &mCurrentOgreGroup);

        editor->getMapMetaFile()->setResourceManagerCurrentDirectory(mCurrentDirectory);

        _processSearchTerms();

        return true;
    }

    const std::string DialogResourcesManager::getFullPathForFile(int draggedIndex) const{
        const filesystem::path currentPath(mCurrentDirectory);
        filesystem::path derivedPath = currentPath / filesystem::path(
            mCurrentDirectoryList[draggedIndex].r.name
        );

        //Can happen if you have an invalid symlink, the make_absolute function doesn't like it.
        if(!derivedPath.exists()){
            return "";
        }

        return derivedPath.make_absolute().str();
    }

    std::string DialogResourcesManager::getIndexItemResPath(int index) const{
        const filesystem::path currentPath(mCurrentDirectory);
        filesystem::path derivedPath = currentPath / filesystem::path(
            mCurrentDirectoryList[index].r.name
        );

        std::string outPath;
        ResourcePathUtils::formatPath(derivedPath.str(), outPath);

        return outPath;
    }

    std::string DialogResourcesManager::getIndexItemName(int index) const{
        return mCurrentDirectoryList[index].r.name;
    }

    bool DialogResourcesManager::requestDirectoryChange(const std::string& requestedDirectory){
        std::string outStr;
        bool success = ResourcePathUtils::parsePath(requestedDirectory, outStr);
        if(!success) return false;

        const filesystem::path p(outStr);
        if(!p.exists()) return false;

        setCurrentDirectory(outStr);

        return true;
    }

    DialogResourcesManager::PathChangeResult DialogResourcesManager::getPathChangeOccured(){
        bool retChange = mPathChangeOccured;
        mPathChangeOccured = false;

        return {mFormattedCurrentDirectory, retChange};
    }

    void DialogResourcesManager::_determineSwitcherList(const std::string& newPath){
        mDirectoryList.clear();

        assert(newPath.rfind(ResourcePathUtils::ResHeader) == 0); //Expect to see this at the start.

        std::string removedPath;
        ResourcePathUtils::removeHeader(newPath, removedPath);

        ExposedPath p(removedPath);
        mDirectoryList.push_back(ResourcePathUtils::ResHeader);

        for(const std::string& i : p.getPathsVector()){
            if(!i.empty()){
                mDirectoryList.push_back(i);
            }
        }
    }

    void DialogResourcesManager::switchDirectory(int index){
        assert(mDirectoryList.size() > index);

        if(index == 0){
            setCurrentDirectory(mDataDirectory);
        }else{
            std::string newPath = mDataDirectory;
            //Reconstruct a path.
            for(int i = 1; i <= index; i++){ //Start at 1 as the first value will always be res://
                newPath += "/" + mDirectoryList[i];
            }
            setCurrentDirectory(newPath);
        }
    }

    void DialogResourcesManager::initialise(const std::string& dataDirectory){
        mDataDirectory = dataDirectory;
    }

    void DialogResourcesManager::setSearchTerm(const std::string& search){
        mCurrentSearchTerm = search;

        std::cout << "Setting new search term" << std::endl;
        _processSearchTerms();
    }

    void DialogResourcesManager::_processSearchTerms(){
        //Go through the current list and resolve down to strings which contain the combination.
        for(WrappedResourceFSEntry& e : mCurrentDirectoryList){
            e.visibleInSearch = (e.r.name.find(mCurrentSearchTerm) != e.r.name.npos);
        }
    }
}
