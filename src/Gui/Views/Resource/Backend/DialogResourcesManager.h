#pragma once

#include <string>
#include <vector>
#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{

    /**
    ResourceFSEntries containing extra information relating to search and visibility.
    */
    struct WrappedResourceFSEntry{
        bool visibleInSearch;

        ResourceFSEntry r;
    };

    /**
    A class to cache relevant dialog information for the imgui sections.
    As imgui dialogs are re-constructed each frame, it makes sense to store what needs to be displayed for quick and efficient access.
    */
    class DialogResourcesManager{
    public:

        typedef std::pair<const std::string&, bool> PathChangeResult;
        typedef std::pair<std::string, bool> ScannedPathResult;
    public:
        DialogResourcesManager();
        ~DialogResourcesManager();

        void initialise(const std::string& dataDirectory);

        bool setCurrentDirectory(const std::string& path);

        /**
        Move the current directory by a directory name.
        */
        void moveCurrentDirectory(const std::string& directoryName);

        /**
        Switch to a directory as specified by the directory switcher list.
        */
        void switchDirectory(int index);

        /**
        Set the search term which will be used to filter resources.
        */
        void setSearchTerm(const std::string& search);

        /**
        Request a switch to a directory path.
        */
        bool requestDirectoryChange(const std::string& requestedDirectory);

        PathChangeResult getPathChangeOccured();

        const std::vector<WrappedResourceFSEntry>& getCurrentDirectoryList() const { return mCurrentDirectoryList; }
        const std::vector<std::string>& getDirectoryList() const { return mDirectoryList; }

        //Returns true if the current directory is an ogre resource location.
        bool getCurrentDirectoryOgreGroup() const { return mCurrentDirectoryOgreGroup; }
        const std::string& getCurrentDirectoryOgreGroupName() const { return mCurrentOgreGroup; }
        const std::string& getDataDirectory() const { return mDataDirectory; }

        const std::string getFullPathForFile(int draggedIndex) const;

        std::string getIndexItemResPath(int index) const;
        std::string getIndexItemName(int index) const;

    private:
        std::vector<WrappedResourceFSEntry> mCurrentDirectoryList;
        std::vector<std::string> mDirectoryList;

        std::string mCurrentDirectory;
        std::string mFormattedCurrentDirectory;
        std::string mDataDirectory;
        std::string mCurrentOgreGroup;

        std::string mCurrentSearchTerm;

        bool mPathChangeOccured;
        bool mCurrentDirectoryOgreGroup;

        ResourceType _determineFileType(const std::string& fileName) const;
        bool _resourceTypeOgre(ResourceType t) const;

        void _determineSwitcherList(const std::string& newPath);
        void _processSearchTerms();

        void _scanDirectory(const std::string& path, std::vector<ScannedPathResult>& outVec);
    };
}
