#pragma once

#include <vector>
#include <string>

namespace Southsea{
    /**
    Data describing determined datablock information.
    */
    struct DisplayDatablockData{
        std::string blockName;
        bool visibleInSearch;
    };

    class DialogMaterialManager{
    public:
        DialogMaterialManager();
        ~DialogMaterialManager();

        void scanForDatablocks();

        void setSearchTerm(const std::string& search);

    private:
        std::vector<DisplayDatablockData> mPbsBlocks;
        std::vector<DisplayDatablockData> mUnlitBlocks;

        std::string mCurrentSearchTerm;

    public:
        const std::vector<DisplayDatablockData>& getPbsBlocks() const { return mPbsBlocks; };
        const std::vector<DisplayDatablockData>& getUnlitBlocks() const { return mUnlitBlocks; };
    };
}
