#include "DialogMaterialManager.h"

#include "OgreRoot.h"
#include "OgreHlmsManager.h"
#include "OgreHlms.h"

#include <iostream>

namespace Southsea{
    static const int NUM_TYPES = 2;

    DialogMaterialManager::DialogMaterialManager(){

    }

    DialogMaterialManager::~DialogMaterialManager(){

    }

    void DialogMaterialManager::scanForDatablocks(){
        Ogre::HlmsManager *hlmsManager = Ogre::Root::getSingleton().getHlmsManager();

        Ogre::HlmsTypes types[NUM_TYPES] = {Ogre::HLMS_PBS, Ogre::HLMS_UNLIT};


        for(int i = 0; i < NUM_TYPES; i++){
            Ogre::Hlms* targetHlms = hlmsManager->getHlms(types[i]);
            static std::vector<DisplayDatablockData>* const TARGETS[NUM_TYPES] = {&mPbsBlocks, &mUnlitBlocks};

            std::vector<DisplayDatablockData>* targetList = TARGETS[i];

            const std::map<Ogre::IdString, Ogre::Hlms::DatablockEntry>& datablockMap = targetHlms->getDatablockMap();
            for(auto it = datablockMap.begin(); it != datablockMap.end(); it++){
                const Ogre::Hlms::DatablockEntry e = it->second;
                if(!e.visibleToManager) continue;

                targetList->push_back({e.name, true});
            }
        }

    }

    void DialogMaterialManager::setSearchTerm(const std::string& search){
        mCurrentSearchTerm = search;

        std::cout << "Setting new material search term" << std::endl;
        static std::vector<DisplayDatablockData>* const TARGETS[NUM_TYPES] = {&mPbsBlocks, &mUnlitBlocks};

        for(int i = 0; i < NUM_TYPES; i++){
            for(DisplayDatablockData& e : *TARGETS[i]){
                e.visibleInSearch = (e.blockName.find(mCurrentSearchTerm) != e.blockName.npos);
            }
        }
    }

}
