#pragma once

namespace Southsea{
    class ResourceScreen;
    class ResourceSelectorScreen;
    class MaterialSelectorScreen;

    struct ResourceButtonData;

    class ResourceSelectorPopup{
    public:
        ResourceSelectorPopup() = delete;
        ~ResourceSelectorPopup() = delete;

        static void updateResourceSelectorPopup();

        /**
        Request to display the resource selector popup.
        This is only a request, as if the popup is already displayed the request will be ignored.

        @return
        Whether or not the popup was triggered by this request.
        */
        static bool requestPopupAppear(ResourceButtonData* data, ResourceButtonData* oldData);

    private:
        static void _drawPopup();
        static void _closePopup();

        static void _performSelection(int selectionIndex);
        static void _copyToOldData();

        static bool popupVisible;
        static bool makePopupAppear;

        static ResourceScreen* mScreen;
        static ResourceSelectorScreen* mResourceScreen;
        static MaterialSelectorScreen* mMaterialScreen;

        static ResourceButtonData* mCurrentButtonData;
        static ResourceButtonData* mOldButtonData;
    };
}
