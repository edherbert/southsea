#include "ResourceSelectorScreen.h"

#include "Editor/EditorSingleton.h"

#include "Editor/Resources/ResourceManager.h"
#include "Editor/Resources/OgreResourcesManager.h"

#include "Gui/Views/ResourceButton.h"
#include "Editor/Logic/EditorState.h"

#include "OgreTextureManager.h"
#include "Platforms/OgreSetup/SouthseaResourceGroups.h"

#include <iostream>
#include <cmath>

namespace Southsea{

    const ImU32 ResourceSelectorScreen::GREEN_COLOUR = IM_COL32(47, 128, 69, 255);

    ResourceSelectorScreen::ResourceSelectorScreen(int selectorId) : ResourceScreen(selectorId){
        mDialogResourcesManager = new DialogResourcesManager();

        sprintf(pathSelectorWindowS, "PathSelecWin##%i", selectorId);

        const AVSetupFile& file = EditorSingleton::getEditor()->getEditorProject().getAVSetupFile();
        const std::string& dataDirectory = file.getDataDirectory();

        mDialogResourcesManager->initialise(dataDirectory);


        setDefaultResourcesDirectory();
    }

    ResourceSelectorScreen::~ResourceSelectorScreen(){
        delete mDialogResourcesManager;
    }

    void ResourceSelectorScreen::getCurrentSelectionData(OutputResourceData& data){
        data.group = mDialogResourcesManager->getCurrentDirectoryOgreGroupName();

        data.displayName = mDialogResourcesManager->getIndexItemName(mCurrentSelection);
        data.name = mDialogResourcesManager->getIndexItemResPath(mCurrentSelection);
    }

    void ResourceSelectorScreen::setDefaultResourcesDirectory(){
        //Try and set the current directory for the resource browser.
        //Some sanity checks need to be performed on the values returned from the map meta file.
        bool dirSetSuccess = false;

        const std::string& determinedDir = EditorSingleton::getEditor()->getMapMetaFile()->getResourceManagerCurrentDirectory();
        if(determinedDir.size() > 0){
            //This might return false if the path is invalid!
            dirSetSuccess = mDialogResourcesManager->setCurrentDirectory(determinedDir);
        }

        if(determinedDir.size() < 0 || !dirSetSuccess){
            mDialogResourcesManager->setCurrentDirectory(mDialogResourcesManager->getDataDirectory());
        }
    }

    void ResourceSelectorScreen::drawResourceSelectorScreen(){

        _drawDirectorySwitcher();

        const ImVec2 windowSize = ImGui::GetContentRegionAvail();
        ImGui::PushItemWidth(windowSize.x);

        _drawPathTextBox(windowSize.x);

        ImGui::Separator();

        if(ImGui::InputTextWithHint("##Search", "Search", searchS, 64)){
            mDialogResourcesManager->setSearchTerm(searchS);
        }

        ImGui::PopItemWidth();

        StateType currentState = EditorSingleton::getEditor()->getEditorState()->getCurrentState();
        bool drawHover = currentState != StateType::FPSCameraController;
        _drawFileSelector(drawHover);

        if(!mTargetChange.empty()){
            mDialogResourcesManager->moveCurrentDirectory(mTargetChange);
            mTargetChange.clear();
        }
    }

    void ResourceSelectorScreen::resetState(){
        mCurrentSelection = -1;
        mCurrentSelectionConfirmed = false;
    }

    void ResourceSelectorScreen::_drawPathTextBox(float boxWidth){
        DialogResourcesManager::PathChangeResult res = mDialogResourcesManager->getPathChangeOccured();
        if(res.second){
            //TODO I should have a dedicated function for when something changes.
            resetState();
            strcpy(pathS, res.first.c_str());
        }

        const ImVec2 inputPos = ImGui::GetCursorScreenPos();

        if(ImGui::InputText("##pathInput", pathS, IM_ARRAYSIZE(pathS), ImGuiInputTextFlags_EnterReturnsTrue)){
            mOgreGroupsSelectorVisible = false;
            mDialogResourcesManager->requestDirectoryChange(std::string(pathS));
        }
        if(ImGui::IsItemActive()){
            mOgreGroupsSelectorVisible = true;
        }

        if(mOgreGroupsSelectorVisible){ //draw the path selection string.

            const ImGuiStyle& style = ImGui::GetStyle();

            ImGui::SetNextWindowSize(ImVec2(boxWidth + style.WindowPadding.x * 2, 200));
            ImGui::SetNextWindowPos(ImVec2(inputPos.x - style.WindowPadding.x, inputPos.y - style.WindowPadding.y));

            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);

            bool selectorShouldClose = false;
            bool otherWindowOpen = true;
            //Keep track of which window has focus (the sub window is the scroll window). If either do then keep the selector visible.
            bool subHasFocus = false;
            bool parentHasFocus = false;

            ImGuiIO& io = ImGui::GetIO();
            if(io.KeysDown[io.KeyMap[ImGuiKey_Escape]]){
                selectorShouldClose = true;
            }

            if(!ImGui::Begin(pathSelectorWindowS, &otherWindowOpen, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize)){
                ImGui::End();
                mOgreGroupsSelectorVisible = false;
                ImGui::PopStyleVar();
                return;
            }

            {
                ImGui::PushItemWidth(boxWidth);
                //I have another box here as the old one will get covered up when the edit starts.
                if(ImGui::InputText("##pathInput", pathS, IM_ARRAYSIZE(pathS), ImGuiInputTextFlags_EnterReturnsTrue)){
                    selectorShouldClose = true;
                    mDialogResourcesManager->requestDirectoryChange(std::string(pathS));
                }
                ImGui::PopItemWidth();

                ImGui::Separator();


                const ImVec2 availableSize = ImGui::GetContentRegionAvail();
                const OgreResourcesManager::ResourceLocationContainer& resources = EditorSingleton::getEditor()->getResourceManager()->getOgreResourcesManager()->getOgreResources();

                if(ImGui::BeginChild("GroupScrollWindow", availableSize, false, 0)){
                    int c = 0;
                    for(const auto& i : resources){
                        ImGui::TextColored(ImVec4(0.5, 0.5, 0.5, 1), "%s", i.first.c_str());
                        ImGui::Separator();

                        bool is_selected = false;
                        for(const auto& y : i.second){
                            char label[128];
                            const std::string& targetStr = y.second;
                            sprintf(label, "%s##%i", targetStr.c_str(), c);
                            if(ImGui::Selectable(label, is_selected)){
                                selectorShouldClose = true;
                                mMousePressed = true; //Just so that the icons below aren't accidentally clicked during the popup disappear.

                                mDialogResourcesManager->requestDirectoryChange(targetStr);
                            }
                            c++;
                        }
                    }
                    subHasFocus = ImGui::IsWindowFocused();
                }
                ImGui::EndChild();

            }

            parentHasFocus = ImGui::IsWindowFocused();

            //If neither the window or the sub scroll window have focus then close the selector window.
            mOgreGroupsSelectorVisible = (subHasFocus || parentHasFocus) && !selectorShouldClose;

            ImGui::End();

            ImGui::PopStyleVar();
        }

    }

    void ResourceSelectorScreen::_drawDirectorySwitcher(){

        const std::vector<std::string>& vec = mDialogResourcesManager->getDirectoryList();

        for(int i = 0; i < vec.size(); i++){
            const std::string& s = vec[i];

            if(i > 0) ImGui::SameLine();

            char label[128];
            sprintf(label, "%s##%i", s.c_str(), i);
            if(ImGui::Button(label)){
                mDialogResourcesManager->switchDirectory(i);
            }
        }

    }

    void ResourceSelectorScreen::_drawFileSelector(bool allowMouseSelection){
        ImGuiIO& io = ImGui::GetIO();
        const std::vector<WrappedResourceFSEntry>& resVector = mDialogResourcesManager->getCurrentDirectoryList();
        if(!io.MouseDown[0]) mMousePressed = false;

        if(mDoubleClickTimer <= 0) mPotentialDoubleClick = false;
        if(mDoubleClickTimer > 0) mDoubleClickTimer--;


        bool resLoc = mDialogResourcesManager->getCurrentDirectoryOgreGroup();
        const ImVec2 canvasSize = ImGui::GetContentRegionAvail();
        const ImVec2 canvas_pos = ImGui::GetCursorScreenPos();

        { //Draw the background box.
            static const float padding = 2.0f;

            ImDrawList* draw_list = ImGui::GetWindowDrawList();

            ImVec2 recStart(canvas_pos.x, canvas_pos.y);
            ImVec2 recEnd(canvas_pos.x + canvasSize.x, canvas_pos.y + canvasSize.y);

            if(resLoc){
                draw_list->AddRectFilled(recStart, recEnd, GREEN_COLOUR);

                recStart = ImVec2(recStart.x + padding, recStart.y + padding);
                recEnd = ImVec2(recEnd.x - padding, recEnd.y - padding);
            }

            draw_list->AddRectFilled(recStart, recEnd, IM_COL32(50, 50, 50, 255));

            if(resLoc){
                static const char* label = "Resource Location";
                ImVec2 textSize = ImGui::CalcTextSize(label);
                ImVec2 textPos(recEnd.x - textSize.x, recEnd.y - textSize.y);

                draw_list->AddRectFilled(textPos, recEnd, GREEN_COLOUR);
                draw_list->AddText(textPos, IM_COL32_BLACK, label);
            }
        }

        static const ImVec2 fileSize(200, 200);
        static const int padding = 5;
        static const ImVec2 fileSizeWithPadding(fileSize.x + padding, fileSize.y + padding);

        //TODO have a prevResourceType, and compare what the current resource was.
        //Or have a boolean flag for when a resouce is set.
        //This way I would be able to tell when the filter type changes and the nums need to be changed.
        //I guess I'd also have to keep track of the number of files previously.
        int numFiles = 0;
        if(mFilterType == ResourceType::unknown){
            for(const WrappedResourceFSEntry& e : resVector){
                if(e.visibleInSearch) numFiles++;
            }
        }else{
            //Determine how many files are needed.
            for(const WrappedResourceFSEntry& e : resVector){
                if(e.r.type == mFilterType || e.r.type == ResourceType::directory || e.r.type == ResourceType::resourcesDirectory){
                    if(e.visibleInSearch) numFiles++;
                }
            }
        }


        int maxFilesWidth = (canvasSize.x - 5) / fileSizeWithPadding.x; //-5 for the scroll bar.
        maxFilesWidth = maxFilesWidth ? maxFilesWidth : 1; //If maxFilesWidth is 0 just make it 1.
        int numFilesHeight = (numFiles / maxFilesWidth) + 1;



        ImGui::BeginChild("ScrollWindow", canvasSize, false, 0);

        ImDrawList* draw_list = ImGui::GetWindowDrawList();

        ImVec2 dummySpacer(10, fileSizeWithPadding.y * numFilesHeight ); //A simple spacer to cause the scroll bar to appear.
        ImGui::Dummy(dummySpacer);

        //End of actual items in this window.

        float currScroll = ImGui::GetScrollY();
        float scrollMax = ImGui::GetScrollMaxY();

        float totalScroll = (!currScroll && !scrollMax) ? 0 : currScroll / scrollMax;
        float totalScrollOffset = totalScroll * ( (numFilesHeight - 1) * fileSizeWithPadding.y);


        int hoverboxX, hoverboxY;
        hoverboxX = -1;
        hoverboxY = -1;

        const ImVec2 mousePos(io.MousePos.x - canvas_pos.x, io.MousePos.y - canvas_pos.y);
        if(mousePos.x >= 0 && mousePos.y >= 0 && mousePos.x < canvasSize.x && mousePos.y < canvasSize.y){

            hoverboxX = mousePos.x / fileSizeWithPadding.x;
            hoverboxY = (mousePos.y + totalScrollOffset) / fileSizeWithPadding.y;

            //Originally checks for the padding space between the file icons. I couldn't get it to work but might re-implement that if it ever starts annoying me.


            // bool valid = coordX < maxFilesWidth && coordY < numFilesHeight; //The division method might return some blank areas of the dialog selector, so these need to be checked.
            //
            // //Now we know which chunk is hovered, check the box is within the padding boundaries.
            // //if(mousePos.x >= 0 && mousePos.y >= 0 && mousePos.x < canvasSize.x && mousePos.y < canvasSize.y){
            //
            // hoverboxX = coordX;
            // hoverboxY = coordY;
        }

        if(!mMousePressed){
            mStartedDragX = -1;
            mStartedDragY = -1;

            mStartedMouseDragX = 0.0f;
            mStartedMouseDragY = 0.0f;
        }
        bool dragMousePressed = false;
        if(!mResourceDragging && !mOgreGroupsSelectorVisible && mAllowResourceDrag){
            if(hoverboxX >= 0 && hoverboxY >= 0){
                if(io.MouseDown[0]){
                    if(mStartedDragX < 0 && mStartedDragY < 0 && !mMousePressed){
                        mStartedDragX = hoverboxX;
                        mStartedDragY = hoverboxY;

                        mStartedMouseDragX = io.MousePos.x;
                        mStartedMouseDragY = io.MousePos.y;
                        dragMousePressed = true;
                    }else{
                        //Find the distance between the two points to check if a drag has occured.
                        float distance = sqrt(pow(mStartedMouseDragY - io.MousePos.y, 2) + pow(mStartedMouseDragX - io.MousePos.x, 2));
                        //std::cout << distance << '\n';
                        if(distance >= 30.0f){
                            mWantsDragBegin = true;
                        }
                    }
                }
            }
        }

        //Used to keep track of whether what was decided as a drag tile is actually valid.
        //Later during the while loop some entries are skipped over, and if the drag tile was skipped over (meaning it's a void space) then nothing can be dragged.
        bool dragFoundMatch = false;

        int filesToDraw = numFiles;

        int xDraw, yDraw;
        xDraw = 0;
        yDraw = 0;

        int iconIndex = 0;
        while(filesToDraw > 0){

            if(!resVector[iconIndex].visibleInSearch){
                iconIndex++;
                continue;
            }
            const ResourceFSEntry& entry = resVector[iconIndex].r;

            if(mFilterType != ResourceType::unknown){ //Unknown as the filter type means don't filter at all.
                if(entry.type != mFilterType && !(entry.type == ResourceType::directory || entry.type == ResourceType::resourcesDirectory)) {
                    iconIndex++;
                    continue;
                }
            }

            ImVec2 drawPos(
                canvas_pos.x + xDraw * (fileSizeWithPadding.x),
                canvas_pos.y + yDraw * (fileSizeWithPadding.y)
            );

            drawPos.y -= totalScrollOffset;

            bool hovered = false;
            if(!mOgreGroupsSelectorVisible && ImGui::IsWindowHovered()){
                if(xDraw == hoverboxX && yDraw == hoverboxY){
                    hovered = true;
                }

                if(io.MouseDown[0] && !mMousePressed){
                    //If a click happened at the very least reset the selection state.
                    mCurrentSelection = -1;

                    if(hovered){
                        mMousePressed = true;
                        _handleIconClick(entry, iconIndex);

                        mDoubleClickTimer = 20;
                        mPotentialDoubleClick = true;
                    }
                }
            }
            if(mWantsDragBegin && mStartedDragX == xDraw && mStartedDragY == yDraw){
                //populate the drag data with the new item.
                mDraggedResource = entry;
                mDraggedResourceIndex = iconIndex;
                dragFoundMatch = true;
            }

            _drawResourceFileIcon(draw_list, drawPos, fileSize, hovered & allowMouseSelection, iconIndex == mCurrentSelection, iconIndex, entry, resLoc);

            xDraw++;
            if(xDraw >= maxFilesWidth - 0){
                xDraw = 0;
                yDraw++;
            }


            filesToDraw--;
            iconIndex++;
        }

        ImGui::EndChild();

        if(!dragFoundMatch){
            mWantsDragBegin = false;
        }
        //Drag mouse pressed is used rather than the other mMousePressed, as setting it could interfere with some checks that come later.
        //So this is deferred until later on to make sure.
        if(dragMousePressed){
            mMousePressed = true;
        }

    }

    void ResourceSelectorScreen::_handleIconClick(const ResourceFSEntry& entry, int entryIndex){
        if(entry.type == ResourceType::directory || entry.type == ResourceType::resourcesDirectory){
            mTargetChange = entry.name;
        }else{
            mCurrentSelection = entryIndex;

            //The second click with this true means a double click was performed.
            if(mPotentialDoubleClick) mCurrentSelectionConfirmed = true;
        }

    }

    void ResourceSelectorScreen::_getUvForFsType(ImVec2& uv0, ImVec2& uv1, ImU32& outCol, ResourceType t){
        float vecs[20] = {
            //Directory icon
            0, 0,
            0.225, 0.358,

            //Default file
            0, 0.358,
            0.225, 0.83,

            //Texture file
            0.235, 0.358,
            0.5, 0.83,

            //mesh file
            0.46, 0.358,
            0.71, 0.83,

            //script file
            0.685, 0.358,
            0.94, 0.83,
        };

        int c = 1;
        if(t == ResourceType::directory ||
            t == ResourceType::resourcesDirectory) c = 0;
        else if(t == ResourceType::texture) c = 2;
        else if(t == ResourceType::mesh) c = 3;
        else if(t == ResourceType::script) c = 4;

        int i = c * 4;
        uv0 = ImVec2(vecs[i], vecs[i + 1]);
        uv1 = ImVec2(vecs[i + 2], vecs[i + 3]);

        if(t == ResourceType::resourcesDirectory)
            outCol = GREEN_COLOUR;
        else
            outCol = IM_COL32(200, 200, 200, 255);
    }

    void ResourceSelectorScreen::_drawResourceFileIcon(ImDrawList* drawList, ImVec2 drawPos, ImVec2 iconSize, bool hovered, bool selected, int iconIndex, const ResourceFSEntry& entry, bool resLoc){
        static const ImU32 HOVER_COLOUR = IM_COL32(150, 150, 150, 255);
        static const ImU32 SELECTED_COLOUR = IM_COL32(220, 220, 220, 255);

        float maxWrapWidth = iconSize.x * 0.7;
        ImVec2 textSize = ImGui::CalcTextSize(entry.name.c_str(), NULL, false, maxWrapWidth);
        float wrapWidth = textSize.x > maxWrapWidth ? maxWrapWidth : textSize.x;

        const float textStartY = iconSize.y * 0.7;

        const std::string* targetString = &entry.name;
        std::string tempStr;
        { //Check if the string has become too long to fit into the icon.
            if(textStartY + textSize.y > iconSize.y){
                //The text has exceeded the label size. We need to cut bits off it until it fits.
                tempStr = entry.name;
                bool shortEnough = false;

                static const int cutAmmount = 1;
                while(!shortEnough){
                    if(tempStr.size() <= cutAmmount){
                        //When entering the loop for the first time it might be that nothing can be cut. In this case don't do anything.
                        shortEnough = true;
                        continue;
                    }

                    tempStr.erase(tempStr.end() - cutAmmount, tempStr.end());
                    ImVec2 newTextSize = ImGui::CalcTextSize(tempStr.c_str(), NULL, false, maxWrapWidth);

                    shortEnough = textStartY + newTextSize.y < iconSize.y;
                }

                //Attach a terminator string to indicate that the full path is not shown.
                static const std::string terminatorString = "...";
                const int sizeToRemove = tempStr.size() < terminatorString.size() ? tempStr.size() : terminatorString.size();
                tempStr.replace(tempStr.end() - sizeToRemove, tempStr.end(), terminatorString);
                targetString = &tempStr;
            }
        }

        ImVec2 textDrawPos( drawPos.x + ((iconSize.x / 2) - (wrapWidth / 2)), drawPos.y + textStartY);

        bool ogreResourceInValid = !resLoc && entry.ogreResource;
        if(hovered && ogreResourceInValid){
            ImGui::SetTooltip(
                "Ogre specific resources cannot be used unless the directory they are within is an ogre directory.\nPlease set this directory in the ogreResources.cfg file if you wish to use this file."
            );
        }
        if(hovered || selected || ogreResourceInValid){
            ImU32 col = HOVER_COLOUR;
            if(selected) col = SELECTED_COLOUR;
            if(ogreResourceInValid) col = IM_COL32(220, 10, 10, 255);
            drawList->AddRectFilled(drawPos, ImVec2(drawPos.x + iconSize.x, drawPos.y + iconSize.y), col);
        }

        ImU32 targetCol = selected ? IM_COL32(0, 0, 0, 255) : IM_COL32(255, 255, 255, 255);
        drawList->AddText(NULL, 0.0f, textDrawPos, targetCol, targetString->c_str(), NULL, wrapWidth);


        static Ogre::TexturePtr tex;
        static bool first = true;
        if(first){
            first = false;
            if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "icons.png")){
                tex = Ogre::TextureManager::getSingleton().load("icons.png", SOUTHSEA_INTERNAL_GROUP_NAME);
            }
        }

        {
            ImVec2 uv0, uv1;
            ImU32 col;
            _getUvForFsType(uv0, uv1, col, entry.type);

            static const float spacerWidth = 0.1f;
            static const float spacerHeight = 0.07f;
            const ImVec2 imgStart(drawPos.x + iconSize.x * spacerWidth, drawPos.y + iconSize.y * spacerHeight);
            const ImVec2 imgEnd(drawPos.x + (iconSize.x - iconSize.x * spacerWidth), textDrawPos.y - (iconSize.y * spacerHeight));
            drawList->AddImage(tex.getPointer(), imgStart, imgEnd, uv0, uv1, col);
        }

    }
}
