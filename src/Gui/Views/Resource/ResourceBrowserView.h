#pragma once

#include "imgui.h"

namespace Southsea{
    class ResourceSelectorScreen;

    class ResourceBrowserView{
    public:
        static void drawResourceBrowserView(bool& windowOpen);

        static void startup();
        static void shutdown();

    private:
        static void _drawDraggedResourceIcon(ResourceSelectorScreen* screen);

        static ResourceSelectorScreen* screen;
    };
}
