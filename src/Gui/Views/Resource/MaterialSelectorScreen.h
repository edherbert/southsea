#pragma once

#include "ResourceScreen.h"
#include <vector>

namespace Southsea{
    class DialogMaterialManager;

    struct DisplayDatablockData;

    class MaterialSelectorScreen : public ResourceScreen{
    public:
        MaterialSelectorScreen(int selectorId);
        ~MaterialSelectorScreen();

        void drawResourceSelectorScreen();

        void getCurrentSelectionData(OutputResourceData& data);

    private:
        DialogMaterialManager* mMatManager;

        enum class DatablockTypes{
            Pbs,
            Unlit
        };
        bool _drawMaterialsColumn(const std::vector<DisplayDatablockData>& values, const char* groupName, DatablockTypes currentType);

        char searchS[64] = "";

        DatablockTypes mCurrentSelectedType;

    };
}
