#include "MaterialSelectorScreen.h"

#include "imgui.h"
#include <iostream>

#include "Backend/DialogMaterialManager.h"

namespace Southsea{
    MaterialSelectorScreen::MaterialSelectorScreen(int selectorId)
     : ResourceScreen(selectorId),
    mCurrentSelectedType(MaterialSelectorScreen::DatablockTypes::Pbs) {
        mMatManager = new DialogMaterialManager();

        mMatManager->scanForDatablocks();
    }

    MaterialSelectorScreen::~MaterialSelectorScreen(){
        delete mMatManager;
    }

    bool MaterialSelectorScreen::_drawMaterialsColumn(const std::vector<DisplayDatablockData>& values, const char* groupName, DatablockTypes currentType){
        bool result = false;

        if(ImGui::CollapsingHeader(groupName, ImGuiTreeNodeFlags_DefaultOpen)){

            int targetSelection = currentType == mCurrentSelectedType ? mCurrentSelection : -1;
            ImGui::Columns(3, NULL, false);
            for (int i = 0; i < values.size(); i++){
                if(!values[i].visibleInSearch) continue;
                bool selected = (i == targetSelection);
                if(ImGui::Selectable(values[i].blockName.c_str(), selected)){
                    //Reset all the values.
                    memset(&selected, 0, sizeof(selected));
                    mCurrentSelection = i;
                    result = true;
                }
                ImGui::NextColumn();
            }
            ImGui::Columns(1);

        }

        return result;
    }

    void MaterialSelectorScreen::drawResourceSelectorScreen(){

        const ImVec2 windowSize = ImGui::GetContentRegionAvail();
        ImGui::PushItemWidth(windowSize.x);
        if(ImGui::InputTextWithHint("##Search", "Search", searchS, 64)){
            mMatManager->setSearchTerm(searchS);
        }
        ImGui::PopItemWidth();

        if(ImGui::BeginChild("materialsScroller", ImGui::GetContentRegionAvail(), true, 0)){

            const std::vector<DisplayDatablockData>& pbsBlocks = mMatManager->getPbsBlocks();
            if(_drawMaterialsColumn(pbsBlocks, "Pbs", MaterialSelectorScreen::DatablockTypes::Pbs)){
                mCurrentSelectedType = MaterialSelectorScreen::DatablockTypes::Pbs;
            }

            const std::vector<DisplayDatablockData>& unlitBlocks = mMatManager->getUnlitBlocks();
            if(_drawMaterialsColumn(unlitBlocks, "Unlit", MaterialSelectorScreen::DatablockTypes::Unlit)){
                mCurrentSelectedType = MaterialSelectorScreen::DatablockTypes::Unlit;
            }

        }
        ImGui::EndChild();

    }

    void MaterialSelectorScreen::getCurrentSelectionData(OutputResourceData& data){
        data.group = "";

        const std::vector<DisplayDatablockData>& blocks = mCurrentSelectedType == MaterialSelectorScreen::DatablockTypes::Pbs ?
            mMatManager->getPbsBlocks() : mMatManager->getUnlitBlocks();

        assert(mCurrentSelection < blocks.size());

        data.name = blocks[mCurrentSelection].blockName;
        data.displayName = blocks[mCurrentSelection].blockName;
    }

};
