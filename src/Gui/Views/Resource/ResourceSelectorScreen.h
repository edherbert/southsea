#pragma once

#include "ResourceScreen.h"

#include "imgui.h"
#include <string>
//I only need the enum, so this could be moved later on.
#include "Backend/DialogResourcesManager.h"

namespace Southsea{
    class DialogResourcesManager;

    /**
    The implementation of the imgui resources selector screen.
    This is intended to be separate from an actual view, so that it can be reused.
    With that it could be integrated into a number of views at once (i.e popups)
    */
    class ResourceSelectorScreen : public ResourceScreen{
    public:
        ResourceSelectorScreen(int selectorId);
        ~ResourceSelectorScreen();

        void drawResourceSelectorScreen();

        //Reset selection.
        void resetState();

        void allowResourceDragging(bool allow){
            mAllowResourceDrag = allow;
        }

        void getCurrentSelectionData(OutputResourceData& data);

        void setDefaultResourcesDirectory();

        //bool isResourceDragging() const { return mResourceDragging; }
        bool wantsResourceDrag() const { return mWantsDragBegin; }
        void notifyDragRequestReceived() { mWantsDragBegin = false; }
        void setResourceDragging(bool dragging) { mResourceDragging = dragging; }
        const ResourceFSEntry& getDraggedResource() const { return mDraggedResource; }
        int getDraggedResourceIndex() const { return mDraggedResourceIndex; }

        DialogResourcesManager* getDialogResourcesManager() { return mDialogResourcesManager; }

        void _drawResourceFileIcon(ImDrawList* drawList, ImVec2 drawPos, ImVec2 iconSize, bool hovered, bool selected, int iconIndex, const ResourceFSEntry& entry, bool resLoc);

    private:
        /**
        @param allowMouseSelection
        Specify whether mouse should have any effect on the selector screen.
        */
        void _drawFileSelector(bool allowMouseSelection);
        void _drawDirectorySwitcher();
        void _drawPathTextBox(float boxWidth);

        static void _getUvForFsType(ImVec2& uv0, ImVec2& uv1, ImU32& outCol, ResourceType t);

        void _handleIconClick(const ResourceFSEntry& entry, int entryIndex);

        //If the mouse is pressed and not yet released.
        bool mMousePressed = false;
        bool mOgreGroupsSelectorVisible = false;

        char pathS[128] = "";
        char searchS[64] = "";
        char pathSelectorWindowS[64] = "";

        //Keeps track of what the target directory change should be.
        //Changing the directory inline isn't always a good idea, so I store it in the mean time until it's safe to request the change.
        std::string mTargetChange;

        int mDoubleClickTimer = 0;
        bool mPotentialDoubleClick = false;

        bool mAllowResourceDrag = false;
        int mStartedDragX, mStartedDragY;
        float mStartedMouseDragX, mStartedMouseDragY;
        bool mResourceDragging = false;
        bool mWantsDragBegin = false;
        ResourceFSEntry mDraggedResource;
        int mDraggedResourceIndex;

        static const ImU32 GREEN_COLOUR;

        DialogResourcesManager* mDialogResourcesManager;
    };
}
