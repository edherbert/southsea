#include "NavMeshView.h"

#include "Editor/EditorSingleton.h"
#include "Editor/Chunk/NavMesh/NavMeshManager.h"

#include "Gui/Views/DisabledButton.h"
#include "Editor/Action/Actions/Scene/NavMeshChangeVariable.h"
#include "Editor/Action/ActionStack.h"
#include "Event/EventDispatcher.h"
#include "Event/Events/SceneEvent.h"
#include "Editor/Chunk/EditedChunk.h"
#include "System/Project/ChunkMetaFile.h"
#include "DetourNavMesh.h"

#include <iostream>

namespace Southsea{

    static NavMeshSettings mCurrentSettings;
    static NavMeshSettings mOldSettings;
    static NavMeshId mCurrentSelectedMesh = INVALID_NAV_MESH_ID;


    void NavMeshView::startup(){
        EventDispatcher::subscribeStatic(EventType::Scene, NavMeshView::sceneEventReceiver);

        mCurrentSelectedMesh = INVALID_NAV_MESH_ID;
    }


    void NavMeshView::drawNavMeshView(bool &windowOpen){

        if(!windowOpen) return;

        if(!ImGui::Begin("NavMesh", &windowOpen, 0)){
            ImGui::End();
            return;
        }

        _drawCurrentMeshes();

        ImGui::End();
    }

    void setCurrentMesh(std::shared_ptr<NavMeshManager> meshManager, NavMeshId id){
        if(id != INVALID_NAV_MESH_ID){
            std::cout << "nav mesh selection changed" << std::endl;

            //Set the view's setting values.
            const std::vector<NavMeshData>& meshData = meshManager->getCurrentMeshes();
            //Check if id is valid.
            if(id >= meshData.size()){
                mCurrentSelectedMesh = INVALID_NAV_MESH_ID;
                meshManager->setCurrentSelectedMesh(mCurrentSelectedMesh);
                return;
            }
            const NavMeshData& e = meshData[id];
            mCurrentSettings = e.settings;
            mOldSettings = e.settings;
        }
        mCurrentSelectedMesh = id;
        meshManager->setCurrentSelectedMesh(id);
    }

    void NavMeshView::_drawCurrentMeshes(){
        std::shared_ptr<NavMeshManager> meshManager = EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager();
        NavMeshId newId = meshManager->getCurrentSelectedMesh();
        if(newId != mCurrentSelectedMesh && newId != INVALID_NAV_MESH_ID){
            setCurrentMesh(meshManager, newId);
        }
        mCurrentSelectedMesh = newId;
        const ImVec2 regionSize = ImGui::GetContentRegionAvail();

        ImGui::Text("Current meshes");

        const std::vector<NavMeshData>& meshData = meshManager->getCurrentMeshes();
        if(mCurrentSelectedMesh >= meshData.size()){
            setCurrentMesh(meshManager, INVALID_NAV_MESH_ID);
        }

        const int panelSize = regionSize.x;
        if(ImGui::BeginChild("currentMeshesScroller", ImVec2(panelSize, 200), true, 0)){
            ImVec2 pos = ImGui::GetCursorPos();
            ImGui::Dummy(ImVec2(panelSize, 175));
            bool clicked = ImGui::IsItemClicked(0);
            ImGui::SetCursorPos(pos);

            bool anySelected = false;
            for(NavMeshId i = 0; i < meshData.size(); i++){
                if(i >= INVALID_NAV_MESH_ID) break;
                const NavMeshData& e = meshData[i];
                if(!e.populated) continue;
                bool selected = i == mCurrentSelectedMesh;

                ImGui::Selectable(e.meshName.c_str(), &selected, 0);
                anySelected |= ImGui::IsItemClicked(0);
                if(selected){
                    if(mCurrentSelectedMesh != i){
                        setCurrentMesh(meshManager, i);
                        mCurrentSettings = e.settings;
                        mOldSettings = e.settings;
                    }
                }
            }

            if(clicked && !anySelected){
                setCurrentMesh(meshManager, INVALID_NAV_MESH_ID);
            }
        }
        ImGui::EndChild();

        //TODO These are defined in a json file now, but there should be some replacement.
        /*
        if(ImGui::Button("Add")){
            meshManager->createNewNavMeshGenericName();
        }
        ImGui::SameLine();
        if(DisabledButton("Remove", mCurrentSelectedMesh != INVALID_NAV_MESH_ID)){
            meshManager->removeNavMesh(mCurrentSelectedMesh);
            setCurrentMesh(meshManager, INVALID_NAV_MESH_ID);
        }
        */

        if(mCurrentSelectedMesh != INVALID_NAV_MESH_ID){
            assert(meshData.size() > mCurrentSelectedMesh);
            _drawSelectedMeshInfo(meshData[mCurrentSelectedMesh]);
        }else{
            ImGui::Separator();
            ImGui::Text("Select a mesh to view its settings.");
        }

    }

    void NavMeshView::_drawSelectedMeshInfo(const NavMeshData& selectedMesh){
        ImGui::Text("Selected mesh info");
        ImGui::Separator();

        const int contributionSize = static_cast<int>(selectedMesh.contributingMarkers.size());
        ImGui::Text("Contributing nav markers: %i", contributionSize);

        ImGui::Separator();
        if(ImGui::Checkbox("Include terrain", &mCurrentSettings.includeTerrain)){
            NavMeshChangeVariable* a = new NavMeshChangeVariable();
            a->populateIncludeTerrain(mCurrentSelectedMesh, mCurrentSettings.includeTerrain);
            a->performAction();
            EditorSingleton::getEditor()->getActionStack()->pushAction(a);
        }

        bool valueChanged = false;
        static const size_t TOTAL_VALUES = 13;
        bool out[TOTAL_VALUES];
        for(int i = 0; i < TOTAL_VALUES; i++) out[i] = false;

        #define CHECK_CHANGE(__i__, __x__) __x__; out[__i__] |= ImGui::IsItemDeactivatedAfterEdit(); valueChanged |= out[__i__];

        ImGui::Separator();
        ImGui::Text("Agent");
        CHECK_CHANGE(0, ImGui::SliderFloat("Height", &mCurrentSettings.agentHeight, 0.1f, 5.0f, "%.3f", 0.1f));
        CHECK_CHANGE(1, ImGui::SliderFloat("Radius", &mCurrentSettings.agentRadius, 0.0f, 5.0f, "%.3f", 0.1f));
        CHECK_CHANGE(2, ImGui::SliderFloat("Max Climb", &mCurrentSettings.agentMaxClimb, 0.1f, 5.0f, "%.3f", 0.1f));
        CHECK_CHANGE(3, ImGui::SliderFloat("Max Slope", &mCurrentSettings.agentMaxSlope, 0.0f, 90.0f, "%.3f", 1.0f));

        ImGui::Separator();
        ImGui::Text("Rasterization");
        CHECK_CHANGE(4, ImGui::SliderFloat("Cell Size", &mCurrentSettings.cellSize, 0.0f, 150.0f, "%.3f", 1.0f));
        CHECK_CHANGE(5, ImGui::SliderFloat("Cell Height", &mCurrentSettings.cellHeight, 0.0f, 150.0f, "%.3f", 1.0f));

        ImGui::Separator();
        ImGui::Text("Region");
        CHECK_CHANGE(6, ImGui::SliderFloat("Min Region Size", &mCurrentSettings.regionMinSize, 0.0f, 150.0f, "%.3f", 1.0f));
        CHECK_CHANGE(7, ImGui::SliderFloat("Merged Region Size", &mCurrentSettings.regionMergeSize, 0.0f, 150.0f, "%.3f", 1.0f));

        ImGui::Separator();
        ImGui::Text("Polygonization");
        CHECK_CHANGE(8, ImGui::SliderFloat("Max Edge Length", &mCurrentSettings.edgeMaxLen, 0.0f, 50.0f, "%.3f", 1.0f));
        CHECK_CHANGE(9, ImGui::SliderFloat("Max Edge Error", &mCurrentSettings.edgeMaxError, 0.1f, 3.0f, "%.3f", 0.1f));
        CHECK_CHANGE(10, ImGui::SliderFloat("Verts Per Poly", &mCurrentSettings.vertsPerPoly, 3.0f, 12.0f, "%.3f", 1.0f));

        ImGui::Separator();
        ImGui::Text("Detail Mesh");
        CHECK_CHANGE(11, ImGui::SliderFloat("Sample Distance", &mCurrentSettings.detailSampleDist, 0.0f, 16.0f, "%.3f", 1.0f));
        CHECK_CHANGE(12, ImGui::SliderFloat("Max Sample Error", &mCurrentSettings.detailSampleMaxError, 0.0f, 16.0f, "%.3f", 1.0f));

        #undef CHECK_CHANGE

        if(valueChanged){
            //Find which value changed
            NavMeshChangeVariable* a = new NavMeshChangeVariable();
            //Just as a default.
            NavMeshDataType t = NavMeshDataType::AGENT_HEIGHT;
            float oldValue, newValue;
            oldValue = newValue = 0.0f;
            for(int i = 0; i < TOTAL_VALUES; i++){
                if(out[i]){
                    NavMeshDataType tt[] = {
                        NavMeshDataType::AGENT_HEIGHT,
                        NavMeshDataType::AGENT_RADIUS,
                        NavMeshDataType::AGENT_MAX_CLIMB,
                        NavMeshDataType::AGENT_MAX_SLOPE,
                        NavMeshDataType::CELL_SIZE,
                        NavMeshDataType::CELL_HEIGHT,
                        NavMeshDataType::REGION_MIN_SIZE,
                        NavMeshDataType::REGION_MERGE_SIZE,
                        NavMeshDataType::EDGE_MAX_LEN,
                        NavMeshDataType::EDGE_MAX_ERROR,
                        NavMeshDataType::VERTS_PERPOLY,
                        NavMeshDataType::DETAIL_SAMPLE_DIST,
                        NavMeshDataType::DETAIL_SAMPLE_MAX_ERROR
                    };
                    t = tt[i];
                    oldValue = *(getNavMeshValueFromSettings(t, mOldSettings));
                    newValue = *(getNavMeshValueFromSettings(t, mCurrentSettings));
                    break;
                }
            }

            a->populate(t, mCurrentSelectedMesh, oldValue, newValue);
            a->performAction();
            EditorSingleton::getEditor()->getActionStack()->pushAction(a);
        }

        ImGui::Separator();
        bool allowBuild = contributionSize > 0 || mCurrentSettings.includeTerrain;

        if(DisabledButton("Bake", allowBuild)){
            bool success = EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager()->buildNavMesh(mCurrentSelectedMesh);

            if(!success) ImGui::OpenPopup("nav mesh fail");
        }
        //NOTE Temporary (I think)
        if(DisabledButton("Bake tiled", allowBuild)){
            bool success = EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager()->buildAllTiles(mCurrentSelectedMesh);

            if(!success) ImGui::OpenPopup("nav mesh fail");
        }
        if(!allowBuild){
            if(ImGui::IsItemHovered()){
                ImGui::SetTooltip("This mesh is not referenced by any nav markers.");
            }
        }

        static bool pOpen2 = true;
        if(ImGui::BeginPopupModal("nav mesh fail", &pOpen2, ImGuiWindowFlags_AlwaysAutoResize)){
            ImGui::Text("There was an error generating the nav mesh.");

            if(ImGui::Button("Ok", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
            ImGui::EndPopup();
        }

        //Print information about the current nav mesh
        if(selectedMesh.outData.mesh){
            int totalPolys = 0;
            int totalVerts = 0;
            int totalTiles = 0;

            const dtNavMesh* mesh = selectedMesh.outData.mesh;
            for(int i = 0; i < mesh->getMaxTiles(); i++){
                const dtMeshTile* tile = mesh->getTile(i);
                if(!tile || !tile->header) continue;
                totalPolys += tile->header->polyCount;
                totalVerts += tile->header->vertCount;
                totalTiles++;
            }
            ImGui::Text("%i polys", totalPolys);
            ImGui::Text("%i verts", totalVerts);
            ImGui::Text(totalTiles == 1 ? "%i tile" : "%i tiles", totalTiles);
        }else{
            ImGui::Text("Mesh not build.");
        }
    }

    bool NavMeshView::sceneEventReceiver(const Event& e){
        const SceneEvent& sceneEvent = static_cast<const SceneEvent&>(e);
        if(sceneEvent.eventCategory() == SceneEventCategory::NavMeshValueChange){
            const SceneNavMeshValueChange& event = static_cast<const SceneNavMeshValueChange&>(sceneEvent);

            if(event.id == mCurrentSelectedMesh){
                const std::vector<NavMeshData>& meshData = EditorSingleton::getEditor()->getEditorChunk()->getNavMeshManager()->getCurrentMeshes();

                const NavMeshData& e = meshData[event.id];
                mCurrentSettings = e.settings;
                mOldSettings = e.settings;
            }
        }

        return false;
    }


}
