#pragma once

namespace Southsea{

    class SettingsView{
    public:
        /**
        Call the appropriate imgui functions to draw the settings view.
        @returns true if the settings window should close.
        */
        static bool drawSettingsContent();

        static void startup();
        static void shutdown();

    private:

        static void _resetValues();
        static void _saveSettings();
    };
}
