#include "ResourceButton.h"

#include <iostream>
#include "imgui.h"
#include "imgui_internal.h"

#include "OgreTextureManager.h"

#include "Gui/Views/Resource/ResourceSelectorPopup.h"
#include "Platforms/OgreSetup/SouthseaResourceGroups.h"
#include "System/Editor/DraggedResourceInfo.h"

#include "System/BaseSingleton.h"

namespace Southsea{
    bool ResourceButton(ResourceButtonData* data, ResourceButtonData* oldData, const char* label){
        //This is a copy of ButtonEx in imgui.
        //I copied it almost directly because I wanted the extra control.

        static const ImU32 GREEN_COLOUR = IM_COL32(47, 128, 69, 255);
        static const ImU32 HOVERED_COLOUR = IM_COL32(67, 148, 69, 255);
        static const ImU32 ACTIVE_COLOUR = IM_COL32(77, 180, 69, 255);
        static const ImU32 WHITE_COLOUR = IM_COL32(255, 255, 255, 255);

        bool resourceValid = data->resourceEmpty();
        const bool resourceDragged = BaseSingleton::getDraggedResourceInfo()->getDraggingResource();
        const ResourceFSEntry& draggedResEntry = BaseSingleton::getDraggedResourceInfo()->getCurrentDraggedResourceEntry();

        //At the moment label is intended to be a hint string for the sort of resource.
        //For instance you could give a message like 'select a blendmap'. I might remove the label later on.
        const char *drawChar = label;
        //If no label was provided then default to this.
        if(label[0] == '\0'){
            drawChar = "<empty>";
        }
        if(resourceValid){
            drawChar = data->displayName.c_str();
        }

        const ImVec2 imageSize(20, 20);
        static const float imagePadding = 5;
        const ImVec2 size_arg(0, imageSize.y + imagePadding * 2);
        ImGuiButtonFlags flags = 0;

        ImGuiWindow* window = ImGui::GetCurrentWindow();
        if (window->SkipItems)
            return false;

        ImGuiContext& g = *GImGui;
        const ImGuiStyle& style = g.Style;
        const ImGuiID id = window->GetID(drawChar);
        const ImVec2 label_size = ImGui::CalcTextSize(drawChar, NULL, true);

        ImVec2 pos = window->DC.CursorPos;
        if ((flags & ImGuiButtonFlags_AlignTextBaseLine) && style.FramePadding.y < window->DC.CurrLineTextBaseOffset) // Try to vertically align buttons that are smaller/have no padding so that text baseline matches (bit hacky, since it shouldn't be a flag)
            pos.y += window->DC.CurrLineTextBaseOffset - style.FramePadding.y;
        ImVec2 size = ImGui::CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

        //Add the size padding for the image.
        //I do this after the CalcItemSize, as that function alters its output based on inputs.
        size = ImVec2(size.x + imageSize.x + imagePadding, size.y);

        const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
        ImGui::ItemSize(size, style.FramePadding.y);
        if (!ImGui::ItemAdd(bb, id))
            return false;

        if (g.LastItemData.InFlags & ImGuiItemFlags_ButtonRepeat)
            flags |= ImGuiButtonFlags_Repeat;
        bool hovered, held;
        bool pressed = ImGui::ButtonBehavior(bb, id, &hovered, &held, flags);

        if(hovered && resourceValid){
            if(data->hoverCount < 40){
                data->hoverCount++;
            }else{
                ImGui::SetTooltip("%s", data->res.name.c_str());
            }
        }else{
            data->hoverCount = 0;
        }

        bool resourceMouseHover = false;
        if(resourceDragged){
            //I need to calculate the hover myself for the resource drag.
            //The reason for that is that imgui doesn't count hover if the mouse is down and it moves over the item.
            //I need that functionality, so I have to do it myself.

            ImGuiIO& io = ImGui::GetIO();
            resourceMouseHover =
                io.MousePos.x >= bb.Min.x &&
                io.MousePos.y >= bb.Min.y &&
                io.MousePos.x <= bb.Max.x &&
                io.MousePos.y <= bb.Max.y;
        }

        // Render
        ImU32 col = ImGui::GetColorU32((held && hovered) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        if(resourceValid){
            col = (held && hovered) ? ACTIVE_COLOUR : hovered ? HOVERED_COLOUR : GREEN_COLOUR;
        }

        ImGui::RenderNavHighlight(bb, id);

        { //Drag outline
            const float OUTLINE_SIZE = resourceMouseHover ? 4.0f : 2.0f;
            static const ImU32 ACCEPTED_COLOUR = IM_COL32(255, 255, 255, 255);
            static const ImU32 REJECTED_COLOUR = IM_COL32(250, 20, 20, 255);

            ImU32 resolvedColour = draggedResEntry.type == data->type ? ACCEPTED_COLOUR : REJECTED_COLOUR;

            ImU32 targetCol = resourceDragged ? resolvedColour : col;
            ImGui::RenderFrame(bb.Min, bb.Max, targetCol, true, style.FrameRounding);

            if(resourceDragged){
                ImGui::RenderFrame(
                    ImVec2(bb.Min.x + OUTLINE_SIZE, bb.Min.y + OUTLINE_SIZE),
                    ImVec2(bb.Max.x - OUTLINE_SIZE, bb.Max.y - OUTLINE_SIZE),
                col, true, style.FrameRounding);

                if(resourceMouseHover){
                    BaseSingleton::getDraggedResourceInfo()->notifyHoverItem(data, oldData);
                }
            }
        }

        const ImVec2 addVal(bb.Min.x + style.FramePadding.x, bb.Min.y + style.FramePadding.y);
        const ImVec2 minusVal(bb.Max.x - style.FramePadding.x, bb.Max.y - style.FramePadding.y);
        ImGui::RenderTextClipped(addVal, minusVal, drawChar, NULL, &label_size, ImVec2(1, 0.5), &bb);


        {
            static Ogre::TexturePtr tex;
            static bool first = true;
            if(first){
                first = false;
                if(Ogre::ResourceGroupManager::getSingleton().resourceExists(SOUTHSEA_INTERNAL_GROUP_NAME, "icons.png")){
                    tex = Ogre::TextureManager::getSingleton().load("icons.png", SOUTHSEA_INTERNAL_GROUP_NAME);
                }
            }

            const ImRect image_bb(
                ImVec2(pos.x + imagePadding, pos.y + imagePadding),
                ImVec2(pos.x + imagePadding + imageSize.x, pos.y + imageSize.y + imagePadding)
            );
            const ImU32 colour = IM_COL32(255, 255, 255, 255);
            window->DrawList->AddImage(tex.getPointer(), image_bb.Min, image_bb.Max, ImVec2(0, 0.358), ImVec2(0.225, 0.83), colour);
        }


        IMGUI_TEST_ENGINE_ITEM_INFO(id, label, window->DC.LastItemStatusFlags);

        if(pressed && data->state == ResButtonState::None){
            ResourceSelectorPopup::requestPopupAppear(data, oldData);
        }

        bool retVal = false;
        if(data->state == ResButtonState::Dirty){
            data->state = ResButtonState::None;
            retVal = true;
        }

        return retVal;
    }
}
