#pragma once

#include "OgreString.h"
#include "OgreVector4.h"

#include "ResourceButton.h"

namespace Ogre{
    class HlmsTerraDatablock;
    class Vector3;
}

namespace Southsea{
    class Event;

    class TerrainPropertiesView{
    public:
        static void drawTerrainProperties();

        static void startup();
        static void shutdown();

        static bool editorEventReceiver(const Event& event);

    private:

        static Ogre::HlmsTerraDatablock* currentTerrainDatablock;

        static bool terrainEnabled;
        static float diffuseCol[3];

        static inline void _copyToVector(const Ogre::Vector3& target, float (&destination)[3]);
        static bool _vec3Colour(float (&colour)[3], Ogre::Vector3& prev);

        struct DetailLayerData{
            bool enabled = false;

            const Ogre::Vector4 defaultOffsetScaleVec = Ogre::Vector4(0, 0, 1, 1);
            //TODO check float to int here.
            float offsetVec[2] = {defaultOffsetScaleVec[0], defaultOffsetScaleVec[1]};
            float scaleVec[2] = {defaultOffsetScaleVec[2], defaultOffsetScaleVec[3]};

            float roughness = 1.0f;
            float metalness = 1.0f;

            ResourceButtonData textureButtonData;

            static DetailLayerData DEFAULT;
        };
        static DetailLayerData layerDetail[4];

        static void _invertDatablockLayer(int layer, bool enabled);

        static void _reobtainTerrainDatablock();

        static void _drawTerrainBrushSettings();
        static void _drawShapeBrushView();
        static void _drawTextureBrushView();
        static void _drawRaiseLowerSettings();
        static void _drawSetHeightBrushView();

        static void _drawDetailLevelSelector();

        /**
        Populate the internal editor datastructures from a datablock.

        @param alterChecked
        Whether or not the function should change the value of the layer enabled checkbox.
        Sometimes a series of undo actions could cause the layer to appear as the default, where the layer would be disabled.
        Occasinally that's not what we want (performing actions).
        */
        static void _populateEditorFromDatablock(Ogre::HlmsTerraDatablock* db);

        static void _clearDetailLayerData();
    };
}
