#pragma once

#include <string>
#include "Editor/Resources/ResourceTypes.h"

namespace Southsea{
    enum class ResButtonState{
        None,
        Dirty, //The resource has changed and an update needs to be performed.
        Selecting //The resource popup is open for this button. When this state is finished the state will switch to Dirty.

        //I might have a 'prospective' resource state, which would be set when a compatible resource is hovered over this button.
        //Then for instance the user could provide feedback about what that change would look like even before the switch is requested.
    };

    struct ResourceButtonData{
        ResButtonState state;
        ResourceType type = ResourceType::unknown;
        Resource res;

        //The name of the resource which is shown on the button.
        //This is stored in a separate string to the path so I don't have to resolve it each frame.
        std::string displayName;
        //Used to keep track of whether the full path tooltip should appear. Its a char because it only needs 8 bits.
        unsigned char hoverCount;

        bool resourceEmpty(){
            return !res.empty();
        }
        ResourceButtonData& operator=(const ResourceButtonData& data){
            state = data.state;
            type = data.type;
            displayName = data.displayName;
            hoverCount = data.hoverCount;

            res = data.res;

            return *this;
        }
        ResourceButtonData& operator=(const Resource& data){
            clear();
            res = data;

            return *this;
        }
        ResourceButtonData& operator=(const ResourceFSEntry& data){
            res.name = data.name;
            res.group.clear();
            displayName = data.name;

            return *this;
        }
        void setName(const std::string& name){
            res.name = name;
            displayName = name;
        }
        void clear(){
            res.clear();
            displayName.clear();
        }
    };

    bool ResourceButton(ResourceButtonData* data, ResourceButtonData* oldData = 0, const char* label = "");
}
