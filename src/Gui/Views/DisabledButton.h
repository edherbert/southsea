#pragma once

#include "imgui.h"
#include "imgui_internal.h"

namespace Southsea{
    static bool DisabledButton(const char* label, bool enabled, ImVec2 size = ImVec2(0, 0)){

        if (!enabled)
        {
            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }
        bool result = ImGui::Button(label, size);
        if (!enabled)
        {
            ImGui::PopItemFlag();
            ImGui::PopStyleVar();
        }

        return result;

    }
}
