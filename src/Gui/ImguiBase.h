#pragma once

#include <OgreFrameListener.h>

namespace Ogre{
    class SceneManager;
}

namespace Southsea{
    class Event;

    class ImguiBase : public Ogre::FrameListener{
    public:
        ImguiBase();
        ~ImguiBase();

        void initialise(Ogre::SceneManager *sceneManager);

        bool systemEventReceiver(const Event &e);

        void update();

    private:
        void _setupImguiStyle();

        bool frameRenderingQueued(const Ogre::FrameEvent& evt);

        void _showOverlay();
    };
}
