#include "ImguiBase.h"

#include <iostream>
#include "ImguiOgre/ImguiManager.h"
#include <OgreRoot.h>

#include "Event/EventDispatcher.h"
#include "Event/Events/SystemEvent.h"

#include "Editor/EditorSingleton.h"

#include "System/Setup/SystemSettings.h"

#include "Views/StartupView.h"
#include "Views/EditorView.h"

namespace Southsea{
    ImguiBase::ImguiBase(){

    }

    ImguiBase::~ImguiBase(){
        ImguiManager::getSingleton().shutdown();
    }

    void ImguiBase::initialise(Ogre::SceneManager *sceneManager){
        EventDispatcher::subscribe(EventType::System, SOUTHSEA_BIND(ImguiBase::systemEventReceiver));

        ImguiManager::getSingleton().init(sceneManager);

        ImGuiIO& io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

        _setupImguiStyle();

        Ogre::Root::getSingleton().addFrameListener(this);
    }

    void ImguiBase::_setupImguiStyle(){
        ImGuiStyle * style = &ImGui::GetStyle();
        style->Alpha = 1;
        //style->WindowRounding = 0;
        style->Colors[ImGuiCol_WindowBg] = ImVec4(0.1, 0.1, 0.1, 1);

        style->ScaleAllSizes(SystemSettings::getGuiScale());
    }

    bool ImguiBase::systemEventReceiver(const Event &e){
        const SystemEvent& event = (SystemEvent&)e;
        if(event.eventCategory() == SystemEventCategory::WindowResize){
            const SystemEventWindowResize& rEvent = (SystemEventWindowResize&)e;
            ImguiManager::getSingletonPtr()->updateProjectionMatrix(rEvent.width * rEvent.widthScale, rEvent.height * rEvent.heightScale);
        }
        return true;
    }

    void ImguiBase::update(){
        //The imgui logic is done outside of the ogre rendering queue. This will help to avoid any conflicts with ogre itself, for instance creating things like SceneManager during a renderOneFrame call.
        ImguiManager::getSingletonPtr()->newFrame();

        _showOverlay();
    }

    bool ImguiBase::frameRenderingQueued(const Ogre::FrameEvent& evt){

        ImguiManager::getSingletonPtr()->render();

        return true;
    }

    void ImguiBase::_showOverlay(){
        /*static bool show_demo_window = true;
        if (show_demo_window)
        ImGui::ShowDemoWindow(&show_demo_window);*/

        static bool editing = false;
        static bool starting = true;
        if(starting){
            StartupView::start();
            starting = false;
        }

        if(!EditorSingleton::isEditing()){
            if(editing){
                EditorView::shutdownEditor();
                editing = false;
            }
            StartupView::drawStartupView();
        }else{
            if(!editing){
                EditorView::startupEditor();
                editing = true;
            }
            EditorView::drawEditorView();
        }
    }
}
