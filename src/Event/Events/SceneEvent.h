#pragma once

#include "Event.h"
#include "Editor/Scene/SceneTypes.h"

namespace Southsea{

    #define SOUTHSEA_SCENE_EVENT_CATEGORY(c) virtual SceneEventCategory eventCategory() const { return c; };

    enum class SceneEventCategory{
        Null,
        NavMeshValueChange
    };

    class SceneEvent : public Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Scene)
        SOUTHSEA_SCENE_EVENT_CATEGORY(SceneEventCategory::Null)
    };

    class SceneNavMeshValueChange : public SceneEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Scene)
        SOUTHSEA_SCENE_EVENT_CATEGORY(SceneEventCategory::NavMeshValueChange)

        NavMeshId id;
    };
}