#pragma once

#include "Event.h"

namespace Southsea{

    #define SOUTHSEA_COMMAND_EVENT_CATEGORY(c) virtual CommandEventCategory eventCategory() const { return c; };

    enum class CommandEventCategory{
        Null,
        GrabCursor,

        Undo,
        Redo,

        Save,
        Selection,
        Copy,
        Paste,
        Cut,
        Duplicate
    };

    class CommandEvent : public Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Null)
    };

    class CommandEventGrabCursor : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::GrabCursor)

        bool grabCursor; //Whether or not to grab or ungrab the cursor.
    };

    class CommandEventUndo : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Undo)
    };

    class CommandEventRedo : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Redo)
    };

    class CommandEventSave : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Save)
    };

    class CommandEventSelection : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Selection)

        enum SelectionType{
            SelectAll,
            DeSelect
        };
        SelectionType t;
    };

    class CommandEventCopy : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Copy)
    };

    class CommandEventPaste : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Paste)
    };

    class CommandEventCut : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Cut)
    };

    class CommandEventDuplicate : public CommandEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Command)
        SOUTHSEA_COMMAND_EVENT_CATEGORY(CommandEventCategory::Duplicate)
    };

}
