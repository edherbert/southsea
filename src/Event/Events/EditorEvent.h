#pragma once

#include "Event.h"
#include "Editor/Scene/SceneTypes.h"
#include "Editor/EditorSelectionTools.h"

namespace Southsea{

    #define SOUTHSEA_EDITOR_EVENT_CATEGORY(c) virtual EditorEventCategory eventCategory() const { return c; };

    enum class EditorEventCategory{
        Null,
        DirtySaveAction,
        TerrainDatablockChange,
        ChunkSwitch,
        ChunkSwitchDeclined,
        SceneTreeSelectionChanged,
        SceneTreeValueChange,
        SceneTreeItemDeleted,
        SelectionToolChange
    };

    class EditorEvent : public Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::Null)
    };

    //A notification event sent out when an action has occured that should warrent a save.
    //This will let the editor class know that a save should be requested before editor shutdown.
    class EditorDirtySave : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::DirtySaveAction)
    };

    //When something in the current terrain datablock changes.
    class TerrainDatablockChange : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::TerrainDatablockChange)
    };

    //When the chunk is switched.
    class ChunkSwitchEvent : public EditorEvent{
        //TODO the chunk switch won't be called that much in comparison to something like the dirty event.
        //Maybe it should be moved somewhere else.
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::ChunkSwitch)

        int oldChunkX, oldChunkY;
        int newChunkX, newChunkY;
    };

    //A chunk is declined if it's requested, and the user clicks cancel on the dirty save popup.
    class ChunkSwitchDeclinedEvent : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::ChunkSwitchDeclined)

        int requestedChunkX, requestedChunkY;
    };

    class SceneTreeSelectionChangedEvent : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::SceneTreeSelectionChanged)

        int selectionCount; //The number of objects which are selected.
    };

    //Used when a value of an object in the scene tree changes, for instance with an undo action. This is just an idicator that any of the ids provided in the event should be updated.
    class SceneTreeValueChangeEvent : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::SceneTreeValueChange)

        EntryId id;
    };

    class SceneTreeItemDeleted : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::SceneTreeItemDeleted)

        EntryId id;
    };

    class EditorSelectionToolChange : public EditorEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Editor)
        SOUTHSEA_EDITOR_EVENT_CATEGORY(EditorEventCategory::SelectionToolChange)

        SelectionTool newTool;
    };
}
