#pragma once

namespace Southsea{
    #define SOUTHSEA_EVENT_TYPE(eType) virtual EventType type() const { return eType; }

    enum class EventType{
        Null = 0,
        System,
        Project,
        EditorGui,
        Editor,
        Command,
        Scene
    };

    class Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Null)
    };
};
