#pragma once

#include "Event.h"

namespace Southsea{
    class Project;

    #define SOUTHSEA_PROJECT_EVENT_CATEGORY(c) virtual ProjectEventCategory eventCategory() const { return c; };

    enum class ProjectEventCategory{
        Null,
        BasicProjectCreated,
        ProjectOpened,
        ProjectStartedEditing,
    };

    class ProjectEvent : public Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Project)
        SOUTHSEA_PROJECT_EVENT_CATEGORY(ProjectEventCategory::Null)
    };

    class ProjectEventBasicProjectCreated : public ProjectEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Project)
        SOUTHSEA_PROJECT_EVENT_CATEGORY(ProjectEventCategory::BasicProjectCreated)

        Project* createdProject;
    };

    //Opened a project from the file system, where it is loaded into the list.
    class ProjectEventProjectOpened : public ProjectEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Project)
        SOUTHSEA_PROJECT_EVENT_CATEGORY(ProjectEventCategory::ProjectOpened)

        Project* openedProject;
    };

    class ProjectEventProjectStartedEditing : public ProjectEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::Project)
        SOUTHSEA_PROJECT_EVENT_CATEGORY(ProjectEventCategory::ProjectStartedEditing)

        Project* openedProject;
        Map* openedMap;
    };
}
