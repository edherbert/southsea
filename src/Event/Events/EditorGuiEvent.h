#pragma once

#include "Event.h"

#include <string>

namespace Southsea{
    struct Resource;
    struct ResourceFSEntry;

    #define SOUTHSEA_EDITORGUI_EVENT_CATEGORY(c) virtual EditorGuiEventCategory eventCategory() const { return c; };

    enum class EditorGuiEventCategory{
        Null,
        CursorLeftEnteredRenderWindow,
        MouseMovedRenderWindow,
        MouseButtonRenderWindow,
        ResizeRenderWindow,
        ChunkSwitcherGridMove,
        ResourceDrag,
        SceneTreeDrag
    };

    class EditorGuiEvent : public Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::Null)
    };

    class EditorGuiCursorLeftEnteredRenderWindow : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::CursorLeftEnteredRenderWindow)

        //In future I might want to be able to have multiple render windows. While I don't plan on doing that right now, I don't want to design myself into a corner.
        //This would eventually be an id of which render window the cursor left.
        int renderWindowId = 0;

        bool cursorLeft;
    };

    class EditorGuiMouseMovedRenderWindow : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::MouseMovedRenderWindow)

        int renderWindowId = 0;

        int mouseX, mouseY;
    };

    class EditorGuiMouseButtonRenderWindow : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::MouseButtonRenderWindow)

        int renderWindowId = 0;

        //X and Y values there more for convenience than necessity.
        int mouseX, mouseY;
        int mouseButton;
        bool buttonDown;
    };

    class EditorGuiWindowResize : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::ResizeRenderWindow)

        int renderWindowId = 0;

        int oldWidth, oldHeight;
        int newWidth, newHeight;
    };

    class EditorGuiChunkSwitcherGridMouseMove : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::ChunkSwitcherGridMove)

        bool gridMoveStarted = false;
    };

    enum class DragEventType{
        Started,
        Completed, //Finished with the mouse button being released, meaning the drag should be actuated.
        Aborted //Aborted with something like the escape key. In this case regardless of where the mouse was it should be ignored and the drag will end.
    };

    class EditorGuiResourceDrag : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::ResourceDrag)

        const ResourceFSEntry* entry = 0;
        const Resource* draggedResource = 0;
        std::string fullPath;
        DragEventType dragType;
    };

    class EditorGuiSceneTreeDrag : public EditorGuiEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::EditorGui)
        SOUTHSEA_EDITORGUI_EVENT_CATEGORY(EditorGuiEventCategory::SceneTreeDrag)

        DragEventType dragType;
    };
}
