#pragma once

#include "Event.h"

namespace Southsea{

    #define SOUTHSEA_SYSTEM_EVENT_CATEGORY(c) virtual SystemEventCategory eventCategory() const { return c; };

    enum class SystemEventCategory{
        Null,
        WindowCloseRequest,
        WindowResize
    };

    class SystemEvent : public Event{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::System)
        SOUTHSEA_SYSTEM_EVENT_CATEGORY(SystemEventCategory::Null)
    };

    class SystemEventWindowCloseRequest : public SystemEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::System)
        SOUTHSEA_SYSTEM_EVENT_CATEGORY(SystemEventCategory::WindowCloseRequest)
    };

    class SystemEventWindowResize : public SystemEvent{
    public:
        SOUTHSEA_EVENT_TYPE(EventType::System)
        SOUTHSEA_SYSTEM_EVENT_CATEGORY(SystemEventCategory::WindowResize)

        int width, height;
        //Often times high-res screens cause width and height to be calculated differently.
        //Sizes are often measured in points rather than pixels, and so the width and height might not actually map to the number of pixels on the screen.
        //These values are the number of pixels on the screen.
        float widthScale, heightScale;
    };
}
