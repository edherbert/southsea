#include "WindowInputManager.h"

#include "imgui.h"
#include "Platforms/Window/SDL2Window.h"
#include <SDL.h>

#include <iostream>
#include "Event/EventDispatcher.h"
#include "Event/Events/CommandEvent.h"

namespace Southsea{

    static char* g_ClipboardTextData = NULL;
    static const char* ImGui_ImplSDL2_GetClipboardText(void*){
        if (g_ClipboardTextData)
            SDL_free(g_ClipboardTextData);
        g_ClipboardTextData = SDL_GetClipboardText();
        return g_ClipboardTextData;
    }

    static void ImGui_ImplSDL2_SetClipboardText(void*, const char* text){
        SDL_SetClipboardText(text);
    }


    WindowInputManager::WindowInputManager(SDL2Window* w)
    : mWindow(w) {

    }

    WindowInputManager::~WindowInputManager(){

    }

    void WindowInputManager::setup(){
        // Setup back-end capabilities flags
        ImGuiIO& io = ImGui::GetIO();
        io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
        io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;
        io.BackendPlatformName = "imgui_impl_ogre_sdl";

        // Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
        io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB;
        io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
        io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
        io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
        io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
        io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
        io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
        io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
        io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
        io.KeyMap[ImGuiKey_Insert] = SDL_SCANCODE_INSERT;
        io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
        io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
        io.KeyMap[ImGuiKey_Space] = SDL_SCANCODE_SPACE;
        io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
        io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;
        io.KeyMap[ImGuiKey_A] = SDL_SCANCODE_A;
        io.KeyMap[ImGuiKey_C] = SDL_SCANCODE_C;
        io.KeyMap[ImGuiKey_V] = SDL_SCANCODE_V;
        io.KeyMap[ImGuiKey_X] = SDL_SCANCODE_X;
        io.KeyMap[ImGuiKey_Y] = SDL_SCANCODE_Y;
        io.KeyMap[ImGuiKey_Z] = SDL_SCANCODE_Z;

        io.SetClipboardTextFn = ImGui_ImplSDL2_SetClipboardText;
        io.GetClipboardTextFn = ImGui_ImplSDL2_GetClipboardText;
        io.ClipboardUserData = NULL;
    }

    bool WindowInputManager::handleMouseEvent(const SDL_Event* event){

        ImGuiIO& io = ImGui::GetIO();
        switch(event->type){
            case SDL_MOUSEWHEEL:
                {
                    if (event->wheel.x > 0) io.MouseWheelH += 1;
                    if (event->wheel.x < 0) io.MouseWheelH -= 1;
                    if (event->wheel.y > 0) io.MouseWheel += 1;
                    if (event->wheel.y < 0) io.MouseWheel -= 1;
                    return true;
                }
            case SDL_MOUSEMOTION:
                {
                    float mouseX = event->motion.x;
                    float mouseY = event->motion.y;

                    if(!_updateGrabbedMouse(mouseX, mouseY)){
                        io.MousePos = ImVec2(mouseX * mWindow->getWidthScale(), mouseY * mWindow->getHeightScale());
                    }

                    return true;
                }
            case SDL_MOUSEBUTTONUP:
            case SDL_MOUSEBUTTONDOWN:
                {
                    int targetButton = 0;
                    switch(event->button.button){
                        case SDL_BUTTON_LEFT:
                            targetButton = 0;
                            break;
                        case SDL_BUTTON_RIGHT:
                            targetButton = 1;
                            break;
                        case SDL_BUTTON_MIDDLE:
                            targetButton = 2;
                            break;
                    }

                    io.MouseDown[targetButton] = event->type == SDL_MOUSEBUTTONDOWN;
                    return true;
                }
            default:
                {
                    assert(false); //If a mouse event wasn't input then fail.
                }
        }

        return false;
    }

    void WindowInputManager::_warpMouseToWindowPos(float x, float y){
        ImGuiIO& io = ImGui::GetIO();

        SDL_WarpMouseInWindow(mWindow->getSDLWindow(), x, y);

        ImVec2 pos(x * mWindow->getWidthScale(), y * mWindow->getHeightScale());
        io.MousePos = pos;
        io.MousePosPrev = pos; //We need to also set this so the deltas don't show incorrect values.
    }

    bool WindowInputManager::_updateGrabbedMouse(float mouseX, float mouseY){
        if(!mWindow->isMouseGrabbed()) return false;

        //Make sure the cursor doesn't move too far from the centre of the window.
        static const float cursorRectSize = 500.0f;

        const float centreX = mWindow->getWidth() / 2;
        const float centreY = mWindow->getHeight() / 2;

        if(mouseX < centreX - cursorRectSize
            || mouseY < centreY - cursorRectSize
            || mouseX > centreX + cursorRectSize
            || mouseY > centreY + cursorRectSize){
                //The mouse has left the bounding box and needs to be warped back to the centre.
                _warpMouseToWindowPos(centreX, centreY);
                return true;
            }

        return false;
    }

    bool WindowInputManager::handleTextInput(const SDL_Event* event){
        assert(event->type == SDL_TEXTINPUT);

        ImGuiIO& io = ImGui::GetIO();
        io.AddInputCharactersUTF8(event->text.text);

        return true;
    }

    void WindowInputManager::_handleKeyCommand(int key, bool ctrlMod, bool shiftMod){
        if(key == SDL_SCANCODE_Z && !shiftMod){ //Undo
            CommandEventUndo e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }else if(key == SDL_SCANCODE_Z && shiftMod){ //Redo
            CommandEventRedo e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }else if(key == SDL_SCANCODE_S && !shiftMod){ //Save
            CommandEventSave e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }else if(key == SDL_SCANCODE_A && !shiftMod){
            CommandEventSelection e;
            e.t = shiftMod ? CommandEventSelection::SelectionType::DeSelect : CommandEventSelection::SelectionType::SelectAll;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }else if(key == SDL_SCANCODE_C && !shiftMod){
            CommandEventCopy e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }else if(key == SDL_SCANCODE_V && !shiftMod){
            CommandEventPaste e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }else if(key == SDL_SCANCODE_X && !shiftMod){
            CommandEventCut e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }

        else if(key == SDL_SCANCODE_D && shiftMod && !ctrlMod){
            CommandEventDuplicate e;
            EventDispatcher::transmitEvent(EventType::Command, e);
        }
    }

    bool WindowInputManager::handleKeyInput(const SDL_Event* event){
        ImGuiIO& io = ImGui::GetIO();

        int key = event->key.keysym.scancode;
        IM_ASSERT(key >= 0 && key < IM_ARRAYSIZE(io.KeysDown));

        io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
        io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
        io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
        io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);

        if((io.KeyCtrl || io.KeyShift) && event->type == SDL_KEYDOWN && event->key.repeat == 0 && !io.WantCaptureKeyboard){
            //If the control key is down, this should be considered to be a command.
            _handleKeyCommand(key, io.KeyCtrl, io.KeyShift);
        }

        io.KeysDown[key] = (event->type == SDL_KEYDOWN);
        //std::cout << key << '\n';

        return true;
    }
}
