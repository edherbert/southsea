#pragma once

#include "SDL_events.h"

namespace Southsea{
    class SDL2Window;

    /**
    A class to deal with input from the SDL2 window.
    Processes it and packages it in a way that imgui can understand.
    */
    class WindowInputManager{
    public:
        WindowInputManager(SDL2Window* w);
        ~WindowInputManager();

        bool handleMouseEvent(const SDL_Event* event);
        bool handleTextInput(const SDL_Event* event);
        bool handleKeyInput(const SDL_Event* event);

        //Setup imgui with the capabilities of the window.
        void setup();

    private:
        SDL2Window* mWindow;

        /**
        Update the grabbed mouse, to determine if it needs to be re-warped.

        @return
        Whether or not the mouse was warped in this update.
        */
        bool _updateGrabbedMouse(float mouseX, float mouseY);
        void _warpMouseToWindowPos(float x, float y);

        void _handleKeyCommand(int key, bool ctrlMod, bool shiftMod);
    };
}
