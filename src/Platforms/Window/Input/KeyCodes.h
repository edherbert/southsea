#pragma once

namespace Southsea{
    #define SOUTHSEA_KEY_CODE_1 30
    #define SOUTHSEA_KEY_CODE_2 31
    #define SOUTHSEA_KEY_CODE_3 32
    #define SOUTHSEA_KEY_CODE_4 33
    #define SOUTHSEA_KEY_CODE_DELETE 76
    #define SOUTHSEA_KEY_CODE_BACKSPACE 42
    #define SOUTHSEA_KEY_CODE_ALT 42
}
