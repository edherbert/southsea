#include "SDL2Window.h"

#include <OgreWindowEventUtilities.h>
#include "OgreRenderWindow.h"
#include "OgreStringConverter.h"

#include <SDL.h>
#include <SDL_syswm.h>

#include "imgui.h"
#include "nfd.h"

#include "Event/EventDispatcher.h"
#include "Event/Events/SystemEvent.h"
#include "Event/Events/CommandEvent.h"

#include "Input/WindowInputManager.h"

#ifdef __APPLE__
    #import <AppKit/NSWindow.h>
#endif

namespace Southsea{
    SDL2Window::SDL2Window()
    : mInputManager(std::make_shared<WindowInputManager>(this)),
      mWidth(1600),
      mHeight(1200),
      mOpen(false) {

        EventDispatcher::subscribe(EventType::Command, SOUTHSEA_BIND(SDL2Window::commandEventReceiver));

    }

    SDL2Window::~SDL2Window(){
        for(int i = 0; i < 8; i++)
            SDL_FreeCursor(mSystemCursors[i]);
    }

    bool SDL2Window::open(){
        if(isOpen()){
            //If the window is already open don't open it again.
            return false;
        }

        if(SDL_Init(SDL_INIT_VIDEO) < 0){
            return false;
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        Uint32 flags = SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI;
        mSDLWindow = SDL_CreateWindow("Southsea", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, mWidth, mHeight, flags);

        _updateScaleVals();

        static const SDL_SystemCursor cursor[8] = {
            SDL_SYSTEM_CURSOR_ARROW,
            SDL_SYSTEM_CURSOR_IBEAM,
            SDL_SYSTEM_CURSOR_SIZEALL,
            SDL_SYSTEM_CURSOR_SIZENS,
            SDL_SYSTEM_CURSOR_SIZEWE,
            SDL_SYSTEM_CURSOR_SIZENESW,
            SDL_SYSTEM_CURSOR_SIZENWSE,
            SDL_SYSTEM_CURSOR_HAND
        };
        for(int i = 0; i < 8; i++){
            mSystemCursors[i] = SDL_CreateSystemCursor(cursor[i]);
                printf(" %s\n", SDL_GetError());
        }

        NFD_Init();

        mOpen = true;
        return true;
    }

    bool SDL2Window::close(){
        if(!isOpen()){
            return false;
        }

        mOpen = false;
        mOgreWindow->destroy();

        SDL_DestroyWindow(mSDLWindow);
        SDL_Quit();

        NFD_Quit();

        return true;
    }

    void SDL2Window::update(){
        Ogre::WindowEventUtilities::messagePump();
        SDL_PumpEvents();

        _pollForEvents();
        _updateMouseCursor();
    }

    void SDL2Window::_pollForEvents(){
        SDL_Event event;
        while(SDL_PollEvent(&event)){
            _handleEvent(event);
        }
    }

    void SDL2Window::_updateMouseCursor(){
        int type = mGrabbedMouse ? SDL_DISABLE : SDL_ENABLE;
        SDL_ShowCursor(type);

        ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
        SDL_SetCursor(mSystemCursors[imgui_cursor]);
    }

    void SDL2Window::_handleEvent(const SDL_Event &event){
        switch(event.type){
            case SDL_QUIT: {
                SystemEventWindowCloseRequest closeEvent;
                //Here we just send out an event to let the other parts of the system know that a close was requested.
                //The window itself isn't closed here, incase we want to popup a dialog to ask the user to save (for instance).
                EventDispatcher::transmitEvent(EventType::System, closeEvent);
                break;
            }
            case SDL_WINDOWEVENT:
                {
                    switch(event.window.event){
                        case SDL_WINDOWEVENT_RESIZED:
                            _handleResizeEvent(event);
                            break;
                    }
                    break;
                }
            case SDL_MOUSEWHEEL:
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            case SDL_MOUSEMOTION:
                {
                    mInputManager->handleMouseEvent(&event);
                    break;
                }
            case SDL_TEXTINPUT:
                {
                    mInputManager->handleTextInput(&event);
                    break;
                }
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                {
                    mInputManager->handleKeyInput(&event);
                    break;
                }
        }
    }

    void SDL2Window::setupImgui(){
        mInputManager->setup();
    }

    void SDL2Window::injectOgreWindow(Ogre::RenderWindow *window) {
        mOgreWindow = window;
        window->resize(mWidth, mHeight);
    }

    void SDL2Window::_updateScaleVals(){
        int w, h;
        SDL_GL_GetDrawableSize(mSDLWindow, &w, &h);
        mWidthScale = w / mWidth;
        mHeightScale = h / mHeight;
    }

    void SDL2Window::_handleResizeEvent(const SDL_Event &event){
        assert(event.window.event == SDL_WINDOWEVENT_RESIZED);

        mWidth = event.window.data1;
        mHeight = event.window.data2;

        _updateScaleVals();

        if(mOgreWindow){
            #ifdef _WIN32
                mOgreWindow->windowMovedOrResized();
            #else
                mOgreWindow->resize(mWidth, mHeight);
            #endif
        }

        SystemEventWindowResize e;
        e.width = mWidth;
        e.height = mHeight;
        e.widthScale = mWidthScale;
        e.heightScale = mHeightScale;

        EventDispatcher::transmitEvent(EventType::System, e);
    }

    Ogre::String SDL2Window::getHandle() const {
        SDL_SysWMinfo wmInfo;
        SDL_VERSION( &wmInfo.version );

        if(!SDL_GetWindowWMInfo(mSDLWindow, &wmInfo)){
            //SDL failed to query window information to obtain the window handle.
            assert(false);
        }

        #ifdef __APPLE__
            NSWindow *window = wmInfo.info.cocoa.window;
            NSView *view = [window contentView];
            return Ogre::StringConverter::toString( (unsigned long)view );
        #elif __linux__
            return Ogre::StringConverter::toString( (uintptr_t) wmInfo.info.x11.window);
        #elif _WIN32
            return Ogre::StringConverter::toString((uintptr_t)wmInfo.info.win.window);
        #endif
    }

    void SDL2Window::grabMouse(bool grabbed){
        if(mGrabbedMouse == grabbed) return;
        mGrabbedMouse = grabbed;
    }

    bool SDL2Window::commandEventReceiver(const Event &e){
        const CommandEvent& commandEvent = (const CommandEvent&)e;
        if(commandEvent.eventCategory() == CommandEventCategory::GrabCursor){
            const CommandEventGrabCursor& event = (const CommandEventGrabCursor&)commandEvent;

            grabMouse(event.grabCursor);
        }

        return false;
    }

}
