#pragma once

#include "SDL_events.h"
#include "OgreString.h"

namespace Ogre{
    class RenderWindow;
}

namespace Southsea{
    class WindowInputManager;
    class Event;

    class SDL2Window{
    public:
        SDL2Window();
        ~SDL2Window();

        void update();

        bool open();
        bool close();

        int getWidth() const { return mWidth; }
        int getHeight() const { return mHeight; }

        float getWidthScale() const { return mWidthScale; }
        float getHeightScale() const { return mHeightScale; }

        bool isOpen() const { return mOpen; }

        Ogre::RenderWindow* getRenderWindow() const { return mOgreWindow; }
        SDL_Window* getSDLWindow() const { return mSDLWindow; }

        void injectOgreWindow(Ogre::RenderWindow *window);
        Ogre::String getHandle() const;
        void grabMouse(bool grabbed);

        bool isMouseGrabbed() { return mGrabbedMouse; }

        bool commandEventReceiver(const Event &e);

        void setupImgui();

    private:
        SDL_Window* mSDLWindow;
        std::shared_ptr<WindowInputManager> mInputManager;

        int mWidth, mHeight;
        float mWidthScale, mHeightScale;
        bool mOpen;
        bool mGrabbedMouse = false;

        void _pollForEvents();
        void _handleEvent(const SDL_Event &event);
        void _handleResizeEvent(const SDL_Event &event);

        void _updateScaleVals();

        void _updateMouseCursor();

        SDL_Cursor* mSystemCursors[8];

        Ogre::RenderWindow* mOgreWindow;
    };
}
