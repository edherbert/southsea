#pragma once

#include "OgreSetup.h"

#include "Ogre.h"
#include <OgreHlmsPbs.h>
#include <OgreHlmsUnlit.h>

#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerra.h"
#include "System/Setup/SystemSettings.h"
#include "filesystem/path.h"

#include "Compositor/OgreCompositorChannel.h"

namespace Southsea{
    class WindowsOgreSetup : public OgreSetup{
    public:
        WindowsOgreSetup() {}

        Ogre::Root* setupRoot(){
            Ogre::Root *root = new Ogre::Root();

            //root->loadPlugin("RenderSystem_Direct3D11_d");
            root->loadPlugin("RenderSystem_GL3Plus_d");
            root->setRenderSystem(root->getAvailableRenderers()[0]);
            root->getRenderSystem()->setConfigOption( "sRGB Gamma Conversion", "Yes" );
            root->initialise(false);

            return root;
        }

        void setupOgreWindow(SDL2Window *window){
            Ogre::NameValuePairList params;

            params["externalWindowHandle"] = window->getHandle();

            Ogre::RenderWindow *renderWindow = Ogre::Root::getSingleton().createRenderWindow("Ogre Window", window->getWidth(), window->getHeight(), false, &params);
            renderWindow->setVisible(true);

            window->injectOgreWindow(renderWindow);
        }

        void setupHLMS(Ogre::Root *root){
            Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
            const std::string &rPath = SystemSettings::getMasterPath();

            const Ogre::String renderSystemPrefix = renderSystem->getName() == "OpenGL 3+ Rendering Subsystem" ? "GLSL" : "HLSL";

            {
                Ogre::ArchiveVec library;

                library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Common/" + renderSystemPrefix, "FileSystem", true ));

                library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Unlit/Any", "FileSystem", true ));
                library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Pbs/Any", "FileSystem", true ));

                Ogre::Archive *archivePbs;
                Ogre::Archive *archiveUnlit;

                archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Pbs/" + renderSystemPrefix, "FileSystem", true );
                archiveUnlit = Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Unlit/" + renderSystemPrefix, "FileSystem", true );
                Ogre::HlmsPbs *hlmsPbs = OGRE_NEW Ogre::HlmsPbs( archivePbs, &library );
                Ogre::HlmsUnlit *hlmsUnlit = OGRE_NEW Ogre::HlmsUnlit( archiveUnlit, &library );

                root->getHlmsManager()->registerHlms(hlmsPbs);
                root->getHlmsManager()->registerHlms(hlmsUnlit);
            }

            //Terra hlms
            {
                std::vector<Ogre::String> cont;
                cont.push_back(rPath + "Hlms/Common/" + renderSystemPrefix);
                cont.push_back(rPath + "Hlms/Common/Any");
                cont.push_back(rPath + "Hlms/Pbs/Any");
                cont.push_back(rPath + "Hlms/Pbs/" + renderSystemPrefix);
                Ogre::ArchiveVec l;

                for(Ogre::String s : cont){
                    Ogre::Archive *a = Ogre::ArchiveManager::getSingletonPtr()->load(s,"FileSystem", true );
                    l.push_back(a);
                }

                Ogre::Archive *archiveTerra = Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "Hlms/Terra/" + renderSystemPrefix, "FileSystem", true );
                Ogre::HlmsTerra *hlmsTerra = OGRE_NEW Ogre::HlmsTerra( archiveTerra, &l );
                Ogre::HlmsManager *hlmsManager = root->getHlmsManager();
                hlmsManager->registerHlms( hlmsTerra );
            }

            Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups(false);

        }

    };
}
