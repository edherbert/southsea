#pragma once

#include "OgreSetup.h"

#include "Ogre.h"
#include <OgreHlmsPbs.h>
#include <OgreHlmsUnlit.h>

#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerra.h"
#include "System/Setup/SystemSettings.h"
#include "filesystem/path.h"

#include "Compositor/OgreCompositorChannel.h"

namespace Southsea{
    class LinuxOgreSetup : public OgreSetup{
    public:
        LinuxOgreSetup() {}

        Ogre::Root* setupRoot(){
            Ogre::Root *root = new Ogre::Root();

            //TODO Remove this when the system settings thing is implemented.
            filesystem::path masterPath(SystemSettings::getMasterPath());
            filesystem::path renderSystemPath = masterPath / filesystem::path("RenderSystem_GL3Plus.so");
            filesystem::path particleFXPath = masterPath / filesystem::path("Plugin_ParticleFX.so");

            root->loadPlugin(renderSystemPath.str());
            root->loadPlugin(particleFXPath.str());
            root->setRenderSystem(root->getAvailableRenderers()[0]);
            root->getRenderSystem()->setConfigOption( "sRGB Gamma Conversion", "Yes" );
            root->initialise(false);

            return root;
        }

        void setupOgreWindow(SDL2Window *window){
            Ogre::NameValuePairList params;

            params["parentWindowHandle"] = window->getHandle();

            Ogre::RenderWindow *renderWindow = Ogre::Root::getSingleton().createRenderWindow("Ogre Window", window->getWidth(), window->getHeight(), false, &params);
            renderWindow->setVisible(true);

            renderWindow->setVSyncEnabled(true);

            window->injectOgreWindow(renderWindow);
        }

        void setupHLMS(Ogre::Root *root){
            Ogre::RenderSystem *renderSystem = Ogre::Root::getSingletonPtr()->getRenderSystem();
            const std::string &rPath = SystemSettings::getMasterPath();

            {
                Ogre::ArchiveVec library;

                library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Common/GLSL", "FileSystem", true ));

                library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Unlit/Any", "FileSystem", true ));
                library.push_back(Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Pbs/Any", "FileSystem", true ));

                Ogre::Archive *archivePbs;
                Ogre::Archive *archiveUnlit;

                archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Pbs/GLSL", "FileSystem", true );
                archiveUnlit = Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "/Hlms/Unlit/GLSL", "FileSystem", true );
                Ogre::HlmsPbs *hlmsPbs = new Ogre::HlmsPbs( archivePbs, &library );
                Ogre::HlmsUnlit *hlmsUnlit = new Ogre::HlmsUnlit( archiveUnlit, &library );

                root->getHlmsManager()->registerHlms(hlmsPbs);
                root->getHlmsManager()->registerHlms(hlmsUnlit);
            }

            //Terra hlms
            {
                std::vector<Ogre::String> cont;
                cont.push_back(rPath + "Hlms/Common/GLSL");
                cont.push_back(rPath + "Hlms/Common/Any");
                cont.push_back(rPath + "Hlms/Pbs/Any");
                cont.push_back(rPath + "Hlms/Pbs/GLSL");
                Ogre::ArchiveVec l;

                for(Ogre::String s : cont){
                    Ogre::Archive *a = Ogre::ArchiveManager::getSingletonPtr()->load(s,"FileSystem", true );
                    l.push_back(a);
                }

                Ogre::Archive *archiveTerra = Ogre::ArchiveManager::getSingletonPtr()->load(rPath + "Hlms/Terra/GLSL", "FileSystem", true );
                Ogre::HlmsTerra *hlmsTerra = OGRE_NEW Ogre::HlmsTerra( archiveTerra, &l );
                Ogre::HlmsManager *hlmsManager = root->getHlmsManager();
                hlmsManager->registerHlms( hlmsTerra );
            }

            Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups(false);

        }

    };
}
