#pragma once

#include "OgreString.h"

namespace Southsea{
    static const Ogre::String SOUTHSEA_INTERNAL_GROUP_NAME = "SouthseaInternal";
    static const Ogre::String SOUTHSEA_RESOURCE_GROUP_HEADER = "SouthseaGroup/";

    class SouthseaResourceGroupUtils{
    public:
        //Slightly obfuscates the resource names with a header to avoid clashes.
        static void _toSouthseaResLocation(const Ogre::String& original, Ogre::String& outString){
            outString = SOUTHSEA_RESOURCE_GROUP_HEADER + original;
        }
    };
}
