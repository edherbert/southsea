#pragma once

#include "Ogre.h"
#include "filesystem/path.h"
#include "System/Setup/SystemSettings.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerra.h"
#include "Editor/Chunk/Terrain/terra/Hlms/OgreHlmsTerraDatablock.h"

#include <Compositor/OgreCompositorManager2.h>
#include "Compositor/OgreCompositorWorkspace.h"

#include "SouthseaResourceGroups.h"

namespace Ogre {
    class Root;
}

namespace Southsea {
    class Window;

    /**
     An interface to setup Ogre.

     Abstracting the setup details means the intricate setup details can be more platform specific.
     This would include tasks like setting up the hlms shaders and the compositor.
     The more diverse the supported platforms, the more these steps will differ, so its worth the abstraction.
     */
    class OgreSetup{
    public:
        OgreSetup() {}
        virtual ~OgreSetup() {}
        virtual Ogre::Root* setupRoot() = 0;
        virtual void setupHLMS(Ogre::Root *root) = 0;
        virtual void setupOgreWindow(SDL2Window *window) = 0;

        void setupOgreResources(Ogre::Root *root){

            //Essential paths that are always used by Southsea. These are expected to have been copied into the distribution by the build system.
            const std::string& masterPath = SystemSettings::getMasterPath();
            root->addResourceLocation(masterPath + "/essential/viewport", "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);
            root->addResourceLocation(masterPath + "/essential/meshes", "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);

            const Ogre::String renderSystemPrefix = root->getRenderSystem()->getName() == "OpenGL 3+ Rendering Subsystem" ? "GLSL" : "HLSL";

            //Stuff for terrain
            {
                root->addResourceLocation(masterPath + "/essential/terrain", "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);
                root->addResourceLocation(masterPath + "/essential/terrain/" + renderSystemPrefix, "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);
                root->addResourceLocation(masterPath + "/essential/common", "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);
                root->addResourceLocation(masterPath + "/essential/common/" + renderSystemPrefix, "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);
                root->addResourceLocation(masterPath + "/essential/compositor", "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);

                //TODO temporary!
                //root->addResourceLocation("/home/edward/Documents/avData/common/textures", "FileSystem", SOUTHSEA_INTERNAL_GROUP_NAME);
            }

            //Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups(false);
        }

        void setupCompositor(Ogre::Root *root, Ogre::SceneManager* sceneManager, Ogre::Camera *camera, Ogre::RenderWindow *window){
            Ogre::CompositorManager2 *compositorManager = root->getCompositorManager2();

            //This workspace doesn't render the scene. It just clears the colour and draws the imgui menus.
            compositorManager->addWorkspace( sceneManager, window, camera, "Southsea/GuiViewportWorkspace", true );
        }

        void setupScene(Ogre::Root *root, Ogre::SceneManager **_sceneManager, Ogre::Camera **_camera){
            Ogre::SceneManager *sceneManager = root->createSceneManager(Ogre::ST_GENERIC, 1, Ogre::INSTANCING_CULLING_SINGLETHREAD, "guiScene");

            Ogre::Camera *camera = sceneManager->createCamera("guiCamera");

            *_sceneManager = sceneManager;
            *_camera = camera;

            Ogre::TexturePtr blendTex;
            for(int i = -1; i < 8; i++){
                using namespace Ogre;
                //Create the blendmap texture which is used by all blendmaps.
                const Ogre::uint16 size = 1024;
                blendTex = TextureManager::getSingleton().createManual(
                    "blendMapTarget" + std::to_string(i),
                    SOUTHSEA_INTERNAL_GROUP_NAME,
                    TEX_TYPE_2D_ARRAY, size, size, 1, 0, PF_R8G8B8A8, TU_DYNAMIC_WRITE_ONLY);
            }

            {
                using namespace Ogre;
                //Create the render target for the viewport.
                TexturePtr viewportTexture = TextureManager::getSingleton().createManual(
                    "renderViewportTarget",
                    SOUTHSEA_INTERNAL_GROUP_NAME,
                    TEX_TYPE_2D, 500, 500,
                    1, PF_R8G8B8, TU_RENDERTARGET );
            }

            { //Create the textures for the default datablock.
                using namespace Ogre;
                const uint16 size = 256;
                for(int i = 0; i < 4; i++){

                    Ogre::Vector4 cols = Ogre::Vector4::ZERO;
                    cols[i] = 1.0f;

                    if(i == 3){ //Special case. Just draw a grey colour for the fourth and final texture.
                        cols[0] = 1;
                        cols[1] = 1;
                        cols[2] = 1;
                    }

                    const Ogre::String name = "terraDefaultTex" + std::to_string(i);
                    TexturePtr tex = TextureManager::getSingleton().createManual(
                        name,
                        SOUTHSEA_INTERNAL_GROUP_NAME,
                        TEX_TYPE_2D_ARRAY, size, size, 1, 0, PF_R8G8B8, TU_DYNAMIC_WRITE_ONLY);

                        //Write the pixels.
                        v1::HardwarePixelBufferSharedPtr pixelBufferBuf = tex->getBuffer(0, 0);
                        const Ogre::PixelBox &pixBox =
                        pixelBufferBuf->lock(Ogre::Image::Box(0, 0, pixelBufferBuf->getWidth(), pixelBufferBuf->getHeight()), Ogre::v1::HardwareBuffer::HBL_NORMAL);

                        Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixBox.data);

                        //Checkerboard pattern
                        const uint8 squareSize = 5;
                        bool draw = false;
                        bool invY = false;
                        for(int y = 0; y < size; y++){

                            if(y % squareSize == 0){
                                invY = !invY;
                            }

                            for(int x = 0; x < size; x++){
                                bool shouldDraw = false;
                                uint8 drawAmount = 0;

                                if(x % squareSize == 0){
                                    draw = !draw;
                                }
                                shouldDraw = draw;

                                if(invY) shouldDraw = !shouldDraw;
                                if(shouldDraw){
                                    drawAmount = 200;
                                }

                                *pDest++ = cols.z * drawAmount;
                                *pDest++ = cols.y * drawAmount;
                                *pDest++ = cols.x * drawAmount;
                                *pDest++ = 0;
                            }
                        }

                        pixelBufferBuf->unlock();
                }
            }

            { //Setup the default datablock for terra.
                using namespace Ogre;
                Ogre::Hlms* hlms = Ogre::Root::getSingletonPtr()->getHlmsManager()->getHlms("Terra");
                Ogre::HlmsTerra* terraHlms = dynamic_cast<Ogre::HlmsTerra*>(hlms);

                Ogre::HlmsDatablock* block = terraHlms->createDatablock(Ogre::IdString("defaultTerra"), "defaultTerra", Ogre::HlmsMacroblock(), Ogre::HlmsBlendblock(), Ogre::HlmsParamVec());

                Ogre::HlmsTerraDatablock* tBlock = dynamic_cast<Ogre::HlmsTerraDatablock*>(block);
                //tBlock->setDiffuse(Ogre::Vector3(0, 1, 0)); //Give it a green colour just to make it a bit more interesting.
                tBlock->setTexture(Ogre::TerraTextureTypes::TERRA_DETAIL_WEIGHT, 0, blendTex);

                { //Assign the dummy blend textures to the default datablock.
                    Ogre::TerraTextureTypes t[4] = {TERRA_DETAIL0, TERRA_DETAIL1, TERRA_DETAIL2, TERRA_DETAIL3};
                    for(int i = 0; i < 4; i++){
                        TexturePtr tex = TextureManager::getSingleton().getByName("terraDefaultTex" + std::to_string(i));

                        tBlock->setTexture(t[i], 0, tex);
                    }
                }

            }
        }
    };
}
