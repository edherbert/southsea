#include "System/Base.h"
#include "System/Setup/SystemSetup.h"

#ifdef TEST_MODE_ENABLE
    #include <vector>

    #include "Testing/TestManager.h"
#endif

#ifdef WIN32

#include <windows.h>

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow) {
#else
int main(int argc, char **argv){
#endif

    #ifdef TEST_MODE_ENABLE
        std::vector<std::string> args;

        for(int i = 0; i < argc; i++){
            args.push_back(std::string(argv[i]));
        }

        Southsea::TestManager* testManager = new Southsea::TestManager();
        testManager->parseArgs(args);
    #endif

    Southsea::SystemSetup::setup();

    Southsea::Base base;

    while(!base.hasShutdown()){

        #ifdef TEST_MODE_ENABLE
            if(testManager->getTestReachedEnd()){
                base.shutdown();
                break;
            }
            testManager->update();
        #endif

        base.update();
    }

    #ifdef TEST_MODE_ENABLE
        testManager->shutdown();
        delete testManager;
    #endif

    return 0;
}
