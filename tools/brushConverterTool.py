#!/usr/bin/python3

import sys
from PIL import Image

def main():
    if len(sys.argv) != 3:
        print("Pass an input and output image.")
        return

    img = Image.open(sys.argv[1])

    pixels = list(img.getdata())

    assert len(pixels) == 50*50

    with open(sys.argv[2], 'w') as outfile:
        outfile.write("float brush[BRUSH_DATA_SIZE*BRUSH_DATA_SIZE] = {\n    ")

        counter = 0
        for i in pixels:
            outfile.write( "{:1.4f}".format( i / 255 ) )
            outfile.write(", ")
            if counter == 10:
                outfile.write("\n    ")
                counter = 0
            else:
                counter += 1

        outfile.write("};")


if __name__ == "__main__":
    main()
