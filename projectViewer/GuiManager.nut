GuiManager <- class{

    window = null;
    windowLayout = null;
    worldPositionLabel = null;
    worldOriginLabel = null;
    worldOgrePosition = null;

    setOriginButton = null;

    mPhysicsView = null;
    mMapView = null;

    constructor(){
        _doFile("res://Gui/PhysicsView.nut");
        _doFile("res://Gui/MapView.nut");

        setupGui();

        mPhysicsView = PhysicsView();
        mMapView = MapView();
    }

    function setOriginCallback(widget, action){
        if(action == 2){
            local pos = _world.getPlayerPosition();
            _slotManager.setOrigin(pos);

            ::guiManager.setOriginText(pos);
            ::guiManager.notifyMovement();

            //The camera position needs to be updated.
            local playerPos = pos.toVector3();
            _camera.setPosition(playerPos + Vec3(0, ::wasdCameraHeight, 200));
        }
    }

    function setupGui(){
        window = _gui.createWindow();
        window.setPosition(0, 0);
        window.setSize(500, 100);

        local layout = _gui.createLayoutLine();

        local playerPos = _world.getPlayerPosition();
        worldPositionLabel = window.createLabel();
        setPositionText(playerPos);
        layout.addCell(worldPositionLabel);

        worldOriginLabel = window.createLabel();
        setOriginText(_slotManager.getOrigin());
        layout.addCell(worldOriginLabel);

        worldOgrePosition = window.createLabel();
        setOgrePositionText(playerPos);
        layout.addCell(worldOgrePosition);

        setOriginButton = window.createButton();
        setOriginButton.setText("Set origin");
        layout.addCell(setOriginButton);
        setOriginButton.attachListener(setOriginCallback);

        layout.layout();
    }

    function notifyMovement(){
        local playerPos = _world.getPlayerPosition();
        setPositionText(playerPos);
        setOgrePositionText(playerPos);
    }

    function simpleStrSlot(slotPos){
        return format("%d %d (%d, %d, %d)", slotPos.slotX, slotPos.slotY, slotPos.x, slotPos.y, slotPos.z);
    }

    function setPositionText(slotPos){
        worldPositionLabel.setText("Player Position: " + simpleStrSlot(slotPos));
    }

    function setOriginText(slotPos){
        worldOriginLabel.setText("Origin: " + simpleStrSlot(slotPos));
    }

    function setOgrePositionText(slotPos){
        local realPos = slotPos.toVector3();
        worldOgrePosition.setText( format("Ogre position: %s", realPos.tostring() ));
    }

};
