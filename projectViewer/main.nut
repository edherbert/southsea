function collisionOverride(){
    print("collision object hit.");
}

function start(){
    _doFile("res://MovementTypes.nut");
    _doFile("res://GuiManager.nut");
    setupInput();

    _physics.setCollisionCallbackOverride(collisionOverride);

    local mapName = _settings.getUserSetting("mapName");
    _slotManager.setCurrentMap(mapName);
    _world.createWorld();
    _world.setPlayerPosition(SlotPosition());

    _camera.setPosition(0, 100, 100);
    _camera.lookAt(0, 0, 0);

    ::guiManager <- GuiManager();

    firstInputFrame();
}

function update(){
    if(updateMovement()){
        //A movement occured.
        guiManager.notifyMovement();
    }
    _developer.drawPoint(_world.getPlayerPosition());
}
