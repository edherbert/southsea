enum InputMethod{
    wasdCamera,
    fpsCamera
};

::wasdCameraHeight <- 200;
::currentInputMethod <- InputMethod.wasdCamera;

function setupInput(){
    _input.setActionSets({
        "default" : {
            "Buttons" : {
                "Accept": "#Accept"
            },
            "StickPadGyro" : {
                "Move":"#Move",
                "Camera":"#Camera"
            }
        }
    });

    ::AcceptHandle <- _input.getButtonActionHandle("Accept");
    ::MoveHandle <- _input.getAxisActionHandle("Move");
    ::CameraHandle <- _input.getAxisActionHandle("Camera");

    _input.mapControllerInput(0, ::MoveHandle);
    _input.mapControllerInput(1, ::CameraHandle);
    _input.mapControllerInput(0, ::AcceptHandle);

    _input.mapKeyboardInput(122, ::AcceptHandle);
    _input.mapKeyboardInputAxis(100, 115, 97, 119, ::MoveHandle);
}

function checkInputTypeChange(){
    local rightDown = _input.getMouseButton(_MouseButtonRight);

    //Enable this when the fps camera is actually implemented.
    //currentInputMethod = rightDown ? InputMethod.fpsCamera : InputMethod.wasdCamera;
}

function updateMovement(){
    checkInputTypeChange();

    local movementOccured = false;
    switch(currentInputMethod){
        case InputMethod.wasdCamera: movementOccured = wasdMove(); break;
        case InputMethod.fpsCamera: movementOccured = fpsCamera(); break;
    }

    return movementOccured;
}

function enoughMovement(val){
    return val > 0.1 || val < -0.1;
}

function firstInputFrame(){
    positionWorldAndCamera(SlotPosition(0, 0));
}

function wasdMove(){
    local pos = _world.getPlayerPosition();

    local amount = [
        _input.getAxisActionX(::MoveHandle) * 3,
        0,
        _input.getAxisActionY(::MoveHandle) * 3
    ];

    local cameraMov = _input.getAxisActionY(::CameraHandle);

    //Don't do all this movement logic if there was no movement.
    if( !enoughMovement(amount[0]) && !enoughMovement(amount[2]) && !enoughMovement(cameraMov) ) return false;

    ::wasdCameraHeight += cameraMov;
    pos.move(amount[0], amount[1], amount[2]);

    positionWorldAndCamera(pos);

    return true;
}

function positionWorldAndCamera(pos){
    _world.setPlayerPosition(pos);

    if("playerCollisionObjectPointer" in getroottable()){
        if(playerCollisionObjectPointer != null) playerCollisionObjectPointer.setPosition(pos);
    }

    local playerPos = pos.toVector3();
    _camera.lookAt(playerPos);
    _camera.setPosition(playerPos + Vec3(0, ::wasdCameraHeight, 200));
}

function fpsCamera(){
    //TODO implement this.
    return false;
}
