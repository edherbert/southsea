::MapView <- class{
    window = null;

    constructor(){
        setupGui();
    }

    function mapBoxTextChange(widget, action){ }

    function setMapButtonPressed(widget, action){
        if(action == 3){
            _slotManager.setCurrentMap(::mapNameBox.getText());
        }
    }

    function setupGui(){
        window = _gui.createWindow();
        window.setPosition(0, 500);
        window.setSize(500, 200);

        local layout = _gui.createLayoutLine();

        local titleText = window.createLabel();
        titleText.setText("==Map==");
        layout.addCell(titleText);

        ::mapNameBox <- window.createEditbox();
        mapNameBox.setSize(300, 30);
        mapNameBox.setText("");
        mapNameBox.attachListener(mapBoxTextChange);
        layout.addCell(mapNameBox);

        local setMapButton = window.createButton();
        setMapButton.setText("set map");
        setMapButton.attachListener(setMapButtonPressed);
        layout.addCell(setMapButton);

        layout.layout();
    }
};