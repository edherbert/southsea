::playerPointerCallback <- function(){
    print("player entered");
}

::PhysicsView <- class{
    window = null;

    enableStaticShapes = null;
    dropBodiesButton = null;
    destroyBodiesButton = null;

    collisionObjectOrigin = null;

    bodiesInWorld = null;

    constructor(){
        setupGui();

        ::bodiesInWorld <- [];
        ::cubeShape <- _physics.getCubeShape(1, 1, 1);

        //TODO remove in future.
        ::staticShapesEnabled <- true;
    }

    function enableStaticShapesCallback(widget, action){
        if(action == 3){
            staticShapesEnabled = widget.getValue();
            print("Static physic shapes: " + (staticShapesEnabled ? "Enabled" : "Disabled"));
            _developer.setMeshGroupVisible(1, staticShapesEnabled);
        }
    }

    function collisionObjectPointerCallback(widget, action){
        if(action != 3) return; //Only care about presses.

        ::playerCollisionObjectPointer <- null;

        local enable = widget.getValue();
        if(enable){
            local shape = _physics.getSphereShape(1);
            playerCollisionObjectPointer = _physics.collision[0].createReceiver({ "type" : _COLLISION_PLAYER }, shape);
            _physics.collision[0].addObject(playerCollisionObjectPointer);
            playerCollisionObjectPointer.setPosition(_world.getPlayerPosition());
        }
    }

    function dropBodiesCallback(widget, action){
        if(action == 2){
            print("Dropping");

            local numCubes = 5;
            local constructionInfo = {"origin": [0, 0, 0]};
            local startPos = _world.getPlayerPosition().toVector3();

            for(local z = 0; z < numCubes; z++){
                for(local y = 0; y < 1; y++){
                    for(local x = 0; x < numCubes; x++){
                        constructionInfo["origin"] = [startPos.x + x * 5, startPos.y + (y * 5) + 50, startPos.z + z * 5];
                        local body = _physics.dynamics.createRigidBody(cubeShape, constructionInfo);
                        local mesh = _mesh.create("cube");
                        mesh.attachRigidBody(body);

                        bodiesInWorld.append(mesh);
                        _physics.dynamics.addBody(body);
                    }
                }
            }
        }
    }

    function destroyBodiesCallback(widget, action){
        if(action == 2){
            ::bodiesInWorld.clear();
        }
    }

    function setupGui(){
        window = _gui.createWindow();
        window.setPosition(0, 100);
        window.setSize(500, 400);

        local layout = _gui.createLayoutLine();

        local titleText = window.createLabel();
        titleText.setText("==Physics==");
        layout.addCell(titleText);

        enableStaticShapes = window.createCheckbox();
        enableStaticShapes.setText("Show static shapes");
        enableStaticShapes.setValue(true);
        layout.addCell(enableStaticShapes);
        enableStaticShapes.attachListener(enableStaticShapesCallback);

        dropBodiesButton = window.createButton();
        dropBodiesButton.setText("Drop bodies");
        layout.addCell(dropBodiesButton);
        dropBodiesButton.attachListener(dropBodiesCallback);

        destroyBodiesButton = window.createButton();
        destroyBodiesButton.setText("Destroy current bodies");
        layout.addCell(destroyBodiesButton);
        destroyBodiesButton.attachListener(destroyBodiesCallback);

        //Collison world settings.
        local titleText = window.createLabel();
        titleText.setText("==Collision==");
        layout.addCell(titleText);

        collisionObjectOrigin = window.createCheckbox();
        collisionObjectOrigin.setText("Collision object origin");
        collisionObjectOrigin.setValue(false);
        layout.addCell(collisionObjectOrigin);
        collisionObjectOrigin.attachListener(collisionObjectPointerCallback);

        layout.layout();
    }
};
